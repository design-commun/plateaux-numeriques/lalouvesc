+++
date = ""
description = "Les lieux chargés de spiritualité"
header = "/media/regis-3.gif"
icon = "🕯️"
subtitle = "Les lieux chargés de spiritualité"
title = "Lalouvesc, haut lieu spirituel"
weight = 1

+++
Lalouvesc est le lieu de Pèlerinage le plus important de toute l’Ardèche.  
St Jean-François Régis, est venu en mission à Lalouvesc, le 24 décembre 1640, et y mourut le 31 décembre. Depuis maintenant plus de 3 siècles, d’innombrables pèlerins se pressent aux portes du village afin le vénérer.  
En 1826, Sainte Thérèse Couderc fonda à Lalouvesc la Congrégation des Sœurs du Cénacle. Le pèlerinage qui lui est aujourd’hui dédié trouve un écho à l’échelle mondiale. Le corps de Sainte Thérèse Couderc repose à Lalouvesc dans la basilique Saint Jean-François Régis.

## **L**a Basilique Saint Jean-François Régis

![](/media/basilique_lalouvesc.JPG)

Sa construction dura 12 ans. Elle abrite les reliques du Saint. Les vitraux racontent en image la vie de Saint-Régis. Œuvre de l’architecte Pierre Bossan, la basilique est de style néobyzantin, comme son autre réalisation la Basilique Notre Dame de Fourvière à Lyon. Bâtie en granit, elle comprend outre une nef centrale, deux nefs latérales, deux chapelles latérales et une chapelle axiale. Les voûtes sont soutenues par des colonnes de marbre dont le fût est d’une seule pièce.  
P. Bossan s’est inspiré du style roman, de l’art byzantin et des monuments arabo – chrétiens de Sicile.

![](/media/lalouvesc_basilique_interieur_choeur.jpg)

Dans une chapelle latérale est placée une chasse contenant les ossements de saint Jean-François Régis. Datant du XIXe siècle, elle est signée Armand Caillat.

Fixé sur un bloc de granit, un reliquaire renfermant le corps de sainte Thérèse Couderc est installé dans la chapelle Sainte-Agathe. Il s'agit d'une création de Stéphane Morit (début XXIe siècle).

Les vitraux de la basilique relatent la vie de Jean-François Régis. Ils sont l’œuvre de Lucien Bégule, peintre verrier lyonnais (1848 - 1935). Pour une visite numérique des vitraux, voir le [site consacré à l’œuvre de Lucien Bégule](https://www.lemonde.fr/pixels/article/2022/01/19/cinq-etoiles-et-10-10-pourquoi-il-ne-faut-pas-faire-confiance-aux-notes-des-internautes_6110162_4408996.html). L'orgue a été inauguré en 1932.

Située sous le portail principal, les deux clochers et la première travée, une crypte a été construite à l'emplacement de l'église bâtie au XVIIIe siècle. Ce lieu a accueilli pendant 75 ans le tombeau de Jean-François Régis. Il contient de très nombreux ex-voto.

Subsiste une petite cloche dite _Cloche de saint Régis_, datée 1551 et réparée en 1875. Pesant 60 kg, elle sonna la mort du saint en 1640. Elle sonne toujours le glas des défunts de Lalouvesc. Le bourdon _Joséphine_ (1875) sonne pour les grands événements. Sa puissance est telle que l'on prétend qu'il s'entend à plus de 20 km à la ronde. Plus grosse cloche d'Ardèche, elle est due à la générosité de Joséphine de Noblet d'Anglure.

Vidéo réalisée par Matthieu Fraysse présentant la Basilique :
<iframe width="560" height="315" src="https://www.youtube.com/embed/sm3Qi-hJJUA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Le musée Saint Jean-François Régis

Le Musée Saint Régis, situé sur le site du sanctuaire et proche de la basilique, a bénéficié d’un programme de rénovation entièrement financé par des donateurs, amis de La Louvesc, et la Communauté des Jésuites propriétaires des lieux.  
Depuis fin 2018 le diorama présentant la vie de Saint Régis a été complètement refait. Nouveaux éclairages, nettoyages, espace de présentation valorisent le merveilleux travail du peintre Georges Serraz. Les 19 scènes sont autant d'invitation à voyager et vivre les grandes étapes de la vie de Saint Jean-François Régis, Compagnon de Jésus.

Vidéo présentant le diorama :
<iframe src="https://player.vimeo.com/video/346806961?h=13a1cef2ea&title=0&byline=0&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/346806961">Vie de Saint Jean-Fran&ccedil;ois R&eacute;gis - Pr&eacute;sentation</a> from <a href="https://vimeo.com/user46051713">P&eacute;lerinage Saint-R&eacute;gis</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

## La chapelle Saint Jean-François Régis

![](/media/chapelle-st-regis-photo-o-de-framond.jpg "Photo O de Framond")

“Le lieu le plus vénérable de Lalouvesc”. La chapelle se situe à la place de la maison où mourut le Saint.

## Le Parc des pèlerins

![](/media/15-aout-2021-5-photo-ot.jpg)

Le Parc des Pèlerins s’ouvre par son grand escalier de lauzes. La grande allée conduit à un amphithéâtre de gazon planté d’arbres qui accueille notamment la grande messe du 15 août, principale fête religieuse de la paroisse.

Un chemin de croix s’échelonne le long du parc. Les scènes sont gravées sur des menhirs (œuvre de Philippe Kaepplin).

![](/media/parc-des-pelerins-1.jpg)

## La Source

![](/media/fontaine1.jpg)

Il y coule une eau aux vertus que l'on dit miraculeuses.

## Pour en savoir plus

Le [site de la Maison St Régis](https://www.saintregislalouvesc.org/), la [page wikipédia de la Basilique](https://fr.wikipedia.org/wiki/Basilique_Saint-R%C3%A9gis_de_Lalouvesc), [Présentation de Lalouvesc sur l’association des villes sanctuaires](https://lalouvesc.netlify.app/decouvrir-bouger/sanctuaire/%22https://www.villes-sanctuaires.com/villes-sanctuaires/lalouvesc%22)