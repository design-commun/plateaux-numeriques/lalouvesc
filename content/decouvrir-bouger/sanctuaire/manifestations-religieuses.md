+++
date = ""
description = "Messes et célébrations à Lalouvesc"
header = "/media/lalouvesc-15-aout-2020-1.jpg"
icon = "🛐"
subtitle = "Messes et fêtes religieuses"
title = "Manifestations religieuses"
weight = 4

+++
## Messes à Lalouvesc

Voir sur le [site de la Maison St Régis](https://www.saintregislalouvesc.org/infos-pratiques/horaires-des-messes-offices/)

## Fêtes et rassemblements religieux

* 16 juin et dimanche : fête de Saint François Régis,
* 31 juillet : la Saint Ignace,
* 15 août : la fête de l’Assomption,
* dernier dimanche d’août : fête de Sainte Thérèse Couderc,
* 26 septembre : anniversaire de la mort de Sainte Thérèse Couderc,
* 30 octobre : la 25ème heure des Villes Sanctuaires en France,
* 31 décembre : anniversaire de la mort de Saint François Régis.