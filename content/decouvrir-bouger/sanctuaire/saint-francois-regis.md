+++
date = ""
description = "Vie et mort du Père Régis, à l'origine du village"
header = "/media/regis-4.jpg"
icon = "✝️"
subtitle = ""
title = "Saint François Régis"
weight = 2

+++
Saint Régis (ou Saint Jean-François Régis) est né à Foncouverte dans l’Aude le 31 Janvier 1597, d’une famille de nobles ruraux assez aisés et profondément chrétiens. Vers ses 14 ans, il est envoyé pour ses études au Collège des Jésuites de Béziers. Il s’y montre travailleur et bon camarade. Il devient membre de la Congrégation de la Sainte Vierge. Ses études terminées, il entre au Noviciat de la Compagnie de Jésus à Toulouse le 8 décembre 1616. Après ses premiers vœux en 1618, il poursuit la longue formation des Jésuites, coupée par des périodes où il enseigne lui-même. Il ira ainsi à Cahors, à Billom, à Tournon, au Puy et à Auch. Il fait ses études théologiques à Toulouse où il est ordonné prêtre en mai 1631.

### Premières missions

En 1632, il est envoyé à Montpellier comme missionnaire, il y prêche beaucoup et s’occupe des pauvres. Il missionne à Sommières et aux environs. En 1634, il est mis à la disposition de l’évêque de Viviers, Monseigneur de la Baume de Suze, dans la visite de son diocèse. Il va de village en village, préparant la venue de l’évêque. C’est dans les rudes montages des Boutières qu’il démontre ses qualités de missionnaire « vivant à l’Evangélique ». Il attire les populations par sa grande bonté et sa parole simple.

![](/media/regis2.gif)

### Le Puy (1636-1640)

Il est nommé au Puy. Désormais, Régis fera deux parts dans son apostolat. A la belle saison, il travaille au Puy ; pendant l’hiver, il reprend ses missions dans les montagnes, car il sait qu’alors il peut trouver les gens chez eux.

Au Puy, l’activité du Saint s’est spécialement concentrée sur trois points : les catéchistes, les pauvres, les pécheurs.

Les catéchismes ne s’adressent pas seulement aux enfants mais à tous. Ce sont des exposés solides quoique simples de la foi chrétienne. Ils attiraient beaucoup de monde, religieux et laïcs, pauvres et gens aisés, parfois au nombre de 4.000 !

La famine et la guerre civile ont fait affluer les gens au Puy. Régis groupe des dames pieuses pour déceler et secourir les misères cachées. Il procure du travail aux dentellières.

Régis enfin fut un confesseur patient et bienveillant. Pour faciliter leur changement de vie, il fonde une maison d’accueil pour les « filles perdues » , initiative qui lui attire beaucoup de critiques de la part des « biens pensant »  et haines de la jeunesse de l’époque.

### Les hivers dans les montagnes

Pendant quatre hivers, Régis visite de nombreux villages des montagnes du Vivarais et du Velay. Il fait route à pied, loge chez les paysans, prêche dans les églises et dans les chemins. Il est attentif aux besoins spirituels et matériels de tous. On l’appelait partout le « Saint Père ».

### Décembre 1640

Régis termine une mission à Montfaucon où sévit la peste. Il part en bénissant la ville et en annonçant la fin de l’épidémie. Il retourne secrètement au Puy où, pendant trois jours, il fait retraite : « J’ai interrompu mes missions pour me préparer à bien mourir ». Le 23 décembre, il repart. Le temps est mauvais ; le voyage est très dur, à travers neige et congères. A la nuit tombée, il se réfugie dans une grange ouverte à tous les vents et contracte une pleurésie.

Au matin du 24 décembre, il se rend à la petite église de LaLouvesc. Sans plus attendre, il commence la mission. Durant trois jours, il travaille sans relâche, prêche sept fois, confesse deux jours et trois nuits, sans prendre presque de nourriture et de sommeil. Travail harassant pour un malade ! Le mercredi 26 décembre, après sa messe dite à deux heures de l’après-midi, il ne peut regagner son confessionnal, tant la foule est dense. Alors, il s’assoit près de l’autel et se met à confesser, tête nue, sous une fenêtre aux vitres brisées. Soudain dans la soirée, il chancelle et s’affaisse, à la grande stupeur des assistants. On le transporte à la Cure. Là, près du feu, il trouve la force d’entendre encore une vingtaine de confessions, puis s’évanouit. Le curé le fait porter dans son lit et congédie la foule consternée.

### Mort du Père Régis

Pendant 5 jours encore, Régis lutte contre la maladie, trois de ses confrères viennent d’Annonay et de Tournon avec un médecin et un apothicaire. Le dimanche, il reçoit les derniers sacrements.

Le lendemain, 31 décembre peu avant minuit, il dit au frère Bideau qui le veillait « je me trouve mal » et tout de suite après : « Ah mon frère, je vois Notre Seigneur et Notre Dame qui m’ouvrent le paradis ». Puis il commence à dire les paroles du Christ expirant : « Seigneur, je remets mon âme entre tes mains ». Ayant fini, il finit ainsi sa vie, il allait avoir 44 ans.

### Le tombeau et le pèlerinage

La nouvelle de la mort du Père Régis se répandit aussitôt dans les montagnes, et « quoique le temps fut fort rude et les chemins mauvais, Lalouvesc regorgea bientôt de monde ». Le 2 janvier, vingt deux curés se trouvaient à l’enterrement, la foule emporta les objets qui avaient appartenu au Père et même la terre qui venait de recouvrir son cercueil.

De peur qu’on enlevât le corps du Père Régis - les Jésuites voulaient le faire transporter dans leur église du Puy - le cercueil fut placé dans un tronc en châtaignier creusé et cerclé de fer, enterré profond dans la petite église de Lalouvesc qui garda ainsi précieusement son trésor.

A dater de ce jour, les pèlerins ne cesseront de venir au Tombeau du Saint Père, pour y obtenir des miracles de guérison et des conversions. L’Église met le Père Régis au rang des Bienheureux en 1716 et au rang des saints en 1737.

Les reliques mises dans une châsse sont offertes à la vénération des fidèles.

![](/media/cierges-basilique-photo-o-de-framond.jpg "Photo O de Framond")

## Pour en savoir plus

[Histoire du Saint sur le site de la Maison St Régis](https://www.saintregislalouvesc.org/saint-regis/)  
[Page Wikipédia](https://fr.wikipedia.org/wiki/Jean-Fran%C3%A7ois_R%C3%A9gis)