---
title: Lalouvesc, ville sanctuaire
description: ''
icon: "⛪"
weight: 3
date: 
subtitle: ''
header: "/media/basilique-photo-o-de-framond.jpg"
menu:
  main:
    weight: 1
    parent: Découvrir & bouger

---
Lalouvesc est un village sanctuaire depuis la canonisation de Saint François Régis en 1737 et il accueille des pèlerins depuis le XVII siècle. Avec le déplacement du corps de Sainte Thérèse Couderc en 2018, la basilique abrite deux saints. Depuis décembre 2019, Lalouvesc a rejoint le réseau de l'association des villes sanctuaires. Le Sanctuaire de Saint François Régis et l'Office du tourisme du Val d'Ay adhèrent conjointement à l'association Villes Sanctuaires en France.

## Pour en savoir plus

[https://www.saintregislalouvesc.org/](https://www.saintregislalouvesc.org/)  
[Présentation de Lalouvesc sur l'association des villes sanctuaires](https://www.villes-sanctuaires.com/villes-sanctuaires/lalouvesc "https://www.villes-sanctuaires.com/villes-sanctuaires/lalouvesc")