+++
date = ""
description = "Vie de Soeur Thérèse, fondatrice du Cénacle"
header = "/media/ste-therese-couderc-2.jpg"
icon = "✝️"
subtitle = ""
title = "Sainte Thérèse Couderc"
weight = 3

+++
Marie Victoire Thérèse Couderc est née le 1er février 1805, au Mas, hameau de la paroisse de Sablières, dans le diocèse de Viviers.

Très tôt, elle est attirée par la vie religieuse. Lors d’une mission à Sablières, elle est remarquée par le Père Etienne Terme, missionnaire diocésain, qui l’envoie rejoindre à Aps le petit groupe de Sœurs qu’il avait réunies pour s’occuper de l’éducation des petites filles.

En 1827, Thérèse, avec deux autres Sœurs, est envoyée à Lalouvesc pour s’occuper de la “Maison Saint Régis”, fondée par le Père Terme, pour accueillir les femmes venues en pèlerinage. Elle en devint Supérieure et inaugure les journées de recueillement. Le Père Terme les introduit aux “Exercices Spirituels de Saint Ignace” et c’est le développement de l’œuvre des Retraites, la fondation du Cénacle.

![](/media/ste-therese-743091.jpg)

La mort du Père Terme, en 1834, risque de mettre l’œuvre en péril, mais Mère Thérèse la maintient avec l’aide des Jésuites. Peu de temps après, elle est remplacée dans sa mission de Supérieure et ne remplit qu’occasionnellement quelques responsabilités.

Elle vécut une longue vie d’humilité et d’union à Dieu.

Elle meurt à Lyon au Cénacle de Fourvière, le 26 Septembre 1885, alors que la Congrégation commence à entrevoir ce qu’elle doit à sa fondatrice.

Son corps est ramené à Lalouvesc le 29 Septembre 1885.

Le Pape Pie XII la déclare bienheureuse le 4 novembre 1951.

Et le Pape Paul VI proclame sa sainteté le 10 mai 1970.

Son corps qui reposait auparavant dans la chapelle du Cénacle se trouve depuis le 22 septembre 2018 dans une chapelle de la basilique.

La Congrégation de Notre-Dame de la Retraite au Cénacle s’est répandue à travers le monde. Aujourd’hui, l’œuvre des retraites (de durée variable) s’est développée dans les autres maisons du Cénacle ainsi que la catéchèse et toutes formes de formations spirituelles pour vivre sa foi au quotidien.

![](/media/le-cenacle.png)

On peut voir la façade de la première maison du Cénacle au 14 rue de la Fontaine.

## Pour en savoir plus

Le [site de Notre Dame du Cénacle](https://www.ndcenacle.org/), la [page wikipédia sur Thérèse Couderc](https://fr.wikipedia.org/wiki/Th%C3%A9r%C3%A8se_Couderc) et un site tenu par un généalogiste sur la [généalogie de Thérèse Couderc](https://gw.geneanet.org/pjame?iz=913&lang=fr&n=couderc&oc=0&p=therese).