+++
date = ""
description = "Concert, cinéma, permaculture, paroles d'habitants..."
header = "/media/visite-paroles-d-habitants.jpg"
icon = "🎉"
subtitle = "Concert, cinéma, permaculture, paroles d'habitants..."
title = "Autres réjouissances (avril-septembre)"
weight = 6

+++
## Office du tourisme

Vous n'avez pas trouvé suffisamment de réponses à vos questions sur le site de Lalouvesc ? L'Office du tourisme a sans doute les réponses manquantes.

Adresse :

Office de Tourisme du Val d’Ay  
Rue Saint-Régis - 07520 Lalouvesc  
Tél : 04 75 67 84 20

[Site web de l'Office du tourisme](), [Page Facebook](https://www.facebook.com/OTValDAy)

Ouvert 7j/7 en été, du mardi au vendredi de novembre à avril, du mardi au samedi en mai, juin, septembre et octobre. (télétravail les lundis de novembre à avril).

![](/media/office-du-tourisme-pot-d-accueil.jpg "Pot d'accueil à l'Office du tourisme")

L’office de tourisme propose ses services :

* des conseils sur la découverte du patrimoine, les promenades, les randonnées et les activités de loisir et vente de documentation sur la randonnée et le patrimoine,
* la vente de billets d’entrée pour Peaugres et Valrhôna,
* la billetterie des spectacles organisés par les partenaires avec qui l’Office de tourisme a conventionné,
* la location de vélos à assistance électrique sur réservation 48h à l’avance.
* Apéro découverte à l'office de tourisme, bureau de Lalouvesc les mercredis en juillet et août à 17h.
* Ouverture les mardis matins à Satillieu pour les pots d’accueil de l’été et les mercredi matin lors de l’organisation de parcours énigmes en juillet et août également.
* Accès Wifi gratuit 24h/24 à proximité immédiate de l'Office de tourisme
* Visites paroles d'habitants de mai à octobre sur réservation de groupes ou dans le cadre de la programmation estivale.

## Grand concert (mai)

{{<grid>}} {{<bloc>}}

![](/media/vassily-concert-2022.jpg)

{{</bloc>}}{{<bloc>}}

Le mois de mai à Lalouvesc, est le mois d’un grand concert organisé par le Comité des fêtes. Jean-Claude Borrely, Natash St Pier, Les Stentors sont venus chez nous !

La basilique St Régis accueille pour l’occasion quelques 500 spectateurs venus assister à un moment d’exception dans un cadre majestueux. Dans le plus grand respect de ce lieu chargé d’histoire, propice au silence et au respect, ces ténors de la musique subliment ce joyau le temps d’un concert pour un mariage d’exception.

{{</bloc>}}{{</grid>}}

## Cinéma (Juillet - août)

![](/media/cinema-le-foyer.jpg)

En été le Cinéma Le Foyer propose chaque semaine cinq séances avec deux films en alternance, une programmation composée de films « Art et Essai », de films familiaux et de films d’animation.  
Cette programmation de films très récents, voire d’avant-premières, permet de créer un lieu d’animation bien prisé de la population locale et des villages voisins.  
Des confiseries et des glaces sont en vente sur place.

**Ouverture** : Juillet et août, tous les lundis, mercredis, jeudis, samedis et dimanches.

**Adresse** : Cinéma Le Foyer, Abri du pèlerin, 04 75 67 82 63

**Plus d'informations** : [Cinéma Le Foyer](https://cinelefoyer.com/).

## Initiation à la permaculture (avril - août)

L'association Les Jardins du Haut-Vivarais propose une initiation à la permaculture, au Mont Besset.

* Avril, mai, juin : initiation “Faites vos semis comme un pro”. par avec la chef de culture de la Maison Marcon (Saint Bonnet le Froid) et le chef restaurateur à la Terrasse (Lalouvesc). Les plants en cellules, le terreau et les graines sont fournis. Les participants pourront repartir avec leurs semis. 20€ la demi-journée.
* Tous les lundis en juillet et août à 10h30 : visite du jardin d'inspiration permaculturelle du Mont Besset.
* En juillet : conférence programmée avec les créateurs du Bec Helloin, la vitrine permaculturelle européenne (évènement à confirmer).

**Renseignement** : [Office du tourisme](http://www.valday-ardeche.com/)

## Visites Paroles d'habitants

Cette visite organisée par l'Office de tourisme permet de découvrir les monuments du village avec la contribution des habitants : une poésie, un témoignage, une chanson. Lalouvesc est un chaudron d'artistes.

**Renseignement** : [Office du tourisme](http://www.valday-ardeche.com/)

## Et encore...

Selon les années, on pourra assister aussi à du théâtre, à des conférences, à des expositions éphémères, des fêtes, bals, feux d'artifices et bien d'autres choses encore.

Le mieux est de suivre la [page Facebook de l'Office du tourisme](https://www.facebook.com/OTValDAy/) pour ne rien manquer !