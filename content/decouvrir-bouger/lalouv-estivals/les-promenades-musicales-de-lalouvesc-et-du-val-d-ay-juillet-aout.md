+++
date = ""
description = "Une série de concerts de qualité pour tous les publics"
header = "/media/promenades-musivales-lalouvesc-1.jpeg"
icon = "🎻"
subtitle = "Une série de concerts de qualité pour tous les publics"
title = "Les Promenades musicales de Lalouvesc et du Val d’Ay (juillet-août)"
weight = 5

+++
Les concerts ont lieu à la Basilique de Lalouvesc et dans divers lieux des alentours.

![](/media/promenades-musicales-lalouvesc-2.jpeg "Grand concert 2019")

Les concerts privilégient la musique classique et incluent des créations par des compositeurs contemporains (30 créations présentées en 13 ans). Les artistes classiques des Promenades Musicales sont des professionnels reconnus : professeurs de Conservatoires, concertistes et solistes). Des élèves les plus prometteurs de ces professeurs sont également invités. Depuis leur création, les promenades musicales ont reçu en résidence près de 100 musiciens classiques. Un chœur d’environ 25 chanteurs est venu se joindre à l’orchestre classique depuis 2015.

Pour satisfaire un public varié qui fréquente le village, les Promenades Musicales ont également accueilli 45 musiciens de jazz et artistes de variétés.

![](/media/promenages-musicales-lalouvesc-3.jpg "Les voleurs de swing")

Pour toucher le plus grand nombre, **l’association maintient un prix modeste des concerts et la gratuité pour les moins de 12 ans accompagnés**. Un tarif de groupe est proposé aux associations. Les artistes classiques offrent également des concerts dans les maisons de retraite de la région.

Neuf villages du voisinage et vingt lieux différents ont accueilli les Promenades Musicales depuis leur création. La découverte musicale lors des concerts se double donc d’une promenade touristique dans les villages de la région.

Le festival de musique est organisé par l’Association “Les Promenades Musicales de Lalouvesc et du Val d’Ay”, et les bénévoles des villages. L’objectif est de contribuer à la diffusion de la musique vivante en Haute Ardèche et à la promotion touristique de la Communauté de Communes du Val d’Ay.

Depuis la création des PMLL, la publicité, l’organisation des concerts, l’accueil des répétitions et l’hébergement des musiciens sont assurés gracieusement par des bénévoles des villages concernés.

![](/media/promenades-musicales-lalouvesc-4.jpeg "Marie Baraton 2020")

Le programme des concerts est annoncé par affiches et flyers dans les villes et villages de la région, par email à plus de 800 personnes, dans les journaux et magazines régionaux d’information touristique, sur les radios locales et sur le Web.

Depuis 2022, les Promenades musicales ont rejoint l'association Le Carrefour des Arts, mais sa vocation et son activité sont inchangées.

## Pour en savoir plus

Le[ site des Promenades musicales](http://www.promenadesmusicales-lalouvesc.com).