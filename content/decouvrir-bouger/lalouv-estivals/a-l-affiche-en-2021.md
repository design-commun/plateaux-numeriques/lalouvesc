+++
date = ""
description = "Calendrier des manifestations"
draft = true
header = "/media/ardechoise-2.jpg"
icon = "📢"
subtitle = ""
title = "À l’affiche en 2021"
weight = 1

+++
Le calendrier ci-dessous ne signale que les principales manifestations. Pour plus de détails consulter la présentation générale de chaque manifestation accessible par le [menu](/decouvrir-bouger/lalouv-estivals/) ou par l'[Office du tourisme](https://lalouvesc.netlify.app/decouvrir-bouger/lalouv-estivals/autres-rejouissances/).

### Avant juillet

* Avril, mai, juin : Faites vos semis comme un pro, au Mont Besset
* 11 avril : Ronde Louvetonne (annulation préfectorale)
* 18-19 juin : L'Ardéchoise (annulation)

### Tout le long de juillet et août

* 3 juillet - 22 août : **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal tous les jours de 14h30 à 18h30
* Juillet et août : **Exposition _La matière habitée_** à la chapelle Saint Ignace
* Les lundis de juillet et août : **Visite du jardin en permaculture** au Mont Besset
* Les lundis, mercredis, jeudis, samedis et dimanches à 9h de juillet et août : **Cinéma** à l'Abri du Pèlerin
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la **bibliothèque**
* Entre le 14 juillet et le 15 août, les lundis, mardis et jeudis : **activités** au camping ouvertes à tous, adultes matin 9h-10h, enfants 14h-17h (jeudi 13h-16h)
* Du 24 juillet au 6 août l'AFTL propose des stages de **tennis**

##### Concerts, cinéma et événements

* 2 juillet : Le **Temps des émerveilleurs**, balade surprise dans le village
* 3 juillet : exposition, ouverture du **Carrefour des Arts** au Centre d'Animation Culturelle
* 10 et 12 juillet : cinéma, **Poly**, [bande annonce](https://youtu.be/_RWTmSpJoLg)
* 11 juillet : cinéma, **Adieu les cons**, [bande annonce](https://youtu.be/hVV1BpNV6m0)
* 13 juillet : bal et **feu d'artifice**
* 15 et 18 juillet : cinéma, **The Father** (vf), [bande annonce](https://youtu.be/HVCs_ahGpls)
* 17 juillet : cinéma, **Antoinette dans les Cévennes**, [bande annonce](https://youtu.be/qsbBWaCKlW4)
* 18 juillet : **Fête du livre** à la bibliothèque
* 19 juillet : cinéma, **La fine fleur**, [bande annonce](https://youtu.be/Fy2_VLFItHs)
* 20 juillet : conférence, **Balades, contes et patrimoine : histoires paysannes et vie des arbres à l’ère Laudato Si’**, _Paul ROY_ à la Maison St Régis
* 21 juillet : conférence, **Aux sources de la civilisation occidentale : les apports grecs** _Dominique ESTRAGNAT_ à la Maison St Régis
* 21 juillet : concert, **Groupe Kimya**, _Between mist and sky_ à la Grange de Polly
* 21 et 24 juillet : cinéma, **Envole-moi**, [bande annonce](https://youtu.be/rVckCelfruE)
* 23 juillet : conférence, **Aux sources de la civilisation occidentale : les apports romains** _Dominique ESTRAGNAT_ à la Maison St Régis
* 22 et 25 juillet : cinéma, **Nomadland** (vf), Lion d'or à la Mostra de Venise 20201, Golden Globe du meilleur film dramatique et Oscar du meilleur film en 2021. [bande annonce](https://youtu.be/mGjTyjGHnyE)
* 26 juillet : concert, **Guy Angelloz** à la Basilique
* 26 juillet : cinéma, **Profession du père,** avant-première, de Jean-Pierre Ameris avec Benoit Poelevorde, Jules Lefebvre.. [bande annonce](https://youtu.be/mlvzDyqCZWM)
* 28 juillet : conférence, **La matière habitée**, _Marie-Odile LAFOSSE-MARIN_ à la Maison St Régis
* 28 juillet : **Bach et associés** à St Romain d'Ay ([Promenades musicales de Lalouvesc et du Val d'Ay](http://www.promenadesmusicales-lalouvesc.com/)) Annulé
* 28 et 31 juillet : cinéma, **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)
* 29 juillet : conférence, **Art et vérité**, _Marie-Odile LAFOSSE-MARIN_ à la Maison St Régis
* 29 juillet et 1er août : cinéma, **Médecin de nuit**, [bande annonce](https://youtu.be/FandAD1hTbY)
* 2 août : cinéma, **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)
* 3 et 6 août : théâtre, **Confinement, quoi d’autres ?**, Camping
* 4 et 7 août : cinéma, **Le sens de la fête**, [bande annonce](https://youtu.be/-fycN9kNaXk)
* 5 et 8 août : cinéma, **Les Croods 2 : une nouvelle ère**, [bande annonce](https://youtu.be/h43qitmhLxQ)
* 6 août : conférence, **La médecine de Sainte Hildegarde de Bingen** _Dominique ESTRAGNAT_ à la Maison St Régis
* 8 août : **kermesse** au Sanctuaire
* 8 août : concert, **Eugène Electre** à la Grange de Polly, hommage à Geny Detto
* 9 août : cinéma, **Adieu les cons**, [bande annonce](https://youtu.be/9uotsv-vf6I)
* 11 août : concert, **Hommage à Gershwin**, jazz à la Grange de Polly
* 11 et 14 août : cinéma, **Présidents**, [bande annonce](https://youtu.be/XFNicafCq_8)
* 12 et 15 août : cinéma, **Ibrahim**, [bande annonce](https://youtu.be/ohh14axZm-c)
* 13 août : Balade poétique, **Transfiguration**, Miche! Béatix et Hervé Tharel, à la Basilique
* 13 août : concert, **Hommage à Armstrong**, jazz à la Grange de Polly
* 16 août : **Réalisation en public et en musique d’une œuvre** par la peintre Martine Jaquemet ( [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) )
* 16 août : cinéma, **Un triomphe** (avant-première) [bande annonce](https://youtu.be/N_q15dNbWtA)
* 17 août : concert à la Basilique **autour des peintures de Martine Jaquemet** ( [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) )
* 18 et 21 août : cinéma, **Les 2 Alfred**, [bande annonce](https://youtu.be/ZzGhk632cYg)
* 19 et 22 août : cinéma, **OSS 117 : alerte rouge en Afrique noire**, [bande annonce](https://youtu.be/I0aviu-FqNo)
* 21 août :  concert **Méditations musicales** Henri Pourtau (orgue) et Raphaël Yacoub (chantre) à la Basilique
* 23 août : cinéma, **Eiffel**, [bande annonce](https://youtu.be/0TX59C_hzYs)
* 25 et 28 août : cinéma, **C'est la vie**,[ bande annonce](https://youtu.be/udZq8XdXqd4)
* 26 et 29 août : cinéma, **Kaamelott**, [bande annonce](https://youtu.be/j7RrsdP-WuM)
* 30 août : cinéma, **Bonne mère**, [bande annonce](https://youtu.be/fTIpmsWfADs)

### Après août

* 5 septembre : La **Brocante**
* 18-19 septembre : **Journées européennes du patrimoine** en Val d'Ay
* du 25 septembre au 9 octobre : **Fête de la science**
* 30 octobre : Le **Trail des Sapins**