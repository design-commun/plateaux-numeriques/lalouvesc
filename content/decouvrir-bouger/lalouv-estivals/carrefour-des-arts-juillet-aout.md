+++
date = ""
description = "Exposition de peintures et sculptures Lalouvesc"
header = "/media/carrefour-des-arts-lalouvesc-gilbert-frison-3.jpg"
icon = "🖼️"
subtitle = "Plus de 8.000 visiteurs pour une exposition de renommée internationale"
title = "Carrefour des arts (juillet-août)"
weight = 4

+++
« **Je viens faire une randonnée, dans les bois, dans l’air pur et le calme, et j’arrive dans un village où une exposition est annoncée, et là dans ce tout petit village, sont réunies ici tant d’œuvres de qualité, c’est incroyable !** »

![](/media/carrefour-des-arts-ewa-karpinska.jpg "Ewa Karpinska 2020")

Chaque été depuis 1989, Lalouvesc accueille une exposition organisée par le Carrefour des arts, une équipe d’amoureux des arts qui veulent partager leurs découvertes et faire la promotion d’artistes.

L'exposition est ouverte tous les jours de début juillet à fin août, tous les jours de 14h30 à 18h30 (19h le week-end et jours fériés). **L'entrée est libre et gratuite.**

Chaque été, le Centre d’Action Communale perd son austérité, les banderoles pavoisent ses murs, les reproductions des œuvres décorent les balustrades extérieures. Les salles deviennent méconnaissables sous les rideaux, les éclairages.

![](/media/inauguration-carrefour-des-arts-2021-7.jpg "Suhail et Sasha Shaikh 2021")

Chaque été, cinq ou six artistes exposent là. 700 mètres carrés leur sont consacrés dans lesquels le visiteur pourra admirer et acheter des tableaux, des photographies, des sculptures, des œuvres en verre, en papier, en bois, en terre, en pierre…

Le Carrefour des Arts c’est aussi des bénévoles. De nombreux bénévoles sans qui rien ne pourrait se faire. Ils assurent la mise en place de l’exposition, l’accueil de plus de 8.000 visiteurs, la dépose de l’exposition.

![](/media/carrefour-des-arts-lalouvesc-installation-2-photo-ot.jpg)

Mais le Carrefour des Arts ne s’arrête pas l’automne venu. Commence alors le travail de recherche, de sélection des candidats pour que l’exposition suivante offre elle aussi un moment de rêve et de beauté aux prochains visiteurs.

![](/media/inauguration-carrefour-des-arts-2021-12.jpg "Hervé Tharel 2021")

## Pour en savoir plus

consulter le [site du Carrefour des arts](http://www.carrefourdesarts-lalouvesc.fr).