+++
date = ""
description = "Premier rassemblement cyclos l'Europe sur route de montagne"
header = "/media/ardechoise-6.jpg"
icon = "🚴"
subtitle = "Premier rassemblement cyclos l'Europe sur route de montagne"
title = "L'Ardéchoise et ses milliers de cyclistes (mi-juin)"
weight = 3

+++
**L’ARDÉCHOISE est une formidable épreuve sportive !**

Avec une moyenne de 15.000 cyclistes, c’est le premier rassemblement cyclos d’Europe sur route de montagne.

![](/media/ardechoise-7.jpg)

L’Ardéchoise et ses milliers de cyclistes à la mi-juin, tous passeront par Lalouvesc. Tous traverseront le village. Certes parfois à une vitesse acquise par un professionnalisme que rien ne saurait distraire. Mais ils sont si nombreux à participer à cette épreuve pour la découverte de notre région, des villages traversés, que le Comité des Fêtes souhaite les accueillir dans une ambiance festive, décorée et colorée. Ballons et fanions aux traditionnelles couleurs de l’Ardéchoise flottent dans les rues.

![](/media/ardechoise-8.jpg)

Chaque année un thème est choisi. Les bénévoles travaillent à la création de décorations autour de ce thème. Le jaune et violet sont à l’honneur. Les résidents de notre maison de retraite, les élèves de notre école s’impliquent eux aussi dans cette préparation. Le village se pare ainsi de décorations dédiées à ces méritants sportifs. Mais elles feront aussi l’admiration des visiteurs tout l’été. Le square dédié à l’exposition ne compte plus les photographes en recherche de souvenirs de leur passage à Lalouvesc...

![](/media/ardechoise-5.jpg)

L’Ardéchoise c’est aussi le plus gros ravitaillement avant l’arrivée à St Félicien. Les bénévoles sont à pied d’œuvre toute la journée pour préparer, servir, réconforter, insuffler un peu d’énergie à ces courageux sportifs qui viennent de donner leurs dernières forces dans la montée du Col des Grands. Arrivés à Lalouvesc, enfin… parcours oh combien difficiles !, c’est à présent de la descente.

![](/media/ardechoise-6.JPG)

L’Ardéchoise, c’est encore un ravitaillement sur le parcours de l’ARDÈCHE VERTE et L’ARDÉCHOISE DES JEUNES. Le mercredi se côtoient alors, pour une pause bien méritée, professionnels de la petite reine et jeunes cyclistes qui arrivent à Lalouvesc de St Félicien pour redescendre sur Satillieu. Quelle prestation, quelle force et quel courage nous montrent-ils dans ce parcours si sportif alors que certains ne sont encore que de jeunes enfants !

## Pour en savoir plus

* [Site officiel de l'Ardéchoise](https://www.ardechoise.com/)
* [Une vidéo de présentation](https://youtu.be/pVzHSCuLBjc)
* [Page Facebook du comité des fêtes](https://www.facebook.com/comitedesfeteslalouvesc/)