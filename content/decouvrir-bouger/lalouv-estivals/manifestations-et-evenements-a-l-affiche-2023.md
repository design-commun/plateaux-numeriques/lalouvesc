+++
date = 2022-11-07T12:03:00Z
description = "Calendrier des manifestations"
header = "/media/carrefour-des-arts-2022-ecole-5.jpeg"
icon = "📢"
subtitle = ""
title = "Manifestations et événements à l'affiche en 2023"
weight = 1
[menu.direct_access]
name = "Manifestations et événements à l'affiche"
weight = 4

+++
Le calendrier est actualisé au fur et à mesure des annonces. Plus de détails par le lien indiqué ou par l'[Office du tourisme](http://www.valday-ardeche.com/).

Le village vous offre aussi l'opportunité de nombreuses [**activités de loisir**](https://www.lalouvesc.fr/decouvrir-bouger/activites/).

### Avant juillet

* Décembre - janvier : [**illuminations et décorations** ](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/decorations-le-village-sera-en-fete-pour-la-fin-d-annee/)de fin d'année
* Les mardis à 14h : les « [**marcheurs du mardi**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/) »
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* Les jeudis après-midi : [**jeux et rencontres**](https://www.lalouvesc.fr/decouvrir-bouger/activites/autres-activites/#seniors-belote-billard-rencontres-formations) au Club des deux clochers (CAC)
* Les premiers dimanches du mois 12h-15h, [**randos conviviales**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/)
* 15 janvier 17h30 : [**Vœux du Maire**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/voeux-du-maire-discours-et-chiffres/) et galette au CAC
* 29 janvier : passage du [**Rallye Monte-Carlo historique**](https://acm.mc/edition/25e-rallye-montecarlo-historique/evenement/presentation-de-lepreuve/)
* 18 février : [**Concours de Belote et loto**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/concours-de-belote-et-loto-le-18-fevrier/) de l'APEL au CAC
* 25 février : [**Aubade et tartiflette**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lyre-louvetonne-aubade-et-tartiflette/) par la Lyre louvetonne au CAC
* Avril - mai : [**Journées citoyennes**](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/des-journees-citoyennes-pour-le-camping/)
* 16 avril : [**Ronde Louvetonne**](/decouvrir-bouger/lalouv-estivals/la-ronde-louvetonne-des-vieilles-voiture-avril/) (voitures de collection) et **concert de Pascal Veyre** (Pascal chante Johnny)
* 14 mai 15h : Concert [**_a Vuciata, chants et polyphonie corse_**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/chant-et-polyphonies-corses-a-la-basilique/), à la Basilique
* 14 - 17 juin : [**L'Ardéchoise**](/decouvrir-bouger/lalouv-estivals/l-ardechoise-et-ses-milliers-de-cyclistes-juin/) (course cycliste) et décoration du village

### Tout le long de juillet et août

#### En continu

* Début juillet à fin août : **Exposition de peintures et sculptures contemporaines** du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal tous les jours de 14h30 à 18h30
* Début juillet à fin août : **Exposition de sculpture**  à la chapelle Saint Ignace collaboration [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) et Maison St Régis
* Entre le 14 juillet et le 15 août, **activités pour enfants et adultes** au camping ouvertes à tous
* Fin juillet - début août l'AFTL propose des stages de **tennis**
* Fin juillet - début août : **Concerts classiques et jazz**  Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)

#### Par jour

* Les lundis, mercredis, jeudis, samedis et dimanches à 21h de juillet et août : [**Cinéma**](/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt) à l'Abri du Pèlerin
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* Les mercredis à 17h **Apéros découverte** à l'Office du tourisme
* 8 et 9 juillet : Fête du [**centenaire de l'Abri du pèlerin**](http://www.abridupelerin-lalouvesc.fr/historique/) :
  * spectacles, cinéma, fête du livre, jeux, rencontres
* 13 juillet : **Bal** et [**feu d'artifice**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vous-avez-rate-le-feu-d-artifice-ou-vous-voulez-le-revoir/)
* 17, 18, 19, 20, 21, 22, 23 juillet : **_Les Heures Musicales_** en duo ou trio à la chapelle St Ignace Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 19 juillet : **Concert classique** à la Basilique **_Les Guitares_** Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 21 juillet : Théâtre de la veillée, **_Un autobus pour le ciel_**, comédie à l'Abri du Pèlerin
* 23 juillet : **Concert classique** à la Basilique **_Les Oratorios_** Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 26 juillet : **Concert classique** à la chapelle de Notre Dame d'Ay **_Les Sérénades Romantiques_** Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 30 juillet : Concert à la Basilique, **_Exodes, voir l'autre versant du matin_**, création par le chœur d'hommes de la Villette, Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 5 août : **Concert Jazz** à la Grange Polly, **_Boogie-Woogie_**, Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 6 août : **Fête du Sanctuaire**
* 8 août : Théâtre de la veillée, **_Un autobus pour le ciel_**, comédie à l'Abri du Pèlerin
* 12 août : **Concert Jazz** à la Grange Polly, **_Les années folles_**, Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 15 août : Sanctuaire [**fête de l'Assomption**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/14-les-beaux-gestes/#deux-f%C3%AAtes-au-sanctuaire-sous-le-soleil), parc des Pèlerin
* 16 août : **Concert Jazz** à la Grange Polly, **_Hommage à Duke Ellington_**, Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 18 août : Théâtre de la veillée, **_Un autobus pour le ciel_**, comédie à l'Abri du Pèlerin
* 19 août : **Concert Jazz** à la Grange Polly, **_Hommage à Sydney Bechet_**, Promenades musicales du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)

### Après août

* Les mardis à 14h : les « [**marcheurs du mardi**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/) »
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* Les jeudis après-midi : jeux et rencontres au Club des deux clochers (CAC)
* Les premiers dimanches du mois 12h-15h, [**randos conviviales**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/)
* 3 septembre : La [**Brocante**](/decouvrir-bouger/lalouv-estivals/la-brocante-debut-septembre/)
* Mi-septembre : **Journées européennes du patrimoine** en Val d'Ay
* Fin septembre : **Fête de la science**
* 28 octobre : Le [**Trail des Sapins**](/decouvrir-bouger/lalouv-estivals/le-trail-des-sapins-fin-octobre/)  et décorations Halloween du village
* Décembre - janvier : **illuminations et décorations** de fin d'année