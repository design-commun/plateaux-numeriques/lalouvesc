+++
date = ""
description = "1.000 coureurs à pieds pour Halloween..."
header = "/media/trail-lalouvesc-1.jpg"
icon = "🏃‍♂️"
subtitle = "1.000 coureurs à pieds pour Halloween..."
title = "Le Trail des sapins (fin octobre)"
weight = 8

+++
**LE TRAIL DES SAPINS, une épreuve sportive qui s’inscrit dans la cour des grands !**

Sa première particularité est d’être organisé en semi-nocturne. Les départs sont donnés de jour et les chronos tombent en heures nocturnes, parfois tard, très tard dans la nuit noire…

![](/media/trail-lalouvesc-2.jpg)

Chaque nouvelle édition présente des parcours inédits. Des parcours forestiers qui insufflent aux participants un véritable bol d’air pur.

* 46 km
* 21 km
* 12 km
* 7 km

Tandis que les deux plus petits sont ouverts en version randonnée pour permettre aux familles et aux débutants de participer à cette belle aventure, les deux plus grands offrent la possibilité d’empocher des points pour l’UTMB… N’avons-nous pas déjà dit que nous étions dans la cour des grands ?

C’est aussi une course pour les enfants dans les allées du camping. Enfants tous gagnants bien sûr et tous récompensés !

![](/media/trail-lalouvesc-3.jpg)

Le TRAIL DES SAPINS c’est 1.000 participants, 1.000 repas mijotés par les commerçants du village. C’est une année de préparation. C’est l’ investissement le plus important du Comité des fêtes en moyens humains pour son organisation. Tous les bénévoles sont les bienvenus.

C’est un leader, Damien, qui depuis la première édition ne lâche rien. Ce sont des partenaires fidèles qui nous encouragent et s’investissent toujours plus d’année en année. Ce sont aussi des bénévoles toujours plus nombreux, bénévoles du village mais aussi bénévoles des villages environnants qui viennent prêter main forte.

Le TRAIL DES SAPINS est une formidable épreuve organisée sur le village de Lalouvesc et alentours… Une manifestation qui ne dément pas l’adage « **seuls on va plus vite, ensemble on va plus loin** ».

![](/media/trail-lalouvesc-6.jpg)

Il est toujours organisé le dernier samedi du mois d’octobre. L’aire de départ/arrivée se pare ainsi de décorations aux couleurs d’ HALLOWEEN. Qui des bénévoles, des sorciers et sorcières, des fantômes et autres monstres croyez-vous croiser ? Ambiance, ambiance…

![](/media/trail-lalouvesc-5.jpg)

C’est aussi une façon de dynamiser le village hors période estivale.

## Pour en savoir plus

Le TRAIL DES SAPINS c’est un site dédié : [https://www.traildessapins-lalouvesc.fr/](https://www.traildessapins-lalouvesc.fr/ "https://www.traildessapins-lalouvesc.fr/")

C’est aussi une page Facebook : [https://www.facebook.com/traildessapins](https://www.facebook.com/traildessapins "https://www.facebook.com/traildessapins")