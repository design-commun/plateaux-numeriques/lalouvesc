+++
date = 2021-11-01T23:00:00Z
description = ""
draft = true
header = "/media/ardechoise-2.jpg"
icon = "📢"
subtitle = "Calendrier des manifestations"
title = "Manifestations et événements à l'affiche en 2022"
weight = 1

+++
Le calendrier ci-dessous ne signale que les principales manifestations de 2022. Il est actualisé régulièrement. Pour plus de détails consulter la présentation générale de chaque manifestation accessible par le lien indiqué ou par l'[Office du tourisme](https://lalouvesc.netlify.app/decouvrir-bouger/lalouv-estivals/autres-rejouissances/).

### Avant juillet

* Décembre - janvier : [**illuminations et décorations** ](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/decorations-le-village-sera-en-fete-pour-la-fin-d-annee/)de fin d'année
* 30 Janvier : [**Rallye Monte-Carlo historique**](https://acm.mc/edition/rallye-monte-carlo-historique-2022/)
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* 22 avril : Présentation des résultats du concours sur le [**Jeu-monument**](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/un-jeu-monument-sur-le-parc-du-val-d-or/)
* Avril - mai : [**Journées citoyennes**](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/des-journees-citoyennes-pour-le-camping/)
* Avril, mai et juin : Faites vos **semis** comme un pro, au Mont Besset
* 3 avril : [**Ronde Louvetonne**](/decouvrir-bouger/lalouv-estivals/la-ronde-louvetonne-des-vieilles-voiture-avril/) (voitures de collection)
* 22 Mai : Grand [**concert**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#grand-concert-mai)**, Amaury Vassili**
* 23 Mai - 7 Novembre : **Exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 15 - 18 juin : [**L'Ardéchoise**](/decouvrir-bouger/lalouv-estivals/l-ardechoise-et-ses-milliers-de-cyclistes-juin/) (course cycliste)
* 18 Juin : [**Feux**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/12-une-page-est-tournee/#un-comit%C3%A9-toujours-%C3%A0-la-f%C3%AAte) de la St Jean

### Tout le long de juillet et août

#### En continu

* 23 Mai - 7 Novembre : **Exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* Du 2 juillet au 28 août : **Exposition de peintures et sculptures contemporaines** du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal tous les jours de 14h30 à 18h30
* Du 9 juillet au 13 août : **Exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* Entre le 14 juillet et le 15 août, **activités pour enfants et adultes** au camping ouvertes à tous
* Fin juillet - début août l'AFTL propose des stages de **tennis**

#### Chaque semaine

* Les lundis de juillet et août : **Visite du** [**jardin en permaculture**](https://www.lalouvesc.fr/projets-avenir/changement-climatique/vers-un-village-resilient/) au Mont Besset
* Les lundis, mercredis, jeudis, samedis et dimanches à 21h de juillet et août : [**Cinéma**](/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt) à l'Abri du Pèlerin
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* Les mercredis à 17h **Apéros découverte** à l'Office du tourisme

#### Jour par jour

* 3 juillet 21h : **Cinéma** _Qu’est ce qu’on a tous fait au bon dieu ?_ ( [bande annonce](https://youtu.be/UkXUfWFiiao) ) à l'Abri du Pèlerin
* 4 juillet 21h : **Cinéma** _Illusions perdues_ ( [bande annonce](https://youtu.be/KGQn_mWDWrA) ) à l'Abri du Pèlerin
* 9 juillet 21h : **Cinéma** _Maison de retraite_ ( [bande annonce](https://youtu.be/bLFIhJUpmNk) ) à l'Abri du Pèlerin
* 10 juillet 20h30 : **Concert Let's Goldman**, Parc des Pèlerins
* 10 juillet 21h : **Cinéma** _Mystère_ ( [bande annonce](https://youtu.be/nmfMMrijOzU) ) à l'Abri du Pèlerin
* 11 juillet 21h : **Cinéma** _La nuit du 12_ ( [bande annonce](https://youtu.be/nC6AbWkkULc) ) · AVANT-PREMIÈRE à l'Abri du Pèlerin
* 12 juillet 20h30 : **Théâtre** _Politiquement correct_ par la Compagnie théâtrale Arcorcour à l'Abri du Pèlerin
* 13 Juillet à partir de 19h : **Bal et feu d'artifice**
* 16 juillet 21h : **Cinéma** _En corps_ ( [bande annonce](https://youtu.be/WMqIkiI6fAA) ) à l'Abri du Pèlerin
* 17 juillet 21h : **Cinéma** _Le Temps des Secrets_ ( [bande annonce](https://youtu.be/RFpGLeipEBA) ) à l'Abri du Pèlerin
* 18 juillet 21h : **Cinéma** _En corps_ ( [bande annonce](https://youtu.be/WMqIkiI6fAA) ) à l'Abri du Pèlerin
* 20 juillet 21h : **Cinéma** _Notre Dame brûle_ ( [bande annonce](https://youtu.be/YlDXdPSEtgk) ) à l'Abri du Pèlerin
* 21 juillet 21h : **Cinéma** _C’est Magnifique_ ( [bande annonce](https://youtu.be/z-MlOwkqhDY) ) à l'Abri du Pèlerin
* 23 juillet 21h : **Cinéma** _Notre Dame brûle_ ( [bande annonce](https://youtu.be/YlDXdPSEtgk) ) à l'Abri du Pèlerin
* 24 juillet 21h : **Cinéma** _C’est Magnifique_ ( [bande annonce](https://youtu.be/z-MlOwkqhDY) ) à l'Abri du Pèlerin
* 25 juillet 21h : **Cinéma** _Elvis_ ( [bande annonce](https://youtu.be/S2nQInzyqvM) ) · Expo et rencontre avec un spécialiste à l'Abri du Pèlerin
* 27 juillet 21h : **Cinéma** _Irréductible_ ( [bande annonce](https://youtu.be/-xXDfZBHV4E) ) à l'Abri du Pèlerin
* 27 juillet 20h30 : **Concert** flûtes virtuose, Guy Angelloz à la Basilique
* 28 juillet 17h et 21h : **Cinéma** _Là où le temps s’est arrêté_ ( [bande annonce](https://youtu.be/zUH1K3JIGzI) ) en présence de Christophe Tardy, réalisateur du film à l'Abri du Pèlerin
* 29 juillet 20h30 : **Théâtre** _Donc y jette ou Y'a Nicole au hublot_ par Chrysalide à l'Abri du Pèlerin
* 30 juillet 21h : **Cinéma** _Irréductible_ ( [bande annonce](https://youtu.be/-xXDfZBHV4E) ) à l'Abri du Pèlerin
* 31 juillet 21h : **Cinéma** _Les Minions 2 : Il était une fois Gru_ ( [bande annonce](https://youtu.be/Cm6wpegvRpA) ) à l'Abri du Pèlerin
* 1er août 21h : **Cinéma** _À l’ombre des filles_ ( [bande annonce](https://youtu.be/0qezBv7i-bc) ) à l'Abri du Pèlerin
* 2 août 20h30 : **Concert** _La voix et la musique_  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr), musique classique à la basilique
* 3 août 20h30 : **Concert** _Bach et associés_  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr), musique classique à la chapelle de Notre Dame d'Ay
* 3 août 21h : **Cinéma** _L'école du bout du monde_ ( [bande annonce](https://youtu.be/hca-7AKq07w) ) à l'Abri du Pèlerin
* 4 août 20h30 : **Concert** _Souvenir de Florence_  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr), musique classique à la basilique
* 4 août 21h : **Cinéma** _Top Gun_ ( [bande annonce](https://youtu.be/V4gQdk1nAn0) ) à l'Abri du Pèlerin
* 6 août 21h : **Cinéma** _L'école du bout du monde_ ( [bande annonce](https://youtu.be/hca-7AKq07w) ) à l'Abri du Pèlerin
* 7 août : **Fête du Sanctuaire**
* 7 août 21h : **Cinéma** _Top Gun_ ( [bande annonce](https://youtu.be/V4gQdk1nAn0) ) à l'Abri du Pèlerin
* 8 août 21h : **Cinéma** _L'innocent_ ( [bande annonce](https://youtu.be/6wl4B5lJgBM) ), avant-première à l'Abri du Pèlerin
* 9 août 20h30 : **Théâtre**  _Sous les ponts de Paris_ par Les Bretelles à Bascule à l'Abri du Pèlerin
* 10 août : **Cinéma** à l'Abri du Pèlerin
  * 17h30 : _De folies en folies_, documentaire, Expo photos et dégustation de produits locaux entre les séances
  * 21h : _Les folies fermières_ ( [bande annonce](https://youtu.be/KD5sB_BPLZI) )
* 11 août 21h : **Cinéma** _L'affaire Collini_ ( [bande annonce](https://youtu.be/4buWqDrWMBo) ), à l'Abri du Pèlerin
* 12 août 19h30 : **Concert _Fats Waller & His Rhythm_**  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr),, jazz à la Grange de Polly
* 13 août 19h30 : **Concert _Jazz Manouche_**  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr),, jazz à la Grange de Polly
* 13 août 21h : **Cinéma** _Les folies fermières_ ( [bande annonce](https://youtu.be/KD5sB_BPLZI) ), à l'Abri du Pèlerin
* 14 août 21h : **Cinéma** _L'affaire Collini_ ( [bande annonce](https://youtu.be/4buWqDrWMBo) ), à l'Abri du Pèlerin
* 15 août : Sanctuaire [**fête de l'Assomption**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/14-les-beaux-gestes/#deux-f%C3%AAtes-au-sanctuaire-sous-le-soleil)
* 15 août 21h : **Cinéma** _Buzz l'éclair_ [(bande annonce](https://youtu.be/q41VoF95fmI) ), à l'Abri du Pèlerin
* 15 août 21h : **Concert d'orgue** d'Henri Pourtau, basilique
* 17 août 21h : **Cinéma** _Ténor_ ( [bande annonce](https://youtu.be/-rhY0fonYwM) [)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/programme-du-cinema-d-aout/), à l'Abri du Pèlerin
* 18 août 21h : **Cinéma** _Jurassic World : le monde d'après_ ( [bande annonce](https://youtu.be/4R87S4YCtI8) ), à l'Abri du Pèlerin
* 20 août 21h : **Cinéma** _Ténor_ ( [bande annonce](https://youtu.be/-rhY0fonYwM) [)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/programme-du-cinema-d-aout/), à l'Abri du Pèlerin
* 21 août 21h : **Cinéma** _Jurassic World : le monde d'après_ ( [bande annonce](https://youtu.be/4R87S4YCtI8) ), à l'Abri du Pèlerin
* 22 août 21h : **Cinéma** _Les volets verts_ ( [bande annonce](https://youtu.be/zLbz061gLzw) ), avant-première à l'Abri du Pèlerin
* 23 août 20h30 : **Théâtre** _Building_ par le Théâtre d'en face à l'Abri du Pèlerin
* 24 août 21h : **Cinéma** _Joyeuse retraite 2_ ( [bande annonce](https://youtu.be/TgNiqEKEydw) ), à l'Abri du Pèlerin
* 25 août 21h : **Cinéma** _La petite bande_ ( [bande annonce](https://youtu.be/npLm1oPOY14) ), à l'Abri du Pèlerin
* 27 août 21h : **Cinéma** _Joyeuse retraite 2_ ( [bande annonce](https://youtu.be/TgNiqEKEydw) ), à l'Abri du Pèlerin
* 28 août 21h : **Cinéma** _La petite bande_ ( [bande annonce](https://youtu.be/npLm1oPOY14) ), à l'Abri du Pèlerin
* 29 août 21h : **Cinéma** _Revoir Paris_ ( [bande annonce](https://youtu.be/idHIUQeF_cw) ), avant première à l'Abri du Pèlerin

  ***

### Après août

* 23 Mai - 7 Novembre : **Exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* 4 septembre : La [**Brocante**](/decouvrir-bouger/lalouv-estivals/la-brocante-debut-septembre/)
* Mi-septembre : **Journées européennes du patrimoine** en Val d'Ay
* 18 septembre : **Fête du livre,** rencontre avec des auteurs à la bibliothèque
* 24 septembre : **Fête de la science**
* 29 octobre : Le [**Trail des Sapins**](/decouvrir-bouger/lalouv-estivals/le-trail-des-sapins-fin-octobre/)
* Décembre - janvier : **illuminations et décorations** de fin d'année