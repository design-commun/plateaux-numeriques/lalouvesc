---
title: Lalouv’estivales
description: festival, exposition, concert,
icon: "☀"
weight: 4
date: 
subtitle: ''
header: "/media/arc-en-ciel-2.jpg"
menu:
  main:
    weight: 2
    parent: Découvrir & bouger

---
Chaque année du printemps à l'automne, et maintenant même en hiver, les Lalouv'estivales présentent un programme nourri de manifestations et d'animations de grandes qualités. Depuis les concentrations de vieilles voitures, les courses cyclistes, jusqu'au Trail en passant par des concerts, des expositions d'arts, du cinéma, des conférences ou de l'initiation à la permaculture. Il y en a pour tous les goûts. Leur dénominateur commun : une exigence de qualité associée à un accueil chaleureux, une accessibilité à tout public et la pleine conscience des défis d'aujourd'hui.