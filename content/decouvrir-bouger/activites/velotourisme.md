+++
date = ""
description = "Vélo, vélo électrique, cyclotourisme"
header = "/media/velo-a-lalouvesc.JPG"
icon = "🚲"
subtitle = "Vélo électricque, VTT, vélo sportif, deux roues pour tous les goûts"
title = "Vélotourisme"
weight = 2

+++
## Sur les routes ardéchoises

Mordus de vélo, vous connaissez certainement la course de l’Ardéchoise.

L’itinéraire "Sur les routes ardéchoises" vous préparera aux redoutables cols de montagne des parcours. Le long des vallées de la Cance et de l’Ay, c’est depuis Saint-Félicien, berceau historique de l’Ardéchoise que vous pourrez parcourir le tracé de 160 km (2506 m de dénivelés cumulés) à retrouver sur [www.monardechoise.com](http://www.monardechoise.com) ou [www.ardechesports.fr](http://www.ardechesports.fr) (L’Ardéchoise à ST FÉLICIEN - 04 75 06 13 43).

## Traversée de l'Ardèche en VTT

Pour les férus de VTT, un itinéraire de 250 km permet de découvrir l’Ardèche du nord au sud et ses magnifiques paysages !  
Cartoguide disponible à l’[Office de tourisme](www.valday-ardeche.com).

Voir en ligne [La grande traversée de l'Ardèche](https://www.ardeche-guide.com/grande-traversee-de-l-ardeche-vtt)

## Depuis la vallée du Rhône, utilisez le porte-vélo

Depuis Tournon sur Rhône accrochez votre vélo sur [le bus (ligne 11)](https://www.auvergnerhonealpes.fr/286-ardeche.htm) pour monter à Lalouvesc et profitez des itinéraires à profil descendant pour rejoindre la Vallée du Rhône. Vous pourrez aussi embarquer sur l’un des [bateaux des Canotiers LALOUVESC](https://www.ardeche-hermitage.com/fr/actualites/une-croisiere-sur-le-rhone-au-depart-de-tournon/) en juillet et août.

## Location de vélos assistance-électrique

![](/media/lalouvesc-velos.jpg)

Les pentes ardéchoises vous empêchent d’envisager de sortir le vélo ? C’est que vous n’avez pas testé le vélo à assistance électrique. Dorénavant, fini crampes, transpiration et autres désagréments dûs à l’effort. Seul le plaisir d’une balade au grand air subsiste !

Louer et réserver un vélo à assistance électrique à l['Office du tourisme](www.valday-ardeche.com) (petits prix).

## Label Accueil vélo

Depuis qu’il est labellisé Accueil Vélo, l’[Office de tourisme du Val d’Ay](www.valday-ardeche.com) peut accueillir toutes les clientèles venant de « Sur les Routes de l’Ardéchoise», de la « Grande Traversée de l’Ardèche à VTT » (avec la variante de Saint Félicien), mais aussi depuis la Via Rhôna à Sarras, la Dolce Via à St Agrève ou Lamastre…