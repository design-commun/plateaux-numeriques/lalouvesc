+++
date = ""
description = "Cours d'équitation tous niveaux"
header = "/media/centre-equestre-fontcouverte-2.JPG"
icon = "🐎"
subtitle = "Cours d'équitation tous niveaux"
title = "Centre équestre"
weight = 3

+++
**Venez découvrir l'équitation dans une ambiance familiale et un cadre exceptionnel, en pleine nature.**

![](/media/centre-equestre-fontcouverte.jpg)

Le centre équestre DOMAINE FONTCOUVERTE se situe au-dessus du village de Lalouvesc, en bordure boisée du Mont Chaix.

Nous vous proposons des cours pour tous niveaux, de l'initiation à la compétition, avec un apprentissage progressif adapté au rythme de chacun. Vous pouvez également venir pour une balade, une randonnée ou bien découvrir d'autres activités autour du cheval (voltige, équifeel, pony-games...).

Nous accueillons aussi les groupes (écoles, anniversaires...) pour des activités ludiques. Nous organisons aussi des week-ends équestres dans notre gîte. Des pensions pour vos chevaux et des demi-pensions sur nos équidés sont possibles.

Enfin, le bien-être de nos équidés constitue une de nos priorités avec notamment un équipement adapté à la morphologie de chaque cheval et notre association, EQUIFONTCOUVERTE, pour nos chevaux réformés.

N'hésitez pas à venir nous rencontrer !

Téléphone : 04 75 34 99 40 ou 07 60 74 88 03

#### Labels de qualité

La Fédération Française d'Équitation propose aux établissements équestres de s'engager dans une démarche qualité. Dans ce cadre, l'établissement a été audité en 2022 et obtenu les labels suivants :

* Cheval Étape hébergement intérieur
* Cheval Étape hébergement extérieur
* Label d'activité Cheval Club de France
* Label d'activité Poney Club de France
* Bien-être animal

Cette labellisation atteste de la qualité globale des prestations proposées par l'établissement « DOMAINE FONTCOUVERTE ».

**Bravo à toute l'équipe !**