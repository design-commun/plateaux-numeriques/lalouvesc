+++
date = ""
description = "Football, Lyre louvetonne, tennis, théâtre, centre de loisirs......"
header = "/media/lyre-louvetonne-17-juillet-2020.jpg"
icon = "🗓️"
subtitle = "Football, Lyre louvetonne, tennis, théâtre, centre de loisirs..."
title = "Autres activités"
weight = 6

+++
## Lyre louvetonne

La Lyre louvetonne est une des plus anciennes associations. Sa déclaration en préfecture date de 1950, mais son activité sous des noms divers remonte aux années 1920.

Son répertoire musical évolue constamment. De marches pour fanfare ou Marching band,jusqu'aux musiques de film, musiques populaires, paso doble, musiques de défilé, musiques baroques, musiques de cérémonies…

Cérémonies officielles : 11 novembre, 8 mai, 14 juillet… Prestations en extérieures possibles.

Représentations en interieur lors de journées musicales diverses, Festivals, Sainte Cécile, Concert d’été en la basilique de Lalouvesc.

![](/media/lyre-louvetonne-17-juillet-2020.jpg)

Que vous soyez musicien débutant ou professionnel, n’hésitez pas ! Venez nous rejoindre tous les vendredi soir pour répéter avec nous et rigoler un bon coup ! Les répétitions ont lieu au centre d’animation communal à Lalouvesc de 20h00 à 21h30 du mois d’octobre jusqu’au concerts estivaux.

[Ecouter un concert de la Lyre (vidéo)](https://www.youtube.com/watch?reload=9&v=vOa5rPfZqhA&feature=youtu.be)

## Football

![](/media/foot-lalouvesc-pailhares-2020.jpg)

Le club de Lalouvesc a réalisé une entente avec Pailharès, il y a maintenant plusieurs années afin de remonter les effectifs. La saison commence mi-septembre et se termine mi-mai. Il y a un match chaque dimanche. A cela s’ajoutent les entraînements, le vendredi soir. Les matchs ont lieu sur le terrain de Lalouvesc, les entraînements sur le terrain de Pailharès plus petit. L’équipe est constituée de 17 joueurs qui ont de 17 ans à 50 ans. Il y a une très bonne ambiance au sein de l’équipe. Régulièrement ils se retrouvent ensemble pour fêter victoire comme défaite devant une pizza ou autre gâterie.  
Le club de foot organise chaque année un méchoui et un concours de boules le premier week-end de juillet sur la commune de Pailharès. Et il s’occupe aussi de la buvette et du bal, le 13 juillet à Lalouvesc.  
Si vous êtes motivés, n’hésitez pas à venir chausser les crampons pour des journées sportives et amicales avec un brin de compétition quand même !!

[Calendrier des compétitions]()

## Tennis

Il existe un cours de tennis communal. Se renseigner à l'Office du tourisme.  
Par ailleurs, l'Association familiale des tennis de Lalouvesc (AFTL) dispose de ses propres cours privés et organise régulièrement des formations.

Contact : 06 32 51 62 24

## Théâtre

La troupe amateur "Théâtre de la veillée" se réunit régulièrement pour proposer à l'été des représentations théâtrales.

Contact : 04 75 32 25 28

## Seniors (belote, billard, rencontres, formations...)

Le Club des deux clochers vous propose de participer à ses activités.

* Des séances de **gymnastique** débuteront le 24 février de 11h à 12h
* Des initiations à l'**informatique** animées par un professionnel
* Tout au long de l'année, vous pourrez vous initier aux techniques des **arts créatifs**
* Des **ateliers de mémoire** sont organisés hebdomadairement afin de faire travailler les neurones endormis

Pour ces activités, contacter le Président Georges IVANEZ 06 62 00 45 69, georges.ivanez@orange.fr

![](/media/gateau-club-2-clochers-2022.jpg)

Tout au long de l'année, le Club réunit ses adhérents le jeudi après-midi, où chacun peut se divertir en oubliant l'espace d'un moment ses tracas de la vie quotidienne.

* jeux de cartes, de société, ... et même bavardage,
* billard,
* Le goûter interrompt les parties, même les plus acharnées.

Quelques jours festifs ponctuent la vie du Club :

{{<grid>}}{{<bloc>}}

![](/media/mardi-gras-club-des-2-clochers-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

* la galette des rois,
* le mardi-gras,
* l'échange avec le Club de Rochepaule nous rapproche par dessus le Doux,
* un voyage annuel en fin de printemps nous fait découvrir d'autres horizons, d'autres cultures, d'autres cuisines... ,
* dans la grisaille de l'automne, nous fêtons l'arrivée du beaujolais nouveau,
* et la bûche de noël vient clore l'année civile en prémices aux agapes de la trêve des confiseurs.

{{</bloc>}}{{</grid>}}

Le Club participe et anime la vie du village

* organisation d'un concours de belote,
* exposition estivale,
* participation au Téléthon.

**Vous qui avez une image vieillotte de notre association de retraités, testez l'ambiance du Club en venant prendre le goûter avec nous un jeudi au Centre d'Animation Communal !**

## Séjours, accueil et camps pour enfants et ados

La communauté de communes du Val d'Ay organise régulièrement des activités dans son local de Jaloine à St Romain d'Ay.

![](/media/centre-de-loisir-jaloine-2921.jpg)

[Centre de loisirs « La cabane des marmots »](http://www.famillesrurales07.org/valday.html)

380 Espace de Jaloine,  
07290 St Romain d'Ay  
07 66 49 07 90  
[cdl.jaloine@gmail.com](mailto:centredeloisirs.roiffieux@gmail.com)