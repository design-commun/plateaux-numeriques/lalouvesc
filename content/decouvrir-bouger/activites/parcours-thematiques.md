+++
date = ""
description = "balades, promenades, randonnées, chemins, sentiers, parcours"
draft = true
header = "/media/sentier-botanique.JPG"
icon = "🌲"
subtitle = "Mille et un sentiers"
title = "Parcours et randonnées"
weight = 1

+++
## Topoguides

Une collection de [topoguides et cartes](http://www.valday-ardeche.com/a-la-rencontre-de-la-nature/randonnee-pedestre/) vous est proposée à l’Office de tourisme pour vous accompagner lors de vos promenades. Plus de 22 balades, randonnées et sentiers thématiques de toutes tailles et difficultés sont présentés.

La Maison de la randonnée, située à côté de l'Office du tourisme, est un espace convivial où l’ensemble du réseau de sentiers balisés du Val d’Ay est présenté. Plus de 230 km vous attendent !

## Quelques exemples

### Pour une balade en famille

![](/media/sentier-des-afars.JPG)

#### Le sentier de la légende des afars et du chapelet infini

Vous irez sur trois km aller et retour d’énigmes en géocaches, de colliers de perles pédagogiques en lutins facétieux rappelant l’histoire de Lalouvesc, celle des scieries et des moulins, et aussi de la faune et de la flore locales …

#### Parcours botanique

Balisé par des panneaux en bois en forme de feuilles, il vous fera découvrir le long d'un joli sentier les espèces végétales locales.

### Pour les randonneurs

#### Chemin de St Régis

![](/media/randonneurs.JPG)

[Chemin de St Régis - GR 430](https://www.gr-infos.com/gr430.htm) : du Puy en Velay à Lalouvesc, une boucle de 200 km retrace le parcours de St Régis. L’occasion de découvrir des paysages variés entre Haute-Loire et Ardèche.

#### Chemin de Ste Thérèse Couderc

![](/media/chemin-ste-therese-2021.JPG)

Chemin de Ste Thérèse Couderc : de Lalouvesc, où cette Ardéchoise fonda la congrégation de Notre Dame du Cénacle jusqu’à sa maison natale à Sablières avec prolongement jusqu’à l’Abbaye de Notre Dame des Neiges. 180 km en huit étapes ardéchoises.

## Pour en trouver plus

Voir sur l'Office du tourisme :

* [Randonnées pédestres](http://www.valday-ardeche.com/a-la-rencontre-de-la-nature/randonnee-pedestre/)
* [Randonnées VTT](http://www.valday-ardeche.com/a-la-rencontre-de-la-nature/circuits-vtt/)