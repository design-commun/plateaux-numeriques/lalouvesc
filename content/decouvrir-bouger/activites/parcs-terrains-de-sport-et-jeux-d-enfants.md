+++
date = ""
description = "Pour s'amuser, se retrouver, se reposer, méditer..."
header = "/media/plan-parcs.jpg"
icon = "🏞️"
subtitle = ""
title = "Parcs, terrains de sport et jeux d'enfants"
weight = 4

+++
![](/media/plan-parcs.jpg)

## 1. Parc du Val d'Or

![](/media/espace_agrement.jpg)

Le Parc du Val d'Or est un vaste espace situé en dessous du bâtiment de la Mairie. Il comprend une vaste aire de jeux pour les enfants, un terrain de pétanque pour les parents, un parcours sportif, des aires de pique-nique pour toute la famille. C'est un joli lieu de détente verdoyant avec vue sur les Cévennes.

La municipalité projette d'y construire un jeu-monument.

## 2. Parc de Grand Lieu

**Attention en cours de réfection en 2022.**

Au Parc de Grand Lieu ou Parc de la SAMOV, il y a un petit étang où un lâcher de truites farios est régulièrement réalisé.

![](/media/peche-samov-1.jpg)

Les petits ou grands doivent être munis d’un permis de pêche  (6 euros pour une année complète pour les moins de 12 ans) qui leur permet de pêcher sur tous les cours d’eau autorisés du département de l’Ardèche.

Pour se le procurer direction : [https://www.cartedepeche.fr/](https://www.cartedepeche.fr/ "https://www.cartedepeche.fr/") , il faut sélectionner son permis et bien choisir dans la liste, l’association _les amis de la ligne_ sur la commune de Satillieu.

La pêche des truites est réglementée, la maille est de 23 cm. En dessous la truite doit être obligatoirement relâchée.

## 3. Parc des Pèlerins

Le Parc des pèlerins est un théâtre de verdure propice à la méditation qui accueille notamment les grandes manifestations religieuses de plein air, comme la messe du 15 août.

![](/media/parc-des-pelerins-2.jpg)

On peut aussi y admirer un chemin de croix, taillé dans le granit, réalisé par Philippe Kaeppelin (1918-2012).

Les initiés savent que c'est un coin propice aux champignons.

## 4. Jeux du camping

![](/media/camping-jeux-1.JPG)

Le camping de du Pré du Moulin comprend un terrain de jeux arborés pour enfants (tyrolienne, balançoires, ping-pong, ...).

En 2021, les Louvetous ont rénové le mini-golf de 18 trous et préparé un terrain multi-sport au cours de journées citoyennes.

## 5. Terrain de foot

Un terrain de football entretenu est accessible à l'entrée du village en venant de St Bonnet le Froid.

Il accueille notamment le dimanche les matchs de l'équipe de foot de Lalouvesc-Paillarès.