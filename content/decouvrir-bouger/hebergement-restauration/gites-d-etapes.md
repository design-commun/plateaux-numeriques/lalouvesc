+++
date = ""
description = ""
header = "/media/chemin-ste-therese-2021.JPG"
icon = "🧙"
subtitle = ""
title = "Gîtes d'étapes"
weight = 3

+++
## Gîte d'étapes communal

Dans une maison en pierre à l'écart du village : au rez-de-chaussée, cuisine et sanitaire - Au 1er étage, dortoir collectif. 14 places disponibles. Tarif 12 € par nuit (+ taxe de séjour).

{{<grid>}}{{<bloc>}}

![](/media/gite-communal-2022.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/gite-d-etapes-lalouvesc-2.jpg)

Uniquement sur réservation.

**Réservations** : Tél : 04 75 67 84 86 (laisser un message en cas d'absence) ou email : [camping.lalouvesc@orange.fr](mailto:camping.lalouvesc@orange.fr)

{{</bloc>}}{{</grid>}}

## Gîte d'étapes l'Abri du Pèlerin

![](/media/abri-du-pelerin.jpg)

Animé par des bénévoles sous la responsabilité de la Maison St Régis, l’Abri cultive un esprit familial. Accueil de groupes dans un bâtiment à 3 niveaux. Gestion libre. Coin cuisine à disposition dans une 2ème salle en annexe.

{{<grid>}}{{<bloc>}}

![](/media/abri-du-pelerin-2021-2.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/abri-du-pelerin-2021-1.JPG)

{{</bloc>}}{{</grid>}}

[Réserver en ligne](http://www.abridupelerin-lalouvesc.fr/index.php/reservations/) une chambre (12 ou 16 € par nuit) ou un lit dans le dortoir (9 € dans le dortoir) du 1er mai au 31 octobre.

## Refuges des Afars

Aux abords du camping municipal, refuges pittoresques en bois, construits sur pilotis, cachés dans les arbres. On y accède par une passerelle ou par une échelle. Chaque refuge est conçu pour deux personnes et dispose de sa terrasse. Certains sont proches et peuvent partager leurs terrasses. Charme, dépaysement.

Tarif : 75 € par nuit pour deux personnes, petits déjeuners compris.

Réservation sur le [site du camping](/decouvrir-bouger/hebergement-restauration/camping/). Ouverts du 1er mai au 31 octobre.

{{<grid>}}{{<bloc>}}

![](https://www.lalouvesc.fr/media/refuge-des-afars-15.jpg)

{{</bloc>}}{{<bloc>}}

![](https://www.lalouvesc.fr/media/refuge-des-afars-7.jpg)

Attention, les refuges ne sont pas chauffés et, à 1.000 m d’altitude, les nuits peuvent être fraîches, même en été. Prévoir l’équipement.

{{</bloc>}}{{</grid>}}