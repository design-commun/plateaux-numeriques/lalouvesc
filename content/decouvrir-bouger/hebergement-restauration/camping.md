+++
date = ""
description = "Un camping familial offrant une vaste gamme d’hébergements et profitant du dynamisme estival du village."
header = "/media/camping-2022-1.JPG"
icon = "🏕️"
subtitle = "Un camping familial offrant une vaste gamme d’hébergements et profitant du dynamisme estival du village."
title = "Camping de Lalouvesc"
weight = 2
[menu.direct_access]
weight = 6

+++
**Ouverture :** 29 avril au 5 novembre 2023 (fermeture de certains hébergements hors saison).

Camping du Pré du Moulin  
241 Chemin de l'Hermuzière  
07520 Lalouvesc  
Tél : 04 75 67 84 86  
camping.lalouvesc@orange.fr

**Réservations :** Par email & par téléphone ou directement en ligne à l’aide de l’application ci-dessous. Attention, les forfaits d’une semaine démarrent nécessairement un samedi. Pour les périodes particulières, préférez le mail ou le téléphone.

<script type="text/javascript" src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"></script>

<script type="text/javascript">

( function() {

    var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"saexsbE", idIntegration:1195, langue:"fr", ui:"OSCA-116693" } );
    
    widgetProduit.Initialise();

})();

</script>

<div id="widget-produit-OSCA-116693"></div>

**Attention, les réservations ne sont confirmées qu'une fois les arrhes encaissées.**

**Horaires d’ouverture du bureau du camping :**

* Mai-juin et septembre-octobre : 9h - 12h et 17h - 19h
* Juillet-août : 9h - 12h et 15h - 19h

**Avis des campeurs :** sur[ Google](https://www.google.com/search?client=firefox-b-d&q=lalouvesc+camping+pr%C3%A9+du+moulin#lrd=0x47f571429bdf2d4d:0xeeff6697329d3a54,1,,,), sur [Tripadvisor](https://www.tripadvisor.fr/Hotel_Review-g1500045-d8501399-Reviews-Camping_Municipal_le_Pre_du_Moulin-Lalouvesc_Ardeche_Auvergne_Rhone_Alpes.html#REVIEWS)

## Situation

Station de moyenne montagne, Lalouvesc est propice à des [activités variées](https://www.lalouvesc.fr/decouvrir-bouger/activites/) pour tous les âges. Les associations, très actives dans le village, proposent l’été de [nombreuses manifestations](decouvrir-bouger/lalouv-estivals/) culturelles. Enfin Lalouvesc est aussi réputé comme [haut lieu de spiritualité](https://www.lalouvesc.fr/decouvrir-bouger/sanctuaire/lalouvesc-haut-lieu-spirituel/).

Situé à 300 mètres du centre du village, à proximité de tous les commerces, et pourtant dans une zone naturelle, notre terrain de camping municipal bénéficie d’une exposition plein Sud.

A quelques centaines de mètres en amont, le Mont Chaix (1213 m) et sa forêt de sapins offrent aux amoureux de la nature ses promenades balisées par l’Office de Tourisme et ses sentiers de grande randonnée. Myrtilles, framboises, champignons y abondent selon la saison.

Le camping du Pré du Moulin, camping municipal,  a été entièrement rénové en 2021 aux cours de journées citoyennes par les habitants de Lalouvesc. Le minigolf de 18 trous a repris des couleurs, le tennis est nettoyé et transformé en terrain multi-sport, l'étang a été empoissonné pour accueillir les pêcheurs. Des hébergements insolites, "les refuges des Afars" dans les arbres ont été rénovés... Lalouvesc s’est mis en quatre pour vous faire oublier les temps difficiles et vous accueillir.

## Hébergements

### En vedette

Huit refuges insolites et deux équipements de qualité supérieure se sont ajoutés à l’offre habituelle depuis 2021.

#### **Insolites : Les refuges des Afars**

Ouverture jusqu'au 2 novembre.

{{<grid>}}

{{<bloc>}}

![](/media/refuge-des-afars-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-1-2021-30.jpg)

{{</bloc>}}

{{</grid>}}

Vous pouvez profiter de quelques-uns de leurs refuges pittoresques en bois, construits sur pilotis, cachés dans les arbres. On y accède par une passerelle ou par une échelle. Chaque refuge est conçu pour deux personnes et dispose de sa terrasse. Certains sont proches et peuvent partager leurs terrasses. Charme, dépaysement et les petits déjeuners sont compris dans la location.

{{<grid>}}

{{<bloc>}}

![](/media/refuge-des-afars-15.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/refuge-des-afars-7.jpg)

Attention, les refuges ne sont pas chauffés et, à 1.000 m d’altitude, les nuits peuvent être fraîches, même en été. Exceptionnel en temps de canicule.

{{</bloc>}}{{</grid>}}

Prévoir literie et équipement.

#### **Exceptionnel : un cottage PMR**

Un cottage, accessible aux personnes à mobilité réduite installé en 2021 complète l’offre d’hébergement avec un produit de grande qualité.

{{<grid>}}

{{<bloc>}}

![](/media/cottage-pmr-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/cottage-pmr-6.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/camping-pmr-3.jpg)

{{</bloc>}}{{<bloc>}}

Idéal pour une famille ou pour un accueil de personnes à mobilité réduite (l'ensemble de l'équipement est accessible en fauteuil roulant). Le cottage est installé à l’entrée du camping, 35m<sup>2</sup>, tout équipé avec des produits de qualité supérieure.

{{</bloc>}}{{</grid>}}

Vous pouvez vérifier la disponibilité de cet hébergement et effectuer votre réservation à l’aide de l’application ci-dessous. Attention, les forfaits d’une semaine démarrent nécessairement un samedi. Pour d'autres périodes, n'hésitez pas à contacter directement le camping. Tél : 04 75 67 84 86. camping.lalouvesc@orange.fr

<script type="text/javascript" src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"></script>

<script type="text/javascript">

( function() {

    var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"saexsbE", idIntegration:1195, langue:"fr", ui:"OSCA-116693-10" } );
    
    widgetProduit.Initialise();

})();

</script>

<div id="widget-produit-OSCA-116693-10"></div>

**Attention, les réservations ne sont confirmées qu'une fois les arrhes encaissées.**

#### **Original : une tente Lodge chauffée**

Ouverture jusqu'au 2 novembre.  Mariez les joies du camping et le confort cosy...

{{<grid>}}

{{<bloc>}}

![](/media/lodge-2.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/lodge-3.jpg)

{{</bloc>}}

{{</grid>}}

Cinq places, deux chambres, sur plancher bois, poêle à bois (bois fourni), entièrement équipée, coin cuisine sur la terrasse de 10m<sup>2</sup>. Le confort canadien en Ardèche.

Vous pouvez vérifier la disponibilité de cet hébergement et effectuer votre réservation à l’aide de l’application ci-dessous. Attention, les forfaits d’une semaine démarrent nécessairement un samedi. Pour d'autres périodes, n'hésitez pas à contacter directement le camping. Tél : 04 75 67 84 86. camping.lalouvesc@orange.fr

<script type="text/javascript" src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"></script>

<script type="text/javascript">

( function() {

    var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"saexsbE", idIntegration:1195, langue:"fr", ui:"OSCA-116693-11" } );
    
    widgetProduit.Initialise();

})();

</script>

<div id="widget-produit-OSCA-116693-11"></div>

**Attention, les réservations ne sont confirmées qu'une fois les arrhes encaissés.**

### Chalets

Ouverture jusqu'au 2 novembre.

![](/media/camping-chalets-5.png)

Dix mini-chalets meublés, équipés de vaisselle et ustensiles de cuisine, bien isolés, disposant du confort électrique (2 plaques chauffantes, réfrigérateur, four micro-onde, convecteur) peuvent accueillir 4 ou 5 personnes. Les occupants utilisent des sanitaires réservés à proximité.

### Mobil-homes

Ouverture en saison estivale.

![](/media/mobil-home-7.JPG)

Cinq mobile-homes avec sanitaire et salle de bains sont également implantés sur le camping.

### Tentes, caravanes, camping-car

Ouverture jusqu'au 2 novembre

Le terrain compte 60 emplacements de tentes, camping-cars ou caravanes dont 35 équipés de bornes pour le branchement électrique, l’eau potable et l’assainissement.

![](/media/camping-lalouvesc-2.jpg)

Les sanitaires chauffés offrent un confort appréciable à cette altitude. L’eau chaude est distribuée sur tous les lavabos, bacs à linge, à vaisselle et douches. Le camping est équipé d'un lave-linge et d'un sèche-linge en libre-service (jetons 5€ disponibles au bureau du camping et à l'agence postale).

## Équipements et activités de loisir

Le camping du Pré du Moulin comprend un terrain de jeux arboré pour enfants (tyrolienne, balançoires, ping-pong, …), un mini-golf, un terrain multi-sport ainsi qu'un étang ouvert à la pêche. Une salle d’accueil (T.V) est à la disposition des campeurs. Un réseau Wifi de bonne qualité couvre maintenant le terrain.

Des animations pour les enfants, encadrées par un moniteur et gratuites, sont offertes plusieurs fois par semaine, ainsi que des balades accompagnées pour les adultes du 14 juillet au 15 août.

![](/media/mini-golf-final.jpg)

![](/media/camping-jeux-1.JPG)

A proximité, nombreuses randonnées, centre équestre, location de vélos électriques à l'Office du tourisme, exposition artistique, concerts et animations diverses l'été dans le cadre de [Lalouv'estival](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/).

L’altitude et le relief ne nous permettent pas de vous offrir des possibilités de baignade sur place, mais des routes touristiques très pittoresques vous conduiront à :

* Une rivière aux eaux limpides : le Doux, à 9 km (alt. 600 m) : baignade surveillée, pêche à la truite…
* Un lac à Devesset à 24 km (alt. 1100 m) : baignade, planche à voile, barque, pêche…

## Tarifs 2023

Les tarifs ci-dessous sont exprimés en euros. Le tarif enfant s’applique jusqu’à 12 ans révolus.

### Chalets, mobil-homes, cottage, tente lodge, refuges

#### Tarif saison estivale (mai-septembre)

Les prix ci-dessous ne comprennent pas la taxe de séjour, payable sur place.

|  | 1 semaine <br>mai / juin / sept. | 2 nuits <br>mai / juin / sept. | 1 semaine <br>juillet / août |
| --- | ---: | ---: | ---: |
| Chalet 4 places | 160 | 100 | 260 |
| Chalet 5 places | 180 | 100 | 280 |
| Mobil-home | 230 | 120 | 370 |
| Lodge | 230 | 120 | 370 |
| Cottage | 580 | 250 | 650 |

**Refuge des Afars** : une nuit pour deux personnes, petits déjeuners compris : 75 €.

#### Tarifs octobre

|  | Tarifs à la journée |
| --- | --- |
| Refuge des Afars (avec petits déjeuners 2 personnes) | 75 |
| Tente Lodge | 105 |
| Chalet | 50 |

### Tarifs emplacements, campeurs, caravanes, camping-cars (toute l'année)

|  | Tarifs à la journée |
| --- | --- |
| Campeur adulte | 3 |
| Campeur enfant | 2 |
| Emplacement de tente | 3 |
| Caravane | 4 |
| Voiture | 3 |
| Branchement eau / EDF | 5 |
| Garage mort juillet / août | 7 |
| Garage mort hors-saison | 1 |

### Mini-golf

Une partie (2 heures) : adulte 5 €, enfant 3 €. Clubs, balles et feuilles de score, fournis.

### Réservations

**Réservations :** Par email & par téléphone

Tél : 04 75 67 84 86  
camping.lalouvesc@orange.fr

ou directement en ligne à l’aide de l’application ci-dessous. Attention les forfaits d’une semaine démarrent nécessairement un samedi. Pour les périodes particulières, préférez le mail ou le téléphone.

**Attention, les réservations ne sont confirmées qu'une fois les arrhes encaissées.**

[**Rechercher**](http://ardeche-mb-prestataire.for-system.com/f116693_fr-.aspx)

## Bienvenue au Camping du Pré du Moulin !