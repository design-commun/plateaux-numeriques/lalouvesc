+++
date = ""
description = ""
header = "/media/arc-en-ciel-2.jpg"
icon = "🔑"
subtitle = "Réserver en ligne votre gîte"
title = "Gites et locations de vacances"
weight = 4

+++
## Gîtes communaux

La municipalité de Lalouvesc propose à la location trois gîtes pouvant accueillir de 4 à 6 personnes.

**Réservations** : Tél : 04 75 67 84 86 ou  
[camping.lalouvesc@orange.fr](mailto:camping.lalouvesc@orange.fr)

Pour les locations d'une semaine, vous pouvez faire votre réservation directement en ligne grâce à l'application ci-dessous. Celle-ci vous permet de vérifier les disponibilités des gîtes.

Attention, les locations à la semaine démarrent nécessairement un samedi.

<script type="text/javascript" src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"></script>

<script type="text/javascript">

( function() {

    var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"saexsbE", idIntegration:1195, langue:"fr", ui:"OSMB-116694" } );
    
    widgetProduit.PreApp("planning.actif", true);
    
    widgetProduit.PreApp("planning.nbMaxProduits", 100);
    
    widgetProduit.Initialise();

})();

</script>

<div id="widget-produit-OSMB-116694"></div>

### Brève présentation

Vous trouverez des photos et une présentation détaillée des gîtes sur la plateforme de réservation.

#### Les Acacias

{{<grid>}}{{<bloc>}}

![](/media/acacias-2.jpg)

Très belle vue sur les Alpes. Sur 2 niveaux : cuisine, salon (canapé et TV), 2ch (2 lits 2 pers), s.d'eau, WC, chauffage central.

{{</bloc>}}{{<bloc>}}

![](/media/acacias-9.jpg)

{{</bloc>}}{{</grid>}}

#### Les Airelles

{{<grid>}}{{<bloc>}}

![](/media/airelles-11.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/airelles-14.jpg)

{{</bloc>}}{{</grid>}}

Séjour avec coin cuisine et coin salon (canapé et TV écran plat), 1ch (1 lit 2 pers), 1ch (2 lits 1 pers superposés), s.d'eau, WC, chauffage central. Petit terrain commun en partie arrière avec salon de jardin. Superbes vues sur le village et sur les Alpes.

Possibilité de regrouper avec le gite mitoyen, pour une capacité max de 8 à 12 pers.

#### Les Genêts

{{<grid>}}{{<bloc>}}

![](/media/genets-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/genets-14.jpg)

{{</bloc>}}{{</grid>}}

Séjour avec coin cuisine et coin salon (canapé et TV écran plat), 1ch (1 lit 2 pers), 1ch (2 lits 1 pers), s.d'eau, WC, chauffage central. Petit terrain commun à l'arrière de la maison avec salon de jardin. Superbes vues sur le village et sur les Alpes..

Possibilité de regrouper ce gite avec le gite mitoyen Les Airelles pour une capacité de 8 à 12 personnes.

### Tarifs 2023

Les prix ci-dessous ne comprennent pas la taxe de séjour.

|  | Week-end et jours <br>fériés (2 jours) | Semaine <br>Basse saison | Semaine <br>juillet/août |
| --- | --- | --- | --- |
| Les Acacias (4 places) | 140 € | 300 € | 330 € |
| Les Genêts (6 places) | 150 € | 320 € | 370 € |
| Les Airelles (6 places) | 150 € | 320 € | 370 € |

Attention, le forfait d'une semaine démarre nécessairement un samedi. Pour toute autre période, un tarif par jour est appliqué : 75 € pour les Acacias, 80 € pour les Airelles et les Genêts.

## Autres gîtes et locations de vacances

Retrouvez toutes les locations de gîtes et maisons de vacances sur la [base de données de l'Office du tourisme](http://www.valday-ardeche.com/hebergements/gites-et-meubles/).

Et n'oubliez pas que bien d'autres hébergements sont aussi proposés à la location sur [le camping du pré du Moulin]( "/decouvrir-bouger/hebergement-restauration/camping/").

## Bienvenue à toutes et tous !

Station de moyenne montagne, Lalouvesc est propice à des [activités variées](https://www.lalouvesc.fr/decouvrir-bouger/activites/) pour tous les âges. Les associations, très actives dans le village, proposent l'été de [nombreuses manifestations](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/) culturelles. Enfin Lalouvesc est aussi réputé comme [haut lieu de spiritualité](https://www.lalouvesc.fr/decouvrir-bouger/sanctuaire/).

Situé à 300 mètres du centre du village, à proximité de tous les commerces, et pourtant dans une zone naturelle, notre terrain de camping municipal bénéficie d’une exposition plein Sud.

A quelques centaines de mètres en amont, le Mont Chaix (1213 m) et sa forêt de sapins offrent aux amoureux de la nature ses [promenades balisées par l’Office de Tourisme et ses sentiers de grande randonnée](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/). Myrtilles, framboises, champignons y abondent selon la saison.