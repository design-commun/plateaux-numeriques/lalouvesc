---
title: Changement climatique
description: ''
subtitle: ''
icon: "\U0001F30D"
weight: 4
date: 
header: ''
menu:
  main:
    weight: 3
    parent: Projets & avenir

---
## Prise de conscience des enjeux planétaires

Le réchauffement climatique et la pandémie du Covid 19, ont grandement accéléré la prise de conscience des enjeux écologiques à la fois chez les politiques et dans la population.

**Un village à 1000m d’altitude, au mode de vie et à la biodiversité largement préservés devient subitement attractif**. Ce changement a été très rapide et déjà perceptible dans la saison estivale 2020 où la fréquentation touristique a été soutenue à Lalouvesc et où le marché de l’immobilier a changé radicalement de perspective.

On trouvera dans cette rubrique quelques initiatives locales répondant à cette prise de conscience.