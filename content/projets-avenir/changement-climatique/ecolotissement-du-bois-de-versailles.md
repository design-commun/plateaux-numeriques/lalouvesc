+++
date = ""
description = "Concilier habitat individuel, préservation de l’environnement et espaces publics de qualité"
header = "/media/ecolotissemet-plan.jpg"
icon = "🌈"
subtitle = "Concilier habitat individuel, préservation de l’environnement et espaces publics de qualité"
title = "Ecolotissement du bois de Versailles"
weight = 1

+++
Pour tous renseignements complémentaires : [mairie@lalouvesc.fr](mailto:mairie@lalouvesc.fr), tél : 04 75 67 83 67

## Inspiration

La commune souhaite faire un lotissement exemplaire par son cadre de vie, son esthétique et sa responsabilité écologique. Un soin tout particulier sera porté aux aménagements communs. Et il sera demandé de privilégier des constructions innovantes à haute qualité environnementale.  
Le lotissement doit s’intégrer harmonieusement dans le paysage et respectant le relief et les perspectives.  
La commune a la volonté de soutenir et développer la filière-bois, au cœur de l’économie locale. Une architecture contemporaine, innovante, mettant en œuvre des matériaux ou de techniques liées aux économies d’énergies, aux énergies renouvelables, ou à l’écoconstruction, notamment le bois, est vivement conseillée. Toutefois, elle devra respecter les fondamentaux de la construction traditionnelle locale, à savoir la simplicité des silhouettes et une bonne insertion dans le paysage.  
[L’architecture écologique du Vorarlberg](https://siteetcite.com/2016/09/30/larchitecture-ecologique-du-vorarlberg-un-modele-de-societe/) en Autriche peut être une bonne source d’inspiration.

## Localisation

Le secteur « Bois de Versailles » se situe au Nord/Ouest du centre bourg à l’entrée de la commune sur la RD 214 en venant de Rochepaule. Le site est accessible :  
– d’un point de vue routier par la voie de desserte du lotissement de Chante Ossel (connectée à la route de Rochepaule/RD 214 et à la route de St Bonnet le Froid/RD532),  
– d’un point de vue piétonnier, au nord, depuis la RD532 par le sentier du bois de Versailles et au sud par un sentier à créer depuis la RD 214.

![](/media/ecolotissement-situation.jpg)  
Le secteur a une topographie marquée avec une pente Nord/Ouest – Sud/Est. Visible de nombreux points de la commune, le site présente une forte sensibilité paysagère.  
L’ensemble comprend une surface de 3 ha environ dont 1 ha est réservé à l’urbanisation.  
![](/media/ecolotissement-plan-de-situation.jpg)

## Aménagements

Une voie centrale servira de colonne vertébrale au quartier. L’ensemble sera structuré par des murets en pierre qui donneront une unité.  
Un parking est placé en ouverture du lotissement avec une place réservée par lot venant compléter le stationnement possible dans les garages privatifs.  
Un petit espace vert est prévu en clôture du lotissement qui se poursuit par un terrain agricole pouvant servir à une exploitation de permaculture, privée ou collective. Ce terrain reçoit les eaux de ruissellement captées.

![](/media/ecolotissement-du-bois-de-versailles-vrd.jpg)

## Lots

L’ensemble comprend 9 lots, d’une surface comprise entre 630 et 900 m2 environ, et répartis de chaque côté de la voie centrale. Les lots sont numérotés de 1 à 8. Deux lots sont numérotés 8a et 8b pour laisser la possibilité d’un achat groupé à un exploitant maraîcher éventuellement intéressé par l’exploitation du terrain agricole concomitant, mais cette option n’est qu’indicative.

![](/media/ecolotissement-bois-de-versaille-plan-2022.jpg)

## Règlement

L’objet du règlement est de maintenir une unité et une vie agréable à l’intérieur du lotissement dans les choix environnementaux privilégiés par la commune, tout en laissant le maximum de liberté aux acquéreurs. Il correspond aussi aux normes actuelles concernant des bâtiments de plus en plus autonomes en énergie.  
Le règlement actualisé en janvier 2022 est [accessible ici.](/media/reglement-ecolotissement-bois-de-versailles-janvier-2022.pdf)  
Le règlement définit les grandes lignes de ce qui est attendu des acquéreurs. Il peut être modifié ponctuellement sur demande, sous réserve de validation par un permis d’aménager modificatif. Le permis et la modification sont validés par un arrêté municipal, signé par la mairie.

## Coût – Prix

### Coût de l’aménagement

Estimation du coût des travaux d’aménagement (à charge de la Mairie) : 273.000 €.  
L’ensemble des aménagements sera réalisé dès que quatre compromis de vente auront été signés.

### Prix de vente des lots

Le prix de chaque lot est calculé sur un prix de base de 37 € au m2 auquel est appliqué une pondération en fonction de la situation du lot. La somme des achats doit couvrir les frais d’aménagement engagés par la commune.

Prix des lots

![](/media/ecolotissement-prix-lots.jpg)

Les lots seront réservés dans l’ordre de signature des compromis de vente.

## Contacts

Pour tous renseignements complémentaires :

[mairie@lalouvesc.fr  
](mailto:mairie@lalouvesc.fr)Tél : 04 75 67 83 6