+++
date = ""
description = ""
header = "/media/jardins-du-haut-vivarais.JPG"
icon = "🪂"
subtitle = ""
title = "Vers un village résilient ?"
weight = 1

+++
L'association "Les Jardins du Haut Vivarais" a pour but de développer l’autonomie et la résilience des personnes et des structures au niveau local. Elle nous présente ci-dessous ses propositions pour faire de Lalouvesc "un village résilient".

_La plupart d’entre nous seront d’accord pour reconnaitre que notre petite planète va au devant d'enjeux inédits à court ou moyen terme : changements climatiques, crises énergétiques, sanitaires ...._

_D’ailleurs les événements que nous vivons depuis ces dernières années sont peut-être les soubresauts de ce que certains appellent un "effondrement". Il n’est pas du tout exclu que notre génération soit bientôt contrainte de revoir totalement son mode de vie : moins d’énergie, moins de confort, moins d’humanité…_

_Être responsable, c'est ne pas ignorer ces questions et commencer à chercher dès aujourd'hui des solutions pérennes. L’avenir ne sera pas une voie individualiste, mais collective._

_A l’échelle d’un petit village ou de quelques communes, il est possible de mettre en place ensemble et dès maintenant des actions efficaces et peu onéreuses, destinées à amortir les probables chocs d’un futur proche_

_Les actions de l'association seront réalisées de façon concrète sur le terrain à travers la mise en place de projets démonstratifs, de formations, d'animations ouvertes à tous les âges._

_Mais aussi de manières non exhaustives : conférences, évènements, projection de films, diffusions d'ouvrages ou magazines, formations et diffusion d’informations sur tout support, etc._

_Les sujets traités sont :_

* _nourriture par production ou par récolte/cueillette naturelle (petits jardins d’inspiration permaculturelle, cueillette sauvage, etc.),_
* _eau (récupération de l’eau de pluie, etc.),_
* _chauffage (notamment au bois, etc.)_
* _gestion des déchets (tri, compostage, toilettes sèches, etc.)_
* _soins naturels (utilisation des plantes sauvages, etc.)_
* _énergie (production, économie, etc.)_
* _interactions sociales (entraides, partage de compétences, etc.)_

### Un jardin idéal en 2022

Il existe déjà le "Palais idéal", construit par le facteur Cheval à Hauterives dans la Drôme avec des galets ramassés sur son chemin, pourquoi pas un "Jardin idéal" à Lalouvesc en Ardèche ? 

_Le projet est d’aménager à Lalouvesc à l’endroit le plus stratégique du village un petit potager d’une vingtaine de m2 qui sera opérationnel dès l’été 2022. Ce potager d’inspiration permaculturelle sera une véritable vitrine pour le village ; il encouragera les visiteurs à penser leur propre potager et pourra faire de Lalouvesc l’exemple du village ardéchois sur la voie de la résilience. Un petit composteur de 3 m3 viendra compléter la réalisation ; il assurera l’autonomie du potager pour sa fertilisation et constituera l’exemple parfait de la gestion des déchets organiques au niveau d’une petite commune._

_Le projet du potager a plusieurs buts :_

* _Promouvoir le village et en faire une vitrine de l’autonomie alimentaire, un thème majeur de la résilience._
* _Le potager idéal sera une structure unique avec très peu d’équivalents sur notre territoire ; une bonne communication en fera un atout majeur pour le village._
* _Inciter les habitants et les visiteurs à créer leur propre potager en prouvant que l’on peut produire énormément et localement sur des toutes petites surfaces. Inutile de préciser que nous sommes assaillis depuis un demi-siècle par la malbouffe et que les perspectives d’avenir ne sont pas très réjouissantes (climat, nourriture…)._
* _Accorder une place privilégiée aux enfants._

Le "Jardin idéal" sera parrainé par Perrine et Charles Hervé-Gruyer, fondateurs de la [ferme biologique du Bec Hellouin](https://www.fermedubec.com/), vitrine mondiale de la permaculture. 