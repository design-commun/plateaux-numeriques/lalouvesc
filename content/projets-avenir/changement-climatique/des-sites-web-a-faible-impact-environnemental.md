+++
date = ""
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = "💻"
subtitle = "Le site Lalouvesc.fr est réalisé en partenariat avec Plateaux numériques, un service de création de sites web durables et accessibles pour les villages d’aujourd’hui et de demain."
title = "Des sites web éco-responsables"
weight = 1

+++
Ce qu’on appelle le “numérique” est composé de milliards d’objets et de systèmes (ordinateurs, smartphones, objets connectés, serveurs, antennes, etc.). Leur fabrication et leur utilisation nécessitent une grande consommation d’énergie, de métaux et d’eau et est l’origine de nombreuses pollutions (gaz à effet de serre, rejets miniers, etc.).

Afin de respecter l’accord de Paris, tous les secteurs doivent diviser leurs émissions de gaz à effet de serre par 4 d’ici 2050. Par rapport au transport et à l’agriculture, le numérique représente une part moins importante des émissions mais devra néanmoins réduire son empreinte. Il devient alors important de comprendre à quoi ressemble des services numériques utiles avec un “poids” écologique le plus bas possible.

Dans ce cadre, l’objectif de [**Plateaux numériques**](https://plateaux-numeriques.fr/) est d’aider les équipes de mairies rurales à créer des sites web utiles, peu chers, accessibles et avec une faible empreinte écologique.

* **Utilité** veut dire que ces sites sont conçus depuis les besoins exprimés par les élus locaux et non pas depuis des idées préconçues et éloignées des réalités du terrain.
* **Peu chers** car le service de Plateaux numériques est à but non-lucratif et les sites créés demandent très peu de maintenance. De plus, nous formons les responsables du site pour qu’ils puisent être autonome et puissent à leur tour former d’autres personnes.
* **Accessibilité** veut dire que ces sites sont pensés pour les personnes avec des déficiences visuelles ou auditives, et peuvent être consultés par tous les citoyens quelque soit l’âge de leur smartphone ou la couverture du réseau télécom.
* **Faible poids écologique** veut dire que ces sites ne favorisent pas le renouvellement des équipements numériques (téléphone, ordinateur) car ils fonctionnent avec des équipements anciens. Ces sites utilisent le moins de ressources possibles pour fonctionner (énergie, eau) et visent à émettre le moins de gaz à effet de serre possible.

Ce projet a reçu un financement du fonds européen “Design Scapes” afin de créer un prototype et un pilote fonctionnel, dont ce site est le résultat.

Le service fonctionne en comptabilité ouverte et les comptes de résultat ainsi que le modèle économique sont visibles par toutes les parties prenantes.

**Vous pouvez en temps réel consulter** [**les statistiques des visiteurs et des pages vues pour ce site**](https://simpleanalytics.com/lalouvesc.fr)**.**

### Pour en savoir plus

Le site de [Plateaux numériques](https://plateaux-numeriques.fr/)