+++
date = ""
description = ""
draft = true
header = ""
icon = ""
subtitle = ""
title = "Grand Atelier des maires ruraux pour la transition écologique"
weight = 1

+++
Le Grand Atelier des maires ruraux pour la transition écologique, ci-après nommé le Grand Atelier, a pour objectif d’élaborer la vision politique des territoires ruraux et de définir la feuille de route de l’AMRF pour la transition écologique de la France.

Le Grand Atelier est appelé à rendre un avis politique et non technique, comportant des propositions claires, utiles, éclairées et élaborées collectivement. Pour mener à bien cette réflexion, il bénéficiera d’interventions d’experts, d’acteurs et de témoins qui apporteront aux membres du Grand Atelier des informations utiles à leurs réflexions individuelles et collectives.

L’objectif est de rassembler 120 maires ou adjoints de communes de moins de 3500 habitants, qui représentent une diversité de profils et d’implantation géographique. Lalouvesc est représentée au Grand Atelier par son 2ème adjoint, Jean-Michel Salaün.

Quatre sessions sont prévues

* Samedi 25 et dimanche 26 février
* Samedi 22 et dimanche 23 avril
* Samedi 3 et dimanche 4 juin
* Samedi 1er et dimanche 2 juillet

La première session sera consacrée à la formation de tous les participants sur les enjeux de la transition écologique et du changement climatique.

Les sessions de travail alterneront ensuite des moments en plénière et des moments en petits groupes. Les thématiques de travail sont actuellement en cours de précision. Elles porteront sur :

* les conditions de la production d’énergies renouvelables,
* les biens communs naturels (aménités rurales et fiscalité),
* les ressources et leviers des communes rurales (citoyenneté active et ingénierie au service de la transition écologique).