---
title: Projets & avenir
description: ''
subtitle: 'Une ambition : Lalouvesc, village exemplaire'
weight: 4
date: 
icon: "\U0001F52D"
header: ''
menu:
  main:
    weight: 6

---
Le programme de la municipalité élue en 2020 s’appuie sur les atouts d'un contexte de nouveau favorable pour les villages de haute montagne :

* Il se résume en trois verbes : **réparer**, **coordonner** et **développer**.
* Il a un fil directeur : faire **un village exemplaire**, adapté au monde nouveau.
* Il met l’accent sur trois thématiques : l’**accueil** (hébergement, animations, services, commerces) qu’il faut entretenir et développer, la **culture** qui ouvre les esprits et attire les amateurs, et enfin la **forêt**, à la fois source de bien-être et activité économique primordiale pour le village et ses alentours.
* Il a une préoccupation : préserver la **douceur de vivre** dans le village.

### Une planification exigeante

Les décisions et les actions sont pilotées et encadrées par **un plan**, public afin que les habitants, les visiteurs et les partenaires participent discutent et, si possible, adhèrent au processus.

Le plan a **deux échelles** : Il comprend d’une part les objectifs précis pour les travaux et actions à entreprendre dans l’année à venir et d’autre part un calendrier prévisionnel indicatif pour les orientations des actions des années futures.

Chaque automne l’équipe municipale, en concertation avec la population, évaluera les réalisations de l’année, repèrera les difficultés rencontrées et les éventuelles opportunités nouvelles pour actualiser le plan, définir les objectifs de l’année suivante et réviser le plan d’ensemble.

### Actions 2020-2021

#### Réparer est la priorité

* cimetière, hôtel Beauséjour, mairie, camping, basilique, assainissement, matériel municipal…
* finances : réviser les dépenses, définir des indicateurs, un budget sincère et cohérent,
* communication : révision du site web, bulletin mensuel, relation avec les employés municipaux, avec la population.

#### Coordonner pour une meilleure efficacité

* ouverture de deux comités (vie locale et développement) aux non élus,
* révision des relations avec les associations (conventions, maison des associations),
* engagement d’un dialogue suivi avec le Sanctuaire,
* organisation de journées citoyennes

#### Développer pour préparer l’avenir

* lancement des projets sur la forêt (Of. du tourisme, jeu-monument, gîte témoin)
* accompagnement et réflexions sur l’utilisation des bâtiments symboles (Ste Monique, Le Cénacle)
* Élargissement du camping

### Perspectives à moyen terme

#### Redessiner les pôles du village

Une année ne suffira pas pour réparer tout ce qui doit l’être, mais progressivement, une fois les investissements indispensables réalisés, “entretenir” devra remplacer “réparer”.

Globalement, à partir de l’action engagée sur l’aire Beauséjour deux pôles d’attractivité seront redessinés : un sur la forêt (Off. du tourisme, aire de jeux), l’autre sur le Sanctuaire (réaménagement de la rue St Régis, accueil des pèlerins, dégagement de la perspective de la Basilique).

#### Gérer le bien commun

La coordination des différentes initiatives (associations, Sanctuaire, Mairie) doit déboucher sur la prise de conscience d’une gestion du bien commun, dépassant l’action municipale avec, pourquoi pas à terme, une société d’économie mixte.

L’investissement collectif dans l’aménagement du camping municipal symbolise cet état d’esprit.

#### Maîtriser le développement

L'objectif n’est pas de retrouver les 12 hôtels des années glorieuses du village, mais de construire une économie prospère, à taille humaine dans un village où il fait bon vivre.

Les bâtiments monumentaux (Ste Monique, le Cénacle) doivent être réinvestis. Deux secteurs seront encouragés : le tourisme et la filière bois, avec une préoccupation commune : l’environnement.