+++
date = ""
description = ""
header = "/media/ecole-4.jpg"
icon = "🚸"
subtitle = ""
title = "Un engagement de tous pour l'école"
weight = 2

+++
Une [école primaire](/vivre-a-lalouvesc/acteurs-services/ecole/#lécole-primaire-saint-joseph) est une richesse exceptionnelle pour un village de 400 habitants. C'est une assurance pour de jeunes parents qu'ils n'auront pas à faire des kilomètres tous les jours pour amener le matin, puis rechercher l'après-midi leur enfant à l'école.

Une petite école, c'est aussi la possibilité d'avoir une relation pédagogique privilégiée entre les enfants, avec les parents et avec une équipe pédagogique disponible et motivée.

Mais une petite école a besoin de l'étroite collaboration de tous les acteurs.

Elle a pu se maintenir d'abord grâce à l'investissement des parents d'élèves qui pilotent son association de gestion (OGEC), qui organisent au travers de l'association des parents d'élèves (APEL) des manifestations de soutien pour financer les sorties et les animations proposées aux élèves, ou encore qui soutiennent l’enseignante dans sa pédagogie.

La tutelle Saint Joseph et la direction Diocésaine fournissent un soutien, une logistique, et des financements nécessaires à la vie d'un établissement privé.

La Commune, de son côté, fournit un soutien financier et matériel largement au-delà de ses obligations statutaires.

Pour défendre notre école, comme un bien commun du village, nous avons besoin de l'engagement de toutes et tous.

**Participez aux ventes et animations organisées par l'APEL !**

**Faites un don à l'école !**