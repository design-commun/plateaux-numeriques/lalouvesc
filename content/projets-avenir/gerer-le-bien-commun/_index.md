---
title: Gérer le bien commun
description: ''
subtitle: ''
icon: "\U0001F932"
weight: 1
date: 
header: "/media/camping.jpg"
menu:
  main:
    weight: 1
    parent: Projets & avenir

---
Tous ensemble, municipalité, associations, commerçants, artisans, professions libérales, retraités, étudiants, écoliers, nous avons le souci de gérer au mieux nos biens communs.