+++
date = ""
description = ""
header = "/media/journee-citoyenne.jpg"
icon = "🎢"
subtitle = ""
title = "Un jeu-monument sur le parc du Val d'Or"
weight = 5

+++
## Actualité du dossier

La présentation du projet ci-dessous date de 2020. Depuis le dossier a bien avancé :

* [**démolition de l'hôtel**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#zoom) début 2021,
* **lancement d'un concours d'idées** en 2021,
* [**résultat du concours**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/22-ce-qui-plait/#jeu-monument--une-premi%C3%A8re-%C3%A9tape-franchie) en avril 2022.

## Projet initial

L'équipe municipale a hérité d'un lourd dossier : celui d'un ancien hôtel érigé en plein centre qui a été endommagé par la tempête de 1999 et s’est depuis terriblement dégradé au point de menacer ruine. Une procédure d’expropriation a eu lieu, un dossier de demande de subventions a été déposé à l'automne 2020, ce qui permet d’envisager une démolition à l'automne 2021.

## De la ruine au monument

La décision a été prise de transformer ce chantier difficile en une opportunité impliquant les habitants du village et lui donnant une attractivité nouvelle. Il s'agirait d'ériger un "jeu-monument", nouveau bien commun des Louvetous. Pour l'aider au démarrage de ce projet, la Commune a fait appel à une équipe d'architecte et de paysagiste du CAUE.

![](/media/plan-jeu-monument.jpg)

L’hôtel Beauséjour est dans une situation centrale. Ce lieu d’articulation entre deux paysages s’ouvre d’un côté sur les sommets de Haute-Loire et, de l’autre, sur les crêtes alpines. C’est ici qu’il est possible de prendre conscience de la géographie particulière de la commune, installée en ligne de crête.

La proposition de l'équipe municipale est de profiter de cette ouverture nouvelle pour construire un jeu-monument/monumental en bois qui redonnerait un nouvel attrait au centre-village et en équilibrerait les dynamiques. L’idée fait référence à la [vague des remparts du parc Blandan](https://www.baseland.fr/projets/lyon-vague-des-remparts/) à Lyon. Son implantation précise reste encore à définir.

Ce projet représenterait une transition entre le centre au caractère urbain et le parc du Val D’or, espace public majeur très prisé des habitants.

## Un projet participatif

Si par ce projet, la commune désire valoriser la filière bois local et le savoir-faire des métiers de cette filière, elle souhaite aussi une implication forte de ses habitants.

Fortement imprégné par son passé religieux, Lalouvesc projette d’inscrire ce travail dans un nouvel imaginaire. Ce jeu-monument veut dépasser la simple mise en place de dispositifs préfabriqués et standards au profit de dispositifs singuliers, reflet d'une histoire écrite ensemble, qui s’adresse à la part sensible de chacun.

Construire un monument, même s’il s’agit d’un jeu, n’est pas anodin. Nous avons déjà à Lalouvesc un monument, face aux Alpes, inscrit dans l’histoire du village, la Basilique, construite par l’architecte Pierre Bossan avec la contribution des villageois et des fidèles ([wikipédia](https://fr.wikipedia.org/wiki/Basilique_Saint-R%C3%A9gis_de_Lalouvesc)). Il s’agit maintenant d’en construire un second, bien sûr plus modeste, mais tourné vers l’autre flanc du col de Lalouvesc, les Cévennes, et tourné vers les nouveaux défis des temps nouveaux sans renier le passé du village.

L’idée serait d’illustrer dans un jeu monumental pour les enfants la thématique de la forêt, qui entoure le village et participe à son économie avec tous les métiers de la filière bois. Cette thématique s’inscrit dans la transformation du tourisme, tourné vers la nature, éco-responsable et bienveillant. Elle trouve aussi un écho dans les propos de l’encyclique _Laudati Si’_.

L'ambition de faire de ce projet un projet collectif pourrait aller jusqu’à la fabrication de certains éléments par la population lors d’ateliers participatifs encadrés par des professionnels ou corporations afférents à la filière bois.

Déjà une carte postale a été éditée pour recueillir les idées des habitants et des estivants.

![](/media/carte-postale-jeu-monument-1.jpg)![](/media/carte-postale-jeu-monument-2.jpg)

D’autres partenaires pourraient contribuer à ce travail collectif et participatif, tels que les scolaires, ou élèves de lycées professionnels, en fonction des besoins. Ces partenariats restent à construire et incomberont aux futurs concepteurs qui pourront en premier lieu, s’appuyer sur le tissu associatif communal.

Le financement d'un tel projet dépasse largement les capacités d'une petite Commune comme Lalouvesc. Des dossiers de demandes de subventions ont été déposés.

### Pour aller plus loin

[Présentation détaillée du projet](https://docs.google.com/document/d/10wbSzgavO6ykDykgHuMkqLvhkm5nBu2t93pf5AJ1k4U/edit?usp=sharing)