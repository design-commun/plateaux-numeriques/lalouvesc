+++
date = ""
description = ""
header = "/media/pompiers-lalouvesc-2.jpg"
icon = "🧑‍🚒"
subtitle = "Soutenir et rejoindre les pompiers volontaires de Lalouvesc"
title = "Des pompiers au service de tous"
weight = 1

+++
Un jour ou l’autre, nous aurons tous besoin de composer ce fameux 18.

Sur Lalouvesc, nous avons la chance d'avoir une caserne de pompiers volontaires. C'est un bien précieux qu'il faut soutenir.

## Organisation du service

Les sapeurs-pompiers volontaires sont engagés sur les mêmes missions que les sapeurs-pompiers professionnels. Secours à personne, lutte contre les incendies, accidents de la circulation, protection de l’environnement… les missions sont multiples et variées, avec toujours le même objectif : protéger les populations.

Le centre de secours de Lalouvesc, est composé au 1er Janvier 2021, de 11 sapeurs-pompiers, dont 3 femmes.

Le centre est dirigé par le lieutenant Lionel Achard, sous les ordres du colonel hors classe Alain Rivière (directeur départemental).

N’hésitez pas à aller à la rencontre des sapeurs-pompiers de Lalouvesc, lors de leur manœuvre qui se déroule en général le deuxième dimanche de chaque mois, ou dès que vous les croisez dans le village, que ce soit en tenue ou en civil.

![](/media/pompiers-lalouvesc-3.JPG)

## Comment devenir pompier volontaire

Les recrutements ont lieu deux fois dans l’année, en avril et en décembre. Deux jeunes recrues de 16 ans ont rejoint la famille des sapeurs-pompiers en décembre 2020 et deux autres de 16 et 30 ans vont intégrer leurs rangs durant le mois d’avril 2021.

Plusieurs conditions sont requises pour devenir sapeur-pompier volontaire:

* être âgé de 16 ans au moins (21 ans pour les officiers). Pour les mineurs, le consentement écrit du responsable légal est obligatoire ;
* jouir de ses droits civiques et ne pas avoir fait l’objet d’une condamnation incompatible avec l’exercice des fonctions (l’autorité de gestion peut se procurer un extrait du bulletin n°2 du casier judiciaire) ;
* se trouver en position régulière au regard des dispositions du code du service national ;
* s’engager à exercer son activité de sapeur-pompier volontaire avec obéissance, discrétion et responsabilité, dans le respect des dispositions législatives et réglementaires en vigueur et de la charte nationale du sapeur-pompier volontaire

Pour intégrer le centre de secours de Lalouvesc, il faut vous rapprocher du chef de centre, le Lieutenant Lionel Achard, qui vous expliquera toute la marche à suivre et vous accompagnera dans votre demande d’inscription pour devenir un « soldat du feu ».

Les sapeurs-pompiers de Lalouvesc, mais plus généralement ceux de toute la France, sont toujours à la recherche de volontaires pour renforcer leurs rangs. Alors n’hésitez pas à sauter le pas !

Nous pouvons être fiers de ces hommes et de ces femmes et de leur engagement. Ils n’hésitent pas à laisser leur famille, leurs amis ou toute autre occupation personnelle pour nous venir en aide.

**Nous devons les soutenir, les rejoindre, les aider mais surtout les remercier pour toutes ces heures passées aux formations, aux manœuvres, aux astreintes, tout ce temps réservé pour nous !**

**Bravo à eux !**

## Pour en savoir plus

[Informations sur le site du ministère de l'intérieur](https://www.interieur.gouv.fr/Le-ministere/Securite-civile/Tous-volontaires).