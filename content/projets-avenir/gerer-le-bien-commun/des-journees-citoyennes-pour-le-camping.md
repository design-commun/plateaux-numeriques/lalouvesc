+++
date = ""
description = ""
header = "/media/journee-citoyenne-jour-3-1.jpg"
icon = "🚧"
subtitle = ""
title = "Des journées citoyennes pour réparer le village"
weight = 3

+++
## Journées citoyennes 2022 : City-Park, parcours thématiques, fleurissement

{{<grid>}}{{<bloc>}}

Un commando de citoyens est allé chercher sur la commune d'Aubière un City-Park acquis par la municipalité. Il a fallu le démonter, le charger sur le camion et le ramener. Le tout en une journée et demi. [**Chapeau les citoyens !**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#le-city-stade-a-%C3%A9t%C3%A9-r%C3%A9cup%C3%A9r%C3%A9)

D'autres se sont attachés à faire le tour du parcours des Lapins et celui des champignons. Un diagnostic a été réalisé et tous les panneaux du parcours des champignons ont été refaits.

La traditionnelle journée citoyenne s'est tenue le 7 mai. Le mini-golf a été nettoyé, le bois des refuges débroussaillé sur le camping, les fleurs vivaces plantées, un chalet réparé... l[**es citoyens n'ont pas chômé**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/une-journee-citoyenne-efficace...comme-toujours/)...

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-13.jpg)

{{</bloc>}}{{</grid>}}

Et le fleurissement du village est toujours assuré par des citoyens bénévoles. **Merci à eux ou plutôt à elles !**

![](/media/journee-citoyenne-2022-26.jpg)

## Journées citoyennes 2021 : le camping

Le camping par ses capacités d’hébergement est un outil important pour la réussite de la saison estivale. Mais il avait besoin d’un sérieux lifting. La Mairie a décidé en 2021 de retrousser ses manches et d’investir pour sa rénovation. Des travaux de terrassement ont été entrepris et de nouveaux hébergements ont été achetés.  
Les employés communaux et les investissements prévus ne suffisaient pourtant pas à redonner au camping tout l’éclat souhaitable pour accueillir dignement les visiteurs. Nous avions besoin de tous les habitants pour retrouver un camping à la hauteur de la réputation d’accueil du village. C’est pourquoi, comme nous l’avions annoncé dans notre programme nous avons planifié des “journées citoyennes”. Toutes les Louvetonnes et tous les Louvetous, petits et grands, jeunes et plus âgés, ont été conviés à **des journées citoyennes qui se sont tenues en mars et avril 2021** !

{{<grid>}}{{<bloc>}}

![](/media/journee-citoyenne-1-2021-11.jpg)

Ce fut un grand succès :

* [1ère journée](/vivre-a-lalouvesc/s-informer/actualites/premiere-journee-citoyenne-2021-premiers-resultats/)
* [2ème journée](/vivre-a-lalouvesc/s-informer/actualites/journee-citoyenne-episode-2/)
* [3ème journée](/vivre-a-lalouvesc/s-informer/actualites/a-lalouvesc-la-neige-n-arrete-pas-le-citoyen/)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-jour-3-4.jpg)

{{</bloc>}}{{</grid>}}

## Journées citoyennes 2020 : le terrain de boules

Au lendemain du second tour des élections municipales, la première journée citoyenne a réuni les citoyens du village pour nettoyer le jeu de boules du Val d'Or envahi par les herbes.

![](/media/journee-citoyenne-lalouvesc-11-juillet-2020.jpg)