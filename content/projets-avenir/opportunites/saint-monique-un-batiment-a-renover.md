+++
date = ""
description = ""
header = "/media/colonie-d-oran-lalouvesc.png"
icon = "🏗️"
subtitle = ""
title = "Sainte Monique, un bâtiment à investir"
weight = 1

+++
Parmi les bâtiments du patrimoine immobilier de la Commune, le bâtiment Sainte Monique tient une place particulière.  
Son histoire est étroitement liée à [celle du village](vivre-a-lalouvesc/histoire-identite/histoire-de-lalouvesc...de-la-prehistoire-a-nos-jours/) au cours de la première moitié du XXe siècle. Sa situation est exceptionnelle, dominant les Alpes.  
Le bâtiment, désaffecté depuis plusieurs années, sert aujourd'hui principalement d'entrepôt pour le matériel des associations du village. Sa structure (toit, murs, niveaux) est en bon état, mais les aménagements intérieurs sont à réviser complètement.

![](/media/sainte-monique.JPG)

La Mairie souhaite transformer le rez-de-chaussée du bâtiment en Maison des associations, avec une salle de réunion/réception et des bureaux mutualisés. La destination des étages reste à imaginer.

Un premier dossier visant à transformer le bâtiment en école de musique n'a pu aboutir, faute de financement. La Mairie étudiera toute proposition réaliste d'institutions ou de particuliers désirant investir pour rénover le bâtiment, tout spécialement ceux qui pourront préserver la destination qu'elle a réservée au rez de chaussée.