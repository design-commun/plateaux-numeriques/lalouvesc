+++
date = ""
description = ""
header = "/media/le-cenacle.png"
icon = "🏰"
subtitle = ""
title = "Le Cénacle, un joyau à promouvoir"
weight = 1

+++
L'ancien bâtiment du Cénacle, autrefois occupé par les sœurs du Cénacle sous l'autorité de [Sainte Thérèse Couderc](decouvrir-bouger/sanctuaire/sainte-therese-couderc/), est aujourd'hui à vendre. Le dossier a été confié à [Karism Conseil](https://www.lesprojetsdesaintjoseph.fr/cenacle-de-lalouvesc/).

Parallèlement, une [étude est lancée](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/24.amenager-pour-accueillir/#bient%C3%B4t-des-sc%C3%A9narios-pour-le-c%C3%A9nacle-et-sainte-monique) pour définir le meilleur scénario pour l'avenir de la propriété et celui de Sainte Monique par le Syndicat mixte Rives du Rhône.

Le Cénacle comprend une surface habitable totale de 4 000 m2 en parfait état et tout équipé, dans un parc de plus d’1 hectare. Il s'agit de la plus belle opportunité immobilière disponible dans le village.

La municipalité suit et accompagne les sœurs dans leur recherche d'un acheteur.

![](/media/cenacle-aerien-2022.jpg)