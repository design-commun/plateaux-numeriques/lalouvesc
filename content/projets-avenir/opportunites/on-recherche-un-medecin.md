+++
date = ""
description = ""
header = ""
icon = "🩺"
subtitle = ""
title = "On recherche : un médecin"
weight = 1

+++
La Commune a la chance de disposer d'un médecin installé sur place. Mais il a annoncé qu'il souhaitait prendre sa retraite.  
Heureusement, nous nous trouvons à la fois dans une zone de revitalisation rurale (ZRR) et aussi dans une zone d'action complémentaire de l'Agence régionale de santé. Cette double situation offre des aides très intéressantes, qui se chiffrent en dizaine de milliers d'euros ([voir ici](https://www.ameli.fr/ardeche/medecin/exercice-liberal/vie-cabinet/aides-financieres/pratique-zones-sous-dotees)), pour l'installation de jeunes médecins et même pour des médecins qui "préparent leur cessation d’activité en accueillant et accompagnant un médecin nouvellement installé dans leur cabinet".