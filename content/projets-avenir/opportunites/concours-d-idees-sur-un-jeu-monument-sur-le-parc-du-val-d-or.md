+++
date = ""
description = "Lancement du concours ouvert aux étudiants en architecture, design et filière bois"
draft = true
header = "/media/journee-citoyenne.jpg"
icon = "🎢"
subtitle = ""
title = "Concours d'idées sur un jeu-monument sur le parc du Val d'Or"
weight = 1

+++
Le projet du concours d’idées participe à la mise en valeur touristique, patrimoniale et paysagère d’un site en lien avec le centre du village. Le site deviendra un lieu original attractif pour tous où la nature et la biodiversité seront préservées dans une atmosphère apaisante pour pratiquer toutes sortes de loisirs, un espace multifonctionnel, festif, sportif, ouvert à la détente, à la flânerie et aux jeux.

La démolition d’un l’hôtel en ruine ouvre l’axe principal du village, situé sur un col, sur un versant jusqu’ici négligé, celui des Cévennes, et marque aussi la complémentarité des deux dimensions de son histoire, l’une spirituelle, abstraite, concrétisée par les pèlerinages et la basilique orientée vers les Alpes, l’autre matérielle, plus concrète, marquée par le tourisme vert, la forêt, le bois.

Cette mise en valeur passe par la réalisation d’un [jeu-monument](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/un-jeu-monument-sur-le-parc-du-val-d-or/) sur un parc de jeux existant en contrebas du chantier de démolition de l'hôtel. Les buts principaux poursuivis par la commune sont les suivants :

* Accroître l’attractivité du village.
* Mettre en valeur un patrimoine local.
* Valoriser le bois et la forêt, ressource et compétence locales..

La conception du monument est prévue en deux étapes. La première, à laquelle participe ce concours, est la réalisation un “petit monument ludique” qui préfigurera le monument définitif dont la conception et la construction seront lancées dans un second temps sur la base d’un financement participatif.

## Règlement du concours d'une idée

Le concours est ouvert aux étudiants en architecture, en design et en ingénierie bois. Il s’agit d’un appel à idées mariant deux dimensions, urbaine et architecturale :

* dimension urbaine : Quelle vocation donner aux espaces libérés au centre du village et comment relier deux quartiers ayant des vocations différentes (centre ville et parc) ?
* dimension architecturale : quel jeu-monument pour célébrer la forêt et à quel emplacement ?

### **Organisation**

#### 1°- Organisateur du concours

Le concours est organisé par la commune de Lalouvesc pour accompagner les élus à mettre en valeur le site dégagé par la démolition de l’hôtel Beauséjour au centre du village.

#### 2°- Calendrier

* 10 janvier 2022 : date limite de l’envoi des candidatures∙
* mi-janvier 2022 : sélection de quatre candidatures.
* 16 avril 2022 : dépôt des dossiers
* 22 avril 2022 : audition publique des candidats ou équipes
* 30 avril 2022 : désignation du lauréat

#### 3°- Conditions de participation au concours d’idées

Le concours d’idées est ouvert aux étudiants en fin de scolarité pour l’année 2021/2022 dans une école d’architecture, d’ingénieur, de bureau d’études bois ou de design. Les candidats peuvent répondre seuls ou en équipe. Les équipes pluridisciplinaires seront appréciées.

#### 4°- Candidature

La candidature est rédigée sur un [formulaire en ligne](https://docs.google.com/forms/d/e/1FAIpQLSfGMFNeNpByrTnyQXf7o-bdg6Y_MCGzBgm5syktlKwRHedbGw/viewform?usp=pp_url) et accompagnée d’une copie de la (ou des) carte(s) d’étudiant. Elle doit être envoyée avant le 10 janvier 2022 minuit.

#### 5°- Sélection des candidatures

Quatre candidatures seront sélectionnées pour participer à la suite du concours. Critères de sélection : motivation, éventail et complémentarité des compétences.

Les quatre candidats ou équipes sélectionnées recevront un numéro d’identification valant confirmation de leur sélection, seul ce numéro sera indiqué sur l’ensemble des documents rendus.

#### 6° - Informations - questions - réponses

Les candidats sélectionnés recevront une série de documents techniques précisant le cadre du projet.  
Il sera répondu aux questions écrites reçues avant le 3 avril 2022 sur un support numérique en accès libre.  
Des visites des lieux pourront être organisées à la demande.

#### 7°- Dépôt du dossier

Dépôt des dossiers avant 16h00 le 16 avril 2022 à la mairie de Lalouvesc rue des Cévennes Lalouvesc 07520. Même date et horaire pour l’envoi de versions numériques (CD, clefs USB) mairie@lalouvesc.fr.

#### 8° Audition publique

Les candidats sélectionnés présenteront en 10mn leur pré-projet dans une séance ouverte au public le 22 avril, suivi d’une séquence de questions-réponses de 10mn

#### 9°- Composition du dossier

1. Deux panneaux sur format A1 (841mmX594mm) :

* le premier au stade d’une esquisse du parti d’aménagement général du site et notamment de l’impact attendu du positionnement du jeu-monument exprimé par des plans, coupes et croquis ou tout autre moyen graphique ;
* le second au stade d’une esquisse rendue suivant tous moyens d’expression graphiques pour rendre compte du jeu-monument envisagé dans ses fonctionalités et volumétrie

1. Sur papier A4 (297mmX210mm) : note explicative du parti retenu (2 à 4 pages recto maximum).
2. Une version numérique clef USB/CD de toutes les pièces.

L’orientation et les échelles utilisées devront être mentionnées.

#### 10°- Sélection du lauréat, classement des équipes

Suite à un processus de sélection, une équipe sera désignée lauréate et les autres équipes pourront être classées.  
La sélection finale et le classement seront réalisés par le Conseil municipal éclairé par les avis de deux instances :

* un jury d’experts (architectes, urbanistes, professionnels du bois) qui classera les dossiers des candidats,
* une consultation des habitants du village.

#### 11°- Critères de sélection

* Caractère singulier du jeu-monument proposé.
* Clarté des points forts du projet proposé.
* Capacité du projet à incarner les objectifs à atteindre.

#### 12°- Les prix

* 1er prix : 2 000 € et participation à la définition du programme urbain dans lequel sera placé définitivement le jeu-monument.
* 2ème prix : 300 €
* 3ème prix : 300 €
* 4ème prix : 300 €

Tous les projets feront l’objet d’une valorisation (expo/publications…).

Le Conseil se réserve le droit de ne pas accorder tous les prix s’il considère que la qualité ou le nombre de projets reçus est insuffisant.

#### 13°- La construction

La mairie réalisera une première étape du projet de jeu-monument dans l’année civile 2022 qui s’inspirera des propositions de l’équipe lauréate. Celle-ci, en partie ou une totalité, pourra être impliquée dans cette réalisation en collaboration avec le maître d'œuvre choisi.