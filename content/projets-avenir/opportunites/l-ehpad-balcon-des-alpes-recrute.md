+++
date = ""
description = "Aides-soignant(e)s temps plein ou temps partiel CDI"
header = "/media/ehpad-lalouvesc.jpg"
icon = "👫"
subtitle = ""
title = "L’EHPAD, Balcon des Alpes recrute"
weight = 1

+++
L’EHPAD recherche activement quatre aides-soignant(e)s en CDI, temps plein ou temps partiel. Faute de personnel, l’établissement est obligé d’avoir recours aux agences d’intérim et subit des surcoûts artificiels.

**Faites-le savoir autour de vous !** Relayez l’information dans tous vos réseaux. Il faut aider cet établissement essentiel à la vie du village.

Un poste permanent dans un village sympathique, préservé,  dynamique et plein de ressources, au climat agréable, au paysage exceptionnel où le coût de la vie et du logement sont abordables, n’est pas si fréquent.

Téléchargez [la fiche de poste](https://www.lalouvesc.fr/media/2022-06-27-avis-de-vacance-de-poste-as-llv-ehpad.pdf) .