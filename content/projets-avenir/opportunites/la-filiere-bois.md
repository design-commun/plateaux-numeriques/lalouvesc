+++
date = ""
description = ""
header = "/media/bois-mont-chaix.jpg"
icon = "🌲"
subtitle = ""
title = "La filière-bois"
weight = 1

+++
La forêt est espace privilégié, de plus en plus reconnu pour la biodiversité ou le stockage du carbone.

Cet [extrait d'une vidéo](https://youtu.be/Enm9eONaNZo) de Matthieu Fraysse montre magnifiquement qu'on entre dans Lalouvesc toujours par la forêt, même par la voie des airs. Le village a une position enviable en balcon, qui offre de larges panoramas où la forêt est toujours en premier plan. Sa situation invite naturellement à la contemplation.

## Lalouvesc au centre de la forêt

![](/media/lalouvesc-satellite.jpg)

Il suffit de regarder une image satellite pour mesurer l'ampleur de la forêt sur le territoire de la Commune. La forêt a pour le village et la région un fort potentiel économique. A Lalouvesc, il existe une scierie qui utilise des bois des forêts alentours, une menuiserie qui construit des maisons à ossature bois, une entreprise spécialisée dans la construction de maison en fustes avec des bois locaux. Les forêts sont exploitées et des grumiers traversent régulièrement le village. Le Bois De Versailles a été replanté, lors du reboisement après la tempête de 1999 avec un mélange de résineux et feuillus.

## Promouvoir la filière-bois

Poursuivant et développant un mouvement déjà amorcé dans les mandatures précédentes, la commune souhaite promouvoir la filière bois :

* écolotissement
* foire forestière
* jeu-monument

## Des jeunes investis

Plusieurs jeunes du village ont choisi un métier dans la filière. Quatre d'entre eux nous ont fait part de leurs idées pour la développer.

**Lionel ACHARD** : J’ai 26 ans, originaire de St Félicien. J’ai suivi une formation forestière au CEFA à Montélimar, où j’ai obtenu un BEP, un Bac Pro et un BTS centré sur la gestion forestière. J’ai choisi la filière bois car je trouve que c’est essentiel à notre vie. C’est un matériau qui vit du début à la fin, dans toute ses utilisations.

**Guillaume BESSET** : J'ai 36 ans et je suis originaire de Lalouvesc. Je suis parti à l'âge de 16 ans pour y revenir 8 ans plus tard. J'ai fait mon apprentissage à l'institut des compagnons à Mouchard (Jura). Ensuite j'ai décidé de faire mon tour de France chez les compagnons charpentiers des devoirs du tour de France qui font partie de la fédération compagnonnique des métiers du bâtiment. Voilà maintenant 14 ans que je suis compagnon charpentier. Prédisposé aux métiers du bois, j'aime cette matière avec laquelle on peut faire diverses choses. Toutes les réalisations sont à elles seules des œuvres qui peuvent marquer des siècles.

**Thomas COUETTE** : J'ai 27 ans, j'ai grandi et j'habite à Lalouvesc. J'ai toujours été passionné par la construction. Après un BTS bâtiment orienté sur la maçonnerie, je me suis tourné vers la construction bois : la fuste. C'est un mode de construction atypique qui m'a tout de suite attiré, j'ai donc suivi une formation en Corrèze avant de me mettre à mon compte en tant que fustier. Ce qui me plaît, c'est de travailler directement le bois issu des forêts locales. C'est un matériau naturel, chaleureux, écologique et durable. Chaque pièce de bois est unique et on peut laisser libre court à son imagination.

**Gaëtan SERAYET** : J’ai 22 ans, je suis originaire de Lalouvesc. A l’âge de 15 ans, à la sortie de la troisième je suis parti à l’institut des compagnons à Mouchard pour passer mon CAP de menuisier. A la fin de ces deux années, j’étais sûr que ce métier était fait pour moi, j’ai donc décidé de continuer à me perfectionner en partant sur le tour de France chez les compagnons menuisiers du devoir de liberté (à la Fédération compagnonnique). J’ai pu passer mon brevet professionnel. Je suis actuellement encore sur le tour de France, il me reste encore 1 an à faire pour terminer mon tour. Je suis en train de passer mon brevet de maîtrise pour continuer à approfondir mes connaissances. Le bois est un matériau très noble, on peut le travailler pour lui donner des formes exceptionnelles.

**_A partir de votre expérience, comment pourrait-on développer des activités autour de la filière bois à Lalouvesc ?_**

GB : Moi je demanderais plutôt : qui serait prêt à venir investir à Lalouvesc autour d'un projet sur du bois ? De mon côté je suis prêt à aider avec mes connaissances et mon savoir-faire une personne qui voudrait s'impliquer dans l'activité de notre village.

AL : Il y a plusieurs moyens et surtout plusieurs acteurs pour promouvoir le développement de la filière bois. Il faut que la municipalité se rapproche d’associations, d’entreprises et de sociétés dont le métier est de promouvoir et développer la filière (CRPF, FIBOIS, Pro Silva France…). Celles-ci pourront organiser, et conseiller la commune dans la gestion et le développement de sa filière bois.  
Il faut mettre la filière bois en valeur au sein du village en commençant par reconstruire, rénover tout ce qui s’en rapproche (sentier botanique, terrain de jeu…).

GS : Dans un premier temps, c’est la communication qui doit jouer, il faut que nous soyons encouragés par la mairie, que celle-ci soit motivée par le sujet. Il y a plein de collectivités, d’associations qui pourraient donner leurs avis. Peut-être aussi mettre un coup de neuf au village, donner envie aux jeunes de s’investir.

TC : Pour encourager la filière bois, il faut commencer par promouvoir la construction bois. Par exemple poursuivre le projet d'un ''écolotissement bois'' en faisant davantage de publicité. Il faudrait sensibiliser les habitants à rénover ou construire en utilisant des produits issus de cette filière. La commune doit soutenir au maximum les acteurs présents sur le secteur (scierie, ETF, Menuiserie/charpente, constructeur, gestionnaire forestier, ...).

**_Quelles types d’activités, par exemple complémentaire aux vôtres ou sur d’autres domaines de la filière, pourraient s’ouvrir à Lalouvesc ?_**

GB : Je pense qu'il y a plein d'idées vu notre situation géographique pourquoi pas une usine de fabrication de papier ou de fabrication de briquettes pour se chauffer ou même une fabrique d'isolant bois ou une école filière bois à taille humaine ?

AL : Beaucoup d’activités manquent dans le secteur forestier que ce soit en amont ou en aval. Nous, par exemple, nous cherchons des bûcherons toute l’année. Un manque considérable d’ETF (entreprise d’exploitation forestière), se fait ressentir depuis plusieurs années dans la région. Des écoles et MFR (Maison forestière rurale), sont présentes dans la région, mais toutes à une heure de Lalouvesc, et nous ne les connaissons pas forcément. La gestion et l’exploitation, doivent aussi être maîtrisées, trop de coupes rases sont encore réalisées dans des buts commerciaux, et cela détériore nos paysages.  
Je pense aussi que la transformation du bois n’est pas assez poussée, je parle à l’échelle nationale, d’autres pays ont une avance considérable, en matière de gestion et de transformation. Ces pays devraient nous servir d’exemple. L’aide à la création de nouvelles entreprises innovantes dans la transformation du bois devrait être une priorité pour la filière bois française.

GS : L’idée pourrait peut-être être de créer un lycée bois au sein de Lalouvesc qui pourrait regrouper plusieurs métiers (scieur, bûcheron, charpentier, menuisier, ébéniste…). Cette école pourrait permettre d’entretenir nos forêts. Avoir des personnes compétentes pour garder le savoir faire et pouvoir donner une belle image à notre beau village. Je pense qu’il manque aujourd’hui beaucoup de jeunes, l'effectif de l’école est faible, il faut un œil neuf.

TC : Tout type d'activité serait la bienvenue pour redynamiser Lalouvesc. Ce qu'il faut c'est donner envie aux plus jeunes de venir s'installer dans notre village et qu'ils puissent travailler sur place.

**_Comment faudrait-il s’y prendre pour les favoriser ?_**

GB : Déjà tout doit commencer par une propagande de notre village pour attirer des investisseurs qui croient en la capacité des zones rurales avec les moyens de communication que l'on connaît tous.

AL : Pour donner à tous ces futurs entrepreneurs, l’envie de venir investir dans le secteur bois sur la région de Lalouvesc, il faut que la municipalité, parle de la filière. Mais pas que, il faut aussi qu’elle montre qu’elle accompagne ces entreprises, qu'elle les aide en tout point. Et comme le dit Guillaume, il ne faut pas hésiter à employer les moyens publicitaires de notre époque.

GS : Pour favoriser ces projets, il faut déjà être entouré de personnes investies, qui aient une bonne conscience et qui soient motivées. Après il est évident, comme l'ont dit Lionel et Guillaume, qu'il faut être soutenu par la municipalité et faire de la publicité. Je pense aussi que le département doit être investi, développer le Nord Ardèche ne peut être que bon pour l’aspect économique et touristique du village.

TC : Pour favoriser l'implantation de nouvelles entreprises il faut mettre à disposition des locaux ou zones artisanales.  
Il faudrait également travailler au maximum en partenariat les uns avec les autres de l'amont à l'aval de la filière bois.