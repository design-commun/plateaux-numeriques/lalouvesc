+++
date = ""
description = "Un village où on a du caractère !"
header = "/media/ferme-crouzet-1.JPG"
icon = "🦸‍♀️"
subtitle = "Un village où on a du caractère !"
title = "Trois aventures à suivre"
weight = 1

+++
Lalouvesc est un village propice à l'aventure. La preuve ? En voici trois, peu ordinaires poursuivies par des Louvetous, d'origine ou d'adoption, mais qui ont assurément tous trois du caractère.

## Alain et l'homologation de l'Azelle

![](/media/azelle.jpg)

La suite de l'aventure de la 2cv Citroën, la deuch, s'écrit à Lalouvesc, et seulement à Lalouvesc !

En 1987, Alain Le Bihan s'est lancé dans la conception d'un kit en polyester sur une 2CV accidentée de récupération. Les kits ont rencontré un grand succès auprès du public et, aujourd'hui, il en circule 135 à travers l'Europe. Toutes les dénominations commerciales des 2cv commencent par A, type A, type AZ, d'où son nom aérien : AZELLE.  
Avec le durcissement de la réglementation, les contrôles techniques sont plus exigeants et les transformations de véhicules sont interdites. Il a donc fallu passer à l'étape supérieure : devenir un vrai fabricant de voiture !  
À l'étroit dans ses locaux de la région parisienne, Alain s'est installé à Lalouvesc dans les locaux de l'ancienne usine SAMOV, un atelier de 1000 m2 confortable dans un cadre de vie unique et idyllique.  
Début 2021, une nouvelle étape est franchie : l’homologation. Alain Le Bihan sera désormais le seul en France à pouvoir homologuer des 2cv cabriolet Azelle (2cv en kit de sa fabrication) et à pouvoir en construire. Autrement dit pour avoir le droit de rouler, toutes les Azelles doivent passer par ses ateliers.

Ainsi à Lalouvesc, il s'écrit le dernier chapitre de l’histoire de la 2cv.

#### Pour en savoir plus

* [Admirer quelques Azelles sur ce diaporama](https://youtu.be/eRTKoTc_s68).
* [Le site web](https://www.azelle.fr/)

## Thomas et le travail des fustes

Thomas Couette a 27 ans. Il nous raconte.

![](/media/fustes-couette-2.jpeg)

_L’aventure commence en juillet 2016. Après avoir réalisé un BTS Bâtiment, j'ai décidé de faire une formation dans la construction en fustes. Passionné j'ai fait quelques mois dans une entreprise de construction en rondins, puis je me suis mis à mon compte en autoentrepreneur._

_Les premières années, j'ai fait de la sous-traitance en partie pour cette entreprise de fuste et j'ai commencé à vendre un peu de mobilier de mon côté : table en rondins, banc, jardinières… Petit à petit j'ai supprimé la partie sous-traitance et commencé à vendre des petits abris en rondins._

_A partir de janvier 2019, j'ai changé mon statut pour passer en EIRL et j’ai investi plus sérieusement afin de réaliser de véritables maisons en rondins (fustes)._

![](/media/fustes-couette-1.jpeg)

_Aujourd’hui le carnet de commande se remplit et j’ai dû embaucher deux salariés. Mes locaux actuels ne sont plus appropriés à cette évolution, c’est pourquoi je suis à la recherche d’un terrain plus grand afin de développer pleinement mon activité dans le bois._

#### Pour en savoir plus

* [Le site web](https://www.couette-fuste.com/)

## Aline et les Salers du Crouzet

![](/media/ferme-crouzet-2.JPG)

Au hameau Le Crouzet se trouve une des dernières fermes du village de Lalouvesc. La famille Delhomme l’exploite depuis 1922 soit quatre générations. Aline Delhomme, 23 ans, jeune et dynamique exploitante, marchant fièrement sur les pas de son père, vous y accueillera avec plaisir.

La ferme se compose aujourd’hui d’environ 40 mères, principalement de race Salers. Sur une superficie d’environ 80 hectares avec quelques terrains situés sur la Commune et d’autres sur les villages voisins comme Saint Pierre sur Doux par exemple.

La production principale est la vente directe en caissettes de viande de jeunes bovins ou de veaux de lait élevés sous la mère. Leur viande de veau de lait est également disponible à la vente chez le boucher du village lors de la saison estivale.

Le bien-être animal est une priorité sur l’exploitation afin de garantir une viande de qualité : Les animaux non stressés produisent sans aucun doute un meilleur lait et une meilleure viande.

Aline souhaite continuer à proposer des produits de qualité et développer la commercialisation locale et en circuit court.

Vous trouverez plus d’informations et toutes les actualités de la ferme sur sa  [page Facebook]().