---
title: Vie municipale
description: La vie municipale de votre village
subtitle: La vie municipale de votre village
icon: "\U0001F3D8"
header: media/lalouvesc-banniere-4x1-1280w-dithered.jpg
weight: 2
date: 
menu:
  main:
    weight: 1
    parent: Mairie et démarches

---
Élue en juillet 2020, l'équipe municipale s'efforce de mettre en pratique son slogan de campagne : "De toutes nos forces pour Lalouvesc".

Le village s’appuie sur sa situation géographique, sur la recherche de sens et l’engagement citoyen des populations, sur la préservation de l’environnement et sur l’ensemble des métiers de la filière bois pour son développement vers un tourisme adapté aux temps nouveaux.