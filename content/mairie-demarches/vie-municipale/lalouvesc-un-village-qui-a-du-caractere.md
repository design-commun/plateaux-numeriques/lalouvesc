---
date: 
description: ''
header: "/media/plan-commune-lalouvesc.jpg"
icon: "\U0001F3DE️"
subtitle: ''
title: Lalouvesc, un village qui a du caractère
weight: 1
draft: true

---
## Prise de conscience des enjeux planétaires

Le réchauffement climatique et la pandémie du Covid 19, ont grandement accéléré la prise de conscience des enjeux écologiques à la fois chez les politiques et dans la population.

**Un village à 1000m d’altitude, au mode de vie et à la biodiversité largement préservés devient subitement attractif**. Ce changement a été très rapide et déjà perceptible dans la saison estivale 2020 où la fréquentation touristique a été soutenue à Lalouvesc et où le marché de l’immobilier a changé radicalement de perspective.

## Nouvelle équipe à la Mairie et au Sanctuaire Saint Régis

![](/media/rencontre-equipe-du-sanctuaire-et-equipe-municipale.jpg "Rencontre entre l'équipe du Sanctuaire et l'équipe municipale")

L’arrivée d’une nouvelle équipe à la Mairie coïncide avec l’arrivée de nouveaux responsables au Sanctuaire Saint Régis et à l’inscription du village dans l’association des Villes sanctuaires. Les uns comme les autres sont décidés à prendre des initiatives et à investir des moyens, financiers et humains, pour lancer une dynamique nouvelle et coordonnée sur le village.

## Un patrimoine immobilier et paysager à exploiter

![](/media/paysage-lalouvesc.png)

Outre la basilique, le village dispose d’un patrimoine immobilier important, tant du côté communal que religieux ou privé, locaux parfois inoccupés ou en mauvais état. Il existe aussi un camping municipal très couru et d’autres espaces sportifs ou d’agréments (“théâtre de verdure”, grand parc de jeux, terrain de foot). L’ensemble a vieilli faute d’investissements d’entretien.

A ce patrimoine urbain il faut ajouter un magnifique patrimoine paysager, qui pourrait facilement être mieux valorisé.

## Un engagement citoyen fort

![](/media/journee-citoyenne.jpg)

La population est très impliquée dans la vie du village. Les dernières élections municipales, boudées par les Français, y ont eu un taux de participation de 83%. La vie associative y est exceptionnelle. Comité des fêtes, organisant de nombreux événements, Carrefour des arts ou Promenades musicales proposant expositions ou concerts durant l’été et encore manifestations religieuses s’appuient sur de nombreux bénévoles prêts à s’impliquer pour animer le village.

La nouvelle municipalité a organisé au lendemain des élections une matinée citoyenne pour nettoyer une partie du parc de jeux qui a rencontré un franc succès. L’engagement citoyen et la solidarité sont dans les gènes louvetous.