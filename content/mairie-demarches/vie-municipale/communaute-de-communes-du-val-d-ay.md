+++
date = ""
description = ""
header = "/media/ccva.jpg"
icon = "🏘️"
subtitle = ""
title = "Communauté de communes du Val d'Ay"
weight = 5

+++
## Présentation

La Communauté de Communes du Val d'Ay comprend huit Communes : Lalouvesc, Préaux, Saint Alban d'Ay, Saint Jeure d'Ay, Saint Pierre sur Doux, Saint Romain d'Ay, Saint Symphorien de Mahun, Satillieu. Le Conseil communautaire est composé de 26 élus issus des conseils municipaux des communes membres. Lalouvesc y est représenté par son Maire, Jacques Burriez, et son premier adjoint, François Besset.

[Compte-rendus du Conseil sur le site de la CCVA](https://www.val-d-ay.fr/conseil-du-val-day/).

## Responsabilités

### Compétences obligatoires

* Développement économique, recherche, commerce et artisanat
* Gestion de l’espace (agriculture, forêts et paysages)
* Promotion du tourisme dont la création d’office de tourisme et de zones d’activités touristiques
* Gestion des ordures ménagères et des déchets
* Aménagement, entretien et gestion des aires d’accueil des gens du voyage

### Les compétences optionnelles adoptées

* Enfance et jeunesse
* Énergies renouvelables
* Réseaux de télécommunications numériques
* Gestion de la voirie communautaire
* Gestion des chiens errants
* Janvier 2018 : GEMAPI (Gestion des Milieux Aquatiques & Prévention des Inondations

## Représentants de Lalouvesc à la CCVA

* Conseil communautaire : Jacques Burriez, François Besset
* Commission gestion de l’espace : Aline Delhomme
* Commission tourisme : Jacques Burriez (président), Julien Besset
* Commission enfance et jeunesse : Aurélie Desbos
* Commission finance : Michel Bober
* Commission développement économique et durable :
* Commission déchet : François Besset
* Commission voirie : Dominique Balaÿ
* Commission ADN communication : Jean-Michel Salaün
* Commission travaux et bâtiments : François Besset

## Pour en savoir plus

[Site de la CCVA]()

[Retrouvez les actualités de la CCVA  sur Panneau Pocket](https://app.panneaupocket.com/ville/326549431-cc-du-val-day-07290)