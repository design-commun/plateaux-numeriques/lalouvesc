+++
date = ""
description = "Divers comptes-rendus et documents administratifs"
header = ""
icon = "📝"
subtitle = "Divers comptes-rendus et documents administratifs"
title = "Autres documents"
weight = 2

+++
## Comité Vie locale

### Circulation et stationnement

* [Lettre de résidents](/media/lettre-residents-stationnement-rue-de-la-fontaine.pdf) de la rue de la Fontaine et [réponse du Maire](/media/reponse-du-maire-stationnement-rue-de-la-fontaine.pdf)
* [Plan de circulation et de stationnement](/media/plan-de-circulation-et-de-stationnement-juin-2021.pdf), juin 2021

## Comité de développement

* [Comité de pilotage sur l'étude du SCOT, novembre 2022](https://www.lalouvesc.fr/media/ue220210-scot-lalouvesc-copil-01-221114.pdf)
* [Troisième réunion plénière, juin 2022](/media/23-06-2022-comite-de-developpement.pdf)
* [Réunion entre le SCOT et les sœurs du Cénacle (21 juin 2022)](/media/cr_reunion_soeurs_du_cenacle_21062022.pdf)
* [Bon de commande du SCOT Rives du Rhône pour une étude sur le couvent du Cénacle et la Maison Ste Monique, mai 2022](/media/bon_de_commande_cenacle_lalouvesc_vf.pdf)
* [Enjeux de la connexion à Lalouvesc (audit déc 2021)](/media/enjeux-connexion-lalouvesc-2021.pdf)
* [Seconde réunion plénière, novembre 2021](/media/reunion-du-comite-de-developpement-novembre-2021.pdf)
* [Première réunion plénière, juin 2021](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/reunion-du-comite-de-developpement-juin-2021.pdf)

### Dossier Hôtel Beauséjour

* [Avant-projet aménagement espace Beauséjour, novembre 2022](https://www.lalouvesc.fr/media/2022-11-14-lalouvesc-apd-ind-1.pdf)
* [Arrêté préfectoral d'utilité publique](/media/arrete-prefectoral-d-utilite-publique-hotel-beausejour.pdf)
* [Rapport d'enquête d'utilité publique](/media/rapport-enquete-publique-hotel-beausejour.pdf)