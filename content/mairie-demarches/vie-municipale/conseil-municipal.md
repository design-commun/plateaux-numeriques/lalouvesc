---
title: Conseil municipal
description: ''
subtitle: Liste des élus au Conseil municipal
icon: "\U0001F3DB"
date: 
weight: 3
header: "/media/conseil-municipal-juillet-2020-lalouvesc.jpg"
menu:
  main:
    identifier: conseil-municipal
    weight: 3
    parent: Vie municipale

---
* BURRIEZ Jacques **Maire**
* BESSET François 1er adjoint
* SALAÜN Jean-Michel 2ème adjoint
* DESBOS Aurélie 3ème adjoint
* BALAY Dominique Conseiller municipal
* BESSET Julien Conseiller municipal
* BOBER Michel Conseiller municipal
* DELHOMME Aline Conseillère municipale
* GUIRONNET Gérard Conseiller municipale
* PORTE Nicole Conseillère municipale 
* TREBUCHET Christine Conseillère municipale 

### Équipe de juillet 2020

![](/media/installation-du-conseil-municipal-2020.jpg "Installation du Conseil, juillet 2020")