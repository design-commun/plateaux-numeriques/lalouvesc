+++
date = ""
description = "Présentation des employés municipaux"
header = ""
icon = "🦸"
subtitle = ""
title = "Employés municipaux"
weight = 3

+++
{{<grid>}}

{{<bloc>}}

![](/media/gerard-fraysse-2-lalouvesc.jpg)

**FRAYSSE Gérard**
service technique
{{</bloc>}}

{{<bloc>}}
![](/media/placeholder-portrait.png)

**FAURIE Henri**
service technique
{{</bloc>}}

{{<bloc>}}
![](/media/placeholder-portrait.png)

**Balaÿ Dominique**  
service technique

{{</bloc>}}

{{<bloc>}}

![](/media/christian-bruc-2.jpg)

**BRUC Christian**
service technique
{{</bloc>}}

{{<bloc>}}
![Sylvie Deygas](/media/sylvie.jpg)

**DEYGAS Sylvie**
camping - entretien
{{</bloc>}}

{{<bloc>}}
![](/media/placeholder-portrait.png)

**DEYGAS Christian**
camping
{{</bloc>}}

{{<bloc>}}
![Françoise Arentz](/media/francoise.jpg)

**ARENZ-FAURIE Françoise**
secrétariat

{{</bloc>}}

{{<bloc>}}

![](/media/mireille-morel-2.jpg)

**MOREL Mireille**
secrétariat

{{</bloc>}}

{{<bloc>}}
![](/media/placeholder-portrait.png)

**PEREZ Valérie**
secrétariat

{{</bloc>}}{{</grid>}}