+++
date = ""
description = ""
header = ""
icon = "🧠"
subtitle = ""
title = "Commissions et comités"
weight = 4

+++
Les commissions municipales sont réglementairement réservées aux élus. Les comités consultatifs peuvent être ouverts à des habitants du village. Le maire préside de droit commissions et comités mais peut déléguer cette présidence à ses adjoints.

Les deux commissions proposées pourront être opérationnelles dès juillet 2020. Les comités démarreront leur activité à la mi-septembre 2020.

# **1- Commission finances et appels d'offres**

Cette commission est réservée aux membres du Conseil municipal. Elle veille notamment au partage des responsabilités et aux relations avec la Communauté de communes, à la gestion des budgets, à l’examen des principaux devis et à la gestion des appels d’offres.

**Membres :**

* Jacques Burriez (président)
* François Besset
* Michel Bober
* Jean-Michel Salaün

## **2- Commission gestion**

Cette commission réservée aux membres du Conseil municipal. Elle instruit les dossiers concernant la gestion du personnel communal, l’organisation des services publics, l’entretien et l’aménagement des infrastructures et des équipements et du patrimoine communaux (cimetière, routes communales, télécommunication, espaces verts, camping, embellissement, etc).

**Membres**

* Dominique Balay
* François Besset (président)
* Jacques Burriez
* Aurélie Desbos
* Jacky Verger

## **3 - Comité consultatif vie locale et communication**

Ce comité ouvert à des non-élus instruit les dossiers concernant les actions pour la dynamisation de la vie locale, les relations avec les associations, avec la paroisse, avec l’école, la bibliothèque, etc. Il suit les actions en faveur des personnes âgées et des personnes en difficulté (CCAS).

Il s'occupe aussi de la communication interne et (en lien avec la Communauté de communes et l’Office du tourisme) externe du village : presse, affichage, sites web et réseaux sociaux, bulletin municipal et promotion du village et de ses activités.

**Membres parmi les élus :**

* Dominique Balay
* Aline Delhomme
* Aurélie Desbos (présidente)
* Jacky Verger
* Jean-Michel Salaün

Les membres non-élus sont chaque année désignés par le Conseil municipal sur proposition des membres élus du Comité.

## **4 - Comité consultatif sur le développement économique et touristique et urbanisme**

Ce comité, ouvert à des non-élus, instruit les dossiers concernant les projets de développement économique, les aides aux entreprises, aux commerces, artisans, agriculteurs

**Membres parmi les élus :**

* Julien Besset
* Michel Bober
* Jacques Burriez
* Aline Delhomme
* Jean-Michel Salaün (président)

Les membres non-élus sont chaque année désignés par le Conseil municipal sur proposition des membres élus du Comité.