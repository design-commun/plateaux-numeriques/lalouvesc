---
title: Budget et finances
description: ''
subtitle: ''
icon: "\U0001F4CA"
weight: 4
date: 
header: ''
menu:
  main:
    identifier: budget-finances
    weight: 4
    parent: Vie municipale

---
## Budgets primitifs 2021

Il nous a bien fallu une année pour nous roder à l'ensemble des procédures comptables. Nous allons enfin pouvoir mettre en place les indicateurs de suivi de gestion que nous avions proposés avant les élections.

Les comptes 2020 ont été approuvés et le budget primitif 2021 a été voté par le Conseil municipal. Vous trouverez dans le [compte-rendu du Conseil](https://www.lalouvesc.fr/media/2021-2-conseil-municipal-lalouvesc.pdf) tous les détails. Voici ci-dessous quelques éléments saillants.  
Concernant le bilan 2020, par rapport à 2019, les dépenses de fonctionnement ont diminué d’environ 41.300 € essentiellement résultant des économies réalisées grâce à une meilleure maîtrise de nos achats et également des services extérieurs (contrats de prestation de services). A noter que les postes énergie et carburant ont baissé de 24.600 € par rapport à 2019.

Quant aux recettes de fonctionnement, la baisse des recettes réelles de 21.000 € par rapport à 2019 s'explique principalement par un remboursement de salaires et charges de 27.500 € en 2019 qui n'a pas été réitéré en 2020. A noter toutefois le tassement des produits issus des gîtes et du camping de l'ordre de 5.000 € dû à la période de confinement du printemps.

Pour le budget principal 2021, nos prévisions de dépenses sont de 461.828 € contre 401.007€ en 2020. Cette augmentation est principalement due au poste des charges de personnel et frais assimilés.

Nos prévisions de recettes sont de 499.946 € contre 519. 851 € en 2020. Cette diminution est due à la baisse des dotations de l’État que nous prévoyons en l’absence d’informations officielles.

Pour réaliser nos projets de rénovation et de développement, il faut compter sur la constitution de notre autofinancement, l’octroi de tout ou partie des subventions et la mise en place des emprunts. Pour cela, il nous faut poursuivre notre "chasse au gaspi", qui a été efficace en 2020, et compter sur la rénovation du camping pour augmenter nos recettes (croisons les doigts pour que la pandémie ne bride pas nos efforts). Nous devons toujours constituer et suivre de près les dossiers de demande de subvention. Et enfin nous avons prévu et négocié des modalités d'emprunt qui pourront nous permettre d'investir sans grever l'avenir de nos finances. 

## Audit financier (2020)

Un audit financier a été demandé à la direction départementale des finances publiques de l’Ardèche. Réalisé sur une analyse de cinq années de compte (2015-2019), l’audit permet de pointer les forces et les faiblesses financières de la Commune. Son détail peut être consulté à la Mairie. En voici un résumé, accompagné de quelques remarques :  
Sur la période, les produits de fonctionnement (recettes) sont en forte baisse. Cette baisse a pour cause principale les “autres produits”, c’est-à-dire des loyers et du camping. Sur la même période, les charges ont aussi baissé, mais de façon moins importante. Les dépenses d’investissement sont nettement inférieures à celles des communes comparables de la région. L’année 2019, quant à elle, année pré-électorale, se présente comme une année atypique avec une baisse conséquente des charges suite à un différé de travaux d’entretien. Ainsi selon l’étude, la commune en menant une politique de maîtrise de ses charges a pu inverser la tendance à l’érosion de son autofinancement et disposer d’une trésorerie de 180.000 € et d’un financement disponible de 111.200 €.  
Pour l’avenir, les projections réalisées par l’audit à partir de l’évolution des ressources montrent une érosion lente des capacités d’autofinancement. Le principal levier pour renverser la tendance est constitué des recettes du camping. Sans recours à un nouvel emprunt, la Commune ne peut assumer tant les investissements pris en compte par l’audit (travaux Mairie, cimetière, Beauséjour) que ceux destinés à rendre attractif notre village.  
Cet audit nous a rassurés en nous montrant que la situation financière de la Commune n’était pas dégradée. Il nous a aussi confortés dans notre analyse : le prix de cette maîtrise financière a été l’absence d’investissement régulier pour l’entretien du village et le camping reste, pour l’instant, la principale source de l’autofinancement de la Commune.

## Budgets primitifs 2020

Compte tenu de la tenue tardive des élections municipales, le budget primitif 2020 a été voté sur la base du budget 2019 avec simplement quelques ajustements.

[Budgets primitifs 2020](/media/2020-budgets-primitifs-lalouvesc.pdf)