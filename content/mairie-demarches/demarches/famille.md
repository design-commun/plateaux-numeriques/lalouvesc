+++
date = ""
description = "Mariage, union libre, naissance, crèche, écoles, décès"
header = "/media/ecole-st-joseph.jpg"
icon = "👫"
subtitle = "Mariage, union libre, naissance, crèche, écoles, centre aéré, ADMR, EPHAD, décès"
title = "Famille"
weight = 2

+++
## Aides aux démarches administratives et numériques

Si vous rencontrez des difficultés dans vos démarches administratives ou numériques, des personnes sont là pour vous aider dans ce service récemment ouvert :

**France Services de Saint Romain d'Ay**  
40 Place de la Fontaine 07290 SAINT ROMAIN D'AY  
Tel : 04 7 5 69 18 01  
Horaires d’ouverture : 

* Du lundi au jeudi 8h-12h
* Vendredi 8h-12h / 14h-18h

## Mariage

**Chacun des futurs époux doit** :

* avoir au moins 18 ans. Toutefois, une dispense d'âge pour des « motifs graves » (par exemple, grossesse) peut être accordée par le procureur de la République du lieu de célébration du mariage ;
* n'avoir aucun lien de proche parenté ou d'alliance avec le futur conjoint (dans certains cas, une dispense peut être accordée par le Président de la République)
* et ne pas être déjà marié en France ou à l'étranger.

**Un livret "Guide des Futurs époux" est à retirer en mairie. Ce livret comprenant tous les documents à fournir, le calendrier, et des renseignements utiles.**

**Commune de célébration du mariage**

Le mariage est célébré dans la commune où l'un des deux futurs époux a son domicile ou sa résidence établie depuis un mois au moins d'habitation continue à la date de la publication des bans.

* Si le mariage est célébré dans la commune où l'un des futurs époux a son domicile, aucune condition de durée de ce domicile ou d'habitation effective dans ce lieu n'est exigée.
* Si le mariage est célébré dans la commune où l'un des futurs époux n'a qu'une simple résidence, il est nécessaire que cette résidence se manifeste par une habitation continue. Dans ce cas, le futur époux doit y résider pendant le mois qui précède la date à laquelle la publication a été affichée. Cette résidence doit être non interrompue, ni intermittente.

L'officier de l'état civil va s'assurer que le futur époux a des liens durables avec la commune et peut justifier d'une adresse dans le ressort de sa circonscription.

**Lieu de la célébration**

Le mariage doit être célébré à la mairie. Toutefois, des exceptions sont prévues : en cas d'empêchement grave, le procureur de la République pourra demander à l'officier d'état civil de se déplacer au domicile ou à la résidence de l'une des parties pour célébrer le mariage.

[Consulter le détail des conditions de mariage en France](https://www.service-public.fr/particuliers/vosdroits/N142)

## Union libre

Si vous vivez en union libre (avec une personne de sexe différent ou de même sexe), certains organismes peuvent vous attribuer des avantages. Vous aurez alors besoin de prouver que vous vivez en couple et de fournir un certificat de vie commune (ou de concubinage). Ce certificat peut être établi par la mairie.

Les pièces à fournir :

* un justificatif d'identité (carte d'identité, passeport),
* des justificatifs de domicile (quittance de loyer, facture de téléphone ...).
* l'attestation de témoins.

[Consulter le détail des conditions d’ostension d'un certificat de vie commune](https://www.service-public.fr/particuliers/vosdroits/F1433)

## Naissance

La déclaration de naissance est obligatoire pour tout enfant né en France. La déclaration doit être faite, dans la commune du lieu de naissance, dans les 5 jours qui suivent le jour de l'accouchement. La naissance est déclarée par le père, ou à défaut, par le médecin, la sage-femme ou une autre personne qui aura assisté à l'accouchement.

L'acte de naissance est rédigé par le maire ou son adjoint.

[Consulter le détail des conditions de déclaration d'une naissance](https://www.service-public.fr/particuliers/vosdroits/F961)

## Crèche - RAM

La structure Nid d’anges, située à Saint Romain d'Ay, est un établissement d’accueil du jeunes enfants associatif géré par l’ACEPP ADeHL. Elle accueille les enfants de deux mois et demi à six ans du lundi au vendredi de 7h30 à 18h30. La capacité d’accueil est de 22 places. La priorité est donnée aux enfants domiciliés dans la communauté de communes du Val d’Ay.  
Elle est en relation étroite avec le RAM du Val d’Ay mais aussi avec l’ensemble des structures Petit Enfance du Nord Ardèche. (projet commun, temps festifs, échange de malles pédagogiques...).  
Le Relais Assistants Maternels (RAM) du Val d’Ay est un lieu d’échanges et de rencontres ouverts aux parents, aux professionnels de la petite enfance et aux enfants. Pour les nouveaux parents, c’est lieux d’information primordial : information aux parents sur l’ensemble des modes d’accueil du territoire, sur le statut de parent employeur, sur les démarches administratives (embauche, contrat de travail, mensualisation, etc.) ; mise en relation des parents employeurs avec les assistants maternels (liste des assistants maternels agréés pour chaque commune, mise à jour des disponibilités) ; organisation de temps collectifs itinérants, proposant des jeux libres, des activités, pour les assistants maternels et les enfants, et les parents.

Adresse et contact : Espace Jaloine,  380, route de Jaloine,  07290 Saint Romain d’Ay  
Tél : 04 75 69 73 05  [nid.d.anges@orange.fr](mailto:nid.d.anges@orange.fr)

## Inscription à l'école primaire

Les parents peuvent inscrire leur(s) enfant(s) à tout moment. Les élèves sont accueillis à l'école St Joseph à partir de 2 ans même si la scolarisation n'est obligatoire qu'à partir de 3 ans.

Il s'agit d'une école privée sous contrat avec l’État et soutenue par la Commune.

L’école St Joseph regroupe l’école maternelle (de la très petite section à la grande section) et l’école élémentaire (du CP au CM2). Elle correspond aux trois premiers cycles d’enseignement : le cycle des apprentissages premiers (TPS/PS/MS/GS), celui des apprentissages fondamentaux (CP/CE1/CE2) et le cycle de consolidation (CM1/CM2 qui se poursuit en 6ème).

Pour inscrire votre enfant, vous pouvez prendre contact avec la directrice : soit par téléphone (04 75 67 82 51) soit par mail ([ecolestjosephlalouvescp@gmail.com](mailto:ecolestjosephlalouvescp@gmail.com)).

[Plus d'informations sur l'école primaire de Lalouvesc]()

## Centre aéré

Les enfants du centre aéré sont accueillis à l'espace Jaloine (St Romain d'Ay) depuis l'été 2019. Ils seront répartis selon 3 tranches d'âge : fripouilles de 3 à 5 ans, chérubins de 6 à 8 ans et canailles de 9 à 11 ans.

[Plus d'information sur le centre aéré de la Communauté de communes du Val d'Ay](http://www.famillesrurales07.org/valday.html)

La cabane des Marmots 380 Espace Jaloine 07290 Saint Romain d’Ay  
06\.98.89.75.75

## Collèges - Lycées

La Commune dépend de l'académie de Grenoble.

### Collèges

* [Collège privé St Joseph à Satillieu](https://bv.ac-grenoble.fr/carteforpub/uai/0070086F)
* [Collège Les Perrières à Annonay](https://bv.ac-grenoble.fr/carteforpub/uai/0071156U)
* [Collège Louis Jouvet St Agrève](https://bv.ac-grenoble.fr/carteforpub/uai/0070023M)

### Lycées à Annonay

* [Lycée polyvalent Boissy d'Anglas](https://bv.ac-grenoble.fr/carteforpub/uai/0070001N)
* [Lycée agrotechnologique privé d'Annonay](https://bv.ac-grenoble.fr/carteforpub/uai/0071231A)
* [Lycée polyvalent privé Saint-Denis](https://bv.ac-grenoble.fr/carteforpub/uai/0071126L)
* [Lycée professionnel J et E de Montgolfier](https://bv.ac-grenoble.fr/carteforpub/uai/0070002P)
* [Lycée professionnel privé Marc Seguin](https://bv.ac-grenoble.fr/carteforpub/uai/0070100W)

Toutes [les informations sur les transports scolaires](https://www.auvergnerhonealpes.fr/285-ardeche.htm)

## Aide à domicile (ADMR)

L’ADMR propose une large gamme de services à la personne pour tous les besoins :

* Garde d’enfants à domicile.
* Livraisons de repas.
* Ménage - Repassage.
* Services pour personnes en situation de handicap.
* Services pour séniors.

[Site web](https://www.admr-ardeche.fr/)  
112 Avenue Ferdinand Janvier  
07100 Annonay  
[nord.ardeche.rhone@fede07.admr.org](mailto:nord.ardeche.rhone@fede07.admr.org)  
04 75 33 49 28

## Ehpad

L'Ehpad le Balcon des Alpes, situé sur la Commune, accueille les personnes âgées dépendantes.

Pour toutes informations et démarches concernant ce service voir le [portail dédié](https://www.pour-les-personnes-agees.gouv.fr/annuaire-ehpad-et-maisons-de-retraite/EHPAD/ARDECHE-07/lalouvesc-07520/ehpad-le-balcon-des-alpes/070780531).

## Déclaration de décès

La déclaration doit être faite, à la mairie, dans les 24 heures de la constatation du décès, hors week-ends et jours fériés.

Pour déclarer le décès, la personne chargée de faire la déclaration doit présenter les documents suivants :

* une pièce prouvant son identité,
* le certificat de décès délivré par le médecin, le commissariat de police ou la gendarmerie,
* toute autre pièce concernant le défunt : livret de famille, carte d'identité, acte de naissance ou de mariage, passeport, etc.

À la suite de la déclaration de décès, la mairie établit un acte de décès.

## Pour en savoir plus

Consulter le site officiel de l'administration française concernant les démarches liées à la famille.

[https://www.service-public.fr/particuliers/vosdroits/N19805](https://www.service-public.fr/particuliers/vosdroits/N19805 "https://www.service-public.fr/particuliers/vosdroits/N19805")