+++
date = ""
description = ""
header = "/media/mairie-lalouvesc-2.jpg"
icon = "⏲️"
subtitle = ""
title = "Adresse, horaires, météo, plans, abonnement"
weight = 1
[menu.direct_access]
weight = 1

+++
{{<grid>}}{{<bloc>}}

## Mairie, Agence postale communale et Bibliothèque

Rue des Cévennes  
07520 Lalouvesc  
Tél mairie : [04 75 67 83 67](tel:+330475678367)  
email : [mairie@lalouvesc.fr](mailto:mairie@lalouvesc.fr)

{{</bloc>}}{{<bloc>}}

## Camping municipal du Pré du Moulin

241 Chemin de l'Hermuzière  
07520 Lalouvesc  
Tél : 04 75 67 84 86  
email : [camping.lalouvesc@orange.fr](mailto:camping.lalouvesc@orange.fr)

{{</bloc>}}{{<bloc>}}

## Horaires d’ouverture de la Mairie

* Lundi, Mardi, Jeudi : 10h - 12h15
* Mercredi, Vendredi : 14h - 16h30
* Fermeture exceptionnelle du 25 décembre au 2 janvier

{{</bloc>}}{{<bloc>}}

## Horaires d’ouverture de la Poste

* Lundi, Mercredi, Jeudi, Vendredi : 10h - 12h15
* Mardi : 14h - 16h
* Fermeture exceptionnelle du 25 décembre au 2 janvier

{{</bloc>}}{{<bloc>}}

## Horaires d’ouverture de la Bibliothèque

* Mercredi : 15h - 17h
* Samedi : 9h30 - 11h30

{{</bloc>}}{{<bloc>}}

## Horaires d’ouverture du bureau du camping

* Mai-juin et septembre-octobre : 9h - 12h et 17h - 19h
* Juillet-août : 9h - 12h et 15h - 19h

{{</bloc>}}{{<bloc>}}

## Météo

<iframe id="widget_autocomplete_prev_w"  width="250" height="300" frameborder="0" src="https://meteofrance.com/widget/prevision/071280##6BB2CC"> </iframe>

{{</bloc>}}{{<bloc>}}

## Plans

Retrouvez tous les plans du village et de ses accès sur [cette page](/vivre-a-lalouvesc/histoire-identite/plans-cartes/).

{{</bloc>}} {{</grid>}}

## Abonnement

Pour recevoir un message tous les lundis rappelant les actualités de la semaine parues sur le site, envoyez simplement à partir de votre adresse mail un message à l'adresse : sympa@numerian.fr avec dans l'objet du message la formule : **subscribe actualites-lalouvesc**. Ne mettez rien dans dans votre message, c'est inutile.

Vous recevrez en retour un message pour vérifier que vous n'êtes pas un robot. Une fois la vérification faite, vous serez abonnés.

Il y a déjà plus de 330 abonnés, n'hésitez pas à les rejoindre. Vous pourrez vous désabonner à tout moment et cette liste ne sert et ne servira qu'à envoyer les actualités du site.