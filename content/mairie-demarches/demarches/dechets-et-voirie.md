+++
date = ""
description = ""
header = ""
icon = "♻️"
subtitle = ""
title = "Déchets et voirie"
weight = 6

+++
## Organisation

La gestion des déchets ménagers est réalisée par le [Syndicat de traitement des déchets Ardèche-Drôme (SYTRAD)](https://www.sytrad.fr/).

## Tri

Il est très important de suivre les consignes ci-dessous. Doivent être triés :

* Les emballages plastiques : 100 % des emballages plastiques. Exemples : bouteilles et flacons en plastique, films, sacs et sachets, pots, boîtes, barquettes, tubes ;
* Les emballages métalliques : 100 % des emballages métalliques. Exemples : boîtes de conserve, barquettes en métal, aérosols non-dangereux, bidons de sirop, canettes, gourdes de compote, tubes, capsules de café, feuilles d’aluminium froissées, opercules, couvercles et capsules, plaquettes de médicaments vidées.

Les recommandations habituelles restent inchangées :  
\- JETER les emballages en VRAC dans la colonne de tri jaune  
\- NE PAS LAVER les emballages, simplement bien les vider  
\- NE PAS IMBRIQUER les emballages les uns dans les autres

Pour les papiers/cartons blancs, pas de changement : les papiers et cartonnettes dans la colonne de tri bleu clair.

Les cartons bruns doivent être aplatis et déposés dans la colonne bleu foncé.

Le verre, pas de changement : Le verre dans la colonne de tri vert.

{{<grid>}}{{<bloc>}}

![](/media/tri-dechet-1-2021.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/tri-dechet-2-2021.jpg)

{{</bloc>}}{{</grid>}}

Le reste des ordures ménagères (sauf produits encombrants ou dangereux à déposer à la déchèterie, ou produits compostables) doit être réuni dans un sac en plastique et déposé dans la colonne grise.

![](/media/bacs-de-tri-ordures-2022.png)

## Composteurs

La plupart des déchets organiques peuvent être recyclés sous forme de compostage

Vous pouvez vous procurer un composteur pour la somme de 35€ auprès du SIRCTOM. Celui-ci propose aussi un livret pour apprendre à réaliser un bon compostage.

[Tous les informations](http://www.sirctom.fr/actualites/vente-de-composteurs-au-sirctom-des-le-05-01-21-58).

## Déchèterie

La Communauté de communes a ouvert une déchèterie. Pensez à vous venir d'une pièce d'identité et d'un justificatif de domicile avant votre venue.

Les déchets qui peuvent être déposés sont :

* encombrants non valorisables (matelas, salon de jardin) ;
* encombrants métalliques (sommiers...) ;
* gros électroménagers (congélateurs, réfrigérateurs, fours...) ;
* petits appareils ménagers (fers à repasser, robots...) ;
* écrans (TV, ordinateurs...) ;
* déchets dangereux des ménages (peintures, solvants, aérosols...) ;
* huile de vidange, gravats (déchets inertes : matériaux issus de bricolage, plâtre, carrelage, lavabo, toilettes...) ;
* déchets verts (tontes de pelouses, tailles de haie...) ;
* pneus (véhicules légers uniquement et déjantés) ;
* bois (palette bois, placards, meubles bois ...) ;
* lampes à économie d'énergie, néons, piles ;
* DASRI (piquants : type seringues diabétiques) ;
* cartouches d'encres ;
* batteries ;
* textiles (vêtements, maroquinerie, chaussures...) ;
* papiers cartons (journaux revues, magazines ...) ;
* plastiques ;
* boites de conserves, verre et capsules Nespresso

Quartier Empête  
160 chemin des Chênes  
07790 St Alban d’Ay  
Tél 04 75 69 90 50
Ouvert le lundi, mardi, vendredi et samedi de 9h à 12h et de 13h à 16h sauf jours fériés.