+++
date = ""
description = "Etat civil, authentification, élections, recensement"
header = "/media/elections.jpg"
icon = "🙋"
subtitle = ""
title = "Papiers - Citoyenneté"
weight = 1

+++
## Photocopies, wi-fi, consultation web

Des photocopies sont possibles au secrétariat de Mairie (25c la page). Un wi-fi en libre accès est proposé dans un rayon de 50m autour de la Mairie. Un poste de consultation internet en libre service est disponible à l'Agence postale communale.

## Aides aux démarches administratives et numériques

Si vous rencontrez des difficultés dans vos démarches administratives ou numériques, des personnes sont là pour vous aider dans ce service récemment ouvert :

**France Services de Saint Romain d'Ay**  
40 Place de la Fontaine 07290 SAINT ROMAIN D'AY  
Tel : 04 7 5 69 18 01  
Horaires d’ouverture : 

* Du lundi au jeudi 8h-12h
* Vendredi 8h-12h / 14h-18h

## État civil - Identité

* **Demande de carte nationale d'identité** : La mairie de Lalouvesc n’est pas équipée pour délivrer une carte d'identité. Votre présence étant exigée lors du dépôt de la demande pour procéder à la prise d'empreintes, vous devez vous rendre dans un guichet équipé pour recevoir les demandes de carte d'identité :  
  Le guichet le plus proche de Lalouvesc est celui de la [Maison des services publics à Annonay](https://www.mairie-annonay.fr/Maison-des-Services-Publics-MSP.html).
* Vous pouvez préparer la démarche en effectuant une pré-demande en ligne en cliquant sur le lien suivant :[  
  Pré-demande de carte d'identité  
  ](https://www.service-public.fr/particuliers/vosdroits/R45668)La carte d'identité d'une personne majeure est valable 15 ans, celle d'un enfant mineur est valable 10 ans.​  
  [Consulter la liste des justificatifs nécessaires à la demande](https://www.service-public.fr/particuliers/vosdroits/N358)

{{<grid>}}{{<bloc>}}

![](/media/passeports-ardeche.JPG)

{{</bloc>}}{{<bloc>}}

* **Demande de passeport** : La mairie de Lalouvesc ne peut pas délivrer les nouveaux passeports biométriques.  
  Pour obtenir un passeport biométrique, il faut se rendre dans une mairie équipée avec les pièces justificatives nécessaires (...). Les documents dépendent de la situation : majeur ou mineur, première demande ou renouvellement, possession (ou non) d'une carte d'identité sécurisée.  
  Le guichet le plus proche de Lalouvesc est celui de la [Maison des services publics à Annonay](https://www.mairie-annonay.fr/Maison-des-Services-Publics-MSP.html).  
  Vous pouvez préparer la démarche en effectuant une pré-demande en ligne en cliquant sur le lien suivant :[  
  Pré-demande de passeport  
  ](https://passeport.ants.gouv.fr/Vos-demarches/Realiser-une-pre-demande-de-passeport)[Consulter la liste des justificatifs nécessaires à la demande](https://www.service-public.fr/particuliers/vosdroits/N360)

{{</bloc>}}{{</grid>}}

* **Autorisation de sortie de territoire** : Un enfant mineur qui vit en France et voyage à l'étranger doit avoir une autorisation de sortie du territoire (AST) s'il n'est pas accompagné par un[ responsable légal](https://www.service-public.fr/particuliers/glossaire/R52001). Un enfant voyageant seulement avec son père ou seulement sa mère n'en a donc pas besoin.  
  La demande s’effectue en ligne sans déplacement à la mairie.  
  [Formulaire en ligne](https://www.service-public.fr/particuliers/vosdroits/F1359)
* **Livret de famille, acte de naissance, de mariage et de décès** : Certaines démarches nécessitent la production d'actes d'état civil (acte de naissance, de mariage, de décès) ou du livret de famille. Les fiches d'état civil n'existent plus depuis fin 2000.  
  Toutes les demandes doivent se faire à l'accueil de la mairie ou par courrier. La mairie de Lalouvesc n'est pas en mesure de recevoir des demandes d'acte d'état civil de manière dématérialisée.  
  [Consulter la liste des justificatifs nécessaires à la demande](https://www.service-public.fr/particuliers/vosdroits/N359)

***

## Authentification

* **Copie certifiée conforme** : La copie d'un document français destinée à une administration française n'a pas besoin d'être certifiée conforme. La copie certifiée conforme peut être exigée uniquement pour les documents français destinés à des administrations étrangères.  
  Demande à faire à l'accueil de la mairie
* **Légalisation de signature** : La légalisation d'une signature sert à authentifier votre propre signature lorsqu'elle est apposée sur des actes sous seing privé. La procédure sert à vérifier que vous êtes bien la personne concernée par le document.  
  Vous devez vous adresser à la mairie et présenter : la pièce à légaliser, si le document est en langue étrangère, il doit être accompagné d'une traduction en français, une pièce d'identité sur laquelle figure votre signature, et, éventuellement, un justificatif de domicile.  
  À défaut de pièce d'identité, vous devez être accompagné de 2 personnes témoins, munies de leurs pièces d'identité et d'un justificatif de domicile.  
  L'authentification de votre signature se fait obligatoirement en votre présence. Vous devrez signer au guichet devant l'élu habilité.  
  Pensez à faire la demander un peu à l'avance en prenant rendez-vous.
* **Papiers perdus** : En cas de perte de papiers officiels, il vous faudra effectuer votre déclaration de perte auprès de la gendarmerie. Les services de la mairie pourront vous produire une attestation de perte de papier d'identité. **Il est utile de photocopier vos papiers importants** (CNI, passeport, carte vitale, permis de conduire, carte grise, etc.). Ces photocopies vous aideront dans vos démarches.
* **Permis de conduire, carte grise** : Les demandes de duplicata de permis de conduire et de certificat d'immatriculation (ex carte grise) ne peuvent être réalisée qu'en ligne.  
  Attention, il n'est désormais plus possible de demander une carte grise auprès de la préfecture ou de la sous-préfecture.  
  En cas de perte du permis de conduire, il faut effectuer une déclaration de perte.  
  [Consulter le site concernant ces documents](https://www.service-public.fr/particuliers/vosdroits/N19812)

Pour effectuer les démarches en ligne, vous devez avoir accès :

* à un équipement numérique (ordinateur, tablette, smartphone) muni d'une connexion internet  
  \- et à un dispositif de copie numérique (scanner, appareil photo numérique, smartphone ou tablette équipé d'une fonction photo).

Ces équipements sont à votre disposition à la mairie si vous n’en disposez pas.

***

## Élections

**Pour participer aux élections politiques**, il faut être inscrit sur les listes électorales. L'inscription est automatique pour les jeunes de 18 ans. Si vous vous trouvez dans une autre situation (déménagement, première inscription...), vous devez prendre l'initiative de la demande.

Il est possible de s'inscrire toute l'année. Toutefois, lors d'une année d'élection, il faut faire cette démarche au plus tard le 6e vendredi précédant le 1er tour de l'élection. Dans [certaines situations](https://www.service-public.fr/particuliers/vosdroits/F34240), ce délai est allongé jusqu'au 10e jour précédant le 1er tour de l'élection.

Vous pouvez à tout moment [vérifier votre inscription sur les listes électorales et votre bureau de vote](https://www.service-public.fr/particuliers/vosdroits/R51788).

**L'inscription peut se faire** [**en ligne**](https://www.service-public.fr/particuliers/vosdroits/R16396) **ou à la mairie**. [formulaire de demande d'inscription](https://www.service-public.fr/particuliers/vosdroits/R16024) sur les listes électorales. Pièces demandées en complément : carte d'identité en cours de validité, justificatif de domicile.

**Les citoyens européens** peuvent aussi demander à être inscrits sur les listes électorales municipales (élections municipales uniquement) et européennes (élections européennes).

#### Vote par procuration

Si vous ne pouvez vous déplacer le jour du vote et que vous souhaitez que quelqu'un, muni d'une procuration, vote à votre place, ne vous y prenez pas au dernier moment pour que la Mairie ait le temps de recevoir la procuration autorisant le vote de la personne qui vous remplace.

Vous pouvez faire la [demande en ligne](https://www.service-public.fr/particuliers/vosdroits/F35802/0_0_0?idFicheParent=F1604#0_0_0) ou récupérer en ligne [un formulaire](https://www.service-public.fr/particuliers/vosdroits/F34852/3_0_0_1?idFicheParent=N47#3_0_0_1) pour le remplir chez vous ou encore aller le chercher dans un commissariat ou une gendarmerie.  
Dans tous les cas, il vous faudra ensuite vous déplacer en personne dans un commissariat de police ou une gendarmerie avec une pièce d'identité pour enregistrer la procuration.

[Plus d'informations](https://www.service-public.fr/particuliers/vosdroits/N47#3_0_0_1).

***

## Recensement

**Recensement militaire ou citoyen** : À faire dans les 3 mois suivant l'anniversaire des 16 ans, le jeune est prié de venir, accompagné d'un de ses parents, avec un justificatif de domicile et une pièce d'identité. Formulaire à compléter en mairie

[Consulter le détail de la procédure](https://www.service-public.fr/particuliers/vosdroits/F870)

***

## Pour en savoir plus sur l'ensemble des démarches administratives

Consulter le site officiel de l'administration française - rubrique "Papiers - Citoyenneté".

[https://www.service-public.fr/particuliers/vosdroits/N19810](https://www.service-public.fr/particuliers/vosdroits/N19810 "https://www.service-public.fr/particuliers/vosdroits/N19810")