+++
date = ""
description = "Abonnement, obligations "
header = "/media/source-du-perrier2.jpg"
icon = "🚰"
subtitle = ""
title = "Eau & assainissement"
weight = 4

+++
## Eau

### Qualité de l'eau

La qualité de l'eau est vérifiée régulièrement par l'ARS en différents points du village.  
L'eau du village a certaines caractéristiques sans incidence sur la santé : pH un peu faible, forte dureté de l'eau (équilibre calcocarbonique), conductivité faible (faible minéralité).

La qualité de l'eau est publiée en temps réel par l'ARS, commune par commune. On peut y avoir accès en ligne par [ce site](https://orobnat.sante.gouv.fr/orobnat/afficherPage.do?methode=menu&usd=AEP&idRegion=84) (entrer le nom du département et la commune).

### Maisons raccordées au réseau communal d'eau potable

La mairie est en charge de la gestion de l’eau du réseau communal. Chaque année les compteurs sont relevés par un agent communal courant du mois d’Août. Ce relevé est obligatoire une fois par an afin d'établir la facture de consommation d'eau.

Attention seuls les compteurs municipaux sont relevés. Les compteurs particuliers mis en place par les propriétaires pour leur location ne sont pas relevés. Les propriétaires doivent gérer eux-mêmes la répartition des charges dues à la consommation d'eau de leurs locataires.

Les abonnés ont la possibilité de déclarer eux-mêmes leur consommation en [remplissant directement en ligne ce formulaire](https://forms.gle/JpjJrfECf4cmm2XW6), accompagné de l'envoi d'une photo de leur compteur présentant clairement les chiffres de consultation à eau@lalouvesc.fr. Le formulaire peut aussi être retiré sur papier à la mairie et retourné par la poste.

Si vous ne souhaitez pas utiliser cette commodité, merci de faciliter l'accès au compteur d'eau et de réserver le meilleur accueil à l'agent communal.

**Attention, faute d'avoir effectué l'une de ces démarches avant le 16 septembre, votre facture d'eau sera établie sur la consommation de l'année précédente augmentée de 20%.**

### Maisons non-raccordées

Les propriétaires des maisons non-raccordée au réseau d'eau doivent gérer eux-mêmes leur source.

Un forage réalisé à des fins d’usage domestique fait l’objet d’une déclaration auprès du maire, selon une procédure autonome régie par l’article [L.2224-9 du Code général des collectivités territoriales](http://www.legifrance.gouv.fr/affichCodeArticle.do?idArticle=LEGIARTI000022494734&cidTexte=LEGITEXT000006070633&dateTexte=20131202&oldAction=rechCodeArticle "Le texte sur Légifrance"). Dès lors que la construction ne figure pas dans une zone desservie par le réseau de distribution d’eau potable définie par le schéma, la collectivité n’a pas d’obligation de raccordement.

### Récupération de l'eau de pluie

Vous êtes autorisé et il est recommandé de récupérer l'eau de pluie pour les eaux des toilettes, le nettoyage, l'arrosage.  
La récupération des eaux de pluie suppose des équipements et des règles particulières. Pour tout savoir : la [page dédiée]() sur le Ministère de la Cohésion des territoires.

## Assainissement

### Réseau de collecte des eaux usées

Le village dispose d'une station d'épuration et d'un réseau d'assainissement.  
Le réseau d'assainissement ne couvre pas encore la totalité du village. Des travaux sont prévus pour le compléter. Ils s'étaleront sur plusieurs années.

### Maisons raccordées ou raccordables au réseau

Toutes les habitations situées en bordure du réseau ont l'obligation d'être raccordées.

Quand un réseau d'assainissement collectif est mis en place dans votre commune, vous avez 2 ans pour raccorder un bâtiment existant. Ce délai démarre à partir de la mise en service du réseau public.

Si le logement est construit après la mise en service du réseau communal d'assainissement, le raccordement doit être réalisé lors des travaux de construction du logement.

Si le logement n'est pas raccordé ou si le raccordement n'est pas conforme, la mairie peut faire réaliser d'office le raccordement ou les travaux de réhabilitation aux frais du propriétaire.

**Toutes les habitations raccordées ou raccordables paient une redevance d'assainissement collectif.**

### Maison non raccordables au réseau

La loi impose aux Communes françaises la mise en place d’un SPANC (Service Public d’Assainissement Non Collectif) afin de préserver, voir restaurer la qualité des eaux. Le SPANC est un service public obligatoire et fait l’objet d’un budget équilibré : les dépenses du service (véhicule, salaire du technicien…) sont compensées par la mise en place d’une redevance payable par les usagers.

La commune de Lalouvesc dépend du SPANC du Syndicat mixte Ay-Ozon. Toutes les informations sur [Syndicat mixte Ay-Ozon](http://rivieres-ay-ozon.fr/spanc.html) .

### Pour en savoir plus

Sur les questions liées à l'assainissement : [la page du Ministère de la Cohésion des territoires](https://www.cohesion-territoires.gouv.fr/assainissement-des-eaux-usees-domestiques).

un [chapitre de l'étude du PLU](/media/plu-piece-1-rdp-tome1-lalouvesc-appobation-juillet-2018.pdf) est consacré aux ressources en eaux du village.