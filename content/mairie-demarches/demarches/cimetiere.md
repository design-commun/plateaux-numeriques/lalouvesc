+++
date = ""
description = "Concessions, entretien"
header = "/media/cimetiere-lalouvesc.jpg"
icon = "✝️"
subtitle = ""
title = "Cimetière"
weight = 5

+++
Le cimetière communal de Lalouvesc est situé Boulevard des élégants sur la route de Saint Félicien. Sur les flancs du Mont Besset, il domine les Alpes.

Il est composé d’une partie ancienne et d’une partie plus récente (1998-1999). Les caveaux “placards”, avec portes en bois, constituent une originalité de ce cimetière.

Voir la photographie des tombes sur [un site de généalogistes](https://www.geneanet.org/cimetieres/collection/28165-cimetiere-communal-de-lalouvesc?page=1).

## Concessions

Il existe deux types de concessions :

* à perpétuité jusqu’à épuisement des héritiers directs ;
* pour un certain nombre d’années renouvelables.

La location d’une concession est effectuée à la mairie avec paiement au Trésor Public. Aucun particulier n’est propriétaire d'une concession et il est interdit de céder son emplacement. seule la commune peut “récupérer” celui-ci avec courrier du cédant.

Depuis une petite décennie les concessions vacantes ont été reprises, nettoyées et redistribuées. Les habitants de Lalouvesc sont prioritaires et chaque demande est étudiée au cas par cas.

## Entretien

L’entretien des tombes est à la charge des familles. Tout travail exécuté sur les concessions (pierre tombale, caveau, monument) est soumis à une autorisation de la mairie avant travaux.

La mairie prend en charge l’entretien général du cimetière.

D’importants travaux de réparation sont prévus en 2021.