+++
date = ""
description = "PLU, cadastre, certificat, permis de construire, déclaration de travaux"
header = "/media/ecolotissemet-plan.jpg"
icon = "🏠"
subtitle = ""
title = "Urbanisme"
weight = 3

+++
## Plan local d’urbanisme

Les règles d’urbanisme sur la Commune de Lalouvesc sont définies par un Plan Local d’Urbanisme (PLU) approuvé en 2018.

Celui-ci est consultable en ligne ci-dessous ou à la mairie. Il comprend les documents suivants (pdf) :

* [Documents administratifs](/media/plu-piece-1-rdp-tome2-lalouvesc-approbation-du-projet-juillet-2018.pdf)
* Rapport de présentation :

1. [Diagnostic et état initial de l'environnement](/media/plu-piece-1-rdp-tome1-lalouvesc-appobation-juillet-2018.pdf)
2. [Justification du projet](/media/plu-piece-1-rdp-tome2-lalouvesc-approbation-du-projet-juillet-2018.pdf)

* [Projet d'aménagement et de développement durables](/media/plu-piece-2-padd-lalouvesc-approbation-projet-plu-juillet-2018-1.pdf)
* [Orientations d'aménagement](/media/plu-piece-3-orientation-d-amenagement-et-de-programmation-lalouvesc-approbation-projet-plu-juillet-2018.pdf)
* [Plan de zonage](/media/plu-piece-4a-plan-de-zonage-approbation-projet-plu-juillet-2018.pdf) et [cahier des emplacements réservés](/media/plu-piece-4b-emplacements-reserves-approbation-projet-plu-juillet-2018.pdf)
* [Règlement](/media/plu-piece-5-reglement-ecrit-lalouvesc-approbation-projet-plu-juillet-2018.pdf)
* [Annexe](/media/plu-piece-6-annexes-lalouvesc-approbation-projet-plu-juillet-2018.pdf)

***

## Cadastre

Le cadastre est l'ensemble des documents qui recensent et évaluent les propriétés foncières de chaque commune. Il sert de base pour le calcul des impôts locaux.

[Informations sur la consultation du cadastre](https://www.service-public.fr/particuliers/vosdroits/F14226).

Le cadastre de la commune est consultable à la mairie. Des extraits de la matrice cadastrale sont produits à la demande.  
Une version du cadastre est aussi [consultable en ligne](https://www.cadastre.gouv.fr/scpc/afficherCarteCommune.do?CSRF_TOKEN=Y17E-XR4O-RBDH-KDX2-S004-B1L4-P7V2-MBJX&c=61128&dontSaveLastForward&keepVolatileSession=)

Le site Géoportail de l’IGN fournit une vue aérienne du cadastre et la possibilité de visionner l’évolution du village selon les années.

[Consulter la page Lalouvesc du site Géoportail](https://www.geoportail.gouv.fr/donnees/parcelles-cadastrales-trait-orange).

A voir aussi la [carte interactive](https://app.dvf.etalab.gouv.fr/) permettant de visualiser les ventes des dernières années..

## Adresses

La commune a précisé récemment l'ensemble des noms de lieux et des adresses, consultable sur la [base nationale des adresses](https://adresse.data.gouv.fr/base-adresse-nationale/07128#11.9/45.117/4.5215/0/2).

***

## Certificat d'urbanisme

Le certificat d'urbanisme est un document qui indique les règles d'urbanisme applicables sur un terrain donné et permet de savoir si l'opération immobilière projetée est réalisable. Il existe 2 types de certificat d'urbanisme. Sa délivrance n'est pas obligatoire, mais il est toutefois recommandé d'en faire la demande avant d'engager la réalisation du projet.

Le dossier (formulaire et pièces à fournir) doit être envoyé ou déposé à la mairie de la commune où est situé le terrain :

* en 2 exemplaires pour les demandes de certificat d'urbanisme d'information,
* en 4 exemplaires pour les demandes de certificat d'urbanisme opérationnel.
* avec 1 exemplaire supplémentaire, si le projet se situe en périmètre protégé au titre des monuments historiques,
* avec 2 exemplaires supplémentaires, si le projet est situé dans un cœur de parc national.

La mairie affecte un numéro d'enregistrement à la demande.

Pour traiter la demande, la mairie dispose d'un délai de :

* 1 mois pour une demande de certificat d'urbanisme d'information,
* 2 mois pour une demande de certificat d'urbanisme opérationnel.

[Consulter les informations détaillées et télécharger le formulaire de demande](https://www.service-public.fr/particuliers/vosdroits/F1633)

***

## Déclaration préalable de travaux

Une déclaration préalable de travaux est obligatoire notamment dans les cas suivants :

* construction (garage, dépendance...) ou travaux sur une construction existante ayant pour résultat la création d'une surface de plancher ou d'une emprise au sol comprise entre 5 m² et 20 m². En ce qui concerne les travaux sur construction existante, ce seuil est porté à 40 m² si la construction est située dans une zone urbaine couverte par un plan local d'urbanisme (PLU) ou un plan d'occupation des sols (POS) ;
* construction d'un mur d'une hauteur au-dessus du sol supérieure ou égale à 2 m ;
* construction d'une piscine dont le bassin a une superficie inférieure ou égale à 100 m² non couverte ou dont la couverture (fixe ou mobile) a une hauteur au-dessus du sol inférieure à 1,80 m ;
* travaux modifiant l'aspect initial extérieur d'une construction (remplacement d'une fenêtre ou porte par un autre modèle, percement d'une nouvelle fenêtre, choix d'une nouvelle couleur de peinture pour la façade) ;
* remplacement de la couverture d’un bâtiment ;
* travaux de ravalement s'ils se déroulent dans un espace faisant l'objet d'une protection particulière (par exemple, abord d'un monument historique) ;
* changement de destination d'un local (par exemple, transformation d'un local commercial en local d'habitation) sans modification des structures porteuses ou de la façade du bâtiment ;
* réalisation d'une division foncière notamment pour en détacher un ou plusieurs lots.

Le dossier de demande préalable de travaux doit être remis à la mairie en deux exemplaires. À cette occasion, la mairie vous délivre (ou vous envoie) un récépissé comportant un numéro d'enregistrement qui mentionne le point de départ de la date à partir de laquelle les travaux pourront commencer en l'absence d'opposition du service instructeur.

Le délai d'instruction est généralement de un mois à partir de la date du dépôt de la demande.

[Consulter les informations détaillées et télécharger le formulaire de déclaration](https://www.service-public.fr/particuliers/vosdroits/F17578)

***

## Permis de construire

Un permis de construire est généralement exigé pour tous les travaux de grande ampleur.

* Pour les constructions nouvelles indépendantes de tout bâtiment existant à l'exception :
  * des constructions dispensées de toute formalité comme les piscines de moins de 10 m² ou les abris de jardin de moins de 5 m²,
  * et de celles qui doivent faire l'objet d'une déclaration préalable.
* Pour les travaux sur une construction existante concernent par exemple l'agrandissement d'une maison.

Un permis est nécessaire :

* si les travaux ajoutent une surface de plancher ou une emprise au sol supérieure à 40 m² ;
* ou s'ils ajoutent entre 20 et 40 m² de surface de plancher ou d'emprise au sol et ont pour effet de porter la surface totale de la construction au-delà de 170 m².

Votre demande de permis de construire doit être déposée en mairie. Le délai d'instruction du dossier est de 2 mois.

[Consulter les informations détaillées et télécharger les formulaires de demande](https://www.service-public.fr/particuliers/vosdroits/F1986)

![](/media/quelles-autorisations-pour-quels-travaux.jpg)

***

## Pour en savoir plus...

Consulter le site officiel des services publics, rubrique logement ...

[https://www.service-public.fr/particuliers/vosdroits/N19808](https://www.service-public.fr/particuliers/vosdroits/N19808 "https://www.service-public.fr/particuliers/vosdroits/N19808")

et le Ministère de la Cohésion des territoires [Autorisations d'urbanisme](https://www.cohesion-territoires.gouv.fr/autorisations-durbanisme).