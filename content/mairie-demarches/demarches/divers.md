+++
date = ""
description = "Objets trouvés, location de salles, ruches, frelons..."
header = ""
icon = "🏷️"
subtitle = "Objets trouvés,location de salles, débroussaillage, élagage, haies, chiens et chats,  ruches, frelons.."
title = "Divers"
weight = 7

+++
## Objets trouvés

Prière de ramener à la Mairie tout objet trouvé sur la voie publique. Une publicité sera faite pour que son propriétaire puisse retrouver son bien.

## Location de salles

La municipalité propose à la location plusieurs salles disponibles pour des réunions ou rassemblements divers. Les tarifs varient selon le type de demandeurs et l'objectif de la location.

#### Salle du théâtre au centre d'animation communal (CAC)

* Associations communales : 75 € / jour (gratuité pour Assemblées générales et réunions statutaires)
* Résidents de la commune : 180 € / jour
* Non résidents : 35O € / jour

#### Salle du 3e âge (CAC)

* Associations communales : 75 € / jour (gratuité pour Assemblées générales et réunions statutaires)
* Résidents de la commune : 75 € / jour

#### Salle du camping

* Associations communales : 75 € / jour (gratuité pour Assemblées générales et réunions statutaires)
* Résidents de la commune : 75 € / jour

Pour toutes autres demandes, prendre contact avec la Mairie.

## Débroussaillage - élagage - haies

L'obligation de débroussaillage et de maintien en état débroussaillé s'applique aux propriétaires de terrains situés à moins de 200 mètres des bois et forêts.

Cette opération doit être réalisée autour de votre habitation sur une profondeur de **50 mètres**. Le long des voies d'accès à votre terrain (route, sentier, chemin privatif), cette opération doit être réalisée sur une profondeur de **10 mètres** de part et d'autre de la voie.

L’élagage des branches qui dépassent sur le domaine public est à la charge des riverains.

Faute de ne pas respecter ces obligations les propriétaires peuvent être mis en demeure par la Mairie. Ils s'exposent à une amende et à payer les travaux commandés en leur place par la Mairie.

Par ailleurs, en matière de plantation de végétaux, le Code civil (articles 670 à 673) impose des règles de distance par rapport au terrain voisin :

* 0,50 mètre de distance **si l’arbre mesure moins de 2 mètres de haut**.
* 2 mètres de distance **si l’arbre mesure plus de 2 mètres de haut**.

Cette obligation ne s'applique pas si l'arbre a plus de 30 ans.

## Chiens et chats

Par arrêté du maire, les chiens doivent impérativement être tenus en laisse à l'intérieur du village de Lalouvesc.

Les déjections canines sont autorisées seulement dans les caniveaux. À l’exception des parties de ces caniveaux se trouvant à l’intérieur des passages pour piétons.

Les résidus de crottes de chiens sont interdits sur :

* les voies publiques ;
* les trottoirs ;
* les espaces verts publics ;
* les espaces des jeux publics pour enfants

**Tout propriétaire ou possesseur de chien est tenu de procéder immédiatement, par tout moyen approprié, au ramassage des déjections canines sur toute ou partie du domaine public communal.**

En cas de non-respect de l’interdiction, l’infraction est passible d’une contravention de 1ère classe.

Depuis le 1er janvier 2021, l'obligation de tatouer ou pucer les chiens à des fins d'identification a été étendue aux chats.  
Les propriétaires n'ayant pas pris les mesures d'identification de leurs animaux de compagnie s'exposent à une amende de 750 €.

## Déclaration annuelle de ruches

La déclaration de ruches est une obligation annuelle pour tout détenteur de colonies d’abeilles, dès la première ruche détenue.  
Elle doit être réalisée chaque année, entre le 1er septembre et le 31 décembre, en ligne, sur le site : [http://mesdemarches.agriculture.gouv.fr](http://mesdemarches.agriculture.gouv.fr "http://mesdemarches.agriculture.gouv.fr")/.  
Toutes les colonies sont à déclarer, qu’elles soient en ruches, en ruchettes ou ruchettes de fécondation.  
En cas de besoin, contactez le service d’assistance aux déclarants par mail à l’adresse suivante : assistance.declaration.ruches@agriculture.gouv.fr;  
Pour les apiculteurs ne disposant pas d’un accès à internet, il est toujours possible de réaliser une déclaration de ruches en sollicitant un accès en mairie.

Le délai d’obtention d’un récépissé de déclaration de ruches est d’environ 2 mois à compter de la réception à la DGAl.  
Pour les nouveaux apiculteurs ou les apiculteurs souhaitant obtenir un récépissé de déclaration actualisé, il est possible de réaliser une déclaration hors période obligatoire (entre le 1er janvier et le 31 Août.) Cette démarche ne dispense cependant pas de la déclaration annuelle de ruches.

## Frelons asiatiques et autres signalements de santé publique

Le frelon asiatique poursuit sa progression sur le territoire régional. Outre la problématique liée à sa présence sur les zones urbanisées, il représente une véritable menace pour la biodiversité et la santé des abeilles.  
Un dispositif de surveillance et de lutte, piloté par la FRGDS, vise à repérer et faire détruire les nids par des entreprises spécialisées avant la sortie des fondatrices (à la fin de l’automne), afin de maintenir la population de frelons asiatiques à un niveau acceptable.  
Deux types de nids peuvent être observés au cours de l’année :  
**Les nids primaires** : visibles dès les premiers beaux jours, au printemps,  
**Les nids secondaires** : visibles dès le début de l’été, correspondant à une délocalisation de la colonie qui abandonne le nid primaire, trop petit.  
[Tout savoir sur le frelon asiatique](https://fr.wikipedia.org/wiki/Vespa_velutina).  
Toute personne suspectant la présence d’un frelon asiatique est invitée à en faire le signalement sur la plateforme de signalement en ligne : [frelonsasiatiques.fr](http://frelonsasiatiques.fr)

**|**-> Pour tout signalement d’un **risque pour la santé publique** 24h/24 (pour les professionnels de santé, administrations, collectivités) : 1 N° _(0 800 32 42 62),_ 1 courriel _(_[_ars69-alerte@ars.sante.fr_](mailto:ars69-alerte@ars.sante.fr)_)._

**|**-> Pour tout signalement d’un **moustique Tigre** (vecteur de Dengue, Chikungunya, Zika) : 1 site ([_http://www.signalement-moustique.fr_](http://www.signalement-moustique.fr "http://www.signalement-moustique.fr")_)_

**|**-> Pour tout signalement d’**ambroisie** aux référents communaux : 1 N° (_0 972 376 888_), 1 site ([_www.signalement-ambroisie.fr_](http://www.signalement-ambroisie.fr/)), 1 application Smartphone ([_App Store_](https://itunes.apple.com/fr/app/signalement-ambroisie/id881819466?mt=8), _G_[_oogle Play_](https://play.google.com/store/apps/details?id=fr.signalement_ambroisie)), 1 courriel ([_contact@signalement-ambroisie.fr_](mailto:contact@signalement-ambroisie.fr))

**|**-> Pour tout signalement d’une morsure par une **Tique** (vectrice de la maladie de Lyme) : 1 site ([_https://www.citique.fr/signalement-tique_](https://www.citique.fr/signalement-tique "https://www.citique.fr/signalement-tique")), 1 application Smartphone ([_Play Store_](https://play.google.com/store/apps/details?id=com.inra.VigiTic&hl=en), _Apps Store_)