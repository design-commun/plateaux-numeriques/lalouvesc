---
title: Aide et accessibilité
date: 2020-09-06T18:45:44.284+00:00
description: ""
subtitle: ""
icon: ""

---

*Notre site est actuellement en phase de test. Nous faisons tout notre possible pour qu’il soit accessible aux personnes en situation de handicap et conforme aux [normes d’accessibilité numérique <abbr title="Référentiel général d’accessibilité pour les administrations">RGAA</abbr>](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/).*

Que vous ayez besoin de personnaliser l’affichage de notre site, d’en savoir plus sur ses fonctionnalités, son accessibilité ou de nous aider à l’améliorer, vous trouverez sur cette page toutes les informations nécessaires.

## Fonctionnalités du site

### Liens d’accès rapide

Un lien d’accès rapide intitulé « Aller au contenu principal » vous permet d’atteindre directement le contenu principal de la page. Il s’agit du premier lien de la page.

Un second lien d’accès rapide intitulé « Aller à la recherche » vous permet d’atteindre directement le champs de recherche du site.

### Affichage

Vous pouvez augmenter et diminueur la taille d'affichage des textes à l’aide de la fonction zoom de votre navigateur.


## Déclaration de conformité

Le contrôle permettant d’établir une déclaration de conformité, conformément aux termes de la loi, a été réalisé durant l’été 2021. Pour garantir la sincérité et l’indépendance de l’audit, ce contrôle a été effectué par [Access42](https://access42.net/), un intervenant externe spécialisé.

[Consulter la déclaration de conformité](/declaration-de-conformite-rgaa)

## Amélioration et contact

Vous pouvez contribuer à améliorer l’accessibilité du site.

N’hésitez pas à nous signaler les problèmes éventuels que vous rencontrez et qui vous empêchent d’accéder à un contenu ou à une fonctionnalité du site.

Nous écrire : mairie@lalouvesc.fr
Nous appeler : 04 75 67 83 67
