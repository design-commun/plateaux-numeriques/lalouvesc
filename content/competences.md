---
title: Compétences
date: 2020-09-06T18:45:44.284+00:00
subtitle: "Depuis la loi NOTRe de 2015"
icon: ""
draft: true

---

{{< cols >}}

{{< col >}}
### Votre commune
[Mairie de Lalouvesc](/mairie-demarches/)

- Compétence 1
- Compétence 2
- Compétence 3

{{</ col >}}

{{< col >}} 
### Intercommunalité
[Communauté de communes du Val d’Ay](/mairie-demarches/vie-municipale/communaute-de-communes-du-val-d-ay/)

- Développement économique, recherche, commerce et artisanat
- Gestion de l’espace (agriculture, forêts et paysages)
- Promotion du tourisme dont la création d’office de tourisme et de zones d’activités touristiques
- Gestion des ordures ménagères et des déchets
- Aménagement, entretien et gestion des aires d’accueil des gens du voyage
- Enfance et jeunesse
- Énergies renouvelables
- Réseaux de télécommunications numériques
- Gestion de la voirie communautaire
- Gestion des chiens errants
- Janvier 2018 : GEMAPI (Gestion des Milieux Aquatiques & Prévention des Inondations)

{{</ col >}}

{{</ cols >}}