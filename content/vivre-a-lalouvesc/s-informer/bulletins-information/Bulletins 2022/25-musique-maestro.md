+++
date = 2022-07-30T05:57:21Z
description = "n°25 Août 2022"
header = "/media/entete-3.png"
icon = ""
subtitle = "n°25 Août 2022"
title = "25 - Musique, Maestro !"
weight = 1

+++
Les Promenades musicales nous enchantent la première quinzaine d'août à Lalouvesc et dans le Val d'Ay depuis quinze ans. C'est l'occasion de faire un bilan. Elles rejoignent cette année le Carrefour des Arts. Ce bulletin fait un zoom sur la musique classique, le jazz, et même la musique religieuse, celle de l'orgue de la basilique. On trouvera aussi dans ce bulletin la présentation de nouveaux magasins, les dernières nouvelles de l'organisation de la brocante, une visite au centre de tri des déchets et encore, comme toujours, bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier](/media/bulletin-de-lalouvesc-n-25-1er-aout-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du maire

Chers amis,

Nous nous trouvons au cœur de la saison avec son lot d'affaires courantes à régler.

La mairie et la poste battent leur plein. Les estivants sont venus, ils sont là, notre village remplit son rôle d'ambassadeur de la nature, de la forêt.

La forêt dans laquelle nous trouvons fraîcheur et repos dans cette canicule qui couvre notre pays, sachons la préserver. Soyez extrêmement vigilants à tout départ de feux, prudents pendant vos pique-niques et autres sorties, un incendie serait une catastrophe et viendrait entacher cette belle saison.

Dans cette période de sécheresse, soyez aussi économes avec l'eau, nous sommes alimentés par des sources et le prélèvement n'est pas inépuisable.

Nous avions fait le pari de nous redresser après deux mauvaises années dues à l'épidémie de COVID, nous étions bien prêts en début de saison, le village a joué son rôle en attirant sont lot de touristes, participants ainsi à l'essor économique du village. Nous avons tous notre rôle à jouer, un consommateur satisfait est fidélisé.

Jouons tous le jeu : commerçants de grâce restez ouverts au maximum, tous les jours des mois de Juillet et Août. Prolongez vos soirées, il fait beau, il faut en profiter, il en va de l'avenir et du développement touristique de notre village, rare source économique de celui-ci.

Je terminerais mon petit mot en remerciant une nouvelle fois tous les acteurs participants de loin ou de près à l'organisation du village, bénévoles, associations, commerçants, employés communaux, entreprises partenaires, secrétariat de mairie, conseillers municipaux.

Certains d'entre nous rentrent de vacances d'autres partent, je vous souhaite à tous de passer de très bonnes vacances, aussi à nos enfants en congés scolaires, reposez-vous bien pour reprendre votre scolarité en pleine forme.

Récoltons le fruit de notre travail, ne mollissons pas, tous sur le pont pour Lalouvesc !

VIVE LALOUVESC ! VIVE LES VACANCES !

Jacques Burriez, Maire de Lalouvesc

## Actualités de la mairie

### Relevé des compteurs d'eau

Comme chaque année, à partir du 1er août il est nécessaire de procéder au relevé des compteurs d'eau. Ce relevé est obligatoire une fois par an afin d'établir la facture de consommation d'eau.

![](/media/robinet-eau.jpg)

Les abonnés ont la possibilité de déclarer eux-mêmes leur consommation en [remplissant directement en ligne ce formulaire](https://forms.gle/JpjJrfECf4cmm2XW6), accompagné de l'envoi d'une photo de leur compteur présentant clairement les chiffres de consultation à eau@lalouvesc.fr. Le formulaire peut aussi être retiré sur papier à la mairie et retourné par la poste.

Si vous ne souhaitez pas utiliser cette commodité, merci de faciliter l'accès au compteur d'eau et de réserver le meilleur accueil à l'agent communal.

**Attention, faute d'avoir effectué l'une de ces démarches avant le 16 septembre, votre facture d'eau sera établie sur la consommation de l'année précédente augmentée de 20%.**

### Réfection d'un mur de soutènement au hameau du Besset

Les travaux de réfection des murs de soutènements écroulés au hameau du Besset ont été réalisés.

![](/media/mur-soutenement-besset-2022-1.jpg)

![](/media/mur-soutenement-besset-2022-2.jpg)

### Stationnements illicites

Chacun peut constater que les règles de stationnement sont mal appliquées dans le village concernant le stationnement-minute ou les places interdites, entraînant difficultés de circulation, désordre, mauvaise humeur et même parfois risque pour les personnes.  
Pour un village plus serein, sécure et accueillant, il est indispensable que chacun fasse preuve de discipline. En conséquence, il a été demandé plus de vigilance à la gendarmerie. A cette fin les services de la mairie disposeront un papillon sur les voitures en infraction (v. ci-dessous). Il s'agit d'un simple avertissement, mais l'amende ne sera pas loin, l'immatriculation ayant été relevée.

![](/media/infraction-stationnement-2022.jpg)

## Zoom

### Histoire d'une alchimie : Les Promenades musicales

Les Promenades Musicales de Lalouvesc et du Val d’Ay sont nées d’une rencontre fortuite à la terrasse d’un restaurant de Lalouvesc et d’un coup de foudre amical entre des habitants et des musiciens de l’Ensemble Instrumental du Val de Loire, en automne 2006. Dès cette première rencontre le projet des Promenades Musicales a germé.

L’idée de quatre concerts, dont deux en plein air, à organiser dès l’été suivant pouvait paraître un peu dingue, mais la détermination du groupe initial et aussi l'hospitalité de Louvetous prenant en charge chez eux les musiciens ont facilité les choses. Et le succès fut au rendez-vous. Quatre années plus tard, des concerts étaient organisés dans tout le Val d'Ay grâce au soutien de la Communauté de communes...

On ne comprendrait pas ce succès sans l'alchimie qui s'est installée entre le village et les musiciens, bien exprimée par Florent Couaillac il y a deux ans dans ce même Bulletin :

> Il y a 11 ans, Gilles Lefèvre m’a proposé de venir participer aux PML en tant qu’élève et j’y ai découvert un lieu, une ambiance et une qualité musicale incroyable. Depuis je suis revenu tous les ans pour apprendre le métier et progresser avec tous ces musiciens de talent.  
> Désormais je ne me vois pas passer une année sans aller à Lalouvesc ! La nature et les lieux de concerts sont magnifiques et le cadre chaleureux de l’équipe du festival en font une grande famille.

![](/media/promenades-musicales-lalouvesc-2.jpeg)

Les concerts privilégient la musique classique et incluent des créations par des compositeurs contemporains. Du jazz et de la musique populaire sont aussi proposés et touchent un public varié. Les concerts classiques se tiennent dans la Basilique de Lalouvesc et les églises des villages du Val d'Ay. Les concerts jazz et variétés sont proposés à la grange Polly, non loin du village, ou sous chapiteau.

L'organisation est le fait de bénévoles, les prix sont modestes et le public est au rendez-vous. Les artistes classiques offrent aussi régulièrement des concerts dans les maisons de retraite de la région (Lalouvesc, Satillieu, St. Félicien).

![](/media/promenades-musivales-lalouvesc-1-1.jpeg)

La programmation privilégie les œuvres classiques en associant des œuvres « grand public » (_Les quatre saisons_ de Vivaldi, _La petite musique de Nuit_ de Mozart etc.) avec des œuvres moins connues aptes à élargir l’univers musical du public.

Les Promenades participent également activement à la diffusion d’œuvres de compositeurs contemporains et, en quinze ans, pas moins de trente créations ont été présentées !

En quinze ans, les promenades musicales ont reçu en résidence près de cent musiciens classiques dont quarante-huit professeurs de conservatoire ou concertistes parmi lesquels cinq artistes étrangers internationaux, neuf compositeurs, sept artistes lyriques. Un chœur de vingt-cinq chanteurs est venu se joindre à l’orchestre depuis 2015.

Parallèlement quarante-cinq musiciens de jazz et de variétés ont été reçus.

Gilles Lefèvre, violoniste

[**Consultez l'impressionnante liste des créations et des artistes qui se sont produits aux Promenades musicales.**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/artistes-des-promenades-musicales/)

#### L'aventure du Jazz à Polly

Mais le jazz aux Promenades musicales est inséparable du lieu principal de son interprétation : la grange de Polly.

> Comment peut-on imaginer qu’une grange pittoresque d’environ 270 m2, autrefois destinée à l’élevage laitier, puisse un jour devenir le lieu privilégié d’une musique de Jazz, sans racine aucune dans l’environnement rural de Lalouvesc et de Saint-Pierre-sur-Doux ?
>
> Au départ, une simple rencontre de voisinage pour préserver le patrimoine naturel et l’environnement contre un projet éolien qui promettait un gigantisme de béton et de métal à la place des arbres sur la plus belle des crêtes du Rouvey. Et non loin de là, à Saint-Symphorien-de-Mahun, un avatar de piano, bourré de contacteurs, d’électricité et de logiciels, intriguait mon visiteur du jour, Jacques Trébuchet.
>
> A sa demande, j’exécute sur l’instrument, quelques morceaux de mon répertoire de jazz traditionnel. Je découvre ainsi l’existence, à la ferme de Polly, d’un amateur de Jazz avec lequel débutera un projet de développement d’une scène musicale jazz dans le contexte des Promenades Musicales de Lalouvesc et du Val d’Ay.
>
> ![](/media/jazz-1.jpg)
>
> Il s’en suivit, pendant quelques années, une série de concerts donnés par l’orchestre de Jean Dionisi dont je suis l’un des membres, avec notamment la présence d’un musicien de grand talent, Jean-François Bonnel, dont la qualité de professeur de jazz au conservatoire d’Aix-en-Provence est largement dépassée par la notoriété internationale de ses prestations.
>
> A cette occasion, Jean-François Bonnel avait également assuré une seconde prestation avec un orchestre de sa composition, dans un style plus moderne, sous le nom de _JFB’S New Quartet_ accompagné par la chanteuse Claire Marlange.
>
> Mais nous retrouvons également dans cette formation la présence de grands musiciens professionnels tels que Félix Hunot à la guitare, Olivier Lalauze à la contrebasse et Stéphane « Zef » Richard à la batterie, lesquels se sont joints également à la formation de Jean Dionisi.
>
> C’est le succès grandissant de ces représentations qui conforta l’idée chez Jacques de poursuivre l’aménagement de la grange de Polly notamment pour y organiser des concerts de jazz. D’autant qu’il était déjà sollicité pour y accueillir d’autres manifestations ou festivités.
>
> Sur ces entrefaites, Jacques quitte sa résidence principale du Pays Basque pour se rapprocher de sa famille lyonnaise. C’est ainsi que, lyonnais d’origine et ancien musicien du _Hot Club de Lyon_ dans les années 60 et 70, je conduis Jacques vers l’une des plus grandes scènes de jazz lyonnaise : « _le New Orleans Jazz Parade de Lyon_ » qui a lieu chaque année le dernier week-end du mois de novembre. Je lui présente donc l’organisateur, Jacky Boyadjian, président de la _New Orleans Jazz Association_ (NOJA) et ancien musicien du _Hot Club de Lyon_ également.
>
> De cette rencontre, naîtra une nouvelle collaboration avec Jacky qui assistera Jacques dans la programmation des orchestres et l’organisation des concerts, en liaison avec la NOJA très active dans la promotion du jazz traditionnel.
>
> {{<grid>}}{{<bloc>}}
>
> ![](/media/jazz-7.jpeg)
>
> {{</bloc>}}{{<bloc>}}
>
> ![](/media/jazz-3.jpeg)
>
> {{</bloc>}}{{</grid>}}
>
> C’est ainsi, que des concerts d’une qualité exceptionnelle ont régalé le public enthousiaste de Lalouvesc et d’ailleurs, avec notamment la prestation de la violoniste Caroline Bugala accompagnée par l’équipe du jazz manouche du _Sinti Swing Trio_ et son fabuleux guitariste Sébastien Félix.
>
> Plus récemment, avec l’idée de monter un programme autour de George Gershwin, Jean-François Bonnel propose à Jacques de faire venir à la Grange Philippe Carment, l’un des meilleurs pianistes de jazz français du moment, sans savoir que Jacques, rouennais dans sa jeunesse a bien connu Philippe et sa famille dans les années 70. C’est ainsi que Philippe a enchanté la Grange de Polly avec un duo inoubliable en compagnie de Jean François Bonnel.
>
> Voici donc la Grange de Polly lancée dans une aventure d’autant plus remarquable qu’elle se déroule dans un cadre bucolique et romantique à souhait. Tout semble réuni pour créer une belle chapelle du Jazz aux vibrations chaleureuses et swinguantes bien sûr.

Stéphane Matthey, pianiste, St-Symphorien-de-Mahun

Cette année deux concerts sont programmés les 12 et 13 août : **_Fats Waller & His Rhythm_** et **_Jazz Manouche_**, la Grange de Polly se trouve sur la Commune de St-Pierre-sur-Doux à proximité immédiate de Lalouvesc.

#### Les Promenades se retrouvent au Carrefour

En 2021, le Carrefour des Arts nous a sollicité pour organiser un concert création autour des œuvres de Martine Jaquemet peintre, des poèmes de Jorge Pimentel, grand écrivain Péruvien et de la musique de Felipe Carrasco compositeur Chilien. Le succès rencontré fut tel que cette année marquera le début de la reprise des activités des Promenades Musicales par le Carrefour des Arts.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Auq5GsrHWTk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

En 2022, sous l'égide du Carrefour des Arts nous invitons des artistes de renommée internationale et proposons trois créations : une de Pierre André Athané et deux de Felipe Carrasco.

* 2 août : Le concert, **_La voix et la musique_** proposé, en la Basilique de Lalouvesc, présente le _Stabat Mater_ de Pergolèse avec les solistes Elise Martineau et Lys Nordet qui seront soutenues par le Chœur de femmes _Colla Voce_, sous la direction de Felipe Carrasco, des airs d’opéra, deux créations : _L’étonnant voyage_ de P.A. Athané, _Esa pequena estrella_ de Felipe Carrasco et une cantate de G. Holst, _Seven parts songs_ pour orchestre à cordes, chœur de femmes et solistes.
* 3 août : Le concert **_Bach et associés_**, à St Romain d’Ay, présentera un trio original avec au hautbois, Laurent Hacquard, au violoncelle, Gwendeline Lumaret et au violon, Gilles Lefèvre. Ils feront vibrer ce lieu magique avec les musiques de J.S. Bach, de sa famille et de ses amis.
* 4 août : Lors du dernier concert classique, en la Basilique de Lalouvesc, le public aura le plaisir de découvrir un des sommets de la musique romantique, **_Souvenir de Florence_** de P. I. Tchaïkovski pour sextuor à cordes. Pour l’occasion le sextuor accompagnera le guitariste Raymond Gratien et le hautboïste Laurent Hacquard dans des concertos romantiques de Crusell, Guiliani et Paggi.

Pendant les Promenades des violonistes équatoriens, élèves de Gilles Lefèvre, et Florent Couaillac animeront le village lors de prestations spontanées en extérieur. Ils joueront aussi, pour nos aînés qui ne peuvent pas se déplacer.

Nous aurons aussi l’immense joie de présenter, lors de ces promenades, la danseuse internationale Aurore Borgo qui se produira en extérieur le 3 août avec le trio solystelle.

Gilles Lefèvre, violoniste

#### Un Carrefour des arts plastiques... et musicaux

L'engagement du Carrefour des Arts dans l'organisation de concerts voulait d'abord préserver le capital de notoriété acquis par les Promenades musicales qui n'avaient pas pu poursuivre leurs activités en 2020 et 2021.

L'expérience menée en 2021 a également montré l'intérêt que trouvaient les artistes et le public dans le croisement de disciplines artistiques, arts plastiques et arts du vivant.

L'ambition est maintenant de créer un programme estival culturel complet à Lalouvesc dont le point d'orgue serait la première quinzaine du mois d'août. Cette année en est la préfiguration. A cette fin, l'association Carrefour des Arts entend fédérer les bénévoles amateurs d'arts de toutes disciplines.

Les événements artistiques sont des pôles d'attractivité pour Lalouvesc et le Val d'Ay. Un sondage réalisé auprès des visiteurs de l'exposition la première quinzaine de ce mois de juillet montre que la moitié ont fait le déplacement spécialement pour la manifestation.

Le Carrefour compte sur son expérience, longue de 34 années, cumulée avec celle des 15 années des Promenades musicales, maintenant réunis, pour préparer une manifestation 2023 plus ambitieuse encore, et, pourquoi pas ?, déboucher sur une activité permanente grâce à la notoriété ainsi capitalisée. C'est l'ambition que nous voulons défendre devant les collectivités qui nous soutiennent.

Jacques Morel et Julien Besset, vice-président et président du Carrefour des Arts

**Toutes les informations sur** [**le Carrefour des Arts et sa programmation sont accessibles sur son site**](https://carrefourdesarts-lalouvesc.fr/)**.**

### Méditations musicales de l'Assomption et souvenirs du village

Les Promenades musicales ne sont pas les seuls événements musicaux du mois d'août. Traditionnellement le Sanctuaire organise un concert à la Basilique à l'occasion de la fête du 15 août.

Cette année les intervenants seront **Henri Pourtau**, organiste à Cannes et ses deux anciens élèves, deux jeunes et brillants musiciens : **Aleksandar Nikolaev** (flûte) et **Raphaël Yacoub** (chantre).

D’origine bulgare, Aleksandar, vient de terminer son cursus à la Hochschule de Dresde et il intègre à la rentrée prochaine un cycle de perfectionnement à Genève. Quant à Raphaël, chantre de la Paroisse Saint-Nicolas de Cannes, il prépare son master à l’université et travaille le chant avec Michel Géraud, à Cannes.

![](/media/henri-pourtau-2022.jpeg)  
Pour Henri Pourtau, Louvetou d’origine, c’est aussi l’occasion d’un retour aux sources…

> Lalouvesc, c’est toute mon enfance, passée au sein de la famille de ma mère, les Faurie, sur la place des Marronniers. La maîtrise précoce de l'orthographe de ce végétal me valut d’ailleurs quelque succès scolaire….
>
> A chaque pas accompli dans ce village, les souvenirs ressurgissent…
>
> La foule du 15 août, les nuées d’enfants des multiples colonies de vacances et leurs chants…
>
> J’aimais particulièrement l’une d’elles, montée de la Fontaine (aujourd'hui Chemin neuf). C’était probablement une communauté juive. Que leurs chants étaient beaux ! Je les hurlais ensuite à mon tour… _Hava Nagila Hava…_ Combien je suis reconnaissant à ces anonymes d’avoir mis en ma mémoire ces mélodies magnifiques qui ont aussi révélé ma soif de l’Ailleurs !
>
> Lalouvesc, c’est aussi des souvenirs olfactifs ! Les champignons de l’automne, la crique, le poulet aux écrevisses confectionné par ma tante Marguerite, cuisinière au mess Lavoisier du Péage de Roussillon. La boulangerie de mon grand oncle Buisson et les bonnes odeurs qui montaient du four « vulcanien » de l’arrière-cour.
>
> J’ai aussi eu la joie de revoir récemment Jeannot Abrial, l’ancien garde-champêtre et lui ai dit combien j’avais été marqué par les roulements de son tambour et ses _Avis à la population…_
>
> Mais par-dessus tout cela, il y avait la voix souveraine et hiératique du bourdon de la basilique. Nulle part ailleurs je n’ai éprouvé une telle sensation. Lalouvesc peut être fière de ce magnifique trésor...
>
> C’est d’ailleurs à la basilique que j’ai joué l’une de mes toutes premières messes, à l’âge de 16 ans. Ce fut catastrophique… ! L’indulgence du père Satin ne fit rien pour apaiser le courroux familial et, rentré à la maison, je pris l’une des plus marquantes engueulades de ma vie… Alors, j’ai travaillé….
>
> Henri Pourtau

Concert de cette année lundi 15 août, à 21 heures : **« Salve Regina »**  
Au programme : Bach, Vinci, Rachmaninoff, Boëllmann et chants d’assemblée.  
Entrée libre dans la limite des places disponibles.

## Culture - Loisirs

### Programme des festivités du mois d'août

Les Lalouv'estivales sont pleines de réjouissances jusqu'à la fin du mois. Profitez-en !

#### En continu

* Jusqu'au 7 Novembre : **Exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* Jusqu'au 28 août : **Exposition de peintures et sculptures contemporaines** du [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal tous les jours de 14h30 à 18h30
* Jusqu'au 13 août : **Exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* Jusqu'au 15 août, **activités pour enfants et adultes** au camping ouvertes à tous
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* Les mercredis à 17h **Apéros découverte** à l'Office du tourisme

#### Jour par jour

* 1er août 21h : **Cinéma** _À l’ombre des filles_ ( [bande annonce](https://youtu.be/0qezBv7i-bc) ) à l'Abri du Pèlerin
* 2 août 20h30 : **Concert** _La voix et la musique_  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr), musique classique à la basilique
* 3 août 20h30 : **Concert** _Bach et associés_  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr), musique classique à la chapelle de Notre Dame d'Ay
* 3 août 21h : **Cinéma** _L’école du bout du monde_ ( [bande annonce](https://youtu.be/hca-7AKq07w) ) à l’Abri du Pèlerin
* 4 août 20h30 : **Concert** _Souvenir de Florence_  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr), musique classique à la basilique
* 4 août 21h : **Cinéma** _Top Gun_ ( [bande annonce](https://youtu.be/V4gQdk1nAn0) ) à l'Abri du Pèlerin
* 6 août 21h : **Cinéma** _L'école du bout du monde_ ( [bande annonce](https://youtu.be/hca-7AKq07w) ) à l'Abri du Pèlerin
* 7 août : **Fête du Sanctuaire**
* 7 août 21h : **Cinéma** _Top Gun_ ( [bande annonce](https://youtu.be/V4gQdk1nAn0) ) à l'Abri du Pèlerin
* 8 août 21h : **Cinéma** _L'innocent_ ( [bande annonce](https://youtu.be/6wl4B5lJgBM) ), avant-première à l'Abri du Pèlerin
* 9 août 20h30 : **Théâtre**  _Sous les ponts de Paris_ par Les Bretelles à Bascule à l'Abri du Pèlerin
* 10 août : **Cinéma** à l'Abri du Pèlerin
  * 17h30 : _De folies en folies_, documentaire, Expo photos et dégustation de produits locaux entre les séances
  * 21h : _Les folies fermières_ ( [bande annonce](https://youtu.be/KD5sB_BPLZI) )
* 11 août 21h : **Cinéma** _L'affaire Collini_ ( [bande annonce](https://youtu.be/4buWqDrWMBo) ), à l'Abri du Pèlerin
* 12 août 19h30 : **Concert _Fats Waller & His Rhythm_**  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr),, jazz à la Grange de Polly
* 13 août 19h30 : **Concert _Jazz Manouche_**  [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr),, jazz à la Grange de Polly
* 13 août 21h : **Cinéma** _Les folies fermières_ ( [bande annonce](https://youtu.be/KD5sB_BPLZI) ), à l'Abri du Pèlerin
* 14 août 21h : **Cinéma** _L'affaire Collini_ ( [bande annonce](https://youtu.be/4buWqDrWMBo) ), à l'Abri du Pèlerin
* 15 août : Sanctuaire [**fête de l'Assomption**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/14-les-beaux-gestes/#deux-f%C3%AAtes-au-sanctuaire-sous-le-soleil)
* 15 août 21h : **Cinéma** _Buzz l'éclair_ [(bande annonce](https://youtu.be/q41VoF95fmI) ), à l'Abri du Pèlerin
* 15 août 21h : **Concert d'orgue** d'Henri Pourtau, basilique
* 17 août 21h : **Cinéma** _Ténor_ ( [bande annonce](https://youtu.be/-rhY0fonYwM) [)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/programme-du-cinema-d-aout/), à l'Abri du Pèlerin
* 18 août 21h : **Cinéma** _Jurassic World : le monde d'après_ ( [bande annonce](https://youtu.be/4R87S4YCtI8) ), à l'Abri du Pèlerin
* 20 août 21h : **Cinéma** _Ténor_ ( [bande annonce](https://youtu.be/-rhY0fonYwM) [)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/programme-du-cinema-d-aout/), à l'Abri du Pèlerin
* 21 août 21h : **Cinéma** _Jurassic World : le monde d’après_ ( [bande annonce](https://youtu.be/4R87S4YCtI8) ), à l’Abri du Pèlerin
* 22 août 21h : **Cinéma** _Les volets verts_ ( [bande annonce](https://youtu.be/zLbz061gLzw) ), avant-première à l'Abri du Pèlerin
* 23 août 20h30 : **Théâtre** _Building_ par le Théâtre d'en face à l'Abri du Pèlerin
* 24 août 21h : **Cinéma** _Joyeuse retraite 2_ ( [bande annonce](https://youtu.be/TgNiqEKEydw) ), à l'Abri du Pèlerin
* 25 août 21h : **Cinéma** _La petite bande_ ( [bande annonce](https://youtu.be/npLm1oPOY14) ), à l'Abri du Pèlerin
* 27 août 21h : **Cinéma** _Joyeuse retraite 2_ ( [bande annonce](https://youtu.be/TgNiqEKEydw) ), à l'Abri du Pèlerin
* 28 août 21h : **Cinéma** _La petite bande_ ( [bande annonce](https://youtu.be/npLm1oPOY14) ), à l'Abri du Pèlerin
* 25 août 21h : **Cinéma** _Revoir Paris_ ( [bande annonce](https://youtu.be/idHIUQeF_cw) ), avant première à l'Abri du Pèlerin

### Fête du sanctuaire le 7 août

![](/media/fete-santuaire-2022.jpg)

Cette année, la kermesse du Sanctuaire devient une fête avec tombola, jeu de piste, apéritif et repas (à réserver au 04 75 67 82 00 ou maisonsaintregis@jesuites.com.

### _Une histoire d'Afar de Lalouvesc_, le jeu

Et s'il pleut ou si vous préférez simplement rester tranquille à la maison, voilà encore une idée pour vous amuser en famille ou entre amis.

{{<grid>}}{{<bloc>}}

![](/media/jeu-des-afars-2022.jpg)

{{</bloc>}}{{<bloc>}}

La course des Afars à travers l’Ardèche ou comment partir de Lalouvesc en rejoignant Le Pont d’Arc pour y récolter un caillou magique.

Ce jeu a été créé par Lilian Doudaine, originaire de St-Julien-Vocance et est fabriqué par l’entreprise CARIBOUD’ BOIS.

C’est un jeu presque tout en bois, à part le jeu de cartes. Il mêle mémoire et stratégie. Il peut se jouer à partir de 6 ans, de 2 à 4 joueurs. Une partie dure environ 30 mn.

Il est vendu au prix de 35 € chez SOUKA GUIGUI.

**_Mais attention, Afar à rien de courir, il faut partir à point...._**

{{</bloc>}}{{</grid>}}

### Bulletin de l'Alauda de 2022

{{<grid>}}{{<bloc>}}

Encore un numéro bien fourni cette année grâce aux historiens de l'Alauda avec, entre autres, une version approfondie de l'histoire de l'hôtel Beauséjour ou plutôt Costet parue dans le [Bulletin n°19 de février](), ou encore la liste des onze saints qui ont foulé le sol du village, ou l'histoire de la reprise forcée du Cénacle par le Dr Vincent au moment de la séparation de l'église et de l'Etat ou celle des Marrels, étrangers au village et qui ont pourtant une place à leur nom, la seule portant un nom de famille...

Si vous voulez mieux connaître l'histoire de Lalouvesc, c'est une **lecture incontournable** chaque année. {{</bloc>}}{{<bloc>}} ![](/media/alauda-2022.jpg)
{{</bloc>}} {{</grid>}}

### Madame d'Ora (suite)

Nous avions évoqué le passage de l'autrice Eva Geber et de la vidéaste Almut Linder toutes deux autrichiennes, dans un précédent Bulletin (n°5, novembre 2021). Elles suivaient les traces d'une célèbre photographe, madame D'Ora qui s'était cachée à Lalouvesc pendant la seconde guerre mondiale. Grâce à cette rencontre, une partie de son histoire a pu être reconstituée et contée dans le _Bulletin de l'Alauda_ de l'année dernière.

Le livre d'Eva Geber, _Madame D'Ora - Tagebücher aus dem exil_ (trad. _Madame D'Ora - Journal d'exil_)  est maintenant paru en Autriche.

{{<grid>}}{{<bloc>}}

![](/media/madame-d-ora-livre-2022.jpg)

{{</bloc>}}{{<bloc>}}

**Présentation du livre par l'éditeur (traduction)**

_D'Ora, de son vrai nom Dora Kallmus, était une photographe de renommée internationale avec des studios à Vienne et plus tard à Paris, où des personnalités de Gustav Klimt à la famille impériale, Joséphine Baker à Pablo Picasso ont fait leurs portraits. En 1940, après l'occupation de la France, elle perd sa propriété. Sa sœur bien-aimée a été déportée à Lodz en 1941. D'Ora s'est enfuie dans le sud de la France et a vécu pendant environ trois ans dans un village de montagne de l'Ardèche, où elle a tenu un journal et écrit des essais et un roman. Elle prévoyait de publier des parties de ces écrits après la guerre, mais n'a pas pu trouver d'éditeur. L'éditrice Eva Geber a pour la première fois passé en revue ce domaine de manière exhaustive et l'accompagne d'un essai explicatif ainsi que de documents contemporains et policiers. Le résultat est l'image intime d'une femme qui, malgré les difficultés et le danger, n'est pas prête à abandonner, qui veut préserver sa dignité et vivre ses valeurs. Les notes de D'Ora montrent clairement comment la menace du national-socialisme change la perspective d'une personne._

{{</bloc>}}{{</grid>}}

**On recherche un ou une germanophone** pour pouvoir rendre compte en français de la partie du journal de madame D'Ora faisant référence à Lalouvesc.

## Commerces

### Le marché fait recette

De plus en plus de forains et de monde au marché du jeudi sur la place du Lac.

{{<grid>}}{{<bloc>}}

![](/media/marche-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/marche-2022-1.jpg)

{{</bloc>}}{{</grid>}}

### De nouveaux magasins à Lalouvesc

#### Maille Doudous

{{<grid>}}{{<bloc>}}

![](/media/maille-doudous.jpeg)

{{</bloc>}}{{<bloc>}}

_Bonjour, je m'appelle Annick Mathevet, Saint-albanaise, je suis maman de trois grandes filles. Cet été, j'ai eu envie de travailler dans votre village, de vous **présenter mes réalisations, et également de vous proposer les retouches**.  
Le tissu a toujours été présent, de mes premiers essais, aux cours donnés par une association, en passant par un emploi dans un magasin de tissus.  
Suite à un changement de travail, j'ai décidé d'en faire mon activité principale. Je réalise beaucoup pour les enfants, mais aussi des accessoires et des décorations de Noël. Au fil des saisons, mon travail évolue, cela me permet de ne pas connaître l'ennui et la lassitude._

{{</bloc>}}{{</grid>}}  
_Je suis heureuse de passer cette saison parmi vous, de vous rencontrer et d'échanger. Je reste disponible et à votre écoute pour toute demande d'information ou travail en création ou retouche.  
Merci pour votre accueil et vos visites. Je vous souhaite un bel été, à bientôt !_

Annick M.  Maille Doudous

#### MarieLouv'art déco

{{<grid>}}{{<bloc>}}  
C’est à l’atelier-galerie _MarieLouve'artdéco_ que Marie-Christine Brunel crée et expose ses œuvres artistiques (toiles, dessins aquarelles...) et artisanales (meubles relookés, sacs peints ou crochetés, objets de décoration...). Vous y trouverez également des articles de loisirs créatifs pour les grands ou les petits.  
L’atelier c’est aussi un lieu dédié à la création. En effet des ateliers relooking de meubles, technique du pochoir ou des effets à la peinture seront proposés toute l’année. Ainsi que des cessions d’approche de l’art instinctif.

{{</bloc>}}{{<bloc>}}

![](/media/marielouvart-deco-2022-1.jpg)

{{</bloc>}}{{</grid>}}  
C’est au milieu de ses couleurs dans une ambiance chaleureuse et décalée que Marie-Christine vous accueille de 8h à 13h et de 14h à 19h tous les jours sauf le mardi en été.

![](/media/marielouvart-deco-2022-2.jpg)

### Le taxi change de main

Eric et Cyril Sprecacenere ont passé le flambeau à Nass Taxi : 06 65 70 81 06, nasstaxi26@gmail.com.

### Pour répondre à la demande brocante s'étend... dans la bonne humeur

Le succès de cette brocante ne se dément pas. Bien au contraire. Il aura suffi de trois petites semaines entre l’envoi des bulletins d’inscription et le constat d’être obligés d’afficher complet. A peine trois semaines et c’est ainsi que plus de 220 emplacements ont été réservés. Cette déferlante d’enveloppes déposées dans notre BP avait pour témoin actif, Alexandre le facteur qui a vu son activité renforcée début juillet !

![](/media/brocntae-2021-2.jpg)

Et sa convivialité s’affirme d’une année sur l'autre par des demandes pressantes comme : « **_l’année dernière j’avais le n° 25, à côté de Mr DUPONT, je souhaite être à nouveau placé à côté de lui_** ». Nous ne savons pourquoi ce monsieur DUPONT suscite un tel engouement, mais l’année dernière il s’est fait des copains et ce ne sont pas moins de quatre exposants qui cette année souhaitent être à proximité de son stand !! Autre demande « **_nous sommes trois copines et souhaitons vraiment être placées ensemble et si possible à côté de Mme DURAND qui elle aussi est de notre village »_**.

C’est ainsi que parfois nos sessions de placements ressemblent à un jeu de Tetris car les bulletins n’arrivent pas forcément en même temps. Il faut donc chercher, mettre en attente, appeler… Si des liens se créent entre exposants, il s’en crée également avec nous. Les appels sont nombreux, les observations exprimées sur les bulletins souvent amicales. Malgré quelques complexités à satisfaire tout le monde, les bénévoles du Comité des Fêtes sommes devenues expertes à ce jeu. Notre souhait premier est de satisfaire le plus grand nombre, même les plus exigeants et les moins compréhensifs. Il y en reste quelques-uns… genre : « **_je souhaite une place au soleil, mais pas trop quand même_** »...

Si sur cette fin juillet les bulletins arrivent moins nombreux, des retardataires se font connaître. C’est pourquoi nous allons ouvrir une extension du site. Les exposants se verront proposer quelques places supplémentaires. Au bout de chemin Grosjean, des stands seront installés en remontant sous le CAC, le long de la basilique et place des Trois Pigeons. Ainsi la boucle sera bouclée : le public aura la possibilité de déambuler dans le village et les commerçants seront tous vus et appréciés !

En amont il faut aussi choisir l’animation de rues. Pas si facile de trouver des disponibilités chez les bandas et/ou autres groupes. Cette année c’est la banda **FRED KOHLER d’ECLASSAN** qui devrait apporter musique et déambulations dans le village.

Et comme toutes les années, buvette, restauration rapide (saucisses/frites) crêpes satisferont les petites fringales de chacun avec aux manettes de la plancha, de la friteuse et de la crêpière nos grands chefs louvetous ! Et derrière la buvette nos serveurs aguerris à cet exercice qui du matin jusqu’à tard le soir ne faiblit pas. Merci déjà à tous !

Encore un mois pour peaufiner cet évènement qui souvent donne la note de fin de la période estivale. Alors profitons encore de ce mois d’août, les animations sont nombreuses au village. Il sera temps ensuite de glisser lentement vers l’automne.

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

## Santé - sécurité

### Visite d'un centre de tri des déchets

Tout a commencé par une invitation reçue à la mairie, avec François Besset nous nous sommes laissé guider par notre curiosité… et nous voilà partis le 5 juillet dernier pour la visite du centre de tri des collectes sélectives de MeTRIpolis à Portes-lès-Valence.

A notre arrivée, nous étions une vingtaine de personnes, le quatrième et dernier groupe de la journée. Après une brève présentation de la visite, nous avons été équipés d’un gilet jaune et d’un casque-audio afin de pouvoir entendre les commentaires du guide car le centre est très bruyant.

En 2019, le syndicat de traitement des déchets Ardèche/Drôme (SYTRAD) a construit un nouveau centre de tri des collectes sélectives en partenariat avec le syndicat des Portes de Provence (SYPP) et le syndicat intercommunal de collecte et de traitements des ordures ménagères de la basse Ardèche (SICTOBA). Le centre traite les déchets des poubelles bleues et jaunes de 771.000 habitants de la Drôme et de l'Ardèche.

Une fois l’indispensable geste de tri effectué par les habitants, les collectivités membres récupèrent les déchets et les acheminent vers le centre de tri. Les camions passent d’abord sur un pont à bascule afin d'être pesés et identifiés, ils sont alors autorisés à décharger leurs déchets dans différentes alvéoles en fonction de leur contenu.

Les déchets sont ensuite envoyés sur la chaîne de tri via deux trémies d’alimentation.

{{<grid>}}{{<bloc>}}

![](/media/centre-de-tri-2-2022.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/centre-de-tri-5-2022.jpg)

{{</bloc>}}{{</grid>}}

Après avoir traversé une passerelle métallique, nous avons pu observer les chaînes de tri par les fenêtres d’un grand couloir et repérer leurs différentes étapes :

1. _Les trommels_. Les trommels sont des cylindres rotatifs percés de trous de différents diamètres qui séparent les déchets selon leur taille.

   {{<grid>}}{{<bloc>}}

   ![](/media/centre-de-tri-1-2022.jpg)
   {{</bloc>}}{{<bloc>}}
2. _Les séparateurs balistiques_. Ils permettent de séparer les déchets selon leur volume. Ce sont des sortes d’échelles inclinées qui font sauter les déchets. Les “corps plats” (comme les cartonnettes ou films plastiques) restent coincés et sont évacués par le haut, alors que les “corps creux” (comme les bouteilles plastiques ou boîtes de conserves) roulent pour être ensuite séparés sur la partie basse.
3. _Les trieurs optiques_. Onze trieurs permettent de séparer les déchets selon leur matière.
4. _Le séparateur de film._ C’est un tapis roulant rotatif équipé de picots qui, alliés à une soufflerie, permettent de séparer les films en plastique des autres emballages en plastique.
   {{</bloc>}}{{</grid>}}
5. _Overband et courant de Foucault_. Les Overband permettent d’enlever par aimantation les métaux qui contiennent du fer (boîtes de conserves, couvercles de pots à confitures). Les courants de Foucault retirent les métaux qui ne contiennent pas de fer (canettes, feuilles aluminium, aérosols).  
   ![](/media/centre-de-tri-3-2022.jpg)
6. _Les robots trieurs._ Grâce à des caméras, l’intelligence artificielle du robot identifie les déchets. Les bras articulés du robot trient les déchets valorisables dans les bacs appropriés. Chaque bras peut porter jusqu'à 2 kg et 900 kg de déchets sont ainsi triés par heure.
7. _Le contrôle de la qualité_. Après le processus de tri, les agents vérifient la conformité des déchets et retirent les éventuels refus restants. Les chaînes de tri sont malheureusement souvent bloquées par des déchets mal sélectionnés au départ par les usagers. Nous avons pu voir des trottinettes, des jantes de voitures et les agents trouvent aussi souvent des cadavres d’animaux !  
   ![](/media/centre-de-tri-4-2022.jpg)
8. _Mise en balles et stockage_. Un fois le tri effectué, les déchets tombent dans des alvéoles et sont envoyés vers la presse à balles. La presse compacte les déchets en cube, dont le poids varie entre 500 et 1.200 kg selon la matière. Ces balles sont ensuite stockées avant leur expédition par camions vers les usines de recyclage qui leurs donneront une nouvelle vie.  
   Les refus, quant à eux, sont compactés dans des bennes. Ils seront traités au centre de valorisation de Beauregard-Baret (26) afin de devenir des combustibles solides de récupération, utilisés en substitution des énergies fossiles.

Enfin nous avons conclu notre visite par un petit et utile rappel de quelques consignes de tri :

* **ne pas rincer les emballages avant de les jeter,**
* **jeter les emballages sans sac poubelle,**
* **ne pas imbriquer les emballages,**
* **laisser le bouchon sur sa bouteille en plastique (tout ce qui fait moins de 6 cm n ‘est pas recyclé).**

Grâce à ce tri, notre poubelle d’ordure ménagère se sent beaucoup plus légère !

Aurélie Desbos

### Les soins en période estivale

Alors que la situation dans certains services d’urgences ou aux centres de régulation des Samu-Centre 15 s’intensifie, l’Agence régionale de santé Auvergne-Rhône-Alpes a décidé d’adapter sa communication vers le grand public afin d’inciter la population à **consulter un médecin avant de contacter le 15 ou de se rendre aux urgences**.

![](/media/ars-urgences-ete-2022.jpg)

Vous pouvez aussi consulter [le site de l'ARS](https://www.auvergne-rhone-alpes.ars.sante.fr/besoin-de-soins-immediats).

### Suite du calendrier Santé-Environnement

L’ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l’habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante). Pour le mois d'août, la suggestion concerne [les punaises de lit](https://www.auvergne-rhone-alpes.ars.sante.fr/les-punaises-de-lit-lutter-efficacement-et-prevenir-leur-apparition?parent=14348). Un autre calendrier alerte sur les [bonnes pratiques face à l’environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0). Pour le mois d'août, la suggestion concerne [la canicule](https://www.auvergne-rhone-alpes.ars.sante.fr/comment-bien-profiter-du-soleil-0?parent=5373).

{{<grid>}}{{<bloc>}}

![](/media/int-08_punaises.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ext-08_canicule.jpg)

{{</bloc>}}{{</grid>}}

### Conseils en architecture

Le CAUE de l'Ardèche propose des conseils gratuits en architecture.

![](/media/permanence-caue-2022.jpg)

## Dans l'actualité, le mois dernier

Ci-dessous, un rappel des informations publiées sur le site [lalouvesc.fr](https://www.lalouvesc.fr/) au cours du mois de juillet qui restent d’actualité.

#### Alerte sécheresse : passage au niveau crise !

26 juillet 2022 Nombreuses restrictions, risque d'amendes [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/alerte-secheresse-passage-au-niveau-crise/ "Lire Alerte sécheresse : passage au niveau crise !")

#### Le PV du Conseil municipal du 11 juillet est en ligne

20 juillet 2022 Ordre du jour et lien vers le PV [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-pv-du-conseil-municipal-du-11-juillet-est-en-ligne/ "Lire Le PV du Conseil municipal du 11 juillet est en ligne")

#### Le sanglier déclaré nuisible en Ardèche

19 juillet 2022 Arrêté préfectoral [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-sanglier-declare-nuisible-en-ardeche/ "Lire Le sanglier déclaré nuisible en Ardèche")

#### "Ardèche Inf'eau Plage" est en ligne !

18 juillet 2022 Un site pour informer sur la qualité de l'eau des plages de la rivière Ardèche [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/ardeche-inf-eau-plage-est-en-ligne/ (Lire "Ardèche Inf'eau Plage" est en ligne !))

#### Vous avez raté le feu d'artifice ou vous voulez le revoir...

13 juillet 2022 C'est cadeau ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vous-avez-rate-le-feu-d-artifice-ou-vous-voulez-le-revoir/ "Lire Vous avez raté le feu d'artifice ou vous voulez le revoir...")

#### Canicule et sécheresse

12 juillet 2022 Restons raisonnables dans notre consommation d'eau [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/canicule-et-secheresse/ "Lire Canicule et sécheresse")

#### En été, ne saturez pas les urgences médicales...

11 juillet 2022 Composez le 15 avant de vous déplacer à l'hôpital [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/en-ete-ne-saturez-pas-les-urgences-medicales/ "Lire En été, ne saturez pas les urgences médicales...")

#### Numérique et ruralité : des rencontres à Lalouvesc cet automne

10 juillet 2022 3-4 octobre : deux jours pour pointer les problèmes et évoquer des solutions [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/numerique-et-ruralite-des-rencontres-a-lalouvesc-cet-automne/ "Lire Numérique et ruralité : des rencontres à Lalouvesc cet automne")

## Vivaldi au bois

Terminons, comme toujours, par un clin d’œil.

Pour ce numéro où la musique est à l'honneur, nous vous proposons pour le plaisir une interprétation originale et énergique d'un morceau de Vivaldi par les enfants d'une école primaire sud-africaine.

<iframe width="560" height="315" src="https://www.youtube.com/embed/k-Pn4usTSi8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>