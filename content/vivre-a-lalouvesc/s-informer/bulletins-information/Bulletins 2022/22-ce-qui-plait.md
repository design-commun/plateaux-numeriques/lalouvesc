+++
date = 2022-04-28T22:00:00Z
description = "n°22 Mai 2022"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°22 Mai 2022"
title = "22 - Ce qui plaît"
weight = 1

+++
En mai fait ce qu'il te plaît dit le dicton... bien des choses nous plaisent à Lalouvesc, à commencer par le réaménagement de l'espace Beauséjour dont les prémices prometteuses sont présentées dans ce Bulletin, et aussi un budget qui nous permet d'investir, des journées citoyennes qui embellissent et réparent le village, des rencontres au Club des deux clochers, un magasin relooké, un concert d'exception et comme toujours, bien d'autres choses encore.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-22-1er-mai-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Mot du maire

Mes chers amis,

L’année 2022 est déjà entamée depuis plusieurs mois. Le beau temps approche, la douceur printanière et les beaux jours également, le soleil va reprendre ses quartiers et faire scintiller notre village et notre moral, nous en avons le plus grand besoin. Le camping ouvre début mai et la saison démarre...

Au cours du dernier conseil municipal, nous avons voté la clôture du budget 2021 et le budget primitif 2022. Comme chaque année, cela a bien occupé l'équipe municipale et le secrétariat au premier trimestre. Les résultats de clôture du budget 2021 reflètent notre volonté et notre détermination de maitriser nos dépenses et d’augmenter nos recettes pour réaliser nos différents investissements. Vous pourrez prendre connaissance des chiffres dans ce bulletin.

Je souhaite vous alerter sur un problème qui nuit à la présentation de notre village : les chats. Je rappelle que quiconque nourrit un chat est considéré comme son propriétaire. Chaque propriétaire d’un ou plusieurs chats a l'obligation de les faire identifier et si besoin stériliser, sous peine d'amende. Pour ceux qui ne sont pas propriétaires de chats, ils ont l'interdiction de les nourrir, de laisser des gamelles de nourriture sur le rebord de leurs fenêtres, sur les bords des murs ou encore sur le trottoir. Cela nuit à l'image de propreté et d’organisation que nous voulons construire pour notre village.

Par ailleurs, il serait important que plus de personnes s’impliquent comme bénévoles dans la vie active du village, Vous pouvez rejoindre nos associations, vous y trouverez une ambiance familiale, chaleureuse, des bons moments de convivialité et de souvenirs.

Tout le monde peut s’engager. Les associations sont accessibles à toutes et tous, indépendamment de l’âge, de la nationalité, de la race, du sexe, de la condition physique, sociale ou matérielle et de l’opinion philosophique, religieuse ou politique.

Le bénévolat, ce n’est pas seulement se rendre utile pour les autres, c'est aussi vous enrichir humainement en vous impliquant, avec d'autres, dans des actions qui ont du sens. Il n'est pas besoin d'y consacrer vos journées. Vous pouvez vous impliquer à votre rythme, de façon ponctuelle ou régulière, selon vos disponibilités.

Alors, nouveaux venus et tous les autres, prenez contact avec nos associations ou contactez-moi à la mairie, je vous mettrai en relation avec les personnes concernées.

Je vous souhaite à toutes et à tous un très bon printemps, vous allez pouvoir préparer vos jardins, le temps est plus clément, nous allons fleurir le village.

Bref à Lalouvesc, en mai faites comme il vous plaît...

Jacques Burriez, Maire de Lalouvesc

## Actualités de la mairie

### Retour sur le budget 2021 et vote du budget 2022

Les comptes 2021 et le budget primitif 2022 ont été votés au Conseil municipal de 2022. Tous les chiffres sont disponibles sur le [compte-rendu du Conseil](https://www.lalouvesc.fr/media/2022-2-conseil-municipal-lalouvesc.pdf).

#### Comptes 2021

En résumé, les comptes de 2021, malgré les difficultés techniques liées à la reprise des comptes de l'eau et de l'assainissement dans le budget général, montrent une situation saine.

Les dépenses avaient été légèrement surévaluées et les recettes sous-évaluées. La trésorerie et le fond de roulement couvrent largement les besoins, malgré la relance des investissements et donc des emprunts bancaires. Les capacités d'autofinancement sont bonnes.

En 2021, les recettes de fonctionnement se sont élevées à 754 k€ et les dépenses à 640,5 k€.

Les recettes d’investissement ont été de 575 k€ comprenant un emprunt de 250 k€, Elles ont permis de financer pour 271,5 k€ pour des acquisitions d'équipement, de travaux de création et d’amélioration des infrastructures,

#### Budget 2022

Compte-tenu de ces résultats satisfaisants, l'orientation du budget 2022 reste la même que celle initiée au début du mandat: orientée résolument vers les investissements, la réparation du village et l'amélioration de son attractivité, tout en maîtrisant de façon rigoureuse le poids de la dette. Du côté du fonctionnement, l'accent est toujours mis sur la chasse aux dépenses inutiles et aux recettes non récoltées, d'autant que cette année l'augmentation des prix de l'énergie risque de peser lourd.

#### Fonctionnement

Le budget de fonctionnement anticipe une augmentation de 11.30% par rapport à  2021. Cette hausse s’explique par l’augmentation des postes énergie-carburant, charges de personnel et subventions allouées. A celle-ci vient s’ajouter une provision pour créances difficiles à recouvrer de 21,7 k€ qui n'avait pas été appliquée jusqu'ici par la Commune et qui doit donc être rattrapée.

Le budget de fonctionnement 2022 s'élève à 963 k€, y compris 332,8 k€ virés à la section d'investissement qui représentent l'autofinancement de la Commune.

#### Investissement

Le budget d'investissement 2022 s'élève à 930,8 k€, y compris les dépenses d'aménagement de l'écolotissement financées par un emprunt de 270 k€ remboursable par la vente des terrains.

Les investissements inscrits au budget 2022 reprennent pour l’essentiel des opérations amorcées en 2021, soit le cimetière, l'opération Beauséjour, l'assainissement et l'écolotissement. Quatre points de moindre importance financière ont été ajoutés : le remontage du Citypark acheté en 2021, l’acquisition d’une machine à laver et séchage qui sera mise en libre-service et servira aussi pour les besoins de la Commune, l’acquisition d’équipements complémentaires pour le tracteur, le réaménagement des points de collectes des poubelles suite au changement décidés par la CCVA.

**Rappelons que ce budget représente le bien commun des Louvetous et que nous ne pourrions tenir nos objectifs budgétaires tout en ayant une vraie ambition pour le développement du village sans leur engagement fort dans les associations et dans l'entretien du village.**

### Le 7 mai, on vous attend à la journée citoyenne !

Plusieurs travaux ont déjà été réalisés par des Louvetous citoyens, comme la récupération du city park, d'autres sont en cours sur les parcours de balade. Mais il y a encore largement de quoi faire pour préparer le village à l'arrivée des estivants : fleurissement du village, réparations et débroussaillage sur le camping, poursuite de la rénovation des parcours, etc.

![](/media/journee-citoyenne-lalouvesc-11-juillet-2020.jpg)

Le maximum d'entre nous doit retrousser ses manches pour le bien de la communauté. C’est seulement au prix d’un effort collectif qu’un village comme le nôtre pourra avancer efficacement et sereinement. C'est pourquoi, comme les années précédentes, **une journée citoyenne est organisée pour nous retrouver toutes et tous, petits et grands, le 7 mai à partir de 9h au camping**. Des équipes seront organisées en fonction des arrivées.

Bien sûr tout le monde au village n'a pas l'énergie pour participer aux travaux, mais vous pourrez aussi participer en venant à midi avec quelques bonnes choses pour prendre le repas en commun. La Mairie offrira une petite collation à l'ensemble des bénévoles. Ce sera l'occasion de fêter joyeusement tous ensemble le printemps, l'ouverture de la saison et de discuter de l'avenir du village !

### Ouverture du camping

Le camping a ouvert ses portes le 30 avril pour la saison 2022. Les réservations sont déjà bien avancées.

Si vous voulez louer un hébergement, dépêchez-vous avant qu'il ne soit trop tard !

### Numérique

{{<grid>}}{{<bloc>}}

Lucas Charel a rejoint la Mairie pour un stage de deux mois après avoir fait une formation de développement web et web mobile à l’École Numérique Ardéchoise au Cheylard.

Il nous aide pour les enquêtes et les développements en cours, notamment pour savoir si nous devons prendre des applications sur smartphone, un panneau numérique, être présents sur les réseaux sociaux... et aussi pour mieux mettre en valeur les parcours de randonnées sur le site web et bien d'autres choses encore.

{{</bloc>}}{{<bloc>}}
![](/media/lucas-chatel-2022.jpg)
{{</bloc>}}{{</grid>}}

### Bientôt du changement sur les espaces de poubelles

{{<grid>}}{{<bloc>}}

![](/media/poubelle-ccva-2022.jpg)

{{</bloc>}}{{<bloc>}}

La CCVA va modifier son système de ramassage des poubelles. Nous aurons l'occasion d'en reparler. Voici, en avant-première à quoi ressembleront les nouveaux bacs.

{{</bloc>}}{{</grid>}}

## Zoom sur l'espace Beauséjour

Après la démolition, la rénovation. L'aménagement de l'espace laissé par l'ancien hôtel ne se fera pas en un jour. L'espoir créé par cette nouvelle ouverture située au centre du village est trop fort pour que l'on ne prenne pas le temps de réfléchir et d'imaginer les solutions les plus pertinentes... et les finances de la Commune sont trop limitées pour que les travaux ne soient pas entrepris sans de solides garanties.

![](/media/demolition-beausejour-2022-53.jpg)  
Reste que la saison estivale approche et il ne faut pas laisser le chantier à l'abandon, ni trop perdre de temps en tergiversations. On trouvera ci-dessous trois initiatives qui nous font avancer :

* une exposition proposée par le Carrefour des arts,
* un échange gagnant-gagnant pour relier l'espace au parc du Val d'Or,
* et, bien sûr, le résultat du concours d'idées sur le Jeu-monument.

### Exposition _D'encre et d'eau_

A partir du 17 mai jusqu'à début novembre, les habitants de Lalouvesc et toutes les personnes qui traverseront le village pourront découvrir une spectaculaire exposition des photographies de Serge Rousse.

Serge Rousse, photographe annonéen, peaufine depuis une dizaine d'années une technique originale. Sa matière est l'eau dans son état solide.

{{<grid>}}{{<bloc>}}

![](/media/affiche-expo-encre-et-eau-2022.jpg)

{{</bloc>}}{{<bloc>}}

> « L'action du froid amplifie son volume, puis se solidifie formant alors des dessins d'éclairs, de fractures ou d'étoiles qui se singularisent par leur graphisme et leur présence inopinée. Dans une seconde étape, des cristaux de sel pourront être disposés sur la glace d'une manière aléatoire ou ciblée : des voies se formeront, des canaux se dessineront sans direction préétablie... La couleur sous forme d'encre aquarelle remplacera la glace devenue eau par l'action du sel, des fusions s'opéreront vers une vision onirique abstraite... » S. Rousse

{{</bloc>}}{{</grid>}}

Le résultat est inattendu, original et magnifique. Dix gabions installés sur l'espace de l'ancien hôtel Beauséjour accueilleront une vingtaine de photographies de un mètre par un mètre cinquante, imprimées sur dibond (plaque composée de plusieurs couches contrecollées). L'ensemble colorera et donnera vie à cet espace en attendant les travaux d'aménagement à venir.

Des originaux des photographies, numérotés et signés, seront en vente au Centre d'Animation Communal dès l'ouverture de l'exposition estivale le 2 juillet 2022.

### Échange gagnant-gagnant

Pour réaliser la liaison entre l'ancienne propriété de l’hôtel Beauséjour et le parc du Val d'Or, il était nécessaire de traverser une parcelle privée appartenant à la copropriété Grosjean. La Commune devait donc disposer d'une surface de terrain supplémentaire d'environ 300 m2 se situant à la pointe de la copropriété Grosjean en contrebas de la parcelle Beauséjour.  
Lors de l'assemblée générale des copropriétaires, présidée par M. Cerizza à laquelle assistait le Maire et le premier adjoint, il a été voté à l'unanimité la proposition suivante :

* la copropriété cède gracieusement le terrain convoité ;
* de son côté, la Commune s'engage à abattre l'ensemble des résineux, côté parking Grosjean, à installer un grillage de bonne qualité délimitant le terrain à partir du mur existant de la propriété Beauséjour jusqu’au portail du bas, côté parc du Val d’Or et à entretenir la tonte de la pelouse du terrain de la copropriété.

Par ailleurs, la Commune sera attentive à la bonne fermeture du portail supérieur à la fin du travail des employés municipaux chaque soir.

Cet échange satisfait tout le monde et est gagnant-gagnant pour les copropriétaires, pour la Commune et pour tous les Louvetous et les visiteurs du village qui disposeront à la fois d'un accès direct du centre-ville sur le parc du Val d'Or et d'une vue complètement dégagée. C'est ainsi que le village avance, avec la volonté de chacun d'aller dans le bon sens.

### Jeu-monument : une première étape franchie

#### Rappel du projet

Après avoir pris l'avis des habitants par [un système de cartes-postales](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/#synth%C3%A8se-des-cartes-postales-sur-le-jeu-monument)...

![](/media/carte-postale-jeu-monument-1.jpg)

... et organisé des [ateliers à l'école St Joseph](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#des-id%C3%A9es-pour-le-jeu-monument),...

![](/media/jeu-monument-ecole-st-joseph-2022-7.jpg)

... la Mairie a lancé un concours d'idées à l'automne 2021 auprès des étudiants en architecture. Voici comment il était présenté dans l'appel envoyé aux écoles :

> Le projet du concours d’idées participe à la mise en valeur touristique, patrimoniale et paysagère d’un site en lien avec le centre du village. Le site deviendra un lieu original attractif pour tous où la nature et la biodiversité seront préservées dans une atmosphère apaisante pour pratiquer toutes sortes de loisirs, un espace multifonctionnel, festif, sportif, ouvert à la détente, à la flânerie et aux jeux.
>
> La démolition d’un l’hôtel en ruine ouvre l’axe principal du village, situé sur un col, sur un versant jusqu’ici négligé, celui des Cévennes, et marque aussi la complémentarité des deux dimensions de son histoire, l’une spirituelle, abstraite, concrétisée par les pèlerinages et la basilique orientée vers les Alpes, l’autre matérielle, plus concrète, marquée par le tourisme vert, la forêt, le bois.
>
> Cette mise en valeur passe par la réalisation d’un [jeu-monument](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/un-jeu-monument-sur-le-parc-du-val-d-or/) sur un parc de jeux existant en contrebas du chantier de démolition de l’hôtel. Les buts principaux poursuivis par la commune sont les suivants :
>
> * accroître l’attractivité du village.
> * mettre en valeur un patrimoine local.
> * valoriser le bois et la forêt, ressource et compétence locales..

Le concours était piloté par un comité technique comprenant trois architectes amis du village (Philippe Bosseau, Jacques Morel et Jérôme Solari), un professionnel du bois du village (Guillaume Besset) et trois élus (l Maire et le premier et le deuxième adjoints).

Le concours visait une première étape, financée par une subvention Leader : un petit jeu-monument, préfigurant un plus grand dont il faudrait à l'avenir trouver les financements.

#### Résultat : _un mur habité_

L'idée des concepteurs est de placer le jeu dans le prolongement du mur de soutènement qui marque la fin de la parcelle Beauséjour et surplombe la parcelle récupérée par la Mairie à la copropriété. Ainsi une terrasse en bois prolonge le surplomb existant. En dessous, la terrasse dessine plusieurs cabanes abritant des jeux : toboggans, échelles, mur d'escalade, etc qui permettent aux enfants de partir à l'assaut de la terrasse ou de descendre au niveau inférieur par des percements. Côté nord un accès creusé en pente douce permet une descente facile pour les adultes, les poucettes ou les personnes à mobilité réduite.

![](/media/jeu-monument-rendu-etudiants-1.jpg)  
La proposition a de nombreux intérêts : elle ne coupe pas la perspective tout en la marquant par quelques signaux en bois visibles tant de la rue des Cévennes que du Val d'Or. Elle concilie les attentes des petits et des grands, elle a une dimension monumentale tout en pouvant être réalisée en différentes phases. Elle a été très appréciée par le comité technique, jury du concours.

Il est toujours difficile de rendre compte d'une idée architecturale, Julie et Guillaume ont réalisé un petit film pour nous éclairer. Par ailleurs, deux posters sont présentés dans le hall de la Mairie.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Wr0Dle0P7HE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Et maintenant...

Le moment est donc venu de préparer la seconde phase des travaux. Un premier nettoyage de l'espace sera réalisé avant la saison. En parallèle un architecte maître d’œuvre va être choisi. Le dossier de subvention Leader sera finalisé avec les devis, d'autres demandes seront aussi déposées et les premiers coups de pioche sont à prévoir pour cet automne.

## Culture - Loisirs

### Une nouvelle Étoffe

Il aura fallu quelques mois d’hiver pour que les idées germent, que la boutique _L’Étoffe_ fasse peau neuve et que tout s’y installe avec harmonie au 16, rue des Cévennes de Lalouvesc.

Simon, artisan tapissier d’ameublement, et sa compagne Magali, décoratrice d’intérieur, ont eu pour projet de monter un concept store en plus de leur métier, qui a pour ligne de conduite la maison, l’écologie, la fabrication française par un choix de produits éthiques.

{{<grid>}}{{<bloc>}}

![](/media/simon-fanget-l-etoffe-2022-1.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/simon-fanget-l-etoffe-2022-2.JPG)

{{</bloc>}}{{</grid>}}

La région Auvergne-Rhône-Alpes y est largement exposée à travers des luminaires, carnets, horloges coussins, tote bags, mugs isothermes, savons, shampoings solides… et aussi une épicerie fine de produits locaux. Des créateurs d’autres régions sont aussi mis en avant. Les visiteurs ont ainsi l'opportunité de se faire plaisir et de faire plaisir en trouvant leur coup de cœur dans les univers représentés tels que les macramés, les tentures murales, coussins, fleurs séchées, jeux, bougies, thés…

On y trouve également une gamme de « _Do It Yourself_ » qui se traduit par « Fais-le toi-même » tel que broderie, macramé, bijoux, accessoires de mode, tout pour recycler un produit et lui donner une seconde vie. Parfait pour laisser libre cours à votre créativité durant vos week-ends, et vos vacances pour d’agréables moments de détente.

Et, bien sûr, vous pourrez admirer le travail de restauration de sièges et meubles de Simon lors de votre visite en boutique.

Imprégnez-vous de cet espace où la sensibilité et le savoir-faire des artisans créateurs français sont mis à l’honneur. On vous dévoile également un secret, des **ateliers sont prévus cet été**, pour cela suivez la page Instagram[ @_letoffe](https://www.instagram.com/_letoffe_/)_

Simon Fanget, tapissier - décorateur  
16 Rue des Cèvennes 07520 Lalouvesc  
07 81 99 18 13

### Un concert d'exception à la Basilique

![](/media/amaury-vassili-2022.jpg)

À l’occasion de ses 10 ans de carrière et de ses 30 ans, Amaury Vassili, véritable performer aux capacités vocales exceptionnelles, poursuit une tournée anniversaire, dans les lieux sacrés. Le 22 mai à 17h, il sera à la Basilique de Lalouvesc.

Il interprète les plus grandes chansons classiques, pop, et s’approprie les plus grands tubes des artistes français mais aussi internationaux tels que Caruso, L’hymme à l’amour, C’est comme ça que je t’aime, My way, Hallelujah, Honesty…

<iframe width="560" height="315" src="https://www.youtube.com/embed/WQoIrsUPySA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Réservation : office de tourisme Lalouvesc (04 75 67 84 20) ou contact@valday-ardeche.com Place 28 € - possibilité d'envoi de billets par courrier à réception de votre réglement par chèque à l'office de tourisme du Val d'Ay Rue Saint Régis 07520 Lalouvesc.

### Des pèlerins d'exception

![](/media/pelerins-fatima-2022.jpg)

Le gite d'étape municipal a abrité le 26 avril quatre pèlerins d'exception. Leur objectif est de réaliser une marche de 6.000 km en partant de Fatima au Portugal jusqu'à Moscou en passant par des sanctuaires, en particulier ceux dédiés à la Vierge Marie, pour favoriser la paix.

L'année dernière, ils sont partis deux mois de Fatima jusqu'à Lourdes en passant par St Jacques. Cette année, ils sont repartis de Lourdes pour atteindre Strasbourg, via Rocamadour, Le Puy, Fourvière, La Salette. Les étapes suivantes passeront par Cologne, Kevelaer, Rostok, Malmö, Stokholm, Helsinki, St Pétersbourg et enfin Moscou.

### Rencontres et activités au club des deux clochers

![](/media/mardi-gras-club-des-2-clochers-2022-1.jpg)

#### Activités

Une semaine bien remplie pour les adhérents.

**Les lundis**

* 14h atelier d'arts créatifs : confection d'objets de décoration,
* 14h15 - 15h15 atelier d'initiation à l'informatique : approche et apprentissage des outils de l'ordinateur et de ses logiciels usuels.

**Les mercredis**

* 14h - 15h atelier mémoire : faire travailler ses méninges par des exercices ludiques, afin de préserver au mieux son état de santé cérébral,

**Les jeudis**

* 14h Réunion hebdomadaire des adhérents : jeux de sociétés, belote et billard, goûter.

**Les vendredis**

* 15 h Jeux de billard : approche, initiation et parties.

Pour ces activités, à l'exception de la réunion hebdomadaire, se faire inscrire auprès de membres du bureau.

#### Rencontres

**Mardi 10 mai**, 12h dans le cadre des échanges fraternels avec les clubs voisins, le Club des deux clochers recevra le Club de Rochepaule. Il est souhaitable qu'un grand nombre d'adhérents participe à cette réception amicale.

**Mercredi 11 mai**  9h30 La société [Proconfort](https://pro-confort-france.fr/) viendra présenter ses produits à Lalouvesc. Cette présentation est ouverte à tous les habitants du village, adhérents ou non. A l'issue de cette présentation, la société Proconfort offre un repas au restaurant à tous les participants dans la limite de 40 personnes. Pour une bonne organisation, il est nécessaire de se faire inscrire au tél 06 62 00 45 69.

Pour tout renseignement, le Président du Club des deux clochers, Georges Ivanez reste à votre disposition 06 62 00 45 69.

### Découvrir l'aviron

![](/media/aviron-2022-1.JPG)

Venez profiter des mois d’été pour découvrir l’aviron.  
Sport complet par excellence, les amateurs de loisirs comme les compétiteurs apprécieront la proximité avec l’eau et plus généralement la nature. Notre terrain de jeu s’étend sur plus de 3km, sur le Rhône entre les villes de Tournon et Tain l’Hermitage. Chacun découvre et pratique l’aviron à son rythme !

Nous accueillons tous les publics, à partir de l’âge de 10 ans. Adhésion Mai-Juin-Juillet : 70 €. Stage d’été : nous consulter.

**Séance initiation samedi 14 mai :**

* **adultes : à 9h**,
* **jeunes : à 14h**,

Base Nautique Tain L’Hermitage Nord.

#### AviFit

{{<grid>}}{{<bloc>}}

L’AviFit est la contraction des termes Aviron et Fitness. En cours collectifs, rythmés et encadrés par un coach formé, cette activité est très complète et adaptée à tous les profils. A la croisée des mondes du fitness et de la préparation physique, l'Avifit sollicite à la fois le corps et l'esprit ! Renforcement musculaire, cardio et bonne humeur sont au rendez-vous.

**Séance initiation le Samedi 14 Mai à 10h**,

Base Nautique Tain L’Hermitage Nord

{{</bloc>}}{{<bloc>}}

![](/media/aviron-2022-2.JPG)

{{</bloc>}}{{</grid>}}

Plus d’info sur notre site [www.avirontaintournon.fr](http://www.avirontaintournon.fr)

Aurélien Raymond

### Bal

{{<grid>}}{{<bloc>}}
![](/media/bal-la-source-2022.jpg)

{{</bloc>}}{{<bloc>}}

**L’association Le Groupe la Source,** compagnie de danse domiciliée à Saint-Félicien, fêtera l’anniversaire de ses 30 ans d'activités par un bal guinguette et des performances de danseurs, musiciens, clowns, le lundi de Pentecôte 6 juin de 19h15 jusqu’à minuit à la salle des fêtes Pouyol à Saint Victor 07410 (à côté du camping).

Entrée ouverte à tous/toutes à 5 € avec une boisson comprise, gratuité pour les enfants de moins de 16 ans.

Possibilité de réserver votre assiette pour 10 € en nous contactant. Buvette sur place.

Véronique Lapsker

{{</bloc>}}{{</grid>}}

## Santé - Sécurité

### La Bio dans les étoiles

La Bio dans les Etoiles, c’est une journée organisée par la Fondation Ekibio et animée par l’association Un Plus Bio, pour découvrir les initiatives de communes qui transforment l'alimentation, une journée pour partager nos expériences et lancer des dynamiques locales autour de l’alimentation. De retour en présentiel à Annonay, c'est l’occasion d’aller à la rencontre d’élus et d’acteurs de la région qui ont acté des changements sur leur territoire avec des temps "OFF" pour se rencontrer en début et fin de matinée autour d’un apéro végétal.

![](/media/bio-dans-les-etoiles-2022.jpg)

_« Comment nourrir demain ? »_ sera le fil rouge de cette rencontre en trois temps :

* une **table ronde avec trois élus locaux** qui portent un changement alimentaire dans leurs communes
* **deux ateliers** au choix sur le foncier et l’éducation alimentaire
* une soirée **ciné-débat**, pour ceux qui souhaitent prolonger les échanges, avec la projection du **documentaire « Douce France »** en présence du réalisateur Geoffrey Couanon.

Retrouvons-nous le **13 mai au théâtre d'Annonay (8h30/16h) et au cinéma Les Nacelles (20h30)**.

Détails sur [le site](https://www.ekibio.fr/la-fondation/la-bio-dans-les-etoiles).

Ségolène Ohl

### Loups et chiens de berger

Vendredi 22 avril avait lieu une réunion d’information sur les chiens de protection à la salle des fêtes de Saint Pierre sur Doux. Une personne de la chambre d'agriculture et un agriculteur, membre de l’association, nous ont présenté le chien de protection des troupeaux.

Aujourd'hui, il faut savoir que le loup est sur notre département. Il a été aperçu sur le Coiron et dans la Haute-Loire à Franc. Pour les agriculteurs, c’est un réel danger à prendre en compte le plus rapidement possible avant que le loup ne soit trop présent.

Il existe beaucoup de solutions, clôture, berger et chien de protection par exemple. Pour l’agriculteur, le chien de protection est la solution la moins coûteuse en temps et financièrement. Placer un chien de protection dans son troupeau n’est pas toujours facile. C’est pourquoi il existe des associations qui proposent différentes aides aux éleveurs afin de bien choisir et bien dresser son chien par exemple.

Le problème de ce chien est la cohabitation sereine dans nos communes touristiques avec les habitants, les randonneurs, les cyclistes… C’est un vrai souci pour les éleveurs car ce chien est la plupart du temps seul avec son troupeau. Il doit apprendre lui-même le discernement entre le danger ou non. Et malheureusement dans certains cas les randonneurs peuvent être pris pour un danger s'ils s'approchent trop près du troupeau par exemple. Et des chiens de cette taille-là peuvent vite effrayer petits et grands.

{{<grid>}}{{<bloc>}}

![](/media/chien-de-berger.jpg)

{{</bloc>}}{{<bloc>}}

Comment se comporter face à un chien de protection ?

* Contournez largement le troupeau et gardez vos distances,
* en présence de chiens de protection, arrêtez-vous le temps qu'ils vous identifient, restez calme, ne les menacez pas, ne les caressez pas,
* descendez de vélo et marchez à côté,
* tenez votre chien en laisse.

{{</bloc>}}{{</grid>}}

Dans certains alpages, il y a des panneaux, des livrets, des réunions d’informations afin d’expliquer les règles pour pouvoir cohabiter. Sur la Commune de Lalouvesc, il n'est pas prévu à notre connaissance de  chien de protection. Ce qu’il faut retenir de cette réunion : il faudra **apprendre la cohabitation** ! Chacun devra faire des efforts de son côté, pour apprendre à vivre avec le loup et ce qui en découle.

Aline Delhomme

### Chasseurs, déclarez vos armes

![](/media/systeme-d-information-sur-les-armes.jpg)  
Depuis le 8 février, les chasseurs détenteurs d’armes doivent obligatoirement créer un compte dans le Système d'information sur les armes (SIA) pour l’ensemble de leurs démarches en matière d’armes, ou au plus tard le 30 juin 2023 en l’absence de nouvelles démarches, à l’adresse suivante : [https://sia.detenteurs.interieur.gouv.fr](https://sia.detenteurs.interieur.gouv.fr "https://sia.detenteurs.interieur.gouv.fr")  
Le SIA s’ouvrira ensuite progressivement aux autres détenteurs d’armes (tireurs sportifs, tireurs de ball-trap et biathlètes, détenteurs d’armes héritées ou collectionneurs).  
Afin d’aider les chasseurs à la création de leur compte, un guide utilisateur a été élaboré et est disponible sur le site Internet de la préfecture de l’Ardèche : [http://www.ardeche.gouv.fr/information-des-usagers-detenteurs-d-armes-a11296.html](http://www.ardeche.gouv.fr/information-des-usagers-detenteurs-d-armes-a11296.html "http://www.ardeche.gouv.fr/information-des-usagers-detenteurs-d-armes-a11296.html")  
Pour les détenteurs d’armes qui auraient des difficultés à créer leur compte (absence de matériel informatique, de connexion Internet ou difficultés dans l’usage de l’outil numérique), la préfecture de l’Ardèche propose à partir du 20 avril 2022 un accompagnement individuel.  
Une permanence physique est mise en place tous les mercredis de 09h00 à 12h00 et de 14h00 à 16h00 à la préfecture de l’Ardèche à PRIVAS  
(rue Pierre Filliat, bâtiment A, salle Olivier de Serres), uniquement sur rendez-vous pris par téléphone au 04.75.66.51.54.

### Feux en Ardèche

Le Préfet a fait le point sur les précautions à prendre pour éviter les départs d'incendies en Ardèche dans [une longue note](/media/note-prefet-sur-les-feux-2022.pdf). On en trouvera ci-dessous quelques extraits.

> En Ardèche, l'année 2021 a été marquée par plusieurs épisodes pluvieux limitant ainsi les départs de feux. La fin de saison estivale a été beaucoup plus sèche. Le département a connu un nombre plus restreint de départs de feux comparativement aux années précédentes :
>
> * nombre de feux en 2021 : 130 (286 en 2020) ;
> * nombre annuel moyen de feux sur 10 ans : 201;
> * superficie brûlée en 2021 : 132 hectares (238 hectares en 2020);
> * superficie brûlée annuelle moyenne sur 10 ans : 260 hectares.
>
> L'une des principales causes de ces incendies de forêts est liée à une tradition encore répandue qui consiste à utiliser le feu pour éliminer certains déchets végétaux. Or, les conditions environnementales dans de nombreux territoires ruraux (embroussaillement, expansion des interfaces village/forêts, changement climatique) ont augmenté les risques dus à l'emploi du feu.

![](/media/carte-feux-ardeche-2022.jpg)

Le brulage des déchets verts est interdit toute l'année aux particuliers, professionnels et collectivités. Les agriculteurs ont une dérogation sauf pour les mois de juillet et août, sous réserve d'une déclaration déposée en Mairie. A titre exceptionnel, les nouveaux propriétaires d'une maison située à moins de 200m de bois ou forêt dont le débroussaillement n'a pas été effectué peuvent aussi obtenir une dérogation en Mairie.

### Suite du calendrier Santé-Environnement

L'ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l'habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante). Pour le mois de mai, la suggestion concerne [l'amiante](https://www.auvergne-rhone-alpes.ars.sante.fr/amiante-1). Un autre calendrier alerte sur les [bonnes pratiques face à l'environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0). Pour le mois de mai, la suggestion concerne [le moustique tigre](https://www.auvergne-rhone-alpes.ars.sante.fr/le-moustique-tigre-vecteur-de-maladies).

{{<grid>}}{{<bloc>}}

![](/media/int-05_amiante.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ext-05_moustique.jpg)

{{</bloc>}}{{</grid>}}

## Dans l'actualité, le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois d'avril qui restent d'actualité.

#### Résultat du second tour de la présidentielle à Lalouvesc

23 avril 2022 Lalouvesc vote à 81% [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/resultat-du-second-tour-de-la-presidentielle-a-lalouvesc/ "Lire Résultat du second tour de la présidentielle à Lalouvesc")

#### Pose du réseau d'assainissement et d'eau potable sur l'écolotissement

16 avril 2022 Avancement du chantier [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/pose-du-reseau-d-assainissement-et-d-eau-potable-sur-l-ecolotissement/ "Lire Pose du réseau d'assainissement et d'eau potable sur l'écolotissement")

#### Vos semis comme un Pro, saison 2

14 avril 2022 Proposé par les Jardins du Haut Vivarais [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vos-semis-comme-un-pro-saison-2/ "Lire Vos semis comme un Pro, saison 2")

#### Vente de plants et légumes par l'APEL

14 avril 2022 Récupérez le bon de commande. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vente-de-plants-et-legumes-par-l-apel/ "Lire Vente de plants et légumes par l'APEL")

#### Compte-rendu du Conseil municipal du 11 avril 2022

14 avril 2022 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/compte-rendu-du-conseil-municipal-du-11-avril-2022/ "Lire Compte-rendu du Conseil municipal du 11 avril 2022")

#### Livret d'accueil pour les Ukrainiens en Ardèche

7 avril 2022 Publié par la Préfecture [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/livret-d-accueil-pour-les-ukrainiens-en-ardeche/ "Lire Livret d'accueil pour les Ukrainiens en Ardèche")

#### Lalouvesc dans le top 5 des événements ardéchois en 2021

5 avril 2022 Publication du bilan de l'ADT [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lalouvesc-dans-le-top-5-des-evenements-ardechois-en-2021/ "Lire Lalouvesc dans le top 5 des événements ardéchois en 2021")

#### Apprivoiser l'ordinateur grâce au Club des deux clochers

4 avril 2022 Tous les lundis de 14h à 15h [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/apprivoiser-l-ordinateur-grace-au-club-des-deux-clochers/ "Lire Apprivoiser l'ordinateur grâce au Club des deux clochers")

#### Un paysage enneigé pour la Ronde Louvetonne

2 avril 2022 70 voitures ont bravé une météo hostile [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/un-paysage-enneige-pour-la-ronde-louvetonne/ "Lire Un paysage enneigé pour la Ronde Louvetonne")

## Puzzle

Autrefois à l'initiative de Mimi Solnon, le printemps à Lalouvesc était salué par la fête des genêts où des chars décorés parcouraient le village. La tradition s'est perdue, mais les genêts fleurissent toujours, comme en témoigne la jolie photo ci-dessous envoyée par Pierre Roger. Vous trouverez facilement où elle a été prise...

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=27707d2afdd2&view=iframe" style="width:100%;height:600px" frameborder=0 allowfullscreen></iframe>