+++
date = 2022-09-30T22:00:00Z
description = "n°27 octobre 2022"
header = "/media/entete-automne.jpg"
icon = ""
subtitle = "n°27 octobre 2022"
title = "27 - Une bonne rentrée"
weight = 1

+++
C'est l'automne, la saison estivale est derrière nous. La vie du village se déplace. Dans ce Bulletin la place d'honneur est laissée à l'école, le cœur battant du village hors-saison. On y parle aussi de l'eau, de l'avancement des chantiers ou dossiers, de l'organisation souterraine la brocante et vous y trouverez encore, comme toujours, bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à[ ce fichier](/media/bulletin-de-lalouvesc-n-27-1er-octobre-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du maire

La saison est terminée, tout s’est bien passé avec un nombre d’estivants très important. Le beau temps était au rendez-vous, enfin une saison ardéchoise normale !

L’heure de la rentrée a sonné. L’école a retrouvé un nombre honorable d’élèves : 23. C’est une réussite due à son dynamisme, comme l’explique ses responsables dans ce Bulletin. Souhaitons-lui de se développer encore.

Pour la reprise d’activité cet automne, confort et solidarité doivent être les maîtres-mots. Nous vous expliquons l’état de la réflexion sur les projets à venir. Il est utile aussi de décrypter les démarches et actions que nous menons, parfois mal connues comme les travaux sur les circuits d’eau potable qui nous causent des soucis.

Notre petit village rural est aussi un haut lieu touristique, à nous d’en prendre la mesure tout en gardant la maîtrise de nos actions.

Beaucoup de tâches nous attendent, plusieurs réunions de concertation sont prévues pour recueillir vos demandes et échanger nos idées. Poursuivons cet élan de solidarité qui nous permet d’avancer dans la bonne direction.

BON AUTOMNE À TOUS !

Amitiés.

Jacques Burriez, Maire de Lalouvesc

## Actualités de la mairie

### L'eau trouble (suite)

Dans le Bulletin du mois précédent nous espérions que les incidents de cet été (coupure d’eau, eau trouble) sur le réseau d'eau ne seraient plus qu'un mauvais souvenir suite aux interventions réalisées. Malheureusement l'eau trouble a persisté.

La solution de ce problème qui impacte une partie de la population est la priorité de la mairie. Des recherches ont été menées conjointement par la Commune et la société Naldéo pour trouver l’explication de ces dysfonctionnements. Quelques fuites ont été repérées et une hypothèse serait que la vétusté de la conduite qui amène l’eau depuis le réservoir du Saint Père sur le village soit à l’origine de l’eau trouble.

Une tentative de solution est en cours. Elle consiste à couper l'alimentation directe du réseau par le réservoir du Saint Père et à réalimenter selon les besoins du village le réservoir du Mont Chaix par la conduite reliant les deux réservoirs. Il faudra quelque temps pour savoir vraiment si cette solution fonctionne car il semble que l'ensemble du réseau soit encrassé.

Quoi qu'il en soit, il ne s'agit que d'un palliatif et nous n'échapperons pas, une fois que nous serons sûrs du diagnostic, à des travaux importants et coûteux. Nous avons fait des demandes et avons obtenu des subventions de l'ordre de 50% pour les travaux à réaliser sur les réseaux d'eau et d'assainissement. C'est bien, mais la somme restant due sera très importante pour le budget d'une petite Commune comme la nôtre. Le Conseil municipal aura donc prochainement des décisions lourdes à prendre.

### Curage de l'étang du camping

L'étang du camping était envahi par les herbes et les algues. Il méritait un bon curage et un aménagement des berges.

![](/media/curage-etang-2022-2.jpg)

![](/media/curage-etang-2022-6.jpg)

Par ailleurs, plusieurs plateformes d'accueil des campeurs étaient bosselées. Elles seront aplanies pour un meilleur confort des visiteurs.

Ces efforts sur le camping, comme ceux précédemment faits avec l'aide des citoyens bénévoles paient. La saison 2022 a été bonne, excellente même, nous en ferons le bilan dans le prochain Bulletin à la fermeture hivernale de l'équipement. Le camping est bien la principale source d'autofinancement de la Commune, comme nous le pressentions.

### Ordures ménagères

Après plusieurs reports, la nouvelle politique du SYTRAD, coordonnée sur notre territoire par la CCVA. se met progressivement en place. Cela commence par la suppression des bacs de 770 litres qui récoltaient les ordures ménagères dans les hameaux et les différents quartiers du village au profit de six points de collecte centralisés de tri :

* Place du Lac,
* Chemin Grosjean,
* Maison de retraite,
* Camping,
* Place des Trois Pigeons,
* Aire du Grand Lieu.

De nouvelles colonnes de tri devaient y être placées courant octobre. La CCVA annonce un nouveau retard de deux mois. En attendant, les bacs de collecte de tri ont été regroupés autour de ces points de collectes afin de réduire le coût du ramassage confié à une société privée.

Trois bacs pour récolter les cartons seront aussi installés. Ils devraient être six à terme (un par point de collecte). En attendant, la récolte hebdomadaire des cartons des commerçants et artisans effectuée par la Commune se poursuivra.

Les discussions au sein de la CCVA sur ces questions ne sont pas toujours faciles et la mise en place de la nouvelle organisation est difficile.

#### Le retard français pour la gestion des déchets

Notre situation n'est malheureusement pas une exception dans le pays.

![](/media/rapport-cour-des-comptes-dechets-menagers-2022-1.jpg)

Un tout récent [rapport de la Cour des Comptes](https://www.ccomptes.fr/fr/publications/prevention-collecte-et-traitement-des-dechets-menagers) montre que la France est en retard sur la gestion des déchets. Les objectifs affichés sont très loin d'avoir été atteints et le pays fait pale figure comparé à l'Allemagne, l'Autriche, les Pays-Bas.... La Cour dénonce notamment des acteurs _« insuffisamment coordonnés »_, un dispositif de suivi _« toujours défaillant »_ et un financement _« peu lisible et trop faiblement incitatif »._

![](/media/rapport-cour-des-comptes-dechets-menagers-2022-2.jpg)

D'autres changements sont annoncés pour les années à venir, avec notamment l'obligation pour les Communes d'organiser la gestion des déchets organiques (compost) qui doit être mise en place avant le 31 décembre 2023. Espérons que les décisions futures pourront être prises à la CCVA dans une meilleure concertation.

En attendant, il est plus que jamais essentiel de bien trier nos déchets. [Relire une visite du centre de tri](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/25-musique-maestro/#visite-dun-centre-de-tri-des-d%C3%A9chets) dans le Bulletin d'août.

### Espace Beauséjour

Suite à la proposition de la société Archipolis, maître d’œuvre sur l'aménagement de l'espace Beauséjour et aux différentes consultations, le Conseil a accepté l'avant-projet présenté. Voir ci-dessous.

![](/media/espace-beausejour-22-09-08-p2-aps-ind-2.jpg)

Archipolis doit maintenant faire parvenir un avant-projet définitif et un chiffrage. Ce n'est qu'à partir de ces éléments que les dossiers de demandes de subvention pourront être rédigés et déposés.

Il est à prévoir qu'en fonction des budgets réunis la réalisation subira encore des modifications. Si tout va bien, le chantier démarrera en 2023 et s'achèvera en 2024. Il faut pour une telle réalisation qui transformera le centre du village patience, énergie et obstination.

### Cénacle et Sainte Monique

Dans le cadre de l’étude initiée par le Syndicat mixte des Rives du Rhône (SCOT) sur les bâtiments du Cénacle et de Ste Monique un atelier en deux parties a été organisé le 20 septembre à la Mairie de Lalouvesc.

Dans une première partie réunissant les élus de la Commune, de la CCVA et de quelques Communes voisines, les bureaux d’études ont présenté un [diagnostic](/media/ue220210-scot-lalouvesc-atelier-01-220920.pdf) : enjeux à l’échelle de la Commune et du site, atouts et difficultés liées aux bâtiments, premières pistes programmatiques à creuser.

![](/media/ste-monique-cenacle-scot-atelier-1-2022.jpg)

La seconde partie réunissait des représentants de la population : membres du Comité de développement, restaurateurs, responsables d’associations. Il s’agissait de recueillir les avis et les besoins relatifs aux deux sites.

![](/media/atelier-1-scot-2022.jpg)

Un deuxième atelier doit se tenir le 14 novembre où des scénarios d’exploitation seront présentés.  
Rappel : cette étude est entièrement financée par le SCOT.

### Pourquoi la taxe foncière augmente ?

Les propriétaires de maison et de terrain sur la Commune ont reçu l'avis d'impôt relatif à la taxe foncière 2022. Nous avons tous constaté une augmentation et pourtant ni le Conseil municipal, ni la Communauté de Communes, ni le Sytrad n'ont relevé leur taux. Comment alors expliquer cette augmentation ?

En réalité, si les taux d'imposition n'ont pas augmenté (sauf sur certaines taxes plus marginales), l'assiette sur laquelle ils sont calculés, c'est-à-dire la valeur locative des biens taxés, a, elle, bien augmenté. Cette valeur est réactualisée chaque année par application d’un coefficient forfaitaire, le même pour tous les logements quels que soient leur localisation, leur état d’entretien et la situation du marché local. Ce coefficient dépend de l'inflation.

Ces dernières années, l'inflation était très faible et le coefficient ne variait pratiquement pas. La situation est différente cette année où l'inflation est repartie à la hausse en 2021. Ainsi, la base de calcul de la taxe foncière a subi une hausse sans précédent : elle augmente de 3,4 % cette année, entraînant mécaniquement une augmentation des taxes en proportion.

## Zoom sur l'école St Joseph

L’école Saint-Joseph est aujourd’hui l’unique école du village. Lalouvesc a une longue tradition d’accueil d’enfants : les bâtiments Sainte-Monique et Saint-Augustin en sont, par exemple, de précieux témoins. Chaque jour, à 10h, les rires des enfants de l’école Saint-Joseph, sortant pour la récréation, nous rappellent que cet héritage est encore bien vivant, et qu’il nous revient à tous d’assurer son avenir !

A l’occasion de la rentrée des classes, Vincent Trébuchet, trésorier de l’organisme de gestion de l’école, et Sasha Haon, nouvelle cheffe d’établissement, nous en disent plus sur l’école Saint-Joseph.

![](/media/img-ecole-2022-1.jpg)

> ### Vincent, comment fonctionne l’école Saint-Joseph ?
>
> Elle est sous la tutelle des sœurs de Saint-Joseph, une des nombreuses congrégations anciennement présentes dans le village, et elle occupe une partie de leur ancien couvent, ce grand bâtiment à gauche de la basilique.
>
> Elle fonctionne comme toutes les écoles catholiques sous contrat d’association avec l’État : elle est sous la responsabilité du diocèse et des sœurs de Saint-Joseph mais son personnel enseignant est rémunéré par l’éducation nationale, dont l’école suit les programmes. Au quotidien, c’est l’OGEC (Organisme de Gestion de l’École Catholique), composé de bénévoles, qui assure la gestion économique, financière et sociale de l’école et emploie le reste du personnel, pour que l’équipe enseignante puisse exercer son métier sereinement. L’OGEC est soutenu par la Mairie et par l’APEL (Association de Parents d’élèves de l’Enseignement Libre) qui organise des événements de collecte de fonds au cours de l’année.
>
> L’équipe pédagogique est composée de la cheffe d’établissement, enseignante à plein temps, d’un mi-temps, assuré par deux enseignantes, et d’une ASEM (Agent Spécialisé des Écoles Maternelles). Un élève bénéficie également de l’accompagnement d’une AESH (Accompagnant d’Élève en Situation de Handicap).
>
> L’école scolarise aujourd’hui 23 élèves de la toute petite section au CM2, au sein d’une classe unique, chacun bénéficiant ainsi d’un accompagnement personnalisé.
>
> #### Quelle est la spécificité de cette école ?
>
> C’est une école catholique, ouverte à tous. Conformément aux statuts de l’enseignement catholique, l’école s’efforce donc de vivre de « _la pensée sociale de l’Église : en matière de promotion du bien commun, de subsidiarité et de responsabilités partagées, de justice dans les rapports entre les personnes et dans le dialogue social, de solidarité avec les autres écoles catholiques et de service des moins favorisés dans la société_ ».
>
> A Lalouvesc, cela s’incarne dans l’énergie déployée par toutes les parties prenantes (diocèse, tutelle, mairie, bénévoles, parents) pour maintenir la présence d’une école de village, contre vents et marées, là où bien d’autres ont fermé. Maintenir la transmission du savoir au plus proche des familles, et accompagner celles qui peuvent être en difficulté, s’inscrit pleinement dans le souci du bien commun et de la solidarité envers les moins favorisés.

> ![](/media/img-ecole2022-2.jpg)
>
> #### Conserver vivante une petite école est un défi
>
> Oui, le premier défi est financier. L’école a connu des années difficiles avec la suppression en 2017/2018 des emplois aidés, qui a conduit à la fermeture de milliers d’écoles comme la nôtre. C’est grâce à l’esprit de responsabilité immédiat de la mairie que l’école a survécu. Le dialogue persévérant entre l’OGEC et la nouvelle équipe municipale a permis d’aboutir à une convention qui pérennise durablement ce soutien.
>
> Le deuxième est la continuité pédagogique. Dans une petite école comme la nôtre, chaque changement de direction peut avoir des conséquences importantes. C’est grâce à l’accompagnement de la tutelle et de l’OGEC, très vigilants dans le recrutement et la transition, que la continuité pédagogique est assurée pour une meilleure progression des élèves, tout en laissant pleine latitude à l’enseignant pour exprimer ses talents propres.
>
> Le troisième et principal défi, qui est la vraie réponse aux deux précédents, est humain: il s’agit de maintenir et de faire croître le nombre d’élèves. En trois ans, l’école est passée de 12 élèves à 23, et attire même des élèves d’autres communes. Une école attractive par son sérieux, sa pédagogie et son niveau ; un village qui rayonne de la vie de multiples commerces et associations ; une mairie proche des familles et attentive à leur préoccupations (emploi, logement, garde d’enfant) : tels sont les ingrédients pour que ce cercle vertueux continue !

> ![](/media/img-ecole2022-3.jpg)
>
> #### Vous parlez d’un cercle vertueux : la présence de l’école est essentielle pour le village
>
> L’école est un bien commun du village.
>
> Elle est sa mémoire, car elle existe aujourd’hui grâce aux générations passées de Louvetous qui se sont dévouées pour la sauvegarder. Créer une école comme celle-ci aujourd’hui ne serait plus possible.
>
> Elle est aussi un signe de sa vitalité : la dizaine de petits qui traverse le village en gilet jaune chaque jour pour se rendre à la cantine de l’EHPAD est un de ces signes qui nous montrent que Lalouvesc est bien vivante, que ces petits Louvetous d’aujourd’hui prendront la relève.
>
> Elle est surtout l’avenir du village, car sa présence est essentielle à l’installation de nouvelles familles. Un village dont l’école ferme est condamné à se dévitaliser petit à petit. A l’inverse, un village comme Lalouvesc, avec tous ses atouts, touristiques et naturels, s’il conserve son école, devient un lieu potentiel de vie, où des gens peuvent envisager de s’installer à l’année.

> ![](/media/img-ecole2022-4.jpg)
>
> #### Comment les Louvetous peuvent-ils vous aider ?
>
> Tous les talents peuvent trouver à s’exprimer :
>
> * L’OGEC recherche régulièrement des personnes prêtes à s’engager pour trois ans pour des tâches précises (trésorerie, secrétariat).
> * Le bâtiment qui abrite l’école a besoin d’entretien et tous les bricoleurs qualifiés peuvent se manifester pour aider ponctuellement (plomberie, menuiserie, la liste est longue).
> * L’équipe enseignante assure l’ouverture culturelle la plus large possible aux enfants : si vous avez des talents ou des connaissances (musicaux, artistiques, découverte de la nature, etc.) à partager, n’hésitez pas.
> * L’APEL organise tout au long de l’année des événements pour récolter des fonds pour les activités scolaires (vente de plants, stand, brocante, etc.). Les idées ou bonnes volontés sont les bienvenues.
> * Enfin, il peut arriver que certains enfants ou familles soient en difficultés. Les personnes qualifiées pouvant fournir un soutien humain ou scolaire sont les bienvenues.
>
> N’hésitez pas à vous adresser à Sasha, la nouvelle cheffe d’établissement (ecolestjosephlalouvesc@gmail.com) ! Un grand merci d’avance !

> ![](/media/img-ecole2022-5.jpg)
>
> ### Sasha, comment avez-vous connu Lalouvesc et notre école ?
>
> Fraîchement diplômée, j’ai débuté dans un établissement de la commune de Charmes-sur-Rhône dans laquelle j’étais enseignante en classe de CP-CE1. En mars 2022, il a été question du mouvement de l’emploi pour les enseignants (classification de vœux des postes à pourvoir pour l’année scolaire 2022-2023). Motivée par les nouveaux défis, l’envie d’évoluer professionnellement et personnellement et par amour pour mon département d’origine, l’Ardèche, j’ai postulé au poste de chef d’établissement de l’école privée Saint-Joseph de Lalouvesc.
>
> #### Qu’est-ce qui vous a plu à l’école Saint-Joseph ?
>
> La formation que je suivais à ce moment-là, m’a permis d’aller à la rencontre de la cheffe d’établissement en poste, Amandine Verdun, et des enfants qui seraient peut-être mes élèves en septembre 2022. L’école de Lalouvesc, chaleureuse et accueillante, m’a tout de suite séduite. Les pédagogies d’inspiration Freinet (tâtonnement de l’enfant évoluant par essais-erreurs, respect de l’élan venant de l’enfant qui le rend curieux d’expérimenter, importance de l’expression des émotions) et Montessori (confiance en soi, autonomie, expérimentation et apprentissages en douceur) implantées dans l’établissement correspondaient en tout point à ma vision de l’enseignement d’aujourd’hui. De plus, la classe unique m’a laissé entrevoir tout un éventail de projets à mettre en place pour les années suivantes. C’est fin juin 2022 que j’ai appris la bonne nouvelle : pour la rentrée scolaire 2022-2023, j’y serai !

> ![](/media/img-ecole2022-6.jpg)
>
> #### Quelles sont vos premières impressions ?
>
> La classe unique fait peur sur le papier. On imagine qu’on va être seule... Et pourtant ! Sortant d’une école de huit classes avec une grande équipe, j’ai l’impression d’être davantage entourée en étant à Lalouvesc : mairie, parents, paroisse, tutelle, intervenants extérieurs, diocèse, chefs d’établissements du département ; de nombreux partenaires qui travaillent ensemble au service des enfants. Bien loin de l’image que l’on a de la société d’aujourd’hui, l’entraide et la coopération sont des notions qui prennent tout leur sens lorsque l’on travaille à Lalouvesc. Des élèves curieux et ayant envie d’apprendre, une équipe pédagogique et des partenaires tournés vers le bien-être et la progression des élèves, tout cela dans un cadre environnemental riche ; voilà ce que j’ai trouvé à l’école Saint-Joseph de Lalouvesc.
>
> #### Quels sont vos projets pour l’année scolaire qui s’ouvre ?
>
> L’école est déjà en ordre de marche et les enfants continuent de progresser dans les savoirs fondamentaux, dans la continuité des années précédentes. Pas de classe de découverte prévue pour cette année scolaire mais des interventions pédagogiques : pour la semaine du goût par exemple (mi-octobre), les enfants recevront la visite du chef Régis Marcon. En termes de projet, j’ai envie de partager avec vous celui d’une « nuit à l’école ». Celui-ci est en cours d’élaboration avec l’équipe pédagogique et sera organisé au retour des beaux jours.
>
> Depuis septembre 2022, la nouvelle équipe enseignante prend ses marques. Deux enseignantes interviennent un jour par semaine à l’école : Pauline Lafond et Elodie Mathieu. Véronique Petit, ASEM, et Mathilde Groud, AESH, viennent apporter leurs expériences passées à cette équipe. On apprend à se connaître, à connaître les élèves et on se familiarise avec le fonctionnement induit par la classe unique. Des idées, vous l’aurez compris, l’équipe n’en manque pas.

> ![](/media/img-ecole2022-7.jpg)
>
> **On vous tiendra au courant en vous partageant ce que l’on vit dans notre école dans chaque bulletin mensuel de Lalouvesc.**
>
> **Bonne rentrée à tous !**

## Loisir - Culture

### Brocante, une chaîne de solidarité

La brocante est passée et s’est bien passée ! Très bien passée !

![](/media/brocante-2022-7.jpg)

Tout a été dit ou presque. Mais insistons sur cette grande chaîne de solidarité et de convivialité dont tous les maillons sont solides et indispensables. Voici donc le descriptif de cette belle et grande chaîne aux maillons solides.

La fidélité de Mme Bellenger dans la mise à disposition de son pré a permis une nouvelle fois à quarante exposants de bénéficier d’un espace verdoyant.

Le défi du stationnement ce jour-là a bien été compris par Mrs Delhomme et Poinard. Si le premier a proposé son parking sur les hauteurs du village, celui mis à disposition par le second a facilité le stationnement à toute proximité du village. Plusieurs centaines de visiteurs en ont profité.

Les pères jésuites, de leur côté, ont ouvert grand le portail de la Maison Saint Régis afin que les exposants qui ont testé cette année l’extension sur le chemin montant puissent garer leur véhicule. L’étroitesse de la route leur interdisait de le garder derrière leur stand.

Nombreux sont les résidents du lotissement du Val d’Or qui ont accepté de voir leur espace vital réduit et quelque peu envahi pour une journée en nous accordant la possibilité d’installer un exposant devant leur maison.

Les caprices électriques de notre super-friteuse ont été résolus par le prêt d’un groupe électrogène de la société Peyrard (43 Riotord). Mickaël et Inès n’ont pas hésité à prêter depuis le samedi leur véhicule réfrigéré. Ainsi la boisson de la buvette a été tenue au frais.

L’Office du Tourisme a gentiment proposé un vélo électrique à nos garçons (pourtant vaillants). À 4h du matin dur dur de sillonner sans relâche le site pour accompagner les exposants jusqu’à leur emplacement. Grâce aux pompiers, la sécurité de tous a été vérifiée dès le matin par le passage du camion conduit par Corentin.

Nous n’oublions pas toute l’équipe municipale. Toujours à notre écoute, toujours présente à nos côtés, force de propositions. Ainsi le site mis à notre disposition est d’année en année toujours mieux organisé et… élargi !

**Merci à tous !** Les bénévoles ne déméritent pas, mais sans tous ces soutiens annexes et discrets la réussite de notre brocante ne serait pas ce qu’elle est.

![](/media/brocante-1.jpg)

Nous sommes à fond sur le dossier TRAIL DES SAPINS mais ça ne nous empêche pas de nous projeter sur le 3 septembre 2023.

**Ça sera la vingtième édition de la brocante ! Un bel anniversaire à fêter dignement. Nous sommes preneurs de témoignages, informations, photos sur ses premiers temps. À vos archives, à vos souvenirs !**

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

### Trésors de la nature au centre de Lalouvesc

Extrait du compte-rendu par Séverine Moulin de la fête de la science coordonnée par l’Office de tourisme du Val d’Ay.

> L’espace Beauséjour et le jardin d’enfants du parc du Val d'Or ont été scrutés samedi 24 septembre à Lalouvesc par Françoise Kunstmann. Son panier à la main avec ciseaux, gants et couteau, elle a partagé ses connaissances.
>
> {{<grid>}}{{<bloc>}}
>
> A l’espace Beauséjour, de belles [vesces craca](https://fr.wikipedia.org/wiki/Vicia_cracca) voisinent avec le [chénopode alba](https://fr.wikipedia.org/wiki/Chenopodium_album), l’[armoise ou artémise](https://fr.wikipedia.org/wiki/Artemisia_vulgaris), la [renouée des oiseaux](https://fr.wikipedia.org/wiki/Renou%C3%A9e_des_oiseaux), la [vergerette du Canada](https://fr.wikipedia.org/wiki/Conyza_canadensis) (diurétique, vermifuge), la[ vulnéraire ](https://fr.wikipedia.org/wiki/Vuln%C3%A9raire)(cicatrisante, antitussive).  
> On trouve le [laiteron maraîcher](https://fr.wikipedia.org/wiki/Sonchus_oleraceus) à l’entrée du jardin d’enfants qu'il ne faut pas confondre avec le pissenlit, (1.200 variétés identifiées, tige creuse) dont la fleur est préparée en gelée ou vin, ou émiettée en salade. Ici aussi [la chicorée](https://fr.wikipedia.org/wiki/Chicor%C3%A9e), l’[onagre](https://fr.wikipedia.org/wiki/Onagre_bisannuelle) (goût de jambon), l’[achillée](https://fr.wikipedia.org/wiki/Achill%C3%A9e), le serpolet parfumé, et l’ortie délicieuse qui peuvent être cuisinés en quiche, tarte, soupe, beurre, et les graines en chapelure.  
> La pâquerette en macérat huileux (30 jours) est riche en oméga 6, régénère la peau… et bien d'autres plantes ont été repérées par la botaniste.
>
> (voir aussi une recette spéciale en fin de Bulletin)
>
> {{</bloc>}}{{<bloc>}}
>
> ![](/media/francoise-kunstmann-fete-de-la-science-2022.jpeg)
>
> {{</bloc>}}{{</grid>}}

Au programme de la fête de la science à Lalouvesc, il y avait aussi Serge Roussé qui a présenté la technique de ses photos exposées sur l'espace Beauséjour.

### Concours de belote au Club des deux clochers

{{<grid>}}{{<bloc>}}

![](/media/concours-belote-club-des-2-clochers-2022.jpg)

{{</bloc>}}{{<bloc>}}

Le Club des deux clochers organise son concours de belote annuel le  dimanche 23 octobre 2022. Toutes les doublettes seront primées. Cette  manifestation amicale se déroulera au Centre d'animation communale.  
Gagnant ou perdant, une buvette et un buffet vous permettront de passer une après-midi ludique et joyeuse.

{{</bloc>}}{{</grid>}}

### Programme du Centre de loisirs - Vacances d'automne

Pendant les vacances d'automne, les enfants de 3 à 11 ans sont invités à la découverte du Royaume, mais pas n'importe quel Royaume, celui de Jaloine. Lors de cette semaine, fortement inspirée du célèbre jeu télévisé, les enfants devront faire face à de nombreuses épreuves. Cohésion et esprit d'équipe seront les maîtres mots...

Inscriptions ouvertes jusqu'au 10 octobre. [Fiche d'inscription](/media/fiche-inscriptions-vacances-automne-2022-jaloine.pdf).

Renseignements au 07 66 49 07 90 ou cdl.jaloine@gmail.com

![](/media/prg-vacances-automne-jaloine1.jpg)

![](/media/prg-vacances-automne-jaloine2.jpg)

## Santé - Sécurité

### Opération Brioche ADAPEI, 9 octobre

Acheter une brioche, c’est se faire un petit plaisir, mais c’est surtout soutenir les actions concrètes et locales menées par l’Adapei 07.

A Lalouvesc, les bénévoles vous attendent le dimanche 9 octobre devant la mairie de 9h à 12h. Tenant compte du succès de l'année dernière, 100 brioches ont été commandées cette année !  
“Nous aider à gagner nos combats, c'est simple comme une brioche”.

#### Qu’est-ce que l’Adapei ?

[L’Adapei 07](https://www.adapei07.fr/) accueille et accompagne tout au long de de la vie, des personnes en situation de handicap, dans ses établissements comme en milieu ordinaire. Elle agit pour que toute personne déficiente intellectuelle, avec ou sans troubles associés, dispose d’une solution d’accueil, d’accompagnement et qu’elle soit partie prenante d’une société inclusive et solidaire.  
L’Adapei 07 défend également les droits des familles pour que la survenue du handicap ne soit pas synonyme d’exclusion sociale.

Chaque année, en octobre, des bénévoles se mobilisent pour vendre des brioches au profit des personnes handicapées. Grâce à la générosité des habitants, des investissements peuvent être réalisés au profit des personnes prises en charge dans les structures de l’ADAPEI.

### Aidants

{{<grid>}}{{<bloc>}}

![](/media/aidants-programme-fin-2022.jpg)

{{</bloc>}}{{<bloc>}}

L'heure de la rentrée a sonné !  
Le collectif KANLARELA est heureux de vous retrouver et vous propose son programme d'activités artistiques et de bien-être à destination des personnes atteintes de maladies neuro-évolutives, en situation de handicap ou d'isolement et à leurs proches aidants d'octobre à décembre 2022.  
Nous vous attendrons à partir du 29 septembre tous les jeudis de 14h30 à 16h30 dans l'ancien couvent (en face de l'hôpital) à St Félicien.  
Retrouvez-nous aussi le jeudi 6 octobre 2022, à l'occasion de la Journée Nationale des Aidants, de 9h à 12h dans le hall de l'hôpital de St Félicien aux côtés d'Arche Agglo et de l'Escale sur notre stand d'information sur l'offre locale d'aide et de soutien aux aidants.

{{</bloc>}}{{</grid>}}

### Suite du calendrier Santé-Environnement

L’ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l’habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante). Pour le mois d'octobre, la suggestion concerne le [monoxyde de carbone](https://www.auvergne-rhone-alpes.ars.sante.fr/monoxyde-de-carbone). Un autre calendrier alerte sur les [bonnes pratiques face à l’environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0). Pour le mois de septembre, la suggestion concerne les [risques auditifs](https://www.auvergne-rhone-alpes.ars.sante.fr/le-bruit-0?parent=5375).

{{<grid>}}{{<bloc>}}

![](/media/int-10_monoxyde.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ext-10_audition.jpg)

{{</bloc>}}{{</grid>}}

## Dans l'actualité, le mois dernier

#### Périodes d'ouverture de la chasse (rappel)

21 septembre 2022 La chasse est ouverte, sauf le mardi [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/periodes-d-ouverure-de-la-chasse-rappel/ "Lire Périodes d'ouverture de la chasse (rappel)")

#### Le Trail des Sapins : le temps des inscriptions

21 septembre 2022 Une compétition et une ambiance [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-trail-des-sapins-le-temps-des-inscriptions/ "Lire Le Trail des Sapins : le temps des inscriptions")

#### Réorganisation de la collecte des ordures

20 septembre 2022 Centralisation des bacs de tri [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/reorganisation-de-la-collecte-des-ordures/ "Lire Réorganisation de la collecte des ordures")

#### La Mairie recrute

8 septembre 2022 Un agent technique polyvalent [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-mairie-recrute-1/ "Lire La Mairie recrute")

#### Beau temps et affluence record pour la brocante 2022

3 septembre 2022 Bravo et merci au Comité des fêtes ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beau-temps-et-affluence-record-pour-la-brocante-2022/ "Lire Beau temps et affluence record pour la brocante 2022")

#### Le club des deux clochers reprend ses activités

1 septembre 2022 Rendez-vous au CAC le 8 septembre [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-club-des-deux-clochers-reprend-ses-activites/ "Lire Le club des deux clochers reprend ses activités")

## Recette de bouillabaisse louvetonne

Suggestion de saison par Françoise Kunstmann aux participants à la fête de la science.

Attention, soyez attentifs dans votre cueillette. Toutes les plantes ne sont pas comestibles et certaines se ressemblent.

Cuire pendant 40mn :

* plantain,
* fenouil,
* cistre,
* grande berce,
* renouée bistorte,
* laiteron,
* ortie

Ajouter en dernière minute des filaments de safran. A accompagner d’œufs, et de pain grillé aillé.