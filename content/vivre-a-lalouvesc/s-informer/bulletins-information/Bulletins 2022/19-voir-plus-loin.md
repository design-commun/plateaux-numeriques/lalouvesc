+++
date = 2022-01-29T23:00:00Z
description = "n°19 Février 2022"
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "n°19 Février 2022"
title = "19 - Voir plus loin"
weight = 1

+++
Avec la démolition de l'hôtel Beauséjour de nouvelles perspectives, au sens propre comme au figuré, se sont ouvertes pour le village. L'hôtel Beauséjour, son histoire et l'avenir de la place qu'il laisse, occupent une bonne part de ce Bulletin. Mais on y parle aussi de l'adressage, des prochaines journées citoyennes, de la Vie Tara, du dernier épisode du feuilleton des enfants... et, comme toujours, de bien d'autres choses encore.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-19-1er-fevrier-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Mot du maire

Eh bien ! voilà c'est fait ! Plus de ruine, plus de rideaux volants aux quatre vents, une place nette est faite. Ce dossier, cher à mon prédécesseur, bien démarré, nous l'avons terminé, tout le monde est satisfait.

Maintenant place à la réflexion, il faut bosser pour trouver le projet final pour créer une liaison douce entre le centre bourg et le parc du val d'or... La gestion urbaine est une discipline complexe, elle est responsable de la ville du futur et doit offrir aux habitants une solide base matérielle ainsi qu'une ambiance culturelle tranquille, harmonieuse, vivante et solidaire.

Du côté du lotissement, les choses vont bon train. Cette semaine, réunion de début de chantier avec le maître d'œuvre et l'entrepreneur choisis pour effectuer les travaux, courant février le premier coup de pelle sera donné.

La Vie Tara renaît. Félicitations aux nouveaux propriétaires ! Romain et Ine, bienvenue dans notre village et tous nos vœux de réussite et de prospérité !

Malheureusement des figures du village nous ont quittés, Alphonse Abrial, Yvonne Osternaud. Mon Conseil se joint à moi pour présenter toutes nos condoléances aux familles. Nous avons perdu également Maurice Richaud, qui n'était pas originaire du village mais son cœur y battait, une tendre pensée pour lui et sa famille.

Si tout va bien nous allons petit à petit sortir des problèmes liés à la pandémie. Alors courage ! Imaginons une prochaine saison estivale sans restriction, juste avec les gestes barrière.

Gardons notre motivation, c’est l’énergie qui nous anime. Sans elle, tout semble fade, gris, sans relief et sans intérêt...

Organisons nos journées citoyennes. Quoi de plus beau que l’unité d’un village ! Mobilisons-nous vers un avenir meilleur autour du bien commun, du bien vivre ensemble.

À bientôt mes amis,

Jacques Burriez, Maire de Lalouvesc

## Actualités de la mairie

### L'adressage est bouclé

Ce fut un lourd travail pour les élus chargés du dossier, il a fallu vérifier toutes les adresses une à une, ajouter ou corriger celles qui le devaient, renommer quelques voies. 383 numéros et 43 voies ont été répertoriés pour le village.

De son côté, la société Signaconcept a pointé et géolocalisé toutes les adresses. Elle a aussi repéré et photographié tous les emplacements de panneaux manquants.

Mais ça y est, l'ensemble des adresses ont été déposées dans la base de données nationale qui sert de référence pour toutes les cartes, les systèmes de géolocalisation ou les divers adressages.

Les propriétaires vont recevoir courant février un courrier précisant leur adresse postale. La plupart de celles qui existaient avec un numéro n'ont pas été modifiées. Pour le centre-bourg, la numérotation ancienne en continu a été maintenue, mais ailleurs, la numérotation métrique (le nombre de mètres depuis le début de la voie) a été privilégiée car elle est plus commode pour se repérer et facilite les évolutions. Progressivement, les employés municipaux vont compléter les numéros et les noms de voies manquants sur les maisons et aux embranchements.

![](/media/adressage-2022.jpg)

Si vous êtes pressés ou curieux, vous pouvez consulter dès maintenant vous-même la [base de données](https://adresse.data.gouv.fr/base-adresse-nationale/07128#11.88/45.117/4.5215/0/2) et repérer votre adresse.

### Préparation des journées citoyennes 2022 : réparer les parcours thématiques

Un village comme Lalouvesc ne peut se développer sans un engagement fort de ses habitants pour le bien commun.

Il y a à Lalouvesc plusieurs parcours thématiques qui offrent de courtes balades entre une et deux heures et sont très appréciés des familles : le chemin des Afars, le parcours botanique, le chemin des champignons, le chemin des lapins, le parcours sportif, etc.

Mais les chemins et les installations se sont dégradés avec le temps et méritent un sérieux effort de rénovation. D’autres parcours pourraient aussi être proposés. Le départ de tous les parcours pourrait être sur le Parc du Val d'Or, au pied du futur Jeu-monument.

C’est pourquoi le Comité de développement du village, qui réunit des élus et des non-élus, a suggéré que les journées citoyennes 2022 soient consacrées à réviser et réparer l’ensemble des parcours thématiques.

{{<grid>}}{{<bloc>}}

![](/media/journee-citoyenne-jour-3-4.jpg)

{{</bloc>}}{{<bloc>}}

Nous pouvons nous appuyer sur la réussite des [précédentes journées](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/des-journees-citoyennes-pour-le-camping/),  sur la rénovation spectaculaire du camping... et aussi tirer les leçons des limites de la participation. Si les citoyens engagés ont été fiers du travail accompli, ils auraient pu être plus nombreux.

Pour les journées 2022, tous les citoyens, toutes les associations,  l’école, l’EPHAD, l’OT, les pompiers, le Sanctuaire, les commerçants, les artisans... toutes les forces vives du village doivent s'engager, chacun, bien sûr, dans la mesure de ses moyens. Ce n'est qu'avec l'implication de tous dans le village que nous pourrons durablement avancer. Nous profitons tous des réparations, il est naturel et conséquent que chacun y apporte sa pierre.

{{</bloc>}}{{</grid>}}

Une réunion de préparation est prévue prochainement.

### Lancement du chantier de l'écolotissement

Le piquetage a été effectué sur le terrain de l'écolotissement du Bois de Versailles. Les espaces sont délimités. Les travaux d'aménagement vont bientôt démarrer. Ils seront réalisés par l'entreprise Faurie, choisie après un appel d'offres.

Le chantier coûtera environ 270 000 €, financé par un emprunt qui doit être entièrement remboursé par la vente des lots. Quatre compromis de vente ont déjà été signés, c'était la condition que nous avions mis au démarrage des travaux. Une fois l'aménagement terminé, nous pourrons accélérer la promotion pour compléter le lotissement le plus tôt possible et rembourser l'emprunt.

### Enquête sur l'information locale

Grâce à une subvention obtenue dans le cadre du dispositif "Guichet numérique" une enquête sur l'usage du site web lalouvesc.fr, et plus largement sur la façon dont les habitants, les amis et visiteurs du village s'informent sur la vie locale, va démarrer.

L'enquête sera réalisée par la société Comly. Certains lecteurs de ce Bulletin seront interrogés bientôt sur leurs pratiques d'information. L'objectif est d'améliorer les services proposés par le site web et de repérer quelles initiatives d'information sur différents supports nous pouvons prendre pour mieux servir la collectivité.

## Zoom

### Disparition d'un hôtel de légende

Jean-Claude Régal nous propose ci-dessous une première histoire de l'hôtel Beauséjour qui s'appelait autrefois Le Central, puis l’hôtel Régis COSTET. Une version plus longue et fouillée sera publiée dans le Bulletin de l'Alauda de 2022.

Au fait, il était là depuis quand ce vénérable Hôtel COSTET ?

Je n'ai pas de date précise pour la construction, ni d'information pour la destination initiale du bâtiment. A-t-il toujours été un hôtel, qui en était le constructeur ou le concepteur ? Y avait-il un véritable architecte à cette époque ?

![](/media/carte-postale-beausejour-fin-19e.jpg)

Les recherches sont en cours, mais il est certain qu'à l'époque où notre église n'était pas encore une basilique et n'avait qu'un clocher, ou le lac existait encore, ou les constructions du chemin neuf n’existaient pas.... il était déjà là.

Le bâtiment apparait aussi sur le cadastre de Lalouvesc de 1832.

![](/media/cadastre-napoleon-2.jpg)

Nous allons donc devoir avancer par constatations et déductions. A ce stade et pour comprendre, il est nécessaire de faire une brève et partielle étude de la généalogie de la famille COSTET, ayant bien sûr, une relation avec notre établissement.

Selon les archives et recherches du père Lacroix, jésuite à Lalouvesc, nous pouvons logiquement avancer ainsi :

* Michel COSTET (d'OTERNAUD de VAUDEVANT) marié à Marie DESCHAMPS en 1680.
* Jacques COSTET (1689/1759) marié en 1724 à Marie MOLEIRE (1700/1782).
* Jacques COSTET (1735) marié en 1761 à Agathe DELHOMME.
* Jean Antoine Joseph COSTET, aubergiste-traiteur à Lalouvesc, (1766/1827) marié en première noce avec Marie ARCHER le 28 messidor de l'an 4 (1796) ; marié en seconde noce le 28/8/1797 avec Marguerite BERGERON (1776/1860).
* Jacques Joseph COSTET, marié à Françoise FOMBONNE en 1886. Il possède trois établissements (Les Trois Pigeons - Le Monarque - Le Central (Beauséjour).
* Alexandre COSTET (1836/1907) marié à Sophie ROMAIN (1841/1903), propriétaire exploitant du Central-Hôtel (futur Beauséjour).
* Sa sœur Marie COSTET (1861/1933) donnera naissance à la branche CHAIX -SOLNON.
* Régis COSTET (1874/1932) qui ne possède plus que le Beauséjour, le Monarque ayant été transmis par succession.

Nos réflexions nous permettent donc de penser que cet établissement aurait été construit par le grand-père d'Alexandre COSTET, selon toute vraisemblance, fin XVIII - début XIX siècle.

Fort d'un héritage familial conséquent et de son travail, Alexandre COSTET lègue à son fils Régis COSTET (1874/1932) un magnifique patrimoine immobilier et commercial qui constituait à l'époque un établissement d'un très haut standing régional.

{{<grid>}}{{<bloc>}}

![](/media/hotel-regis-costet-beausejour-col-p-roger-2.jpeg "Collection P. Roger")

{{</bloc>}}{{<bloc>}}

![](/media/hotel-regis-costet-beausejour-col-p-roger-3.jpeg "Collection P. Roger")

{{</bloc>}}{{</grid>}}

A sa mort, Régis COSTET, lègue à son fils André COSTET un superbe outil de travail qui ne cessera de prospérer entre les mains d'une famille d'infatigables travailleurs très investis et à la renommée quasi régionale.

André COSTET (1902/1978), gérera le Beauséjour pendant près de 45 ans, Maire de Lalouvesc de 1965 à 1972, marié à Gabrielle OLLIER (1908/1982) père de deux filles Andrée (1927) épouse LEVIS et Paulette épouse VIGNON (1929).

{{<grid>}}{{<bloc>}}

![](/media/menu-hotel-costet-1897.jpg "Menu de fête de l'hôtel Costet (1897 ?)")

{{</bloc>}}{{<bloc>}}

L'hôtel possédait un parc remarquable, arboré de magnifiques marronniers, avec une extraordinaire vue sur les monts Gerbier et Mézenc, et faisait, avec ses 32 chambres, le plein durant les mois d'ouverture (de mi-mai à septembre) pour le plus grand bonheur des villégiateurs d'avant et d’après la seconde guerre mondiale.

Le restaurant était unanimement reconnu, et certains dimanches, plus de 250 repas pouvaient être servis. Des dizaines d'autobus déversaient une clientèle bourgeoise venue se régaler des mets les plus fins.

{{</bloc>}}{{</grid>}}

Andrée COSTET épouse LEVIS, fille d'André COSTET ne reprendra pas les rênes de l’établissement, pas plus que sa sœur Paulette. Andrée LEVIS aura trois enfants, Pierre né en 1952, Laurent né en 1962 et l'ainée une fille, Françoise LEVIS née en 1949, aujourd'hui veuve, vivant en Ardèche (nous protégeons ici a sa demande sa vie privée). C'est par son truchement que nous avons pu aussi remonter l'histoire de l’hôtel Beauséjour de Lalouvesc.

Gabrielle COSTET, veuve d'André COSTET conservera l'établissement durant quelques années après la mort de son époux en 1978. La charge est lourde. Il sera géré de façon familiale environ encore deux années jusqu'en 1982, mais déjà, à partir de 1980 la restauration n'était plus assurée.

Gabrielle COSTET décède en 1982. Succession faite, l'établissement sera cédé à Monsieur CHAVET Jean-Louis dit « Jean-Lou » en 1985. Mais hélas, la nouvelle aventure du Beauséjour ne durera pas très longtemps, déjà en 1989 le restaurant ne fonctionnait plus.

L'établissement est fermé durant de nombreuses années, et la tempête de décembre 1999 va mettre à mal sa toiture. Début 2000, l’hôtel est vendu à Madame NARECE et Monsieur Georges JEAN-BAPTISTE.

Le projet de ces nouveaux propriétaires hélas n'arrivera pas à maturité et très rapidement l'établissement va se dégrader. Le bâtiment laissé à l'abandon par ses propriétaires retournés vivre en Martinique menace rapidement ruine, et va bientôt présenter un danger pour la population sans compter l'aspect visuel négatif pour le village.

Il devient dangereux et la municipalité du village va prendre en main ce dossier, et il faut à ce stade remercier monsieur COUETTE, maire du village à l'époque pour le temps qu'il a passé à démêler ce dossier difficile.

Les choses auraient pu se régler rapidement à l'amiable, mais là encore les choses se compliquent. Les divers courriers expédiés par la municipalité restent sans réponse, et quand une procédure fut engagée, celle-ci fut contestée par les propriétaires.

Une enquête d'utilité publique est alors lancée au printemps 2019 pour l'acquisition et l'aménagement de l’hôtel et de la parcelle sur laquelle est situé l'établissement. La procédure débouche sur un arrêté préfectoral la même année. A l'automne 2020 un jugement d'expropriation a été prononcé, et la Commune est devenue propriétaire du bâtiment et du terrain.

En Janvier 2022, l'hôtel Beauséjour disparaissait. Un nouveau projet pouvait voir le jour.

Jean-Claude Régal

### Beauséjour : fin de partie et nouvelle donne

C'est peu de dire que la démolition de l'hôtel Beauséjour a marqué les esprits des habitants et des amis du village. L'[album de photos du chantier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition/) a pulvérisé le record des actualités avec plus de 1500 vues ! Les posts et commentaires se sont multipliés sur la [page Facebook des amis de Lalouvesc](https://www.facebook.com/groups/47779411002/?hc_ref=ARRri9HzpWSzmvEopvqV1uScuE0ILWIiKrK6iMqrqzuUzbBMJLEP3k12H-4HCBha-bU&ref=nf_target). Les badauds en ont fait un lieu de rencontres, les voitures s'y arrêtaient...

La disparition de l'hôtel, c'est une page d'histoire, gravée dans la pierre, qui se tourne et une autre qui s'ouvre, encore vide mais déjà lumineuse aujourd'hui et qu'il faudra demain écrire ensemble.

![](/media/demolition-beausejour-2022-67.jpg)

#### La démolition

Nous devons d'abord remercier Jean-François Couette, maire du village à l'époque. C'est grâce à lui que ce dossier difficile a pu être démêlé. Lorsque nous avons été élus, la partie juridique était pratiquement bouclée. A l'automne 2020, un jugement d'expropriation a été prononcé et la Commune est devenue propriétaire du bâtiment et du terrain.

Il a fallu alors trouver des appuis pour financer les travaux de démolition. Un grand merci à la Préfecture et à la Région qui ont accepté de subventionner le chantier à 80% sur la base d'un devis à 120 000 €, les 20% restant de toute façon légalement à la charge de la Commune. Finalement, suite au marché public, le coût sera inférieur.

Ensuite, il fallait déplacer les lignes électriques et téléphoniques pour dégager l'espace à détruire. Pour cela l'accord des riverains était nécessaire. Ce fut un peu long, mais remercions-les d'avoir signé les conventions indispensables.

Enfin, l'agenda de l'entrepreneur devait se libérer. Ce n'est qu'en janvier 2022, soit une année et demi après notre prise en main du dossier que les grues sont arrivées. Et ce fut spectaculaire : en [deux semaines](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition-deuxieme-semaine/) le bâtiment avait disparu !

![](/media/demolition-beausejour-2022-53.jpg)

![](/media/demolition-beausejour-2022-74.jpg)

#### De nouvelles perspectives

La démolition de l’hôtel ouvre l’axe principal du village sur un versant jusqu’ici négligé, celui des Cévennes, et marque aussi la complémentarité des deux dimensions de son histoire, l’une spirituelle, abstraite, concrétisée par les pèlerinages et la basilique orientée vers les Alpes, l’autre matérielle, plus concrète, marquée par le tourisme vert, la forêt, le bois orientée vers les Cévennes et les principaux départs de randonnée.

![](/media/plan-jeu-monument.jpg)

C'est pourquoi, il faut mener une réflexion qui associe l'aménagement du parc de l'hôtel, la descente vers le parc du Val d'or et aussi la rénovation du parc du Val d'Or et leur impact sur l'équilibre urbain du village.

Le défi est de résoudre un paradoxe : d'un côté, il faut aller vite pour ne pas laisser le terrain en friche et ouvrir un espace agréable au public ; et, d'un autre côté, il ne faut pas se précipiter pour ne pas se tromper, ni grever les finances de la Commune par des investissements inconsidérés. Ce sont des aménagements qu'il faut planifier dans le temps et des dossiers qui sont longs à monter pour trouver les financements opportuns.

Ci-dessous, nous ne présentons que les premières initiatives à prendre. La réflexion à moyen terme fera l'objet de développements ultérieurs.

#### Premiers aménagements

Il faut donc articuler trois espaces : la parcelle sur laquelle se trouvait l'hôtel et son jardin, la liaison entre cette parcelle et le parc et enfin le parc du Val d'Or, lui-même.

##### Le parvis et le parc

L'[arrêté d'utilité publique](https://www.lalouvesc.fr/media/arrete-prefectoral-d-utilite-publique-hotel-beausejour.pdf) a défini en 2019 un aménagement projeté par la Commune de l'espace libéré. Il indique dans son article 1er :

> Est déclaré d'utilité publique, au bénéfice de la commune de Lalouvesc, le projet d'aménagement urbain, visant à aérer le centre du village, à créer une place publique avec un jardin, quelques places de stationnement et une liaison piétonne vers l'autre espace public situé en contrebas...

Le projet est précisé dans un plan annexé à l'arrêté.

![](/media/beausejour-plan-des-amenagements-arrete-nov-2019.jpg)

Dans un premier temps, nous allons donc lancer un marché public conforme à cet arrêté, demander des subventions pour le réaliser et donc aménager déjà un espace sympathique et agréable. Pour cette étape, il y a  des délais incompressibles et cela prendra au moins une année.

Il faut avoir néanmoins à l'esprit qu'il ne s'agit que d'une première étape avant un projet plus ambitieux à construire collectivement.

##### La liaison vers le parc

L'arrêté prévoit aussi "une liaison piétonne vers l'autre espace public situé en contrebas". Mais il existe une parcelle privée appartenant à une copropriété située entre l'espace Beauséjour et le parc du Val d'Or.

Plutôt que de se lancer dans une nouvelle procédure, la Commune a préféré contacter la copropriété pour régler cette question à l'amiable en rachetant à un prix raisonnable le terrain indispensable à la liaison. Le règlement de cette question est en cours de discussion.

Il faudra ensuite décider du mode de liaison : chemin, escalier, autre chose... Quel que soit le choix, la liaison est une priorité et sera incluse dans le marché public précédent.

##### Vers un jeu-monument sur le parc du Val d'Or

Le troisième espace, celui du Val d'Or, ne saurait rester non plus en l'état, maintenant qu'il sera visible directement depuis le centre bourg. C'est pourquoi nous avons émis l'idée d'y construire un jeu-monument qui a fait déjà l'objet de nombreuses présentations. Comme pour les autres espaces la réflexion se mène en deux temps avec l'aide d'un comité technique réunissant des architectes et des professionnels du bois amis du village.

Dans un premier temps, un concours d'idées a été lancé en direction des étudiants en architecture. Il a largement été relayé. Nous avons sélectionné une équipe qui va travailler avec le comité technique.

{{<grid>}}{{<bloc>}}

![](/media/g-diche-et-j-demeulenaere.jpg "Julie Demeulenaere et Guillaume Diche")

{{</bloc>}}{{<bloc>}}

Licenciée en architecture, Julie poursuit ses études dans un master spécialisé en rénovation et en réhabilitation. Ses intérêts pour les arts graphiques enrichissent ses projets.

Guillaume est licencié en sociologie et étudiant en architecture. Son approche se soucie de l’environnement dans son histoire (patrimoine), dans sa sociologie (démographie) et sa géographie (ressources).

{{</bloc>}}{{</grid>}}

Ils remettront leur projet à la mi-avril et le présenteront publiquement peu après. Il s'agit de construire un petit jeu-monument qui préfigurera un projet plus ambitieux dont la conception du dossier sera amorcée par cette première réalisation.

## Suivi

### La Vie Tara change de mains

Les nouveaux propriétaires se présentent. Nous leur souhaitons la bienvenue !

{{<grid>}}{{<bloc>}}

![](/media/la-vie-tara-equipe-janvier-2022.jpeg " En haut : Gera Gilda, Michaël, Romain, dessous : Ine, Joke (et le chien Mila)")

{{</bloc>}}{{<bloc>}}

Bonjour aux habitants de Lalouvesc,

Albert et Joke ont vendu « La Vie Tara » en décembre dernier ! Nous avons déjà rencontré certains d'entre vous récemment, c'est pourquoi nous souhaitons nous présenter brièvement ainsi que nos projets.

Pour l'instant, nous sommes deux vivant à La Vie Tara, Romain au poste de manager et Ine à la communication. Michaël (directeur/coach) et Gera Gilda (cuisine et arts) sont aussi impatients d'arriver. En outre, certaines personnes seront ici à temps partiel comme Joke (administration) et Jessica (comptabilité) qui travailleront avec nous depuis les Pays-Bas. Tout le monde est originaire des Pays-Bas, sauf Romain, qui est né et a grandi à Paris.

Nous nous sentons très chanceux de pouvoir prendre soin de ce bâtiment spécial et de cet endroit. Nous sommes très impressionnés par le bâtiment, l'idyllique Lalouvesc et les magnifiques environs.

{{</bloc>}}{{</grid>}}

Nous espérons pouvoir vous accueillir prochainement et accueillir de nombreux visiteurs qui souhaitent se détendre, se promener, méditer, suivre des retraites ou faire des activités artistiques.

Nous disposons déjà d'un site web en ligne où de plus amples informations seront bientôt disponibles sur [www.lavietara.eu](http://www.lavietara.eu). Nous espérons de belles collaborations avec vous et le voisinage, afin que La Vie Tara redevienne un centre florissant dont nous pourrons tous profiter. Comme l'a si bien dit le maire Jacques Burriez dans un échange de courriels avec nous: "La Vie Tara est un bâtiment remarquable, une bâtisse pleine d'histoire qui ne demande qu’à vivre et reprendre son rang dans la vie et l'activité de l'établissement dans notre village, plein de potentiel et de développement futur".

Nous prévoyons d'organiser quelque chose pour le village plus tard dans l'année afin que vous puissiez découvrir ou re-découvrir le domaine La vie Tara. Mais cela n'a pas encore pris forme. En attendant, nous nous occupons de tout préparer pour la saison prochaine.

Si vous voulez nous rencontrer, donner un coup de main, avoir des idées ou en savoir plus : vous êtes les bienvenus ! Ou vous pouvez nous joindre par courrier :

* Romain: [management@lavietara.eu](mailto:management@lavietara.eu)
* Ine: [pr@lavietara.eu](mailto:pr@lavietara.eu)
* Informations générales: [info@lavietara.eu](mailto:info@lavietara.eu)

Au plaisir de vous voir bientôt!

L’équipe La Vie Tara

### Alphonse Abrial nous a quittés

{{<grid>}}{{<bloc>}}

![](/media/alphonse-et-magdelaine-abrial.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/alphonse-abrial.JPG)

{{</bloc>}}{{</grid>}}

Alphonse Abrial nous a quittés le 14 janvier dernier à l'âge de 94 ans. Madeleine et Alphonse ont vécu plus de 67 ans à la ferme et l'ont exploitée durant plus d'un demi-siècle. Leur porte était toujours ouverte pour celui qui voulait discuter ou partager la vie paysanne en haute Ardèche. Des générations de petits colons et touristes y sont venus voir les vaches...

Nous nous souviendrons longtemps de sa silhouette sur ses terres ou dans son jardin. Pour beaucoup d'entre nous son sourire épanoui restera sur les murs de la ferme.

Irène sa fille

### Spectacle des enfants à la basilique

Le spectacle de noël des enfants de l'école St Joseph a été présenté en janvier à la Basilique.

{{<grid>}}{{<bloc>}}

![](/media/spectacle-ecole-st-joseph-noel-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/spectacle-ecole-st-joseph-noel-2021-2.jpg)

{{</bloc>}}{{</grid>}}

Pour les raisons que vous imaginez, les invitations n’avaient pu se faire à grande échelle, et c’est bien "providentiellement" que j’en ai été informée... en regardant le lever de soleil sur le chemin de ronde sous le mur du Cénacle. Un enfant et sa maman qui allaient à l’école m’en ont parlé.

A 9 h, une joyeuse troupe, savamment costumée, a gravi les marches de notre Basilique et s’est installée dans le chœur. Il ne manquait personne. Les bergers facilement reconnaissables avec houppelande et grand bâton... les brebis représentées par les plus petits cachés derrière leurs masques, fabrication maison, et, bien sûr, les "Rois Mages" magnifiquement vêtus et, plus étincelante encore !, l’étoile qui les guidait. La maitresse lisait "le conte" (inspiré des évangiles) et les enfants mimaient et chantaient.

Quelle gaité et quelle fraicheur dynamique de ces jeunes générations ! A la fin, tous ont déambulé autour de l’autel et sont venus déposer au pied de l’Enfant de la crèche, leurs jolis dessins, et toute la jeune petite troupe a encore donné de la voix pour nous exprimer sa joie. Joie bien partagée, les spectateurs ont applaudi de tout cœur !

A tous ces enfants (et à leurs parents !) très joyeuse bonne année 2022 !

Edith Archer

### La Parentibulle

La Parentibulle, espace d'accueil pour les parents et les enfants de moins de 6 ans, poursuit sa route en 2022 sur le Val d'Ay.

Les permanences ouvertes à tous ont lieu tous les 15 jours, un vendredi matin dans les locaux du Relais Petite Enfance (anciennement RAM) à Jaloine. Des professionnelles de la petite enfance seront présentes pour vous proposer des animations, espaces de jeux, échanges, café et partages et ressources.

![](/media/parentibulle-2022-1.jpg)

### Café des aidants

{{<grid>}}{{<bloc>}}

Le prochain café des aidants de St Félicien aura lieu samedi 5 février 2022 de 10h à 11h30 à l'Auberge de St Félicien sur le thème « Une histoire d'aide, une histoire de famille ».  
Les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).

Vous avez la possibilité de venir avec la personne que vous aidez, un relais sera mis en place par le collectif KANLARELA (actions artistiques et de bien-être à destination de personnes ayant des maladies neuro-évolutives, en situation de handicap ou d'isolement, ainsi qu'à leurs proches aidants) - gratuitement - sur inscription.

Renseignements auprès de Carole Guilloux : [cguilloux@fede07.admr.org](mailto:cguilloux@fede07.admr.org) ou 06 81 50 19 26

{{</bloc>}}{{<bloc>}}

![](/media/cafe-des-aidants-2022-1.jpg)

{{</bloc>}}{{</grid>}}

### Passer son BAFA et trouver un job d'été

![](/media/bafa-fol-2022.jpg)

### Prévention du suicide

{{<grid>}}{{<bloc>}}

![](/media/prevention-suicide-2022.jpg)

{{</bloc>}}{{<bloc>}}

Depuis le 28 septembre dernier, un numéro national de prévention du suicide a été mis en place : le **3114**.

Ce numéro, **gratuit et confidentiel**, est opérationnel depuis le 1er octobre 2021, il est **ouvert 24H/24, 365 jours par an.**

Il est à disposition de tous-tes : usager-ère-s, leurs familles ou ami-e-s, professionnel-le-s.

Le centre de Saint-Etienne est répondant pour les départements de l’Allier, du Cantal, du Puy de Dôme, de la Haute-Loire, de la Loire et de l’Ardèche. Les répondants Stéphanois sont tous des infirmier-ère-s expérimenté-e-s en psychiatrie, aidés, si besoin, par un-e psychiatre. Ils sont basés au SAMU 42.

Il est important de communiquer sur l’existence de ce numéro auprès des professionnel-le-s, des personnes, des familles, des jeunes, car cela peut être une aide dans les situations de crise suicidaire.

{{</bloc>}}{{</grid>}}

### Dans l'actualité, le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de janvier qui restent d'actualité.

#### Foot : les matchs reprennent au stade

22 janvier 2022  
Le prochain dimanche 30 janvier 14h30 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/foot-les-matchs-reprennent-au-stade/ "Lire Foot : les matchs reprennent au stade")

#### Programme du centre de loisirs de Jaloine pour février

20 janvier 2022  
Bientôt les vacances : des occupations pour les enfants [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/programme-du-centre-de-loisirs-de-jaloine-pour-fevrier/ "Lire Programme du centre de loisirs de Jaloine pour février")

#### Beauséjour : pause sur le chantier

20 janvier 2022  
La démolition est terminée [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-pause-sur-le-chantier/ "Lire Beauséjour : pause sur le chantier")

#### Les vitraux de la Basilique

19 janvier 2022  
Une visite sans quitter vos pantoufles [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-vitraux-de-la-basilique/ "Lire Les vitraux de la Basilique")

#### Elections et inscriptions sur les listes électorales

18 janvier 2022  
Toutes les dates pour la présidentielle et les législatives [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/elections-et-inscriptions-sur-les-listes-electorales/ "Lire Elections et inscriptions sur les listes électorales")

#### Beauséjour : l'album de la démolition, deuxième semaine

10 janvier 2022  
Dans cette actualité, les photos au jour le jour... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition-deuxieme-semaine/ "Lire Beauséjour : l'album de la démolition, deuxième semaine")

#### Beauséjour : l'album de la démolition, première semaine

4 janvier 2022  
Dans cette actualité, les photos au jour le jour... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition/ "Lire Beauséjour : l'album de la démolition, première semaine")

## Feuilleton : _Les quatre éléments_

Histoire écrite collectivement par les élèves des classes CE1//CE2/CM1 de l’école St Joseph de Lalouvesc (Ardèche)

### Résumé des épisodes précédents

_Deux enfants partent autour du monde à la recherche de quatre pierres pour soigner un cheval blessé. Grâce à la fourmi, ils récupèrent la pierre TERRE en forêt amazonienne. Aidés par le dragon et le phénix, ils trouvent la pierre FEU à la Réunion. Des dauphins leur ouvre la voie à la pierre EAU grâce à un message codé._

Vous pouvez lire ou relire les épisodes déjà publiés :

* [Chapitre 1 : Le cheval blessé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/#_les-quatre-%C3%A9l%C3%A9ments_)
* [Chapitre 2 : La pierre TERRE](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#chapitre-2--la-pierre-terre)
* [Chapitre 3 : A la recherche du FEU !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/17-engranger/#chapitre-3--a-la-recherche-du-feu-)
* [Chapitre 4 : Le message codé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#chapitre-4--le-message-cod%C3%A9)

### Chapitre 5 : Le temple du vent

Les dauphins disent aux enfants que pour trouver le temple du vent, il faut suivre le vent du nord. Cependant les enfants se rappellent d'un cours de géographie, regardent leur boussole et ils s'aperçoivent que les dauphins se sont trompés et qu'il faut suivre le vent de l'ouest et non le vent du nord. Les dauphins se rendent compte de leur erreur et pour se faire pardonner ils proposent aux enfants de les emmener au moins jusqu’à la Chine.

Les voilà donc partis une nouvelle fois à dos de dauphins pour traverser une partie de l’océan pacifique.

![](/media/les-4-elements-ch-5-7.jpg)

En arrivant sur la côte-est de la Chine, les enfants ont très faim et ils cherchent un endroit où ils pourraient s’acheter à manger. Au coin de la rue, ils aperçoivent un petit magasin chinois qui semble abandonné, un vieux monsieur étrange les accueille.

Il dit : « Je suis sûr que vous avez très faim mais que vous cherchez également comment aller jusqu'au Mont Everest pour trouver la dernière pierre qui abrite le quatrième élément ? J'ai justement ce qu'il vous faut ! »

Le vieil homme leur donne alors un diamant bien particulier et leur dit :

« Ce diamant vous permettra de vous téléporter jusqu'au sommet de l'Everest. Surtout ne le perdez pas sinon vous ne pourrez jamais franchir les portes du temple du vent. »

Et hop, en un clin d’œil les voilà arrivés !

Au sommet de l'Everest, le vent souffle très fort, les enfants ont du mal à se tenir debout. Ils ont du mal à apercevoir le temple qui est au milieu du brouillard.

{{<grid>}}{{<bloc>}}

![](/media/les-4-elements-ch-5-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/les-4-elements-ch-5-5.jpg)

{{</bloc>}}{{</grid>}}

Le diamant est connecté au temple. Lorsqu’ils arrivent près du temple, une lumière éblouissante jaillit. Ainsi les enfants voient la lumière et trouvent le temple. Pour entrer dans le temple ils doivent trouver la forme géométrique qui correspond au trou qu'il y a sur la porte du temple : un octogone.

{{<grid>}}{{<bloc>}}

![](/media/les-4-elements-ch-5-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/les-4-elements-ch-5-2.jpg)

{{</bloc>}}{{</grid>}}

Ils trouvent d'abord une forme qui semble être un octogone mais en fait il s'agit d'un hexagone (qui n'a que six côtés). Alors ils se remettent à la recherche de cette forme. Le vent souffle toujours très fort et il est difficile de distinguer les formes. Ils trouvent un losange (quatre côtés), un pentagone (cinq côtés), un heptagone (sept côtés), un décagone (dix côtés) et aussi un dodécagone (douze côtés) mais toujours pas d’octogone ! Mais soudain le diamant se met à briller encore plus et commence à former un faisceau lumineux qui indique un emplacement. A cet emplacement, ils trouvent enfin le fameux octogone (huit côtés) qui leur permet de rentrer dans le temple. L’aigle royal, qui est le gardien de ce temple, les accueille chaleureusement car il est impressionné par la persévérance des enfants.

Il leur remet la pierre qui abrite le dernier élément : le **VENT**.

Enfin munis des quatre pierres, les enfants peuvent rentrer en France pour aller soigner le cheval. Pour les aider l’aigle royal va les amener jusqu’en France. Le trajet est long, il faut survoler plusieurs pays : l’Afghanistan, l’Iran, la Turquie, l’Italie et enfin la France !

![](/media/les-4-elements-ch-5-1.jpg)

Ils arrivent enfin dans la forêt où ils avaient laissé le cheval avec l’ours. En arrivant avec les quatre éléments, les enfants voient soudain quatre socles qui sortent du sol autour du cheval. Ils comprennent vite qu’il faut poser les quatre pierres sur les socles… et tout à coup le miracle se produit : le cheval est entouré d’une lumière bleutée et une lumière se dirige vers sa patte blessée. En un instant il est guéri !

![](/media/les-4-elements-ch-5-4.jpg)

Et voilà les enfants sont très fiers d’avoir réussi leur mission en sauvant le cheval. Cette aventure fut extraordinaire, les enfants s’en souviendront longtemps !

**FIN**

En cadeau, [le livre en entie](/media/les-quatre-elements-ecole-st-joseph-lalouvesc-2021-22.pdf)r à télécharger et à imprimer.

Et pour finir, faites vous aussi votre octogone en origami :
<iframe width="560" height="315" src="https://www.youtube.com/embed/UUijZGgFpE0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>