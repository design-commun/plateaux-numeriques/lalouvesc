+++
date = 2022-05-28T22:00:00Z
description = "n°23 Juin 2022"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°23 Juin 2022"
title = "23 - Salut les artistes !"
weight = 1

+++
Le village prend ses atours d'été grâce aux artistes : les artistes louvetous qui inventent et trouvent des outils, des formes et des matériaux originaux pour décorer le village, aussi, bien sûr, les artistes invités pour des événements d'envergure, expositions et musique au Carrefour des Arts, concerts du Comité des fêtes qui nous enchantent tout au long de la saison et encore des artistes, nouveaux venus ou nouveaux talents sur le village, méconnus des Louvetous à découvrir en conclusion de ce Bulletin. Un numéro de début de saison particulièrement illustré et fourni donc, avec aussi, comme toujours, bien d'autres informations encore.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce[ fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-23-1er-juin-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du maire

Mes chers amis,

Le beau temps est là, même un petit peu trop présent. Ne nous plaignons pas, la fréquentation du village est importante, qui laisse présager d’une bonne saison et qui permet aux commerçants de bien la démarrer.

La démolition de l’hôtel est terminée. L'espace Beauséjour est né, apportant son flot de lumière au cœur du bourg. Considérons cette opération comme le symbole du renouveau de notre village, une sorte de désenclavement entre la chaîne des Alpes et notre beau massif du Mézenc, doté du Gerbier de Jonc, deuxième site touristique d’Ardèche. Tout n’est pas fini, nous repartons pour une prochaine tranche de travaux qui dessinera la liaison douce entre le centre du village et le parc du Val-d’Or.

Les travaux de viabilisation du lotissement vont bon train. Tout n’est pas signé mais six lots sur neuf sont réservés. L'initiative opportune de cette opération revient à mon prédécesseur. Nous l'avons fait aboutir.

La rue partagée de la Fontaine est très appréciée, nous avons reçu les félicitations de Monsieur Pierre Roger, référent des riverains.

Merci à tous ceux qui œuvrent pour le village à tous les niveaux. L’union fait la force. J'ai une pensée toute particulière pour la commission fleurissement qui a vraiment mis tout en œuvre pour fleurir le village, un gros boulot a été fourni, merci pour tout ce qui a été fait et pour l’entretien qui continue les mois prochains.

Le village s'en ressent : propreté, embellissement, convivialité et joie sont les leitmotivs de ce début de saison.

Sincères amitiés à toute la population.

Jacques Burriez, Maire de Lalouvesc

## Actualités de la mairie

### Fin de la première phase du chantier Beauséjour et suite

La façade dégagée par la démolition de l'hôtel a été protégée, crépie, mettant le point final à la première phase des travaux. Cet été l'esplanade sera occupée par l'exposition de photographies du Carrefour des Arts (voir plus loin).

A l'automne démarreront les travaux d'aménagement de l'espace. Ils s'appuieront, dans un premier temps sur les[ résultats du concours d'idées sur le jeu-monument](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/22-ce-qui-plait/#jeu-monument--une-premi%C3%A8re-%C3%A9tape-franchie) qu'il faudra adapter en fonction des contraintes du terrain et du budget. D'autres aménagements seront aussi entrepris pour une liaison la plus douce et fluide possible avec le parc du Val d'Or.

La société Archipolis a été choisie pour piloter la suite du chantier. Elle doit présenter prochainement ses propositions.

### Étude sur le Cénacle et la Maison Ste Monique

Le SCOT Rives du Rhône a priorisé dans son calendrier l'étude sur le couvent du Cénacle, mis en vente par la congrégation, couplée avec une étude sur le devenir du bâtiment Ste Monique. L'étude s’inscrit dans un marché à accord-cadre plus large qui a été attribué en avril 2022 à un groupement piloté par le cabinet Lieux Fauves. Les résultats de l'étude devraient être disponibles dans le courant de l'automne.

Ils seront bien utiles pour éclairer les décisions à prendre et les éventuelles stratégies pour les demandes de subvention à engager. Trouver des solutions raisonnables et viables pour l'avenir de ses bâtiments est essentiel pour le devenir du village.

{{<grid>}}{{<bloc>}}

![](/media/le-cenacle.png)

{{</bloc>}}{{<bloc>}}

![](/media/sainte-monique.JPG)

{{</bloc>}}{{</grid>}}

Suite à une réunion avec les élus, le SCOT Rives du Rhône a finalisé le [bon de commande](/media/bon_de_commande_cenacle_lalouvesc_vf.pdf) en collaboration avec la mairie de Lalouvesc.

Une première rencontre aura lieu avec les Sœurs du Cénacle le 21 juin à Lyon. Le premier comité de pilotage se tiendra le 1er juillet à Lalouvesc.

### Aire du Grand Lieu

Nous allons profiter de la présence d'engins de terrassement sur le chantier de l'écolotissement pour curer et nettoyer l'étang de l'aire du Grand Lieu.

L'opération sera réalisée vers la mi-juin. Une fois nettoyé, l'étang sera remis en eau et réempoissonné. Cela promet de sympathique concours de pêche pour cet été...

### Rue de la Fontaine, une rue partagée

Aboutissement de la concertation avec les résidents de la rue, à la satisfaction générale, la rue de la Fontaine a été transformée en rue partagée.

Les panneaux ont été posés.

![](/media/rue-de-la-fontaine-partagee-2022-1.jpg)

Les refuges pour les piétons et les marques pour les stationnements ont été tracés.

{{<grid>}}{{<bloc>}}

![](/media/rue-de-la-fontaine-partagee-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/rue-de-la-fontaine-partagee-2022-3.jpg)

{{</bloc>}}{{</grid>}}

L'important maintenant est que tout le monde maintienne cette discipline.

### Un village bien fleuri !

Comme l'année dernière, le Comité fleurissement a bien travaillé et, mieux encore que l'année dernière, de multiples fleurs, persistantes comme annuelles, décorent le village. **Merci à toutes et tous les bénévoles qui ont planté et qui continueront à arroser tout au long de la saison !**

Sans l'engagement de ces citoyennes et citoyens, le village ne serait pas à la hauteur pour accueillir tous les visiteurs de l'été. La commune n'a pas les moyens d'un tel effort.

{{<grid>}}{{<bloc>}}

![](/media/fleurissement-mai-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-mai-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-mai-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-mai-2022-3.jpg)

{{</bloc>}}{{</grid>}}

![](/media/fleurissement-mai-2022-8.jpg)

![](/media/fleurissement-mai-2022-9.jpg)

### Nouvelle organisation pour les poubelles

Les obligations de plus en plus fortes qui pèsent sur la gestion des déchets, sur notre territoire comme ailleurs, conduise le Sytrad à rationaliser son système de collecte en réduisant les points de collectes et les passages des bennes.

Ainsi la Communauté de Communes a fait l'acquisition de bacs spécialisés pour le tri qui seront placés dans quelques points de chaque commune. **Tous les bacs à roulettes seront supprimés, les bacs privés ne seront plus ramassés.**

A Lalouvesc, il y aura six points de collectes centralisés :

1. sur la place du Lac (près des wc),
2. à l'entrée du camping (sur le parking),
3. sur l'aire du Grand Lieu (à gauche après l'étang),
4. à la maison de retraite (au fond du parking),
5. sur le parking Grosjean,
6. sur la place des Trois Pigeons (au fond)

Sur chacun de ces emplacements, quatre bacs spécialisés recevront respectivement : les ordures ménagères, les corps creux (emballages), les corps pleins (papiers, petits cartons) et le verre. De plus, les trois derniers points de collecte (maison de retraite, parking Grosjean et place des Trois pigeons) disposeront d'un bac pour la récolte des cartons.

![](/media/poubelle-ccva-2022.jpg)

Cette organisation nouvelle sera mise en place début juillet. D'ici là, les bacs à roulettes seront supprimés. Ces changements sont importants, ils bousculent nos habitudes. Mais ils sont essentiels. La gestion des déchets est un lourd défi qui engage l'avenir.

La CCVA a constaté récemment à Lalouvesc plusieurs incivismes, certains déposant dans les bacs d'ordures ménagères des objets qui relevaient de la déchetterie. Cette attitude irresponsable pèse sur l'ensemble de la collectivité et ne peut être tolérée. Si vous avez des difficultés pour vous débarrasser d'objets encombrants faites-le savoir, nous trouverons une solution pour vous aider à les amener à la déchetterie.

## Zoom : l'Ardéchoise et le Carrefour des Arts

### L'Ardéchoise, bicycle et recycle

#### Décorer en recyclant

Recycler, pourquoi et comment ?

* Pourquoi ? Pour contenir ses effets nocifs pour la planète...
* Comment ? Chez soi au jour le jour ou de façon industrielle…

Tout cela est louable, mais un peu fastidieux n’est-ce pas ? Alors, n’y aurait-il pas une dimension plus ludique, une façon de réveiller en chacun son âme créative ? C’est ce défi que nous avons voulu relever au Comité des fêtes de Lalouvesc à l'occasion de l'Ardéchoise.

Après la collecte de plusieurs centaines de bouteilles en plastique (versant quotidien), le tri (versant quasi-industriel tellement chacun a pris soin de nous garder ses contenants), la créativité et à la convivialité ont fait des merveilles. Les idées ont fusé de toutes parts.

La fabrication en petits ateliers ou chacun chez soi débouche sur une belle saynète haute en couleurs en train de se construire. Encore de nombreuses heures de travail pour tous ces artistes en herbe et moins en herbe (car nous avons des artistes aguerris au village) et nous aurons encore cette année une superbe décoration pour le plus grand plaisir des touristes qui visiteront Lalouvesc cet été. Le village ne faiblit pas et montre son attachement, année après année, et son respect à cette fabuleuse organisation qu’est **l’Ardéchoise**.

Allez on vous montre quelques photos, mais pas trop ! Rendez-vous début juin sur le square pour la mise en situation de cette œuvre collective. Pour les plus éloignés, les réseaux sociaux et lalouvesc.fr vous présenteront aussitôt ces décorations.

Deux artistes en plein travail : à l'école et à l'Ehpad...

{{<grid>}}{{<bloc>}}

![](/media/decoration-ardechoise-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-ardechoise-2022-3.jpg)

{{</bloc>}}{{</grid>}}

... et deux fragments de création. Saurez-vous deviner l’œuvre globale et son matériel de fabrication ?

{{<grid>}}{{<bloc>}}

![](/media/decoration-ardechoise-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-ardechoise-2022-4.jpg)

{{</bloc>}}{{</grid>}}

Merci à ceux qui ont participé en triant leurs plastiques ou en maniant les ciseaux, crayons et pinceaux...

Souhaitons la bienvenue aux milliers de cyclistes qui passeront par Lalouvesc dans le cadre de cette fameuse épreuve cycliste mais aussi à tous ceux qui traverseront notre village cet été et surtout… respect à eux !

Le Comité des Fêtes est toujours joignable soit :

* ·par téléphone au 07.66.67.94.59,
* ·par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* ·par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

#### Stationnement et circulation pour l'Ardéchoise

La traversée du village par des milliers de vélos au cours des trois jours de l'Ardéchoise oblige à des plans de circulation et de stationnement exceptionnels. Voici donc les principales mesures qu'il sera impératif de respecter.

##### Stationnement

Le stationnement sera interdit :

* devant le square rue des Cévennes, le mercredi 15 juin de 8h à 20h (mise en place des décorations) ;
* dans le parking sous la mairie du mercredi 15 juin 20h au samedi 18 juin 20h (stand de ravitaillement) ;
* tout le long du circuit des cyclistes (voir carte) ainsi que rue de la Fontaine et route d'Annonay depuis la Pharmacie jusqu'au chemin menant à la station d'épuration (déviation) le samedi 18 juin de 8h30 à 20h30.

Il ne sera pas possible de sortir un véhicule des parkings Grosjean et Trois Pigeons le samedi 18 juin de 8h30 à 20h30. Des parkings seront ouverts Place du Lac, au Grand Lieu et sur le terrain de foot ce même jour.

![](/media/plan-lalouvesc-ardechoise-deviation-2022.jpg)

##### Circulation

Les routes départementales en direction de St Félicien, de Lamastre et de Rochepaule seront entièrement fermées.

Une déviation pour les véhicules légers (hors 3,5t, camping-car et caravane) sera mise en place par la rue de la Fontaine permettant de relier Satillieu à St Bonnet-le-froid avec un système de feux alternatifs.

### Carrefour des Arts : demandez le programme !

* **du 23 mai au 7 novembre 2022 – Exposition de photographies** à ciel ouvert sur l'espace Beauséjour à Lalouvesc
* **du 2 juillet au 28 août 2022 – Exposition estivale** au CAC.
* **les 2, 3 et 4 août 2022 - Concerts de musique Classique** à la Basilique de Lalouvesc, et à la Chapelle de Notre Dame d’Ay
* **les 12 et 13 août 2022 - Concerts de jazz** à la Grange de Polly à St Pierre sur Doux

#### Gabions et dibonds font triompher les photographies de Serge Rousse

Jacques Morel nous explique l'origine de l'idée de la présentation de l'exposition de photographies en plein air :

> L'idée d'exposition en extérieur n'est pas neuve, On en trouve en ville par exemple à Paris à la sortie de la gare de Lyon, beaucoup plus rarement à la campagne sauf dans quelques sites touristiques mais de manière ponctuelle. C'est en découvrant l'exposition réalisé par le manoir du Tourp en Cotentin que je me suis rendu compte de la curiosité que suscitait un ensemble de panneaux insolites, dans un pré en bord de route et l'appel qu'il constituait vers d'autres découvertes.
>
> Cette image qualitative a immédiatement fait écho à une triple question que nous nous posions au Carrefour des Arts :
>
> * Où exposer des photographies suite à notre décision de ne plus utiliser l'escalier du CAC ?
> * Comment inciter les visiteurs de Lalouvesc à "pousser" leur balade jusqu'au CAC un peu à l'écart du centre touristique du village, encore plus qu'ils ne le font déjà ?
> * Comment l'exposition d'art plastique qui présente quelque fois des sculptures en extérieur peut-elle encore plus animer le village et conforter une vocation touristique artistique à Lalouvesc ?

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-s-rousse-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-s-rousse-2022-6.jpg)

{{</bloc>}}{{</grid>}}

> La réalisation technique est partie d'un cahier des charges simple : pouvoir accueillir des photos de 1,5 mètre par 1 mètre dans un support costaud et mobile car nous ne savions pas encore où les installer.
>
> Les photographies ont été imprimées sur dibond.
>
> Les savoir-faire locaux pour réaliser des encadrements en bois ont été bien utiles. Et la mise en place des gabions a été finalisée avec une grande précision.

![](/media/carrefour-des-arts-s-rousse-2022-1.jpeg "Photo OT")

> Pour le lieu d'implantation, le site de l'hôtel Beauséjour est immédiatement apparu comme une opportunité exceptionnelle.
>
> Il est au cœur commercial du village dans un lieu de passage entre le parking du lac et la basilique. En 2022 cette friche n'a pas encore de vocation. Cette exposition meuble ainsi l'espace en offrant une distraction supplémentaire aux visiteurs tout en évitant l'effet catastrophique qu'aurait eu pour le piéton un stationnement anarchique sur la qualité paysagère en plein centre.
>
> A l'avenir la question reste posée, mais les gabions sont mobiles et il ne manque pas d'autres lieux intéressants à Lalouvesc.

Et l'exposition a été inaugurée en présence des élus du territoire et des habitants du village.

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-s-rousse-2.jpeg "Photo OT")

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-s-rousse-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-s-rousse-2022-3.jpeg "Photo OT")

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-s-rousse-2022-7.jpg)

{{</bloc>}}{{</grid>}}

#### Sept artistes peintres, sculpteurs et plasticiens, sept jours sur sept au CAC en juillet et août

Les artistes retenus pour l'exposition 2022 ont été présentés par les responsables du [Carrefour des Arts](https://carrefourdesarts-lalouvesc.fr/). Ils sont sept.

##### Judith Chancrin, peintre

{{<grid>}}{{<bloc>}}

![](/media/judith-chancrin-1-carrefour-des-arts-2022.png)

> Je pars souvent de photographies que je recadre et travaille ensuite de manière à n’utiliser qu’un fragment suffisamment mystérieux pour être débarrassé de toute certitude, de toute vérité.

{{</bloc>}}{{<bloc>}}

![](/media/judith-chancrin-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Isabelle Tapie, doreuse, premier étage

{{<grid>}}{{<bloc>}}

![](/media/isabelle-tapie-1-carrefour-des-arts-2022.jpg)

> La dorure sur bois (technique complexe et subtile, inchangée depuis le XVIIème siècle) voyage au gré des branches, fragments de troncs d’arbres et racines, sculptures naturelles, rencontrées avec émotion lors de mes promenades.

{{</bloc>}}{{<bloc>}}

![](/media/isabelle-tapie-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### **François Lelièvre, sculpteur**

{{<grid>}}{{<bloc>}}

![](/media/francois-lelievre-1-carrefour-des-arts-2022.png)

> J'ai appris à travailler le bois. Sans doute, mais on peut dire cela autrement et avec davantage de justesse : j'ai appris à aimer le bois, à l’écouter comme à le contraindre, à le caresser comme à le violenter.

{{</bloc>}}{{<bloc>}}

![](/media/francois-lelievre-3-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Cécile Windeck, peintre

{{<grid>}}{{<bloc>}}

![](/media/cecile-windeck-1-carrefour-des-arts-2022.png)

> Depuis longtemps, je peins des crépuscules. Les ‘Heures Bleues’ sont un moment incertain où le ciel prend cette teinte bleue si particulière, où tout devient plus contrasté durant quelques minutes.

{{</bloc>}}{{<bloc>}}

![](/media/cecile-windeck-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Erica Stéfani, mosaïste

{{<grid>}}{{<bloc>}}

![](/media/erica-stefani-1-carrefour-des-arts-2022.png)

> Je choisis d’utiliser les techniques gréco-romaines dans la création de mes mosaïques, auxquelles j’intègre l’exploration de chemins contemporains, par le biais de matériaux différents et formes plus libres.

{{</bloc>}}{{<bloc>}}

![](/media/erica-stefani-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Malika Ameur, céramiste

{{<grid>}}{{<bloc>}}

![](/media/malika-ameur-1-carrefour-des-arts-2022.jpg)

> Chaque pièce est choisie avec soin pour sa texture, sa forme, sa couleur au plus proche de la chaleur de la brique et de la rouille. Puis, j’y mêle des clous, des métaux, chargés d’histoire pour créer un mariage cosmopolite et harmonieux.

{{</bloc>}}{{<bloc>}}

![](/media/malika-ameur-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Françoise Papail, peintre

{{<grid>}}{{<bloc>}}

![](/media/francoise-papail-3-carrefour-des-arts-2022.jpg)

> Souvent ce sont les paysages du bord de mer qui couvrent la toile... Là où je vis, je respire chaque jour l’odeur d’iode et de goémon... et m’absorbe dans ces vastes horizons de mer, de dunes...

{{</bloc>}}{{<bloc>}}

![](/media/francoise-papail-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

#### Du classique et du jazz en août

Les concerts classiques sont structurés autour des "cordes" de Gilles Lefèvre, arrangées en septuors et en trios, auxquels viendront se joindre le hautbois de Laurent Hacquart, la guitare de Raymond Gratien et les voix d'un chœur féminin et de solistes.

Le programme, très varié, comporte des œuvres de compositeurs connus allant du 17 au 20 siècle et inclut également deux créations de compositeurs classiques contemporains qui seront présents lors des concerts, continuant ainsi la tradition des Promenades Musicales qui ont déjà présenté une vingtaine de créations.

![](/media/promenades-musivales-lalouvesc-1.jpeg)

Pour la première soirée de jazz, un sextet entourant Jean-François Bonnel et Philippe Carment fera revivre la musique de Fats Waller, pianiste, chanteur, compositeur et chef d’orchestre décédé en 1943. Si l'on vous dit _Honeysuckle Rose_ et _I Can't Give You Anything but Love_ vous saurez immédiatement de qui il s’agit.

Le second concert sera sous le signe du jazz manouche avec un quartet dans lequel deux virtuoses de la guitare et du violon vous entraineront dans des interprétations et des improvisations effrénées dignes de Django Reinhardt et de Stéphane Grappelli.

#### Une association en plein développement

Avec plus de 8.000 visiteurs, le Carrefour des Arts s'est hissé au quatrième rang des événements culturels du département en 2021 selon Ardèche Tourisme.

Nouveau logo, nouveau site web, élargissement des activités, le Carrefour justifie plus que jamais son nom et vise 10.000 visiteurs cette année. Une assistante de projet, Shasha Sheikh, a été recrutée, mais l'action des bénévoles est plus que jamais indispensable à tous les moments de l'événement : choix des artistes, mise en place et démontage des expositions, permanences pendant l'ouverture, etc.

L'association est ouverte à toutes et tous. En adhérant vous pourrez aussi profiter des visites de musée et d'ateliers d'artistes organisées toute l'année et en donnant un peu de votre temps vous rejoindrez une équipe de passionnés sympathiques et vous aurez l'occasion de côtoyer de près les artistes.

Toutes les informations sur le [site web du Carrefou](https://carrefourdesarts-lalouvesc.fr/)r qui a fait peau neuve.

## Suivi : Culture - Loisirs

### Les concerts du Comité des fêtes

Une question nous est souvent posée : **_comment nait la réalisation de si grands concerts à Lalouvesc ?_**

C’est avec Philippe Chaize, et ce depuis 2015, que le Comité des Fêtes collabore pour la mise en musique des spectacles dans notre village.

Philippe Chaize a toujours été lié de très près à la musique ; tout d’abord avec la disco mobile _Chorus_ ; s’ensuit la prise de la gérance de la discothèque le _Rouge Baiser_ pour enfin (et depuis trente ans) s’investir avec succès dans le rôle producteur de spectacles à travers toute la France.

Il a suivi les pas de son père Michel Chaize, musicien professionnel pendant 25 ans dans le grand orchestre international de Pol Malburet. Ensuite, Michel Chaize est devenu l’un des plus grands agents artistiques en France. Il s’occupera de nombreux artistes durant sa longue carrière avec en autre l’organisation des concerts de Claude François et Johnny Halliday à Annonay.

Le professionnalisme de Philippe Chaize nous a permis d’organiser des concerts de qualité d’artistes très connus. Le Comite des Fêtes de Lalouvesc a accueilli notamment un groupe de Gospel, s’ensuit la venue de Jean Claude Borely et sa trompette. Une soirée hommage a été rendue à Pierre Bachelet avec _Le petit bonhomme_ donnée par un groupe d’enfants. Natasha St Pierre, les Stentors et enfin Amaury Vassily nous ont offert des moments exceptionnels à la basilique de Lalouvesc.

Dimanche 22 mai Amaury Vassily, accompagné de son pianiste, a donné un concert exceptionnel devant 450 personnes enchantées. À 30 ans, le jeune ténor surprend encore en étoffant son style lyrique – pop de nouvelles cordes.

Amaury Vassily est un maître de la voix. Il montre qu’il n’est plus seulement une voix de ténor, mais une voix qui sait exprimer les sentiments.

Un grand merci à la paroisse d’avoir accueilli ce grand artiste.

{{<grid>}}{{<bloc>}}

![](/media/amaury-vassily-concert-2022-2.jpeg "Photo OT")

{{</bloc>}}{{<bloc>}}

![](/media/amaury-vassily-concert-2022-1.jpeg "Photo OT")

{{</bloc>}}{{</grid>}}

![](/media/amaury-vassily-concert-2022-3.jpg)

{{<grid>}}{{<bloc>}}

Vient à présent le moment de vous donner rendez-vous pour une prochaine soirée au Parc des Pèlerins, le 10 juillet prochain avec une voix qui a traversé toutes les générations représentée par le groupe **_Let's Goldman_**. Vous fermez les yeux et vous entendez la voix de Jean-Jacques Goldman… Un grand moment vous attend.

{{</bloc>}}{{<bloc>}}

![](/media/let-s-goldman-2022.jpg)

{{</bloc>}}{{</grid>}}

Ils sont six musiciens : Flavie, Florent, Simon, Léo, Emeric et Loÿs. Leur nouveau concert est intitulé _Let’s Goldman - chansons pour les autres_ autour des chansons de Jean-Jacques Goldman mais aussi écrites par Jean-Jacques Goldman pour d’autres artistes comme Johnny Hallyday, Céline Dion, Zaz, Patrick Fiori...

Les billets sont déjà en vente. Attention, il n’y en aura pas pour tout le monde…

* Lalouvesc : Office du Tourisme 04.75.67.84.20, Café du lac et VIVAL
* Annonay : Office du Tourisme 04.75.33.24.51
* Satillieu : Presse Cathy MARCOUX
* Davézieux : ARCADA Outillage
* Dunières : Tabac Presse BÉAL
* St Bonnet Le Froid : MAMZEL PIZZA
* Villevocance : Presse Tabac

15 euros en prévente et 17 euros le soir même. Attention, il n’y en aura pas pour tout le monde…

Nathalie Desgrand Fourezon

## Suivi : Sanctuaire

### Colloque de l’association des Villes Sanctuaires en France

Lalouvesc reçoit du 8 au 10 juin le colloque de l’association des Villes Sanctuaires en France qui réunit des représentants de sanctuaires et offices de tourisme en provenance de toute la France. C’est un temps fort dans l’année de l’association qui permet de se retrouver pour évoquer ensemble les actions en cours ou à mener mais aussi pour découvrir un sanctuaire, son environnement. Le binôme réunissant le sanctuaire de Lalouvesc et l’office du tourisme du Val d''Ay accueilleront donc du mercredi 8 après-midi au vendredi 10 juin les membres de l’association.

Au programme de nombreuses réunions de travail, des échanges de bonnes pratiques, mais aussi des dégustations et des découvertes du village pour les membres de l'association. **Un concert d'orgue par Louis Grangé, ouvert à tous, est prévu à la Basilique jeudi 9 juin à 21h30.**

Le Chemin de St Régis (récemment mis en valeur par le[ magazine Géo](https://www.geo.fr/voyage/gr430-pour-changer-des-chemins-de-compostelle-partez-sur-le-chemin-de-saint-regis-210068)) sera, bien entendu, à l'honneur puisque deux villes sanctuaires le ponctuent. La rédactrice en chef du Pèlerin sera aussi de passage pour réaliser un Cahier spécial sur les villes sanctuaires dans le journal.

Dans cette association, Lalouvesc se trouve en bonne compagnie et profite largement de cette opportunité, la preuve en images :

<iframe width="560" height="315" src="https://www.youtube.com/embed/Bnb0ngffGFI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Exposition et concert cet été

**Marie-Josée Planson** exposera ses peintures de fleurs et d'oiseaux à la chapelle St Ignace du 9 juillet au 13 août.

![](/media/m-j-planson-st-ignace-2022.jpg)

Le **15 août à 21h Henri Pourtau** donnera un concert d'orgue à la Basilique.

### Rencontre avec les responsables du sanctuaire

Michel Barthe-Dejean et Yves Stoessel, responsables du Sanctuaire, ont donné un entretien dans l'émission Mosaïque de la Radio Chrétienne Francophone (RCF). C'est une bonne occasion de faire connaissance pour ceux qui ne les ont pas encore croisés.

<iframe allowfullscreen width=100% height=140px src="https://rcf.fr/actualite/mosaique-0/embed?episodeId=208862"></iframe>

## Suivi : Santé - Sécurité

### Emploi ADMR

Dans le cadre des remplacements d’été, les associations de l’ADMR de l’Ardèche recrutent des aides à domicile/auxiliaires de vie.

Tél : 07 85 60 02 53

![](/media/emploi-d-ete-admr-2022.jpg)

### Suite du calendrier Santé-Environnement

L’ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l’habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante) . Pour le mois de juin, la suggestion concerne les [produits ménagers](https://www.auvergne-rhone-alpes.ars.sante.fr/produits-menagers-et-qualite-de-lair-interieur?parent=14750). Un autre calendrier alerte sur les [bonnes pratiques face à l’environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0) . Pour le mois de juin, la suggestion concerne l'[ambroisie](https://www.auvergne-rhone-alpes.ars.sante.fr/ambroisie-attention-aux-allergies).

{{<grid>}}{{<bloc>}}

![](/media/ext-06_ambroisie.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/int-06_menage.jpg)

{{</bloc>}}{{</grid>}}

### Conseils pendant les fortes chaleurs

Même si à Lalouvesc, il fait toujours plus frais, il n'est pas superflu de relayer ces conseils de Santé Public France en cas de fortes chaleurs.

![](/media/fortes-chaleurs-2022.jpg)

### Conseils pour la rénovation des bâtiments

Rénofuté est un service de conseil gratuit pour la rénovation des bâtiments. N'hésitez pas à le consulter et à prendre rendez-vous : 04 75 35 87 34, nord07@renofute.fr.

![](/media/renofute-2022.jpg)

## Dans l'actualité, le mois dernier

Ci-dessous, un rappel des informations publiées sur le site [lalouvesc.fr](https://www.lalouvesc.fr/) au cours du mois de mai qui restent d'actualité.

#### Candidatures à l'élection législative sur le nord-Ardèche

24 mai 2022 Neuf candidats validés pour le premier tour du 12 juin [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/candidatures-a-l-election-legislative-sur-le-nord-ardeche/ "Lire Candidatures à l'élection législative sur le nord-Ardèche")

#### Fermetures exceptionnelles

17 mai 2022 Mairie fermée le 27 mai, la déchetterie fermée le 4 et 6 juin. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/fermetures-exceptionnelles/ "Lire Fermetures exceptionnelles")

#### Plus que trois lots disponibles sur l'écolotissement

16 mai 2022 Faîtes-le savoir aux intéressés. Ce sont les derniers terrains constructibles sur le village... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/plus-que-trois-lots-disponibles-sur-l-ecolotissement/ "Lire Plus que trois lots disponibles sur l'écolotissement")

#### L'Ardéchoise, les parcours

10 mai 2022 Heures de passage à Lalouvesc, routes privatisées [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/l-ardechoise-les-parcours/ "Lire L'Ardéchoise, les parcours")

#### Une journée citoyenne efficace... comme toujours

8 mai 2022 Le mini-golf nettoyé, le bois des refuges débroussaillé, les fleurs vivaces plantées, un chalet réparé... les citoyens n'ont pas chômé... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/une-journee-citoyenne-efficace...comme-toujours/ "Lire Une journée citoyenne efficace... comme toujours")

#### Cérémonie du 8 mai et nouvelles inscriptions sur le Monument aux morts

7 mai 2022 Défilé, allocutions, Lyre louvetonne et inscription de trois Louvetous morts pour la France au cours de la guerre de 1870 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/ceremonies-du-8-mai-et-nouvelles-inscriptions-sur-le-monument-aux-morts/ "Lire Cérémonie du 8 mai et nouvelles inscriptions sur le Monument aux morts")

#### Ouverture des inscriptions à l'école St Joseph

3 mai 2022 Une pédagogie adaptée, des activités variées... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/ouverture-des-inscription-a-l-ecole-st-joseph/ "Lire Ouverture des inscriptions à l'école St Joseph")

#### Une plateforme contre les e-escroqueries

1 mai 2022 Dépôt de plaintes en ligne [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/une-plateforme-contre-les-e-escroqueries/ "Lire Une plateforme contre les e-escroqueries")

## Découvrez deux auteurs louvetous

Pour conclure ce Bulletin consacré principalement aux artistes, nous vous proposons de faire connaissance avec deux talents louvetous, deux auteurs, dans des genres très différents.

### Thierry Rouby

{{<grid>}}{{<bloc>}}

Thierry Rouby est connu de nombreux villageois... comme cuisinier à l'EHPAD, mais il n'exerce pas seulement ses talents de créateurs devant ses fourneaux, c'est aussi un dessinateur, un peintre et, tout récemment, un nouvel auteur de romans policiers. Son premier roman a été publié en février dernier.

Il se passe juste après la guerre de 39-45 à St Symphorien-de-Mahun et on y rencontre des créatures qui trouvent encore refuge par chez nous... L'auteur nous a fait l'amitié de nous envoyer trois dessins originaux et inédits pour l'illustrer.

{{</bloc>}}{{<bloc>}}

![](/media/thierry-rouby-2022.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/immanis-creatura-th-rouby-2022.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/t-rouby-dessin-4-2022.jpg)

{{</bloc>}}{{</grid>}}

> Mai 1947, un village d'Ardèche est plongé dans l'horreur après la mort d'un enfant dévoré par une bête étrange. Très rapidement, isolée à la suite d'un éboulement, l'unique route d'accès est impraticable, recouverte de pierres et de branchage.
>
> Livrés à eux-mêmes, les habitants règlent leurs comptes, dans cette France incertaine d'après-guerre, en luttant contre une légende ancestrale qui refait surface et devient tout à coup bien réelle. La mort rôde partout dans la commune. Le passé rattrape le présent et ouvre les portes de l'enfer.

{{<grid>}}{{<bloc>}}

![](/media/t-rouby-dessin-2-2022.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/t-rouby-dessin-1-2022.jpg)

{{</bloc>}}{{</grid>}}

Le livre a été publié aux éditions du Lys bleu. Vous le trouverez bientôt à la bibliothèque, au Vival et déjà, bien sûr, dans toutes les bonnes librairies.

Et sur le [site de la FNAC](https://livre.fnac.com/a16684191/Thierry-Rouby-Immanis-creatura), vous pouvez en feuilleter quelques pages en ligne.

### Félix Brassier

{{<grid>}}{{<bloc>}}

![](/media/felix-et-joyce-brassier-2022-2.jpeg)

{{</bloc>}}{{<bloc>}}

Félix et sa compagne Joyce se sont installés à Lalouvesc l'année dernière. Ils partagent leur temps entre le Liban et l'Ardèche. Joyce est professeur de langue et travaille en ligne, il lui suffit d'un coin tranquille et d'une bonne connexion pour exercer sa profession.

Lui est photographe, peintre, dessinateur, vidéaste documentariste. Malgré son jeune âge, il a déjà derrière lui une œuvre importante.

{{</bloc>}}{{</grid>}}

Suite à son engagement dans l'aide humanitaire, il a beaucoup voyagé. Voici, parmi d'autres, un de ses documentaires, réflexion personnelle et exceptionnelle sur son expérience dans l'aide humanitaire.

<iframe width="560" height="315" src="https://www.youtube.com/embed/l9Jd27gS2-g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Ses dessins sont déclinés sur toutes sortes d'objets qui sont en vente à l'international sur [ce site](https://society6.com/flixbrassier). On pourrait peut-être les retrouver dans un magasin de Lalouvesc... Pourquoi pas ?

Exemple de dessin :

![](/media/felix-brassier-dessin-2022.jpg)

Félix travaille actuellement sur une bande dessinée _Atlantis 2.0_ dont le premier tome, déjà terminé, doit paraître cet automne, imprimé à Annonay. Ci-dessous une planche extraite de la BD :

![](/media/felix-brassier-atlantis-2-0-2022.jpg)

Vous pourrez découvrir le travail de Félix sur sa [page Facebook](https://www.facebook.com/felix.brassier.5).

Ils se sont installés rue de la Fontaine, bienvenue à eux !