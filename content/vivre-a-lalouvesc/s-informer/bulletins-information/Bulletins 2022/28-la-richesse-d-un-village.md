+++
date = 2022-10-29T22:00:00Z
description = "n°28 novembre 2022"
header = "/media/entete-automne.jpg"
icon = ""
subtitle = "n°28 novembre 2022"
title = "28 - La richesse d'un village"
weight = 1

+++
La richesse d'un village, c'est d'abord la capacité de ses habitants à construire un avenir en commun. Dans ce Bulletin, nous mettons à l'honneur deux associations dont les bénévoles portent haut les couleurs de Lalouvesc : le Comité des fêtes et le Carrefour des arts. Vous y trouverez aussi des nouvelles des pompiers, de l'école, de la Vie Tara, de l'Abri du pèlerin et encore, comme toujours, bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier](/media/bulletin-de-lalouvesc-n-28-1er-novembre-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du maire

Bonjour à toutes et tous

Nous voilà arrivés en automne, la forêt a changé de couleur. Feuilles rousses, feuilles folles tournent et voltigent au vent, nous préparons l’hiver.  
Je salue la performance du Comité des fêtes pour l’organisation du TRAIL DES SAPINS. Cette magnifique manifestation suscite un intérêt très particulier. Le nombre d’inscrits est en perpétuelle augmentation. Félicitations à toute l’équipe organisatrice, c’est un réel succès !  
L’instant est à la Toussaint. Les travaux du cimetière sont presque terminés. Nous nous attachons et veillons à ce que ce lieu de recueillement pour les familles soit propre.  
Nous réfléchissons déjà à l’organisation de la saison prochaine. Toute l’équipe municipale travaille sur la continuité des projets déjà définis : l’espace Beauséjour, la reconversion des bâtiments Cénacle et Ste Monique, l’espace paysagé du Grand Lieu et bien d’autres sujets encore à développer ! Mais la priorité reste évidemment le réseau d'eau.  
Si tout va bien nous allons pouvoir fêter la nouvelle année comme il se doit en décorant le village comme nous savons le faire.  
Je souhaite à nos enfants de bonnes vacances. L’assemblée générale de l’OGEC vient de se dérouler, je me réjouis d’avoir une bonne équipe administrative et des enseignants motivés pour développer l’école et apprendre à nos jeunes à découvrir une nouvelle sensibilité sur la nature qui nous entoure, de prendre conscience de la chance qu’ils ont d’être en milieu rural, dans un cadre de vie accueillant et sain.  
Je vous souhaite à toutes et à tous de bonnes fêtes de Toussaint en famille. Faites attention au COVID car il y a encore quelques cas. Il faut être prudent.

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Une excellente saison pour le camping

Le camping a fermé ses portes à la fin du mois d'octobre. Il est trop tôt pour faire des comptes précis, mais on constate que, dans la logique d'une excellente saison pour le village, le camping a fait le plein en 2022.

![](/media/camping-2022-1.JPG)

{{<grid>}}{{<bloc>}}

Les mobile-homes ont été pris d'assaut. Le nouveau cottage a rencontré le succès. Les chalets au tarif abordable ont été aussi appréciés, les camping-cars et campeurs ont été nombreux et il y a eu plus de 70 nuitées dans les refuges des Afars. Et les campeurs, comme les Louvetous ont fait un bon accueil au nouveau self-service de machine à laver.

{{</bloc>}}{{<bloc>}}

![](/media/refuge-des-afars-7.jpg)

{{</bloc>}}{{</grid>}}

Les retours des estivants sur le cahier du camping ou [en ligne](https://www.google.com/search?client=firefox-b-d&q=lalouvesc+camping+pr%C3%A9+du+moulin#lrd=0x47f571429bdf2d4d:0xeeff6697329d3a54,1,,,) sont excellents.

Il n'y a guère que la tente lodge qui n'a toujours pas trouvé son public.

### Un cimetière enfin d'aplomb

Les travaux du cimetière sont presque finis. Le cimetière est enfin d'aplomb pour la Toussaint 2022.

On a oublié l'état dans lequel nous l'avions trouvé il y a deux ans. Le chantier fut long car il était délicat. Il ne fallait pas que les tombes s'effondrent. Il a été subventionné à 80% par la DETR (Préfecture) et la Région.

{{<grid>}}{{<bloc>}}

![](/media/cimetiere-lalouvesc-5.jpg "Hier, juin 2020")

{{</bloc>}}{{<bloc>}}

![](/media/cimetiere-octobre-2022-1.jpg)

{{</bloc>}}{{</grid>}}

### Des étangs nettoyés

L'étang du camping et celui du Grand Lieu ont été nettoyés. Ils ont été élargis, leurs abords ont été dégagés. Ils se remplissent... doucement. Même s'il a plu récemment, les sources sont encore modestes.

![](/media/etang-camping-oct-2022.jpg)

![](/media/etang-grand-lieu-oct-2022.jpg)

### Numérique et ruralité

Plateaux numériques, collectif de designers engagés dans une démarche écoresponsable pour le numérique, et la commune de Lalouvesc dont le site lalouvesc.fr sert de démonstrateur, avaient invité à une journée-et-demi d’échanges et de réflexions sur les expériences en cours.

Institutionnels, chercheurs, designers, journalistes et élus ont présenté les résultats de leurs travaux et de leurs réalisations dans une ambiance conviviale.

![](/media/ruralite-et-numerique-2022-2.jpg)

Vous trouverez[ ici un compte-rendu](https://hackmd.io/@timotheegoguely/rencontres-plateaux-numeriques) des échanges, qui pourra encore être complété. En résumé, un numérique adapté aux villages et au monde rural, répondant efficacement aux attentes locales, est possible, mais il est éloigné des injonctions des grands groupes du numérique ou des start-ups, pressés de vendre leurs services en captant l’attention des internautes par la multiplication des applications.  Il doit être simple, accessible, prenant le temps de mesurer les attentes des internautes et être facile à éditer.

### Raccordez-vous à l'assainissement collectif

Il reste encore trop de résidences non raccordées au réseau d'assainissement pour que la station d'épuration puisse fonctionner de façon optimale. Par ailleurs, le Service public d'assainissement non collectif (SPANC) a commencé la révision des systèmes individuels (fosses septiques) dans le village et toutes les installations non conformes devront être réparées, entraînant de coûteux travaux à la charge des propriétaires.

Dans cette perspective, plusieurs Louvetous préfèrent se regrouper entre voisins pour réaliser un raccordement mutualisé au réseau collectif. C'est une bonne idée, gagnante pour tout le monde.

Mais cette option n'est pas toujours réalisable. Tout dépend de la situation des résidences concernées. Si  vous êtes intéressés, nous vous suggérons de prendre rapidement contact avec la Mairie pour étudier les possibilités. Des travaux sur le réseau d'assainissement collectif devraient démarrer prochainement.

### Eau trouble, on avance

{{<grid>}}{{<bloc>}}

![](/media/reparation-eau-2022.jpg)

{{</bloc>}}{{<bloc>}}

Le réseau d'eau potable reste la priorité de la Mairie. Avec l'aide de la société Naldéo, petit à petit à force de jouer sur les vannes qui pilotent le réseau, le repérage des portions défaillante, celles qui produisent les particules gênantes et celles où l'eau stagne trop longtemps, se fait plus précis. C'est malheureusement un travail délicat et de longue haleine. Une fois le diagnostic terminé, il faudra changer quelques vannes pour que les purges indispensables se réalisent de façon automatique. Et surtout, il faudra remplacer les tuyaux qui se dégradent. Tout cela aura un coût qu'il faudra supporter.

Par ailleurs, d'autres fuites se sont déclarées sur le réseau, côté mont Chaix, en octobre. Elles ont été réparées (cf. photo ci-contre de la réparation du 28 octobre). Mais de ce côté-là aussi la vétusté du réseau atteint ses limites. Là encore des travaux d'envergure devront être lancés.

{{</bloc>}}{{</grid>}}

## Zoom : La réussite spectaculaire de deux associations louvetonnes

### Le Comité des fêtes fait des étincelles

Le Comité des fêtes de Lalouvesc est salué... et envié en haute-Ardèche pour son dynamisme et son savoir-faire. Voici quelques exemples de sa réussite.

#### Des artistes décorent le village

{{<grid>}}{{<bloc>}}

C'est devenu une tradition. Trois fois par an les artistes du Comité des fêtes rivalisent d'imagination dans la bonne humeur pour décorer le village devant la place du Lac. Retrouvez-les grâce aux liens sur les photos du FB du Comité : pour [Noël](https://www.facebook.com/comitedesfeteslalouvesc/posts/pfbid0vGUrpyoXLhCavL8tsJZ4thBg3MyQdfSEn6riTWXC82SfFprCdykB7DDfSYis1WCFl), pour[ ](https://www.facebook.com/groups/47779411002/permalink/10158680191396003/)[l'Ardéchoise](https://www.facebook.com/comitedesfeteslalouvesc/posts/pfbid0fqGzFL1ETSzFVJskDEu9ZjuUJYJsot24NXR6mg77R6WMBQgVJsxh8T7zVs4C9vHvl) et pour le [Trail (Halloween)](https://www.facebook.com/comitedesfeteslalouvesc/posts/pfbid0LBZPs3x89nEc2W6fX5Ro9RPK5EYEo54xDC9avbnZUxzLxDyW8KNJbsFp5eR5yMail).

Ainsi le village fait tout au long de l'année un clin d’œil à ses visiteurs. Et chaque année est différente. On vient à Lalouvesc pour découvrir et sourire des trouvailles... et même parfois pour s'en inspirer et les reproduire ailleurs... car il n'y a pas de copyright.

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2021-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-ardechoise-2022-14.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/deco-trail-2022-1.jpg)

{{</bloc>}}{{</grid>}}

#### Des manifestations qui font le plein

Le second talent du Comité est sa capacité à organiser et promouvoir des manifestations d'ampleur.  Année après année, le Comité des fêtes a accumulé matériel, savoir-faire et motivation des bénévoles devant les succès qui s'enchaînent. C'est un capital précieux que l'association réinvestit dans des projets chaque fois plus ambitieux.

L'année 2022 a été marquée par deux réussites exceptionnelles, qui toutes deux ont explosé les objectifs initialement fixés : la brocante et le Trail des Sapins.

La brocante 2022 avait élargi sa surface pour accueillir près de 300 exposants. Et pourtant un mois avant la manifestation, les réservations étaient complétées. Ce fut un franc succès d'affluence favorisé par la météo, une organisation impeccable et une ambiance bon enfant. Près de 5.000 visiteurs se sont retrouvés à la [brocante](https://www.facebook.com/comitedesfeteslalouvesc/posts/pfbid0bPDJgRZnisH3kX9QGfJbe6MimjeSj8fQPGDLFCMArCVJUAajxzJT2cLz4rnwcERAl) de Lalouvesc le dimanche 5 septembre.

![](/media/brocante-2022-9.jpg)

##### Le Trail des Sapins

L’annulation du TRAIL DES SAPINS en 2020 avait fait du mal dans le cœur des bénévoles. 2021 avait pansé les plaies malgré les nombreuses restrictions imposées et une météo difficile…

2022\. Ha ! Ha ! 2022 c’est le retour en force du succès. La jauge déterminée de 1.000 participants a été atteinte bien avant la date fixée pour la clôture des inscriptions. Le Comité des fêtes, devant la déception de nombreux retardataires, a ouvert la porte à des rattrapages… Au final quelques 1.200 sportifs étaient là. Quelques 1.200 sportifs ont foulé le sol de notre camping, de nos chemins forestiers et ceux des villages voisins. Toutes les régions de France étaient représentées ce 29 octobre à Lalouvesc… et aussi la Grande Bretagne, le Luxembourg et l’Espagne qui ont aussi envoyé un ambassadeur.

{{<grid>}}{{<bloc>}}

![](/media/trail-2022-depart-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/trail-2022-depart-2.jpg)

Dans le cadre de la campagne OCTOBRE ROSE, le Comité des Fêtes n’est pas resté insensible à cette noble cause et a tenu à se mobiliser et à soutenir l’Association@SeinformerCancer. Rappelez-vous la maxime sérigraphiée sur le tee-shirt rose : **_Rien n’est impossible à ceux qui osent_**. Le Comité des fêtes a osé voir en grand. Il a grandement réussi.

{{</bloc>}}{{</grid>}}

Beau temps, bonne humeur, ambiance Halloween, et souffle court.

{{<grid>}}{{<bloc>}}

![](/media/trail-2022-depart-photo-ot-1.jpg "Photo OT")

{{</bloc>}}{{<bloc>}}

![](/media/trail-2022-depart-photo-ot-3.jpg "Photo OT")

{{</bloc>}}{{</grid>}}

![](/media/trail-2022-depart-4.jpg)

![](/media/trail-2022-depart-5.jpg)

Place à quelques semaines de repos mérité et fort de ces derniers succès nous le retrouverons en fin d’année pour embellir le square.

### Le Carrefour des arts étend son activité

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-ecole-5.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-voleurs-de-swing-2022.jpg)

Le Carrefour des arts embellit le village chaque été dans une exposition d'artistes au CAC. Il a repris l'activité des Promenades musicales en 2022. Il ambitionne maintenant d'étendre son activité tout au long de l'année et sur un territoire plus large.

{{</bloc>}}{{</grid>}}

#### Une vente aux enchères

Après une exposition estivale qui a accueilli plus de 7.000 visiteurs (et battu les records de vente d’œuvres), le Carrefour des Arts a clôturé sa saison 2022 en beauté avec une vente aux enchères le dimanche 30 octobre, organisée en partenariat avec l’association La Source Annonay.

Le Carrefour des Arts a mis en vente des photographies du photographe Serge Roussé de la collection « d’Encre et d’Eau » en exposition plein air depuis le 23 mai sur l'espace Beauséjour. La Source Annonay, pour sa part, a proposé des « robes durcies », œuvres de l’artiste plasticienne Danielle Stéphane. Dix œuvres ont été vendues pour un total de 1.500 €.

![](/media/vente-aux-encheres-carrefour-2022-1.jpg)

![](/media/encheres-carrefour-2.jpg)

Cet événement témoigne d'un rapprochement entre les deux associations, actives en haute Ardèche depuis plusieurs années qui partagent une vocation similaire, celle de la promotion de l'art et de son potentiel éducatif et social.

_La Source Annonay_ est une association à vocation sociale et éducative par l’expression artistique, à destination des enfants et des jeunes en difficulté et de leurs familles.

#### Un musée virtuel

En 2023, le _Carrefour des Arts_ ambitionne de développer ses activités d’éducation artistique et culturelle toute l’année. Il a été retenu pour implanter un projet [_Micro-Folie_](https://www.culture.gouv.fr/Actualites/Micro-Folies-l-irresistible-succes-d-un-dispositif-innovant) en Val d’Ay. Ce dispositif est initié par le ministère de la Culture pour faciliter l'accès à la culture des habitants éloignés des centres urbains. Il s'agit d'un musée virtuel réunissant plusieurs milliers d’œuvres numérisées en très haute définition de douze établissements fondateurs : le Centre Pompidou, le Château de Versailles, la Cité de la Musique –Philharmonie de Paris, le Festival d’Avignon, l’Institut du monde arabe, le Louvre, le Musée national Picasso-Paris, le musée d’Orsay, le Musée du Quai Branly-Jacques Chirac, l’Opéra national de Paris, la Réunion des musées nationaux – Grand Palais, Universcience et La Villette.

Dès le début de l’été 2023, la _Micro-Folie_ du Val d’Ay donnera la possibilité aux collectivités et établissements scolaires de découvrir les trésors des plus grandes institutions nationales, soit en accès libre, en mode conférencier, ou par biais d’animations scolaires.

En juillet et août, le dispositif sera installé au CAC en complément de l’exposition annuelle qui fêtera sa 35ème édition.

## Sanctuaire

### Une bonne saison pour l'Abri du pèlerin

Le 31 octobre, après le Trail, l’Abri a fermé ses portes pour les mois d’hiver. L’année 2021-2022 fut une année bien remplie !

#### Des rénovations

Avec l’aide précieuse de bénévoles, l’Abri du pèlerin a pu bénéficier de quelques travaux de rénovation :

{{<grid>}}{{<bloc>}}

* réaménagement du réfectoire avec une nouvelle cuisine, installation d’un lave-linge,
* aménagement d’un petit coin cosy autour de la cheminée,
* création d’un salon à l’entrée de l’Abri pour accueillir les pèlerins et randonneurs fatigués par leur journée de marche (l’étape, sur le Chemin de St Régis, de St Agrève à Lalouvesc fait 27km avec un dénivelé de 900m),
* quelques chambres repeintes,
* et, cette dernière semaine, le remplacement d’une partie des volets par la menuiserie Besset !

{{</bloc>}}{{<bloc>}}

![](/media/abri-du-pelerin-oct-2022-1.png)![](/media/abri-du-pelerin-oct-2022-2.png)

{{</bloc>}}{{</grid>}}

#### De nouveaux lits

Un autre chantier assez conséquent fut le remplacement des 70 lits d’hôpitaux (dont avait bénéficié l’Abri il y a certainement plus d’une trentaine d’années) par 70 lits offerts par le Collège jésuite Saint Joseph d’Avignon suite à la fermeture d’un internat. Grâce encore à quelques bénévoles, nous avons pu les chercher à Avignon en deux voyages, les nettoyer puis les monter dans les chambres et dortoirs.

#### Un site internet pour les inscriptions

{{<grid>}}{{<bloc>}}

![](/media/abri-du-pelerin-oct-2022-3.png)

{{</bloc>}}{{<bloc>}}

Avec la création du site internet [www.abridupelerin-lalouvesc.fr](http://www.abridupelerin-lalouvesc.fr), en lien direct avec la base de données des hébergements de l’Agence du tourisme d’Ardèche, la saison pouvait démarrer !

{{</bloc>}}{{</grid>}}

#### 1.730 nuitées, 1.110 personnes, 658 pèlerins et randonneurs, 35 groupes, 3 familles

Entre le 1 mai 2021 et le 31 octobre 2022 nous avons offert un peu plus de 1.730 nuitées (enfants et adultes confondus). Cela correspond à une hausse de la fréquentation de plus de 40% par rapport à l’an passé. Elle est due, à plusieurs facteurs : d’une part la météo très favorable en juillet et d’autre part le site internet qui facilite les inscriptions !

#### Une collaboration heureuse entre bénévoles

{{<grid>}}{{<bloc>}}

Ce fut pour moi un réel plaisir de travailler avec l’ensemble des bénévoles durant cet été (Nicole, Geneviève de Valence, Jean-Pascal et Corinne de Normandie, Nicole et Roger de Lyon, Christine de Chateauneuf-de-Galaure, Chantal une de mes sœurs d’Alsace, Élisabeth de Tence…). Il y a une véritable relation de confiance qui existe entre nous et cela nous permet de vivre paisiblement ce service pour le pèlerinage.

{{</bloc>}}{{<bloc>}}

![](/media/abri-du-pelerin-oct-2022-4.png)

{{</bloc>}}{{</grid>}}

#### L’Abri fête ses 100 ans en 2023 !

Il reste néanmoins des questions sur l’avenir de cette belle aventure qu’est l’Abri du pèlerin, surtout liées aux financements de travaux qu’il faut prévoir dans les années à venir (remplacement des velux, fenêtres, et probablement la toiture…).

En 2023 nous fêterons les 100 ans de l’Abri ! Si vous avez des anecdotes à nous partager autour de la vie de l’Abri du pèlerin et de la salle des fêtes, n’hésitez pas à nous contacter, je serai heureux de les publier sur le site… Nous pourrions aussi les partager lors d’une soirée « 100 années d’histoires à raconter à l’Abri ! »

Fr. Yves Stoesel s.j.

## Octobre à l'école

La rentrée s'est bien passée pour tout le monde.

Les élèves ont découvert de nouvelles maîtresses. Cette année, l'équipe pédagogique est composée de : Sasha Haon (enseignante et chef d'établissement), Pauline Lafond et Élodie Mathieu (enseignantes), Véronique Petit (Asem) et Mathilde Groud (AESH). Et nous avons à ce jour 24 élèves inscrits de la TPS au CM2.

Plusieurs temps forts pour cette première période :

{{<grid>}}{{<bloc>}}

![](/media/ecole-oct-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecole-oct-2022-1.jpg)

{{</bloc>}}{{</grid>}}

Tout d'abord les enfants ont pu bénéficier de l'activité poney financée par l'APEL, six séances durant lesquelles les enfants ont bien sûr monté mais aussi pratiqué de la voltige sous la houlette de Anne (monitrice) et de Françoise.

{{<grid>}}{{<bloc>}}

![](/media/ecole-oct-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecole-oct-2022-6.jpg)

Pour la semaine du goût, Régis Marcon est venu parler des différentes saveurs avec dégustations à l’appui (un régal pour les papilles), une intervention fort appréciée par les enfants comme par les adultes.

{{</bloc>}}{{<bloc>}}

![](/media/ecole-oct-2022-2.jpg)

Enfin, jeudi 20 octobre nous avons passé une journée avec le Père Michel avec qui les enfants ont pu admirer les vitraux de la basilique, visite du musée St Régis (merci à Séverine de l'OT qui nous a si bien guidés), pique-nique dans la galerie de la maison des Pères et pour l’après-midi, saynètes sur la vie de Jean-François Régis interprétées par les enfants dans la salle du CAC sous l’œil de quelques membres du Club des deux clochers.

{{</bloc>}}{{<bloc>}}

![](/media/ecole-oct-2022-3.jpg)

{{</bloc>}}{{</grid>}}

Bien sûr, nous avons aussi pratiqué du sport, plusieurs anniversaires ont été fêtés comme il se doit... et les apprentissages sont allés bon train !!

## Culture - Loisir

### La vie Tara, monastère moderne

Bonjour Louvetounes & Louvetous !

Pour ceux qui ne le savent pas encore, voici quelques informations sur ce que nous faisons et souhaitons faire.

La Vie Tara est maintenant un monastère moderne, dans lequel les gens peuvent parvenir à eux-mêmes par la méditation et le développement personnel. La Vie Tara était un pensionnat de jeunes filles tenu par des sœurs d'inspiration ignatienne avant de devenir un centre de retraite et de vacances. A présent, il se transforme en un lieu où les gens peuvent venir faire une retraite pour se rétablir et se régénérer, à travers de la méditation, des temps de réflexion, mais aussi par la musique et l'art. La priorité est au bien-être, à la spiritualité et à la durabilité.

L'été dernier, nous avons vécu des expériences merveilleuses travaillant en collaboration avec les « Promenades musicales » et le « Carrefour des Arts ». Recevoir Gilles Lefèvre et sa troupe d’artistes séjournant à La Vie Tara fut un moment magique, La vie Tara vibrait au son de leurs répétitions.

![](/media/louvetous-la-vie-tara-2022.png)

Nous avons été également très heureux de voir que les gens de la région commençaient à venir nous retrouver.

Après une retraite de méditation de deux semaines avec nos groupes d’étudiants cet été, nous ressentons encore plus fortement que La Vie Tara est parfaite pour cela : l'énergie du lieu est très favorable au développement spirituel et psychologique !

{{<grid>}}{{<bloc>}}

![](/media/retraite-la-vie-tara-2022-2.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/retraite-la-vie-tara-2022-1.jpeg)

{{</bloc>}}{{<bloc>}}

Oh oui, beaucoup de choses pratiques se sont déjà passées ces derniers mois. Les réparations du bâtiment ont commencé. Nous avons également fait réparer et accorder notre très beau piano et aussi nettoyé et restauré beaucoup de choses, trop nombreuses pour toutes les mentionner. Des amis et membres ont fabriqué de nombreux coussins de méditation à partir des vieux rideaux de La Vie Tara ! Nous avons donc du beau matériel fait main pour le yoga et la méditation ;) C'est une bénédiction pour nous de pouvoir l'utiliser tous les jours dans ce magnifique endroit serein.

Ce fut un été, notre premier, à jamais inoubliable pour nous !

Et à présent, nous nous régénérons, prévoyant des plans pour cet hiver et la saison à venir, tout en poursuivant la rénovation du bâtiment.

{{</bloc>}}{{<bloc>}}

![](/media/concert-chapelle-2022.jpeg)

{{</bloc>}}{{</grid>}}

Nous travaillons toujours activement pour devenir un bâtiment public, mais malheureusement cela prendra encore quelques années. La bâtisse est immense et les travaux nombreux. C'est pourquoi nous avons commencé par une association : "l'Association La Vie Tara".

Alors n'hésitez pas et faites-nous savoir si vous voulez prendre un café avec nous ou si vous souhaitez plus d'informations. Les portes de La vie Tara vous seront toujours ouvertes.

L’équipe La Vie Tara : Ine, Romain, Michaël, Gigi, Joke et Jessica

Contact:

* [www.lavietara.eu](http://www.lavietara.eu)
* Romain : [management@lavietara.eu](mailto:management@lavietara.eu)
* Nous disposons également d'une liste de diffusion. Si vous souhaitez recevoir notre bulletin d'information, veuillez vous [inscrire ici](http://eepurl.com/h0Pju9).

### Une vidéo pour le chemin de Saint Régis

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y8mcHj8mtZA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Commerçants - artisans

### Café du Lac

Le café sera fermé du lundi 31 octobre jusqu'au jeudi 17 novembre, réouverture pour le beaujolais nouveau. Fermé également le week-end du 26 - 27 novembre.

### Salon de coiffure

Le salon de coiffure sera fermé du mercredi 16 novembre au mercredi 23 novembre inclus.

## Santé -sécurité

### Porte ouverte chez les pompiers

{{<grid>}}{{<bloc>}}

![](/media/pompiers-2022-4.png)

{{</bloc>}}{{<bloc>}}

Samedi 5 novembre 2022, les pompiers de Lalouvesc ouvrent les portes de leur caserne afin de partager leur passion à travers différents ateliers et démonstrations tout au long de la matinée.

Cette visite est l’occasion d’échanger avec les hommes du feu, c’est le moment de poser toutes les questions qui vous taraudent. Vous toucherez du doigt ce qu’est la vie du pompier à l’intérieur de son centre en visitant les locaux de la caserne.

Vous pourrez même entrer dans la peau du pompier avec des ateliers spécifiques et l'utilisation de matériels.

{{</bloc>}}{{</grid>}}

Les pompiers de Lalouvesc c'est en 2022 : neuf volontaires qui ont pour l'instant assuré quatre-vingt-cinq interventions, allant du secours à personne, au feu de forêt.

{{<grid>}}{{<bloc>}}

![](/media/pompiers-2022-1.jpg)

Avec une saison de feu de forêt très intense les pompiers, de l'Ardèche ont été mis à rude épreuve et les pompiers de Lalouvesc par la même occasion.

{{</bloc>}}{{<bloc>}}

![](/media/pompiers-2022-3.jpg)

{{</bloc>}}{{</grid>}}

Notre petit centre n'a pas démérité avec pas moins de cinq interventions "feu de forêt" durant la période juillet/août.

On vous attend nombreux.

Lionel Achard

### Café des aidants

Le prochain café des aidants de St Félicien aura lieu samedi 5 novembre 2022 de 10h à 11h30 à l'Auberge de St Félicien sur le thème « Face aux conseils et regard des autres, comment je réagis ? ». Samedi 3 décembre le thème sera « Etre aidant, c'est aussi du positif », même endroit, même heure.  
Les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).

Renseignements auprès de Carole Guilloux : [cguilloux@fede07.admr.org](mailto:cguilloux@fede07.admr.org) ou 06 81 50 19 26

### Prévention des cambriolages

La gendarmerie rappelle que _la période qui suit le changement d'horaire est propice aux cambrioleurs qui profitent de la tombée de la nuit pour repérer les habitations inoccupées._

Conseils utiles à tous pour se prémunir contre les atteintes aux biens sur ce document : « [Contre les cambriolages, ayez les bons réflexes !](https://www.lalouvesc.fr/media/flyer_contre_les_cambriolages-2022.pdf) ».

### Portes ouvertes au Sytrad

Le Sytrad organise des journées portes ouvertes.

{{<grid>}}{{<bloc>}}

![](/media/centre-de-tri-1-2022.jpg)

{{</bloc>}}{{<bloc>}}

**Le samedi 19 novembre 2022** 14h 15h et 16h : Visite du centre de valorisation d’Etoile-sur-Rhône (les enfants de moins de huit ans ne sont pas acceptés) :[ Inscriptions](https://docs.google.com/forms/d/e/1FAIpQLScGu2XWq9xYpAPsOZBrR5m3l_Ns8FErSCkCXuksl7NIPEvXNQ/viewform)

**Tous les premiers mardis du mois (à compter de décembre) :** Visite du centre de tri de Portes-lès-Valence : [Inscriptions]()

{{</bloc>}}{{</grid>}}

### Suite du calendrier Santé-Environnement

L’ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l’habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante). Pour le mois d'octobre, la suggestion concerne le [radon](https://www.auvergne-rhone-alpes.ars.sante.fr/le-radon-0?parent=5387). Un autre calendrier alerte sur les [bonnes pratiques face à l’environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0). Pour le mois de septembre, la suggestion concerne les[ téléphones mobiles](https://www.auvergne-rhone-alpes.ars.sante.fr/telephones-mobiles?parent=14636).

{{<grid>}}{{<bloc>}}

![](/media/int-11_radon.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ext-11_telephone.jpg)

{{</bloc>}}{{</grid>}}

## Dans l'actualité, le mois dernier

#### Gîtes de France recherche un chargé de développement pour l'Ardèche-nord

Fiche de poste [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/gites-de-france-recherche-un-charge-de-developpement-pour-l-ardeche-nord/ "Lire Gîtes de France recherche un chargé de développement pour l'Ardèche-nord")

#### Portes ouvertes au collège de Satillieu

17-19 novembre [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/portes-ouvertes-au-college-de-satillieu/ "Lire Portes ouvertes au collège de Satillieu")

#### Fermeture exceptionnelle de la déchetterie

Samedi 5 novembre [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/fermeture-exceptionnelle-de-la-dechetterie/ "Lire Fermeture exceptionnelle de la déchetterie")

#### Attention le COVID revient...

A Lalouvesc et ailleurs... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/attention-le-covid-revient/ "Lire Attention le COVID revient...")

#### Pneus neiges obligatoires à partir du 1er novembre

Carte du territoire concerné et précisions [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/pneus-neiges-obligatoires-a-partir-du-1er-novembre-1/ "Lire Pneus neiges obligatoires à partir du 1er novembre")

## Puzzle : le marché de Lalouvesc dans les années cinquante

Que vendait-on sur le marché de Lalouvesc en 1953 ?

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=3980f6b79ac3&view=iframe" style="width:100%;height:600px" frameborder="0" allowfullscreen></iframe>