+++
date = 2022-11-28T23:00:00Z
description = "n°29 décembre 2022"
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "n°29 décembre 2022"
title = "29 - D'un chantier à l'autre"
weight = 1

+++
Dans ce Bulletin, deux chantiers qui redessinent le village sont présentés, un qui s'achève (l'écolotissement du Bois de Versailles) et un qui va démarrer (l'espace Beauséjour). On y trouve aussi des nouvelles des employés municipaux, du recensement, du centre équestre, du Club des deux clochers, le premier épisode du nouveau feuilleton des enfants de l'école et encore, comme toujours, bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier](https://www.lalouvesc.fr/media/bulletin-de-lalouvesc-n-29-1er-decembre-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du maire

Déjà début décembre, Noël arrive à grands pas, les décorations de Noël vont bientôt être installées. Nous allons illuminer plusieurs points de notre village pour le rendre attrayant, attractif et accueillant.

Le temps a été clément jusqu’à maintenant, nous avons passé un très agréable mois de novembre mais ça se refroidi, les premières gelées sont là, pensez à vos équipements d’hiver sur vos autos, des contrôles sont prévus par les forces de l’ordre.

Nous allons retirer toutes les poubelles à couvercle cette première semaine de décembre et installer les nouvelles colonnes à ordures ménagères et cartons bruns. La société de sous-traitance **COVED débute la nouvelle collecte des ordures ménagères et des cartons bruns à partir du lundi 5 décembre**.

Nous sommes en plein travail pour préparer les dossiers de demande de subvention afin de continuer nos travaux sur le réaménagement de l’espace Beauséjour et également sur l’organisation du devenir du Cénacle et du bâtiment sainte Monique. Nous aurons l’occasion de vous expliquer tout cela à l’occasion des vœux du maire début janvier.

En suivant l'exemple de la mairie et du comité des fêtes, je vous encourage à décorer, pour les commerçants, vos vitrines, pour les particuliers, vos habitations. Les guirlandes actuelles à LED ont une consommation insignifiante, il en est de même de notre éclairage publique full LED.

Passez toutes et tous un joyeux Noël, réunissez-vous en famille, c’est un grand moment, les enfants sont aux anges. Bientôt les vacances, les cadeaux et peut-être la neige…

Amitiés.

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### De nouveaux employés municipaux

Il y a eu beaucoup de mouvements chez les employés municipaux ces derniers mois, d'autres départs sont aussi prévus. Voici donc un point complet pour s'y retrouver.

**Service administratif**

{{<grid>}}{{<bloc>}}

![](/media/mireille-morel-3.jpg "Mireille Morel, secrétariat")

{{</bloc>}}{{<bloc>}}

La mairie a recruté récemment Mireille Morel comme secrétaire de mairie. Celle-ci est appelée à remplacer Françoise Arenz Faurie qui partira à la retraite dans le courant de l’année prochaine. Elle se forme actuellement à son nouveau métier.

Valérie Pérez reste actuellement en renfort du secrétariat pour la partie comptabilité.

Sylvie Deygas est employée au camping, à l’entretien et à la poste.

{{</bloc>}}{{</grid>}}

**Service technique**

{{<grid>}}{{<bloc>}}

![](/media/christian-bruc-3.jpg "Chritian Bruc, service technique")

{{</bloc>}}{{<bloc>}}

Christian Bruc vient d’être recruté comme employé communal. Il remplace Patrick Mazoyer qui a quitté son emploi à la fin de son contrat en septembre. Et il vient seconder Dominique Balaÿ qui terminera son contrat fin mars 2023.

Nous avons donc actuellement deux employés municipaux opérationnels au service technique.

{{</bloc>}}{{</grid>}}

Plusieurs autres employés sont en congé maladie et sur le départ :

● Henri Faurie est en arrêt de travail pour raison de santé.

● Gérard Fraysse est en arrêt de travail pour cause de maladie et doit prendre sa retraite courant 2023.

● Christian Deygas est en congé longue maladie.

Bienvenue aux nouveaux employés ! Il faut dire aussi un grand merci à tous les employés municipaux. C'est grâce à eux, à leur dévouement, leurs compétences et leur énergie, que la maison commune qu'est la mairie a pu et peut encore rendre les services indispensables à la population.

### Un véhicule électrique pour la mairie

Les véhicules de la commune arrivent en fin de vie et il est indispensable d’acquérir rapidement un nouveau véhicule type fourgonnette. Il a paru opportun d’étudier la piste des véhicules électriques pour montrer la volonté de la commune à s’engager dans la transition écologique et aussi profiter des aides de l’État qui ne dureront peut-être pas.

Par ailleurs, compte tenu de l’utilisation actuelle des véhicules et aussi des conséquences sur le budget de la commune, la solution du crédit-bail est apparue comme la plus appropriée.

La prime de l’État pour l’achat d’un véhicule électrique (5.000 €) plus la récupération d’un véhicule ancien (7.000 €) couvre la première mensualité du crédit-bail. Les mensualités sont d’un peu plus de 500 € sur 60 mensualités avec une valeur de rachat de 10% en fin de contrat. Ces sommes comprennent l’entretien du véhicule sauf le changement des pneus.

Compte-tenu de ces éléments, le Conseil a voté l'achat d'un berlingot-citroën électrique accessible rapidement.

Par ailleurs, l'utilitaire avec benne est actuellement en réparation, suite à un accident. Il devrait être de nouveau opérationnel rapidement. Avec le tracteur quasi-neuf, les employés municipaux disposeront de tous les véhicules indispensables pour leur travail.

### Les plaques de rues s'installent

{{<grid>}}{{<bloc>}}

![](/media/adressage-nov-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

Les employés municipaux posent progressivement les nouvelles plaques de rues, puis viendra la pose des numéros pour chaque maison. L'ensemble devrait être terminé pour cette fin d'année.

Un courrier sera déposé dans chaque boite aux lettres avec la précision de la nouvelle adresse et les éventuelles démarches à réaliser pour les changements d'adresse. Comme déjà indiqué, vous pouvez déjà repérer la vôtre sur la [base nationale d'adresses](https://adresse.data.gouv.fr/base-adresse-nationale/07128#11.9/45.117/4.5215/0/2).

Il faut néanmoins faire preuve de patience. L'expérience des autres communes montre que l'intégration des nouvelles adresses prend du temps sur les cartes, chez les différents expéditeurs et livreurs, même si elle est déjà opérationnelle sur les GPS... à condition qu'ils aient été mis à jour.

{{</bloc>}}{{</grid>}}

### Le recensement se rapproche

En 2023, Lalouvesc doit réaliser le recensement de sa population pour mieux connaître son évolution et ses besoins. L’ensemble des logements et des habitants seront recensés **à partir du 18 janvier 2023**.

#### Comment ça se passe ?

{{<grid>}}{{<bloc>}}

![](/media/nicole-porte-2022-2.JPG "Nicole Porte, agent recenseur")

{{</bloc>}}{{<bloc>}}

Une lettre sera déposée dans votre boîte aux lettres début janvier. Puis, l'agent recenseur, Nicole Porte, vous fournira une notice d’information soit dans votre boîte aux lettres soit en mains propres. Suivez simplement les instructions qui y sont indiquées pour vous faire recenser. Ce document est indispensable, gardez-le précieusement.

Se faire recenser en ligne est plus simple et plus rapide pour vous, et également plus économique pour la commune. Moins de formulaires imprimés est aussi plus responsable pour l’environnement. Si vous ne pouvez pas répondre en ligne, des questionnaires papier pourront vous être remis par l’agent recenseur.

{{</bloc>}}{{</grid>}}

Les informations seront transmises à l'INSEE et anonymisées. Elles ne sont traitées qu'à des fins statistiques et **aucune donnée particulière n'est transmise à une administration ou une quelconque institution**.

#### Pourquoi êtes-vous recensés ?

Le recensement de la population permet de savoir combien de personnes vivent en France et d’établir la population officielle de chaque commune. Le recensement fournit également des statistiques sur la population : âge, profession, moyens de transport utilisés, et les logements…

Les résultats du recensement sont essentiels. Ils permettent de :

1\. Déterminer la participation de l’État au budget de notre commune : **plus la commune est peuplée, plus cette dotation est importante !** Répondre au recensement, c’est donc permettre à la commune de disposer des ressources financières nécessaires à son fonctionnement.

2\. Définir le nombre d’élus au conseil municipal, le mode de scrutin, le nombre de pharmacies…

3\. Identifier les besoins en termes d’équipements publics collectifs (transports, écoles, maisons de retraite, structures sportives, etc.), de commerces, de logements…

Voir la ["photographie" de Lalouvesc ](https://www.insee.fr/fr/statistiques/2011101?geo=COM-07128)réalisée par l'Insee à partir des précédents recensements.

Pour en savoir plus sur le recensement de la population, rendez-vous sur le site [le-recensement-et-moi.fr](http://le-recensement-et-moi.fr/rpetmoi/).

### L'étude sur le Cénacle et Ste Monique avance

Une réunion du Comité de pilotage de l'étude du SCOT s'est tenue en Mairie le 14 novembre dernier en présence de sœur Jacqueline représentant les sœurs du Cénacle.

![](/media/cenacle-ste-monique-scot-nov-2022.png)

Le bureau d'études a présenté l'[avancement de sa réflexion](https://www.lalouvesc.fr/media/ue220210-scot-lalouvesc-copil-01-221114.pdf). Des scénarios commencent à s'esquisser. Les scénarios élaborés seront présentés au cours d'une réunion publique dans la troisième semaine de janvier. Le Conseil, appuyé par le Comité de développement devra privilégier un ou deux scénarios qui feront alors l'objet d'une étude technique et stratégique plus précise et chiffrée. Cette étude permettra à la Mairie de prendre une décision lucide sur l'avenir de Ste Monique et aux acheteurs et porteurs de projets pour le Cénacle de se faire connaître en toute connaissance des opportunités et des contraintes de la propriété.

Malgré plusieurs visites, aucun porteur de projet n'a, à l'heure actuelle, présenté une proposition acceptable.

## Zoom sur deux chantiers

### Le Bois de Versailles est un succès

L'écolotissement du Bois de Versailles était un projet mis en place par la municipalité précédente à l'occasion de l'élaboration du PLU. C'était une bonne idée pour dynamiser le village qui n'a pas pu se concrétiser tout de suite.

![](/media/ecolotissemet-plan.jpg)  
A notre arrivée, nous l'avons relancé avec quelques ajustements pour qu'il soit plus attractif et ne pèse pas sur les finances de la Commune. Aujourd'hui, nous pouvons affirmer que l'opération est un succès :

* L'aménagement est quasi-terminé. Les derniers travaux de montage des murets sont en cours. Il ne restera plus que l'enrobage, le revêtement de la voie.
* L'ensemble des lots sont réservés. Trois ventes et deux compromis sont signés. les autres compromis sont en cours de rédaction. Une liste d'attente a été ouverte. Si vous êtes intéressés, contactez rapidement la Mairie, des désistements sont toujours possibles. Deux ventes sont pour des occupations à l'année, la plupart des autres pour des résidences secondaires occupées sur la durée, une pour des locations.
* Le financement des travaux d'aménagement a été couvert par un prêt _in fine_ (c'est-à-dire qui se rembourse au fur-et-à-mesure des ventes) de 270.000 €. Comme les ventes sont effectives, ce sera une opération blanche pour la Commune.

{{<grid>}}{{<bloc>}}

![](/media/ecolotissement-nov-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecolotissement-nov-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

Et, cerise sur le gâteau, suivant les recommandations du règlement, plusieurs futurs propriétaires se sont lancés dans la construction bois, et notamment avec des projets en fustes. Vous pourrez ainsi bientôt admirer les maisons en cours de montage sur le terrain de Thomas Couette.

**Ainsi, la réussite d'un projet inscrit au PLU, de nouveaux logements sur la Commune, pas de frais pour la Commune et un coup de pouce à l'industrie locale du bois : c'est un carré d'as !**

{{</bloc>}}{{<bloc>}}

![](/media/fustes-couette-1.jpeg)

{{</bloc>}}{{</grid>}}

### L'espace Beauséjour est dessiné

{{<grid>}}{{<bloc>}}

Début 2022, l'hôtel Beauséjour s'est effacé...

![](/media/demolition-beausejour-2022-61.jpg)

{{</bloc>}}{{<bloc>}}

... fin 2022, il reste un espace à aménager.

![](/media/beausejour-nov-2022-3.jpg)

{{</bloc>}}{{</grid>}}

Parallèlement, une consultation a été lancée pour un jeu-monument sous forme de [cartes postales](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/#synth%C3%A8se-des-cartes-postales-sur-le-jeu-monument) et les enfants de l'école St Joseph ont aussi donné [leur avis](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#des-id%C3%A9es-pour-le-jeu-monument).  Au printemps, un concours destiné aux étudiants en architecture a permis d'imaginer un jeu-monument pour accompagner l'aménagement. Guillaume Diche et Julie Demeulenaere, les lauréats, ont proposé un "[mur habité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/22-ce-qui-plait/#jeu-monument--une-premi%C3%A8re-%C3%A9tape-franchie)".

Maintenant, nous sommes à l'étape des choix pour l'aménagement général. La société Archipolis a été mandatée pour le dessiner. Un avant-projet définitif a été élaboré suite à plusieurs consultations et suggestions. Le Comité de développement  a validé le choix avec quelques modifications pour en alléger les coûts.

#### Le projet

![](/media/beausejour-nov-2022-1.png)

On y trouve à la hauteur de la rue une esplanade comprenant une partie pavée et une autre en stabilisé avec deux arbres. A ce niveau une végétation et une marche faisant office de banc court le long du mur de la façade. L'esplanade se termine par une pergola qui ouvre sur le jardin d'un côté et sur une rampe en pente douce de l'autre. Pour des raisons d'économie, la pergola sera marquée principalement par un effet de végétation.

Le jardin est formé d'une grande pelouse arborée entourée par une allée et de bancs réalisés avec les pierres de l'ancien hôtel. Sous la pelouse se trouve une citerne qui récupère les eaux pluviales qui serviront à l'arrosage de ce niveau et du niveau inférieur. Il se termine par une terrasse avec une vue sur le parc du Val d'Or et les Cévennes.

La rampe mène à l'étage inférieur où se trouve d'un côté un jardin potager où l'on pourrait installer le projet de "[jardin idéal](https://www.lalouvesc.fr/projets-avenir/changement-climatique/vers-un-village-resilient/#un-jardin-id%C3%A9al-en-2022)" de l'association “Les Jardins du Haut Vivarais” si elle le souhaite. De l'autre côté, accolé au mur de soutènement, se tient, bien sûr, le jeu-monument qui permettra aussi aux enfants de grimper directement dans le jardin supérieur.

Après un coude, la rampe continue sa descente vers le parc au travers d'un talus végétalisé. Pour des raisons de coûts, l'escalier présent sur le plan n'a pas été retenu.

[Avant-projet complet](https://www.lalouvesc.fr/media/2022-11-14-lalouvesc-apd-ind-1.pdf).

![](/media/beausejour-nov-2022-2.png)

Ce programme a rencontré un accueil très favorable, tant du côté du Conseil municipal que du Comité de développement et encore des architectes, amis du village, qui ont aussi été consultés.

#### Échéance et coûts

L'ensemble de ces aménagements (hors jeu-monument) a été évalué à 235.000 €. Nous avons bon espoir de trouver des subventions pour nous aider à le réaliser. Mais quoi qu'il en soit 20% minimum resteront réglementairement à la charge de la Commune.

Le jeu-monument a été évalué à 120.000 €. Il sera réalisé en deux lots. Un premier lot devrait être subventionné dès cette année par le programme Leader. Nous chercherons aussi des subventions pour le compléter.

Ces investissements peuvent paraître importants par rapport à notre budget annuel, mais ils façonnent durablement l'aspect du village en le rénovant et perdureront sur des dizaines d'années. C'est à cette échelle qu'il faut les mesurer.

Sauf imprévu, cet important chantier doit démarrer au printemps pour se terminer à l'automne 2023. En 2024, le centre bourg aura changé d'aspect...

## Sanctuaire

### Répétitions de chants

Répétitions de chants pour préparer les célébrations de Noël et autres dimanches : les jeudis de 20h à 21h (24 novembre, 1er, 8, 15 et 22 décembre) à la Maison Saint-Régis. Art et convivialité !

**VEILLÉE DE NOËL LE SAMEDI 24 DÉCEMBRE à 23h à LA BASILIQUE**

Michel Barthe-Dejean

## Commerces

### Atouts Val d'Ay

Atouts Val d'Ay est l'association des artisans et des commerçants du secteur du Val d'Ay plus Ardoix et Quintenas. Son bureau est composé de :

* TRACOL Carole Quintenas présidente
* LATRECHE Hervé Quintenas vice-président
* BONNET Véronique Saint Alban d'Ay trésorière
* VINCENDON Charly Satillieu vice-trésorier
* FEASSSON Myriam Saint Romain d'Ay secrétaire
* BONNET Cécile Ardoix vice-secrétaire
* Membres du bureau : LACOSTE Hubert, MONTAGNON Christian, BONNET Jérôme, SEILLIER Jacques, FARIZON Vanessa, CARROT Gisèle.

Cette année l’association compte 70 adhérents référencés dans l’annuaire. Plusieurs actions ont été organisées.

#### La quinzaine commerciale

{{<grid>}}{{<bloc>}}

![](/media/atouts-val-d-ay-2022-2.JPG)

{{</bloc>}}{{<bloc>}}

Chaque commerçant qui le désire peuvent participer à la quinzaine commerciale, au minimum un gagnant par commerçant. La valeur mise en jeu est de 1.300 € sous forme de bons d’achat. La gagnante 2022 a été Mme DEFOUR Agnès pour la somme de 200 €.

{{</bloc>}}{{</grid>}}

#### Le salon

{{<grid>}}{{<bloc>}}

Le salon s’est déroulé à Ardoix le 26 septembre. Les artisans et les commerçants y exposent leurs savoir-faire. L’association remercie leurs clients venus nous rencontrer échanger et participer à notre salon.

{{</bloc>}}{{<bloc>}}

![](/media/atouts-val-d-ay-2022-3.JPG)

{{</bloc>}}{{</grid>}}

#### Soirée cabaret

L’association remercie toutes les personnes qui se sont investies. Cette année la soirée ne s’est pas déroulée en cause de manque de participants.

## Culture - Loisir

### Appel pour les décorations de Noël

La mairie et les habitants se sont réunis pour concocter les prochaines décorations de Noël. On s’apprête un peu partout. Premières mises en place à partir du 10 décembre.

Un appel est lancé pour la confection de sapins en bois. Vous pouvez les déposer devant la Mairie. Coordination Aurélie Desbos (06 32 56 24 04) et François Besset (06 88 49 94 64).

### Assemblée générale de la Lyre louvetonne

![](/media/lyre-louvetonne-17-juillet-2020.jpg)

La Lyre louvetonne a réuni son assemblée générale annuelle vendredi 18 novembre 2022. La majorité des musiciennes et musiciens était présente.

Après un mot de bienvenue, le président Stéphane Roche a rappelé que cette assemblée générale officielle était la première depuis 2019 à cause du covid. Le secrétaire Gérard Borne a présenté un bilan moral très complet et détaillé illustré d'anecdote. Puis Marion Besset a développé le bilan financier avec un résultat positif, malgré le covid.

Le chef de musique Georges Anaudon a fait ses propositions pour l'année qui vient avec pas moins d'une quinzaine de sorties jusqu'en juillet 2023, suivant la disponibilité des musiciens. Le président a remercié tous les gens qui viennent de l'extérieur pour leur implication aux répétitions et aux manifestations diverses.

En fin de réunion François Besset a remercié au nom de municipalité la Lyre pour sa participation aux cérémonies patriotiques et villageoises et a rappelé le soutien des élus à l'association.

**La Lyre a besoin de renforts. Si vous avez envie et aimez la musique d'harmonie, venez rejoindre le groupe les vendredi soir de 20h30 à 21h30.**

### Succès pour les cavalières et labels de qualité pour Fontcouverte

Dimanche 6 novembre, le Domaine Fontcouverte s’est rendu aux écuries O’Hara, à l’Hôpital-le-Grand (42) pour participer à un concours de sauts d’obstacles. Ce fut une journée riche en réussites, avec des parcours 100 % sans faute et 100 % classés pour les trois cavalières qui sortaient ce jour-là : Maé Pappo ; Louise Dumas et Marie-Ange ZAK. Le club remporte notamment les deux premières places de l’épreuve Poney 3 (sur 33 partants) et les deux premières places de l’épreuve Club 3 (21 partants), ainsi que la première place de l’épreuve Club 2 (18 partants).

![](/media/fontcouverte-nov-2022.jpg)

**Bravo à ces cavalières, sans oublier toute l’équipe du Domaine Fontcouverte et le soutien des parents !**

#### Labels de qualité

La Fédération Française d'Équitation propose aux établissements équestres de s'engager dans une démarche qualité. Dans ce cadre, l'établissement a été audité et obtenu les labels suivants :

* Cheval Étape hébergement intérieur
* Cheval Étape hébergement extérieur
* Label d'activité Cheval Club de France
* Label d'activité Poney Club de France
* Bien-être animal

Cette labellisation atteste de la qualité globale des prestations proposées par l'établissement « DOMAINE FONTCOUVERTE ».

**Bravo à toute l'équipe !**

### Le Téléthon au Club des deux clochers

{{<grid>}}{{<bloc>}}

![](/media/telethon-2022.jpg)

{{</bloc>}}{{<bloc>}}

Dans le cadre du Téléthon, le Club des deux clochers sera présent en matinée, en accord avec les pompiers, au village de Lalouvesc, le **samedi 3 décembre 2022**.

Nous vous proposerons divers objets confectionnés par nos membres. La recette de cette vente sera versée intégralement à l’association du Téléthon.

{{</bloc>}}{{</grid>}}

Le même samedi 3 décembre 2022 après-midi à partir de 14 heures, le Club accueillera tous ceux qui désirent participer à cette noble cause. De nombreux jeux de société, parties de cartes, jeu de billard animeront cet après-midi avec une participation de 1 € par joueur et par partie, reversée intégralement au Téléthon.

Comme chaque année, venez nombreux animer cette manifestation nationale. Donnez un peu de votre précieux temps. Les modestes participations demandées cumulées représenteront notre village revendiquant l’adage : _« Ardéchois cœur fidèle »._

### Une belle année à la Cabane des marmots

Des super-héros aux aventuriers, le centre de loisirs adapte son royaume, un royaume où l'on passe de belles vacances et des mercredis enrichissants.

Parce que jouer est le travail des enfants, l'équipe d'animation se fait un plaisir de les accompagner dans leur entrainement quotidien.

Au royaume de Jaloine, tout est bon pour jouer !

{{<grid>}}{{<bloc>}}

![](/media/cabane-des-marmots-dec-2022.jpg)

{{</bloc>}}{{<bloc>}}

Cette année a été riche en émotions avec les thèmes suivants : dans la peau d'un soigneur avec la visite au safari de Peaugres, Harry Potter et l'apprentissage de tours de magie avec un intervenants, Mario Party et sa course folle dans le labyrinthe d'Hauterives, Incroyable talents, Koh Lanta et sa redoutable épreuve au Accrobois ...

Sans oublier les grands jeux qui resteront gravés dans la mémoire des enfants : jeu de l'oie revisité, la chasse aux trésors, la course d'orientation, l'escape game, les olympiades, la journée casion, les kermesses, ...

{{</bloc>}}{{</grid>}}

Le royaume est ouvert tous les mercredis et les vacances scolaires (fermeture 1 semaine à Noël et 3 semaines l'été).

Pour les enfants de 3 à 13 ans, inscription à la carte 1/2 journée ou journée.

Inscription et renseignements : [cdl.jaloine@gmail.com](mailto:cdl.jaloine@gmail.com) ou sur facebook : Jaloine, st Romain d'ay

Sophie Archier, directrice

## Santé - sécurité

### Suite du calendrier Santé-Environnement

L’ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l’habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante). Pour le mois de décembre, la suggestion concerne [l'aération et la ventilation](https://www.auvergne-rhone-alpes.ars.sante.fr/air-interieur-3?parent=4946). Un autre calendrier alerte sur les [bonnes pratiques face à l’environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0). Pour le mois de septembre, la suggestion concerne [les transports](https://www.auvergne-rhone-alpes.ars.sante.fr/transports-et-sante?parent=14635).

{{<grid>}}{{<bloc>}}

![](/media/int-12_aeration.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ext-12_transports.jpg)

{{</bloc>}}{{</grid>}}

## Dans l'actualité, le mois dernier

#### Camping : les réservations sont ouvertes pour 2023

28 novembre 2022 Il est prudent de réserver tôt [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/camping-les-reservations-sont-ouvertes-pour-2023/ "Lire Camping : les réservations sont ouvertes pour 2023")

#### Le procès-verbal du Conseil municipal est en ligne

27 novembre 2022 Séance du 22 novembre 2022 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-proces-verbal-du-conseil-municipal-est-en-ligne/ "Lire Le procès-verbal du Conseil municipal est en ligne")

#### Les nouveaux bacs installés début décembre

25 novembre 2022 Prenez les bons réflexes de tri [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-nouveaux-bacs-installes-debut-decembre/ "Lire Les nouveaux bacs installés début décembre")

#### Prochain café des aidants le 3 décembre

24 novembre 2022 Auberge de St Félicien [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/prochain-cafe-des-aidants-le-3-decembre/ "Lire Prochain café des aidants le 3 décembre")

#### Opération d'abandon d'armes

23 novembre 2022 Enregistrement au SIA du 25 novembre au 2 décembre 2022 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/operation-d-abandon-d-armes/ "Lire Opération d'abandon d'armes")

#### Les fleurs sont à l'abri

17 novembre 2022 Nous les retrouverons l'année prochaine. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-fleurs-sont-a-l-abri/ "Lire Les fleurs sont à l'abri")

#### 11 novembre : le village se souvient

11 novembre 2022 Une cérémonie émouvante réunissant l'ensemble des générations [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/11-novembre-le-village-se-souvient/ "Lire 11 novembre : le village se souvient")

#### Enquête sur les besoins des parents

8 novembre 2022 Activités des enfants de 0 à 17 ans [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/enquete-sur-les-besoins-des-parents/ "Lire Enquête sur les besoins des parents")

#### Calendrier des manifestations 2023

8 novembre 2022 Déjà bien fourni, mais à compléter [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/calendrier-des-manifestations-2023/ "Lire Calendrier des manifestations 2023")

#### Vidéo du Trail des Sapins

4 novembre 2022 Réalisation Matthieu Fraysse et Christelle Lebeau, merci à eux ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/video-du-trail-des-sapins/ "Lire Vidéo du Trail des Sapins")

## Feuilleton : _Le tour du monde de Gulli_

{{<grid>}}{{<bloc>}}

![](/media/gulli-ecole-st-joseph-2022.jpg)

{{</bloc>}}{{<bloc>}}

C''est devenu une tradition : nous retrouvons au début de l'hiver un feuilleton proposé et illustré par les enfants de l'école St Joseph.

Voici donc la première partie de l'histoire créée par les enfants de l'école St Joseph.

Notre thème annuel est le voyage autour du monde et nous nous occupons, en classe, de notre tortue Gulli. Les enfants ont donc décidé de la mettre en personnage principal de notre histoire.

_Sasha Haon, Cheffe d'établissement_

{{</bloc>}}{{</grid>}}

> C'est l'histoire d'une [tortue pelomedusa](https://fr.wikipedia.org/wiki/Pelomedusa_subrufa) qui se sentait bien seule et qui décida de partir faire le tour du monde pour trouver des amis et peut-être même l'amour.
>
> ![](/media/feuilleton-ecole-dec-2022-3.png)
>
> Gulli, c'est son nom, partit de Lalouvesc un matin d'automne, direction l'Alsace. Comme Gulli avait décidé de goûter à tous les plats du monde, il commença par une bonne choucroute avec des saucisses de Strasbourg. Le ventre bien rempli, il partit se coucher.
>
> Le lendemain matin, quand Gulli sortit de l'hôtel, les rues étaient toutes enneigées. Noël approchait déjà. Il prit le Transsibérien pour se rendre en Russie rejoindre sa promise Marina, une [tortue des steppes](https://fr.wikipedia.org/wiki/Tortue_de_Horsfield), et passer un Noël magique. Là-bas, les amoureux visitèrent le Bolchoï, un célèbre théâtre et dansèrent la Polka avec un petit verre de vodka accompagné de blinis et de caviar.
>
> ![](/media/feuilleton-ecole-dec-2022-2.png)
>
> Le temps passe vite, il faut penser à la prochaine étape...

A suivre...

Découvrez Gulli, colorié par les petits de l'école :

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=2421a4bcb765&view=iframe" style="width:100%;height:600px" frameborder="0" allowfullscreen></iframe>