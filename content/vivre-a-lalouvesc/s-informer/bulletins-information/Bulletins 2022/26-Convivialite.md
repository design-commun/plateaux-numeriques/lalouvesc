+++
date = 2022-08-30T22:00:00Z
description = "n°26 Septembre 2022"
header = "/media/entete-automne.jpg"
icon = ""
subtitle = "n°26 Septembre 2022"
title = "26 - Convivialités"
weight = 1

+++
Les convivialités nourrissent la vie d'un village en bonne santé, comment peut-on les traduire sur un site web ou dans des services numériques ? Voilà quelques réponses que nous chercherons en septembre. Dans ce Bulletin, nous ferons aussi un premier retour sur la saison qui s'achève et vous y trouverez encore, comme toujours, bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier ](/media/bulletin-de-lalouvesc-n-26-1er-septembre-2022.pdf)et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du maire

Nous y voilà : la rentrée des classes est là.

Un temps merveilleux, beaucoup de monde sur le village, nous avons eu une bonne saison estivale, une très bonne saison !

Les commerçants débordés ont fait face à leurs obligations pour satisfaire la clientèle.

De notre côté, l’organisation des festivités s’est bien déroulée : une Ardéchoise sans problème malgré la chaleur, un 14 juillet rarement si fréquenté, un 15 août avec près de 2.000 personnes à la messe.

Du côté des associations, on a constaté une bonne fréquentation au Carrefour des Arts, un volume de vente important y a été réalisé. Pour les concerts des Promenades Musicales : beaucoup de monde à la basilique et à la ferme de Poly. La brocante ce premier week-end de septembre, vu le nombre d’exposants, nous laisse présager d’une grosse fréquentation.

Il n’y a pas eu d’incident majeur pendant la saison. Notre vigilance s’est accrue pendant la sécheresse, nous avions craint des incendies. Nous avons été plus rassurés, après les pluies du 15 août.

Un problème de distribution d’eau est venu perturber la vie des habitants ces dernières 24 heures. Le problème est partiellement résolu. Nous continuons les recherches et vérifications nécessaires au bon fonctionnement du réseau. Je remercie le service technique et mes collaborateurs pour leur efficacité et leur rapidité d’exécution.

Un très bon été 2022 donc avec une bonne organisation. La rentrée est là. Je souhaite une bonne rentrée scolaire à nos jeunes et bon courage à tous les travailleurs ayant repris leurs activités professionnelles.

Jacques Burriez, Maire de Lalouvesc

## Actualités de la mairie

### Coupure d'eau

Une coupure d'eau s'est produite dans la nuit du 29 août sur une partie du village. L'eau n'a pu être rétablie pour l'ensemble de la population que le lendemain vers 21h. Voici un retour sur cet incident.

![](/media/coupure-d-eau-1.jpg)

Le maire a été prévenu en début de soirée par la Maison de retraite et quelques administrés que l'eau n'arrivait plus au robinet. Avec le premier adjoint, ils se sont rendus immédiatement au réservoir du mont Chaix et ont constaté que celui-ci était vide, malgré une alimentation amont qui se poursuivait. Dans le courant de la nuit, il n'a pas été possible de repérer d'où provenait l'incident.

Le lendemain matin, une inspection générale des réservoirs a été réalisée. Celui du mont Chaix et du camping étaient vides, tandis que celui de la Croix du Saint Père était plein. La société Faurie alertée s'est déplacée et une analyse globale de la situation a été réalisée. Une vérification des débits amont et de consommation a été réalisée qui a montré une forte discordance. Ainsi une trop forte consommation d'eau entre juillet et août pourrait être une des causes de la panne, mais il ne faut pas exclure non plus la possibilité d'une fuite.

A midi, un camion-citerne de 10.000 litres d'eau a été livré à la maison de retraite à midi pour pallier aux difficultés rencontrées par l'établissement.

Une vanne de communication entre les réservoirs a été réouverte et, le temps que le réservoir aval se remplisse la distribution d'eau a été rétablie. La recherche de fuites se poursuit au moment où nous écrivons ces lignes.

Plusieurs leçons doivent être tirées de cet incident :

* il nous faut apprendre à mieux gérer l'équilibre entre les réservoirs ;
* malgré les alertes, il semble que les consommations d'eau aient été excessives cet été, il faudra donc être plus incitatif ;
* il nous faut investir dans la rénovation des réseaux et ce sera coûteux ;
* il nous faut aussi améliorer notre communication sur les incidents, nous aurons l'occasion d'en discuter aux prochaines rencontres Numérique & Ruralité (voir plus loin).

### Nouvelles plaques de rue, nouveaux numéros

Rappel : l'ensemble des nouvelles adresses ont été déposées dans la base de données nationale qui sert de référence pour toutes les cartes, les systèmes de géolocalisation ou les divers adressages.

[![](/media/adressage-2022.jpg)](https://adresse.data.gouv.fr/base-adresse-nationale/07128#11.88/45.117/4.5215/0/2)

Vous pouvez consulter vous-même la [base de données](https://adresse.data.gouv.fr/base-adresse-nationale/07128#11.88/45.117/4.5215/0/2) et repérer votre adresse.

{{<grid>}}{{<bloc>}}

![](/media/usine-plaque-de-rue-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

Nous avions prévu de changer les plaques de rue au printemps, mais les impressions ont pris plus de temps que nous le souhaitions. Les plaques ainsi que les numéros ont été fabriqués et émaillés par l'usine _Rochetaillée-Email_ à Sarras. Un cycle de trois cuissons à 800 degrés était nécessaire. Chaque plaque a été sérigraphiée à la main, une à une. Chaque fois que la plaque est composée de plusieurs couleurs, le processus est répété pour chaque couleur.

{{</bloc>}}{{</grid>}}

Ce ne sont pas moins de 40 panneaux, 37 plaques et 414 numéros qui seront posés à partir de la mi-septembre par les employés municipaux. Le village aura ainsi pour la fin de l'année une signalétique complète entièrement rénovée.

Tous les logements recevront un courrier, précisant la nouvelle adresse, ainsi que toutes les instructions sur les démarches à effectuer le cas échéant pour votre changement d'adresse.

**Découvrez les nouvelles plaques de rue en réunissant les morceaux du puzzle en fin de bulletin...**

### Recensement début 2023

Le recensement du village est prévu début 2023. Il a été retardé par l'Insee d'une année à cause du Covid. Le recensement consiste à compter précisément et à caractériser toutes les personnes résidant dans le village. Toutes les résidences secondaires sont aussi répertoriées.

Ces données alimentent les statistiques officielles qui donnent une [photographie chiffrée du village](https://www.insee.fr/fr/statistiques/2011101?geo=COM-07128). Réalisé normalement tous les cinq ans, le recensement sert de base aux calculs de l'Insee, les chiffres des années intermédiaires sont des extrapolations de l'année de base en fonction des décès, des naissances, des arrivées constatées et des transactions immobilières.

![](/media/population-commune-de-lalouvesc-insee-2022.png)

Rappelons que les données individuelles recueillies par l'Insee ne sont communiquées à aucun autre organisme, ni aux services fiscaux, ni à la police, ni à aucune institution publique ou privée.

Mais le recensement n'est pas pour autant sans conséquence pour la Commune. Les dotations de l’État sont calculées proportionnellement au nombre d'habitants, auquel il faut ajouter le nombre de résidences secondaires, chiffres globaux et anonymisés transmis par l'Insee sur la base du recensement. La qualité du recensement conditionne donc les ressources principales de la Commune pour cinq années.

**Il faudra réaliser cette opération avec le plus grand soin et nous aurons pour cela besoin de la coopération de tous les habitants.** Plus de détails dans un prochain numéro de ce Bulletin.

### L'étude sur le Cénacle se poursuit

![](/media/2022-08-21-le-dauphine-libere-etude-cenacle-de-lalouvesc-1.jpeg)

Le _Dauphiné Libéré_ a publié une page entière sur l'étude menée sur le Cénacle. Les lecteurs de ce Bulletin sont déjà au courant (voir le [Bulletin n°24](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/24.amenager-pour-accueillir/#bient%C3%B4t-des-sc%C3%A9narios-pour-le-c%C3%A9nacle-et-sainte-monique)). Une prochaine réunion se tiendra à la mairie le 20 septembre où les premiers scénarios seront présentés par les bureaux d'études. Rendez-vous donc dans le numéro d'octobre.

### Travaux pour la mise en place de la fibre

Dans le cadre d'études préliminaires pour le déploiement de la fibre optique (relevés d'infrastructures aériennes et souterraine, hydro-aiguillage) et pose de fibre optique des travaux pour le déploiement de la fibre optique des travaux auront lieu sur la Commune au dernier trimestre de l'année 2022.

La société AXIONE-BOUYGUES ENERGIES SERVICES assurera la mise en sécurité des équipes et des tiers grâce à un balisage adapté et limitera au maximum la gêne occasionnée pour les riverains.

## Zoom

### Les usages de l'information locale

Une étude, subventionnée par la préfecture, a été menée par Lydia Khodja de la société Comly pour mieux connaître les usages du numérique et plus largement de l'information locale sur le village.

A partir d'entretiens individuels, une série de personnages-types représentatifs des usagers de l'information locale a été construite. Dans le jargon des designers, on les appelle _des personas_ (consulter la [présentation des personas](/media/dossier-personas_-v2.pdf)).

![](/media/personas-categories-2022.jpg)

Sur cette base, nous tirerons des leçons pour mieux qualifier et améliorer l'information dans et sur le village. En voici quelques-unes parmi les principales :

* le site _lalouvesc.fr_ est bien apprécié et utilisé, néanmoins les principaux vecteurs de l'information dans le village restent le bouche-à-oreille et les affiches posées chez les commerçants ;
* la plupart des villageois disposent d'un smartphone, mais les plus âgés ne s'en servent que pour téléphoner et aussi recevoir des photos de leur famille ;
* les jeunes ados sont peu concernés par les informations sur le village et préfèrent échanger directement entre eux via les réseaux sur les fêtes et les occasions de rencontres sur un territoire plus large ;
* l'attractivité du village est principalement exercée par sa situation, la nature, les balades et randonnées, viennent ensuite les activités puis le sanctuaire.

Il reste à transformer ces constats en actions positives.

### Des Rencontres pour un numérique convivial en milieu rural

Le site [_lalouvesc.fr_](https://www.lalouvesc.fr/) a servi et sert encore de démonstrateur pour l'équipe des designers _Plateaux numériques_. _Plateaux numérique_ se présente comme _une petite équipe \[qui\] s’attèle aujourd’hui à accompagner les mairies rurales (moins de 3500 habitants) dans la création de sites web vertueux_. Leur action se déploie actuellement à Rocamadour et dans le Béarn.

Gauthier Rousshile, un de ses animateurs, vient de publier [un article](https://gauthierroussilhe.com/articles/plateaux-numeriques) présentant l'originalité de leur démarche. Extraits :

> * on ne déploie jamais un site sans aller sur place ;
> * on interroge autant sur ce qu’il faudrait numériser que ce qu’il ne faudrait pas numériser dans une commune ;
> * nous essayons que le déploiement soit fait par un acteur local afin de garder les compétences et de l’emploi au sein du territoire ;
> * nous documentons l’ensemble de nos outils et de nos méthodes afin que notre travail soit appropriable par la communauté ;
> * nous publions tout ce que nous faisons sous licence libre ;
> * nous essayons de favoriser d’autres initiatives numériques vertueuses ;
> * nous essayons de répondre à la plupart des bonnes pratiques numériques et des demandes de conformité d’un coup et avec un effort minimum.
>
> (...)
>
> Au-delà de Facebook, Uber et consorts, Plateaux numériques me montre qu’il y a d’autres façons de créer des services numériques, sans management à outrance, sans industrialisation, sans surveillance ou captation de données, sans marketing, sans envahissement ; juste guidé par l’envie de faire le juste nécessaire là où il y en a besoin.

![](/media/plateaux-numeriques-lalouvesc-210116.jpg "Visite des designers de Plateaux numériques à Lalouvesc en février 2021")

Forts de ces premières expériences, ensembles, avec l'appui de la préfecture de l'Ardèche, nous avons décidé d'organiser à Lalouvesc des _Rencontres Numérique & Ruralité_.

{{<grid>}}{{<bloc>}}

Le monde rural a reçu le numérique comme il a reçu par le passé les autres injonctions de la modernité : sommé de s’adapter aux nouveaux services qui, comme un dogme, devraient contribuer au développement du territoire en lui facilitant l’accès à distance aux services publics, en accompagnant l’application des réglementations administratives, en rendant possible le télétravail, etc. Pourtant il ressort de l’observation des pratiques numériques dans le monde rural des attentes sensiblement différentes.

Designers, professionnels du numérique, élus ruraux, institutionnels et tous les intéressés pourront échanger sur les questions, les problèmes et les solutions particulières aux villages sans chercher à reproduire les pratiques du numérique héritées de situation et besoins urbains.

{{</bloc>}}{{<bloc>}}

![](/media/affiche-rencontres-numerique-et-ruralite-2022-3.jpg)

{{</bloc>}}{{</grid>}}

Les rencontres se tiendront les 3 et 4 octobre au CAC. L'entrée y est libre sur inscription. [**Présentation détaillée, programme et inscriptions**](http://rencontres.plateaux-numeriques.fr/).

## Culture - loisir

### Affluence au cinéma

Une excellente saison pour le cinéma _Le Foyer_ à l'Abri du pèlerin avec pas moins de 2.500 entrées cumulées sur les deux mois, juillet et août. La dernière séance du lundi 29 août a encore réuni 96 spectateurs.

**Pour fêter cette réussite, les responsables vous proposent une séance surprise supplémentaire à un prix réduit de 4 €, samedi 3 septembre à 21h, veille de la brocante**. Les spectateurs de lundi ont voté pour choisir le titre qui sera présenté. Ce sera donc _The Duke_, une comédie en VF.

Cette séance sera suivie par un échange avec les spectateurs autour du verre de l'amitié.

<iframe width="560" height="315" src="https://www.youtube.com/embed/jEaoH4AHbDM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Démontage de l'exposition du Carrefour des Arts

L'exposition du Carrefour des Arts a aussi fait le plein avec plus de 7.000 visiteurs, dont près de la moitié ont fait le déplacement pour la visite, et un nombre record de ventes a été réalisé.

Les nombreux commentaires recueillis sont très élogieux et les artistes exposés sont ravis.

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-demontage-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-demontage-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-demontage-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-demontage-4.jpg)

{{</bloc>}}{{</grid>}}

Les responsables de l'association se sont retrouvés pour une soirée festive à La Vie Tara en l'honneur de Didier Chieze, président fondateur des _Promenades Musicales_ et de Pierre Roger, malheureusement absent. Les _Voleurs de swings,_ un des tous premiers groupes à se produire aux _Promenades musicales_, ont enchanté les participants.

![](/media/carrefour-voleurs-de-swing-2022.jpg)

Et l'exposition en plein air des étonnantes photographies de Serge Rousse se poursuit jusqu'au 7 novembre, égayant l'espace Beauséjour au centre du village.

![](/media/carrefour-des-arts-s-rousse-2022-1.jpeg)

### Parentibulle : rencontres parents, jeunes enfants

Après un repos estival, le Parentibulle reprend ses accueils le vendredi 9 septembre à partir de 9h30 Espace Jaloine à St Romain d'Ay.

Cet espace d'accueil est destiné aux parents de jeunes enfants. Au programme, des jeux, jouets, espaces de motricité, échanges entre parents, livres...

Chaque parent peut oser pousser la porte, c'est un lieu ouvert à tous, libre, gratuit.

![](/media/parentibulle-rentree-2022.jpg)

Compléments d'infos sur la page Facebook  [https://www.facebook.com/Parentibulle07/](https://www.facebook.com/Parentibulle07/ "https://www.facebook.com/Parentibulle07/")

### Bientôt la fête du livre

Le livre sera en fête le 18 septembre de 10h à 18h dans la salle de la mairie.
![](/media/fete-du-livre-2022.jpg)

### Voitures de collection

{{<grid>}}{{<bloc>}}

35 voitures de collection traverseront le village le 30 septembre vers 12h30 dans le cadre d'une randonnée touristique, le _Classic Forez_.

{{</bloc>}}{{<bloc>}}

![](/media/classic-forez-2022.jpg)

{{</bloc>}}{{</grid>}}

## Sanctuaire

### Un 15 août sous le soleil

Comme chaque année, de nombreux fidèles sont venus assister à la célébration du 15 août au parc des Pèlerins.

{{<grid>}}{{<bloc>}}

![](/media/15-aout-2022-photo-ot-1.jpg "Photo OT")

{{</bloc>}}{{<bloc>}}

![](/media/15-aout-2022-photo-ot-2.jpg "Photo OT")

{{</bloc>}}{{</grid>}}

Et l'après-midi, les terrasses étaient pleines.

![](/media/le-vivarais-15-aout-2022.jpg)

![](/media/cafe-du-lac-15-aout-2022.jpg)

![](/media/commerces-15-aout-2022.jpg)

### Fête de Sainte Thérèse Couderc

A l'occasion de la mémoire de Sainte Thérèse Couderc fêtée ce dimanche 28 août, les Sœurs du Cénacle ont ouvert au public - à titre exceptionnel - leur petit musée aménagé rue de la Fontaine.

![](/media/ste-therese-couderc-2.jpg)A travers documents et objets du quotidien il permet de retracer l'histoire de cette maison du Cénacle fondée il n'y a pas loin de 200 ans pour l'accueil des pèlerines. Il met en lumière la personnalité de Thérèse Couderc à l'initiative de cette œuvre avec le Père Étienne Terme. Ce musée est destiné à la formation des jeunes Sœurs qui viennent de divers pays du monde.

P. Michel Barthe-Déjean

## Fermetures temporaires de commerces

Le **salon de coiffure** sera fermé du mercredi 14 au samedi 17 septembre 2022. réouverture mercredi 21 septembre 9h.

L'épicerie **Vival** sera fermée tous les lundis de septembre à juin. Le lundi les journaux seront disponibles au Café du Lac.

## Santé - sécurité

### Un nouveau kiné à Lalouvesc

{{<grid>}}{{<bloc>}}

![](/media/e-allombert-kine-2022.jpg)

{{</bloc>}}{{<bloc>}}

> Kiné depuis 22 ans et exerçant dans la région lyonnaise, je viens à Lalouvesc depuis 20 ans, ma femme venait enfant avec des religieuses. Nous sommes venus chaque année jusqu'à ce qu'on achète un petit appartement à Lalouvesc et on s'est finalement dit pourquoi ne pas rester? Nous avons donc opté pour ce meilleur cadre de vie au milieu de la forêt ! **J'exerce d'ores et déjà à Lalouvesc et environs, à domicile et dans mon cabinet Chemin de l'école, à côté de l'infirmier. Vous pouvez me joindre au 06 22 19 95 15.**
>
> Eric Allombert

{{</bloc>}}{{</grid>}}

### Destruction de nids de frelons

{{<grid>}}{{<bloc>}}

![](/media/nids-de-frelons-2022.jpg)

Un professionnel récemment homologué pour la destruction de nids de frelons à St-Alban-d’Ay. 06 74 90 66 98.

{{</bloc>}}{{<bloc>}}

![](/media/frelon-asiatique-2022.jpg)

Une association : 07 85 93 06 37, ardeche@lefrelon.com, lefrelon.com.

{{</bloc>}}{{</grid>}}

### Suite du calendrier Santé-Environnement

L’ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l’habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante). Pour le mois de septembre, la suggestion concerne[ les légionelles](https://www.auvergne-rhone-alpes.ars.sante.fr/legionelles-et-legionellose). Un autre calendrier alerte sur les [bonnes pratiques face à l’environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0). Pour le mois de septembre, la suggestion concerne[ les tiques](https://www.auvergne-rhone-alpes.ars.sante.fr/la-tique?parent=5374).

{{<grid>}}{{<bloc>}}

![](/media/int-09_legionelles.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ext-09_tiques.jpg)

{{</bloc>}}{{</grid>}}

## Dans l'actualité, le mois dernier

#### Évolution de la sécheresse

23 août 2022

Passage du niveau crise à alerte renforcée [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/evolution-de-la-secheresse/ "Lire Evolution de la sécheresse")

#### Circulation et stationnement réglementés pour la brocante

23 août 2022

Organisation du 4 septembre [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/circulation-et-stationnement-reglementes-pour-la-brocante/ "Lire Circulation et stationnement réglementés pour la brocante")

#### La brocante affiche déjà complet !

17 août 2022

Si vous vouliez réserver un stand... c'est trop tard. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-brocante-affiche-deja-complet/ "Lire La brocante affiche déjà complet !")

#### Interdiction temporaire de pécher la truite fario

17 août 2022

Arrêté préfectoral [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/interdiction-temporaire-de-pecher-la-truite-fario/ "Lire Interdiction temporaire de pécher la truite fario")

#### Lalouvesc à l'honneur dans l'Hebdo de l'Ardèche

11 août 2022

Une page complète dans le numéro de cette semaine [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lalouvesc-a-l-honneur-dans-l-hebdo-de-l-ardeche/ "Lire Lalouvesc à l'honneur dans l'Hebdo de l'Ardèche")

#### Feux de forêts : le plan A.L.A.R.M.E. déclenché en Ardèche

10 août 2022

Soyez TRES prudents ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/feux-de-forets-le-plan-a.l.a.r.m.e.declenche-en-ardeche/ "Lire Feux de forêts : le plan A.L.A.R.M.E. déclenché en Ardèche")

#### Racontez - nous votre Doux !

3 août 2022

Le Syndicat mixte du bassin versant du Doux recueille les témoignages [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/racontez-nous-votre-doux/ "Lire Racontez - nous votre Doux !")

#### Le Comité des fêtes prépare la rentrée...

3 août 2022

Une nouvelle affiche pour la brocante, une nouvelle vidéo pour le trail [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-comite-des-fetes-prepare-la-rentree/ "Lire Le Comité des fêtes prépare la rentrée...")

### Quatre mariages cet été

Quatre mariages ont été célébrés cet été à la mairie de Lalouvesc :

* Kelly Maia de Carvalho et Dany Antunes, le 23 juillet ;
* Elodie Hoornaert et Guillaume Clot, le 13 août ;
* Elodie Valentin et Alain Bontoux, le 20 août ;
* Sophie Dalverny et Thierry Fanget, le 20 août.

Nos félicitations aux heureux époux. Quelques photos.

![](/media/d-antunes-k-de-carvalho-mariage-2022.jpg)

{{<grid>}}{{<bloc>}}

![](/media/a-bontoux-e-valantin-mariage-2022.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/t-fanget-s-dalverny-mariage-2022.jpg)

{{</bloc>}}{{</grid>}}

## Puzzle : les nouvelles rues louvetonnes

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=305ce3faa9a3&view=iframe" style="width:100%;height:600px" frameborder="0" allowfullscreen></iframe>