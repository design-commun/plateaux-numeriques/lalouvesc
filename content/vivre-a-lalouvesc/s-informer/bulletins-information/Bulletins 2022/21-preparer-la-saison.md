+++
date = 2022-03-30T22:00:00Z
description = "n°21 Avril 2022"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°21 Avril 2022"
title = "21 - Au printemps, on sème..."
weight = 1

+++
Le printemps est là, il est temps de préparer la saison estivale. Le Carrefour des arts prend un nouvel élan, le Comité des fêtes fait son plein de jus, la Vie Tara et l'Abri du pèlerin se rénovent, les journées citoyennes préparent les parcours de balade et pourquoi pas prendre un bain de forêt ? On vous raconte tout cela ... et, comme toujours, bien d'autres choses encore.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-21-1er-avril-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Mot du maire

Mes chers amis,

Le temps a laissé un moment son manteau de vent et de pluie, il s'est vêtu de soleil clair et beau, mais attention l'hiver n'est pas fini, quelques mauvais jours sont encore à prévoir...

Nous voilà début avril et nous avançons à grands pas vers la saison estivale. Notre équipe communale est constituée pour cet été, nous préparons l’ouverture du camping où une partie des terrasses ont été refaites, déformées au fil des années.

Les travaux d’aménagement du lotissement vont bon train, les différents dossiers en cours sont bien appréhendés et maîtrisés.

Nous aurons besoin du concours de tous pour assurer une bonne saison à nos vacanciers. Je compte sur vous pour les accueillir dans les meilleures conditions possibles.

Nous allons apporter un soin tout particulier à l’état et à la présentation du village, entretien et fleurissement, à l'occasion je tiens à remercier mes employés municipaux ainsi que les membres du comité fleurissement pour la qualité de leur travail et leur assiduité.

Je remercie tous les bénévoles qui œuvrent pour le village, aussi bien pendant les journées citoyennes, que dans des actions ponctuelles individuelles.

Tout ceci émane du travail collectif de nos forces vives, d'une collectivité ou tout le monde apporte une pierre à l'édifice : les habitants, les commerçants, les associations, notre communauté religieuse, l'ensemble des parties prenantes.

L'équipe municipale ne peut qu'être fière de voir une unité mobilisée pour le plaisir de vivre dans un village paisible où règne le bien commun.

Amicalement,

Jacques Burriez, Maire de Lalouvesc

## Actualités de la mairie

### En avril, on vote !

Premier tour des élections présidentielles le 10 avril, second tour le 24 avril. Quel que soit votre choix, **faites votre devoir de citoyens**, c'est important, essentiel même pour défendre la démocratie, qui reste fragile et est attaquée, en ces temps troublés en Europe.

### Ecolotissement

Le chantier de viabilisation des lots avance bien. La preuve avec une photo.

![](/media/chantier-ecolotissement-mars-2022-2.jpg)

### Camping

Une partie de la terre du terrain de l'écolotissement a été utilisée sur les terrasses du camping qui avaient bien besoin de s’aplanir.

![](/media/plateforme-camping-mars-2022-2.jpg)

Côté réservations, la saison semble prometteuse, favorisée notamment par la réouverture de l'Ardéchoise, puisque nous avons engrangé déjà le double de réservations par rapport à la même date l'année dernière. Si vous souhaitez passer au [camping](https://www.lalouvesc.fr/decouvrir-bouger/hebergement-restauration/camping/) de Lalouvesc cet été, il est prudent de ne pas tarder à réserver.

### Nouvelle secrétaire

Bienvenue à Valérie Perez qui a rejoint l'équipe du secrétariat pour un poste à temps partiel (20%) ! Elle occupe, par ailleurs, un poste de secrétaire à la mairie de St Alban d'Ay à temps partiel (80%). Elle complète ainsi à Lalouvesc son poste à plein temps.

### Beauséjour et jeu monument : présentation publique

Retenez la date : **le 22 avril les lauréats du concours d'idées sur le jeu-monument présenteront leur projet dans une séance publique**.

Vous êtes tous conviés. Ce sera aussi l'occasion de faire le point sur les réflexions du comité technique concernant l'aménagement de l'espace Beauséjour et du Val d'Or et, plus généralement, des initiatives prises par la municipalité dans l'urbanisme.

### Fibre optique

Le bâtiment qui doit permettre la répartition des connexions de la fibre est achevé.

![](/media/local-fibre-optique-2022.jpg)

Le chantier avance, mais, ne rêvons pas, Lalouvesc n'aura pas la fibre avant plusieurs années.

## Zoom

### Une nouvelle ambition pour le Carrefour des arts

Le Carrefour des arts, fort de ses succès passés, élargit ses activités en approfondissant sa vocation à faire se croiser les disciplines artistiques et à présenter leurs réalisations au plus grand nombre. Cette ambition renouvelée a été présentée à la dernière assemblée générale du 19 mars 2022.

Les statuts ont été modifiés pour reprendre les activités des Promenades Musicales de Lalouvesc et du Val d'Ay. Vous pourrez ainsi assister à trois concerts de musique classique les 2, 3 et 4 août, et deux concerts jazz les 12 et 13 août.

Une exposition à ciel ouvert sera réservée à un photographe. Cette année vous pourrez découvrir les œuvres de Serge Rousse exposées sur l'espace laissé libre par la démolition de l'hôtel Beauséjour de mi-mai à fin octobre 2022. Ainsi la photographie retrouvera et élargira sa place, réduite à cause des mesures sanitaires imposées ces deux dernières années.

Le Carrefour des Arts poursuit évidemment son activité principale qui est de faire découvrir des artistes plasticiens et d'exposer leurs œuvres pendant la période estivale. Cette année nous exposerons au _Péristyle_ les œuvres de Judith Chancrin (peinture), au _Théâtre_ Isabelle Tapie (sculpture et peinture) et François Lelièvre (sculpture), au _Grand Salon_ Cécile Windeck (peinture), au _Petit Salon_ Erica Stéfani et Malika Ameur (mosaïques), et au _Grenier des peintres,_ Françoise Papail (peinture). Nous rallongeons d'une semaine l'ouverture au public qui aura lieu cette année du 2 juillet au 28 août.

![](/media/francoise-papail-1-carrefour-des-arts-2022.JPG "Françoise Papail")

{{<grid>}}{{<bloc>}}

![](/media/judith-chancrinl-1-carrefour-des-arts-2022.jpg "Judith Chancrin")

{{</bloc>}}{{<bloc>}}

![](/media/malika-ameurl-1-carrefour-des-arts-2022.jpg "Malika Ameur")

{{</bloc>}}{{</grid>}}

![](/media/s-roussel-1-carrefour-des-arts-2022.JPG "Serge Rousse")

Vous avez peut-être assisté aux visites de musées et d'ateliers d'artistes que nous organisons. Ces visites sont ouvertes à tous nos adhérents. Les prochaines visites sont prévues le 14 mai au Musée d'Art Contemporain de Lyon et le 11 juin à la Biennale du design de Saint-Étienne. N'hésitez pas à nous contacter.

Pour participer à ce bel élan, venez-vous investir dans la vie et les activités de l'association, en intégrant une des cinq commissions de travail que nous mettons en place : Arts Plastiques, Musique, Technique, Administration et finances et Communication.

Michèle S

### Le Comité des fêtes toujours au jus

La mission première du Comité des Fêtes est d’animer notre village, mais nous devons bien avouer quelques activités parallèles… tout à fait légales pour ne pas dire honorables ! 😀

Dans toutes nos démarches nous essayons de mettre en valeur les commerçants, les artisans, les acteurs économiques de notre région. Aujourd’hui nous tournons notre regard vers Pailharès.

L’histoire débute même à Arlebosc. Nicolas Gamon, producteur de fruits, nous a offert une belle opportunité d’achat de 650kg de pommes. Merci à lui. Il a eu la gentillesse de livrer cette remorque remplie de jolis fruits directement à Pailharès dans les ateliers de **Nectardéchois**. Ainsi en quelques heures nos pommes se sont transformées en 350 litres de jus.

{{<grid>}}{{<bloc>}}

![](/media/nectardechois-2022-8.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/nectardechois-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/nectardechois-2022-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/nectardechois-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/nectardechois-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/nectardechois-2022-2.jpg)

{{</bloc>}}{{</grid>}}

#### Une coopérative locale

Nectardéchois est, en effet, un atelier de transformation de fruit dont Pénélope, notre interlocutrice, nous explique le parcours :

> L’idée de la création d’un atelier de jus de fruits à Pailharès est née en 1991, sous l’impulsion de la municipalité. Il s’agissait de dynamiser l’économie et de valoriser les ressources locales, dans une commune rurale isolée.
>
> Pari gagné ! En 1998, « Nectardéchois » ouvrait ses portes ; sa capacité de production était de 60 000 litres. En 2001, l’installation d’une nouvelle chaîne d’embouteillage a fait passer cette capacité à 150 000 litres.
>
> En 2004, une nouvelle phase de modernisation de l’outil de production a permis de diversifier les prestations et d’augmenter encore la capacité de production (± 200 000 bouteilles)…
>
> En Assemblée générale extraordinaire du 15 octobre 2010, après une période de réflexion et de travail interne, la structure associative Nectardéchois se transforme en Société Coopérative d’Intérêt Collectif. Ce nouveau statut est une ouverture à l’implication des collectivités territoriales, et permettra un développement plus large.
>
> Nectardéchois, c’est aujourd’hui un ensemble de quatre collèges : les usagers, les salariés, les collectivités territoriales et les prestataires qui gèrent la nouvelle structure.
>
> La gérance est accompagnée dans sa tâche par les conseils et propositions du Comité d’Orientation (composé de sociétaires issus de chaque collège), qui se réunit mensuellement." N’hésitez pas à consulter le site : [http://www.nectardechois.fr/](http://www.nectardechois.fr/ "http://www.nectardechois.fr/")

![](/media/nectardechois-2022-7.jpg)

Sur les buvettes que nous organisons lors de toutes nos manifestations, nous privilégierons à présent l’offre d’un produit sain et local. À n’en pas douter que les clients, petits et grands, apprécieront.

Si nous tenons aujourd’hui à donner ce coup de chapeau et faire part de ce coup de cœur que nous avons pour cet atelier de transformation, c’est que nous savons pouvoir compter sur leur fidélité pour nous soutenir en terme de partenariat. Les gagnants des trois paniers gourmands/découverte proposés par le biais de la tombola lors de **La Ronde Louvetonne** trouveront, à l’intérieur de chacun d’eux, le signe de leur attachement à cette manifestation tout comme les participants au **Trail des Sapins** retrouvent toutes les années leurs produits sur les ravitaillements.

Ouvrons grand les yeux et apprécions ces valeurs sûres que nous avons la chance de posséder à quelques encablures de notre village. Vous retrouverez les produits Nectardéchois au Pavé de Saint Régis et au Vival de Lalouvesc.

Une autre petite information ? Nous avons enregistré la participation de **130** voitures pour la Ronde Louvetonne ! Record de 2018 battu, puisque nous avions accueilli 126 voitures. **Rendez-vous le dimanche 3 avril pour admirer tous ces bijoux !**

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

### Vous prendrez bien un bain de forêt !

Brigitte Blancart-Villemagne propose pour la saison prochaine une nouvelle activité sur Lalouvesc : le « bain de forêt ». Elle nous explique de quoi il s'agit.

{{<grid>}}{{<bloc>}}

![](/media/bain-de-foret-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

> La pratique du « bain de forêt » s'inspire  du _SHINRIN YOKU_ japonais. Il vise à améliorer le bien-être et prévenir la maladie, il peut aussi représenter un simple moment de détente au milieu des arbres, sans autre attente que le simple plaisir... ce qui est déjà beaucoup.
>
> Le bain de forêt aborde la forêt dans la lenteur et le silence, comme si c'était la première fois,  en captant les informations environnantes par nos sens. Il permet d’installer notre corps et notre esprit dans un état de calme et de détente dans lequel nous sommes pleinement conscient(e)s de l’endroit où nous nous trouvons et de ce que nous y vivons au moment présent. Il vise une prise de recul, un lâcher prise et une ouverture à une meilleure connaissance de soi.

{{</bloc>}}{{</grid>}}

> Il s'agit de se détacher des attentes, d'apprendre à recevoir, de se laisser surprendre, d'accueillir les cadeaux offerts : sons, paysages, énergies. Et, plus fondamentalement par la connexion avec les arbres, les bains de forêt impliquent une conscience du réseau qui relie tous les êtres vivants.
>
> Concrètement, cela comprend notamment des temps de connexion aux arbres par la rencontre directe ; différentes formes de marches (en aveugle, pieds nus, afghane...) ; des temps de création (Land Art, écriture, sons...) ou de méditation... sans oublier, pour conclure, la cérémonie du thé.

![](/media/bain-de-foret-2022-2.jpg)
Renseignements & Inscriptions

* Tél : 06 70 15 32 04
* Courriel : pardeversoi07gmail.com

## Culture - loisirs

### Carnaval et plongeons à l'école St Joseph

Vendredi 25 mars, les enfants de l'école St Joseph ont fêté le carnaval en défilant déguisés dans tout le village. La promenade s'est terminée devant la mairie avec une bonne crêpe.

{{<grid>}}{{<bloc>}}

![](/media/carnaval-ecole-2022-4.jpg)

![](/media/carnaval-ecole-2022-5.jpg)

![](/media/carnaval-ecole-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carnaval-ecole-2022-6.jpg)

![](/media/carnaval-ecole-2022-2.jpg)

![](/media/carnaval-ecole-2022-1.jpg)

{{</bloc>}}{{</grid>}}

Et les sorties piscine ont démarré.

{{<grid>}}{{<bloc>}}

![](/media/piscine-ecole-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/piscine-ecole-2022-3.jpg)

{{</bloc>}}{{</grid>}}

### Centre de loisirs de Jaloine

Et c'est reparti, les vacances approchent !!! A Jaloine, au centre de loisirs, nous sommes impatients de vous retrouver.

Pour ces vacances, les super-héros seront à l'honneur et ils devront participer à de nombreux challenges, sans oublier les activités manuelles et culinaires préparées par l'équipe d'animation...

[Fiche d'inscription](/media/fiche-inscriptions-vacances-printemps-2022-jaloine.pdf).

![](/media/jaloine-printemps-2022-1.jpg)

![](/media/jaloine-printemps-2022-2.jpg)

![](/media/jaloine-printemps-2022-3.jpg)

### Les coulisses du Théâtre de la veillée

Frédéric Brunel nous signale qu'il n'y aura pas de pièce jouée cette année. Mais la troupe planche sur une pièce originale, imaginée par ses membres qui sera proposée l'année prochaine.

## Rénovations

### Du neuf à l'Abri du pèlerin

![](/media/abri-du-pelerin-neige.JPG)

{{<grid>}}{{<bloc>}}

![](/media/abri-du-pelerin-travaux-2022-2.jpg)

Grâce à l’aide d’Alain Chatagnon, François Besset et Vincent Trébuchet, nous avons pu chercher 70 lits d’occasion offerts gratuitement par l’établissement scolaire jésuite Saint Joseph d’Avignon (fermeture d’une partie de l’internat). Avec l’aide de Kofi, nous avons démonté les anciens lits d’hôpitaux et nous les avons déposés dans le champ à l’arrière de l’Abri. Bientôt un ferrailleur viendra les ramasser. Nous profiterons aussi de l’occasion pour rénover quelques pièces : des petites réparations et un rafraichissement des peintures !

{{</bloc>}}{{<bloc>}}

![](/media/abri-du-pelerin-travaux-2022-3.jpg)

#### Opération « nettoyage des lits »

> **Le 8 avril de 10h00 à 16h00**
>
> Nettoyage et montage des nouveaux lits. **Vous êtes les bienvenus pour nous donner un coup de main !**
>
> Pour ceux qui le peuvent nous partagerons le repas de midi ensemble !

{{</bloc>}}{{</grid>}}

Tout cela nous permettra d’améliorer la qualité de l’accueil dans les chambres de l’Abri du pèlerin qui ressembleront un peu moins à des chambres d’hôpital !

Le réfectoire aussi à fait peau neuve : installation d’une cuisine Ikéa, habillage de l’insert, nouvelle peinture, raccordement d’un lave-linge d’occasion…

![](/media/abri-du-pelerin-travaux-2022-1.jpg)

Nicole Kponoume et Christine M. ont accepté de prendre en charge la gestion des bénévoles. Si vous pouvez donner un peu de temps n’hésitez pas à vous adresser à elles.

Le 7 avril passera la Commission de sécurité qui, nous l’espérons, nous autorisera à poursuivre les activités d’accueil durant l’été.

Nous nous sommes aussi proposés, si besoin, pour l’accueil de réfugiés ukrainiens durant les mois chauds.

Yves Stoesel

### La Vie Tara en construction

Nous pensions être presque prêts à recevoir des groupes pour la nouvelle ouverture de “La vie Tara" cette saison 2022. Nous avons été un peu choqués lorsque nous avons découvert récemment que nous devions encore procéder à quelques ajustements dans le bâtiment. Compte tenu de la politique plus stricte en matière d'accueil et d'hébergement de groupes pour les règles de sécurité et de prévention des incendies, nous ne pourrons obtenir la permission de recevoir des groupes importants à passer la nuit sur place seulement après la réalisation des travaux requis.

Notre planning est encore flou pour l’instant. Mais nous espérons pouvoir organiser quelques événements durant les mois d’été. Pour cela, nous avons besoin d’aide de bénévoles et des artisans de la région pour avancer plus rapidement. Nous avons déjà de l’aide mais elle n'est pas suffisante, car il y a beaucoup plus à faire que prévu.

Il y a peut-être des gens qui voudraient nous aider, et nous espérons apprendre à mieux vous connaître et à mieux connaître la région.

##### On recherche...

![](/media/la-vie-tara-2022-2.jpeg)

Nous avons besoin d'aide pour la coupe de bois, le jardinage, le ponçage et l'huilage des meubles de jardin, la peinture, etc.

Nous recherchons également des personnes capables d'effectuer des travaux spécialisés tels que des plombiers, des électriciens et des peintres et spécialistes en solutions d’énergies renouvelables.

![](/media/la-vie-tara-2022-1.JPG)

Toute aide est la bienvenue, le partage de ce message est également très apprécié. Merci d’avance pour votre soutien. La vie Tara en a besoin et nous aussi.

N'hésitez pas à nous contacter si vous êtes intéressés ou si vous voulez en savoir plus : Vous pouvez nous contacter par email à : [management@lavietara.eu](mailto:management@lavietara.eu), par téléphone au : 06 66 99 28 37ou tout simplement passez nous voir !

Romain, Ine et l'équipe de La Vie Tara

### Appel à projet et étude pour le couvent du Cénacle

Les sœurs de Notre Dame du Cénacle ont confié à la société Karism Conseil la rédaction et le suivi d'un [appel à projet](https://www.lesprojetsdesaintjoseph.fr/cenacle-de-lalouvesc/) pour donner une nouvelle vocation au couvent de Lalouvesc.

{{<grid>}}{{<bloc>}}

![](/media/le-cenacle.png)

{{</bloc>}}{{<bloc>}}

> "Les Sœurs proposent à la vente, en un ou plusieurs lots, plusieurs bâtiments pouvant être indépendants sur une surface habitable totale de 4 000 m2, dans un parc de plus d’1 hectare.
>
> Les projets porteurs de sens, en fidélité à l’histoire et à l’esprit du lieu, seront accueillis favorablement : habitat partagé, hôtellerie, lieu de ressourcement ou de formation, dispositif social ou médico-social et bien évidemment tout type d’initiatives pastorales…"

{{</bloc>}}{{</grid>}}

De son côté, la mairie a rencontré des représentants de l’Etablissement public foncier de la Région (EPORA) et du SCoT Rives du Rhône. Une étude précise du potentiel d’exploitation du couvent du Cénacle et aussi du bâtiment Ste Monique, prise en charge par ces institutions, va être réalisée cet été. Elle servira à éclairer des éventuels repreneurs et aussi à repérer les dispositifs de subventions concernés et à prévoir des demandes.

Les deux initiatives sont complémentaires. Espérons qu'elles permettront enfin de trouver un projet  qui fera revivre ces bâtiments symboliques dans l'histoire du village et essentiels pour sa prospérité.

### Journées citoyennes

Les chantiers prévus pour les journées citoyennes 2022 sont donc la rénovation et la promotion des parcours de balades (Lapins, champignons, botanique). D'autres petites réparations ou débroussaillages sont aussi nécessaires sur le camping.

Une première équipe va bientôt faire des repérages sur les parcours. Ensuite les travaux précis à prévoir vont être répertoriés et planifiés.

Les travaux s'organiseront de deux façons :

* d'une part au fil de l'eau avec des volontaires selon les besoins. Déjà la récupération du Citypark par un commando de citoyens a inauguré les chantiers de 2022. Et le fleurissement du village sera aussi pris en charge par des citoyennes du village. D'autres chantiers seront ouverts au fur et à mesure ;
* une journée plus festive, où tous les citoyens pourront se retrouver autour de travaux collectifs et d'un pique-nique sera organisée fin avril ou début mai à une date à préciser.

Au plaisir de vous y retrouver bientôt !

## Santé - sécurité

### Maintenir l'offre de santé sur le village

Notre docteur va bientôt faire valoir ses droits à une retraite bien méritée. La mairie est en relation avec le cabinet médical de Saint Félicien pour discuter de la meilleure solution de continuité possible pour une couverture médicale sur le village. Ce sera aussi l'occasion d'évoquer un avenir possible pour la pharmacie.

![](/media/pharmacie-lalouvesc.jpg)

En effet, notre pharmacienne souhaite, elle aussi, passer la main. Il faut donc trouver un repreneur pour cette pharmacie idéalement située dans un village touristique fort de ses nombreux événements religieux, sportifs et culturels. Elle dispose d'une clientèle fidélisée et le secteur de la phytothérapie et de l'aromathérapie, très porteur est à développer.

Le local a été refait à neuf et les charges sont faibles.  
Idéal pour une première acquisition. CA : 350 k€.  
Contact : 05 50 74 32 86

### Non au cyberharcèlement !

Le 8 février dernier, l’association e-Enfance a lancé avec le soutien du gouvernement, l’application mobile **3018**, pour signaler toute situation de harcèlement et assurer une prise en charge rapide et globale des victimes.

![](/media/cyberharcelement-2022.jpg)

### Café des aidants

Le prochain café des aidants de St Félicien aura lieu samedi 2 avril 2022 de 10h à 11h30 à l'Auberge de St Félicien sur le thème « Dépendance, autonomie, quelles différences ? »  
Les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).

Et à partir de 14h, à la salle des fêtes de St Félicien, participez à un après-midi convivial et artistique avec le collectif KANLARELA (actions artistiques et de bien-être à destination de personnes ayant des maladies neuro-évolutives, en situation de handicap ou d'isolement, ainsi qu'à leurs proches aidants du canton de St Félicien) - ouvert à tous

Renseignements auprès de Carole Guilloux : [cguilloux@fede07.admr.org](mailto:cguilloux@fede07.admr.org) ou 06 81 50 19 26

### Gendarmerie

La gendarmerie appelle à la vigilance les personnes qui ont des réservoirs de gasoil ou fuel pour stocker leur énergie. Des vols de carburants ont été signalés sur le territoire.

{{<grid>}}{{<bloc>}}

_L'application smartphone ministérielle **Ma Sécurité** est maintenant disponible sur les magasins applicatifs App Store et Play Store._

_L'application comprend diverses fonctionnalités tels une fonction "besoin d'aide" permettant d'orienter l'usager vers la ou les meilleurs solutions à son problème après avoir répondu à quelques questions ou un tchat accessible 24/24 et 7j/7 avec un personnel pour poser toutes les questions en lien avec la sécurité._

_Cette application est complémentaire à la proximité de votre brigade territoriale._

{{</bloc>}}{{<bloc>}}

![](/media/application-gendarmerie-2022.jpg)

{{</bloc>}}{{</grid>}}

### Calendrier Santé-Environnement

L'ARS de la région propose un calendrier pour alerter sur les [bonnes pratiques concernant l'habitat et la santé](https://www.auvergne-rhone-alpes.ars.sante.fr/habitat-et-sante). Pour le mois d'avril, la suggestion concerne [les travaux de rénovation](https://www.auvergne-rhone-alpes.ars.sante.fr/travaux-de-renovation?parent=14614). Un autre calendrier alerte sur les [bonnes pratiques face à l'environnement](https://www.auvergne-rhone-alpes.ars.sante.fr/environnement-exterieur-0). Pour le mois d'avril, la suggestion concerne l'arrachage de la plante invasive [_Berce du Caucase_](https://www.auvergne-rhone-alpes.ars.sante.fr/la-berce-du-caucase-0?parent=14402).

{{<grid>}}{{<bloc>}}

![](/media/int-04_travaux.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ext-04_berce.jpg)

{{</bloc>}}{{</grid>}}

## Mariage

{{<grid>}}{{<bloc>}}

![](/media/mariage-vaissie-2.jpg)

{{</bloc>}}{{<bloc>}}

Florencia Barragan et Pascal Vaissié se sont mariés à la mairie de Lalouvesc le 12 mars 2022.

Tous nos vœux de bonheur aux époux !

{{</bloc>}}{{</grid>}}

## Dans l'actualité, le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de mars qui restent d'actualité.

#### La Ronde Louvetonne, dernière ligne droite avant le coup de frein final !

26 mars 2022  
Demandez le programme ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-ronde-louvetonne-derniere-ligne-droite-avant-le-coup-de-frein-final/ "Lire La Ronde Louvetonne, dernière ligne droite avant le coup de frein final !")

#### Compte rendu du Conseil municipal

23 mars 2022 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/compte-rendu-du-conseil-municipal-3/ "Lire Compte rendu du Conseil municipal")

#### Arrêt de la récolte de dons en matériel pour l'Ukraine

16 mars 2022  
Privilégiez les dons en argent [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/arret-de-la-recolte-de-dons-enmateriel-pour-l-ukraine/ "Lire Arrêt de la récolte de dons en matériel pour l'Ukraine")

#### Qualité de l'eau en 2021

9 mars 2022  
Bonne selon les analyses de l'ARS [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/qualite-de-l-eau-en-2021/ "Lire Qualité de l'eau en 2021")

## Poissons

Au moment où nous publions ce bulletin, nous ne savons pas encore si cette météo du 1er avril sera un poisson ou une réalité...

![](/media/meteo-1er-avril-2021.jpg)

Quoiqu'il en soit, n'oubliez pas de faire des farces aujourd'hui premier avril, date de parution de ce Bulletin ! Il ne faut pas négliger les occasions de sourire en ces temps difficiles.

Pour stimuler votre imagination, voici un des meilleurs poissons d'avril : celui proposé par la SNCF en 2009, grâce à Philippe Peythieu, doubleur d’Homer Simpson, pour remplacer les messages diffusés en gare. Difficile de garder son sérieux en l'écoutant...

<iframe width="560" height="315" src="https://www.youtube.com/embed/Ha8qqmHacwk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>