+++
date = 2021-12-29T23:00:00Z
description = "n°18 Janvier 2022"
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "n°18 Janvier 2022"
title = "18 - Une année-et-demi bien remplie"
weight = 1

+++
Le titre du premier numéro du Bulletin d'information de Lalouvesc, celui de juillet 2020, était : _Lalouvesc "un village résilient"._ C'était un souhait émis par un conseiller nouvellement élu. Après une année-et-demi, peut-on dire que le souhait a été exaucé ?

Sans doute pas encore, la période a été marquée par les vagues successives de la pandémie et le village n'a pas été épargné. Et pourtant, il s'est embelli, des travaux importants ont été lancés, plusieurs manifestations ont pu se tenir, tout cela grâce à l'engagement et la solidarité de ses habitants.

Ce Bulletin fait le point sur une année-et-demi bien remplie, vous pourrez juger par vous-mêmes. Il comprend aussi, bien sûr comme toujours, plein d'autres informations...

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-18-1er-janvier-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Retour sur une année-et-demi, bien remplie

### Mot du maire

Chers amis,

Face à la dégradation du contexte épidémiologique, il nous a semblé plus prudent et responsable d'annuler la traditionnelle cérémonie des vœux municipaux. C'est pourquoi vous trouverez dans ce Bulletin un premier retour sur nos actions depuis notre arrivée à la Mairie.

Gardons l'espoir que l'avenir deviendra plus favorable, plus prospère, pour nous tous et surtout pour nos enfants. Avec ces grands espoirs sincères, je vous adresse au nom de l'équipe municipale, mes meilleurs vœux pour un avenir meilleur empreint de liberté, d'égalité, de solidarité, dans la joie et le bonheur partagés.

Amitiés,

Jacques Burriez, Maire de Lalouvesc

### Ce que nous avions annoncé, ce qui a été réalisé

Dans un [document](/media/lalouvesc-un-village-qui-a-du-caractere-2.pdf) largement diffusé en 2020, nous avons présenté notre programme. Ci-dessous l'extrait concernant le programme 2020-2021 avec, entre crochets, ce qui a été réalisé ou non.

{{<grid>}}{{<bloc>}}

![](/media/lalouvesc-un-village-qui-a-du-caractere.jpg)

{{</bloc>}}{{<bloc>}}

#### _Action 2020-2021_

##### _Réparer est la priorité_

* _cimetière \[en cours\], hôtel Beauséjour \[en cours\], mairie \[réalisé\], camping \[réalisé\], basilique \[en discussion\], assainissement \[en préparation\], matériel municipal \[réalisé\]…_
* _finances : réviser les dépenses \[réalisé\], définir des indicateurs \[à faire\], un budget sincère et cohérent \[en cours\],_
* _communication : révision du site web \[réalisé\], bulletin mensuel \[réalisé\], relation avec les employés municipaux, avec la population \[réalisé\]._

##### _Coordonner pour une meilleure efficacité_

* _ouverture de deux comités (vie locale et développement) aux non élus \[réalisé\],_
* _révision des relations avec les associations (conventions, maison des associations) \[à faire\],_
* _engagement d’un dialogue suivi avec le Sanctuaire \[réalisé\],_
* _organisation de journées citoyennes \[réalisé\]_

{{</bloc>}}{{</grid>}}

##### _Développer pour préparer l’avenir_

* _lancement des projets sur la forêt (Of. du tourisme \[à faire\], jeu-monument \[en cours\], gîte témoin \[abandonné\])_
* _accompagnement et réflexions sur l’utilisation des bâtiments symboles (Ste Monique, Le Cénacle) \[en cours\]_
* _Élargissement du camping \[réalisé\]_

En conclusion, la plupart des projets ont été lancés, un certain nombre sont même achevés, d'autres sont encore en discussion. Un projet a été abandonné : le gîte-témoin. Il devait amorcer la dynamique de l'écolotissement. Nous n'en avons pas eu besoin puisque quatre compromis de vente ont été signés, d'autres sont en discussion et les travaux d'aménagement vont démarrer. Des incertitudes restent sur l'avenir des bâtiments Ste Monique et du couvent du Cénacle. Et des opportunités imprévues ont été saisies comme l'achat d'un city-park ou l'audit sur les connexions dans le village.

Il reste deux points noirs :

* L'obligation de fusionner les trois budgets de la commune (général, eau et assainissement) par sa complexité imprévue a réduit la transparence financière au cours de l'année 2021. Nous sommes restés prudents dans les dépenses et dans les emprunts indispensables aux investissements, mais ce n'est qu'avec le budget 2022 que nous pourrons maîtriser pleinement l'outil budgétaire et construire les indicateurs que nous avions prévus.
* La vétusté et l'incomplétude des réseaux d'eau et d'assainissement entraînent de nombreux incidents coûteux. Nous avons actualisé le schéma directeur des réseaux. Mais d'importants travaux sont prévus et même si nous nous battrons pour obtenir des subventions, ces travaux indispensables pèseront sur le budget de la commune.

Terminons sur plusieurs notes optimistes.

La discussion des dossiers, les demandes de subventions et même les nombreux événements organisés sur la commune, nous ont permis de renouer le contact avec les principaux décideurs et bailleurs de fond de notre territoire : Préfecture, Département et Région. Ces bonnes relations sont indispensables au succès des projets du village, à la fois pour recueillir conseils et informations et aussi, bien sûr, pour faire déboucher positivement nos dossiers.

Enfin et surtout, nous avions fait le pari que le développement du village ne serait possible que si la population s'engageait pleinement dans sa dynamique. Ce pari est très largement gagné. Aussi si ce premier retour est positif, c'est d'abord grâce aux Louvetonnes et aux Louvetous qui nous ont accompagnés.

Les projets pour l'année à venir ont été discutés dans le [Comité de développement](https://www.lalouvesc.fr/media/reunion-du-comite-de-developpement-novembre-2021.pdf).

## Actualités de la mairie

### Audit sur la connexion dans le village

Dans la continuité de la collaboration avec le collectif Plateaux numériques avec lequel a été construit le site lalouvesc.fr, [un audit sur la connexion numérique à Lalouvesc](/media/enjeux-connexion-lalouvesc-2021.pdf) a été réalisé par Aurélien Béranger et Gauthier Roussilhe, doctorants en sciences de l'information et de la communication. Les auteurs ont fait le point sur le réseau 4G et fibre actuel et formulent huit recommandations au Conseil municipal.

Ces éléments s'ajoutent aux travaux qui vont démarrer cette année pour mesurer l'utilisabilité du site web et prévoir des améliorations.

### Le retour des Babets

![](/media/babet-2021-2.jpg)

Les babets ont changé de couleur et gagné un point en 2021. Ils sont passés d'une valeur de 5 euros à 6 euros. Ils permettent toujours un achat dans les commerces du village qui l'acceptent.

Pour la fin de l'année comme l'année dernière, une plaque de cinq babets a été distribuée aux résidents du village de plus de 65 ans. Gageons qu'ils rencontreront le même succès. Certains ont préféré un repas de fêtes en commun offert par la municipalité. Espérons que le virus autorisera sa tenue.

### Rencontre avec les sœurs du Cénacle

Le maire a rencontré à Paris sœur Véronique Fabre et sœur Jacqueline Guieu, économes provinciales de la congrégation Notre-Dame du cénacle à Paris. Il s'agissait d'un échange sur l’avenir de la propriété appartenant à la congrégation Notre-Dame du cénacle à Lalouvesc.

La congrégation a la volonté de vendre le bien et, suite à l'échec d'une vente l'année passée, la commune a offert d’accompagner et aider la congrégation dans sa démarche. Diverses possibilités d’accompagnement ont été évoquées pour rendre plus efficace la présentation et la diffusion du bien de façon à trouver un panel plus large et plus ciblé de prospects.

Le bâtiment offre un réel potentiel. Il pourrait favoriser une belle dynamique pour l’avenir de notre village. C'est aussi un patrimoine traditionnel du village. La commune a tout intérêt à réunir ses compétences pour accompagner la congrégation dans ses recherches commerciales.

### Visites de la Commission de sécurité

La Commission de sécurité a validé les travaux réalisés pour la mise en conformité de Milagro. Son exploitation va donc pouvoir reprendre. Après une visite à la Maison de retraite, la Commission a pu constater la bonne tenue des mesures de sécurité dans l'établissement.

Enfin une réflexion est en cours pour la mise en conformité de la Basilique.

## Zoom

### Vœux pour 2022

Toute l'équipe municipale vous présente ses vœux pour la nouvelle année :

![](/media/voeux-2022.jpg)

***

#### Vœux du père Olivier

**_Tous mes vœux d'amitié aux Louvetous pour 2022 de la part d'un qui a profité de vous et qui a filé comme un sauvage ! Merci encore. Olivier (père Olivier)_**

#### Vœux des sœurs du Cénacle

***

![](/media/voeux-cenacle-2021.jpg)

#### Vœux de Pierre Lollioz

##### Bon bout d'an !

En direct de Montréal, mais avec 6h en moins (je me réveille !!!), je vous souhaite de bien finir l'année. Profitez tous de ces bons moments de partage et de joie en famille et entre amis.

Ici entre températures basses (je dirai même très basses) et neige, je vous embrasse.

Pierre Lollioz

#### Vœux d’Élise Berment (régisseuse du _Capitaine Marleau_)

Joyeuses fêtes de fin d'années à tout le village !  
Je pense fort à vous.

Élise Berment

#### Vœux de Noëlle et Frédéric

Bonjour à toutes et à tous,  
Bien que nous ne soyons pas résidents à l’année (quelques mois par an seulement), nous profitons de cette opportunité mise en place par la mairie pour souhaiter à toutes les Louvetonnes et tous les Louvetous **nos MEILLEURS VŒUX pour 2022** et en particulier à nos voisines et voisins de Chante Aussel.

NOELLE ET FREDERIC  
10A Chante Aussel

***

#### Vœux des enfants et petits-enfants de Georges et Annie Chavanes

Tous nos bons vœux pour 2022 à tous les Louvetous !

Bonjour à tous les Louvetous,

C'est avec beaucoup de plaisir que nous lisons les actualités hebdomadaires de la Mairie, ce qui nous permet de rester en contact privilégié avec votre beau village. Nous remercions Mr le Maire et son équipe municipale, pour leur excellent travail réalisé cette année et les nombreux projets à venir. Ci-joint une photo de notre cher village, prise lors de notre dernier séjour à la Toussaint. Toute notre famille vous souhaite de bonnes fêtes de Noël, et vous adresse ses meilleurs vœux pour 2022 du Canada, d'Espagne, du Portugal et de France.

Les enfants et petits-enfants de Georges et Annie Chavanes

![](/media/lalouvesc-toussaint-2021-chavanes.jpg)

***

### Almanach 2021

Malgré les difficultés ou peut-être justement à cause d’elles, l'année passée a montré un village exceptionnellement vivant et solidaire. Voici un bref retour, mois par mois, sous forme d'un almanach, grâce à quelques actualités choisies sur ce site avec même un petit quiz.

Vous pouvez feuilleter l’almanach (ci-dessous, agrandissez-le en cliquant sur le carré en bas à droite de l'image) ou le [télécharger en pdf](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/almanach-de-lalouvesc-2021.pdf).

<iframe src="https://cdn.flipsnack.com/widget/v2/widget.html?hash=7jabm59zhm" width="100%" height="480" seamless="seamless" scrolling="no" frameBorder="0" allowFullScreen></iframe>

Et si vous voulez relire un Bulletin de l'année 2021 : format [web](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/) ou [Pdf](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-de-l-annee-2020/).

### Le Comité des fêtes n’a rien lâché !

À toutes et à tous le Comité des Fêtes souhaite une bonne et heureuse année.

Ces deux dernières années, nous n’avons rien lâché. Notre détermination à travailler les dossiers est restée intacte. Nous nous sommes organisés différemment. La Ronde louvetonne et le concert d’Amaury Vassily annulés (2020 & 2021), le Trail des Sapins annulé en 2020 ne nous ont pas demandé moins d’investissement. Loin de là ! Peu de réunions, l’administratif a été géré par téléphone et/ou en visio, par mail… Les décorations de Noël, de l’Ardéchoise, du Trail ont été l’occasion de petits ateliers individuels ou en petits groupes.

Bref, la distanciation, les gestes barrières, toutes les précautions requises ont été respectées.

Nous avons géré, nous nous sommes habitués. Nous avons été déçus. Beaucoup déçus même… Trop souvent déçus. Mais nous avons rebondi après chaque annulation pour s’orienter vers l’évènement suivant...

Et pour cette adaptation, pour cette détermination nous remercions tous les bénévoles car il est quand même plus motivant de travailler en équipes et en présentiel. Souhaitons-nous de nous retrouver « comme avant » pleins d’énergie, de convivialité, de complicité et de complémentarité. Oui… comme avant !

{{<grid>}}{{<bloc>}}

![](/media/brocante-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/trail-des-sapins-2021-course-1.JPG)

{{</bloc>}}{{</grid>}}

Deux grands évènements du Comité des fêtes seulement ont animé notre village en 2021, à l’automne.

Nous formulons le vœu que 2022 soit l’année de la liberté retrouvée. Nous formulons le vœu que Lalouvesc soit le témoin de grandes et belles manifestations.

À toutes et à tous le Comité des Fêtes propose de retenir ces quelques dates. Nous n’épargnerons pas notre énergie pour qu’un grand bol de convivialité souffle dans le village lors de ces journées.

![](/media/agenda-comite-des-fetes-2022.jpg)

**Une belle année à toutes et tous… à Lalouvesc !**

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

## Suivi

### Le Téléthon au club des deux clochers

Le jeudi 9 décembre 2021, dans le cadre du Téléthon, le **Club des deux clochers** proposait aux Louvetous de jouer à divers divertissements avec une participation modique. Plusieurs participants ont répondu à l’appel. Dans la salle du CAC mis à disposition par la mairie, l’après midi fut joyeuse et ludique. Cette animation a permis de récolter **266.20 €.** Le Club des deux clochers versera intégralement cette somme récoltée au Téléthon.

Le président Georges Ivanez remercie tous ceux qui ont contribué à cette noble cause.

### Retour sur le conseil municipal par une Louvetonne

Nous étions quatre Louvetous (ayant bravé la neige !) à assister à ce "Conseil" du 1er décembre, tous bien masqués et très distanciés. Et autour de la très grande table, nos élus presque au complet. Tout s'est déroulé "en bonne et due forme".

Au début, les questions étaient un peu techniques mais semblaient bien maitrisées’ et tout a été voté à l'unanimité. Pour une question qui concernait un membre du Conseil, celui-ci par honnêteté n'a pas pris part au vote.

{{<grid>}}{{<bloc>}}

Pour vous résumer dans le concret, nous allons avoir de nouvelles poubelles, qui permettront un "tri" plus affiné. Merci pour la planète ! Et puis, vous avez déjà dû le voir passer, nous avons un nouveau "chasse-neige". Il est orange et vert et sera aussi "multi-service".

J'ai été heureuse d'entendre que sur certains points, nos élus ont les mêmes pensées que moi : sur l'accueil des nouveaux habitants notamment, qui serait à renforcer (il faudrait presque créer un "club" des "Néo-Louvetous") et au sujet des chemins de randonnées et promenades de proximité, le balisage n'est pas assez visible et étoffé (Ce tourisme-vert est pourtant un atout majeur de notre village "entre Alpes et Cévennes") !

{{</bloc>}}{{<bloc>}}

![](/media/chasse-neige-lalouvesc-2021-5.jpg)

{{</bloc>}}{{</grid>}}

Les "grands chantiers" vont enfin démarrer ! Les journaux en ont déjà parlé. Notre village risque même de devenir très "chicos" avec tous les futurs projets ! Et ce serait là un juste retour des choses bien méritées, par la beauté de ces paysages et tout le patrimoine de son "riche passé" !

{{<grid>}}{{<bloc>}}

![](/media/illumination-2021-10.JPG)

{{</bloc>}}{{<bloc>}}

Et puis, (un peu hors sujet) la décoration de Noël a été mise en place à la Basilique et plus récemment sur la place panoramique devant nos "deux clochers". C’est tout du local et de très bon goût ! Allez voir ! Vive Lalouvesc ! Vive l'Ardèche !...et "Espérance" pour notre Ville Sanctuaire. Que les deux Bienheureux Saints qu'elle abrite nous bénissent déjà l'année 2022 qui s'approche sous un magnifique soleil après la haute neige !

{{</bloc>}}{{</grid>}}

Edith Archer

### Population

{{<grid>}}{{<bloc>}}

![](/media/mariage-a-dejhomme-2021.JPG "Aline Delhomme et Lionel Achard")

{{</bloc>}}{{<bloc>}}

La population légale de Lalouvesc au 1er janvier 2019 en vigueur à partir du 1er janvier 2022 d'après les enquêtes de recensement de l'Insee est de : **384 habitants**.

A noter qu'il s'agit d'un calcul par extrapolation, le prochain recensement précis du village devrait être réalisé en 2023.

#### Mariages, naissances

Quatre mariages ont été célébrés et quatre reconnaissances de naissance ont été enregistrées à la mairie en 2021.

{{</bloc>}}{{</grid>}}

#### Décès

Dix-huit décès ont été enregistrés sur la commune au cours de l'année. Deux hommages ont été publiés dans ce Bulletin :

* [Hommage à Mimi Solnon](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/7-lalouvesc-ne-connait-pas-la-deprime/#hommage-%C3%A0-mimi-solnon)
* [Eugenio Detto, dit « Geny », artiste](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/11-embellir/#eugenio-detto-dit--geny--artiste)

Par ailleurs, un avis de décès en relation avec le village a été publié dans la presse pour les personnes suivantes : Paulette Parat, née Chalaye ; André Guyennet ; Larie Grangeon ; Arlette Clemençon ; Jean Maisonnas ; Rose Perraux ; Marie Vallon ; Eliane Verrier d'Aigebonne, née Chavet ; Julien Cesa ; Roger Vernet ; Pierrette Ouillon, née Brouty ; Marie de L'Hermussiere ; Guy Crouzet ; Bernadette Pouzols, née Balandraud ; Gilbert Clapier ; Monique Couette ; Stéphanie Jedrzejczak ; Monique Pacory ; Maria Echevarria, née Santonja ; Jules Maisonnas ; Georgette Pramayon ; Simone Michel ; Régis Faurie.

### Prendre soin à la fois de notre santé et de l'environnement

Plusieurs organismes régionaux se sont réunis pour lancer une grande campagne de conseils pour améliorer nos cadres de vie.

![](/media/campagne-sante-et-environnement.jpg)L'environnement est un déterminant majeur de notre santé, par le biais de la qualité de l'eau, de l'air, du bruit, de l'insalubrité des lieux de vie...

Et les activités humaines ont quant à elles un impact sur notre environnement, en polluant l'air et l'eau par exemple.

En questionnant nos habitudes de vie et d'achat, et en partageant des idées d'actions simples, **il est possible de prendre soin à la fois de notre santé et de l'environnement !**

Voici quelques conseils :

* [Sauver sa peau...C'est possible !](https://c-possible.net/c-possible/sauver-sa-peau-cest-possible)
* [Ne pas lui faire avaler n'importe quoi...C’est possible !](https://c-possible.net/c-possible/ne-pas-lui-faire-avaler-nimporte-quoi-cest-possible)
* [Le laisser respirer...C'est possible !](https://c-possible.net/c-possible/le-laisser-respirer-cest-possible)
* [Le regarder pousser...C'est possible !](https://c-possible.net/c-possible/le-regarder-pousser-cest-possible)
* [Ne pas lui empoisonner la vie...C'est possible !](https://c-possible.net/c-possible/ne-pas-lui-empoisonner-la-vie-cest-possible)

### Dans l'actualité, le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de décembre qui restent d'actualité.

#### Lalouvesc sur France-Inter !

29 décembre 2021  
Le concours pour le jeu-monument présenté dans l'émission "Carnets de campagne" [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lalouvesc-sur-france-inter/ "Lire Lalouvesc sur France-Inter !")

#### Un Noël au Guatemala

24 décembre 2021  
Joyeux Noël à toutes et tous ! Un cadeau de Louvetous globe-trotters... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/un-noel-au-guatemala/ "Lire Un Noël au Guatemala")

#### L'ADMR recrute sur Lalouvesc

23 décembre 2021  
On recherche des aides à domicile et des auxiliaires de vie [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/l-admr-recrute/ "Lire L'ADMR recrute sur Lalouvesc")

#### Décorations : tout est en place !

21 décembre 2021  
Le coin des enfants, le coin des résidents de la Maison de retraite... les derniers occupants de l'hôtel Beauséjour [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/decorations-tout-est-en-place/ "Lire Décorations : tout est en place !")

#### Du lever au coucher, le soleil brille à Lalouvesc

20 décembre 2021  
Une semaine de soleil éclatant... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/du-lever-au-coucher-le-soleil-brille-a-lalouvesc/ "Lire Du lever au coucher, le soleil brille à Lalouvesc")

#### Le compte-rendu de la seconde réunion du Comité de développement est en ligne

13 décembre 2021  
Le Comité réunit élus et non-élus et réfléchit aux orientations à prendre pour le développement du village [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-compte-rendu-de-la-seconde-reunion-du-comite-de-developpement-est-en-ligne/ "Lire Le compte-rendu de la seconde réunion du Comité de développement est en ligne")

#### Quand Lalouvesc s'illumine...

12 décembre 2021  
C'est encore plus beau la nuit ! Un petit aperçu [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/quand-lalouvesc-s-illumine/ "Lire Quand Lalouvesc s'illumine...")

#### Le virus n'épargne pas le village

9 décembre 2021  
Des cas de Covid ont été détectés, respectez les gestes barrières et n'attendez pas pour les rappels de vaccination ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-virus-n-epargne-pas-le-village/ "Lire Le virus n'épargne pas le village")

#### Attention aux arnaques sur internet

9 décembre 2021  
Alerte de la gendarmerie [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/attention-aux-arnaques-sur-internet/ "Lire Attention aux arnaques sur internet")

#### Décorations : la collection 2021-22 s'installe

7 décembre 2021  
Le comité des fêtes a une fois encore magnifiquement égayé le square du Lac [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/decorations-la-collection-2020-21-s-installe/ "Lire Décorations : la collection 2021-22 s'installe")

#### Compte-rendu du Conseil municipal du 1er décembre

6 décembre 2021  
Compte-rendu du dernier Conseil municipal de l'année [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/compte-rendu-du-conseil-municipal-du-1er-decembre/ "Lire Compte-rendu du Conseil municipal du 1er décembre")

#### Un nouveau site web pour l'Abri du pèlerin

3 décembre 2021  
Réservation, présentation et historique [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/un-nouveau-site-web-pour-l-abri-du-pelerin/ "Lire Un nouveau site web pour l'Abri du pèlerin")

#### Les actions de l'APEL, au bénéfice des enfants de l'école redémarrent

3 décembre 2021  
Coffrets de noël, papiers cadeaux, billets de tombola, calendriers sont en vente dans les commerces de Lalouvesc [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-actions-de-l-apel-au-benefice-des-enfants-de-l-ecole-redemarrent/ "Lire Les actions de l'APEL, au bénéfice des enfants de l'école redémarrent")

## Feuilleton : _Les quatre éléments_

### Résumé des épisodes précédents

_Deux enfants partent autour du monde à la recherche de quatre pierres pour soigner un cheval blessé. Grâce à la fourmi, ils récupèrent la pierre TERRE en forêt amazonienne. Aidés par le dragon et le phénix, ils trouvent la pierre FEU à la Réunion..._

Vous pouvez lire ou relire les épisodes déjà publiés :

* [Chapitre 1 : Le cheval blessé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/#_les-quatre-%C3%A9l%C3%A9ments_)
* [Chapitre 2 : La pierre TERRE](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#chapitre-2--la-pierre-terre)
* [Chapitre 3 : A la recherche du FEU !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/17-engranger/#chapitre-3--a-la-recherche-du-feu-)

### Chapitre 4 : Le message codé

> Les enfants ont donc récupéré la pierre qui abrite l'élément du feu à la Réunion. Le phénix flamboyant dit aux enfants pour les aider que la troisième pierre se trouve vers Hawaï.
>
> {{<grid>}}{{<bloc>}}
>
> ![](/media/feuilleton-ch-4-2021-3.jpg)
>
> {{</bloc>}}{{<bloc>}}
>
> ![](/media/feuilleton-ch-4-2021-4.jpg)
>
> {{</bloc>}}{{</grid>}}
>
> Il appelle ses amis les gardiens du temple de l'eau. Ce sont deux dauphins nommés Jean-Philippe et Marguerite. Ils vont venir pour aider les enfants à faire le chemin jusqu'à Hawaï.
>
> Les voilà donc partis sur le dos des dauphins. Ils traversent l'océan Indien puis contournent les îles de l'Indonésie et arrivent dans l'océan Pacifique pour enfin se rendre à Hawaï.
>
> {{<grid>}}{{<bloc>}}
>
> ![](/media/feuilleton-ch-4-2021-1.jpg)
>
> {{</bloc>}}{{<bloc>}}
>
> ![](/media/feuilleton-ch-4-2021-2.jpg)
>
> {{</bloc>}}{{</grid>}}
>
> En arrivant vers Hawaï, les dauphins donnent aux enfants la rose des océans qui permet de respirer sous l'eau. En effet, le temple se trouve dans les profondeurs de l'océan Pacifique à l'est d'Hawaï.
>
> Les enfants plongent avec leurs fleurs, suivent les dauphins dans les profondeurs et respirent comme par magie grâce à la rose des océans.
>
> Ils arrivent devant la porte de l'_escape game_. Ils entrent facilement et, là, ils découvrent qu'ils doivent trouver un code secret pour pouvoir ouvrir la porte du temple.
>
> Voici le message codé :
>
> ![](/media/message-code-feuilleton-2021.png)
>
> {{<details "Clé du code, saurez-vous la découvrir ? (Cliquer pour basculer le triangle, mais quel triangle ?)">}} ![](/media/cle-du-code-feuilleton-2021.png)
>
> {{</details>}}
>
> Les enfants trouvent la clé et comprennent rapidement le message codé qui permettra d'ouvrir le temple de l'eau.
>
> Ils tapent le code sur le l'écran tactile et entrent dans le temple aquatique. Les dauphins les félicitent et leur donnent la pierre qui abrite le troisième élément : l'eau.
>
> A suivre...

(Histoire écrite collectivement par les élèves des classes CE1//CE2/CM1 de l’école St Joseph spécialement pour le Bulletin d’information de Lalouvesc. Vous pouvez suivre toute l’actualité de l’école St Joseph sur [sa page Facebook](https://www.facebook.com/Ecole-St-Joseph-Lalouvesc-107859627383105/))

#### Vous aussi, nagez avec les dauphins à Hawaï !

<iframe width="560" height="315" src="https://www.youtube.com/embed/nVvxZEzYqDg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Et en bonus, voici mille-et-une façons pour construire [un message codé](https://fr.wikihow.com/cr%C3%A9er-des-codes-et-des-chiffres-secrets) avec vos enfants, petits-enfants ou avec ceux des autres.