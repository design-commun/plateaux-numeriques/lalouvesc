+++
date = 2022-02-26T23:00:00Z
description = "n°20 Mars 2022"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°20 Mars 2022"
title = "20 - Place aux jeunes"
weight = 1

+++
Les jeunes ont la part belle dans ce numéro : les écoliers, leur voyage-découverte et leurs idées pour le jeu-monument, les étudiants, chargés d'y réfléchir, qui nous ont rendu visite et aussi le voyage du city-parc pour les jeunes sportifs, acheté sur une autre Commune et qu'il a bien fallu démonter et transporter. On vous raconte tout cela ... et, comme toujours, bien d'autres choses encore.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-20-1er-mars-2022.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Mot du maire

Mes chers amis,

De nouveau parmi vous au village, je me réjouis vraiment de retrouver mes amis, mes attaches. J’ai été hospitalisé trois semaines pour des problèmes consécutifs à une thrombose pulmonaire.

Très bien soigné aux soins intensifs du service cardiologique de l'hôpital d'Annonay, la situation s’est grandement améliorée et me revoilà à vos côtés, encore plus motivé. Cette épreuve personnelle a renforcé encore mon désir d’accomplir mon travail quotidien (qu'est ce qu'on est bien à la mairie !).

Tout d'abord, je tiens à remercier tout particulièrement les membres du conseil municipal et le personnel de la Mairie pour leur soutien et la motivation dont ils font preuve dans l’accomplissement des tâches journalières. Merci aussi à toutes les personnes qui m'ont appelé ou envoyé des messages pour me souhaiter un bon rétablissement. Je n'ai pas eu le temps de m'ennuyer, il fallait répondre...

Dans ce Bulletin, nous présentons plusieurs actions en direction des jeunes. Nous avons déjà proposé un local pour les ados. Je crois qu'ils sont en train de s'organiser pour pouvoir en profiter. Le jeu-monument et le city-stade sont deux autres projets qui devraient toucher plusieurs tranches d'âge.

Nous sommes pratiquement au printemps, c’est le moment d’organiser notre stratégie estivale, tout le monde doit se motiver : commerçants, artisans, associations, bénévoles. Toutes les idées seront les bienvenues, vous avez ce Bulletin d'information ou les actualités de la semaine pour vous exprimer, n’hésitez pas. Profitons-en, la situation sanitaire semble vouloir s’améliorer, il faut se mobiliser pour organiser une très bonne saison.

Soyons unis pour faire avancer notre village, avançons dans le même sens, notre intérêt est commun, continuons dans cette relation de confiance entre les élus et les Louvetous, qui, a mes yeux, doit demeurer inaltérable.

Amitiés

Jacques Burriez

## Actualités de la mairie

### Chantier de l'écolotissement

Le chantier d'aménagement de l'écolotissement va démarrer mi-mars. Une première phase de travaux durera cinq semaines pour le tracé des voies et la mise en place de tous les réseaux : eau, assainissement, électricité, télécommunication, soit la voirie et la viabilisation du lotissement.

Une pause dans les travaux d'aménagements est ensuite prévue pour donner l'occasion du lancement des premières constructions sans risquer de dégrader l'esthétique des aménagements finaux : revêtement, murets, espaces verts. Ces derniers auront lieu à l'automne.

![](/media/ecolotissement-ventes-fevrier-2022.jpg)

Rappelons que le lancement des opérations fait suite à la signature de quatre compromis de vente, concernant les lots en rouge sur la carte.. Deux autres compromis sont en discussion.

Attention, le début des travaux d'aménagement et l'arrivée prochaine des beaux jours risquent de réveiller l'intérêt de nouveaux acheteurs. Si vous souhaitez construire à Lalouvesc sur un espace préservé, ce sont les derniers lots accessibles sur la Commune.

[Plus d'informations](https://www.lalouvesc.fr/projets-avenir/changement-climatique/ecolotissement-du-bois-de-versailles/).

### Changements chez les employés municipaux

Du côté des services techniques, Gérard Fraysse est en arrêt-maladie. La municipalité aura donc besoin d'un autre ouvrier pour les cinq mois d'été, de mai à septembre. Si vous êtes intéressé ou si vous connaissez une personne intéressée, contactez rapidement la Mairie.

Du côté du secrétariat, Françoise Arenz-Faurie est, de nouveau, seule au bureau. L'expérience de ces dernières années, et celle d'autres Communes comparables, montrent qu'il est difficile de trouver sur ce profil de poste des personnes polyvalentes et motivées. Pour épauler notre secrétaire, nous cherchons une alternative qui allierait l'externalisation de certaines tâches comme les fiches de paie et l'utilisation à temps partiel d'une secrétaire en poste dans une Commune proche.

Et enfin du côté du camping, la saison estivale approche et les réservations vont bon train. Nous aurons besoin, comme l'année dernière, d'une personne sur la période juin-août pour seconder la responsable et d'une autre titulaire du BAFA sur un mois-et-demi pour les animations.

### Un local pour les employés municipaux

Attendu depuis plusieurs années, le local destiné aux employés municipaux, situé derrière le garage municipal, est presque terminé. Merci aux employés qui ont participé aux travaux et surtout à Dominique Balaÿ qui a grandement contribué à sa réalisation !

{{<grid>}}{{<bloc>}}

![](/media/local-employes-fev-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/local-employes-fev-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/local-employes-fev-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/local-employes-fev-2022-7.jpg)

{{</bloc>}}{{</grid>}}

### Journées citoyennes

Une première inspection du parcours des lapins et de celui des champignons a été faite par les élus en prévision des chantiers à ouvrir pour les journées citoyennes. N'hésitez pas à y faire un tour, et aussi du côté du sentier botanique. Il faut recenser tous les travaux et améliorations à apporter. Il faudra aussi imaginer la communication et l'attractivité pour les futurs promeneurs.

Il y a aussi d'autres points à nettoyer et à embellir dans le village. Toutes les idées seront débattues... et surtout toutes les énergies seront indispensables, chacun et chacune dans la mesure de ses possibilités.

Une réunion, à laquelle vous êtes toutes et tous conviés, sera programmée en mars pour organiser les chantiers. Préparez vos agendas.

Déjà un commando d'une dizaine de citoyens louvetous ont devancé l'appel en allant récupérer le city-parc que nous avons acquis, près de Clermont-Ferrand (voir plus loin.)

## Zoom

### Musique et cirque

Amandine Verdun, directrice de l'école St Joseph :

Nous sommes partis en classe de découverte « musique et cirque » du 31 janvier au 4 février 2022 au centre Musiflore à Crupies dans le sud de la Drôme.

L'organisation de cette classe de découverte n'a pas été simple. Initialement nous devions partir à Autrans mais, en octobre, nous avons appris que le centre qui devait nous accueillir fermait pour des raisons financières. Il a donc fallu trouver rapidement un autre centre avec la même thématique. Finalement nous avons pu réserver le créneau fin janvier - début février... et ce fut juste après une grosse vague de Covid à l'école !

Nous sommes partis avec 18 élèves de la petite section au CM1. Ce fut une riche expérience.

#### Musique

{{<grid>}}{{<bloc>}}

![](/media/classe-decouverte-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/classe-decouverte-2022-7.jpg)

{{</bloc>}}{{</grid>}}

Nous avons pu chanter dans une grande chorale, avec trois autres classes, ce qui faisait beaucoup de monde dans l’auditorium ! Puis nous avons expérimenté le rythme avec des instruments : djembé, darbouka, congas, caisse claire, maracas, claves...

#### Cirque

![](/media/classe-decouverte-2022-2.jpg)

{{<grid>}}{{<bloc>}}

![](/media/classe-decouverte-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/img-20220202-wa0101.jpg)

{{</bloc>}}{{</grid>}}

Nous avons aussi eu plusieurs séances de cirque : découverte des assiettes chinoises, du bâton du diable, du fil, de la jonglerie, de la boule, du rola bola et des pédalettes.

#### Nature

{{<grid>}}{{<bloc>}}

![](/media/classe-decouverte-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/classe-decouverte-2022-5.jpg)

{{</bloc>}}{{</grid>}}

Et de nombreuses activités nature : land art, observation de salamandres, travail de l'argile, construction d'un nid géant, écoute du chant des oiseaux.

#### Autonomie

Les enfants ont enfin également pu appréhender une semaine sans les parents ! Les moments de vie quotidienne étaient encadrés par des animateurs mais l'autonomie était de mise pour : se préparer le matin, débarrasser la table, trier les déchets, se laver... Tout cela a aussi été très formateur !

Ce fut ensuite le retour à l'école pour une semaine puis les vacances.

### Des idées pour le jeu-monument

La disparition de l’hôtel Beauséjour a dégagé la vue sur le Val d'Or. C'est une bonne raison pour revoir sa configuration et renouveler son équipement et l'idée d'un jeu monument prend corps, en commençant par une première étape avec le concours lancé auprès des étudiants.

#### Première visite

Julie et Guillaume, les étudiants sélectionnés pour le concours d'idées sont venus début février, depuis la Belgique où se trouve leur école, visiter le village. Ils ont pu découvrir les lieux, le parc du Val d'Or d'abord où doit s'ériger le jeu-monument et puis l'ensemble du village. Ils ont rencontré aussi des habitants, à commencer par la responsable de l'Office du tourisme, ceux du Sanctuaire et des professionnels du bois.

Ils nous ont envoyé leurs premières impressions sous forme d'une carte-postale et d'un joli poème.

![](/media/carte-lalouvesc-g-diche-j-demeulenaere-2.jpg)

> **Beau séjour**
>
> Selon nous, découvrir un lieu c’est avant tout le traverser.  
> Faire du terrain, n’est-ce pas se promener et rencontrer ?  
> Bien décidés et carnet à la main nous optons pour la marche du matin,  
> Où les Cévennes prennent forme sous une épaisse brume au loin.
>
> Ce matin le décor est époustouflant.  
> Le vent frais anime les arbres derrière le gîte, le soleil se faufile entre des pins qui chantent leurs âges.  
> Il y a de quoi être émerveillé par la couleur que prend la vallée lorsque le soleil apparaît.
>
> La traversée fut longue,  
> Mais ce n’est pas faute d’avoir été prévenus,  
> Entre sentiers, parcours et rues oblongues,  
> Lalouvesc nous souhaite finalement la bienvenue.
>
> Ce soir le décor est encore différent.  
> La neige a disparu des bas-côtés, le Vival se ferme, la pharmacienne est sur son chemin de retour.  
> Il y a de quoi être inspiré par toutes ces énergies habitées, par tous ces Louvetous motivés.
>
> Le chemin des Afars est une poésie,  
> Où le Val d’Or fait l’endormi,  
> Cachés dans l’ancien réservoir les petits chapardeurs sont de sortie,  
> La cascade assouvit le récit du chapelet infini.
>
> Nous vous remercions pour votre accueil chaleureux.  
> Il n’est pas simple de voir disparaitre un bâtiment dans un village, la lumière traverse désormais une ruine louvetou.
>
> Beauséjour,  
> Avec un peu d’amour.

#### Les idées des enfants

Les enfants de l'école Saint Joseph ont aussi travaillé sur le projet de jeu-monument. Extraits du compte-rendu des ateliers tenus à l'école par André Lamourère qui les a animés :

> La plupart des enfants ont une bonne connaissance des arbres et de la forêt et une bonne partie a un -ou des- proche qui travaille au contact du bois, mort ou vivant, du fait de leur environnement proche. Quelques-uns expriment leurs craintes de la dégradation du climat.
>
> Ils ont entendu parler du projet de jeu monument sans que cela soit bien précis ni clair. Au cours du second atelier nous sommes entrés  dans le concret du projet de la commune : entre temps, l’hôtel a été démoli…

![](/media/jeu-monument-ecole-st-joseph-2022-2.jpeg)

> Alors, qu’est-ce qu’un jeu monument ? Je collecte leurs idées de jeux et de monuments… du côté « jeu », ça fuse, du côté « monument » c’est plus difficile. Nous imaginons ensuite comment allier les deux termes : ils travaillent en petits groupes à des propositions dessinées. Le matériau privilégié est logiquement le bois. Au-delà des jeux classiques (tyrolienne, toboggan, balançoire….) ressort aussi une envie de jeux de découverte de la forêt et de ses animaux (memory, quizz…) et d’un « coin calme ». Les cabanes perchées figurent dans la plupart des dessins.

![](/media/jeu-monument-ecole-st-joseph-2022-5.jpg)

{{<grid>}}{{<bloc>}}

![](/media/jeu-monument-ecole-st-joseph-2022-1.jpg)

![](/media/jeu-monument-ecole-st-joseph-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/jeu-monument-ecole-st-joseph-2022-7.jpg)

![](/media/jeu-monument-ecole-st-joseph-2022-8.jpg)

{{</bloc>}}{{</grid>}}

> En fin de séance, nous commençons des maquettes à l’aide de petits matériaux en bois… les enfants les continueront en classe avec Amandine.

### Le city-stade a été récupéré

{{<grid>}}{{<bloc>}}

![](/media/city-park-2021-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/city-park-2021-4.jpg)

{{</bloc>}}{{</grid>}}

Une petite dizaine de citoyens louvetous, menés par le premier adjoint, sont partis dès l'aube le 25 février pour Aubière, près de Clermont-Ferrand, pour démonter et ramener sur la Commune le city-stade acheté aux enchères à l’automne dernier.

Un chantier spectaculaire, rondement mené dans la bonne humeur. En une journée ce fut descellé, dévissé, décollé, roulé et emballé... Et le camion a été déchargé sur le village le lendemain.

> **Bravo, vraiment bravo ! et merci !**

![](/media/demontage-city-stade-2022-3.jpg)

{{<grid>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-15.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-14.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-13.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-11.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-8.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demontage-city-stade-2022-9.jpg)

{{</bloc>}}{{</grid>}}

![](/media/demontage-city-stade-2022-10.jpg)

Et maintenant, il reste à trouver sa meilleure place sur le village, et, si possible, une subvention pour le montage.. Plusieurs emplacements sont envisagés. Il faudra peser les avantages et les inconvénients de chacun.

## Suivi

### Ronde Louvetonne : les anciennes de retour à Lalouvesc

Tandis que l’hiver nous tourne le dos et que le printemps nous fait de l’œil, le Comité des Fêtes s’active pour l’organisation de l’évènement qui ouvrira le bal des animations prévues pour cette année 2022 dans notre village.

![](/media/ronde-louvetonne-2022-logo.jpg)

Après deux rendez-vous manqués, l’impatience est palpable et l’implication de tous n’est pas démentie pour accueillir ces anciennes que nous affectionnons tout particulièrement. Le parc d’exposition sera haut en couleurs, en exceptions et en richesses. Chacun aura plaisir à les voir partir pour la balade proposée, sur un parcours savamment tracé pour offrir à l’œil de chacun les beautés de notre région.

Les[ inscriptions sont ouvertes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-inscriptions-pour-la-ronde-louvetonne-2022-sont-ouvertes/) et elles arrivent nombreuses ! Fin février, ce sont déjà 40 voitures qui sont attendues.

![](/media/ronde-louvetonne-2022-1.jpg)

Leur retour sur le parc d’exposition sera un beau spectacle avant que chacun ne pense à se restaurer.

La possibilité de réserver un plateau ardéchois pour le déjeuner est un succès. C’est une nouveauté qui devrait fluidifier l’accueil et la vente de la restauration rapide habituelle. Cela va de soi, la composition est 100 % locale avec l’investissement de nos commerçants de bouche.

#### Tombola

Dans le cadre de cette journée une tombola est proposée. Elle permettra de gagner un des trois beaux paniers gourmands/découverte.

{{<grid>}}{{<bloc>}}

* **Gourmands** : les beaux, bons et nombreux produits régionaux qui les composent nous ont été offerts par des commerçants, artisans et producteurs locaux.
* **Découverte** : moult parcs, musées et autres sites à visiter dans notre belle région ont été nombreux à nous offrir des entrées.

Les commerçants du village sont encore sollicités pour vous proposer de participer. C’est chez eux que vous pourrez acheter un (ou des) numéro(s), 2€ l'un. Nous les remercions pour leur disponibilité. N’hésitez pas à tenter votre chance avant que la déferlante de visiteurs le 3 avril ne vous soufflent la mise... c'est ce même jour dans l'après-midi que les trois heureux  gagnants seront annoncés

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-panier-loto.jpg)

{{</bloc>}}{{</grid>}}

Diverses animations, musicales notamment, devraient emmener notre village dans une belle fête annonciatrice de bien d’autres évènements festifs à Lalouvesc. Souhaitons-nous une belle saison estivale 2022 !

N’hésitez pas à nous contacter et à nous rejoindre : « **_seul on va plus vite, ensemble on va plus loin_** »

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

### Nouvelles sorties pour le club du Carrefour des Arts

Jacques Morel, vice-président du CdA :

![](/media/sortie-1-carrefour-des-arts-2021-2.jpg)

Après les visites au Musée d'Art Moderne et Contemporain de Saint-Étienne puis au salon l'Hivernale de Lyon en janvier,  nous vous proposons de poursuivre nos découvertes des arts plastiques à Annonay le 12 mars prochain, avec au programme :

* 10h30 : accueil au GAC (Groupe d'Art Contemporain), 1 rue de la République à Annonay. Le GAC est lieu de production et de diffusion de l’art contemporain qui déploie ses activités à travers un programme annuel d’expositions, d’éditions d’estampes, de soutien à la production artistique :
  * visite de l'exposition Monotrémia de Julia Scalbert,
  * présentation de l'artothèque du GAC.
* 12h30 : déjeuner dans le vieil Annonay.
* 14h00 : rendez-vous avec l'association "La moustache et les dentelles " qui a pour but la création artistique et design, sous forme d’un atelier ouvert. Elle propose des expositions permanentes ainsi que des expositions ponctuelles d’autres artistes. Elle invite aussi les artistes et les habitants à intervenir dans l'espace public et sur les vitrines.
* 15h : visites de galeries et ateliers d'artistes dans le vieil Annonay.

L'ensemble de ces visites est gratuit pour les adhérents du Carrefour, une somme d'environ 20€ est à prévoir pour le déjeuner. Si cette sortie vous intéresse, nous vous remercions de nous le faire savoir avant le 7 mars : contact@carrefourdesarts-lalouvesc.com

Vous pouvez également noter nos prochaines sorties :

* le 14 mai : Musée d'art contemporain de Lyon,
* le 11 juin : la Biennale du Design de Saint-Étienne.

### Visite du centre de secours des pompiers

Le nouveau président du Conseil d'Administration du Service Départemental d'Incendie et de Secours (CASDIS), Pierre Maisonnat, le colonel Alain Rivière, le capitaine Jérôme Ployon sont venus visiter le centre de secours de Lalouvesc fin janvier 2022.

![](/media/pompiers-visite-janvier-2022.jpg)

Ils ont échangé avec les pompiers et les élus présents sur les disponibilités du personnel et les problèmes liés au Covid. Malgré l’effectif réduit, les interventions ont pu être assurées. Il a été aussi constaté que les locaux mériteraient des rénovations pour être aux normes, notamment du fait de l'ouverture des effectifs aux femmes.

Une réflexion doit être engagée entre les SDIS 07 et la Mairie pour remédier à cette difficulté.

### Hommage à Albert Morino

Le village a perdu Albert Morino, son doyen, qui tenait une place toute particulière dans le cœur des Louvetonnes et des Louvetous. Ci-dessous un résumé des mots prononcés par Sylvie Jay, sa belle-fille, au cours de la cérémonie des obsèques.

{{<grid>}}{{<bloc>}}

![](/media/albert-morino-hommage.jpg)

{{</bloc>}}{{<bloc>}}

> Albert fut un personnage, un colosse, un homme jovial, gentil, attentionné et aussi volontaire, voire tenace et entier. Il a vécu 98 ans, il a donc eu le temps de lier de nombreuses amitiés et d'avoir eu plusieurs vies dont certaines nous resteront peu connues.
>
> Il est né dans le nord de l'Italie, dans une famille simple et en avait gardé un dégout pour les châtaignes dont il avait sans doute abusé enfant. La Résistance a été un épisode de sa vie intense et violent.
>
> Il a eu une entreprise florissante de fournitures de boulangerie et pâtisserie dans la région d'Aubagne dont Metro a eu raison. Ce fut aussi un grand sportif, joueur de foot à l'OM, marcheur infatigable, franchissant des cols à plus de 5.000 m passé 70 ans, et faisant sa culture physique jusqu'à la fin.  
> {{</bloc>}}{{</grid>}}
>
> Il a eu une première épouse et une fille Régine. Puis ce fut sa rencontre avec Nanou qui a bouleversé sa vie. Leur temps s'est partagé entre Cassis et Lalouvesc où il s'est progressivement investi dans la vie du village, au théâtre de la Veillée, au Carrefour des arts, dans le balisage des sentiers et dans de multiples rencontres. Il a aussi laissé un fort et toujours présent souvenir aux compagnons d'Emmaüs.
>
> Il s'est aussi beaucoup investi pour ses petits-enfants.
>
> Je voudrais rendre hommage aux Louvetous qui, après le départ de Nanou qui l'a laissé inconsolable, ont su continuer à l'entourer. Je peux vous assurer qu'il n'a oublié personne et qu'il m'a confié qu'il avait été heureux "immensément heureux".

### Attention, les élections présidentielles sont pour bientôt !

Premier tour des élections présidentielles : **10 avril**.

#### Inscription sur les listes électorales

Pour participer à cette élection et vous inscrire sur les listes électorales, si ce n'est déjà fait, vous avez jusqu'au **2 mars** pour une[ inscription en ligne](https://www.service-public.fr/particuliers/vosdroits/R16396) et jusqu'au **4 mars** pour une inscription par courrier ou en mairie.

Si vous avez un doute, [vérifiez que vous êtes bien inscrit sur les listes électorales]( "https://www.service-public.fr/particuliers/vosdroits/services-en-ligne-et-formulaires/ISE").

#### Vote par procuration

Si vous ne pouvez vous déplacer le jour du vote et que vous souhaitez que quelqu'un, muni d'une procuration, vote à votre place, ne vous y prenez pas au dernier moment pour que la Mairie ait le temps de recevoir la procuration autorisant le vote de la personne qui vous remplace.

Vous pouvez faire la [demande en ligne](https://www.service-public.fr/particuliers/vosdroits/F35802/0_0_0?idFicheParent=F1604#0_0_0) ou récupérer en ligne [un formulaire](https://www.service-public.fr/particuliers/vosdroits/F34852/3_0_0_1?idFicheParent=N47#3_0_0_1) pour le remplir chez vous ou encore aller le chercher dans un commissariat ou une gendarmerie.  
Dans tous les cas, il vous faudra ensuite vous déplacer en personne dans un commissariat de police ou une gendarmerie avec une pièce d'identité pour enregistrer la procuration.

[Plus d'informations](https://www.service-public.fr/particuliers/vosdroits/N47#3_0_0_1).

### Du répit pour les aidants

L’Adapei de l’Ardèche est chargée de venir en **soutien aux aidants** du territoire. Elle a mis en place un dispositif d’aide pour les proches de personnes en situation de handicap.

La **plateforme de répit PR3A** se charge d’organiser des temps de répit pour les aidants qui ont à leur domicile, un enfant, un adolescent ou un adulte en situation de handicap.

Coordinatrice pour l'Ardèche-nord : Nancy Ducord, [nancy.ducord@adapei07.fr](mailto:nancy.ducord@adapei07.fr), tél 06 08 41 93 62.

![](/media/pr3a-aidants.jpg)

### L'IGN autorisé à entrer sur les propriétés privées

Par arrêté préfectoral, les agents de l'Institut national de l'information géographique et forestière (IGN), les opérateurs privés opérant pour le compte de l'IGN et le personnel qui les aide dans ces travaux, sont autorisés à circuler librement sur le territoire de l'ensemble des communes du département et à accéder aux propriétés publiques ou privées, closes ou non closes, à l'exception des maisons d'habitation.

Concernant les opérations de l'inventaire forestier national, les agents pourront effectuer au besoin dans les parcelles boisées, les haies, les alignements, les terres plantées d'arbre épars ou à l'état de landes ou de broussailles, des coupes de la végétation herbacée ou arbustive selon des couloirs pour permettre de mesurer des angles ou des longueurs d'objets distants, planter des piquets, effectuer des mensurations ou des sondages à la tarière sur les arbres, apposer des marques de repère sur les arbres ou les objets fixes du voisinage. Il est précisé qu'il ne peut être abattu d'arbres fruitiers, d'ornements ou de haute futaie, avant qu'un accord amiable se soit établi sur leur valeur, ou qu'à défaut de cet accord il ait été procédé à une constatation contradictoire destinée à fournir les éléments nécessaires pour l'évaluation des dommages.

[Lire l'arrêté](/media/20220208_arr_ign_auto_penetrer_propr_privees_vs.pdf).

### Dans l'actualité, le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de février qui restent d'actualité.

#### Le Dauphiné Libéré recherche son correspondant local

23 février 2022  
... sur le Val d'Ay [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-dauphine-libere-recherche-son-correspondant-local/ "Lire Le Dauphiné Libéré recherche son correspondant local")

#### Réduire les incivilités

22 février 2022  
Ne mettez pas n'importe quoi dans les poubelles... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/reduire-les-incivilites-1/ "Lire Réduire les incivilités")

#### Journées portes ouvertes au SYTRAD

17 février 2022  
Les 19 et 22 mars, sur inscription [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/journees-portes-ouvertes-au-sytrad/ "Lire Journées portes ouvertes au SYTRAD")

#### Coupure d'électricité sur la Commune, mardi 22 mars

16 février 2022  
Durée approximative : 2h30 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/coupure-d-electricite-sur-la-commune-mardi-22-mars/ "Lire Coupure d'électricité sur la Commune, mardi 22 mars")

#### Le camping se remplit pour l'Ardéchoise

15 février 2022  
Suggestion : réserver un refuge dans les arbres ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-camping-se-remplit-pour-l-ardechoise/ "Lire Le camping se remplit pour l'Ardéchoise")

#### Des nouvelles des chantiers

10 février 2022  
Piquetage de la façade sur l'espace Beauséjour, jeu-monument, ouverture du chantier sur l'écolotissement [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/des-nouvelles-des-chantiers/ "Lire Des nouvelles des chantiers")

#### Les inscriptions pour la Ronde louvetonne 2022 sont ouvertes

6 février 2022  
Ce sera le 3 avril. Rassemblement et balade. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-inscriptions-pour-la-ronde-louvetonne-2022-sont-ouvertes/ "Lire Les inscriptions pour la Ronde louvetonne 2022 sont ouvertes")

#### Edition de l'Ardéchoise des Jeunes reportée en 2023

1 février 2022  
Courrier des organisateurs [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/edition-de-l-ardechoise-des-jeunes-reportee-en-2023/ "Lire Edition de l'Ardéchoise des Jeunes reportée en 2023")

## Puzzle

L'hôtel Beauséjour, n'est plus là. Faites revivre sa carte des grands jours.

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=023d0e87ba06&view=iframe" style="width:100%;height:600px" frameborder=0 allowfullscreen></iframe>