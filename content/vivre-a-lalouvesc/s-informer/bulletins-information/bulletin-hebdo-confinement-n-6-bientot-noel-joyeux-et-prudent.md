+++
date = 2020-12-11T23:00:00Z
description = ""
draft = true
header = ""
icon = ""
subtitle = ""
title = "Bulletin Hebdo-confinement n°6 Bientôt Noël, joyeux et prudent !"
weight = 1

+++
Voici donc le dernier bulletin hebdomadaire, puisque le confinement sera levé le 15 décembre. Vous y trouverez de la joie (les illuminations du village) de la prudence (le point sur la pandémie), la suite de l’histoire de Lalouvesc et plein d’autres informations encore…

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier pdf (cliquer) ](https://www.lalouvesc.com/wp-content/uploads/2020/12/Bulletin-Hebdo-confinement-de-Lalouvesc-n%C2%B06-12-decembre-2020.pdf)et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Chères villageoises et chers villageois,

Nous sommes à la veille des journées les plus festives de l’année, plongés dans une grande interrogation, comment aborder ces fêtes ? Dans un climat de contraintes sanitaires, Noël se déroulera dans la plus stricte intimité. Nous devrons nous réunir avec la famille la plus proche et en nombre très restreint.  
Le grand réveillon de fin d’année avec les amis, ce sera pour une autre fois. Il faut respecter les consignes et se limiter à un bon repas à la maison. C’est contraignant, mais il en va de la santé générale. Mon conseil : soyez plus rigoureux encore que les recommandations préfectorales et nationales.  
Nous avons tout de même notre beau manteau blanc pour Noël. La neige est là avec ses joies et ses difficultés. Notre employé municipal, Michel Astier au volant du chasse neige communal, s’active à nettoyer le village du mieux possible. Il a un plan de déneigement avec ses priorités qu’il doit respecter. Soyez patient il passera partout mais quelle que soit l’organisation de la journée, il y aura toujours un premier et un dernier déneigé.  
La décoration du village est presque achevée ! L’installation n’a pas été facile à cause des conditions climatiques, nos bénévoles transis de froid ont été très courageux et, encore une fois, je leur adresse, du fond du cœur tous mes remerciements pour leur mobilisation. Sans eux, sans vous tous, rien ne serait possible. Saluons cette unité, cette mobilisation très spécifique à notre village qui a décidément du caractère !  
Les conditions actuelles de vie ne sont pas des plus réjouissantes, chacun le ressent, nous les subissons sans pouvoir vraiment y changer grand chose. Soyons optimistes, nous allons vers des jours meilleurs. Je vous souhaite une fin d’année pleine de bonheur et de vitalité, réchauffons nos cœurs et apportons la joie autour de nous.

Jacques Burriez, Maire de Lalouvesc en fêtes.

## Activités de la Mairie

### Succès pour l’agence postale

L’agence postale communale a ouvert ses portes il y a tout juste un mois. C’est l’occasion de faire un premier point sur sa fréquentation. Françoise Arenz a reçu 173 personnes durant les 20 premiers jours d’ouverture, soit une moyenne arrondie de 9 personnes par jour. Ce résultat, nettement plus élevé que l’affluence de l’ancienne poste, montre que l’agence répond à un réel besoin.

### La Biblio’Chouette remplit ses étagères

Les étagères et les livres ont rejoint les nouveaux locaux, flambant neufs de la bibliothèque. Sa réouverture est pour bientôt. Nul doute qu’elle aussi, maintenant bien visible de la rue centrale, verra sa fréquentation augmenter.

### Assainissement : réparation d’une pompe et programmation

Sur le bord du Chemin Grosjean au-dessus du terrain de jeux se situe la chambre de relevage des eaux usées provenant du lotissement du Val d’Or. Dans le cadre de la remise en état du réseau d’assainissement il est prévu de refaire cette chambre qui accueille deux pompes de relevage. Les travaux sont programmés dans l’avenir, plus largement, la municipalité travaille sur une programmation pluriannuelle des travaux d’assainissement à réaliser.  
Malheureusement une des deux pompes a lâché et doit être remplacé. La société BP2E doit intervenir pour changer celle ci. Elle sera réimplantée dans la nouvelle chambre.

### Fermeture exceptionnelle de la déchetterie

La déchetterie du Val d’Ay fermera ses portes mardi 22 décembre à 16 heures, pour rouvrir lundi 4 janvier à 9 heures.

## Zoom

### Covid-19 : le point avant les fêtes

#### Le constat

Nous avons demandé à Cyrille Reboulet, infirmier, comment il avait perçu cette seconde période de confinement dans le village.

**Quelles différences avez-vous constatées à Lalouvesc entre la première et la seconde vague de la pandémie ?**

C.R. : Les gens ont pris beaucoup plus au sérieux la seconde vague,  mais ceci a été sans doute dû au fait qu’il y ait eu des cas très proches de la population louvetounnes. En effet, lors de la première vague notre village n’ayant pas été touché,  les gens avaient pris le confinement et les gestes barrières un peu à la légère. Ils voyaient les choses de loin.

**La population est-elle mieux sensibilisée ? Comment évaluez-vous son moral ?**

C.R. : Le fait d’avoir été touché de près à fait évoluer la mentalité. Malheureusement, il a fallu en arriver à des extrémités pour sensibiliser les gens. Mais maintenant,  je dois admettre que tout le monde fait de son mieux pour que “l’épidémie” recule,  et c’est le cas. Je prends pour référence le nombre de cas diagnostiqués ou présumés qui semble reculer. Ceci redonne du moral aux gens, notamment à l’approche des fêtes de fin d’année.

**Quels conseils peut-on donner pour que les fêtes de Noël se passent au mieux pour tous ?**

C.R. : Il va falloir rester vigilants et continuer à respecter les mesures barrières et sanitaires.  Mais les fêtes de fin d’année restent un moment en famille,  et il ne faudra pas non plus s’isoler complètement. MAIS RESTONS PRUDENTS !

J’en profite pour souhaiter de bonnes fêtes de fin d’année à toute la population de LALOUVESC. Profitez du moment présent,  mais restez vigilants et protégez-vous et protégez vos proches.

![Évolution des hospitalisations pour la Covid-19 en Ardèche comparée à la moyenne française](/media/covid-ardeche-10-12-2020.jpg)

On trouvera ci-dessus l’actualisation d’un graphique déjà publié dans un précédent bulletin. Rappel : ce ne sont pas des chiffres absolus d’hospitalisations, mais la comparaison de leur évolution entre la situation en Ardèche et celle de toute la France. La nombre d’hospitalisations en Ardèche est actuellement de 171.  
On constate sur le graphique que la seconde vague a touché plus durement l’Ardèche que la moyenne des autres départements, mais aussi que la baisse des hospitalisations est sensible, même si leur nombre est encore important. Aussi, comme le dit justement notre infirmier : restons prudents !

#### Nouvelles mesures

La période de confinement s’achèvera le 15 décembre, mais plusieurs mesures resteront en vigueur et d’autres viendront s’y ajouter. En voici un résumé :  
➔ Les déplacements entre régions seront désormais autorisés et les attestations de sortie ne seront plus nécessaires pendant la journée.  
➔ Un couvre-feu sera décrété sur l’ensemble du territoire métropolitain entre 20 heures et 6 heures du matin. Les déplacements seront interdits durant ces horaires sauf exception :  
– motifs professionnels,  
– motifs familiaux impérieux,  
– raisons médicales.  
➔ Le couvre-feu concernera également le réveillon du 31 décembre. **Il sera levé pour la soirée du 24 décembre.**  
➔ La fermeture des lieux accueillants du public est prolongée de 3 semaines. Cela concerne les musées, cinémas, salles de spectacle…  
**L’étape suivante sera pour le 20 janvier (si les conditions sanitaires le permettent)**

La Région Auvergne-Rhône-Alpes propose des tests gratuits de dépistage du Covid-19 du 16 au 23 décembre. Il n’y a pas besoin de rendez-vous, ni d’ordonnance, mais il faut présenter sa carte vitale. La liste des [centres de tests est accessible ici](https://www.auvergnerhonealpes.fr/actualite/936/25-covid-19-campagne-regionale-de-depistage.htm).

### Lalouvesc s’illumine

Cette année, Louvetous et Louvetonnes se sont retrouvés dans un seul et même élan pour décorer leur village de créations diverses, de couleurs et de lumières dans l’attente de Noël. Une envie de terminer cette année en beauté, en solidarité et en convivialité. En plus des traditionnelles guirlandes qui ornent la rue principale et des sapins disposés par la mairie, chacun et chacune, chez soi, en solo ou en petits ateliers, ont fait preuve d’imagination et ont dévoilé leur talent de créateur et d’ingéniosité.

Le parvis de la mairie ne manque pas de charme : un chalet ouvre ses fenêtres sur un aménagement intérieur qui réserve bien des « waouh », sapins et lumières sont du plus bel effet. Les commerçants ont décoré leur vitrine pour accueillir le chaland dans une ambiance festive et colorée. De nombreux bénévoles du Comité des Fêtes se sont investis dans des décorations originales. Le bois est à l’honneur. Ainsi : rennes, sangliers, lapins et autres chouettes sculptés règnent sur le square de la place du Lac. Les résidents de la maison de retraite n’ont pas démérité. Le bonhomme de neige qu’ils ont confectionné a fière allure.

Un arbre à souhaits vous attend et vous invite à venir faire un vœu et déposer une petite guirlande (mise à disposition) sur ses branches. Ainsi l’arbre scintillera et la légende dit que les vœux seront exaucés… Les plus petits ne sont pas oubliés puisque une boite aux lettres est à leur disposition. Et il est bien connu que les lettres postées à Lalouvesc arrivent en priorité sur le bureau du Père Noël ! Ils ont aussi décoré l’arbre de noël placé par la mairie dans la cour de l’école. Ils ont aussi étonné la directrice de l’Office du tourisme par leur créativité. Celle-ci fait aussi grand merci aux employés municipaux pour tous les sapins disposés dans le village.

Le sanctuaire n’est pas en reste. Pour la première fois, il est installé une grande banderole sur la Basilique souhaitant à toutes et tous un joyeux Noël, et surtout n’oubliez pas de gravir les escaliers et pénétrer dans la majestueuse Basilique pour admirer l’époustouflante et magnifique crèche.

Toute cette occupation pour ne pas dire agitation s’est déroulée dans le plus strict respect des conditions sanitaires actuellement imposées.

Si vous souhaitez flâner et retrouver la magie de Noël, Lalouvesc vous propose une balade hivernale des plus étonnantes. Vous en trouverez ci-dessous un aperçu.

Agnès Gemmiti

NDLR : les chutes de neige à répétitions ont ralenti les installations et les photographies.

![](/media/deco-noel.jpg)

![](/media/deco-noel-1.jpg)

![](/media/deco-noel-2.jpg)

![](/media/deco-noel3.jpg)

#### Le mot des Pères

_Comme vous pouvez le lire sur la banderole visible sur la façade de la basilique, la paroisse et les paroissiens souhaitent à tous les Louvetonnes et Louvetous un JOYEUX « NOËL, le cœur de Dieu bat pour le monde ! ». Avec notre foi à chacune, chacun, toutes et tous ont leur place, malgré ce confinement qui semble nous éloigner les uns des autres._

_Pour le sanctuaire, P Olivier de Framond_

La messe du 24 décembre débutera à 23h (il y a une dérogation pour le couvre-feu ce soir là). Le 25, vendredi jour de Noël la messe est à 10h.

### Envoyez des cartes de Noël et des bons vœux… pour les Louvetous

Ces diverses décorations ont été réalisées et posées par des bénévoles du village, des élus et les employés municipaux. Si vous n’êtes pas présent(e)s à Lalouvesc cette fin d’année, les photos témoignent aussi pour vous de la vitalité et de la joie de vivre qui y règne.

En retour, ce serait formidable si nous pouvions publier dans un prochain numéro du bulletin un témoignage inverse : des photos des amis du village dispersés en France et dans le monde. Si vous vous trouvez en dehors de Lalouvesc, envoyez-nous une photo des décorations de Noël du lieu où vous résidez présentement avec un petit mot sympathique ! Cela nous fera chaud au cœur et montrera à toutes et tous jusqu’où rayonne l’esprit de Lalouvesc ! Adresse pour les envois : mairie@lalouvesc.fr

## Histoire de Lalouvesc : épisode 2 – Le Moyen-Âge et du XIVe au XVIIe siècle

Suite de l’histoire de Lalouvesc par Pierre Roger. Cette semaine nous nous arrêtons au XVIIe siècle pour aborder dans le prochain bulletin les XVIIe et XIXe.

### Lalouvesc au Moyen-Âge

Durant le Moyen-Âge, Lalouvesc dépendait de la seigneurie puis de la baronnie de Mahun qui englobait les villages de St. Symphorien, Veyrines, St. Pierre des Machabées (devenu St. Pierre sur Doux) La Louvesc, Seray, Satillieu, Préaux, Vaudevant, Saint Jeure d’Ayet ainsi que la partie de la vallée de la Vocance située entre St Bonnet-le-Froid et Villevocance. Cinq grandes familles se sont succédé sur les terres de Mahun du XI au XVII siècle : Les Pagan, les Retourtour, les Tournon, les Lévy et les du Faure.

Lalouvesc eut une certaine notoriété locale sous le règne des Pagan, car les seigneurs de La Louvesc étaient en relation intime avec eux notamment Siméon de La Louvesc, juge du seigneur Guigue de Pagan en 1200 ; Jean de La Louvesc, juge des terres de Guillaume de Roussillon, seigneur d’Annonay en 1300, qui fit construire en 1338 un manoir adossé à la Tourelle primitive au lieu-dit Maison Claire ; Catherine de La Louvesc qui se fait dame Clarisse en 1320 et Pierre de La Louvesc qui accompagna à Rome le cardinal du Colombier allant couronner l’empereur Charles IV.

1179 : Le premier document authentique où il est question de La Louvesc date du 1er avril 1179. Il s’agit d’une bulle pontificale énumérant les biens de l’abbaye de Saint-Chaffre, située au Monastier-sur-Gazeille en Haute Loire. Cette puissante abbaye a possédé au Moyen Âge jusqu’à une soixantaine de dépendances en Vivarais, parmi lesquelles figure l’église d’Alaudiscum entre celle de Vérines (Veyrines) et de Saint-Pierre-des Macchabées (Saint-Pierre-sur-Doux). La Louvesc est donc la paroisse d’Alaudiscum. A noter qu’à cette époque, Lalouvesc est une paroisse et non une simple chapelle. L’église se trouvait au niveau du chœur actuel de la crypte de la Basilique.

Le Père de Curley , dans son ouvrage, indique qu’il a retrouvé huit membres d’une famille de gentilshommes portant le nom de « de La Louvesc » entre 1200 et 1400. Ils sont qualifiés de seigneurs dès l’an 1294. Leur maison était une maison forte avec tour et chapelle. La tour a été démolie et la chapelle a disparu. Cette maison était appelée la Maison Claire (peut-être primitivement la maison des clercs). Il semble que ce puisse être la Maison-Claire actuelle, cf. photo vers 1915 (Caillet & Mathieu, bul. de l’Alauda n°3) ?

Les armes de la famille étaient « d’argent au chef d’azur chargé d’une fleur de Lys ».Les trois premiers de la lignée s’appelaient tous Siméon. Siméon I n’avait pas de profession connue, Siméon II était juge des terres de Guignes Pagan et Siméon III était curé de Vocance. Viendra ensuite Jean qui était juge des terres et intendant de la Maison de Guillaume de Roussillon, seigneur d’Annonay. Avec son fils Pierre il accompagna en 1355 le Cardinal Bertrand au sacre de l’empereur germanique Charles IV. Pierre de La Louvesc sera armé Chevalier du Saint-Empire. Le texte de son serment a été conservé : « Je, noble Pierre de La Louvesc, chevalier bachelier de la Couronne de France et chevalier du Saint Empire, promet par la foy de mon corps, comme loyal chevalier, de défendre et de protéger la Sainte Église romaine et le Saint Empire contre tous leurs ennemis, voulant être tenu pour faux, mauvais et déloyal chevalier, parjure et foy menteur, si je fais le contraire. Ainsi Dieu m’aide en ces saints Évangiles, que je touche ». La Maison de La Louvesc s’éteignit vers 1400 alors que s’éteignait Guigues V de Pagan, le dernier des Pagan. Lalouvesc eut ensuite pour seigneurs les « de Beauchastel », puis les « de Tournon » vers 1600.

1312 : Une maison à Lalouvesc appelée « l’hôpital » et le mas de la Vialette sont donnés par Hugo de Paganis aux Chevaliers de Saint-Jean de Jérusalem de la commanderie de Sainte-Épine-lès-Tournon, située sur la commune de St. Jean de Muzols. L’ordre de Saint-Jean de Jérusalem, appelé aussi ordre des Hospitaliers, est un ordre religieux catholique, hospitalier et militaire qui a existé de l’époque des croisades jusqu’au début du XIXe siècle.

### Du XIVe au XVIIe siècle

Au XIV siècle, il est probable que Lalouvesc n’est qu’un ensemble de quelques maisons. Sur la carte présentée dans « Mentalités nobiliaires en Vivarais au XIVe siècle : la succession de Briand de Retourtour » par Marie-Claire CHAVAROT on s’aperçoit que La Louvesc ne figure pas en tant que village mais uniquement en tant que « Col de Lalouvesc ».

1603 : Sous le gouvernement des de Tournon, les jésuites du Puy prirent possession des anciens prieurés à la place des Bénédictins. Dès 1603, les Pères jésuites étaient seigneurs de Veyrines et patrons des églises de Saint-Pierre, de Lalouvesc et de Veyrines (Poidebard R. 1928. L’église de Veyrines : XIIe siècle. Aubenas : C. Habauzit. 64 p).

1621 : Guerres de religion. L’église de Lalouvesc est pillée par les troupes protestantes (Mathieu, Alauda n°2)

1640 : Un pays dépeuplé. Laissons la parole au Père de Curley_. En l’année 1640, Lalouvesc dépend de Tournon. La maison forte des de La Louvesc appartient à noble Gilles de la Franchière, seigneur de Girodon. On compte comme principaux propriétaires, noble Guillaume de Julliens, seigneur de Rocheume et Jean Buisson de la Grange Neuve, dont la famille subsistait déjà à Lalouvesc depuis plusieurs siècles. La commanderie de Saint Epine-les-Tournon possédait encore la maison appelée la maison de l’hôpital et des propriétés. Cinquante-quatre autres familles se partageaient le territoire. Une trentaine au moins n’habitaient pas le pays. Le plus grand nombre étaient dispersés dans les hameaux dépendants du village. Quant au village proprement dit, les témoins qui déposèrent dans les enquêtes ecclésiastiques sont unanimes à lui donner trois ou quatre maisons outre l’église. C’était une véritable décadence. Dans les temps anciens La Louvesc avait été paroisse mais dès l’an 1602 était devenue simple annexe de Verrines qui à son tour, comme n’ayant plus aucune importance, avait été annexé au prieuré de Macheville (Lamastre). Le pays se dépeuplait

\**1640 :**Mission et mort du R.P. Régis à Noël à La Louvesc.

La Louvesc ne s’est véritablement développé qu’à partir du 31 décembre 1640 quand François Régis apôtre du Vivarais et du Velay (1597–1640) vint mourir dans le hameau qui ne comptait alors qu’une chapelle et « _quatre feux_ ». Il était envoyé par les Pères Jésuites pour évangéliser les populations vivaroises. Il partit du Puy à pied, le 22 décembre et alla coucher à Raucoules, village située à l’est du Velay, dans le département de la Haute-Loire. Le 23 décembre, il repartit et s’égara à la nuit tombante aux environs de La Louvesc. Il atteignit Vérines à bout de force et passa la nuit dans une masure. Il arriva le 24 décembre à La Louvesc, fiévreux, et mourut le 31 décembre. Le lendemain de sa mort, il fut proclamé saint dans tous les pays qu’il avait évangélisés. « _Des miracles, des guérisons, s’opérèrent alors à La Louvesc et dans tout le Vivarais_ ».

Jean-François Régis était né Fontcouverte en Languedoc en 1597 (d’où le jumelage de Lalouvesc avec ce village). Après des études au Collège de Béziers, il était entré dans la Compagnie de Jésus à l’âge de 19 ans. Son nom de famille est devenu grâce à lui un prénom.

## Suivi

### Votre avis sur l’aménagement de l’espace de jeux du Val d’Or

Sauf imprévu, la démolition de l’hôtel Beauséjour sera effective avant l’été. Cette démolition modifiera sensiblement l’aspect du quartier en ouvrant une perspective nouvelle sur le parc du Val d’Or, l’aire de jeu et plus largement sur les Cévennes.  
Jusqu’à présent, l’aire de jeu n’était visible et accessible que depuis la rue partant de la Mairie (rond rouge) sur la carte, demain la suppression du bâtiment insalubre (croix rouge) changera radicalement la donne. Que l’on vienne de la Basilique ou de la Mairie, l’aire de jeu sera visible depuis le centre du village.

Dès lors, le parc déjà très fréquenté dès les beaux jours prendra une dimension nouvelle. Comme nous l’avons souvent répété et exposé notre première idée serait de profiter de cette opportunité pour y ériger un jeu-monument. Le CAUE de l’Ardèche a été mandaté pour en étudier la faisabilité. Les responsables de l’étude souhaitent mieux comprendre les pratiques des habitants, pour déterminer le périmètre du futur projet de jeu-monument et l’insérer dans les usages actuels et à venir.

Pour les aider à avancer, ils vous proposent de répondre à quelques questions. C’est simple et rapide. Il est important que le maximum d’entre vous prennent quelques instants pour répondre, que vous soyez ou non présents actuellement dans le village.

[Le questionnaire est ici (cliquer)](https://framaforms.org/lalouvesc-le-parc-du-val-dor-et-les-espaces-publics-1607613577).

![](https://i2.wp.com/www.lalouvesc.com/wp-content/uploads/2020/12/Plan-Val-dor.jpg?resize=768%2C783&ssl=1)

### 2020, une année particulière, mais importante pour l’aide à domicile en milieu rural (ADMR)

Extraits d’un document de l’ADMR nord-Ardèche :

La crise ne nous a pas épargnés. Nombreux de nos intervenants ont été touchés pas la COVID et bon nombre de nos bénéficiaires nous ont quittés. En cette période de fin d’année, nous pensons bien à eux et à leur famille. Malgré cette pandémie, l’ADMR a poursuivi sa mission et est restée au plus près de tous ses bénéficiaires. Nos intervenants ont assumé les tâches qui nous sont dévolues : accompagnement de la personne âgée ou fragilisée, entretien courant du cadre de vie (ménage, repassage), livraison de repas, garde d’enfant, … (…)  
Les activités du Club ADMR ont été maintenues dans la mesure du possible. Activité physique adaptée, art thérapie, action D’marche, atelier autour des plantes, ont été proposés et ont permis à nos ainés de sortir de leur isolement et de partager des moments de convivialité. Des activités à distance ont également été mises en place grâce à notre partenariat avec Happyvisio site de web-conférence santé et prévention destiné aux seniors. L’ADMR a signé un accord avec cette plateforme afin de donner l’accès gratuitement aux seniors ardéchois à un programme d’activités ludiques et culturelles en visioconférence.

#### Un nouveau Centre de santé à Annonay

L’ADMR a ouvert son Centre de santé dans les anciens locaux de la clinique des Cévennes à Annonay. Depuis le 1er avril, une équipe de quatre infirmières arpente les routes pour dispenser les soins aux personnes malades. Une infirmière est toujours présente au pôle des Cévennes et reçoit toutes les personnes de 0 à 110 ans, bénéficiaires de l’ADMR ou non, pour dispenser des soins courants : prise de sang, injections, vaccins, pansement, …. N’hésitez pas à prendre contact avec elle pour tout rendez-vous au 04.75.33.77.27 ou en vous inscrivant sur doctolib.fr.  
Depuis le 12 mai une équipe pluridisciplinaire reçoit sur rendez-vous tout patient au pôle des Cévennes. (Doctolib ou 04.75.33.77.30 pour toute prise de rendez-vous).

* Le docteur Thomas Bonnard – Médecin Généraliste,
* Le docteur Pierre Yves Fayolle – Médecin du sport,
* Marie Barral-Baron et Margaux Legrand – Sages-femmes,
* France Drevon – Kinésithérapeute,
* Laura Lhoussaine – Orthophoniste,
* Nathalie Runner – Psychomotricienne,
* Aude Di Bin – Naturopathe  
  Et dès janvier 2021 deux médecins rejoindront le centre ainsi qu’un acupuncteur.

### Fin du reconfinement, on repasse au bulletin mensuel

La période du reconfinement due à la deuxième vague de la pandémie se termine le 15 décembre. Nous allons donc réduire le rythme de parution de ce bulletin et revenir à une parution mensuelle. Le prochain numéro paraîtra pour la nouvelle année 2021 dont on espère qu’elle sera meilleure que 2020.  
Si l’on s’en tient au seul critère de la consultation, il semble que le passage à l’hebdomadaire pour la période de confinement a été apprécié. Nous espérons que ces numéros de “l’hebdo-confinement” vous ont plu, intéressé et et qu’ils ont contribué à resserrer les liens dans le village et avec les amis du village. Si vous souhaitez lire ou relire un des six numéros hebdomadaires publiés durant le second confinement, vous trouverez les liens ci-dessous :  
[Bulletin Hebdo-confinement n°1 – 7 novembre 2020. S’adapter](https://www.lalouvesc.com/2020/11/07/bulletin-hebdo-confinement-n-1-7-novembre-2020/)  
[Bulletin Hebdo-confinement n°2 – 14 novembre 2020. Actifs et solidaires](https://www.lalouvesc.com/2020/11/14/bulletin-hebdo-confinement-n2-14-novembre-2020/)  
[Bulletin Hebdo-confinement n°3 – 21 novembre 2020. Habiter à Lalouvesc](https://www.lalouvesc.com/2020/11/21/bulletin-hebdo-confinement-n3-21-novembre-2020/)  
[Bulletin Hebdo-confinement n°4 – 28 novembre 2020. Embellir](https://www.lalouvesc.com/2020/11/28/bulletin-hebdo-confinement-n4-28-novembre-2020/)  
[Bulletin Hebdo-confinement n°5 – 5 décembre 2020. Un village en forêt](https://www.lalouvesc.com/2020/12/05/bulletin-hebdo-confinement-n5-5-decembre-2020/)

### Courrier des lecteurs

Nous avons reçu ce courrier :

Découverte improbable : peinture « Mort de saint Francois Régis à Lalouvesc » qui ne se trouve ni à Lalouvesc, ni même en Ardèche, mais dans une grande chapelle parisienne du XIXe siècle, Saint Ignace. Cordialement. André Gauthier

## Les enfants de Lalouvesc sont des artistes ! (puzzle)

Merci aux enfants de l’école St Joseph qui ont fait un magnifique cadeau de Noël à la Mairie ! Vous le découvrirez ci-dessous.