---
title: Bulletins (2021 - ...)
date: 2020-05-15
description: Retrouvez tous les bulletins d’information
subtitle: Les bulletins d’information
icon: "\U0001F4F0"
weight: 2
header: "/media/entete-bulletin.jpg"
menu:
  main:
    identifier: bulletins-information
    parent: Vie municipale
    weight: 1

---
Depuis juillet 2020, la Commune publie un Bulletin mensuel en ligne. Quelques exemplaires sont tirés sur papier pour les habitants non-connectés. En période de crise (ex. confinement), la périodicité est hebdomadaire.