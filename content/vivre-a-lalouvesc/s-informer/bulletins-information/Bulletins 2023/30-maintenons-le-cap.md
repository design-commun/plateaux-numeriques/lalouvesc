+++
date = 2022-12-27T23:00:00Z
description = "Bulletin d'information de Lalouvesc n°30 janvier 2023"
header = "/media/entete-logo-hiver.png"
icon = ""
subtitle = "n°30 janvier 2023"
title = "30 - Maintenons le cap"
weight = 1

+++
Maintenir le cap, c'est faire aussi bien, sinon mieux, en 2023 qu'en 2022, année de crises en Europe et dans le monde mais pourtant année faste pour le village. Quelques étapes de 2022 sont rappelées dans ce Bulletin par des photos. Vous y trouverez aussi les étonnantes réalisations de l'école, le programme 2023 des principales associations, la date de la fête du centenaire de l'Abri du pèlerin, des invitations à la balade, une carte des loyers, bien sûr des vœux pour la nouvelle année... et encore, comme toujours, bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier](/media/bulletin-de-lalouvesc-n-30-1er-janvier-2023.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du maire

C’est avec une grande joie que Lalouvesc a fêté Noël tout au long du mois de décembre. Illuminations et décorations égayent le village, je remercie de tout cœur tous les bénévoles impliqués dans cette réalisation.

Les enfants de l’école ont été rassemblés à la mairie pour le goûter de Noël, grands et petits ont pu passer un moment très chaleureux.

La traditionnelle crèche des santons a été installée dans la basilique, nous invitons tout le monde à venir la découvrir.

Je remercie le service technique, le secrétariat de mairie, le conseil municipal pour leur implication toute cette année au sein de notre commune, l’avenir du village en dépend, ils maintiennent constamment leur travail sans relâche.

Je tiens aussi à remercier les Louvetous, pour leur amour du village que je partage, c’est avec beaucoup de satisfaction que j’ai passé cette année 2022 à vos côtés.

Je vous souhaite une bonne fête de fin d’année et je vous donne rendez-vous le 15 janvier à 17h30 au centre d’action communale pour célébrer ensemble la nouvelle année 2023 !

Jacques BURRIEZ, Maire de Lalouvesc

## Actualités de la Mairie

### Les vœux du Maire

![](/media/voeux-2023-lalouvesc.png)Rendez-vous le **dimanche 15 janvier à 17h30 au CAC** pour la présentation des vœux du maire à toutes les Louvetonnes et les Louvetous. Vous êtes toutes et tous invités. La Lyre nous proposera un peu de musique et nous tirerons les rois ensemble.

A cette occasion, l'équipe municipale présentera les réalisations de l'année 2022 et les projets pour les années à venir, les réussites comme les difficultés. Et vous pourrez aussi vous exprimer.

Nous reprenons ainsi une tradition qui avait été malheureusement interrompue par la pandémie.

### Réunion publique sur l'avenir de Ste Monique et du Cénacle

Notez sur vos agendas : les responsables du SCOT Rives du Rhône viendront à Lalouvesc présenter les premiers résultats et les premiers scénarios envisagés pour les deux sites, Ste Monique et le Cénacle, le **mardi 28 février à 18h (réunion reportée)**.

Il ne s'agit que d'un point d'étape. Le rendu final de l’étude est prévu pour mai 2023. Les travaux se poursuivent. Les élus sont associés à des comités de pilotage, des visites, et des rencontres avec de potentiels porteurs de projet.

### Babets ou repas festif

Comme chaque année, un courrier a été envoyé aux seniors pour un cadeau de fin d'année. Cette année, comme les mesures sanitaires sont moins rigoureuses, il leur est demandé de choisir entre un repas festif ou des Babets (bons d'achat dans les magasins locaux).

![](/media/babet-2023.png)

### La CCVA sur Panneau Pocket

La Communauté de commune du Val d'Ay s'est dotée d'un nouvel outil de communication, une application appelée [Panneau Pocket ](https://app.panneaupocket.com/ville/326549431-cc-du-val-day-07290)que vous pouvez télécharger sur votre smartphone, si vous en avez un, vous recevrez ainsi les alertes d'actualité de la CCVA. D'autres communes du territoire utilisent aussi cette application et il est possible de s'y abonner.

Nous sommes en réflexion pour la rendre disponible sur Lalouvesc. Jusqu'ici suite à nos travaux avec les designers de _Plateaux numériques_, nous avons privilégié les actualités sur le site web avec l'envoi aux abonnés des actualités dans leur boîte mails tous les lundis matin. Les taux de consultations sont très satisfaisants et les retours recueillis par notre enquête sont très positifs. A l'évidence une communauté d'information s'est constituée. Il faut alors savoir utiliser les nouveaux outils disponibles sans rompre les habitudes et disperser le lectorat, ni épuiser les éditeurs.

Rappel les [statistiques du site web](https://simpleanalytics.com/lalouvesc.fr?period=day&count=1) sont toujours disponibles en temps réel. Ci-dessous un focus sur le Bulletin.

#### Lecture du Bulletin en 2022

![](/media/lecture-du-bulletin-mensuel-en-2022.png)Les chiffres ont été recueillis au 15 décembre, le dernier numéro est donc désavantagé.

Deux Bulletins ont été lus plus de 800 fois, sept entre 600 et 750 fois, trois entre 400 et 600 fois. Le temps médian de lecture est compris entre 2mn 14s et 3mn 40s selon les numéros, cela signifie que plusieurs centaines d'internautes prennent largement le temps de lire les Bulletins.

Sachant qu'en plus une vingtaine d'exemplaires sont distribués sur papier chaque mois pour les personnes non connectées et que la population du village est d'environ 390 habitants, ce sont d'excellents chiffres.

### Très bientôt le recensement

Rappel : le recensement du village démarrera officiellement le 19 janvier. Des prospectus expliquant la procédure seront distribués dans les boîtes aux lettres la semaine précédente.

Toutes les informations ont été données dans le[ Bulletin du mois de décembre](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#le-recensement-se-rapproche).

![](/media/recensement-2023-ou.jpg)

Le recensement permet de compter les habitants du village. Voici son évolution depuis un siècle-et-demi.

![](/media/evolution-de-la-population-2022.png)

Le défi pour tous les Louvetous est de renverser la tendance en rendant le village de nouveau attractif !

## ZOOM : D'une année à l'autre

### Un mois, une image

Il s'est passé vraiment beaucoup de choses dans le village cette année 2022. Ce fut une très belle année. Nous avons sélectionné une image par mois avec un lien pour en donner un aperçu. Mais cet aperçu reste très partiel. Vous pouvez aussi retrouver tel ou tel événement dans l'un ou l'autre des [douze Bulletins mensuels](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/).

{{<grid>}}{{<bloc>}}

#### Janvier 2022

![](/media/demolition-beausejour-2022-53.jpg)

[L'album de la démolition de l'hôtel Beauséjour](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition-deuxieme-semaine/)

Voir aussi : [l'histoire de l'hôtel Beauséjour](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#disparition-dun-h%C3%B4tel-de-l%C3%A9gende)

{{</bloc>}}{{<bloc>}}

#### Février 2022

![](/media/jeu-monument-ecole-st-joseph-2022-1.jpg)

![](/media/jeu-monument-ecole-st-joseph-2022-8.jpg)

[Le projet de jeu-monument vu par les enfants de l'école](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#des-id%C3%A9es-pour-le-jeu-monument)

{{</bloc>}}{{<bloc>}}

#### Mars 2022

![](/media/s-roussel-1-carrefour-des-arts-2022.JPG)

[Sélection des artistes de la saison 2022 du Carrefour des Arts](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#une-nouvelle-ambition-pour-le-carrefour-des-arts)

Voir aussi [la présentation des artistes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#carrefour-des-arts--demandez-le-programme-).

{{</bloc>}}{{<bloc>}}

#### Avril 2022

![](/media/ronde-louvetonne-2022-8.jpg)

[La Ronde louvetonne sous la neige](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/un-paysage-enneige-pour-la-ronde-louvetonne/)

{{</bloc>}}{{<bloc>}}

#### Mai 2022

![](/media/decoration-ardechoise-2022-19.jpg)

[En attendant l'Ardéchoise, les décorations sont prêtes...](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#lard%C3%A9choise-bicycle-et-recycle)

Voir aussi : [Recyclomania](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/24.amenager-pour-accueillir/#recyclomania-au-comit%C3%A9-des-f%C3%AAtes)

{{</bloc>}}{{<bloc>}}

#### Juin 2022

![](/media/exercice-pompiers-ecole-2022-2.jpg)

[Les pompiers à l'école St Joseph](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/24.amenager-pour-accueillir/#r%C3%A9fl%C3%A9chir-%C3%A0-lengagement-des-pompiers-volontaires)

Voir aussi : le bilan de la [saison des pompiers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#porte-ouverte-chez-les-pompiers)

{{</bloc>}}{{</grid>}}

#### Juillet 2022

<iframe width="560" height="315" src="https://www.youtube.com/embed/JzkuzJzZNHs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[Le feu d'artifice du 13 juillet 2022](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vous-avez-rate-le-feu-d-artifice-ou-vous-voulez-le-revoir/)

#### Août 2022

![](/media/15-aout-2022-photo-ot-2.jpg)

[Un 15 août sous le soleil](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#un-15-ao%C3%BBt-sous-le-soleil)

{{<grid>}}{{<bloc>}}

#### Septembre 2022

![](/media/brocante-2022-2.jpg)

[Beau temps et affluence record à la brocante](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beau-temps-et-affluence-record-pour-la-brocante-2022/)

{{</bloc>}}{{<bloc>}}

#### Octobre 2022

![](/media/trail-2022-depart-4.jpg)

[1.200 sportifs au Trail des Sapins](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#le-comit%C3%A9-des-f%C3%AAtes-fait-des-%C3%A9tincelles)

Voir aussi la [vidéo du Trail 2022](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/video-du-trail-des-sapins/)

{{</bloc>}}{{<bloc>}}

#### Novembre 2022

![](/media/espace-beausejour-22-09-08-p2-aps-ind-2.jpg)

[L'espace Beauséjour est dessiné](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#lespace-beaus%C3%A9jour-est-dessin%C3%A9)

{{</bloc>}}{{<bloc>}}

#### Décembre 2022

![](/media/decoration-noel-2022-11.jpg)

[Les décorations de Noël sont installées](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-decorations-de-noel-ont-ete-installees/)

{{</bloc>}}{{</grid>}}

### Vœux

Une pensée de FOS SUR MER pour LALOUVESC où nous ne vivons que quelques mois par an.

Nous souhaitons à toutes les LOUVETONNES et à tous les LOUVETOUS et en particulier aux habitants du quartier CHANTE OSSEL de bonnes fêtes de fin d’année et tous nos meilleurs vœux pour 2023.

DENAERDT Noëlle et Frédéric

***

Nous souhaitons à tous les lecteurs de ce sympathique bulletin d'informations sur La Louvesc de très belles fêtes de Noël en famille, et une excellente année 2023, avec, nous l'espérons tous, un retour de la paix en Europe.  
Ce précieux bulletin nous permet de vivre à distance les importants progrès réalisés par toute l'équipe municipale, que nous remercions vivement de nous tenir informés des projets et de la vie de notre cher village. Une réelle dynamique est en marche sous l'impulsion de Monsieur le Maire et de ses adjoints, malgré les difficultés imprévues rencontrées.

Enfin, nous tenons à nous associer de tout cœur avec tous ceux qui ont perdu cette année un proche dans leur famille, et qui pour certains reposent en paix dans ce beau cimetière. Fidèlement avec les Louvetous !  
La famille CHAVANES

***

![](/media/noel-a-chiloe-2022-1.jpg)

Quelques crèches de l'île de Chiloe de nos envoyés spéciaux au Chili : Brigitte et Michel Bober.

## Sanctuaire

### Le centenaire de l'Abri du pèlerin

L'AG de l'Abri du pèlerin du lundi 12 décembre a décidé de la date de **la fête des 100 ans** : les **8 et 9 juillet 2023.** Voir l'histoire de sa fondation par le père Pierre Bonnard sur [le site de l'Abri](http://www.abridupelerin-lalouvesc.fr/historique/).

![](/media/abri-du-pelerin-carte-postale-2022.jpg)

Tous les Louvetous y sont invités. Nous faisons aussi appel, avec l'Office du tourisme, aux villageois qui auraient des photos historiques liées à la vie de l'Abri (cf. [article sur la page Facebook](https://www.facebook.com/OTValDAy/posts/pfbid0roEiDAF2xkyp89j76cnviCDLPFXnb3RPNQyZoaz26itktp1FNkCiLG1xFNXQKEDGl) de l'OT), pour monter une petite exposition.

Je serai aussi heureux si des villageois venaient me raconter quelques histoires vécues à l'Abri....

L'association du Cinéma y participera, la troupe du Théâtre de la Veillée et la Lyre louvetonne, aussi j'espère.

Yves Stoesel

## École Saint Joseph

Pas de feuilleton ce mois, l'épisode 2 des aventures de Guli est prévu pour le mois de février.

### Des enfants entreprenants

La deuxième période de l'année scolaire est passée à toute vitesse. Les apprentissages sont allés bon train et les six semaines ont été ponctuées de moments forts.

#### Le 11 novembre

D'abord, nous avons élaboré en classe une fresque pour la commémoration du 11 novembre. Ce fut l'occasion d'échanger autour de l'Armistice. Certains enfants ont assisté à la [commémoration organisée par la commune](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/11-novembre-le-village-se-souvient/), ont déposé la fresque collective aux pieds du monument aux Morts et ont lu un petit texte.

#### Une crèche originale

![](/media/creche.jpg)

Puis, nous avons souhaité participer au concours de crèches organisé par l'association « les amis de Notre Dame d'Ay ». Il était indispensable d'être original et créatif. Les idées ont été nombreuses. Mais les élèves se sont finalement mis d'accord sur la fabrication d'une crèche totalement comestible ! Construction de l'étable en gaufrettes, personnages en pâte sablée, sapins en meringue, toit en langues de chat... Les élèves s'en sont donnés à cœur joie et étaient très fiers du résultat.

{{<grid>}}{{<bloc>}}

![](/media/ecole-dec-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecole-dec-2022-9.jpg)

{{</bloc>}}{{</grid>}}

Fin du concours et résultat début janvier.

#### Saynettes à la Basilique

A chaque période scolaire, l'équipe pédagogique a fait le choix de banaliser une journée. Celle-ci est destinée à un temps pastoral. Cette période-ci, nous avons donc travaillé sur l'élaboration de la célébration de Noël. Au programme : chants en français et en anglais, saynètes sur le premier arbre de Noël, lecture de texte, présentation de la crèche de la Basilique... Nous remercions chaleureusement le Père Michel qui nous a beaucoup épaulé dans la conception des activités proposées lors de cette journée.

![](/media/ecole-dec-2022-8.jpg)

#### Une alimentation équilibrée

Enfin, les élèves de l'école ont pu prendre part à un petit-déjeuner partagé. Les élèves de cycle 3 ont travaillé tout au long de la période sur le thème de l'alimentation. Familles d'aliment, équilibre de l'alimentation, rôles d'une alimentation équilibrée sur l'organisme... Ce petit projet servait à donner du sens aux apprentissages mais a également donné l'occasion aux élèves plus grands de sensibiliser les plus jeunes à l'importance de la prise régulière d'un petit-déjeuner équilibré.

{{<grid>}}{{<bloc>}}

![](/media/ecole-dec-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecole-dec-2022-7.jpg)

{{</bloc>}}{{</grid>}}

Les enfants sont donc partis pour 15 jours de vacances et reprendront les apprentissages dès le mardi 3 janvier.

De la part de toute l'équipe éducative de l'école privée Saint Joseph, de belles fêtes de fin d'année à toutes les familles et à tous les lecteurs.

A l'année prochaine !

### Tombola, calendrier et fromage à l'APEL

Vous trouverez toujours des billets de tombola et des calendriers des enfants chez nos commerçants : boulangerie, bar du Lac, Vival et pharmacie, et auprès des enfants de l’école.

Eh aussi oui ! L'APEL se lance dans la revente de fromages, de vache entre autres, venant d'autres régions afin que nos papilles en profitent !

Merci de remettre au plus tard votre commande le 31 janvier 2023 dans la boîte aux lettres de l’école Saint Joseph à LALOUVESC. Le règlement se faisant par chèque à l’ordre de l’APEL (les chèques seront encaissés après livraison).

Les sommes recueillies payent les activités des enfants de l'école.

[Bon de commande](/media/bon-de-commande-fromage-apel-2023.pdf)

## Culture - Loisirs

### On prépare l'année 2023...

On ne s'ennuiera pas à Lalouvesc en 2023. Retrouvez les événements programmés sur le [site du village](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/manifestations-et-evenements-a-l-affiche-2023/). Voici comment ils se préparent.

#### ... au Carrefour des Arts...

Si la vie du village est bien calme pendant l'hiver, c'est à cette période que le Carrefour des Arts concocte sa programmation.

Son objectif cette année est de couvrir tout l'été d'événements culturels avec _Les estivales du Carrefour des Arts._

![](/media/carrefour-des-arts-installation-2022-1.jpg)

Du côté arts plastiques, nous retrouverons bien évidemment la grande exposition au CAC pour laquelle **cinq artistes** ont déjà été retenus. Une nouveauté dans le "grenier des peintres" qui sera occupé par des **artistes émergents** issus de l'ESADSE (Ecole supérieur d'art et de design de Saint-Étienne). Ils nous feront découvrir dans différentes techniques les approches les plus contemporaines en matière d'art et de design.

L'**exposition photo** en extérieur sera aussi reconduite dès le mois de mai. Les discussions avec la mairie sont en cours afin d'arrêter un nouveau site compte tenu du chantier sur l'espace Beauséjour. Si vous avez des idées dites-le nous. Le photographe a été choisi... peut-être le croiserez-vous bientôt en repérage dans le village.

Le Carrefour des arts est également en relation avec la maison Saint-Régis pour établir une synergie entre ses événements et l'**exposition de la chapelle Saint-Ignace**.

Enfin le Carrefour espère ouvrir cet été 2023 son musée numérique "[**Micro-Folie**](https://www.culture.gouv.fr/Demarches-en-ligne/Par-type-de-demarche/Appels-a-projets-candidatures/Deploiement-de-Micro-folies)" à Lalouvesc et organiser quelques événements et installations sur l'espace public afin d'offrir aux habitants et aux touristes une large palette d'accès à la culture sur le village.

![](/media/carrefour-voleurs-de-swing-2022.jpg)

Du côté musique, nous aurons aussi rendez-vous avec une **semaine de musique classique** autour du 20 juillet, et des **soirées jazz** reparties dans la saison. A noter également, un **opéra choral** prévu le 30 juillet à la basilique. Mais tout cela est encore en cours de montage.

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-8.jpg)

{{</bloc>}}{{<bloc>}}

Nous vous rappelons par ailleurs que nous organisons tous les mois des **visites** de musées ou d'atelier d'artistes. Les adhérents du Carrefour en sont informés par mail. Si vous n'êtes pas encore adhérent, vous pouvez aussi repérer ces sorties en suivant les actualités du Carrefour des Arts sur son site [www.carrefourdesarts-lalouvecs.fr](http://www.carrefourdesarts-lalouvecs.fr).

La prochaine sortie est prévue le **28 janvier au musée d'Art contemporain de Saint-Étienne** autour de la remarquable exposition Gabrielle Kahn.

{{</bloc>}}{{</grid>}}

**Nous recherchons aussi des bénévoles. Venez vous joindre à nous pour vivre cette belle aventure. Bonne et  belle année 2023 à toutes et tous !**

N’hésitez pas à nous contacter : contact@carrefourdesarts-lalouvesc.fr

Jacques Morel, vice-président

#### ... au Comité des fêtes...

{{<grid>}}{{<bloc>}}

Le froid n’arrête pas un bénévole ! Encore moins une équipe vous pensez bien !!! Les décorations de Noël sont la dernière action de l’année menée par le Comité des Fêtes.

C’est fait, le square est décoré ! Merci et bravo à celles et ceux qui ont œuvré pour que la magie de Noël nous emmène parmi couleurs et lumières.

A présent un peu de repos sur le terrain. Mais nos réflexions vont bon train pour préparer l’année 2023. Voici ci-dessous ce que nous envisageons :

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-2.jpg)

{{</bloc>}}{{</grid>}}

⇒ le **16 avril LA RONDE LOUVETONNE** avec le retour d’un concert musical l’après-midi : Pascal VEYRE (_Pascal chante Johnny_).

⇒ le **03 septembre** fêtera la 20ème édition de **LA BROCANTE**. Cet anniversaire sera grandement fêté.

⇒ le **28 octobre** **LE TRAIL DES SAPINS** proposera une nouvelle fois quatre parcours inédits.

Nous souhaitons inclure dans nos projets un concert à la basilique au mois de mai. Un autre au mois de juillet au Parc des Pèlerins. Ces deux dossiers n’en sont encore qu’aux premiers balbutiements. À suivre !

Bien évidemment le square sera décoré pour le passage de l’Ardéchoise et saluera ainsi tous les cyclistes et visiteurs tout l’été. Et quand décembre sera venu les décorations de Noël remplaceront celles mises en place pour Halloween. En fait c’est un peu notre terrain de jeux ce square…

Nous vous souhaitons une bonne et heureuse année.

Sachez toutes et tous que vous serez les bienvenus au sein des bénévoles du Comité des Fêtes.

N’hésitez pas à nous contacter :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G., secrétaire

#### ... au Club des deux clochers...

Le Club des deux clochers vous propose de participer à ses activités pour 2023.

* Des séances de **gymnastique** débuteront le 24 février de 11h à 12h
* Des initiations à l'**informatique** animées par un professionnel
* Tout au long de l'année, vous pourrez vous initier aux techniques des **arts créatifs**
* Des **ateliers de mémoire** sont organisés hebdomadairement afin de faire travailler les neurones endormis

Pour ces activités, contacter le Président Georges IVANEZ 06 62 00 45 69, georges.ivanez@orange.fr

![](/media/gateau-club-2-clochers-2022.jpg)

Tout au long de l'année, le Club réunit ses adhérents le jeudi après-midi, où chacun peut se divertir en oubliant l'espace d'un moment ses tracas de la vie quotidienne.

* jeux de cartes, de société, ... et même bavardage,
* billard,
* Le goûter interrompt les parties, même les plus acharnées.

Quelques jours festifs ponctuent la vie du Club :

{{<grid>}}{{<bloc>}}

![](/media/mardi-gras-club-des-2-clochers-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

* la galette des rois,
* le mardi-gras,
* l'échange avec le Club de Rochepaule nous rapproche par dessus le Doux,
* un voyage annuel en fin de printemps nous fait découvrir d'autres horizons, d'autres cultures, d'autres cuisines... ,
* dans la grisaille de l'automne, nous fêtons l'arrivée du beaujolais nouveau,
* et la bûche de noël vient clore l'année civile en prémices aux agapes de la trêve des confiseurs.

{{</bloc>}}{{</grid>}}

Le Club participe et anime la vie du village

* organisation d'un concours de belote,
* exposition estivale,
* participation au Téléthon.

**Vous qui avez une image vieillotte de notre association de retraités, testez l'ambiance du Club en venant prendre le goûter avec nous un jeudi au Centre d'Animation Communal !**

#### ... au Cinéma Le Foyer...

{{<grid>}}{{<bloc>}}

![](/media/cinema-le-foyer.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/frequentation-cinema-2022.png)

{{</bloc>}}{{</grid>}}

La saison 2022 a enregistré près de 2.600 spectateurs, au cours de 40 séances sur un total de 27 films : une fréquentation en nette hausse avec une programmation de qualité. La fréquentation moyenne par séance a été de 65 spectateurs. Tiercé gagnant : _Les folies fermières_ (211 entrées, deux séances), _Les volets verts_ (avant-première 182 entrées sur une seule séance), _L'école du bout du monde_ (173 entrées, deux séances). La baisse due au Covid a été rattrapée beaucoup plus vite à Lalouvesc que dans le reste des salles françaises.

Cette activité sera poursuivie l'année prochaine afin de satisfaire un plus grand nombre de cinéphiles encore si possible, toujours dans un souci de qualité et une participation au centenaire de l'Abri du pèlerin. Courant 2023, outre la modernisation des équipements, notamment de caisse informatique afin de satisfaire aux nouvelles obligations réglementaires, nous envisageons le rafraîchissement des murs de la salle.

#### ... et au théâtre de la Veillée.

On raconte que les séances de répétition vont bon train cet hiver au théâtre de la Veillée avec une troupe qui se renouvelle et aussi une pièce inédite d'un auteur dramatique bien connu au village.

On a hâte de découvrir cette création mondiale dans une des séances promises pour cet été !

### Balades louvetonnes

Il ne manque pas de [balades sympas autour du village](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/). Ci-dessous deux initiatives pour ne pas marcher seuls et faire de nouvelles connaissances dans une ambiance sympathique.

#### Pique-niquer et marcher ensemble un dimanche par mois...

**Nouveau** à Lalouvesc : Une rando dominicale mensuelle.

Quand ? Chaque **premier dimanche du mois,** une promenade conviviale est proposée à toutes et tous, accessible pour tout le monde car sans difficulté, avec pique-nique tiré du sac, de 12h à 15h.

Prochaines dates : le **8 Janvier** (et non le 1er, car lendemain du réveillon du 31, oh non, pitié !), **5 Février, 5 Mars** 2023.

Où se retrouver? **Rendez-vous place Marel,** devant la basilique, à **11h45**. Si le temps n'est pas favorable pour un pique-nique, un repli est possible à la Maison Saint-Régis, le temps de déguster nos pique-niques et de profiter les uns des autres, avant d'aller marcher.

Essayez et venez nombreux ! Sans inscription, entièrement gratuit, bonne humeur assurée !

> {{<grid>}}{{<bloc>}}
>
> ![](/media/balades-dimanche-2023-3.png)
>
> {{</bloc>}}{{<bloc>}}
>
> #### Récit: "Lalouvesc-sur-mer en hiver"
>
> Le coup d'envoi a été donné le dimanche 4 décembre.
>
> Nous étions quatre adultes avec une météo hivernale et la neige mouillée qui tombait allègrement à midi. Repli donc à la Maison Saint-Régis pour déjeuner ensemble et faire connaissance. Après ce fraternel temps de convivialité, en avant !, bien équipés, à l'assaut du Mont Chaix.
>
> Dès la sortie du village, chemin de Veyrines, la forêt nous a offert son féérique manteau neigeux !
>
> Le belvédère de Rochelipe, lui, n'offrait pas le large panorama habituel mais les abrupts rochers, coiffés de neige, en augmentaient le charme mystérieux. Plus on montait, plus la couche de neige s'épaississait ! Arrivées au sommet, les immenses croix du calvaire elles-mêmes étaient incrustées de flocons !
>
> {{</bloc>}}{{<bloc>}}

![](/media/balades-dimanche-2023-2.png)

{{</bloc>}}{{<bloc>}}

![](/media/balades-dimanche-2023-1.png)

{{</bloc>}}{{</grid>}}

> Retour par le chemin de Bellevue, le bien nommé habituellement, mais là en plein brouillard ! La promenade s'est terminée autour d'une sympathique infusion.
>
> En sortant au crépuscule, une surprenante lueur orangée enflammait l'horizon ouest, signe de beau temps. Puis un clair de lune dans un ciel étoilé a éclairé notre paysage neigeux !
>
> Le lendemain matin, splendide lever de soleil sur la chaîne des Alpes! Et sa douce chaleur n'a pas tardé à se manifester, irisant de ses rayons la mer de nuages à nos pieds ! Merveilleux pays que Lalouvesc-sur-mer en hiver !

Contact : Edith Archer, 11 rue des Alpes, Lalouvesc - 06.86.O4.66.90. Photos : Dorothée Fournier

#### ... et marcher tous les mardis

Nous vous rappelons aussi que toutes et tous, marcheurs aguerris ou débutants, messieurs ou mesdames de Lalouvesc ou des villages voisins… vous êtes les bienvenus parmi les «marcheurs du mardi». Rendez-vous est donné tous les mardis à 14h devant la mairie (hors juillet et août).

C’est assurément un bon moment de détente et de convivialité qui vous est proposé. Pas de contrainte ! Que le bonheur de découvrir ou redécouvrir les chemins alentours de notre village.

![](/media/balades-mardi-2022-1.jpg)

## Logement

### Carte des loyers

Le ministère du Logement propose une [carte des loyers](https://www.ecologie.gouv.fr/carte-des-loyers) commune par commune.

En France, le loyer mensuel médian, charges comprises, est de 7,7 € au m2 pour une maison (avec de grandes variations de 5,2 € à 28,4 €), contre 9 € pour un appartement (de 5,8 à Abzac en Charente à 29,4 à Neuilly-sur-Seine), selon les estimations de l’Agence nationale pour l’information sur le logement (ANIL) sur la base des annonces publiées sur les plateformes de leboncoin et du Groupe SeLoger pour des biens types mis en location au 3ème trimestre 2022.

Selon ces calculs à Lalouvesc pour un appartement le loyer moyen est de 7,7 € m2 (9,1 € le m2 pour un T1 et pour un T3 et plus 6,1 € le m2) et pour une maison individuelle : 7,3 € le m2. Voir ci-dessous les variations dans un rayon de 50 km autour du village (la commune de Lalouvesc est juste au centre du cercle).

![](/media/carte-des-loyers-maison-2022.png)

Rappel : le ministère met aussi à disposition une [carte des ventes immobilières](https://app.dvf.etalab.gouv.fr/) par commune.

### Coupures de courant : réfrigérateur, congélateur... précautions alimentaires

Il est possible que des coupures de courant soient programmées cet hiver. L'ARS rappelle quelques conseils élémentaires.

#### Dans les réfrigérateurs

Lors d'une coupure de courant, les aliments qui se trouvent dans les réfrigérateurs vont rester au froid pendant au moins 4 heures. Il est important dans ce contexte de ne pas ouvrir la porte de l’appareil, afin de ne pas laisser entrer la chaleur. Attention, la chaîne du froid ne doit pas être brisée pour certains aliments, particulièrement délicats, notamment si la coupure d'électricité dure plus de 6 heures.

Après la coupure de courant, ne pas ouvrir immédiatement le réfrigérateur. Laissez-lui le temps de se remettre à bonne température.

#### Dans les congélateurs

Si le congélateur est plein, celui-ci va être en capacité à rester froid pendant 48 heures environ. En amont des coupures, ne pas hésiter à placer autant de bacs à glaçons et de bouteilles d'eau que possible dans le congélateur : cela permettra aux aliments de tenir plus longtemps.

Reste ensuite à trier éventuellement les denrées à la fin de la coupure de courant. En pratique, il ne faut pas recongeler un aliment qui a été décongelé. Si les aliments sont dégelés mais que la température de l’appareil affiche 4°C, les aliments peuvent être cuisinés, pour une consommation rapide.

Pour savoir si des coupures sont envisagées sur votre secteur, il est recommandé de consulter les sites Internet :

* [https://www.monecowatt.fr](https://www.monecowatt.fr "https://www.monecowatt.fr")
* [https://coupures-temporaires.enedis.fr](https://coupures-temporaires.enedis.fr "https://coupures-temporaires.enedis.fr")

### Monoxyde de carbone : comment prévenir les intoxications

L'ARS communique :

Le monoxyde de carbone est un gaz toxique qui touche chaque année près de 4.000 foyers en France, entraînant une centaine de décès. Il peut être émis par **tous les appareils à combustion** : chaudière, chauffage d’appoint, poêle, groupe électrogène, cheminée.

![](/media/monoxyde-de-carbone-2022.png)

Pour éviter les intoxications, il suffit d’avoir quelques réflexes essentiels :

* Avant l’hiver, faites vérifier vos installations de chauffage et vos conduits de fumée par un professionnel qualifié.
* Veillez à une bonne aération et ventilation de votre logement tout au long de l’année.
* Respectez les consignes d’utilisation de vos appareils à combustion.
* N’utilisez jamais pour vous chauffer des appareils non destinés à cet usage : cuisinière, four, brasero, barbecue, etc.
* Si vous devez installer des groupes électrogènes, placez-les impérativement à l’extérieur des bâtiments.

En savoir plus : [https://www.auvergne-rhone-alpes.ars.sante.fr/monoxyde-de-carbone](https://www.auvergne-rhone-alpes.ars.sante.fr/monoxyde-de-carbone "https://www.auvergne-rhone-alpes.ars.sante.fr/monoxyde-de-carbone")

## Rivières

### Dates d'ouverture de la pêche

En 2023, la pêche sera ouverte dans tous les cours d'eau de première catégorie du 2ème samedi de mars au 3ème dimanche de septembre inclus, ainsi que tous les plans d'eau. Pour les lacs de Devesset, de Coucouron et d'Issarles, l'ouverture est prolongée de trois semaines.

Pour les cours et plans d'eau de deuxième catégorie, la pêche est ouverte toute l'année.

La pêche ne peut s'exercer plus d'une demi-heure avant l'heure légale du soleil, ni plus d'une demi-heure après son coucher.

Attention, il existe des dispositions particulières pour certaines espèces et selon les pratiques de pêches.

Pour toutes précisions, se référer à l'[arrêté préfectoral](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/proposition-n-2-_lalouvesc.pdf).

### L'agence de l'eau nous parle de la biodiversité

<iframe width="560" height="315" src="https://www.youtube.com/embed/YxTuU_6ZCgw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Naissances, mariages, décès en 2022

En 2022, quatorze décès ont été enregistrés ou transcrits et six mariages célébrés à la Mairie de Lalouvesc.

Décès annoncés dans la presse en 2022 en relation avec le village :

Michele GUIAS née ALBRIEUX, 78 ans ; Alain DUPRE, 63 ans ; Marie GRAIL née BASTIN, 92 ans ; Antonia BASTIN, 92 ans ; Regis VALSON, 72 ans ; Madeleine FOMBONNE née SEIGNOVERT, 95 ans ; Josette GEORGES, 92 ans ; Suzanne CROS, 93 ans ; Marthe JULLIA née COMTE, 90 ans ; Louis BAYLE, 87 ans ; Michel DEYGAS, 80 ans ; Albert MORINO, 98 ans ; Jeanne PRE, 93 ans ; Maurice RICHAUD, 86 ans ; Yvonne OSTERNAUD née CELETTE ; Alphonse ABRIAL, 94 ans ; Marcelle STIEVENART, 86 ans ; Simone MONFORT, 98 ans ; Yvette VALETTE, née MONTAGNE, 89 ans.

![](/media/naissances-et-deces-2014-2021.png)

## Dans l'actualité le mois dernier

#### Un Noël au Chili

27 décembre 2022  De nos envoyés spéciaux à l'île Chiloe [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/un-noel-au-chili/ "Lire Un Noël au Chili")

#### Joyeux Noël à toutes et tous !

24 décembre 2022  Pour tous les Louvetous et tous les amis du village [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/joyeux-noel/ "Lire Joyeux Noël à toutes et tous !")

#### Attention, les virus circulent !

19 décembre 2022  Covid, grippe, bronchiolite sont bien présentes dans la région... et dans le village [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/attention-les-virus-circulent/ "Lire Attention, les virus circulent !")

#### Un nouvel Emmaüs à Davézieux

14 décembre 2022  Adresse, horaires [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/un-nouvel-emmaus-a-davezieux/ "Lire Un nouvel Emmaüs à Davézieux")

#### La tombola de l'école est lancée

11 décembre 2022  De nombreux lots [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-tombola-de-l-ecole-est-lancee/ "Lire La tombola de l'école est lancée")

#### Les décorations de Noël ont été installées

10 décembre 2022 Un aperçu... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-decorations-de-noel-ont-ete-installees/ "Lire Les décorations de Noël ont été installées")

#### Aide aux démarches administratives

10 décembre 2022  Une maison France services a ouvert à St Romain d'Ay [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/aide-aux-demarches-administratives/ "Lire Aide aux démarches administratives")

#### Une nouvelle récompense pour le Domaine Fontcouverte

4 décembre 2022  Décidément, l'année se termine en beauté pour Fontcouverte [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/une-nouvelle-recompense-pour-le-domaine-fontcouverte/ "Lire Une nouvelle récompense pour le Domaine Fontcouverte")

#### L'Ardéchoise : la vidéo de l'épreuve 2022

4 décembre 2022  Et découvrez les parcours de la 30e édition et le nouveau site 2023 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/l-ardechoise-la-video-de-l-epreuve-2022/ "Lire L'Ardéchoise : la vidéo de l'épreuve 2022")

#### Offres d'emploi : tutrice ou tuteur pour le Service national universel

2 décembre 2022   3 X 15 jours, printemps-été 2023 région d'Aubenas [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/offres-d-emploi-tutrice-ou-tuteur-pour-le-service-national-universer/ "Lire Offres d'emploi : tutrice ou tuteur pour le Service national universel")

## Un nouveau logo à découvrir (puzzle)

En 2022, nous avons mené, grâce à l'appui de la préfecture et de l'ANCT, une réflexion approfondie sur la communication locale comprenant, entre autres, une [enquête dans la population](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#les-usages-de-linformation-locale) et des rencontres sur [Numérique et ruralité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lalouvesc.fr-montre-l-exemple/).

Parmi les nombreuses leçons de cette action, il est apparu qu'alors que le village avait une image forte, celle-ci n'était pas traduite et incarnée par un graphisme, un logo et une écriture, symbolisant le village et permettant de le repérer d'un coup d’œil. Le dessin de St Régis marchant et l'écusson du village suggéraient des premières pistes, mais il manquait la situation naturelle privilégiée du village (station verte) soulignée comme étant le premier attrait du village dans l'enquête. C'est pourquoi nous avons demandé à Lydia Khodja de nous faire quelques propositions dans le cadre du contrat passé.

Vous pouvez découvrir ci-dessous la proposition retenue. Le logo a vocation à être décliné sur toute une série d'objets (sacs, mugs...) qui pourraient être vendus dans les commerces du village dès la saison prochaine.

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=131679e3c696&view=iframe" style="width:100%;height:600px" frameborder="0" allowfullscreen></iframe>