+++
date = ""
description = ""
draft = true
header = ""
icon = ""
subtitle = ""
title = "34 - Investir"
weight = 1

+++
 et, comme toujours, vous y trouverez bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/bulletin-de-lalouvesc-n-33-1er-avril-2023.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du Maire

## Actualités de la Mairie

### Vote du budget prévisionnel 2023

### Rencontre avec la Députée

Un [document présentant le village](/media/lalouvesc-un-village-qui-a-du-caractere-avril-2023.pdf) et quelques uns des défis auxquels il doit faire face lui avait été envoyé préalablement.

### Demande de subvention au "Fonds vert"

### Ouverture du camping

Le camping a ouvert le 29 avril. Les réservations à l'ouverture sont du même ordre de grandeur que l'année dernière : 23,07 k€ contre 24,74 k€ en 2022.

Si vous n'avez pas encore réservé et que vous voulez profiter du [camping du Pré du Moulin](https://www.lalouvesc.fr/decouvrir-bouger/hebergement-restauration/camping/), il est prudent de ne plus tarder.

### Étude du SCOT sur Ste Monique et le Cénacle

l’ordre du jour :

1. Présentation de plusieurs scénarios sur Sainte Monique et sur le Cénacle avec les premiers éléments de chiffrage
   1. Cénacle : scénario Propolis + scénario SNU (où nous n’avons pas plus d’éléments que vous)
   2. Sainte-Monique : scénarios avec logements (logement classique, logement CetteFamille ou logements pour personnel EHPAD), équipement public en RDC (stockage pour associations + salle des assocs).
   3. Eléments sur la chaufferie bois.
2. Objectif de prendre une décision sur la dernière partie de l’étude et ce qui sera fait à la réunion suivante :
   1. Cénacle :
      1. soit un projet (SNU ou Propolis) est validé et le BE affine le scénario en conséquent sur le projet et propose un chiffrage + recherche des subventions mobilisables
      2. Soit pas de projet et le BE réalise une mise à jour de l’appel à projet des sœurs en proposant des éléments complémentaires en lien avec l’étude réalisée.
   2. Sainte-Monique :
      1. Soit affinage du scénario validé + montage financier et opérationnel + subventions mobilisables.
      2. Soit lancement d’un AMI sur Sainte-Monique pour trouver un porteur de projet privé.

Concernant la fin de l’étude, celle-ci pourrait être finalisée en début septembre.

## Zoom : Parlons finances

### Investir pour le village

Nous avons reçu un document récapitulant l'évolution des finances de la Commune sur cinq années. Nous en avons tiré quelques leçons :

Voir le [document complet](/media/12800-dvff-2022-finances-lalouvesc.pdf).

### L'eau, une priorité

## Puzzle : l'école St Joseph en 1967-68

L'école St Joseph propose à l'occasion de sa matinée "portes ouvertes" le 13 mai une exposition des anciennes photos de classe.

En préfiguration, ci-dessous la photo de l'année 67-68. Quelques sexagénaires pourront se reconnaitre...

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=0a4c26e23bf9&view=iframe&bgcolor=0x008080" style="width:100%;height:600px" frameborder="0" allowfullscreen></iframe>