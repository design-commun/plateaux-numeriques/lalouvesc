+++
date = 2023-01-30T23:00:00Z
description = "Bulletin d'information de Lalouvesc n°31 février 2023"
header = "/media/entete-logo-hiver.png"
icon = ""
subtitle = "n°31 février 2023"
title = "31 - Inspirations"
weight = 1

+++
En hiver, le froid, la neige, le gel ralentissent les activités dans le village, c'est le temps des inspirations. Dans ce Bulletin on en trouvera plusieurs, bien différentes mais réchauffantes, chaufferie bois ou chœur d'hommes. On suivra aussi les "feuilletons" de l'espace BeauSéjour, des réseaux d'eau, du Cénacle, de la Ronde louvetonne et celui aussi des aventures de Gulli des enfants de l'école... et encore, comme toujours, vous pourrez lire bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier](/media/bulletin-de-lalouvesc-n-31-1er-fevrier-2023.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du Maire

Bonjour à toutes et tous,

Un demi-mandat s’achève, j’ai l’impression d’avoir commencé il y a quelques mois, le temps passe très vite…

Nous venons de terminer un conseil municipal, mercredi 25 janvier 2023. Principalement à l’ordre du jour, un sujet qui peut vous surprendre :

- Vote pour élection partielle complémentaire.

Monsieur Jean Michel Salaün, deuxième adjoint, sous le coup d’une charge de travail très importante, réclame l’extension à onze personnes de notre effectif statuant au conseil municipal. Nous sommes huit, suite à trois démissions en début de mandat.

De mon côté, mon avis diverge. Je souhaitais utiliser d’autres solutions, pour répondre à cette surcharge de travail, comme l’extension des commissions et comités organisés par groupes de travail.

Le vote a été finalement déterminant, 5 POUR/3 NON. Nous allons donc organiser de nouvelles élections. C’est un sujet sensible, je ne souhaite pas replonger le village dans la division, comme nous l’avons connue.

La fraternité, la coalition, l’union sont les leitmotiv de mon mandat. Vous pourrez voir mes arguments dans le prochain compte-rendu du Conseil municipal concernant le vote, je vous invite à en prendre connaissance, vous comprendrez ma position.

J’invite toutes les personnes susceptibles d’être intéressées par ces places de conseillers municipaux à venir me rencontrer à la Mairie, nous en parlerons, suivant les sensibilités de chacun.

Quoi qu’il en soit, je continuerai dans la même voie, dans le même sens, dans la même optique, en toute confiance, transparence et en toute abnégation.

L’ACTE DEUX (deuxième partie du mandat) s’ouvre à nous, gardons le goût de la solidarité, il faut faire le job, en paix et en toute harmonie.

L’ACTE DEUX : LE MAIRE FACTUEL, DE TOUTES SES FORCES POUR LALOUVESC !!

Bien à vous toutes et tous. Amitiés.

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Demandes de subvention pour l'espace BeauSéjour

Des demandes de subventions ont été déposées à l’État (DETR), à la Région et au Département pour obtenir si possible une aide de 80% pour financer l'aménagement de l'espace BeauSéjour dont l'investissement a été chiffré par la société Archipolis à 267 k€.

Extrait de l'argumentaire :

> Le projet tire parti de la configuration existante de la parcelle, avec un espace urbain et minéral du côté de la place, qui accueillera deux arbres de haute tige nouvellement plantés, un jardin sur le fond de la parcelle au sein duquel sont conservés les sujets les plus remarquables et de plus grande ampleur (bouleaux et châtaigniers), et la conservation du mur ancien en pierre faisant le soutènement de la partie haute. Une rampe sera aménagée permettant la descente au travers de ce soutènement vers le niveau intermédiaire où seront installés des jeux pour enfants et un espace de jardin pédagogique en permaculture.
>
> La placette sera réalisée en sol stabilisé, les circulations piétonnes principales en béton désactivé, y compris sur les rampes. Le jardin sera enherbé et planté d'arbustes en périphérie. Une treille suspendue entre la placette et le jardin, ménagera un espace ombragé où pourra prendre place du mobilier urbain.
>
> Une grande assise longe la place jusqu'à la treille, ménageant un bac planté d'arbustes le long du mur. Le mur de pierre de soutènement sera rénové, et les interventions de gros œuvre seront réalisées en béton de site (béton avec ajout de matériaux de site). Une borne fontaine sera installée sur la place et une borne foraine électrique dans le jardin.
>
> L'éclairage du jardin sera réalisé par deux mâts à projecteurs. Une citerne de récupération des eaux pluviales par un système de canaux est enterrée dans le jardin. Elle est munie d'une pompe permettant l'arrosage. Les jeux pour enfants seront bardés de bois.
>
> L'ensemble du mobilier de cet espace sera réalisé avec les plus belles pierres de récupération de l'hôtel démoli.

![](/media/beausejour-3d-2023.jpg)

Par ailleurs, le jeu-monument fera l'objet d'un investissement séparé. Il a été chiffré en deux lots de 60 k€. Le premier lot fait l'objet d'une demande de subvention au programme Leader.

Une nouvelle étape est donc franchie sur ce dossier BeauSéjour. Il nous reste à défendre ces demandes et, selon les réponses, organiser la part communale du financement avec toute la prudence nécessaire et prévoir les travaux. Si tout va bien, ceux-ci pourront être entrepris dans le courant de l'année.

### Demandes de subvention pour des réparations sur le réseau d'eau potable

Des demandes de subventions ont été déposées à l’État (DETR), à l'Agence de l'eau et au Département pour obtenir si possible une aide de 80% pour une première phase de réparation du réseau d'eau potable dont l'investissement a été chiffré par la société Naldéo à 137 k€, suite à la finalisation du _Schéma Directeur et Diagnostic des Réseaux d’alimentation en Eau Potable_.

Extrait de l'argumentaire :

> Sur les dix travaux prioritaires préconisés par les auteurs du schéma directeur, la Commune a choisi d'en flécher trois pour une première phase raisonnable pour ses finances (sous réserve des aides accordées), considérés comme les plus urgents et impératifs :
>
> 1 - Amélioration de la qualité : les difficultés pointées par l'ARS proviennent d'une canalisation très ancienne en fonte et rouillée qui alimente une partie du village à partir de la Croix du St Père. Il convient de la remplacer en commençant par le tronçon le plus dégradé et principale source de pollution.
>
> 2 - Maîtrise des pertes : faute de pouvoir remplacer tout de suite les canalisations vétustes et susceptibles de rompre, il est important de repérer au plus vite les fuites quand elles surviennent pour pouvoir intervenir. Pour cela un système de compteurs et de télégestion sera mis en place.
>
> 3 - Maîtrise des pertes : pour réduire le risque de fuite, il est indispensable d'avoir des réducteurs de pression en bon état de marche. Un réducteur est défaillant, justement sur le tronçon qui s'est brisé plusieurs fois en 2022, rue de la Fontaine.

Quelles que soient les réponses à ces demandes, il faudra entreprendre dès cette année ces travaux urgents et indispensables.

![](/media/plan-reseau-eau-2023.jpg)

Le Schéma directeur a pointé dix travaux prioritaires et plusieurs autres travaux. L'ensemble à réaliser et à étaler dans la durée a été chiffré à 1 million d'€ sur le seul réseau d'eau potable. Le réseau d'assainissement et la station d'épuration demandent eux aussi de coûteuses réparations. Comme un peu partout en France, il s'agit pour Lalouvesc de réviser et rénover son système vieilli d'eau et d'assainissement pour l'adapter aux défis d'aujourd'hui et, plus encore, à ceux de demain. Cela demande de prendre le temps d'une réflexion approfondie. Nous y reviendrons dans un Bulletin consacré à cette question.

### Scénarios pour le Cénacle et St Monique

L'étude menée par le SCOT Rives du Rhône avance. Plusieurs ateliers ont déjà eu lieu avec les sœurs du Cénacle, des représentants d'associations et du Sanctuaire et des premiers porteurs de projet potentiel.

L'étude montre qu'il sera difficile de trouver un seul projet pour occuper un ensemble aussi grand. Le défi est alors de savoir comment pourraient être découpé l'immobilier en plusieurs lots répondant à des vocations différentes.

![](/media/ste-monique-cenacle-scot-atelier-1-2022.jpg)

Plusieurs porteurs de projet se sont manifestés. Certains se sont rapidement désistés. Deux projets, l'un d'accueil de seniors, l'autre de formation, servent actuellement de tests au bureau d'études pour définir une séparation possible des volumes disponibles.

Quatre découpages différents ont été présentés et discutés dans un atelier qui s'est tenu le 24 janvier. Les différentes parties prenantes doivent y réfléchir.

L'alternative est la suivante :

* soit des porteurs de projets s'engagent rapidement dans une transaction ferme avec les sœurs du Cénacle, et avec la Mairie pour ce qui concerne le bâtiment Ste Monique, et l'étude leur permettra de conforter leurs hypothèses d'occupation des locaux ;
* soit aucune transaction n'est réalisée à court terme et l'étude servira à publier un appel à manifestation d'intérêt sur des bases précises et réalistes pour rechercher des porteurs de projets correspondants à la situation des lieux.

Pour ne pas interférer dans les différentes discussions, il est proposé de reporter la réunion publique prévue fin février à une date ultérieure.

### Installation du city park

Le city park [acheté l'an dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#le-city-stade-a-%c3%a9t%c3%a9-r%c3%a9cup%c3%a9r%c3%a9) et provisoirement entreposé démonté sur le tennis du camping va être installé ce printemps. L'emplacement choisi est le tennis du parc du Grand Lieu. Celui-ci n'est plus opérationnel pour le tennis, mais son sol et son grillage pourront accueillir facilement le city park.

![](/media/city-park-2021-3.jpg)

Cet emplacement a l'avantage de valoriser le parc dont l'étang a été récemment curé. Ce quartier qui va s'étendre avec l'écolotissement ne disposait pas d'équipement public. Le City Park viendra combler cette lacune.

## Zoom

### Chaufferie bois

Une hypothèse émise dans le cadre de l'étude sur le Cénacle et la maison Ste Monique est d'installer une chaufferie bois qui pourrait aussi alimenter, outre les deux bâtiments concernés, la Maison de retraite et éventuellement des particuliers du quartier.

![](/media/chaudiere-a-bois-vocances-2023-6.jpg)

Afin d'approfondir la réflexion, les responsables de l'étude ont organisé une visite de la chaufferie de Vocance pour les élus de la Commune, accompagnés du frère Yves intéressé aussi par ce type d'installation. Ils ont été bien reçus, guidés et très éclairés par la maire de Vocance, Virginie Ferrand.

{{<grid>}}{{<bloc>}}

![](/media/chaudiere-a-bois-vocances-2023-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/chaudiere-a-bois-vocances-2023-2.jpg)

{{</bloc>}}{{</grid>}}

![](/media/chaudiere-a-bois-vocances-2023-5.jpg)

Dans cette Commune une ancienne chaudière-bois alimentée en sciure n'était plus fonctionnelle, elle a été remplacée il y a deux ans par deux chaudières bois de 110 KW chacune qui fonctionnent alternativement uniquement avec des plaquettes de bois.

Nous avons été impressionnés par la qualité et la productivité de l'équipement. Les plaquettes arrivent une fois par semaine dans un réceptacle extérieur protégé par un couvercle sur vérins hydrauliques, elles sont amenées jusqu'aux silos par des vis sans fin, puis jusqu'aux chaudières par deux autres vis sans fin. L'ensemble ne demande que très peu d'interventions des employés municipaux.

Les fumées sont très peu impactantes et le volume de cendres récoltées est dérisoire (une petite poubelle par an).

#### Quelques chiffres

![](/media/chaudiere-a-bois-vocances-2023-3.jpg)

Les chaudières alimentent les bâtiments municipaux (2.300 m2) et six habitations privées (1.000 m2). Le coût du chauffage pour un particulier revient pour un couple dans un logement ordinaire à environ 1.750 € annuel dont 650 € d'abonnement, sachant qu'il n'aura aucun coût supplémentaire d'entretien, seulement à ouvrir, fermer ou régler la température selon ses souhaits.

Le coût des travaux et de l'installation a été de 367 k€ comprenant la rénovation complète des locaux, la réalisation du bac extérieur pour recevoir les plaquettes, l'achat et l'installation du matériel.

Ce coût ne comprend pas la mise en place du réseau de chaleur qui était déjà existant. On considère que pour une bonne rentabilité, un réseau doit pouvoir transporter 1 Mégawatt/heure par mètre linéaire et par an.

### Un opéra choral exceptionnel cet  été en la Basilique

Jeudi 19 janvier, Le frère Yves Stoelen et l'équipe du Carrefour des Arts ont accueilli en la Basilique une délégation de musiciens : Gualtiero Dazzi compositeur, Christophe Galland récitant, Loïk Blanvillain chef de chœur, Thibaut Martin choriste et acousticien.

{{<grid>}}{{<bloc>}}

![](/media/essai-villette-2023-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/essai-villette-2023-3.jpg)

{{</bloc>}}{{</grid>}}

Pourquoi ce déplacement sous la neige en plein hiver ? Le Carrefour des Arts prépare activement la saison prochaine et ces musiciens venaient faire des essais de voix et préciser la logistique pour un événement exceptionnel, un spectacle unique de trente-six hommes chantant _a capella_, un récitant et trois musiciens, alto, clarinette et accordéon.

Parmi les surprises à venir du prochain programme estival, le Carrefour des Arts vous propose, en effet, **le 30 juillet 2023 en la Basilique de Lalouvesc, un opéra choral : _Exodes – Voir l’autre versant du matin_,** une œuvre originale composée par Gualtiero Dazzi sur un livret d’Elisabeth Kaess. _Afin de traduire en musique le thème de l’exode en tant que déplacement d’une multitude, la nécessité d’une écriture chorale, d’une multiplicité de langues et d’une pluralité des sources sonores s’est imposée à nous_. Notez-le sur votre agenda !

{{<grid>}}{{<bloc>}}

![](/media/essai-villette-2023-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/essai-villette-2023-1.jpg)

{{</bloc>}}{{</grid>}}

Le Chœur d'Hommes de La Villette est un chœur masculin _a cappella_ qui, depuis sa création en mars 2014, est rattaché à l’École Nationale Supérieure d'Architecture de Paris La Villette. Chaque année, il travaille sur le thème de l'invitation au voyage. Sous la direction de Loïk Blanvillain, il est composé d'une trentaine de chanteurs.

Voici un aperçu de la qualité de leur travail.

<iframe width="560" height="315" src="https://www.youtube.com/embed/pcTTkIvfAPs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

Michèle Guisset-Salaün, secrétaire du Carrefour des Arts

## Loisirs - Culture

### L'Office du Tourisme élargit son impact

L’Office de tourisme Ardèche Grand Air à Annonay et l’Office de tourisme du Val d’Ay à Lalouvesc, mutualisent leurs forces pour travailler ensemble sous une **bannière commune _Ardèche Grand Air_**. Il s'agit de pouvoir participer à plus d’actions (capacités financières et humaines) à une échelle de territoire plus pertinente : le nord-Ardèche / Ardèche verte.

L'avantage de la mutualisation c’est d’ouvrir les possibles en termes de moyens financiers et humains. C’est avoir la capacité de mener plus d’actions de communication qualitative que ce soit pour le site internet, le magazine de destination, la présence sur salons, les réseaux sociaux. C’est aussi une nouvelle équipe avec qui on partage les tâches et les outils dont plusieurs sont nouveaux pour l’Office de tourisme du Val d’Ay. Cette collaboration apporte ses fruits car les compétences, les connaissances, les expériences de chacun sur plusieurs champs d’action permettent de se renouveler, de s’orienter vers de nouvelles cibles.

![](/media/reunion-ot-2023.JPG "Alexandre, Annette, Cécile, Julie, Sandrine et Séverine des équipes des Offices de tourisme Ardèche Grand Air et Val d'Ay étaient réunis au Domaine de la Gentilhommière le 30 novembre. Camille Roberjot, apprentie de l'Office de tourisme du Val d'Ay, en centre de formation à Saint Laurent en Chamousset à cette date-là, a pu suivre les échanges en visio. Une visite du Domaine par Jean Astic a clôturé cette journée de travail.")

{{<grid>}}{{<bloc>}}

La bannière Ardèche Grand Air a été retenue pour l’ensemble de la destination car elle bénéficie déjà d’un référencement, qu’elle est identifiée et finalement elle nous correspond plutôt bien… Lalouvesc, comme d'ailleurs les autres communes du Val d’Ay, peut facilement s’associer à ce nom par ses randonnées, sa forêt, sa nature environnante et parce qu’on ne peut que gagner à prendre un souffle nouveau, un bol d’air, dans lequel on peut savourer l’instant présent, s’aérer l’esprit, se ressourcer, cheminer entre Nature et Spiritualité et se reconnecter à soi-même.

{{</bloc>}}{{<bloc>}}

![](/media/logo-ot-2023.png)

{{</bloc>}}{{</grid>}}

Mutualiser nous amène à « faire savoir » nos richesses communes, nos savoir-faire et pépites.

Travailler à une échelle plus grande, avec une stratégie commune et partagée, c’est une offre plus diversifiée à l’échelle d’un bassin, que les habitants se partagent déjà au quotidien et qui demain est à faire découvrir dans l’intégralité de la destination.

Séverine Moulin, directrice de l'Office du tourisme du Val d'Ay

### La Ronde louvetonne ouvre le bal des festivités...

{{<grid>}}{{<bloc>}}

![](/media/ronde-louvetonne_2023.jpg)

{{</bloc>}}{{<bloc>}}

La **Ronde louvetonne** a tout d’une grande. Née en 2017 elle a trébuché par deux fois devant la crise sanitaire. Elle a traversé des tempêtes météorologiques (une sévère en 2022 !). Chaque année elle se relève toujours plus déterminée. Elle a séduit de nombreux amateurs de véhicules anciens ou d’exception. Exposants ou spectateurs se pressent chaque année pour ce rassemblement dans notre village.

2023 renoue avec l'organisation d’un concert l’après-midi. **_Pascal chante Johnny_** enchantera et mettra le feu sur le site où nous attendons quelques 150 voitures et un public nombreux. Ce moment plus _rock & roll_ apportera une touche supplémentaire de _peps_ à ces quelques 150 bijoux qui auront, le matin même, apprécié la balade au _road book_. Cette balade offrira comme toutes les années tout le charme de nos petites routes sillonnant notre si belle région. Balade et non pas course de vitesse : chacun son pas, chacun son rythme. Un apéritif/étape sera proposé le temps que ces petits bijoux reprennent leur souffle avant de reprendre la route pour rejoindre Lalouvesc…

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-1.jpg)

{{</bloc>}}{{</grid>}}

La tombola sera bien évidemment reconduite faisant comme chaque année la joie de trois heureux gagnants de trois splendides paniers gourmands/découverte. Au fait… quels sont les Louvetous qui ont misé sur le bon numéro l’année dernière ? De mémoire ils étaient deux… Et comme sur toutes nos manifestations, restauration et buvette sauront ravir les papilles affamées et assoiffées.

En conclusion, les bénévoles du Comité des Fêtes se mobilisent pour que le 16 avril soit une journée des plus festives pour ouvrir en grand la porte des festivités saisonnières du village. Les projets annoncés le mois dernier se concrétisent peu à peu. Les dossiers s’étoffent, les échanges de mails et d’appels téléphoniques se multiplient, les conventions se signent… encore un peu de patience et …. Waouh ! Quelle belle saison s’annonce pour notre village !

#### Le Comité des fêtes vous invite

Vous êtes toutes et tous convié(e)s à notre Assemblée Générale le **vendredi 10 février 2023 à 18H00** au CAC. Un petit moment de sérieux et d’officiel (et oui, ça nous arrive) avant de partager une petite collation offerte pour en terminer avec chiffres et bilans… Venez nombreux !

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

### Assemblée générale du Club des deux clochers

Le club des deux clochers tiendra ses assemblées générales ordinaire et extraordinaire le : **jeudi 23 février 2023 à 14h** salle Louis Besset au Centre d'animation communal (CAC).

Les membres du conseil invitent tous les adhérents à participer aux débats et délibérations de ce moment important pour l’association.

Un pot d’amitié clôturera la manifestation.

## Santé - sécurite

### Frelons asiatiques, bilan Ardèche 2022

{{<grid>}} {{<bloc>}}

![](/media/frelons-ardeche-2022.png)

{{</bloc>}} {{<bloc>}}

Le nombre de nids est à peu près le même qu'en 2021, nous terminons la saison 2022 avec plus de 500 nids répertoriés sur [lefrelon.com](http://lefrelon.com) dont 400 ont pu être détruits.

Le nombre de nids est inférieur à celui de 2021 en basse vallée du Rhône ainsi que pour Beaume Drobie. Il est par contre élevé en centre ouest du département (Source et Volcans, bassin d'Aubenas...). Les frelons poursuivent leur progression en nord Ardèche. Quelques gros nids signalés à plus de 900 m d'altitude, favorisés par la chaleur.

A Lalouvesc, deux nids ont été signalés en 2022 dont un a été détruit.

[Rappel des démarches](https://www.lalouvesc.fr/mairie-demarches/demarches/divers/#frelons-asiatiques-et-autres-signalements-de-sant%C3%A9-publique).

{{</bloc>}} {{</grid>}}

## Dans l'actualité le mois dernier

Rappel : pour recevoir une alerte chaque lundi avec les actualités de la semaine précédente, envoyez un message à l’adresse : [sympa@numerian.fr](mailto:sympa@numerian.fr) avec dans l’objet du message la formule : _subscribe actualites-lalouvesc_, et suivez les instructions.

![](/media/bandeau-actualites-2023.jpg)

#### Prochain café des aidants

26 janvier 2023  4 février à 10h [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/prochain-cafe-des-aidants/ "Lire Prochain café des aidants")

#### Avancement du recensement

25 janvier 2023 N'oubliez pas de remplir le formulaire en ligne... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/avancement-du-recensement/ "Lire Avancement du recensement")

#### Colonie de vacances au col du Marchand

24 janvier 2023  Cinq places disponibles [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/colonies-de-vacances-au-col-du-marchand/ "Lire Colonie de vacances au col du Marchand")

#### Alerte de la gendarmerie

19 janvier 2023  Ne donnez pas d'informations sur vos habitudes par téléphone [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/alerte-de-la-gendarmerie/ "Lire Alerte de la gendarmerie")

#### Aides énergie 2023 - entreprises

19 janvier 2023   Toutes les aides aux TPE et PME [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/aides-energie-2023-entreprises/ "Lire Aides énergie 2023 - entreprises")

#### Offre d'emploi CCVA : Agent technique polyvalent

16 janvier 2023  Gardien de déchetterie [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/offre-d-emploi-ccva-agent-technique-polyvalent/ "Lire Offre d'emploi CCVA : Agent technique polyvalent")

#### Voeux du Maire : discours et chiffres

15 janvier 2023  Des retrouvailles chaleureuses et instructives. Résumé pour les absents [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/voeux-du-maire-discours-et-chiffres/ "Lire Voeux du Maire : discours et chiffres")

#### Comment faire pour que les alertes de lalouvesc.fr ne tombent pas en spam

15 janvier 2023  Pour les abonnés email en @orange.fr [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/comment-faire-pour-que-les-alertes-de-lalouvesc.fr-ne-tombent-pas-en-spamt/ "Lire Comment faire pour que les alertes de lalouvesc.fr ne tombent pas en spam")

#### Inscriptions au collège St Joseph de Satillieu

3 janvier 2023  Contacter le secrétariat [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/inscriptions-au-college-st-joseph-de-satillieu/ "Lire Inscriptions au collège St Joseph de Satillieu")

#### Parentibulle, rencontres du 1er trimestre

2 janvier 2023  Pour les jeunes enfants et leurs parents [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/parentibulle-rencontres-du-1er-trimestre/ "Lire Parentibulle, rencontres du 1er trimestre")

## Feuilleton, Le voyage de Gulli, épisode 2

Gulli a donc passé Noël avec sa fiancée Marina. Sous le sapin, il a trouvé le plus merveilleux des cadeaux : un billet d'avion pour l'Amérique, direction le Canada !

![](/media/feuilleton-2023-dressage.jpg)

Là-bas, Gulli participe à un cours de dressage de bisons, il finit premier et obtient son diplôme.

Après trois assiettes de pancakes au sirop d'érable, Gulli se rend à New York pour visiter le zoo de Central Park. Il passe bien sûr faire un coucou à la Statue de la Liberté.

![](/media/feuilleton-2023-statue.jpg)

Le lendemain, il part voir ses cousines, les Tortues de Floride, avant de se rendre au Brésil pour manger du bon chocolat en dansant la salsa au carnaval de Rio.

Gulli finit son périple par une promenade à dos de lama sur le Machu Picchu. Tout en haut, il déguste le plat traditionnel : le ceviche.

{{<grid>}} {{<bloc>}}

![](/media/feuilleton-2023-lama.jpg)

{{</bloc>}} {{<bloc>}}

![](/media/feuilleton-2023-repas.jpg)

{{</bloc>}} {{</grid>}}

En redescendant, Gulli plonge dans l'océan Pacifique, direction...

![](/media/feuilleton-2023-direction.jpg)

A suivre...

Comme Gulli, découvrez, vous aussi, le Machu Picchu, mais sans quitter votre fauteuil, en vous promenant dans ce site exceptionnel grâce au service 3D de Google earth. Il y a douze espaces différents à explorer.

[C'est par ici](https://earth.google.com/web/@-13.170664,-72.535688,2727.86355106a,3699.60478935d,35y,0h,59.9407796t,0r/data=Ci4SLBIgMjRiNWFhNTFlOGExMTFlNmJhN2U2ZjgxOGQ2OWE2ZTciCG92ZXJ2aWV3)

[![](/media/machu-picchu-g-2023.jpg)](https://earth.google.com/web/@-13.170664,-72.535688,2727.86355106a,3699.60478935d,35y,0h,59.9407796t,0r/data=Ci4SLBIgMjRiNWFhNTFlOGExMTFlNmJhN2U2ZjgxOGQ2OWE2ZTciCG92ZXJ2aWV3)