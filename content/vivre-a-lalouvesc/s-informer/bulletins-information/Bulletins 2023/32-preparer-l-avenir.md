+++
date = 2023-02-28T09:34:45Z
description = "Bulletin d'information de Lalouvesc n°32 mars 2023"
header = "/media/entete-mars-2023.jpg"
icon = ""
subtitle = "n°32 mars 2023"
title = "32 - Préparer l'avenir"
weight = 1

+++
Le changement climatique affecte les villages comme l'ensemble de la planète. Le monde rural doit s'y préparer, mais il est aussi bien placé pour proposer une alternative. Dans ce Bulletin, on trouvera un retour sur deux ateliers qui préparent l'avenir, celui des villages, grâce à une concertation menée par l'AMRF, et aussi celui de notre village avec la suite de la réflexion sur le Cénacle et Sainte Monique.

Le printemps approche et les événements de la saison se préparent, théâtre, concerts, voitures anciennes, expositions se dévoilent petit à petit... et encore, comme toujours, vous pourrez lire bien d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier](/media/bulletin-de-lalouvesc-n-32-1er-mars-2023.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens et des vidéos.

## Mot du Maire

Bonjour à toutes et à tous

Je me réjouis de vous présenter dans mon petit message les dernières avancées que nous avons mises en place, conformément à nos engagements.

Nous avons déposé deux demandes de subventions, auprès de l’État, du Conseil départemental, du Conseil régional et de l'Agence de l'eau. La première concernant la réhabilitation de l’espace Beauséjour, il s’agit de la liaison douce entre le centre bourg et le parc du Val-d’Or, les plans du projet sont consultables en mairie. La deuxième concerne la remise en état des canalisations d’adduction d’eau potable, depuis le réservoir du Saint-Père, jusqu’à la distribution dans le village. Ce seront les deux gros dossiers de l’été prochain.

De plus, le City parc sera installé sur le terrain de tennis devant l’ancienne usine SAMOV pour la saison estivale.

Nous sommes pratiquement à la sortie de l’hiver, Il faut se préparer pour l’été 2023. Les travaux de l’espace Beauséjour seront commencés, le chantier sera interdit au public tout le long des travaux.

N’oubliez pas la réfection du RD 278A, à l’entrée du village, ces travaux occasionneront certainement quelques petits problèmes de circulation.

Globalement sachez que vous pouvez compter sur l’équipe municipale pour toutes informations concernant notre village, n’hésitez pas à nous interpeller, nous sommes à votre écoute et disposition pour vous donner tous les renseignements concernant vos interrogations et nous serons heureux de recueillir vos remarques et suggestions.

PANNEAUPOCKET- La communication d’une collectivité ne se limite pas aux seuls services de communication généraux, ni aux supports produits par celle-ci. D’une part c’est la collectivité toute entière qui doit être communicante au travers des relations et des dispositifs d’écoute établis sous des formes multiples avec les citoyens. D’autre part plus il y a de supports plus nous développons notre visibilité, c’est pourquoi nous nous intéressons à PanneauPocket, application mobile d’information et d’alerte numéro 1 en France.

Le tout c’est de pouvoir l’harmoniser avec notre propre information, de façon à éviter les doublons, les superpositions d’informations et diriger nos rédactionnels dans le même sens. Vous êtes très nombreux à nous interroger sur ce mode de communication, sachez que nous travaillons dessus et vous trouverez dans ce Bulletin la réponse à vos interrogations.

Le mois de mars c’est le retour du printemps, c’est le mois des arcs-en-ciel, c’est le mois du changement d’heure et c’est le mois de la journée nationale du bonheur. Les jours grandissent, je vous souhaite à toutes et à tous un très bon mois de mars !

Amitiés, Jacques

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Élections partielles

La clôture des candidatures étant fixée au 2 mars, nous ne connaissons encore pas le nom des personnes qui se présenteront aux **suffrages des Louvetous le 19 mars.** Dès qu'ils seront connus, ils seront, bien sûr, affichés à la mairie et publiés sur lalouvesc.fr.

La décision de déclencher ces élections partielles a pu vous surprendre. Nous l'avons expliquée dans un Bulletin spécial. Ajoutons qu'il s'agit d'une chance pour le village qui a ainsi l'occasion de montrer son intérêt pour la commune et son attachement à la démocratie.

![](/media/elections.jpg)

Lalouvesc, traditionnellement champion de la participation aux élections, fera ainsi l'expérience de l'arrivée de nouveaux élus dans son assemblée représentative à mi-mandat.

### Recensement

Le recensement de la population est maintenant terminé, merci à toutes les Louvetonnes et les Louvetous qui ont pris le temps de remplir les formulaires, sur internet ou sur papier. Et merci aussi à Nicole Porte, notre agent recenseur, pour son efficacité souriante. Grâce à elle, les 497 logements du village ont pu être recensés.

Il est trop tôt pour avoir le retour de l'Insee sur l'évolution de la population. Les chiffres vérifiés ne seront officiellement connus qu'en juin et ils ne seront pris en compte dans les statistiques qu'en 2025.

![](/media/insee-recensement-2023-questions.jpg)

La population légale de Lalouvesc en 2020 selon l'Insee était de 380. Elle ne devrait pas avoir beaucoup bougé selon notre première estimation non officielle. De même, la proportion de résidences secondaires sur le village reste toujours très forte : très largement plus de la moitié des logements.

### Panneau Pocket est opérationnel

L'application d'informations locales est accessible sur tablette et smartphone. Il vous suffit de la télécharger sur votre banque d'applications et de mettre Lalouvesc en favori. Vous aurez ainsi accès aux informations du village publiées sur ce support.

Nous avons choisi de privilégier sur PanneauPocket les informations services et les annonces d'événements avec un rappel à quelques jours de leur tenue. Les principales associations et les commerçants pourront y publier directement leurs annonces.

<iframe src="https://app.panneaupocket.com/embeded/77003417" height="518" width="330" frameborder="0"></iframe>

Pour autant, les actualités ne disparaissent pas sur lalouvesc.fr et le message du lundi matin sera toujours fidèle dans les boîtes aux lettres des abonnés. On ne change pas des habitudes qui ont fait leurs preuves et il est prudent pour le village de garder la maîtrise de ses données de connexion. De plus le fil des actualités construit une histoire vivante du village, pérenne et accessible à tous.

L'application Panneau Pocket est aux mains d'une société privée dont nul ne connaît l'évolution de la stratégie dans les années à venir. On sait combien celles-ci sont changeantes dans le numérique, pas toujours pour le meilleur. Nous nous efforcerons de l'utiliser au mieux, mais sans naïveté.

L'articulation entre les deux supports restera donc étroite : Panneau Pocket sera accessible par un raccourci sur lalouvesc.fr et inversement les alertes du lundi seront aussi publiées sur l'application.

## Zoom : Un village en transition

### Grand atelier pour la transition écologique

Lalouvesc participe au Grand atelier des maires ruraux pour la transition écologique qui réunit au cours de quatre séances 99 d'élus de petites communes.

![](/media/atelier-amrf-fev-2023-1.jpg)

Son objectif est _d’élaborer la vision politique des territoires ruraux et de définir la feuille de route de l’AMRF \[Association des maires ruraux de France\] pour la transition écologique de la France, pour discuter des mesures susceptibles de préparer les petites communes aux changements climatiques_. Pour Lalouvesc, il s'agit aussi d'écouter, de repérer les bonnes idées, les bons réseaux, si possible quelques opportunités et aussi de partager tout cela.

En attendant les comptes-rendus de l'AMRF, on trouvera ci-dessous quelques impressions sur la première session qui s'est tenue le dernier week-end de février.

#### Les villages au cœur de la transition écologique

Le samedi, des responsables et des experts ont présenté l'état inquiétant de la situation et les défis généraux que les communes devront relever.

Valérie Masson-Delmotte, membre du GIEC, le groupe d'experts qui a alerté sur la situation difficile qui touche la planète, a fait une conférence remarquable... et implacable sur la situation, particulièrement celle de la France qui se trouve dans une zone bien affectée par le réchauffement avec une augmentation à 1,5°C supérieure à celle la moyenne générale de 1,1°C. Le dernier rapport du GIEC est paru l'année dernière. Vous pourrez en trouver un bref résumé en français [ici](https://www.ipcc.ch/site/assets/uploads/2021/08/IPCC_WGI-AR6-Press-Release_fr.pdf), un résumé un peu plus long [là](https://www.ipcc.ch/report/ar6/wg1/downloads/report/IPCC_AR6_WG1_SPM_French.pdf). Il montre sans contestation possible que le réchauffement est dû à l'activité humaine et que ses conséquences sont et seront graves.

Le réchauffement a déjà commencé à nous affecter et, quelles que soient les mesures que nous pouvons prendre aujourd'hui, il se poursuivra compte tenu de l'inertie du processus. Nous pouvons au mieux le ralentir pour tenter de renverser la tendance... en 2050. Pour autant, il ne faut pas baisser les bras, c'est une question de survie.

Un exemple l'illustrera mieux qu'un long discours. Depuis le 14e siècle les vendanges se tenaient régulièrement à Beaune en Bourgogne entre la fin septembre et le début octobre. La date est brutalement devenue plus précoce à partir de la fin du siècle dernier pour démarrer maintenant fin août, comme le montre le graphique ci-dessous.

{{<grid>}}{{<bloc>}}

![](/media/dates-vendanges-masson-delmotte-fev-2023.png)

{{</bloc>}}{{<bloc>}}

Cet exemple est parlant, mais il ne dit rien des conséquences graves du réchauffement : périodes de sécheresse et de pluie intenses, multiplication des incendies et des phénomènes météos brutaux, baisse des glaciers, montée du niveau de la mer, etc. Le réchauffement produit et produira aussi des changements lourds sur la biodiversité avec une modification des plantes et des animaux selon les régions.

{{</bloc>}}{{</grid>}}

Tout le monde doit s'y préparer, et les villages ont peut-être un rôle de premier plan, particulier, positif, à jouer. C'est en tous cas l'avis de Valérie Jousseaume de l'université de Nantes. Sa proposition est résumée de façon un peu abstraite sur la diapositive ci-dessous :

![](/media/atelier-amrf-fev-2023-2.jpg)

Nous sommes passés de l'ère paysanne à celle de la modernité au 19e siècle avec l'utilisation des machines, l'industrie, l'énergie, la montée des villes qui a progressivement atteint les campagnes avec l'accent mis sur la productivité, l'agrandissement des exploitations, entraînant l'exode rural. Aujourd'hui sous l'impulsion combinée de la crise climatique et du numérique, nous rentrerions dans une nouvelle ère avec un retour vers le rural, les jardins, les énergies renouvelables, les filières courtes, la sobriété...

Vous pourrez trouver son analyse, transcrite dans un [petit livre](https://www.urbanisme-puca.gouv.fr/IMG/pdf/discoursjousseaume_web_v5.pdf) accessible en ligne. Selon cette proposition le modèle du village est essentiel pour construire un récit nous donnant une vision positive d'un avenir qui risquerait d'être trop déprimant et paralysant sans cela. Il n'est pas surprenant qu'elle ait trouvé une écoute parfois enthousiaste auprès des participants de l'atelier.

#### Le rôle essentiel de la forêt

Passées les remarques générales, la thématique de cette première session s'est focalisée sur les énergies renouvelables. Différents groupes de travail se sont tenus parallèlement. Parmi ceux-ci, un accent tout particulier a été mis sur un domaine qui nous est familier à Lalouvesc : la forêt.

![](/media/atelier-amrf-fev-2023-3.jpg)

On l'oublie souvent, mais, en France, le bois-énergie est aujourd'hui et de loin la première source d'énergie renouvelable avec 32,9% de l'ensemble. De plus, la France comprend une très importante couverture forestière, toujours en accroissement du fait de la déprise agricole. La forêt française est encore très largement sous-exploitée ou mal exploitée.

L'ensemble des coupes peut être valorisé avec des applications différenciées selon les types d'essence et aussi selon les éléments de l'arbre. Mais il est important de l'exploiter correctement. Par exemple, laisser sur place les résidus de coupe, outre qu'il s'agit d'un manque à gagner, est un danger pour la planète et pour la forêt elle-même : le pourrissement des résidus produit du méthane et surtout favorise le développement et l'accélération des incendies.

Compte tenu des sécheresses, présentes et à venir, les incendies sont les principaux dangers pour les forêts. Avec le feu tout est perdu : l'énergie part en fumée et le réchauffement s'accroit.

Inversement, les chaufferies bois et les réseaux de chaleur sont une des meilleures valorisations de cette énergie renouvelable à condition qu'elles puissent être alimentées localement. Plusieurs réalisations dans des petites communes ont été présentées. Nous aurons l'occasion d'y revenir.

La prochaine session du Grand atelier se tiendra fin avril, d'ici là plusieurs webinaires sont prévus. A suivre donc. Déjà cette première session est entrée en résonance avec nos propres réflexions, notamment celles qui suivent ci-dessous.

J.-M. Salaün

### Point d'étape sur l'avenir du Cénacle et de Sainte Monique

#### Rappel de l’étude en cours

Le Cénacle de Lalouvesc est aujourd’hui propriété de la Congrégation des Sœurs du Cénacle. Ces dernières ont définitivement quitté le Cénacle il y a quelques années et ont ainsi mis en vente le bâtiment. Dans ce cadre, les sœurs se sont rapprochées de la société Karism Conseil pour les accompagner dans cette vente et trouver un repreneur pour le site. Les Sœurs, fortement attachées à ce lieu, son histoire et à la commune de Lalouvesc, ont toutefois fixé des critères pour le futur acheteur. Ce dernier devra porter un projet qui s’inscrive dans les valeurs portées par la Congrégation : un projet au service de la vie, que ce soit dans le domaine social, écologique, associatif ou ecclésial.

La Maison Sainte-Monique est quant à elle l’ancien bâtiment de la colonie d’Oran. Le bâtiment, propriété de la commune, est aujourd’hui sous-utilisé (un peu de stockage des associations sur le RDC) et est malheureusement en train de se dégrader. La commune souhaite donc qu’un nouveau projet émerge sur ce site, la vente de ce bien pouvant in-fine servir à financer d’autres projets publics sur la commune au service des habitants.

![](/media/cenacle-stemonique-2023.jpg)

En juin 2022, une étude portée conjointement par la commune de Lalouvesc, la communauté de communes du Val d’Ay, Epora et le Syndicat Mixte des Rives du Rhône a été lancée sur le Cénacle et la maison Sainte-Monique. Un groupement de bureaux d’études mené par l’agence d’urbanistes/architectes/paysagistes Lieux Fauves, basée à Lyon, a été choisi pour accompagner la commune et ses partenaires sur ce projet. L’objectif de cette étude est de réaliser un diagnostic complet de ces sites et bâtiments et de leur rôle dans la commune, de proposer plusieurs pistes et scénarios pour de futurs projets et, au final, de trouver de nouveaux porteurs de projets.

#### Les réflexions déjà engagées

Depuis juin 2022, plusieurs comités de pilotage et ateliers ont été organisés. Les bureaux d’études ont ainsi pu échanger avec différents acteurs de la commune et définir précisément les atouts et contraintes des sites, les enjeux pour le développement d’un ou plusieurs nouveaux projets, mais aussi de mieux cerner les besoins de la commune (habitants, professionnels, associations) auxquels pourraient répondre en partie ces futurs projets.

Aujourd’hui, deux porteurs de projet potentiels se sont manifestés :

* Une entreprise dans l’économie sociale et solidaire (ESS) qui souhaiterait créer une ou deux colocations solidaires pour des personnes âgées valides mais souvent en perte d’autonomie. Ce projet doit permettre à des personnes âgées de rompre la solitude et de disposer de services adaptés : logement adapté, salles communes à la colocation, présence de professionnels médicaux, paramédicaux et d’accompagnants… le tout à un loyer adapté pour permettre au plus grand nombre de personne d’y avoir accès.
* Une association qui souhaite développer un centre de formation avec hébergement à destination des adultes sur les questions environnementales et sociétales (dérèglement climatique, préservation de l’environnement, la bioéthique…). L’objectif est de proposer différents types de cursus, d’un cursus long sur une année scolaire alliant à la fois cours les matins et des activités pratiques et actions de bénévolat avec les associations et acteurs de la commune les après-midi, jusqu’à des cursus cours sur une semaine à un mois.

Dans le cadre des études en cours, les bureaux d’études ont proposé différentes possibilités d’implantation de ces porteurs de projet dans les bâtiments, mais aussi d’autres types d’occupations possibles et complémentaires : du logement, une salle à destination des associations de la commune, une zone de stockage de matériaux pour les associations, des locaux pour du personnel de santé, une chaufferie bois collective…

_Ci-dessous, un exemple de scénario possible d’accueil de découpage des bâtiments pour l’accueil de plusieurs projets (scénario non validé ayant uniquement vocation à mieux cerner les possibilités offertes par les bâtiments)._

![](/media/scenario-scot-mars-2023.jpg)

Dans les prochaines semaines, l’objectif est que les deux porteurs de projet aient pu avancer sur leurs projets (coûts, montage financier, estimation précise des besoins…) et se positionnent sur leur volonté d’aller plus loin ou non. En mars 2023, un nouveau comité de pilotage sera organisé et permettra d’affiner les études dans deux directions :

* Soit les porteurs de projet souhaitent réaliser leurs projets sur la commune. L’étude aura vocation à les accompagner dans la réussite de ces derniers.
* Soit les porteurs de projet se retirent. Dans ce cas, les études réalisées permettront d’enrichir l’appel à projet lancé sur le Cénacle par la Congrégation, mais aussi pour lancer potentiellement un appel à manifestation d’intérêt sur la maison Sainte-Monique dans l’objectif de trouver de nouveaux porteurs de projet intéressés.

Suite à cette réunion, une réunion publique sera organisée au deuxième trimestre 2023 afin de présenter les projets aux habitants et répondre à leurs questions.

Cédric Lansou, Syndicat mixte des Rives du Rhône

## Culture - loisirs

### Lalouvesc au Salon de l'agriculture

{{<grid>}}{{<bloc>}}

![](/media/fr-brunel-mars-2023.jpg)

{{</bloc>}}{{<bloc>}}

Les deux spécialités de Frédéric Brunel du Pavé St Régis sont proposées pour la première fois sur le stand de l'Ardèche au Salon de l'agriculture à Paris : Le pavé de St Régis et Lou chastanha.

Félicitations !

{{</bloc>}}{{<bloc>}}

![](/media/salon-de-l-agriculture-2023-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/salon-de-l-agriculture-2023-2.jpg)

{{</bloc>}}{{</grid>}}

![](/media/salon-de-l-agriculture-2023-1.jpg)

### Le Théâtre de la veillée en pleines répétitions

Le théâtre de la veillée, réunissant actrices et acteurs louvetous, propose cette année une pièce d'un auteur bien connu dans le village : **_Un Autobus Pour Le Ciel ou Rencontres et Chamailleries dans l’Au-delà_**, inspirée à Jacques Trébuchet par un roman de C.S. Lewis, [_Le Grand Divorce_](https://fr.wikipedia.org/wiki/Le_Grand_Divorce_entre_le_ciel_et_la_terre).

{{<grid>}}{{<bloc>}}

![](/media/theatre-de-la-veillee-mars-2023-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/theatre-de-la-veillee-mars-2023-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/theatre-de-la-veillee-mars-2023-3.jpg)

{{</bloc>}}{{<bloc>}}

L’auteur et les acteurs nous embarqueront dans un voyage insolite de l’enfer vers le ciel. Une pièce pleine d’humour et d’humanité qui soulève quelques questions « métaphysiques » telles que :

* L’enfer est-il ce lieu où les méchants brûlent dans un feu diabolique ?
* Le ciel est-il au-dessus des nuages ?
* La religion, une intériorité ou des rites immuables ?
* Où donc se cache Dieu dans ce monde ?

A travers des aventures, des « rencontres et des chamailleries », l’auteur nous partage ses réflexions à la fois comiques et sérieuses.

{{</bloc>}}{{</grid>}}

![](/media/theatre-de-la-veillee-mars-2023-4.jpg)

* Auteur : Jacques Trébuchet.
* Mise en scène et distribution : Le Théâtre de la Veillée.
* Costumes : L’atelier loisirs créatifs du Club des Deux Clochers.
* Décors : Marie-Christine Brunel.

Les représentations auront lieu à 20h30 à l'Abri du pèlerin les **vendredi 21 juillet, mardi 8 août et vendredi 18 août**.

La troupe cherche des salles en haute-Ardèche pour une tournée d'automne à partir de septembre 2023.

Renseignements : Frédéric Brunel 06 75 42 75 89.

### Comité des fêtes : démarrage de la saison 2023

Après avoir adressé une nouvelle fois nos remerciements à tous ceux et celles (nombreux) qui étaient présents à l’Assemblée Générale de notre Association le vendredi 10 février, place à l’avenir et aux nombreux projets de manifestations qui feront, cette année encore, de Lalouvesc un village animé et attractif.

#### LA RONDE LOUVETONNE pied au plancher…

![](/media/ronde-louvetonne-2022-1.jpg)

Nous avons activé le démarreur en envoyant [les bulletins d’inscription](/media/inscriptions-2023-ronde-louvetonne.pdf) le vendredi 10 février et dès le lundi 13 nous recevions deux bulletins !!! Autant dire que cette mise en route était attendue. Et depuis la machine tourne à plein régime. La boite aux lettres est desservie tous les jours pour notre plus grand plaisir.

Nous inscrivons des fidèles depuis la première édition ; ceux qui se sont accrochés aux wagons plus récemment mais qui eux aussi se fidélisent. Il y a ceux qui ont bravé la météo 2022 et qui sur leur bulletin d’inscription nous disent compter sur le soleil cette année. Donc pas de rancune, que du positif !

Nous sommes semble-t-il sur la bonne voie qui nous mènera jusqu’à l’étape finale du 16 avril. Nous espérons ainsi rouler à plein régime et ne pas connaître de souci mécanique. Nous avons les mains fermement ancrées sur le volant pour ne pas laisser partir le bolide en roues libres. Nous sommes vigilants sur la trajectoire pour que cette cinquième édition de **LA RONDE LOUVETONNE** soit une réussite.

A ce jour nous comptons 25 inscriptions. Nous aurons l’occasion de vous reparler de ce dossier le mois prochain.

#### CONCERT _A VUCIATA_ une autre ronde... aux chansons !

La billetterie est désormais ouverte aux points suivants :

* Office du tourisme d'Annonay : 04.75.33.24.51
* Office du tourisme de Lalouvesc : 04.75.67.84.20
* Arcada Davezieux : 04.75.67.95.74
* Coin des livres Davezieux : 04.75.67.73.14
* Tabac presse Dunières : 04.71.59.16.61

C’est un plaisir de recevoir ce quintette de bruns, beaux et talentueux chanteurs. La Corse vient jusque chez nous. Précipitons-nous à la basilique le dimanche 14 mai à 15h00 pour une chaleureuse et ensoleillée après-midi.

![](/media/affiche-a-vuciata-2023.jpg)

> **Un petit brin d’histoire** : L’âme corse semble parfois impénétrable. Elle alimente souvent le flot tumultueux de toutes sortes de rumeurs, venant grossir le lit des préjugés, jusqu’à le faire déborder…
>
> **_A Vuciata_** est une rumeur bien différente. C’est une parole, un écho, une voix… un fleuve qui naît au cœur du granit et du schiste. Creusant la terre au plus profond de son âme, il rejaillit à l’orée du maquis et des forêts, terminant sa course dans les eaux bleues de la Méditerranée, souvent portée par des courants capricieux vers des terres d’exil. Intarissable, il suit le cours sinueux d’une histoire séculaire, témoin d’une époque où la poésie se chantait, vibrait au rythme du cœur des Hommes. Source de création collective, il revêt la forme d’un chant polyphonique (cantu in paghjella), porte-parole de tout un peuple, mémoire éternelle qui a su transgresser les lois de l’usure du temps. Un chant qui par le lien universel de la vibration, unit les Hommes de Corse et d’ailleurs au sein d’une même conscience collective. Nous qui habitons d’autres rives de la méditerranée, nous avons voulu perpétuer l’œuvre de nos aînés, véhiculant notre culture et notre langue, «faisant courir le bruit» toujours plus loin, afin que jamais ne se perde le chemin de notre identité. C’est cet art, dans ses formes naturelles, sa profondeur et sa spontanéité que s’attache à restituer le groupe **_A Vuciata_**. Aussi vous invite-t-il, le temps d’un concert, au cœur de l’âme Corse.

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

### Assemblée générale du Carrefour des arts

**Le 18 mars au CAC à 15h**. Vous êtes toutes et tous conviés.

![](/media/carrefour-des-arts-janvier-2023.jpg)

Au menu : outre la présentation des bilans, moral et financier, le renouvellement du CA, venez **découvrir en avant-première les artistes plasticiens des expositions 2023, l'annonce des concerts et toutes les explications sur le nouveau dispositif "Micro-Folie" qui fait entrer les plus beaux musées de France sur les territoires ruraux**.

### L'Ardéchoise se prépare pour sa 30e édition

L'Ardéchoise est mi-juin et il y a déjà plus de [5.000 cyclistes inscrits](https://www.ardechoise.com/liste-inscrits/)… le succès est garanti.

Cette année on fêtera son 30ème anniversaire. A cette occasion un [maillot collector](https://drive.google.com/file/d/1WaTrvRG79eZ-XYGdx1uwJXYJZPoY_I3a/view) est offert à tous les inscrits. Un livre relatant l'histoire de l'événement a aussi été publié.

![](/media/livre-la-fabuleuse-ascension-de-l-ardechoise-2023.png)

Et, bien sûr, le Comité des fêtes prépare une déco du village à la hauteur de l'événement. Cette année ce sera sur le thème de [_Alice au pays des merveilles_](https://fr.wikipedia.org/wiki/Les_Aventures_d%27Alice_au_pays_des_merveilles).

## Santé - sécurité

### Une Sainte Barbe, des promotions et des recrues pour les sapeurs-pompiers de Lalouvesc

![](/media/pompiers-ste-barbe-2023-3.JPG)

La traditionnelle Sainte Barbe n'avait pu être fêtée depuis deux ans pour des raisons sanitaires. La cérémonie s'est tenue enfin cette année le 4 février, l'occasion pour le lieutenant Lionel Achard, chef de centre, de rendre hommage aux sapeurs-pompiers du village, devant les élus du territoire et les responsables du SDIS.

{{<grid>}}{{<bloc>}}

![](/media/pompiers-ste-barbe-2023-4.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/pompiers-ste-barbe-2023-5.JPG)

{{</bloc>}}{{</grid>}}

Eric Serayet a été nommé sergent-chef. Corentin Serayet est passé caporal. Xavier Mohr a suivi des formations comme accompagnateur et formateur. Il pourra ainsi encadrer les nouvelles recrues.

{{<grid>}}{{<bloc>}}

![](/media/pompiers-ste-barbe-2023-7.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/pompiers-ste-barbe-2023-6.JPG)

{{</bloc>}}{{</grid>}}

Les sapeurs Thomas Mohr, Maël Huchet et Mathias Serayet passent première classe grâce aux formations suivies.

Trois nouvelles jeunes recrues féminines rejoignent les rangs des sapeurs-pompiers de Lalouvesc : Nina Porras, Lauréna Mohr et Elodie Antressangle.

Il y a à ce jour 12 sapeurs-pompiers actifs et 9 retraités.

![](/media/pompiers-ste-barbe-2023-2.JPG)

En 2022, 104 interventions ont été assurées soit 5 de plus qu’en 2021, dont 42 sur la commune de Lalouvesc, 5 sur St Pierre sur Doux et 4 sur Lafarre. Le reste a été effectué en renfort sur d’autres communes.

* 81 secours à personnes
* 10 feux de forêt
* 5 incendies
* 3 accidents sur la voie publique
* 5 opérations diverses
* soit un total de 630 heures en intervention.

Malgré la bonne réponse opérationnelle, 14 interventions n’ont pu être assurées faute de personnel disponible.

#### Matinée caillettes-frites

Le dimanche 5 mars, les sapeurs-pompiers de Lalouvesc vous proposent la 8ème édition de leur **matinée caillettes-frites à déguster sur place ou à emporter**. Rendez-vous devant la mairie à partir de 9h.

### Sécurité numérique

La gendarmerie nous alerte sur la nécessité d'être vigilant dans notre navigation sur l'internet, les messageries, les réseaux sociaux et applications diverses qui sont souvent très utiles, mais aussi un terrain de jeu pour les arnaqueurs. Dix bonnes pratiques sont pointées.

{{<grid>}}{{<bloc>}}

![](/media/securite-informatique-2023.jpg)

{{</bloc>}}{{<bloc>}}

 1. Protégez vos accès avec des mots de passe solides
 2. Sauvegardez vos données régulièrement
 3. Appliquez les mises à jour de sécurité sur tous vos appareils (PC, tablettes, téléphones…) dès qu'elles vous sont proposées
 4. Utilisez un antivirus
 5. Téléchargez vos applications uniquement sur les sites officiels
 6. Méfiez-vous des messages inattendus
 7. Vérifiez les sites sur lesquels vous faites des achats
 8. Maîtrisez vos réseaux sociaux
 9. Séparez vos usages personnels et professionnels
10. Évitez les réseaux wifi publics ou inconnus

{{</bloc>}}{{</grid>}}

Elles sont détaillées dans une [fiche pratique](/media/fichepratique_securitenumerique-mars-2023.pdf).

### Comptage de la circulation routière

Savez-vous qu'il existe une carte qui rend compte des flux de circulation routière sur l'Ardèche ?

![](/media/plan-comptages-routiers-2023.jpg)

On y apprend que le trafic moyen journalier sur la route de St Félicien en sortie et entrée de Lalouvesc est de 790 véhicules, tandis qu'il est de 655 passages sur la route de St Bonnet. Le comptage sur la route d'Annonay n'est pas significatif car le compteur est placé juste à la sortie d'Annonay.

Cette carte, comme de nombreuses autres cartes spécialisées sur le village ou le département, est accessible par la page [Plans, cartes](https://www.lalouvesc.fr/vivre-a-lalouvesc/histoire-identite/plans-cartes/) du site lalouvesc.fr.

## Dans l'actualité du mois dernier

#### La mairie recrute

22 février 2023 Un agent communal polyvalent à plein temps [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-mairie-recrute-2/ "Lire la suite – La mairie recrute")

#### Loto du Balcon des Alpes

20 février 2023  26 mars à 14h au CAC, au profit des résidents de l'EHPAD [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/loto-du-balcon-des-alpes/ "Lire la suite – Loto du Balcon des Alpes")

#### Le CIDFF recrute

16 février 2023 Deux temps partiels sur Aubenas et Tournon [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-cidf-recrute/ "Lire Le CIDFF recrute")

#### Passages de l'opticien mobile

15 février 2023 Quatre dates de mars à mai [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/passages-de-l-opticien-mobile/ "Lire Passages de l'opticien mobile")

#### Le chic louvetou

15 février 2023 Les sacs siglés sont arrivés [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-chic-louvetou/ "Lire Le chic louvetou")

#### Élections partielles : pourquoi pas vous ?

14 février 2023 Trois bonnes raisons de candidater [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/elections-partielles-pourquoi-pas-vous/ "Lire Elections partielles : pourquoi pas vous ?")

#### Circulation alternée sur la RD 532

14 février 2023 Du 23 février au 10 mars [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/circulation-alternee-sur-la-rd-532/ "Lire Circulation alternée sur la RD 532")

#### Le Comité des fêtes au mieux de sa forme

11 février 2023 Des résultats impressionnants [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-comite-des-fetes-au-mieux-de-sa-forme/ "Lire Le Comité des fêtes au mieux de sa forme")

#### Le nouveau véhicule communal est arrivé

8 février 2023 C'est un berlingo électrique [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-nouveau-vehicule-communal-est-arrive/ "Lire Le nouveau véhicule communal est arrivé")

#### Concours de Belote et loto le 18 février

6 février 2023 Organisé par l'APEL au CAC [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/concours-de-belote-et-loto-le-18-fevrier/ "Lire Concours de Belote et loto le 18 février")

#### Bulletin d'information : Spécial élections partielles

5 février 2023 n° hors-série - février 2023 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/bulletin-d-information-special-elections-partielles/ "Lire Bulletin d'information : Spécial élections partielles")

#### Chant et polyphonies corses à la Basilique

3 février 2023 Retenez la date : 14 mai [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/chant-et-polyphonies-corses-a-la-basilique/ "Lire Chant et polyphonies corses à la Basilique")

#### Élections partielles les 19 et 26 mars 2023

2 février 2023 Arrêté préfectoral [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/elections-partielles-les-19-et-26-mars-2023/ "Lire Elections partielles les 19 et 26 mars 2023")

#### Lettre d'information n°1 de la CCVA

1 février 2023 Focus sur les ordures ménagères [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lettre-d-information-n-1-de-la-ccva/ "Lire Lettre d'information n°1 de la CCVA")

## Feuilleton, Le voyage de Gulli, épisode 3

> Gulli a sauté dans l'Océan Pacifique en direction de l'Australie. En chemin, il croise des requins, des baleines, des dauphins et il se retrouve même au milieu de ses cousines les tortues de Galapagos.
>
> Gulli arrive à Canberra, la capitale de l'Australie. Là, il décide de goûter une des spécialités du pays : le poulet parmigiana. Un véritable délice !
>
> ![](/media/feuilleton-mars-2023-1.jpg)
>
> Après une bonne nuit de sommeil, Gulli se rend à Kangourou Island pour un combat de boxe hors norme avec Barth le Kangourou. Le séjour est sportif car il fait même de l'accrobranche au milieu des eucalyptus avec les Koalas.
>
> {{<grid>}}{{<bloc>}}

![](/media/feuilleton-mars-2023-3.jpg)

{{</bloc>}}{{<bloc>}}

> ![](/media/feuilleton-mars-2023-2.jpg)
>
> {{</bloc>}}{{</grid>}}
>
> Gulli se rend dans le territoire aborigène, au pied de l'Ayers Rock pour déguster les fameuses Barbecue Shags.
>
> Gulli termine son périple par une séance de plongée sous-marine avec les tortues punks dans la grande barrière de corail.
>
> ![](/media/feuilleton-mars-2023-4.jpg)
> à suivre...

Les enfants de l'école St Joseph

**Vous aussi, plongez avec une tortue punk !**

<iframe width="560" height="315" src="https://www.youtube.com/embed/GuACJj9deCc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>