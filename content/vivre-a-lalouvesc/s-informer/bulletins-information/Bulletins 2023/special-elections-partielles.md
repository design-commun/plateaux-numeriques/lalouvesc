+++
date = 2023-02-05T23:00:00Z
description = "n° hors-série - février 2023"
header = "/media/entete-logo-hiver.png"
icon = ""
subtitle = "n° hors-série - février 2023"
title = "Bulletin d'information : Spécial élections partielles"
weight = 1

+++
## Pourquoi des élections partielles

De toutes nos forces pour Lalouvesc ! Telle est la maxime que nous avons mis en œuvre dans la première partie du mandat. Pour cette seconde partie, nous souhaitons compléter l'équipe pour la renforcer encore et donner le maximum de chance de réussite à notre village.

[Version imprimable du Bulletin](/media/bulletin-de-lalouvesc-hors-serie-special-elections-partielles-fevrier-2023.pdf)

{{<grid>}}{{<bloc>}}

![](/media/conseil-municipal-2020-lalouvesc.jpg)

{{</bloc>}}{{<bloc>}}

[Nous étions onze](https://www.lalouvesc.fr/mairie-demarches/vie-municipale/conseil-municipal/). Suite à la démission de trois conseillers, nous ne sommes plus que huit. Par ailleurs, certains sont pris par d'autres tâches et n'ont que peu de temps à donner à la Mairie.

Néanmoins, dans la mesure de la disponibilité de chacun, élus de Lalouvesc nous n'avons pas ménagé notre peine dans la première partie du mandat. Nous avons mis en œuvre notre programme.

{{</bloc>}}{{</grid>}}

Et avec l'appui d'une population, elle aussi très impliquée dans le développement du village, les résultats sont déjà visibles.

### Rénover le village...

Tout d'abord, il a fallu rénover et ouvrir plus largement la Mairie, embellir et faire les premiers travaux urgents dans le village. La transparence, la disponibilité et l'accueil ont été notre leitmotiv pour faire de notre village un lieu chaleureux où il fait bon vivre.

Alors que faute d'investissement, le village était en difficulté comme d'autres communes rurales, il se transforme, cela se voit et cela se sait, comme l'a montré le Maire dans [ses vœux](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/voeux-du-maire-discours-et-chiffres/).

Exemples parmi bien d'autres : Lalouvesc était en 2021 à la [quatrième place](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lalouvesc-dans-le-top-5-des-evenements-ardechois-en-2021/) pour la fréquentation des manifestations culturelles (Carrefour des Arts) et aussi pour celle des manifestations sportives en Ardèche (Trail des Sapins), en 2022 la brocante, le Trail et le camping ont battu leurs records d'affluence, la quasi-totalité des espaces de son écolotissement a trouvé preneur, son site web est un démonstrateur pour un numérique efficace et écoresponsable, etc.

### ... le développer...

De très gros dossiers, préparés dans la première partie du mandat doivent être finalisés :

* aménagement du centre bourg suite à la démolition de l'hôtel BeauSéjour ;
* revitalisation du Cénacle et du bâtiment Ste Monique suite à une étude du SCOT ;
* rénovation et réparation des réseaux d'eau et d'assainissement.

![](/media/cenacle-stemonique-2023.jpg)

Ils s'ajoutent aux tâches ordinaires de modernisation de la Commune : adressage, recensement, passage à la nouvelle norme comptable, arrivée de la fibre, réparation de la Basilique etc.

### ... et relever les défis à venir.

De plus, il reste des gros dossiers à ouvrir comme la rénovation du quartier de la Basilique ou la rue de la fontaine ; malgré nos efforts d'autres chantiers interne à la Mairie sont encore à entreprendre : l'archivage, la comptabilité analytique et surtout faire vivre les Comités pour impliquer plus d'habitants dans les décisions de la municipalité... il faut enfin préparer la Commune aux défis qui s'annoncent, liés aux changements climatiques et géopolitiques : transport, isolation, énergie... sans parler des défis que nous ne connaissons pas encore et qui ne manqueront pas de surgir.

**Nous ne sommes pas assez nombreux pour assumer l'ensemble des tâches qui nous attendent et sans énergies supplémentaires nous risquons de décevoir.**

**Et il faut aussi songer à la suite, et que d'autres aient l'expérience de la gestion de la Commune.**

Compte-tenu de l'élan donné, nous sommes sûrs que d'autres Louvetonnes ou Louvetous sont prêts à nous rejoindre.

## Qui peut se présenter comme conseiller municipal ?

Trois postes sont à pourvoir pour compléter le Conseil. **Le premier tour des élections se tiendra le 19 mars**. Si besoin un second tour aura lieu le 26 mars.

Il faut avoir au moins dix-huit ans révolus. Tous les électeurs de la commune et tous les citoyens inscrits au rôle des contributions directes ou justifiant qu'ils devaient y être inscrits au 1er janvier de cette année sont éligibles.

> [Formulaire à remplir](https://www.service-public.fr/particuliers/vosdroits/R34319). Documents à fournir :  
> 1\. Un justificatif d’identité avec photographie ;  
> 2\. **Si vous avez la qualité d’électeur dans la commune** (1 document) :
>
> * soit une attestation d’inscription sur la liste électorale, délivrée dans les trente jours précédant le dépôt de la candidature ;  
>   \- soit une copie de la décision de justice ordonnant votre inscription (l’original doit être présenté) sur la liste électorale.  
>   3\. **Si vous avez la qualité d’électeur dans une autre commune** (2 documents) :  
>   3\.1. Un document de nature à prouver votre qualité d’électeur : l’un des deux documents visés au 2.  
>   3\.2. Un document de nature à prouver votre attache avec la commune :  
>   – soit un avis d’imposition ou un extrait de rôle, délivré par le comptable du Trésor, qui établit que vous êtes inscrit personnellement au rôle des contributions directes de la commune à la date du 1er janvier ;  
>   – soit une attestation du directeur départemental ou régional des finances publiques établissant que vous serez inscrit au rôle des contributions directes dans la commune à la date du 1er janvier ;  
>   – soit la copie d’un acte notarié établissant que vous êtes devenu, dans l’année dernière, propriétaire d’un immeuble dans la commune, ou d’un acte (notarié ou sous seing privé) enregistré établissant que vous êtes devenu locataire d’un logement d’habitation dans la commune.  
>   4\. **Si vous n’êtes pas inscrit sur une liste électorale** (3 documents) :  
>   4\.1. Les deux documents de nature à prouver votre qualité d’électeur :  
>   4\.1.1. Un certificat de nationalité ou un passeport ou une carte nationale d’identité en cours de validité.  
>   4\.1.2. Un bulletin nº3 du casier judiciaire délivré depuis moins de trois mois.  
>   4\.2. Un document de nature à prouver votre attache avec la commune : l’un des trois documents visés au 3.2.  
>   Si vous êtes ressortissant d’un État membre de l’Union européenne autre que la France, vous devez également joindre une déclaration certifiant que vous n’êtes pas déchu du droit d’éligibilité dans l’État dont vous avez la nationalité.

**Le dépôt des candidatures sera ouvert à la sous-préfecture de Tournon pour le premier tour de scrutin du 27 février au 2 mars 2023**. Nous réfléchissons à la Mairie pour faciliter le dépôt à ceux qui le souhaitent.

## Portrait robot d'un(e) conseiller(e) municipal(e) idéal(e)

Bien sûr, les électeurs sont souverains et ils choisiront les conseillers qui leur conviennent à la majorité. Les élus actuels sont tenus à la neutralité et nous ne favoriserons aucune candidature. Cela ne nous empêche pas de donner un avis général sur les qualités dont nous aurions besoin pour nous épauler à la municipalité.

Basé sur presque un demi-mandat municipal, nous pouvons dresser le portrait-robot d'un conseiller municipal idéal, idéal qu'aucun d'entre nous n'atteint évidemment... mais on fait des efforts !

### Qualités du conseiller idéal

Le conseiller idéal devrait être :

* **Bienveillant** : sait écouter les difficultés de chacun et y répondre dans les limites de la qualité suivante.
* **Solidaire** : fait passer l'intérêt commun avant les réclamations égoïstes et ses intérêts privés.
* **Positif** : recherche les solutions simples sans se noyer dans les problèmes particuliers, ni remettre à plus tard ce qui peut être résolu rapidement.
* **Raisonnable** : adapte les solutions aux moyens accessibles sans viser trop haut ni grever l'avenir.
* **Disponible** : est prêt à donner de son temps, dans la limite de ses autres obligations. Est capable de se mobiliser en cas d'urgence.
* **Imaginatif** : trouve les solutions et des chemins imprévus malgré les difficultés apparentes.
* **Conciliant** : accepte des compromis face aux mouvements contradictoires.
* Et le petit plus : garde le sourire, a de l'humour.

Inversement, des défauts sont à proscrire car ils retardent inutilement le travail municipal : nous n'avons pas besoin de grognons, de _y-a-qu'à_, de _on-aurait-du_, de _je-l'avais-bien-dit_.

### Compétences utiles

De même, nous avons pu mesurer combien dans une petite commune, qui ne dispose pas de services spécialisés pour préparer et traiter les dossiers, les conseillers doivent être multicartes pour répondre à tous les problèmes quotidiens ou lancer et suivre les projets de développement pour le village.

Ainsi des compétences seront appréciées pour un conseiller nouvellement élu dans au moins un des quatre domaines ci-dessous :

* **Administration** : réglementation, droit, comptabilité..
* **Travaux** : voirie, bâtiment, mécanique, espaces verts, entretien...
* **Communication** : numérique, rédaction, relation publique.
* **Management** : planification, organisation, pilotage.

![](/media/vous-elections-2023.jpg)