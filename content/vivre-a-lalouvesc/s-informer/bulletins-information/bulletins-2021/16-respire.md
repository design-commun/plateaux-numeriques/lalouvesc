+++
date = 2021-10-30T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°16 Novembre 2021"
title = "16 - Respire"
weight = 1

+++
Ce mois d'octobre a été généreux pour les chercheurs de champignons, mais plus sévère pour les coureurs trempés du Trail des Sapins. On trouvera aussi dans ce numéro le compte-rendu d'une étude sur la pureté de l'air dans la région et, comme toujours, bien d’autres informations, sans oublier la suite du feuilleton des enfants de l'école St Joseph.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce[ ](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/13-serieux-et-sourires-lalouvesc.pdf)[fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-16-1er-novembre-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Chers amis,

Nous y sommes, les feuilles tourbillonnent sur le sol, le Trail des Sapins met fin aux réjouissances de l’été. Il nous reste encore les vacances de Toussaint pour animer notre village et la saison des champignons, généreuse cette année.

Nous avons moins de restrictions sanitaires, le Trail a pu se dérouler d’une façon quasi-normale, c’est un renouveau, il fait bon respirer à pleins poumons notre bon air en toute liberté.

Après Halloween nous allons nous attacher à préparer les décorations de Noël, nous serons vite à la fin de l’année et, juste derrière, tout début janvier, la démolition de l’hôtel Beauséjour donnera pour le printemps une vaste ouverture sur le parc du Val d’or.

En cette fin d’année, le Comité de développement va se réunir. Il faut réfléchir, en effet, à l’avenir de notre village et capitaliser sur le surcroît d’intérêt lié à notre territoire.

La ruralité est pauvre mais notre patrimoine est riche. L’économie rurale se trouve au cœur de débats contemporains et fournit un terrain propice à des investigations nouvelles et prometteuses pour le développement de nos territoires. L’économie rurale ou agricole devient un exemple des relations entre la nature, l’alimentation et la société en général.

Unissons nos réflexions dans ce domaine pour ne pas louper l’organisation de notre développement futur. Toutes vos suggestions, toutes vos idées peuvent nous aider et alimenter la discussion du Comité de développement et du Conseil municipal. N’hésitez pas à nous en faire part en envoyant un message à mairie@lalouvesc.fr.

Je vous souhaite à toutes et à tous de bonnes fêtes de Toussaint.

Votre dévoué Jacques Burriez

## Actualités de la Mairie

### Un nouvel employé municipal

Michel Astier a quitté son poste d'employé municipal à l'expiration de son contrat fin septembre. La mairie le remercie pour tout le travail très utile qu'il a accompli sur le village.

Pour le remplacer un nouvel employé nous rejoint le 8 novembre : M. Patrick Mazoyer. Il a été recruté pour un contrat de six mois renouvelable comme agent technique territorial. Bienvenue à lui !

### Chasse-neige

Suite à une analyse précise de l'état du chasse-neige actuel, les réparations indispensables sont apparues trop coûteuses pour un résultat incertain.

Il a donc été décidé de le remplacer par un engin plus petit et polyvalent, mais à la puissance équivalente. La transaction, compte tenu de la reprise de l'ancien tracteur, devrait tourner autour de 50 k€. Cette somme non-négligeable pour les finances de la commune, devrait être heureusement compensée par la récupération d'une ancienne subvention obtenue par la municipalité précédente sur les travaux sur le réseau d'eau et non encore récupérée.

Le chasse-neige sera piloté cet hiver par Dominique Balaÿ, jeune retraité.

### Rencontre avec les commerçants

A l'invitation de la mairie, une quinzaine de commerçants sont venus partager avec les adjoints leurs impressions sur la saison passée, sur les points à améliorer et sur leurs idées pour l'attractivité du village. La discussion a été sympathique et nourrie et les suggestions et échanges informations nombreuses. En voici un résumé.

#### A améliorer

* Les arrêts-minutes doivent être respectés. Chacun doit faire un effort et ne pas laisser de voitures stationnées durablement sur ces places réservées.
* Le Sanctuaire devrait donner aux commerçants un calendrier des pèlerinages pour que les commerces puissent adapter leurs jours d'ouverture et ne pas donner l'image d'un village peu accueillant.
* De même, les cafés-restaurants devraient se coordonner pour avoir toujours un accueil ouvert le soir et le week-end.

#### Suggestions

* Organiser un parcours ludique de magasin en magasin avec chaque fois un jeu devant la vitrine.
* Organiser entre les commerçants des journées ou des week-ends à thème.
* Ouvrir pour la saison d'hiver une patinoire éphémère.
* Pour les décorations de fin d'année, coordonner les efforts de chacun sur un thème avec un parcours dans tout le village.
* Pour faciliter la coordination et les échanges avec les institutions, prévoir une "union des commerçants".

#### Informations

* Un nouveau topo-guide sur le chemin de St Régis est en cours d'élaboration, sortie prévue au printemps.
* Une nouvelle organisation de la récole des poubelles est proposée par le Sytrad et en discussion à la CCVA. Dans l'état actuel de la proposition, la mairie n'y est pas favorable.
* Compte-tenu du retard pris par les signatures des conventions sur le déplacement de la ligne électrique, l'entrepreneur chargé de la démolition de l'hôtel Beauséjour a du réviser son planning. La démolition aura lieu au début de l'année 2022.
* Une journée de nettoyage de l'étang du Grand Lieu est prévue avec la société de pêche, afin de rendre ses abords plus sympathiques pour les jeunes pécheurs.
* Quatre compromis de vente ont été signés sur l'écolotissement. L'appel d'offres pour les travaux d'aménagement a été lancé. Ils devraient démarrer ce printemps.
* Le concours pour la première étape du jeu-monument auprès des étudiants en architecture va être lancé en cette fin d'année pour la construction d'un "petit jeu" en 2022, grâce à la collaboration d'architectes amis du village et à une subvention du programme Leader.
* Des améliorations sont prévues pour l'année prochaine sur l'information locale (affinement du site web, réseaux sociaux, panneau lumineux...). Une demande de subvention est en cours d'instruction à la préfecture.

### Audit sur l'infrastructure des télécommunications (fibre et 4G)

Sur la suggestion de la mairie, deux étudiants en doctorat sont venus faire une analyse des conditions concrètes de l'implantation des réseaux de fibre optique et d'antennes 4G sur notre territoire. Ils ont réalisé une cartographie et des entretiens des acteurs locaux.

Il s'agit d'une réalisation où chacun trouve son compte ; une étude de terrain pour alimenter la thèse des étudiants et un audit gratuit pour la mairie.

Nous aurons ainsi dans quelques mois une photographie plus réaliste de la situation que celle fournie par des opérateurs intéressés, en vue des décisions à prendre pour l'avenir.

## Zoom

### Les champignons... nature, plein air, environnement sauvegardé

{{<grid>}}{{<bloc>}}

![](/media/champignon-2021-1.JPG)

_Personne, sauf lui (et ses parents) ne saura où..._

{{</bloc>}}{{<bloc>}}

A Lalouvesc, les champignons permettent d'associer les plaisirs de la promenade en famille, une balade en forêt suffit pour remplir son panier, mais... mais... il faut quand même se lever de bonne heure ! Laissons-nous tenter et réussir la cueillette : on trouve des cèpes, bien sûr, mais aussi des girolles, des mousserons, des sparassis, des chanterelles grises, des pieds de moutons... simplement parce qu'ils s'épanouissent en toute liberté dans leur espace naturel.

La cueillette de champignons peut être vécue comme une partie de plaisir, c'est surtout une partie de cache-cache pour les connaisseurs autochtones... chacun gardant jalousement ses coins secrets et les confidences sur le climat, l'humidité, la lune... ressemblent souvent à des histoires de Pagnol.

Mais le champignon de chez nous est aussi souvent un complément de ressource pour les petits agriculteurs et autres. Et si l'on y va "mollo" sur les sites qu'on ne dévoile même pas aux meilleurs de ses collègues, on incite aussi les amateurs à faire de même en évitant les "razzias" ravageuses. Et attitude d'une grande importance : on coupe les champignons dans le bois, on les nettoient et les débris doivent être légèrement enterrés ou couverts... tout cela pour ne laisser aucun signe de nos trouvailles aux passants.

{{</bloc>}}{{</grid>}}

![](/media/champignons-2021-2.jpg)Une cueillette réussie et généreuse permet de savourer le champignon toute l'année. La formule séchée est la principale, mais il en existe de nombreuses autres, souvent déjà cuisinées.

Maintenant vous dire où on peut en trouver... eh bien... dans la montagne, chez nous elle est vaste et généreuse...

Gérard Guironnet

### Lalouvesc un air pur... sauf pour l'ozone

Le docteur Eugène Vincent écrivait en 1911 dans son opuscule intitulé [_La Louvesc, station de cure d'air et d'altitude _](http://www.pierre-armand-roger.fr/lalouvesc/vincent_ll/pages/picture_000.html):

> A La Louvesc la population n’est pas nombreuse, c’est un village, il n’y a pas d’usines. Les immenses forêts de sapins contribuent à la purification de l’air par l’absorption de l’acide carbonique et par les flots d’émanations balsamiques, qu’elles projettent dans l’atmosphère, surtout en été. L’absence de poussières et de fumées, l’absence de germes morbides, l’absence de microbes, sont encore assurées par les longs hivers, par l’abondance des neiges, qui entraineraient si il y en avait, les impuretés de l’air, et les fixeraient au sol, où s’opère un travail très actif d’assimilation et d’épuration chimique.

Le docteur était-il trop enthousiaste et optimiste ? En plus de cent ans la qualité de l'air a évolué et, surtout, les mesures sur les conséquences de sa qualité sur la santé sont plus précises.

La [première étude régionale effectuée en France](https://www.santepubliquefrance.fr/determinants-de-sante/pollution-et-sante/air/documents/enquetes-etudes/evaluation-quantitative-d-impact-sur-la-sante-eqis-de-la-pollution-de-l-air-ambiant-en-region-auvergne-rhone-alpes-2016-2018), à partir de modélisation et de données d’exposition aux polluants de l’air sur 2016-2018 en Auvergne-Rhone-Alpes est, en effet, éclairante. Elle a été réalisée par Santé Publique France. Elle ne concerne pas les gaz à effets de serre (pollution anthropique), mais la pollution de l'air ayant un effet direct sur la santé humaine et plus particulièrement les pollutions au dioxyde d'azote (NO2), aux particules fines (PM2,5) et à l'ozone.

L'étude montre qu'aujourd'hui à Lalouvesc, l'air est pur, plus pur qu'en de nombreux endroits de la région... avec pourtant quelques nuances à surveiller.  Vous trouverez ci-dessous des cartes que nous avons centrées sur l'Ardèche. L'étoile rouge marque la situation de Lalouvesc. Pour les cartes complètes de la région, on peut se référer à l'étude en ligne.

{{<grid>}}{{<bloc>}}

![](/media/pollution-2021-azote.jpg)![](/media/pollution-2021-legende-azote.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/pollution-2021-particules.jpg)![](/media/pollution-2021-legende-particules.jpg)

{{</bloc>}}{{</grid>}}

La pollution au dioxyde d'azote (NO2) est principalement due au trafic routier tandis que la pollution aux particules fines provient majoritairement du chauffage individuel au bois. L'étude montre que 7 % de la mortalité de la région est attribuable à l'exposition à long terme aux particules fines soit environ 4 300 décès par an. Plus de 4 % des cancers du poumon, 6 % des accidents vasculaires cérébraux et 5 % des recours aux urgences pour asthme chez les enfants sont attribuables à l'exposition chronique aux particules fines.

Mais, comme on peut le voir sur les cartes, Lalouvesc se trouve dans une zone peu touchée par ces deux pollutions qui concernent principalement les grandes agglomérations, les fonds de vallée et les alentours de la vallée du Rhône.

#### Une pollution à l'ozone à surveiller

En ce qui concerne l’ozone présent dans les couches basses de l'atmosphère (à ne pas confondre avec la "couche d'ozone" qui nous protège des UV), il s’agit d’un polluant secondaire, découlant des autres polluants. L’ensoleillement joue un rôle déterminant dans sa formation. C’est le seul polluant dont les concentrations mesurées ne diminuent pas ces dernières années, vraisemblablement à cause du réchauffement climatique.

Comme on peut le voir sur la  carte ci-dessous, la situation de Lalouvesc est beaucoup moins favorable pour cette pollution, comme d'ailleurs celle de toutes les zones d'altitude.

![](/media/pollution-2921-ozone.jpg)![](/media/pollution-2021-legende-ozone.jpg)

L'ozone serait responsable chaque année, chez les personnes âgées de 65 ans et plus, de 0,6 % des hospitalisations pour causes respiratoires et de l'ordre de 1 % des hospitalisations pour causes cardio-vasculaires dans la région. Il faut se rappeler également que la pollution de l’air impacte l’homme, mais aussi le vivant et la biodiversité.

Nous subissons pour une bonne part le contrecoup de la pollution des agglomérations et de la vallée du Rhône qui se transforme en ozone sous l'effet du soleil. Mais cela ne nous empêche pas de réduire nos propres sources de pollution. Il faudra y réfléchir.

### Trail des Sapins : ils ont répondu présents !

Alerte météo orange pour le 30 octobre, jour du Trail des Sapins... une couleur de saison pour Halloween, mais bien des inquiétudes pour la sécurité de la course !

![](/media/trail-des-sapins-2021-course-7.jpg "Photo OT")

Au grand soulagement des organisateurs, le vent s'est calmé dans l'après-midi et, malgré le froid et la pluie, (presque) tous les coureurs ont répondu présents dans la bonne humeur et le respect sans faille des règles sanitaires. Il y eut :

* au 46 km, 87 coureurs au départ sur 102 inscrits,
* au 21 km, 175 partants sur 188,
* au 12km, 128 sur 143,
* et au 7 km 112 au départ sur 123, sans compter les nombreux randonneurs,

... soit 90% de participation pour la course. A l'évidence après l'annulation de 2020 et une année pénible l'attente était forte et le Trail des Sapins est devenu une "classique" incontournable. Belle récompense pour les organisateurs et les bénévoles qui ont tenu bon malgré les menaces sanitaires et météos.

{{<grid>}}{{<bloc>}}

![](/media/trail-des-sapins-2021-course-5.jpeg "Photo OT")

{{</bloc>}}{{<bloc>}}

![](/media/trail-des-sapins-2021-course-1.JPG "Photo OT")

{{</bloc>}}{{</grid>}}

#### Les élus étaient là

![](/media/elus-trail-des-sapins-2021.jpeg "O. Amrane, J. Burriez, A. Delhomme, S. Roche, Fr. Garayt")

Les élus aussi étaient là, outre le maire et les conseillers du village, nous avons eu le plaisir de recevoir : Olivier Amrane (président du Conseil départemental), Frédéric Garayt (maire de St Laurent du Pape), qui tous les deux ont parcouru le 7 km en moins de 47min, un temps tout à fait honorable, Virginie Ferrand (maire de Vocances et conseillère régionale), Laeticia Bourjat (maire de Vaudevant et conseillère départementale), Stéphane Roche (maire de Lafarre), Jacques Dubay (maire de St Péray et conseiller régional) et Delphine Comte (maire de Colombier) qui a aussi couru le 7km.

Merci à toutes et tous pour avoir répondu présents. Ainsi en plus de la reconnaissance sportive, c'est une reconnaissance institutionnelle : le Trail des Sapins est maintenant installé sur le territoire comme un événement avec lequel il faut compter.

#### Un énorme bravo aux organisateurs et aux bénévoles !

![](/media/trail-des-sapins-2021-course-6.jpg "Le staff du Trail - Photo OT")

## Suivi

### Départ du père Olivier

Après un an au service du pèlerinage à la Maison Saint Régis, le Père Olivier de Framond a rejoint la Maison de « Fleurs des Neiges » à Saint Gervais en Haute Savoie.

Nous le remercions pour son sourire, sa bienveillance et son esprit d'ouverture. Il nous avait laissé dans le Bulletin du mois d'octobre un [très joli cadeau d'au revoir](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/#les-saisons-%C3%A0-lalouvesc-sur-lalbum-du-p%C3%A8re-olivier).

### Rallye Exbrayat

Après plus d'un an d'absence pour cause de pandémie, les voitures de collection étaient de retour à Lalouvesc, le 17 octobre. L'occasion d'un [joli reportage photos de Robert Faurie](https://www.facebook.com/robert.faurie/posts/4507166919349186) qu'il a partagé sur le groupe Facebook des[ Amis de Lalouvesc](https://www.facebook.com/groups/47779411002). Merci à lui !

{{<grid>}}{{<bloc>}}

![](/media/rallye-exbrayat-2021-photo-r-faurie-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/rallye-exbrayat-2021-photo-r-faurie-1.jpg)

{{</bloc>}}{{</grid>}}

### La saison à l'Abri du pèlerin

L’AG, ce lundi 25 octobre, de l’ « Avant-Garde Louvetonne » qui gère l’Abri du Pèlerin nous a permis de faire le bilan de la saison : ce sont 455 personnes qui ont été accueillies entre mai et septembre à l’Abri pour un total de 1080 nuitées. C’est une baisse par rapport à l’année précédente (1395 nuitées) mais baisse modérée si l’on considère l’effet COVID !

![](/media/chemin-ste-therese-2021.JPG)

Ce sont les randonneurs sur le chemin de Saint Régis qui assurent la plus grande régularité tout au long de ces mois d’ouverture, en solitaire, en couple, entre amis. Ce peut être à pied mais aussi en VTT ou à cheval !

Des familles sont heureuses d’y être hébergées en autonomie pour leur cuisine.

L’Abri a accueilli aussi des groupes constitués comme des aumôneries, des scouts – heureux de planter leurs tentes dans la prairie de Mme Bélanger - et même une petite compagnie de théâtre … Une association d’«éducation renforcée » vient régulièrement avec leurs stagiaires.

Il faut signaler la belle mobilisation de Louvetous pour la remise en marche de l’Abri en début de saison, avec le renouvellement des matelas : cela a été possible grâce aux municipalités : celle de St Félicien (dons de mobilier venant l’ancienne maison St Joseph) et celle de Lalouvesc pour le prêt de véhicules et la participation du personnel. Un grand merci !

{{<grid>}}{{<bloc>}}

![](/media/abri-du-pelerin-2021-1.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/abri-du-pelerin-2021-2.JPG)

{{</bloc>}}{{</grid>}}

Les garages ont aussi été désencombrés et aménagés.

D’autres rénovations sont envisagées au cours de l’hiver pour permettre à l’Abri de continuer son accueil certes rustique mais sympathique, encourageant la convivialité et l’entraide, dans l’esprit du Père Bonnard. Construit à partir de 1921, l’Abri a ouvert ses portes en 1923 : nous nous approchons du centenaire !

Père Michel Barthe-Dejean  
(Photos de deux pèlerins du chemin de Ste Thérèse Couderc)

### Club des 2 clochers : AG extraordinaire et après-midi récréative

Le club des 2 clochers renouvelle son bureau et sa présidence, n'hésitez pas à prendre votre carte et à présenter votre candidature. Les élections auront lieu au cours d'une assemblée générale extraordinaire le jeudi 25 novembre à 14h au CAC en présence de Monsieur Jean-Pierre Ferlay, président de la section nord-Ardèche d'Ensemble et solidaires de l'Union Nationale des Retraités et des Personnes Agées (UNRPA). Passe sanitaire ou test PCR récent exigé.

Jeudi 18 novembre : Après-midi récréative, chansons des années 60, goûter, musique. A St Victor Salle Pouyol co-voiturage. Inscription avant le 4 novembre au club ou à la boulangerie Brunel. 5 €. Passe sanitaire ou test PCR récent exigé.

### Assises pour le développement de la vie associative en Ardèche

Le Département de l'Ardèche organise entre le 23 novembre et le 9 décembre 2021 des Assises pour le développement de la vie associative sur l'ensemble du territoire qui réuniront les associations, les élus et les partenaires.

La réunion concernant notre territoire aura lieu le mardi 7 décembre de 18h à 20h30, salle des fêtes route Levert  07100 Annonay. [Inscription en ligne obligatoire avant le 7 novembre](https://forms.office.com/pages/responsepage.aspx?id=BZ1Qx8dpg0Kx_VqhyaKVps6HZScACTBJoFuVU1PGBSNUNUhDVklKNUwzWjhEVjM5ODI0VFpCTUlOWS4u).

### Naissance

Bienvenue au jeune Louvetou, François-Régis Trébuchet, né le 3 octobre dernier. Et félicitations à ses parents !

### Dans l'actualité le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois d'octobre qui restent d'actualité.

#### Premières visites du club du Carrefour des arts

23 octobre 2021  
Une dizaine d'adhérents ont visité le Musée d'art moderne et contemporain et des galeries et ateliers d'artistes à St Etienne [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/premieres-visites-du-club-du-carrefour-des-arts/ "Lire Premières visites du club du Carrefour des arts")

#### Votre enfant entre bientôt au collège...

21 octobre 2021  
Renseignez-vous. Portes ouvertes au collège de Satillieu du 18 au 20 novembre [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/votre-enfant-entre-bientot-au-college/ "Lire Votre enfant entre bientôt au collège...")

#### Des offres intéressantes de l'Office du tourisme

20 octobre 2021  
Réductions pour le Safari de Peaugres et pour la location de vélos électriques [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/des-offres-interessantes-de-l-office-du-tourisme/ "Lire Des offres intéressantes de l'Office du tourisme")

#### Adressage : révisions et précisions

12 octobre 2021  
La société chargée de réviser les coordonnées GPS des adresses postales est intervenue en début de semaine [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/adressage-revisions-et-preisions/ "Lire Adressage : révisions et précisions")

#### L'appel d'offres pour la viabilisation de l'écolotissement est lancé

7 octobre 2021  
Bientôt quatre compromis de ventes signés, les travaux peuvent démarrer [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/l-appel-d-offres-pour-la-viabilisation-de-l-ecolotissement-est-lance/ "Lire L'appel d'offres pour la viabilisation de l'écolotissement est lancé")

#### Square… changement de décor !

2 octobre 2021  
Araignées, courges, fantômes ont remplacé les vaches [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/square-changement-de-decor/ "Lire Square… changement de décor !")

#### Le réchauffement climatique mesuré à Lalouvesc

1 octobre 2021  
L'OT présente 50 ans de météo France à l'occasion de la fête de la science... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-rechaufement-climatique-mesure-a-lalouvesc/ "Lire Le réchauffement climatique mesuré à Lalouvesc")

## Feuilleton : Les quatre éléments

Comme promis, la suite du feuilleton des enfants de l'école St Joseph. Rappel : le [premier épisode](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/#_les-quatre-%C3%A9l%C3%A9ments_) a été publié dans le Bulletin du mois d'octobre.

### Chapitre 2 : La pierre TERRE

> Pour commencer leur quête ils décident de partir à la recherche de la pierre qui se trouve dans le temple de la fourmi des galeries au cœur de la forêt Amazonienne.

![](/media/feuilleton-ch-2-2021-1.jpg)

> Pour cela, ils doivent se rendre au bord de la mer. La route est longue et en arrivant sur la côte, les deux enfants trouvent un bateau dont la coque est fendue. Juste à côté du bateau, ils remarquent une malle. Dans cette malle, ils découvrent un message :
>
> A l’intérieur du bateau se trouve un passage secret. Pour l’activer vous devez résoudre une énigme et il faut que ce soit un jour de pleine lune.
>
> Si l’énigme est résolue, les enfants pourront être directement téléportés vers le temple de la forêt Amazonienne sans avoir à traverser tout l’océan Atlantique en bateau.
>
> Voici donc l’énigme :
>
> > « Le matin, il marche à quatre pattes, l’après-midi, il marche sur deux pattes et le soir il marche sur trois pattes… de qui s’agit-il ? »
>
> Les enfants réfléchissent longtemps mais ils trouvent finalement la réponse à l’énigme ! Il s’agit de l’être humain bien sûr ! (Au début de sa vie quand il est bébé, il marche à quatre pattes, puis après il marche sur ses deux jambes et à la fin de sa vie, il marche sur ses deux jambes mais avec l’aide d’une canne !)
>
> Les voilà donc partis pour le temple de la fourmi des galeries pour trouver la pierre qui abrite l’élément **TERRE**.

{{<grid>}}{{<bloc>}}
![](/media/feuilleton-ch-2-2021-5.jpg)
{{</bloc>}}{{<bloc>}}
![](/media/feuilleton-ch-2-2021-4.jpg)
{{</bloc>}}{{<bloc>}}
![](/media/feuilleton-ch-2-2021-6.jpg)
{{</bloc>}}{{<bloc>}}
![](/media/feuilleton-ch-2-2021-3.jpg)
{{</bloc>}}{{</grid>}}

> Après avoir été téléportés directement vers la forêt Amazonienne, ils arrivent devant un arbre gigantesque. Sur l'arbre il y a un bouton. Les enfants décident d'appuyer sur sur ce bouton et ils entendent soudain une chanson :

> <iframe width="560" height="315" src="https://www.youtube.com/embed/yF1hqIZUHhw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

> Ani couni chaounani  
> Ani couni chaounani  
> Awawa bikana caïna  
> Awawa bikana caïna  
> Eiaouni bissinni
>
> Cette chanson va les guider dans le labyrinthe qui mène au temple de la fourmi des galeries.
> {{<grid>}}{{<bloc>}}
> ![](/media/feuilleton-ch-2-2021-8.jpg)
> {{</bloc>}}{{<bloc>}}
> ![](/media/feuilleton-ch-2-2021-7.jpg)
> {{</bloc>}}{{</grid>}}
> En arrivant devant le temple ils frappent à la porte.
>
> La fourmi demande :
>
> « Mais qui vient frapper à ma porte ? »
>
> Les enfants répondent :
>
> « Nous venons chercher l'élément Terre que vous abritez dans votre temple. Nous avons pour mission de sauver un cheval blessé et pour cela nous avons besoin de regrouper les quatre éléments : la Terre, le Feu, l'Eau et l'Air . S'il vous plaît, pouvez-vous nous donner l'élément Terre pour que nous puissions sauver le cheval ? » {{<grid>}}{{<bloc>}} ![](/media/feuilleton-ch-2-2021-2.jpg)
> {{</bloc>}}{{<bloc>}}
> ![](/media/feuilleton-ch-2-2021-9.jpg)
> {{</bloc>}}{{</grid>}}
> La fourmi est émue par l'histoire des enfants et leur donne la pierre. Mais les enfants doivent faire vite car, pendant que la pierre est en leur possession, le temple sera plongé dans les entrailles de la Terre jusqu'à ce qu'ils la rapportent.
>
> Ça y est, les enfants ont la première pierre !!!

> En route maintenant pour le temple du Piton de la Fournaise, à la Réunion, pour trouver le deuxième élément :
>
> Le **FEU**.

A suivre...