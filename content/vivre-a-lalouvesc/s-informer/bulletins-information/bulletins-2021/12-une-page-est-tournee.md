+++
date = 2021-06-29T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Juillet 2021"
title = "12 - Une page se tourne"
weight = 1

+++
Avec l'ouverture de la saison estivale 2021, une saison au programme bien rempli, c'est aussi une page de l'histoire du village qui se tourne. Avec la vente du bâtiment Sainte Monique, les premiers travaux du Comité de développement, la rénovation complète du camping, l'appel à imaginer les contours d'un jeu-monument sur le Parc du Val d'Or et tous les autres projets encore dans les cartons, c'est un nouveau chapitre de l'histoire du village que nous écrivons tous ensemble. Dans ce bulletin vous en trouverez quelques prémisses, et, comme toujours, aussi plein d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-12-1er-juillet-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Nous voilà arrivés en haute saison, juillet et août, les deux mois les plus significatifs de notre activité économique de l'année.

Le camping présente un taux d'occupation similaire aux autres années. Il va se remplir à sa capacité maximum dès que les écoles fermeront.

Accueillons du mieux possible nos touristes, ils sont notre fonds de commerce, donnons-leur l'envie de revenir passer un moment avec nous, chez nous.

Il nous reste le plan de circulation à acter, les modifications ont été proposées par un groupe du Comité consultatif vie locale, association d'élus et de non-élus. L'entreprise sollicitée pour les travaux nous a informés qu'il y aurait un peu de retard. Il devait intervenir fin juin, ce sera dans la première semaine de juillet. Les travaux de l'antenne TDF avance bien, le socle du pylône est déjà coulé, il sèche. Bientôt la masse métallique s'érigera dans le ciel.

Tout est prêt pour passer un bon été, le temps n’est pas tout à fait au rendez-vous mais ça va venir.

Passez toutes et tous un bel été, sachez que les services de mairie et moi-même sont à votre disposition pendant l'été.

Amitiés, Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Subvention pour la démolition de l'hôtel Beauséjour

Nous avons reçu la notification de la subvention de la région pour la démolition de l'hôtel Beauséjour qui est donc, comme espéré, subventionnée à 80%.

C'était la dernière réponse attendue concernant les demandes faites à l'automne dernier. Il a fallu se battre un peu, mais le résultat est là : carton plein.

### Compromis signé pour la vente de Sainte Monique

Suite à la proposition d'achat d'un promoteur, le Conseil municipal a autorisé le Maire à signer un compromis de vente du bâtiment Sainte Monique pour la somme de 225.000 €. La signature a eu lieu chez le notaire le 29 juin. Voir aussi plus loin, la présentation du projet.

### Comité de développement

La première réunion plénière du Comité de développement s'est tenue le 18 juin en Mairie. Son compte rendu est [disponible ici](https://www.lalouvesc.fr/media/reunion-du-comite-de-developpement-juin-2021.pdf).

C'est le début d'un processus de réflexions et de décisions sur l'avenir et la reconfiguration du village auquel tous les Louvetous sont invités à participer. Dans un premier temps, il est demandé à ceux qui le souhaitent de remplir un [formulaire en ligne](https://docs.google.com/forms/d/e/1FAIpQLSditZ6DVizWDO4FVqG5P0VDO43Fii4Tpk4v9bs5fn_6HbSlcA/viewform?usp=pp_url).

La première étape est de redessiner le Val d'Or, voir ci-dessous.

### Eau-assainissement

Plusieurs réponses positives ont déjà été reçues aux demandes de subvention pour le schéma directeur, le prolongement du réseau d'assainissement et la liaison entre les réservoirs.

Le schéma directeur est bien avancé. Les appels d'offres ont été lancés pour les premières tranches de travaux qui devraient démarrer cet automne.

## Zoom

### Comment imaginer le futur du Val d'Or ?

Au moment de la campagne électorale, l'équipe municipale actuelle a proposé de construire un nouveau jeu-monument sur le Val d'Or en profitant de la nouvelle ouverture offerte par la destruction de l'hôtel Beauséjour. Mais cela reste une idée. Si nous voulons la réaliser, il faut maintenant en préciser les contours pour pouvoir à l'automne lancer les demandes de subventions puis les appels d'offres correspondant.

![](/media/appel-aux-dons-basilique-lalouvesc.png)

Construire un monument, même s'il s'agit d'un jeu, n'est pas anodin. Nous avons déjà à Lalouvesc un monument, face aux Alpes, inscrit dans l'histoire du village, la Basilique, construite par l'architecte Pierre Bossan avec la contribution des villageois et des fidèles ([wikipédia](https://fr.wikipedia.org/wiki/Basilique_Saint-R%C3%A9gis_de_Lalouvesc)). Il s'agit maintenant d'en construire un second, bien sûr plus modeste, mais tourné vers l'autre flanc du col de Lalouvesc, les Cévennes, et tourné vers les nouveaux défis des temps nouveaux sans renier le passé du village.

L'idée serait d'illustrer dans un jeu monumental pour les enfants la thématique de la forêt, qui entoure le village et participe à son économie avec tous les métiers de la filière bois. Cette thématique s'inscrit dans la transformation du tourisme, tourné vers la nature, éco-responsable et bienveillant. Elle trouve aussi un écho dans les propos de l'encyclique _Laudati Si'_. On trouvera une présentation du projet sur cette [page du site](/projets-avenir/gerer-le-bien-commun/un-jeu-monument-sur-le-parc-du-val-d-or/).

![](/media/carte-postale-jeu-monument-1.jpg)

![](/media/carte-postale-jeu-monument-2.jpg)

Pour récolter des premières idées et suggestions, une carte postale a été dessinée par le graphiste du CAUE. Elle sera largement distribuée tout le long de l'été. En la remplissant, vous pourrez ainsi déjà envoyer au Comité de développement vos propositions, en attendant une concertation plus approfondie.

### Sainte Monique, d'une vocation à l'autre

Le bâtiment Sainte Monique, rue de la Fontaine, est emblématique d'un passé révolu du village : un souvenir taillé dans la pierre, témoin de l'accueil en colonie de vacances de très nombreux enfants à la belle saison. Il a été construit à l'initiative du Père Féraud pour accueillir "les colonies d'Oran" dont l'histoire a été racontée par Jean-Claude Regal dans le n°4 de la revue de l'Alauda (2016) dont on pourra consulter un extrait ci-dessous.

![](/media/alauda-ste-monique.jpg)

Voir aussi l'[histoire du village](/vivre-a-lalouvesc/histoire-identite/histoire-de-lalouvesc...de-la-prehistoire-a-nos-jours/) sur ce site.

Le bâtiment a été racheté par la Commune en 1974. Mais on doit constater que, faute sans doute de moyens, cette dernière n'a pas réussi à lui trouver une nouvelle vocation malgré plusieurs projets. Il a simplement servi d'entrepôt pour du matériel de la Commune et celui d'associations.

Aujourd'hui, les temps ont changé et la Commune vient de signer un compromis de vente avec un promoteur qui a le projet d'y aménager des appartements modernes. On trouvera ci-dessous une modélisation du projet.

{{<grid>}} {{<bloc>}}

**Aujourd'hui**

![](/media/sainte-monique.JPG)

{{</bloc>}}{{<bloc>}}

**Projet du promoteur**

![](/media/ste-monique-projet-promoteur.jpg)

On peut consulter[ ici le plan du projet](/media/ste-monique-plan-du-promoteur.pdf).

{{</bloc>}} {{</grid>}}

### Un Comité toujours à la fête

C’est l’été, un vent de liberté souffle sur Lalouvesc et les bénévoles du Comité des Fêtes veulent en profiter ! La motivation est bien présente chez chacun d’eux.

#### Le feu de la Saint Jean

Après deux tentatives stoppées pour cause de pluie, les bénévoles du Comité des Fêtes et de la Lyre Louvetonne ont enfin pu dresser le chapiteau et installer le parquet. Encore quelques difficultés à l’allumage du feu de joie… Peu importe car c’est dans une ambiance musicale et très conviviale que la Fête de la Saint Jean a été honorée à Lalouvesc. Tradition oblige et… respectée !

{{<grid>}} {{<bloc>}}

![](/media/feu-st-jean-2021-1.jpg)

{{</bloc>}}{{<bloc>}}  
![](/media/feu-st-jean-2021-3.jpg)

{{</bloc>}} {{</grid>}}

#### Les décorations

Un grand merci à toutes celles et ceux qui ont travaillé à l’élaboration de notre gentil troupeau de vaches qui paissent tranquillement sur le square ! Elles sont les reines du village et photographiées jusqu’à l’éblouissement ! La mobilisation a été vaste et intense mais nous sommes récompensés par le succès rencontré par cette exposition.

![](/media/decoration-2021-1.jpg)  
Une nouvelle fois, nous remercions et félicitons les enfants de l’école et les résidents de la maison de retraite pour leur soutien sans faille lorsque nous les sollicitons pour se joindre à nous. Bravo à toutes et tous !

#### La brocante

Tout étant sous contrôle du côté de notre cheptel bovin, place à présent à la préparation de la brocante. Elle est ardemment attendue de toutes parts. Les appels téléphoniques, les mails sont nombreux : « la brocante de Lalouvesc aura-t-elle lieu ? » OUI elle aura lieu !

{{<grid>}}{{<bloc>}}

Les bulletins d’inscription ont été envoyés et déjà nous recevons en retour ceux des plus adeptes de cet incontournable évènement. Vous pouvez le faire vous-mêmes en imprimant et renvoyant ce [bulletin d'inscription](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/brocante-bulletin_inscription_2021.pdf).  
Nous adapterons son organisation. Pour ce faire, et comme l’année dernière, nous aurons besoin de nombreux bénévoles. A bon lecteur… bienvenue le 05 septembre parmi nous !  
Nous avons renouvelé notre confiance à la [Batucada Samba Démonts](https://www.facebook.com/123488528384236/videos/740738983169875) (Ouest Lyonnais) pour animer cette journée.

{{</bloc>}}{{<bloc>}}  
![](/media/brocante-3.jpg)

{{</bloc>}} {{</grid>}}

#### Le Trail des sapins

{{<grid>}} {{<bloc>}}

![](/media/trail-lalouvesc-2.jpg)

{{</bloc>}}{{<bloc>}}  
C’est parti ! Entendons-nous bien : la préparation du trail est partie. La ligne de départ pour les sportifs sera ouverte seulement le 30 octobre.

Les contacts que nous avons noués avec nos partenaires sont si chaleureux et encourageants que nous ne lâcherons rien pour faire de cette journée une clôture de saison haute en sport mais en aussi en convivialité.

{{</bloc>}} {{</grid>}}

Nous préparons ces deux évènements en laissant place dans l’immédiat à d’autres très belles manifestations qui enchanteront notre village pour un été que nous souhaitons à toutes et à tous festif et ensoleillé. Bon vent de liberté et de légèreté ! Que vive gaiement et intensément Lalouvesc cet été !

Le Comité des Fêtes est toujours joignable, soit par téléphone au 07.66.67.94.59, par mail comitedesfetes.lalouvesc@gmail.com ou par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.

### Lalouv'estival, un programme bien rempli !

On ne s'ennuie pas l'été à Lalouvesc, il se passe toujours quelque chose ! Ci-dessous le programme des principales manifestations de juillet.

{{<grid>}} {{<bloc>}}

* **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Action Communale tous les jours de 14h30 à 18h30
* **Exposition _La matière habitée_** à la chapelle Saint Ignace
* Les lundis : **Visite du jardin en permaculture** au Mont Besset
* Les lundis, mercredis, jeudis, samedis et dimanches à 9h : **Cinéma** à l'Abri du Pèlerin
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la **bibliothèque**
* Du 24 juillet au 6 août l'AFTL propose des stages de **tennis**

##### Concerts, cinéma et événements

* 2 juillet : Le **Temps des émerveilleurs**, balade surprise dans le village
* 3 juillet : exposition, ouverture du **Carrefour des Arts** au Centre d'Animation Culturelle
* 10 et 12 juillet : cinéma, **Poly**, [bande annonce](https://youtu.be/_RWTmSpJoLg)
* 11 juillet : cinéma, **Adieu les cons**, [bande annonce](https://youtu.be/hVV1BpNV6m0)
* 13 juillet : bal et **feu d'artifice**
* 15 et 18 juillet : cinéma, **The Father** (vf), [bande annonce](https://youtu.be/HVCs_ahGpls)

{{</bloc>}}{{<bloc>}}

* 17 juillet : cinéma, **Antoinette dans les Cévennes**, [bande annonce](https://youtu.be/qsbBWaCKlW4)
* 18 juillet : **Fête du livre** à la bibliothèque
* 19 juillet : cinéma, **La fine fleur**, [bande annonce](https://youtu.be/Fy2_VLFItHs)
* 21 juillet : concert, **Groupe Kimya**, _Between mist and sky_ à la Grange de Polly
* 21 et 24 juillet : cinéma **Envole-moi**, [bande annonce](https://youtu.be/rVckCelfruE)
* 22 et 25 juillet : cinéma, **Nomadland** (vf), Lion d'or à la Mostra de Venise 20201, Golden Globe du meilleur film dramatique et Oscar du meilleur film en 2021. [bande annonce](https://youtu.be/mGjTyjGHnyE)
* 26 juillet : concert, **Guy Angelloz** à la Basilique
* 26 juillet : cinéma, **Profession du père,** avant-première, de Jean-Pierre Ameris avec Benoit Poelevorde, Jules Lefebvre.. [bande annonce](https://youtu.be/mlvzDyqCZWM)
* 28 juillet : **Bach et associés** à St Romain d'Ay ([Promenades musicales de Lalouvesc et du Val d'Ay](http://www.promenadesmusicales-lalouvesc.com/))
* 28 et 31 juillet : cinéma, **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)
* 29 juillet et 1er août : cinéma, **Médecin de nuit**, [bande annonce](https://youtu.be/FandAD1hTbY)
* 30 juillet : concert, **La voix et la musique** à Satillieu ([Promenades musicales de Lalouvesc et du Val d'Ay](http://www.promenadesmusicales-lalouvesc.com/))

{{</bloc>}} {{</grid>}}

## Suivi

### Un nouveau spectacle théâtral

{{<grid>}}{{<bloc>}}

![](/media/ph-bosseau-2021.jpg)

{{</bloc>}}{{<bloc>}}

_Confinement, quoi d’autres ?_ par Philippe Bosseau

Le spectacle est né lors du premier confinement après le constat du comportement de nos concitoyens qui ont un moment oublié le fameux « vivre ensemble » si important en ces pénibles moments.

Votre aimable saltimbanque 100% confinement, au travers du prisme de son humble existence, dépeint notre société avec panache, avec humour et totale bienveillance en égratignant quelques-uns de nos semblables et en mettent en évidence certains de nos travers notamment durant cette pandémie.

Toujours tourné vers l’avenir et donc la construction du monde de demain, ce spectacle a pour but de vous divertir et de vous faire réfléchir sur, justement, ce monde d’après.

Séance au camping début août (date à préciser).

{{</bloc>}} {{</grid>}}

### Fête du livre le 18 juillet

![](/media/fete-du-livre-2021.jpg)

### Du côté de Lalouvesc n°9 (Bulletin de l'Alauda)

Le prochain numéro du Bulletin de l'Alauda sur l'histoire du village devrait être disponible pour 10 € dans les commerces dans la seconde quinzaine de juillet.

![](/media/e-gerber-jl-fanget-a-linder-jcl-regal-2020.jpg)

On y trouvera un article Mme Eva Geber (à gauche sur la photo prise à son passage à Lalouvesc), auteure et journaliste renommée en Autriche, sur la photographe d’Art Dora Kallmus ("Madame d’Ora") réfugiée à Lalouvesc entre 1942 et 1944. Madame d’Ora est l’une des plus grandes photographes de l’entre-deux guerres qui a côtoyé les plus grand artistes de l’époque. Son œuvre est mondialement reconnue. Nous l'avions évoquée dans le [Bulletin du mois de novembre 2020](/media/bulletin-d-information-n-5-novembre-2020-lalouvesc.pdf)  
Au sommaire encore, bien sûr, les cartes postales anciennes de Pierre Roger, qui cette année opportunément évoquent le Val d'Or, mais aussi la suite du passionnant article de Myriam Mosnier sur les bâtisseurs italiens (Entreprise Valeriani) et quelques autres petites pépites locales.

### Le CAC a mis ses beaux atours pour accueillir les œuvres des artistes

Comme chaque année les bénévoles ont travaillé d'arrache-pied pour préparer les salles d'exposition et installer les œuvres.

{{<grid>}}  {{<bloc>}}

![](/media/carrefour-des-arts-installaion-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-installaion-2021-2.jpg)

{{</bloc>}} {{</grid>}}

Inauguration le 3 juillet en fin de matinée, ouverture au public à 14h30 le même jour. Rappel des heures d'ouverture : tous les jours de 14h30 à 18h30 dimanche et jours fériés de 14h30 à 19h. Entrée libre.

### Une "journée paroissiale"... du tonnerre

Nous avons reçu ce témoignage d'une fidèle de la paroisse.

Tout s’annonçait "super bien".... Parc des Pèlerins bien fauché, podium bien balayé, météo des plus clémentes en tout point... Et dès 10 h, des fidèles, toutes générations confondues, montaient au-dessus de notre Basilique "aux deux Clochers". Puis vint la procession des célébrants présidée par notre Curé, le Père Armand Manoha, encadré de nos deux nouveaux Pères Jésuites, Olivier et Michel, accompagnés de plusieurs autres Prêtres. Un "joli parterre" de fidèles très recueillis assis dans l'herbe (et la sono qui marchait à peu près). Une très belle homélie du Père Recteur : _c'est ce qui s'appelle "parler vrai"... commentaire de l'évangile "la tempête apaisée" et "passons sur l'autre rive"_!..

Et c'est ce qui nous est arrivés ! Le ciel petit à petit s'est obscurci... et des roulements de tonnerre, discrets, se rapprochaient. Quelques prudentes brebis commencèrent à quitter l’enclos. On en était à la "prière universelle" lorsque, en écho, éclata un franc coup de tonnerre. Le célébrant annonça que nous allions poursuivre à la Basilique En moins de temps qu'il n'en faut pour le dire, tout fut délocalisé. En entrant sous les voûtes protectrices, Carole notre fidèle organiste - arrivée sûrement sur les "ailes du vent"- était déjà au clavier et nous illustrait la "tempête apaisée" !

![](/media/basilique_lalouvesc.JPG)

Paisiblement la Basilique fut remplie, tous et chacun le masque sur le nez. Nous étions "passés sur l'autre rive" ! Le verre de l'amitié et le pique-nique se sont aussi passés à l'abri. Le préau de la maison Saint Régis retrouvait là une de ses belles vocations. Très joyeuse et fraternelle ambiance, et belle image d'Eglise (dehors, il pleuvait à seaux !) Après-midi, non pas "récréative" mais très "profonde". En deux groupes, nous avons partagé sur le message de Saint-Régis dans nos vies. Puis, explications des vitraux (toujours très intéressantes)! et nous avons chanté les vêpres accompagnés du Père Manoha.

C’était aussi la "Fête des Pères"! Très bonne fête donc à nos "Chers Pères"!... à nos "Chers Pasteurs" que l'on sentait heureux au milieu de leurs joyeuses "brebis".

Le soleil est revenu en toute fin d’après-midi... Alléluia ! Béni soit Saint-Régis !

Edith Archer

### 300 enfants visitent le village

Le village a retrouvé, l'espace d'une journée, les autocars et les foules d'enfants qui enchantaient ses étés autrefois.

![](/media/groupe-enfants-st-chamond-juin-2021.jpeg)

Les collégiens de sixième des établissements scolaires jésuites de Sainte Marie Lagrange à St Chamond étaient de passage vendredi 25 juin dans la patrie de Saint Régis pour une visite guidée.

### Deux cavalières de Lalouvesc au concours Club 3

Coline Fourezon et Sarah Blachier sont deux cavalières qui s’entraînent au centre équestre du Domaine de Foncouverte de Lalouvesc toute l’année. Pour la première fois elles ont participé au concours Club 3 aux écuries O’Hara à L’hôpital le grand, dimanche 27 juin 2021. Les deux chevaux qui ont été leur partenaire étaient Quetzal et Cerda.  
Cette participation a nécessité un entrainement, une préparation ainsi qu’une certaine organisation  facilités et encadrés par le coach Anne de Fontcouverte.  
Le parcours du concours comprenait onze obstacles. Elles ne devaient ni chuter, ni tomber des obstacles et leurs chevaux ne devaient pas refuser de sauter sinon des points de pénalités leur étaient attribués.  
Le classement se faisait sur le meilleur temps, révisé en fonction des pénalités et sur le premier quart. Après avoir passé deux tours Sarah a été classé quatrième et Coline septième sur un groupe de quatorze participants.

{{<grid>}}{{<bloc>}}

![](/media/concours-hippique-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/concours-hippique-2021-2.jpg)

{{</bloc>}}{{</grid>}}  
Après l’effort le réconfort le cheval a eu droit à une friandise, une bonne douche et un brossage avant de grimper dans le van pour le retour à Lalouvesc. Coline et Sarah avaient des images plein les yeux sur le chemin du retour.  
On peut être fier d’avoir dans notre village un centre où nos enfants peuvent pratiquer l'équitation.  
Félicitations à ces deux jeunes cavalières ainsi qu’à Cerda et Quetzal, les chevaux, pour leur première participation à ce concours ! Et longue vie à Fontcouverte !

Nathalie Desgrand Fourezon

### Bienvenue à deux nouvelles Louvetounes !

Maëlya Serpolet est née le 30 mai. Agathe De Jésus est née le 16 juin.

Bienvenue à elles ! Et félicitations aux heureux parents !

### Réaction au plan de circulation

Un[ plan de circulation et de stationnement](/media/plan-de-circulation-et-de-stationnement-juin-2021.pdf) a été présenté dans le Bulletin du mois de juin.

Certains résidents de la rue de la Fontaine ont réagi, contestant l'interdiction de stationnement dans leur rue. Ils ont présenté leurs arguments dans [un courrier au Maire](/media/lettre-residents-stationnement-rue-de-la-fontaine.pdf). Le Maire leur[ a répondu](/media/reponse-du-maire-stationnement-rue-de-la-fontaine.pdf) en expliquant les raisons de ce choix.

### Dans l'actualité le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de mai qui restent d'actualité.

**Compte rendu du Conseil municipal**  
27 juin 2021  
Conseil du 24 juin 2021 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/compte-rendu-du-conseil-municipal-2/ "Lire Compte rendu du Conseil municipal")

**Résultat du second tour des élections régionales et départementales**  
26 juin 2021  
Abstention toujours forte, résultats confirmés [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/resultat-du-second-tour-des-elections-regionales-et-departementales-1/ "Lire Résultat du second tour des élections régionales et départementales")

**Résultat du premier tour des élections régionales et départementales**  
19 juin 2021  
Une participation faible mais supérieure à la moyenne nationale, un choix net... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/resultat-du-premier-tour-des-elections-regionales-et-departementales/ "Lire Résultat du premier tour des élections régionales et départementales")

**Aller au ciné à Lalouvesc !**  
18 juin 2021  
Tout le programme de juillet. Excellents choix ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/aller-au-cine-a-lalouvesc/ "Lire Aller au ciné à Lalouvesc !")

**Première tranche de travaux sur le cimetière**  
16 juin 2021  
Le mur de soutènement se répare, deux mètres par deux mètres [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/premiere-tranche-de-travaux-sur-le-cimetiere/ "Lire Première tranche de travaux sur le cimetière")

**La dalle de l'antenne 4G a été coulée**  
16 juin 2021  
L'antenne doit être opérationnelle le 14 juillet au plus tard. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-dalle-de-l-antenne-4g-a-ete-coulee/ "Lire La dalle de l'antenne 4G a été coulée")

**Stages de tennis du 24 juillet au 6 août**  
15 juin 2021  
L'AFTL propose des stages de formation ou de perfectionnement au tennis [Lire la suit](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/stages-de-tennis-du-24-juillet-au-6-aout/ "Lire Stages de tennis du 24 juillet au 6 août")e

**L'orchestre philharmonique départemental de l'Ardèche en concert les 26 juin et 6 juillet**  
15 juin 2021  
Place de l'église à St Félicien le 26 juin, concert gratuit [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/l-orchestre-philarmonique-departemental-de-l-ardeche-en-concert-les-26-juin-et-6-juillet/ "Lire L'orchestre philarmonique départemental de l'Ardèche en concert les 26 juin et 6 juillet")

**Compte-rendu du Conseil municipal**  
8 juin 2021  
Séance du 7 juin 2021 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/compte-rendu-du-conseil-municipal-1/ "Lire Compte-rendu du Conseil municipal")