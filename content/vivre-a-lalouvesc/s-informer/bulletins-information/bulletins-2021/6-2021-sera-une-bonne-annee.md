+++
date = 2020-12-31T23:00:00Z
description = "2021 sera une bonne année !"
header = "/media/hiver-lalouvesc.jpg"
icon = ""
subtitle = "Janvier 2021"
title = "6 - 2021 sera une bonne année !"
weight = 12

+++
Dans ce premier Bulletin de l’année 2021 revenu à une parution mensuelle, vous trouverez un retour en images sur l’année passée, l’amorce d’un débat sur les leçons à tirer de l’histoire du village, un point sur les aides à l’installation de commerces dans le village, le livre d’or des Louvetous de l’extérieur, et bien d’autres informations comme toujours. Bonne année à toutes et tous et bonne lecture !

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier pdf (cliquer)](/media/bulletin-d-information-n-6-janvier-2021-lalouvesc.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Bonne année 2021 !

![Place Marel – Noël 2020](https://i2.wp.com/www.lalouvesc.com/wp-content/uploads/2020/12/2021-Place-Marrel.jpg)

**Toute l’équipe municipale vous souhaite une bonne et heureuse année 2021 ! Que la santé et la prospérité vous accompagnent tout au long de cette nouvelle année ! Meilleurs vœux de bonheur et réussite !**

## Actualité de la Mairie

Les vœux du Maire, accompagnés de quelques surprises, seront envoyés à chacun des habitants du village dans la première quinzaine de janvier. Ils seront publiés conjointement sur le site de la Mairie.

### Le point sur les demandes de subventions

Nous avons reçu l’accord officiel des demandes de subventions auprès de la préfecture pour les travaux de la Mairie, du cimetière et de l’hôtel Beauséjour, soit 35% du coût total des travaux. Côté Région, nous avons été prévenus que la subvention pour les travaux de la Mairie avait été votée. Les demandes pour les murs de soutènement du cimetière et pour l’hôtel Beauséjour sont encore en cours d’instruction. Les demandes de subventions faites à la Région représentent 45% du coût des travaux.

Nous préparons l’ensemble des dossiers pour que les opérations puissent commencer sur les chantiers restants dès que nous aurons reçu les réponses les concernant.

Par ailleurs, plusieurs dossiers de demandes de subventions auprès de l’Agence de l’eau et du Département sont en cours de rédaction pour l’eau et l’assainissement. Ils devront être votés au Conseil et envoyés courant janvier.

### De la clarté pour les comptes de la Mairie

Nous accueillons à partir du 6 janvier Philippe Piot qui a une formation de comptable et assurera donc la comptabilité de la Mairie deux jours par semaine en appui à notre secrétaire de Mairie. Bienvenu à lui !

La première semaine de janvier sera aussi l’occasion d’une remise à plat de l’ensemble des finances de la municipalité en vue de la préparation du budget 2021. Le 7 janvier nous aurons la présentation de l’audit financier que nous avions demandé aux services de la préfecture. La renégociation du prêt actuel et les demandes de prêts pour les investissements à prévoir pour les années à venir seront aussi discutées au cours du mois. Des décisions importantes et engageantes pour le prochain Conseil municipal !

### Avancement du nouveau site web

Retardée à cause du reconfinement, la construction du nouveau site web redémarre. A partir de la fin de la première semaine de janvier, les premières pages seront intégrées sur le prototype du site. Nous accueillons les designers les 15, 16 et 17 janvier pour une présentation et un travail en commun. Ce sera aussi l’occasion de présenter le projet à la responsable pour les collectivités de Numerian et au chargé de mission numérique du département de l’Ardèche.

Des tests seront effectués avec quelques habitants dans la seconde moitié de janvier. Le nouveau site devrait être opérationnel vers la fin du mois de février.

La réponse à notre demande d’une subvention pour le recrutement d’un conseiller numérique devrait être connue dans la première quinzaine de janvier.

## Zoom

Trois sujets pour le Zoom de ce mois : un retour sur l’année 2020 en images, le lancement d’un débat sur les leçons de l’histoire de Lalouvesc, et un point sur les aides à l’installation de commerces dans le village.

### L’année 2020 en quelques images

On se rappellera de 2020 comme l’année de la pandémie. Sans nul doute un tournant dans l’Histoire de la planète. A l’échelle du village, ce fut au cours de la deuxième vague que malheureusement des personnes ont été touchées par la maladie, quelques-unes gravement. Les élections ont eu un calendrier perturbé. La première et de la dernière manifestation de la saison, la Ronde louvetonne et le Trail des sapins, ont été annulées. Mais chacun a pu aussi constater une grande solidarité dans le village et la volonté de dépasser la morosité qui aurait pu nous accabler. Finalement une année qui aura été, malgré toutes les difficultés et les soucis, plutôt bonne avec une saison réussie et un nouvel élan qui se manifeste un peu partout.

L’année a été trop riche en évènements pour pouvoir toute l’illustrer. On trouvera ici simplement quelques photos publiées dans le bulletin en ordre chronologique, choisies un peu arbitrairement : la campagne électorale, les résultats du premier tour, le premier confinement, l’installation du Carrefour des arts, le vote du second tour, la nouvelle équipe municipale, le désherbage citoyen du jeu de boules, l’exposition du Carrefour des arts, la répétition de la Lyre louvetonne, un concert des Promenades musicales à la Basilique, l’affluence à la messe du 15 août, la réussite de la location de vélos électriques, la rentrée des classes, le changement d’équipe au Sanctuaire, l’élagage, la brocante, les travaux à la Mairie, l’accueil des nouveaux Pères par la Municipalité, l’ouverture de l’agence postale, la relance de l’écolotissement, la préparation des décorations de Noël à l’EPHAD, les décorations in situ, les babets, l’arbre aux souhaits, l’histoire du village et, pour finir, Lalouvesc sous la neige.

### Débat sur l’histoire et l’avenir de Lalouvesc

Suite à la publication, de l’Histoire de Lalouvesc de la préhistoire à nos jours (rappel : la frise historique est consultable en [cliquant ici](https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1mDDQVtwzQWTFBde7m10PI92NDikzuKh-Q1pZtSstsl8&font=Default&lang=fr&initial_zoom=2&height=650))., Paul Besset nous a envoyé un point de vue, qui enrichit le débat que nous devons avoir sur l’avenir du village et des voies originales à trouver pour son développement. Cette discussion collective est essentielle, n’hésitez pas à nous écrire pour l’enrichir. Pour décider quelle direction prendre, il est préférable de bien savoir d’où l’on vient.

Vous avez bien appréhendé l’automne de notre commune. Voici quelques réflexions “moroses”

En ce qui concerne le village :

1. **La fin des colonies de vacances est due,** d’une part, aux normes de sécurité des bâtiments, au quota de colons par moniteur, à la protection sociale du personnel, à la tache de quelques encadrants malveillants, à la disparition du semi-bénévolat des animateurs, aux lourdes responsabilités des adultes donnant un peu de leur temps libre à la jeunesse. Tous ces facteurs renchérissent le coût des séjours. Les activités traditionnelles de promenades, de jeux collectifs, de chants, n’attirent plus. Les seules colos qui subsistent proposent des activités plus onéreuses (cheval, plongée, spéléo…) qui réduisent la mixité sociale par les tarifs. Si on ajoute le désamour de vivre un moment de vacances loin de sa famille et de ses appendices électroniques, on aura fait le tour de l’abandon de cette forme d’occupation de la jeunesse pendant les congés. (J’ai été mono avant mon service militaire 1960)
2. **Le déclin des pratiques religieuses et séjours spirituels, c’est un fait.** Si quelques grandes célébrations attirent encore des pèlerins, il est à noter que bien peu de personnes séjournent au village pour un moment religieux. Un autre facteur est aussi à prendre en compte. Il s’agit de la voiture. Dans la décennie 1930, on a comblé le lac pour faire de la place au stationnement des autocars. Aujourd’hui ils sont assez peu nombreux. Autrefois, les pèlerins ainsi véhiculés étaient captifs du village et se restauraient sur place soit au restaurant, soit en pique-nique, soit en un hors sac dans un bistrot. Les commerces de bouche offraient les produits mythiques des montagnes. La concurrence était rude pour le pain de seigle, la pogne, le saucisson…
3. **Les séjours de longue durée font partie du passé.** Nos hautes maisons villageoises de plusieurs étages logeaient les estivants dans des meublés. Ces meublés n’ont pas su se rajeunir ! Les commodités dans la cour où l’on vient vider son seau au petit matin, le broc d’eau et la cuvette sont révolues. La tranquillité relative d’un étage à l’autre séparé par un plancher de bois, l’escalier, en bois aussi, permettant de noter les allers et venues des voisins ne sont plus appréciés. Où sont les curistes de grand air qui passaient une semaine ou deux dans les hôtels ?
4. **Le villégiateur a changé.**  Il abandonne, souvent pour des questions de coût, les vacances à l’hôtel, pour se tourner vers le séjour en bungalow ou sous tente. Il lui faut de l’eau et de la neige ! Ces deux éléments sont rares à La Louvesc. Les pique-niqueurs se sont raréfiés. Toutefois s’amorce une amathie pour la nature de la part de la tranche d’âge à plusieurs décennies.

Et pour la campagne (Jean Ferrat l’a beaucoup mieux exprimé que moi dans la Montagne) :

1. **La géographie et la topographie** sont des facteurs défavorisant l’agriculture productiviste. Les terrains sont difficilement mécanisables. Les sols sont pauvres. L’altitude restreint la variété des cultures. La saison d’hiver est longue et sans activité complémentaire. L’été, une saison sèche, ralentit la production de fourrage.
2. **Les surfaces cultivables** sont assez étriquées et trop morcelées. (les fermes de deux vaches n’existent plus). Le morcellement est dû souvent aux successions et à la petite valeur du foncier qui n’incite pas à vendre.
3. **Les exploitations se sont éteintes** avec la mort des générations nées au début du XX siècle. Les enfants suivant des études côtoient d’autres enfants à la vie moins rude. L’exode vers ces pôles d’activités que sont les métropoles vide les campagnes de leurs habitants.
4. **La modernisation des exploitations** a pu se faire grâce aux subventions et aux banques. Pour beaucoup cela n’a pas suffi tellement le fossé était profond.

_Un espoir ? La prise de conscience de la valeur illusoire des proximités des envies et besoins à côté des bienfaits de la vie paisible de nos contrées où toutes les modernités de la communication et d’approvisionnement sont possibles._

_Paul Besset_

### Les aides pour le commerce et l’artisanat

Dans un précédent bulletin, nous avons présenté les aides pour la rénovation de l’habitat et nous avons vu combien elles étaient nombreuses et intéressantes. Voyons maintenant quelles sont les aides pour l’installation ou la rénovation d’un commerce ou d’une entreprise artisanale. Les locaux disponibles ne manquent pas, en effet, à Lalouvesc.

Les aides sont particulièrement avantageuses en zone rurale, d’autant plus avantageuses que Lalouvesc se trouve en Zone de revitalisation rurale (ZRR) et que le plan de relance a ouvert des opportunités nouvelles. Le Ministère de l’économie a édité un guide pour permettre de s’y retrouver car elles sont nombreuses et parfois très spécifiques ([cliquer ici)](https://www.economie.gouv.fr/entreprises/entreprise-zone-rurale-aides-guide). De son côté, la Banque publique d’investissement a aussi repéré un certain nombre d’aides, notamment fiscales ([cliquer ici](https://bpifrance-creation.fr/encyclopedie/aides-a-creation-a-reprise-dentreprise/aides-specifiques/aides-milieu-rural)) . Il existe aussi une base de données des aides aux entreprises que vous pouvez interroger : selon la nature de votre projet, des aides spécifiques vous seront proposées [(cliquer ici)](https://www.aides-entreprises.fr/).

La Région Auvergne-Rhône-Alpes propose des aides particulières pour les commerces de proximité ([cliquer ici)](https://www.auvergnerhonealpes.fr/actualite/934/24-aides-financieres-pour-les-commerces-de-proximite.htm).

La Chambre de Commerce et de l’industrie de l’Ardèche peut vous accompagner dans vos projets [(cliquer ici](http://www.ardeche.cci.fr/)), il existe même un “Espace Entreprendre” à Annonay.

Il ne faut pas hésiter à passer un peu de temps dans la recherche. Même si cela paraît fastidieux, cela peut être très rentable selon votre projet ou votre profil. Par exemple, il existe des aides spécifiques pour le développement des sites web ou encore, si vous êtes une femme, vous pouvez bénéficier de conditions avantageuses d’investissement.

## Suivi

### Ouverture de Biblio’Chouette le 6 janvier

Pour la nouvelle année, une bibliothèque rénovée ! L’ouverture de la bibliothèque dans ses nouveaux locaux aura lieu le mercredi 6 janvier.

Horaires :

* Mercredi de 15h à17h.
* Samedi de 9h30 à 11h30.

### Population de Lalouvesc

L’INSEE vient de publier les chiffres du dernier recensement qui donne le chiffre officiel de la population des communes en 2018 ([Source](https://www.insee.fr/fr/statistiques/5001880?geo=COM-07128)). La “population comptée à part” comprend des personnes dont la résidence habituelle est dans un autre commune, mais qui ont conservé une résidence sur la commune (p. ex. des étudiants).

![](/media/population-lalouvesc-2018.jpg)

### Décès au cours de l’année 2020

Il y a eu 21 décès au village au cours de l’année 2020. Voici la liste de ceux dont les familles ont publié une annonce dans la presse : Marie Madeleine Moutard née Solnon, Régine Pourtau-Cazalet née Faurie, André Combettes, Rémy Deygas, André Gaillard, André Tavernier, René Osternaud, Marie Abel, Guy Roche, Clovis Bosc, Charles de L’Hermusière.

L’équipe municipale présente ses sincères condoléances à toutes les familles concernées et s’associe à leur tristesse.

Parmi les départs récents, celui de Marie Madeleine Moutard marque le tournant d’une page de l’histoire du village. Son empreinte, son dynamisme et sa personnalité ont marqué le Lalouvesc de la fin du XX siècle. Nous aurons l’occasion de revenir sur son action pour lui rendre hommage dans un prochain numéro du bulletin.

### Le livre d’or des Louvetous de l’extérieur

Plusieurs Louvetous de l’extérieur ont répondu à notre appel. Vous pourrez feuilleter leur sympathique message en [cliquant ici](https://www.flipsnack.com/Lalouvesc/lalouvesc-livre-d-or-2020.html).

### Il est encore temps de donner votre avis sur l’espace de jeu

Si vous ne l’avez pas encore fait, donnez votre avis sur l’espace de jeu du Val d’Or. C’est important, plus il y aura de réponses au questionnaire du CAUE plus les propositions seront représentatives des pratiques des Louvetous. Il n’y a que quelques questions, cela ne vous prendra pas beaucoup de temps. [Cliquez ici]().

### Le cadeau des enfants de l’école à la Mairie

Pour ceux qui n’ont pas eu le temps de faire le puzzle du Bulletin précédent, voici le magnifique cadeau de Noël offert par les enfants de l’école. Vous pouvez l’admirer aussi au centre du village dans la vitrine du bureau du Maire.

![](/media/ecole-4.jpg)

### Courrier des lecteurs

Nous avons reçu ce message d’une maman de passage à Lalouvesc pendant les fêtes :

Joachim (4 ans et demi) devait raconter quelque chose de ses vacances dans son cahier de vie. Il a choisi de dessiner et de parler de l’arbre à souhaits de Lalouvesc.

![](/media/dessin-arbre-a-souhaits.jpg)

### Le futur des fêtes ?

Et pour finir avec un sourire…

Vous êtes restés bien sage le jour du réveillon ? D’autres sans crainte du virus s’en sont donné à cœur joie ! Vous pouvez l'admirer sur [cette vidéo](https://youtu.be/fn3KWM1kuAw) mise en ligne par la société américaine constructrice de robots Boston Dynamics le 29 décembre.