+++
date = 2021-01-31T23:00:00Z
description = ""
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "Février 2021"
title = "7 - Lalouvesc ne connaît pas la déprime"
weight = 11

+++
La Mairie prévoit les investissements futurs, les associations préparent une saison estivale joyeuse et passionnante, les citoyens louvetous sont appelés à aider à rénover le camping… voici quelques-uns des thèmes développés dans ce numéro avec, comme toujours, bien d’autres informations. Bien loin de la morosité ambiante et pourtant touché comme toute la planète par la pandémie, notre village ne connaît pas la déprime.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à [ce fichier pdf (cliquer)](/media/bulletin-d-information-n-7-fevrier-2021-lalouvesc.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Bonjour à tous les villageois,

Ces jours-ci l’hiver semble derrière nous, mais attention février n’est pas fini. Le froid et la neige peuvent encore nous surprendre. Les vacances de février pour les scolaires sont arrivées. Profitez-en tous bien, le temps est propice à de belles promenades.  
Un conseil municipal s’est tenu le 3 février à 20h, malheureusement à huis-clos à cause de la période que nous traversons. Nous avons pris d’importantes décisions pour l’avenir du village et son développement. Nous avons mûrement réfléchi aux actions à entreprendre et nous sommes convaincus que nous allons dans le bon sens avec des investissements qui à terme apporteront une richesse nouvelle au village. Nous avons aussi réactivé notre volonté de soutenir l’école en votant le nouveau forfait communal pour l’année 2021. Vous trouverez une présentation de ces orientations dans ce bulletin et vous pourrez consulter le compte-rendu du conseil municipal qui sera publié dans quelques jours.  
Le site web de la Commune est en pleine reconstruction, dans quelques temps il sera opérationnel et vous pourrez vous promener dans les diverses rubriques riches d’informations qu’il présente avec beaucoup de confort, d’aisance et de simplicité.  
Les illuminations et décorations des fêtes de fin d’année sont rangées dans les placards. Il faut maintenant songer à la préparation de la saison nouvelle. Les associations s’y consacrent activement. Nous souhaitons aussi que chacun puisse nous aider à redonner des couleurs au camping au cours de “journées citoyennes” à venir.  
Nous sommes toujours et encore menacés par l’épidémie du Covid, ne relâchons pas nos efforts, continuons à prendre toutes les précautions nécessaires à la protection sanitaire.

Amitiés à toutes et tous

Jacques Burriez, Maire de Lalouvesc

## Actualité de la Mairie

### Une nouvelle secrétaire à la Mairie

![](/media/gladys-durieux-lalouvesc-scaled.jpg)

Après plusieurs mois de recherche, nous avons enfin une nouvelle employée municipale pour compléter l’équipe du secrétariat de la Mairie suite à l’ouverture de l’Agence postale. Gladys Durieux vient de St Romain Lachalm en Haute-Loire. Elle a déjà une expérience professionnelle, précieuse pour nous, dans les banques, les collectivités locales et même dans une agence postale. Elle nous a rejoint début janvier. Bienvenue à elle !  
Léa, notre jeune stagiaire, nous a quittés vendredi 5 février. Elle a montré pendant ses deux mois de stage motivation, compétences et bonne humeur. Nous devons la remercier et lui souhaiter une pleine réussite dans la suite de ses études.

### Une relation forte avec l’école

Plusieurs réunions se sont tenues avec les partenaires de l’école Saint Joseph : l’OGEC et ses tutelles ainsi que l’association des parents d’élèves. Elles ont permis de remettre à plat les modalités des relations entre la Mairie et l’école et de trouver une formule qui, nous l’espérons, permettra de pérenniser le soutien de la Mairie pour l’école sur des bases plus solides qu’auparavant.  
Une petite école à classe unique est fragile et toujours menacée. Elle a besoin de l’étroite collaboration de tous les acteurs pour sa défense.  
Elle a pu se maintenir d’abord grâce à l’investissement des parents d’élèves qui pilotent son association de gestion (OGEC), qui organisent au travers de l’association des parents d’élèves (APEL) des manifestations de soutien pour financer les sorties et les animations proposées aux élèves, ou encore qui soutiennent l’enseignante dans sa pédagogie. La tutelle Saint Joseph et la direction diocésaine fournissent un soutien, une logistique, et des financements nécessaires à la vie d’un établissement privé. La Commune, de son côté, fournit un soutien financier et matériel largement au-delà de ses obligations statutaires. Le forfait communal a été fixé pour cette année à 140 € par élève et par mois.

![](/media/ecole-4.jpg)

Nous nous réjouissons tous que les élèves de l’école St Joseph progressent et se sentent heureux dans leur école et nous souhaitons qu’ils gardent en mémoire pour le reste de leur vie ce passage à l’école Saint Joseph comme une période privilégiée.  
Pour suivre la vie de l’école, connectez-vous à sa [page Facebook](https://www.facebook.com/Ecole-St-Joseph-Lalouvesc-107859627383105/?ref=page_internal).

Il faut défendre notre école comme un bien commun du village. Pour cela nous avons besoin de l’engagement de tous les habitants et les amis du village. Ainsi, par exemple, votre participation aux ventes et animations organisées par l’APEL est essentielle. Ça tombe bien, justement cette semaine il y a une tombola !  
Vous pouvez aussi faire un don pour l’école ou tout autre appui sera bienvenu.

### Démarrage prochain des travaux au cimetière et pour l’ancien hôtel Beauséjour

Nous avons acquis une subvention de 35% de la part de la Préfecture (DETR-DSIL) pour les travaux concernant le chantier du cimetière et celui de l’ancien hôtel Beauséjour. Nous n’avons toujours pas de réponse pour les demandes effectuées à la Région depuis plusieurs mois.  
Mais ces travaux sont urgents et ne peuvent attendre. C’est pourquoi le Conseil municipal a décidé de lancer les opérations, en sécurisant néanmoins les risques financiers (voir plus loin les développements sur les finances de la Commune). Ainsi pour le cimetière, il reste à comparer quelques devis pour choisir l’entreprise qui effectuera les travaux. Ceux-ci devraient démarrer dans les mois prochains. Le chantier Beauséjour est beaucoup plus important. Il nécessite donc un appel d’offres qui devrait être lancé dans les semaines à venir pour un démarrage que nous espérons en avril- mai.  
Parallèlement la réflexion menée avec l’aide du CAUE sur l’aménagement du Parc du Val d’Or et la construction d’un Jeu-monument avance bien. Des nouvelles dans le prochain bulletin.

### Vente d’une parcelle

La Commune souhaite relancer la mise en vente d’une parcelle, déjà votée sous l’ancienne mandature. Il s’agit d’une parcelle de 0,0815 ha située au lieu-dit Le Clot. Parcelle AB-109. Cette parcelle servait autrefois de déchetterie, elle est actuellement en friche. Une annonce a été passée sur [B2F – Bourse Foncière Forestière de l’Ardèche](https://geoids.geoardeche.fr/b2f/index.html?context=EdFr).

### Assainissement et eau

Trois demandes de subventions ont été déposées à l’Agence de l’eau, le Département et la Préfecture pour des travaux sur les réseaux d’eau et d’assainissement. Il s’agit d’une modification de la station de pompage du camping (coût 150.000 €), d’une nouvelle tranche du réseau d’assainissement, rue de la fontaine, quartier Grand Lieu et quartier Chante Aucel (375.000 €) et d’une actualisation du schéma directeur (50.000 €). Les subventions demandées couvrent 80% du coût des chantiers.  
Il s’agit de chantiers importants et coûteux qui seront étalés sur plusieurs années, mais indispensables compte tenu de l’état actuel du réseau. Leur coût a été pris en compte dans nos prévisions financières (voir plus loin).  
Exemple de la vétusté de l’infrastructure et de l’importance d’un entretien régulier, il a été constaté une fuite importante sur le réseau d’eau. Une étude a été réalisée dans la nuit du 2 au 3 février. Le lieu de la fuite a été repéré. Il reste à réparer la canalisation.

### Site web

Le nouveau site web avance bien. La structure générale est finalisée, Plus de la moitié des informations ont été intégrées. Le site comprend plus de 80 pages. Son calendrier initial devrait être tenu. Les premiers tests avec les usagers sont programmés pour les 18, 19 et 20 février, vous serez aussi tous consultés à l’aide d’un sondage. Le site ouvrira officiellement début mars.  
Le prototype du site sert déjà de démonstrateur pour les designers de Plateaux numériques. Il a été présenté le 3 février à des responsables de la [Direction Générale Déléguée Numérique ](https://agence-cohesion-territoires.gouv.fr/lacces-au-numerique-337)de l’Agence nationale de la cohésion des territoires (ANCT) et de l’[Incubateur des territoires](https://incubateur.anct.gouv.fr/) où il a reçu un très bon accueil, nous en reparlerons. Ainsi, on entend parler de Lalouvesc dans les Ministères parisiens…  
Rappel : une page du site est consacrée aux paysages qui seront accessibles sous forme de diaporama. Le paysage est une des valeurs très sûres du village. Partager la magnificence des paysages louvetous, c’est inviter les internautes à venir les voir sur place. Pour cela nous avons besoin de vos plus belles photos. Nous les attendons nombreuses à l’adresse de la Mairie mairie@lalouvesc.fr. Ci-dessous un paysage envoyé par Christiane Loucel, merci à elle !

![](/media/les-cevennes-ch-loucel.jpg)

## Zoom

Nous mettons l’accent ce mois sur trois sujets, exemplaires du dynamisme retrouvé du village. Le premier est un peu austère, mais essentiel, comme on le dit souvent : les finances sont le nerf de la guerre. Nous vous dévoilons comment l’équipe municipale a réfléchi et négocié pour trouver de quoi relancer les investissements sur le village. Le second est plus festif et collaboratif. Les Lalouv’estivales seront animées en 2021, les associations louvetonnes s’y sont engagées. Enfin le troisième sujet est un appel à un engagement citoyen pour rénover le camping, notre bien commun, principale source d’attractivité du village et aussi d’autofinancement pour la Commune.

### Finances

Comme promis, voici un premier point financier. La construction du budget 2021 va bientôt démarrer. Pour avancer dans sa préparation, l’équipe municipale a pris plusieurs initiatives : la réalisation d’un audit financier, la négociation de nouveaux emprunts et enfin de nouvelles demandes de subventions et le choix des priorités dans les investissements.

#### **Audit financier**

Un audit financier a été demandé à la direction départementale des finances publiques de l’Ardèche. Réalisé sur une analyse de cinq années de compte (2015-2019), l’audit permet de pointer les forces et les faiblesses financières de la Commune. Son détail peut être consulté à la Mairie. En voici un résumé, accompagné de quelques remarques :  
Sur la période, les produits de fonctionnement (recettes) sont en forte baisse. Cette baisse a pour cause principale les “autres produits”, c’est-à-dire des loyers et du camping. Sur la même période, les charges ont aussi baissé, mais de façon moins importante. Les dépenses d’investissement sont nettement inférieures à celles des communes comparables de la région. L’année 2019, quant à elle, année pré-électorale, se présente comme une année atypique avec une baisse conséquente des charges suite à un différé de travaux d’entretien. Ainsi selon l’étude, la commune en menant une politique de maîtrise de ses charges a pu inverser la tendance à l’érosion de son autofinancement et disposer d’une trésorerie de 180.000 € et d’un financement disponible de 111.200 €.  
Pour l’avenir, les projections réalisées par l’audit à partir de l’évolution des ressources montrent une érosion lente des capacités d’autofinancement. Le principal levier pour renverser la tendance est constitué des recettes du camping. Sans recours à un nouvel emprunt, la Commune ne peut assumer tant les investissements pris en compte par l’audit (travaux Mairie, cimetière, Beauséjour) que ceux destinés à rendre attractif notre village.  
Cet audit nous a rassurés en nous montrant que la situation financière de la Commune n’était pas dégradée. Il nous a aussi confortés dans notre analyse : le prix de cette maîtrise financière a été l’absence d’investissement régulier pour l’entretien du village et le camping reste, pour l’instant, la principale source de l’autofinancement de la Commune.

#### **Négociations des emprunts**

![](/media/cimetiere-lalouvesc-2.jpg)

Le recours à l’emprunt est donc la seule façon de retrouver les capacités d’investissements indispensables au développement du village. Mais celui-ci doit être maîtrisé pour ne pas grever l’avenir. Nous avons pour cela engager des négociations avec les banques pour deux emprunts distincts : le premier concerne les projets généraux du village, le second est spécifique à l’écolotissement.  
Pour le premier, l’objectif a été de mesurer quelle somme supplémentaire il était possible d’emprunter sans modifier sensiblement les annuités actuelles de remboursement de l’emprunt contracté pour la station d’épuration. Ce résultat était rendu possible par l’allongement de la période de remboursement accompagné par une réduction sensible du taux d’intérêt. Le pari est que les finances de la Commune ne soient pas affectées par un remboursement équivalent à celui qu’elle effectue actuellement et qu’inversement les investissements rendus possibles confortent à moyen terme le budget de la Commune et, plus largement, enrichissent l’ensemble du village.  
Le recours à l’emprunt n’est donc justifiable qu’à la condition que les investissements prévus amènent des retours intéressants pour faciliter son remboursement et donc autorisent d’autres investissements à venir ou évitent des dépenses à venir plus lourdes qu’elles ne le seraient sans investissement (réparation, entretien).  
Deux opérations concomitantes étaient alors nécessaires pour arriver à ce résultat :  
1\. renégocier la dette actuelle pour allonger sa période de remboursement (qui passerait de 10 années à 20) ;  
2\. ouvrir un nouvel emprunt sur 20 ans pour dégager des fonds propres.  
Après discussions avec deux banques, nous arrivons selon ce raisonnement à emprunter une somme de 325.000 € pour un remboursement annuel de 36.300 €. Cette échéance inclut également le remboursement du refinancement de l’emprunt existant. Le remboursement bien que légèrement supérieur au remboursement du prêt actuel (35.080 €), s’accompagne d’une baisse sensible des intérêts. Le montant des intérêts sur les cinq prochaines années (2022 à 2026) sera de l’ordre de 21 425 € alors qu’il se situait à 46 147 € avec le seul emprunt actuel. Cette économie de dépenses de fonctionnement influera positivement sur la constitution de notre autofinancement, compensant les conséquences de l’allongement de notre dette de 10 années supplémentaires.  
Pour le second emprunt concernant les aménagements de l’écolotissement du Bois de Versailles, nous avons négocié avec les banques un emprunt “in fine”, c’est-à-dire que les sommes empruntées le sont au fur et à mesure des besoins de financement et peuvent être remboursées sans indemnité lors de la vente des terrains. La totalité des travaux relatifs à la viabilisation de la voirie a été estimée à 270.000 €(corrigé le 8-01) et les banques proposent un taux d’intérêt à 0,7%, sachant que les travaux ne démarreront qu’une fois quatre compromis de vente signés. La concrétisation des ventes dégagera ainsi des fonds propres et donc diminuera l’emprunt. Les risques financiers pour la Commune sont faibles compte-tenu de l’attractivité du projet et du montage envisagé.

#### **Investissements à court et à moyen terme**

Deux chantiers ont été réalisés en 2020 : la rénovation de la Mairie avec les nouveaux locaux de la bibliothèque municipale et l’installation de l’Agence postale communale pour lesquels des subventions avaient été obtenues pour 80 et 100% des coûts estimés. Deux chantiers vont être bientôt ouverts, la réparation du cimetière et la réhabilitation de l’emplacement de l’ancien hôtel Beauséjour, pour lesquels nous avons obtenus une subvention de 35% de la Préfecture (DETR), mais pas encore de réponses de la Région pour les 45% complémentaires demandées. Nous venons aussi de déposer trois autres demandes de subvention pour des chantiers indispensables sur les réseaux d’eau et d’assainissement.  
Pour l’ensemble de ces chantiers, compte-tenu des incertitudes, nous prévoyons une provision de 240.000 €. Soulignons que ce choix est celui de la prudence, en attendant d’avoir des nouvelles des demandes de subventions à la Région.  
Ainsi à court terme, nous aurions une capacité d’investissement supplémentaire de 325.000 € (total de l’emprunt) – 240.000 € (provision) = 85.000 € que nous avons choisi de consacrer à des investissements amenant des retours rapides : la rénovation du camping et le projet de jeu monument.  
Ainsi ce scénario de court terme, prudent mais volontaire, prévoit : **la réparation du cimetière, la démolition de Beauséjour, les travaux d’eau et d’assainissement, le réaménagement du camping et le lancement du jeu monument.**  
Ces chantiers ne couvrent évidemment pas la totalité des besoins du village, loin de là. Mais notre pari est qu’ils amorceront un renouveau qui permettra à la Commune de rebondir sur d’autres investissements tout aussi essentiels dans les années à venir.

### Préparation de la saison 2021

Les calculs financiers ne doivent pas nous faire oublier la préparation de la saison estivale. L’attractivité du village trouve une partie de sa source dans les nombreuses manifestations qui y sont organisées en été et les associations louvetonnes n’ont pas non plus chômé pas pendant l’hiver. Les incertitudes de la pandémie n’ont pas bridé les imaginations ni les initiatives. Voici un aperçu des réjouissances qui nous attendent.

#### Le Comité des fêtes se projette

Après quelques semaines d’une trêve hivernale régénératrice, les membres du bureau se sont réunis le 11 janvier dernier. Dans un contexte particulièrement complexe et incertain, il a été décidé d’imaginer une perspective d’avenir positive. Un calendrier a ainsi été élaboré et des dates ont été fixées.  
Ces objectifs seront suivis au jour le jour en fonction de l’évolution de la crise sanitaire sévère que nous traversons. Nous préparons dès à présent les dossiers avec énergie et confiance. Et nous ferons face, comme nous l’avons déjà expérimenté en 2020, à des annulations si nécessaire.  
Nous gardons toujours en mémoire que le rôle du Comité des Fêtes est d’animer le village tout au long de l’année. Ces animations demandent de longues réflexions en amont de chaque évènement. C’est pourquoi, fixer des dates est nécessaire dès ce début d’année, les voici :

**11 avril LA RONDE LOUVETONNE  
30 mai LE CONCERT  
19 juin L’ARDÉCHOISE  
05 septembre LA BROCANTE  
30 octobre LE TRAIL DES SAPINS**

Nous sommes bien conscients que les évènements tels que nous les avons organisés et connus jusqu’à maintenant devront subir toutes les adaptations nécessaires pour coller parfaitement aux restrictions, aux consignes, aux exigences du moment. Animer le village oui, mais respecter et préserver la santé de chacun doit être notre leitmotiv de chaque instant.

![](/media/affiche_trail_ephemere-scaled.jpg)

Notre commençons l’année par une nouveauté bien adaptée au contexte actuel : le balisage du **TRAIL DES SAPINS ÉPHÉMÈRE** ! Vous saurez tout de ce nouveau concept en vous rendant sur le [site du Trail](https://www.traildessapins-lalouvesc.fr/).

Nous espérons poursuivre ainsi la mission que tous les bénévoles du Comité des Fêtes affectionne particulièrement : **Faire rayonner LALOUVESC !**

Agnès G.

#### Les Promenades musicales

La réflexion et les contacts des Promenades musicales ont aussi été fructueux. Voici le programme prévisionnel 2021, tel qu’il a été envoyé aux organismes subventionnaires :

**26 juillet : Musique de chambre à la Basilique St Régis**  
Quatuor avec flûte, Mozart ; Trio à cordes op 9 n° 3, Beethoven ; “Petites musiques de nuits d’automne” soprano, alto et violon (Création), P Busseuil ; Entregas breves de amor y desarraigo pour quatuor à cordes et soprano) (Création), F. Carrasco ; Rondo pour violon et quatuor, Schubert.  
**28 juillet : Bach et Associés à St Romain d’Ay**  
Sonatine en mi mineur, G.Ph. Telemann ; Sonate à trois en ré mineur, C.P.E. Bach ; Première suite pour violoncelle seul (extraits), J.S. Bach ; Concerto a trois en ré mineur F.XII n°42, A. Vivaldi ; Trio en sol mineur BWV 1020, J.S. Bach ; Trio en sol majeur, J. Ch. Bach ; Trio londonien n°1 en do majeur, F. J. Haydn.  
**30 juillet : La voix et la musique à St- Félicien ou Satillieu**  
Stabat Mater de Pergolèse (chœur femmes et solistes) ; Air du froid de Purcell ;La mort de Didon de Purcell l’extrait “when I am laid in earth” ; Lascia ch’io Pianga de Haendel ou Ach ich fulh’s de Mozart ; La donna e mobile” Rigoletto,Verdi ; “Barcarolle” Offenbach ; “Papageno/papagena” Mozart ; Terzetto dans la flûte enchantée, Mozart ; “Soave il vento” trio de Cosi fan tutte, Mozart ; Bella figlia del amor ds Rigoletto.  
**1er Août : Un concert concertos et Chœurs de femmes à la Basilique St Régis**  
Symphonie de Mendelssohn pour cordes n°13 ; Concerto pour violoncelle n° 1 en do majeur Hob.VIIb.1, J. Haydn ; Rondo en sol mineur op. 94 pour violoncelle et orchestre, A. Dvorak ; Esa pequeña estrella (Création), Carrasco ; Seven Part Songs, G. Holst.  
**5 août : Voyage du baroque au jazz « Carte blanche au duo KARAMANDE » Grange de POLLY Saint Pierre sur Doux**  
Music for a while, H. Purcell (From Orpheus Britannicus) ; Aria, J.S. BACH (extrait de la suite en Ré Majeur) ; In a sentimental mood, D. Ellington ; Interplay, B. EVANS ; Cristal Silence, Ch. Corea ; Over the Rainbow, H. Arlen ;Ave Maria, Fr. Schubert.  
**8 août : Concert de jazz classique Grange de POLLY Saint Pierre sur Doux**

![](/media/promenades-musicales-lalouvesc-2.jpeg)

#### Le Carrefour des Arts

L’année d’une association comme le Carrefour des Arts est ponctuée de moments importants. Notre Assemblée générale en est un. Devant les incertitudes sanitaires, nous avons décidé que l’AG se tiendrait par correspondance. C’est une version nouvelle pour nous et nous comptons sur le soutien et la participation de tous nos adhérents.  
Côté exposition, l’édition 2021 sera aussi exceptionnelle que celle de 2020. La sélection des artistes qui exposeront du 4 juillet au 22 août est achevée et nous travaillons activement à la programmation d’évènements autour de la manifestation. La diversité et la qualité des œuvres, toujours au rendez-vous, sauront encore vous surprendre et vous émerveiller.  
La préparation, la mise en place, les permanences d’accueil, la dépose de l’exposition sont des moments d’intense activité pour lesquels nous avons besoin de tous les talents. Nous serions heureux d’accueillir les vôtres. N’hésitez pas, venez partager quelques heures de votre temps, nous en avons besoin pour la préparation des salles et l’accueil des visiteurs.  
Nous réfléchissons parallèlement à de nouvelles activités que nous pourrions proposer à nos adhérents tout au long de l’année. Venez nous aider à développer la qualité et la diversité de l’offre culturelle sur notre territoire et profitez, vous aussi, du dynamisme et des avantages proposés par l’association.  
Faites-vous connaître en laissant un message à contact@carrefourdesarts-lalouvesc.com.

Michèle G S.

![](/media/carrefour-des-arts-2020-installation-photo-ot.jpg)

#### Les Jardins du Haut-Vivarais

Une nouvelle association a été créée cet hiver. L’association Les Jardins du Haut-Vivarais. Celle-ci propose notamment une initiation à la permaculture, au “Mont Besset”. Voici le programme prévu pour 2021 :

* Avril, mai, juin : initiation “Faites vos semis comme un pro”. par Karine Davier chef de culture chez Régis Marcon (Saint Bonnet le Froid) et Xavier Pourtau Cazalet chef restaurateur à la Terrasse (Lalouvesc). Les cellules de plant, le terreau et les graines sont fournis. Les participants pourront repartir avec leurs semis. 20€ la demi-journée.
* Tous les lundis en juillet et août : visite du jardin en permaculture du Mont Besset à 10h30.

### Le camping rénové par les citoyens

Le camping par ses capacités d’hébergement est donc aussi un outil important pour la réussite de la saison estivale. Mais il a besoin d’un sérieux lifting. La Mairie a décidé de retrousser ses manches et d’investir pour sa rénovation. Des travaux de terrassement vont être entrepris et de nouveaux hébergements vont être achetés.  
Après une recherche juridique, nous avons conclu que les cabanes perchées étaient bien revenues propriété de la Commune. Il reste à savoir si leur état permet une exploitation. Une commission de sécurité doit passer prochainement pour une expertise. Selon son diagnostic, nous verrons s’il est possible d’entreprendre des réparations en vue de location dès l’été prochain. Une idée serait de proposer des séjours couplés avec une animation (location de vélo électrique, promenade en poney…).  
Les cabanes ne sont pas le seul chantier à prévoir sur le camping. Les employés communaux et les investissements prévus ne suffiront pas à redonner au camping tout l’éclat souhaitable pour accueillir dignement les visiteurs. Nous avons besoin de tous les habitants pour retrouver un camping à la hauteur de la réputation d’accueil du village. C’est pourquoi, comme nous l’avions annoncé dans notre programme nous avons planifié des “journées citoyennes”. Toutes les Louvetonnes et tous les Louvetous, petits et grands, jeunes et plus âgés, sont conviés à **ces journées citoyennes les week-ends** **des 6-7 mars et 20-21 mars**, (en cas de météo défavorable, des dates ultérieures seront proposées) !  
Un groupe de travail s’est réuni pour prévoir les actions à entreprendre. Voici un résumé de ses propositions.

#### Six chantiers + un

Les journées sont organisées autour de six chantiers principaux. Chaque chantier sera piloté par un chef de chantier.

A ces six chantiers viendra s’ajouter celui des cabanes le cas échéant. La désignation des chefs de chantiers et la répartition des ouvriers-citoyens se feront dans les semaines à venir. Chaque chef de chantier devra définir les tâches précises à réaliser et organiser le travail. Selon le nombre de citoyens prêts à s’investir dans ces journées, d’autres travaux pourront aussi être envisagés. Prochainement vous pourrez vous inscrire en Mairie ou en ligne pour participer à ces chantiers.  
Parallèlement, le groupe de travail a aussi examiné les nouveaux hébergements qu’il serait possible d’acquérir (tente-lodge, bungalow, mobile-home…), proposé qu’une buvette puisse être ouverte sur le camping dont la gestion reste à définir et qu’un jeune titulaire du BAFA puisse être recruté pour des animations en direction des enfants (une demande a été effectuée à la Communauté de Communes pour un financement).  
De son côté, le Conseil a voté une révision des tarifs du camping pour la prochaine saison.

## Suivi

### Hommage à Mimi Solnon

Marie-Moutard Solnon nous a quittés le jour de Noël. “Mimi Solnon” était un personnage, une personne d’initiatives et de caractère à l’image du village de Lalouvesc qu’elle aimait et qu’elle a contribué à animer et à développer.

![](/media/mariage-de-mimi-solnon-et-lucien-moutard.jpg)  
Nous lui devons beaucoup à elle et à son mari Lucien Moutard (la photo ci-dessus est celle de leur mariage dans les rues du village). Son action reste gravée dans la mémoire des anciens du village et de nombreux témoignages matériels et concrets y sont encore visibles et largement exploités.  
Elle a mené d’une main ferme et accueillante l’hôtel-restaurant “le Relais du Monarque”. Elle fut une des toutes premières à moderniser son établissement dès les années 60 en équipant les chambres de cabinets de toilette et en organisant un circuit d’hôtel en hôtel pour que les clients n’aient pas à porter leurs bagages.

Son action pour l’organisation du tourisme a été considérable : Présidente du syndicat d’initiative de nombreuses années, elle en a réorganisé le secrétariat qui est devenu permanent et, par ses relations, l’a largement ouvert sur l’extérieur ardéchois en créant l’Union Touristique du Haut Vivarais et national en participant à des émissions radio-télévisées.

Elle participa aussi activement au balisage des chemins de randonnée et à l’ouverture et au traçage du parcours du Chemin de Saint Régis, pas moins de 190 km de repérage, de discussions avec les propriétaires de parcelles traversées et de balisage dont son mari a dessiné le logo qui symbolise toujours aujourd’hui notre village. On la retrouve aussi au moment de la fondation de l’association de l’Alauda et de celle du Carrefour des Arts. Elle y a montré sa curiosité pour l’histoire, les arts, comme en témoigne son [article sur les muletiers](/media/muletiers-m-solnon.pdf) paru dans le bulletin n°3 de l’Alauda.

![](/media/son-et-lumiere-lalouvesc.jpg)

Elle aimait aussi les fêtes et les célébrations et avait un talent exceptionnel pour les organiser et mobiliser l’ensemble du village. Du temps où elle était encore active, le printemps était l’occasion d’une fête des genêts où des dizaines de chars fleuris sillonnaient le village. Et surtout, morceau de bravoure et souvenir inoubliable pour ceux qui y ont participé : le 350ème anniversaire de la mort de Saint Régis en 1990 fut l’occasion d’un spectacle Son et Lumière au Parc des pèlerins comprenant plus d’une soixantaine d’acteurs et figurants du village. Avec ceux qui s’occupaient des décors, de la régie, on peut dire que tout le village a été mobilisé. La manifestation a eu tant de succès qu’elle fut répétée l’année suivante.

Mimi Solnon repose en paix. Elle reste dans nos cœurs comme un exemple de ce qu’un village peut réussir quand il a du talent !

### Numéro d’appel pour la vaccination

À compter du 1er février, un centre d’appels est mis en place conjointement par la préfecture et le conseil départemental. Le numéro unique **04.75.66.77.99** remplace les numéros des différents centres de vaccination.  
Il est ouvert du lundi au vendredi de 08h30 à 12h30 et de 13h30 à 17h00 (16h30 le vendredi).  
Réservé uniquement aux personnes éligibles au vaccin n’ayant pas internet ou n’étant pas familières des réservations en ligne, ces personnes sont inscrites sur une liste d’attente et seront rappelées dès la ré-ouverture des plates-formes de prise de rendez-vous.

### Formation designers web

![](/media/formation-web.png)

### Tri et compost

![](/media/sytrad-tri.jpg)

Les consignes de tri des ordures sont trop mal respectées dans le village. Pourtant, il n’est pas besoin d’un long discours pour comprendre leur importance. Sans même reprendre des arguments écologiques, il suffit d’observer les deux schémas présentés ci-dessus. Ils ont été réalisés par le Sytrad, l’organisme qui ramasse les poubelles de notre village. Le manque de discipline dans le tri, démontré par la composition d’une poubelle moyenne ramassée sur le réseau de récolte, conduit à de lourdes dépenses inutiles… payées par nos impôts, du fait de l’importante différence de coût de traitement des déchets.  
Une façon intelligente de réduire le volume des déchets est de réaliser un compost. Mais celui-ci ne s’improvise pas. Le Sytrad vous propose un guide pour apprendre à bien composter ([cliquer ici](/media/sytrad_guide-compostage-2019.pdf)).

### Courrier des lecteurs

Nous avons reçu beaucoup de courriers sympathiques nous remerciant de l’envoi du livret sur l’histoire de Lalouvesc qui accompagnait les vœux du Maire et de l’équipe municipale. Plusieurs nous ont fait remarquer quelques erreurs factuelles. Nous les prendrons en compte pour une prochaine publication.   
Edith Archer nous a envoyé le courrier suivant :  
  
_“Cher Monsieur le Maire (et Conseil Municipal)…  
Grand Merci !…(tout d’abord pour l’ordinateur !) et pour vos “Bons vœux ” que j’accueille de tout cœur et avec qui je suis bien d’accord !…Oui !…”Un village de caractère” !… et cela suppose que les “villageois” en ait aussi (du caractère).  
Permettez-moi donc de revenir sur la ” Source St.Régis”. Elle est aussi “lamentable” et abandonnée qu’elle l’était quand on m’avait permis (le Conseil Municipal) de l’améliorer (au printemps dernier). Non ; “la religion ce n’est pas triste ” comme peut-être certains vous l’aurait dit. La Beauté est au contraire indispensable pour “les choses de Dieu”! Je vais parler de cette question avec notre nouveau Recteur, et avec la “Communauté-locale” qui attend de se réunir. Cette Fontaine est un lieu emblématique de notre “Ville Sanctuaire” et il me “tient à cœur” qu’elle puisse être un lieu de recueillement et d’harmonie pour tous, pèlerins ou simples visiteurs (et que cette “Source miraculeuse” ne soit pas dans un état de “taudis” et aussi négligée (car elle est très fréquentée… et fait mal juger de notre si beau village!).  
Tout cela très cordialement ! (et bien “fraternellement si j’ose dire!) dans ce même amour que chacun de nous avons au fond du cœur pour Lalouvesc ! Vive Lalouvesc ! Et que St.Régis bénisse l’année 2021 !  
Edith Archer”_  
  
Réponse : vous pointez fort justement une difficulté à résoudre. Votre démarche est la bonne : alerter la Mairie et engager une collaboration Sanctuaire-Mairie pour trouver une solution afin de rendre à la Source l’éclat qu’elle mérite. Nous allons poursuivre dans la voie que vous suggérez.

## Puzzle cosmique

Pour conclure ce numéro dense, voici, pour se détendre (euh… pas sûr) et admirer, un étonnant puzzle qui nous fait voyager tout autour de la planète depuis un satellite. Il s’agit de reconstituer de très spectaculaires photos (volcans, déserts, littoraux, banquise, etc.) de la terre prises par satellite. Le principe est d’arrêter avec sa souris les images qui tournent pour les faire de proche en proche coïncider avec celles qui sont fixes. Une fois l’image reconstituée, on clique dessus pour passer à l’image suivante, de plus en plus difficile à stabiliser. Attention exercice fascinant, addictif… et parfois éprouvant pour les nerfs fragiles.  
Astuces : commencez à repérer la partie fixe de la photo et garder cliquée l’image mobile la plus proche pour la positionner correctement avec la souris.  
Amusez-vous ! [C’est ici (cliquer)](https://okogame.ch/)

![](/media/puzzle-cosmique.jpg)