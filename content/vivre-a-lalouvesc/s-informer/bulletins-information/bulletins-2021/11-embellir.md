+++
date = 2021-05-30T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Juin 2021"
title = "11 - Embellir"
weight = 7

+++
L'été approche. Balayage, fleurissement, nouveau plan de stationnement, nouveaux hébergements de qualité sur le camping, préparation des décorations, présentation des événements... le village se fait beau pour accueillir les estivants.

Dans ce bulletin vous trouverez aussi un hommage de ses amis à Geny Detto qui nous a quittés. Et comme toujours, plein d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-11-1er-juin-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Bonjour à toutes et à tous

Nous sommes début juin, le temps passe très vite et nous sommes juste à l’ouverture de la période estivale. À ce sujet nous allons tout au long de la première semaine de juin travailler sur le nettoyage des rues et routes du village.

Il est demandé aux habitants de garer les autos sur les parkings afin de laisser libre champ aux employés communaux pour passer le Rotofil, ainsi jeudi 3 juin le camion de balayage pourra passer dans tout le village.

Nous sommes prêts pour la démolition de l’hôtel Beauséjour. Mais le déplacement des câbles électriques a nécessité des démarches réglementaires supplémentaires retardant les délais d'ouverture du chantier, nous allons donc devoir remettre cette démolition à l’automne prochain. Il ne serait pas raisonnable de lancer ces travaux pendant la période commerciale la plus importante de l’année.

Je souhaite à l’ensemble de la population un très bel été avec une météo bloquée sur "beau fixe". Nous allons avoir plus de liberté, profitez-en bien, néanmoins respectez les indications données en matière de règles sanitaires. Nous ne sommes pas sûrs que toutes les difficultés soient passées, il faut rester prudent.

Je suis vraiment très heureux de l’implication de vous tous, à tout niveau pour la dynamique du village. Il renaît. Nous allons le fleurir en tous coins, essayez d’en faire autant dans vos maisons, balcons, jardins, entretenez vos trottoirs. Il faut que le village resplendisse pour accueillir les estivants !

Œuvrons ensemble de toutes nos forces !

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Travaux

Les travaux sur le cimetière ont démarré. Rappel :  le chantier durera quelques mois, car il faut avancer prudemment pour ne pas abîmer les tombes. Ainsi le mur sera démoli puis reconstruit progressivement par tranches de quelques mètres avec séchage dans l’intermédiaire. Le mur d’enceinte et l’escalier seront aussi révisés.

![](/media/cimetiere-travaux.jpg)

Nous souhaitions faire tomber l'hôtel Beauséjour avant la saison estivale. L'appel d'offres a suivi son cours et le maître d’œuvre a été choisi. Il était prêt à engager les travaux. Mais la signature des conventions entre Enedis et les propriétaires concernés par le déplacement de la ligne électrique a pris du retard.

Il n'était plus possible de terminer le chantier avant le début de la saison estivale. Nous avons donc décidé à regret de reporter le démarrage de la démolition à la seconde quinzaine de septembre.

### Nettoyage et fleurissement

Un grand nettoyage et balayage du village sera entrepris dans la première semaine de juin. Les fleurs seront installées la semaine suivante.

Ainsi le village montrera un visage souriant pour accueillir les estivants de la saison 2021.

### Rencontre avec le directeur diocésain de l'enseignement catholique

Jacques Burriez accompagné de Julien Besset ont rencontré Patrick Jouve, directeur diocésain de l'enseignement catholique, le 27 mai dans ses locaux à Viviers.

Ils ont pu lui rappeler l'attachement et le soutien de la municipalité à l'école Saint Joseph et lui expliquer pourquoi il avait été réglementairement indispensable de rédiger et signer une nouvelle convention entre l'OGEC et la Mairie. Cette convention vise à assurer à l'école un budget pérenne.

Le directeur diocésain les a remerciés pour leur visite et les informations données. Chacun a souhaité que les incompréhensions qui ont pu se manifester soient maintenant levées. Ainsi le Maire rencontrera les parents d'élèves à la prochaine rentrée.

### Nous voterons les 20 et 27 juin dans la nouvelle salle de la Mairie

Les élections régionales et départementales se tiennent le 20 juin pour le premier tour et  le 27 juin pour le second tour.

Les conseillers régionaux sont élus pour 6 ans selon un système mixte combinant les scrutins majoritaires et proportionnels ([explication du système](https://www.service-public.fr/particuliers/vosdroits/F1958)). [Candidatures pour la Région Auvergne-Rhône-Alpes](https://elections.interieur.gouv.fr/regionales-2021/84/C184.html).

Les conseillers départementaux sont élus aussi pour 6 ans. Dans chaque canton un binôme (femme-homme) est élu au scrutin majoritaire ([explication du système](https://www.service-public.fr/particuliers/vosdroits/F1958)). [Candidatures pour le canton du Haut-Vivarais](https://elections.interieur.gouv.fr/departementales-2021/007/C10070708.html) auquel appartient Lalouvesc.

Pour la première fois depuis longtemps, nous voterons dans la Mairie, dans la nouvelle salle du Conseil entièrement refaite. Ce sera la meilleure occasion de la découvrir pour toutes les citoyennes Louvetonnes et tous les citoyens Louvetous.

## Zoom

### Modifications au plan de circulation et de stationnement

Un groupe de travail du Comité vie locale, comprenant des élus et des non-élus, s'est réuni pour réfléchir à une meilleure organisation de la circulation et du stationnement dans le village. Il fallait faire preuve d'inventivité pour concilier trois objectifs : 1. la sécurité et le confort des piétons, 2. le meilleur compromis entre les contraintes et les besoins des uns et des autres, 3. les moyens limités d'une petite commune.

Sur la base des propositions du groupe, voici les décisions prises qui seront applicables dès le mois de juillet. Nous tirerons les leçons cet automne de cette nouvelle organisation et corrigerons ce qui doit l'être.

#### Circulation

{{<grid>}}

{{<bloc>}}

![](/media/circulation-2021-14.jpg)

Le panneau indiquant le parking planté à gauche de la Basilique induit les automobilistes en erreur. La flèche semble indiquer qu'il faut tourner à gauche, c'est-à-dire vers l'école. Il faut donc inverser la flèche et, par sécurité, mettre un panneau supplémentaire à l'intersection suivante pour confirmer la direction à prendre.

{{</bloc>}}

{{<bloc>}}

![](/media/circulation-2021-12.jpg)

La rue de l'école est empruntée à tort par nombre de voitures, de plus, malgré l'interdiction de stationner, des automobilistes se garent dans la rue, gênant la circulation et induisant un danger pour les usagers de l'école.

Pour lever cette difficulté, deux poteaux amovibles seront implantés sur la chaussée, rétrécissant le passage, accompagnés de deux panneaux indiquant l'interdiction d'emprunter la voie, sauf pour les riverains et les usagers de l'école et rappelant l'interdiction de stationner.

{{</bloc>}}

{{<bloc>}}

![](/media/circulation-2021-15.jpg)

La rue de la fontaine est une rue étroite, fragile et historique du village, empruntée par de nombreux piétons. Elle est mise en sens unique dans le sens de la montée. De plus, elle est interdite aux poids lourds qui dégradent la chaussée.

{{</bloc>}}

{{<bloc>}}

![](/media/circulation-2021-16.jpg)

La circulation des piétons est dangereuse depuis l'Abri du pèlerin vers le centre du village. Il est indispensable de ralentir les véhicules qui entrent dans le village par le nord. La solution serait de mettre un stop sur la route qui vient de Saint Bonnet. Cette route étant une départementale, la décision revient à la DDT. Nous lui ferons une proposition en ce sens.  
Pour ne pas reporter la difficulté, sur les automobilistes venant de Rochepaule, un ralentisseur sera posé sur cette voie.

{{</bloc>}}

{{<bloc>}}

![](/media/circulation-2021-13.jpg)

Les difficultés de croisement de véhicule, la montée en hiver, sans parler de la nécessité du passage du chasse-neige, conduisent à réviser les sens de circulation entre la rue du Pré du moulin et le chemin neuf.

Un sens unique de montée et de descente seront installés entre la rue du pré du moulin (montée) et le chemin neuf (descente). De plus, un panneau de "laisser le passage" sera placé à l'embranchement avec la rue du Pré du moulin de façon à laisser la priorité à la circulation montante.

{{</bloc>}}

{{<bloc>}}

![](/media/circulation-2021-11.jpg)

Il existe deux sens de circulation opposés autour de la place du lac, un pour les véhicules individuels, un pour les autocars. Cette situation absurde amène régulièrement des incompréhensions qui tournent parfois au vinaigre.  
Pour des questions pratiques il n'est pas possible de modifier le sens de circulation des autocars. C'est donc celui-là qui primera dorénavant. Le sens unique de circulation sera de tourner autour de la place du lac dans le sens des aiguilles d'une montre.

{{</bloc>}}

{{</grid>}}

#### Stationnement

Le stationnement dans le village n'est pas toujours organisé au mieux et de mauvaises habitudes ont été prises gênant la circulation des voitures et mettant en danger les piétons.

L'esprit général du nouveau plan de stationnement sur le centre du village est de limiter le stationnement dans les rues, et de privilégier les parkings. L'objectif est de dégager le centre bourg pour le rendre plus attrayant et mieux sécurisé.

Nous ne manquons pas de parking dans le village et chacun pourra facilement garer sa voiture à moins de quelques centaines de mètres de son domicile.

**Près de la basilique**  
La place réservée personne à mobilité réduite (PMR) dans la rue le long de la basilique est en pente et donc impraticable. Elle sera remplacée par deux places ordinaires.  
En remplacement, une place PMR sera installée dans le parking des Trois pigeons.

**Rue des Alpes et devant l'hôtel du Monarque**  
Aucune place de stationnement sera offerte rue des Alpes.  
Une place PMR et un dépose-minute seront installés devant l'hôtel du Monarque.

**Rue de la fontaine**  
Aucun stationnement autorisé.

**Rue des Cévennes**  
Le dépose minute est conservé au coin de la rue de la fontaine.  
En attendant la mise en place du parking sur l'espace Beauséjour, une place dépose-bagages est ouverte devant l'hôtel des voyageurs. Ce parking sera bienvenu car tout stationnement entre l'hôtel des voyageurs et le bar des Cévennes interdit le croisement des voitures dans la rue.  
La rue s'élargit ensuite un peu autorisant une place handicapée installée devant le Souk à Guigui. Un dépose-minute est ouvert devant le Vival.  
Toutes les autres places de stationnement sont donc supprimées rue des Cévennes.

**Route d'Annonay**  
Deux places de stationnements sont installées devant le "Grand hôtel" sans gêner les ouvertures et un marquage au sol signalera un trottoir réservé aux piétons. Deux autres places sont ouvertes le long du mur en face du chemin débouchant sur la rue.  
Ces quatre places sont les seules autorisées sur cette portion de la route d'Annonay.

Le plan de stationnement sera mis en pratique dès cet été. A cette fin la signalisation sur le sol sera revue et des panneaux seront disposés. Dès leur mise en place un arrêté municipal sera émis.

La gendarmerie disposera d'une copie du plan de stationnement et sera amenée à prendre les mesures réglementaires en cas d'infraction. Mais nous comptons avant tout sur l'esprit de responsabilité de chacun.

**Parking sous la Mairie**  
Deux places réservées pour les pompiers seront indiquées.

### Ouverture de deux hébergements de qualité sur le camping

{{<grid>}}

{{<bloc>}}

![](/media/cottage-pmr-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/cottage-pmr-6.jpg)

{{</bloc>}}{{</grid>}}

D'après les constructeurs, le tarif de location au Camping du Pré du moulin pour ce Cottage haut de gamme est actuellement le plus bas du marché. Faites-le-savoir !

{{<grid>}}{{<bloc>}}

![](/media/lodge-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/lodge-3.jpg)

{{</bloc>}}{{</grid>}}

Marier le plaisir du camping et la douce chaleur d'un poêle à bois, ce n'est possible qu'au camping du Pré du Moulin à Lalouvesc, grâce à cette tente Lodge tout confort ! Faites le savoir à vos amis et sur vos réseaux !

Pour réserver [c'est ici](/decouvrir-bouger/hebergement-restauration/camping/).

### Eugenio Detto, dit « Geny », artiste

Geny nous a quittés paisiblement le dimanche 2 mai à l’hôpital de St Félicien. Il était Louvetou depuis 2001.

{{<grid>}}

{{<bloc>}}

![](/media/eugenio-detto-dit-geny-artiste-1.jpeg)

Geny jusqu’à la fin de sa vie joue encore la comédie en imitant Woody Allen

{{</bloc>}}{{<bloc>}}

Geny est originaire des Pouilles au sud de l’Italie. Arrivé à St Étienne à l’âge de 4 ans avec ses parents, il se met très tôt à la guitare, et devient en 7-8 ans un des meilleurs guitaristes en France. A 18 ans il en fait sa profession.

En 1965 à St Étienne, il rencontre Graeme Allwright, avec qui, à Paris à la Contrescarpe, il rode ses chansons qui deviendront de très grands succès en France. Par la suite il accompagnera de grands noms tels que Guy Béart, Marie Laforêt, Lény Escudero, Mouloudji, et bien d’autres. Avec eux il fera le tour du monde.

En 1975, il s’installe à Lyon où la peinture prendra une grande place dans sa vie et, peu à peu, elle deviendra son nouveau métier. En 2003 il obtient le Prix de la Société des Artistes Français. On retrouve ses tableaux partout en Europe et jusqu’en Australie ou aux États-Unis. Il expose dans de prestigieuses galeries en France. Et, bien sûr, au Carrefour des Arts cette même année 2003 !

{{</bloc>}}

{{</grid>}}

C’est à St Étienne qu’il a rencontré Maurice Richaud, pour une amitié de plus de 40 ans. Maurice lui fait connaître Lalouvesc où il pose ses valises en 2001. C’est alors que se créent des amitiés louvetonnes lors de soirées musicales inoubliables chez Didier Chieze, où avec les deux premières notes d’une chanson il accompagnait entre autre Régine C. et son « Étoile des neiges », Maurice R. et son Brassens, Didier C. et son Johnny, Pierre R. et son Aznavour, Jacques T. et son Léo Ferré, Françoise C. et sa Piaf, Françoise P. et son Deguelt, Odette G. et son « Il faut que je m’en aille », et bien d’autres encore !

A Annonay existe un héritage de Geny : le Groupe « Eugène Électre ». En effet, depuis les années 75, il accompagnera certains de ses musiciens guitaristes puis finalement le groupe dans son ensemble. Il maintiendra avec eux des liens profonds d’amitiés à tel point que les enfants de Geny ont offert au groupe sa guitare.

Merci Geny, nous n’oublierons ni ta musique, ni ta peinture, ni ton humour de blagues incessantes, et surtout pas ton humanisme.

Tous tes amis de Lalouvesc.

Pour en savoir plus : son [site de peinture](https://www.eugeniodetto.com/oeuvres), le [Carrefour des Arts 2003](http://www.carrefourdesarts-lalouvesc.com/p_detto.html).

## Suivi

### Livre et performance d'artistes au Carrefour des arts

Une conférence de presse s'est tenue pour présenter les programmes 2021 du [Carrefour des arts](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/10-s-organiser/#un-festival-poétique-de-formes-et-de-couleurs-pour-le-carrefour-des-arts-2021) et des [Promenades musicales](/vivre-a-lalouvesc/s-informer/actualites/programme-2021-des-promenades-musicales/) dont les Louvetous avaient eu la primeur sur lalouvesc.fr.

A noter sur vos agendas, les 16 et 17 août un moment de grâce, un événement inédit réunira à Lalouvesc les deux manifestations.

Les poèmes de Jorge Pimentel, poète péruvien, et fondateur du mouvement « Hora Zero » (heure zéro), traduits en français par Jorge Najar ont inspiré le compositeur Felipe Carrasco et la peintre Martine Jaquemet.

Le lundi 16 août après-midi, Martine Jaquemet réalisera en public une peinture pendant la lecture des poèmes de Jorge Pimentel et leur interprétation musicale par le quatuor à cordes Solystelle de Gilles Lefevre.

Le mardi 17 août en soirée dans la basilique de Lalouvesc, le quatuor à cordes Solystelle de Gilles Lefevre se produira en concert. Les œuvres réalisées par Martine Jaquemet seront exposées et elle pourra en marge du concert présenter sa démarche créative.

Ci-dessous, un poême de Jorge Pimentel inscrit sur un livre d'art offert par la peintre au musicien.

![](/media/livre-d-artiste-jaquelet-carrefour-des-arts-2021.jpg)

### Visitez depuis chez vous le centre de valorisation des déchets du SYTRAD

{{<grid>}}{{<bloc>}}

![](/media/sytrad-2021-2.jpg)

{{</bloc>}}{{<bloc>}}

Au regard du contexte sanitaire actuel, le SYTRAD (SYndicat de TRaitement des déchets Ardèche Drôme) ne peut réaliser, comme à son habitude, une porte ouverte grand public de son centre de valorisation des déchets ménagers résiduels d’Etoile-sur-Rhône.

Une visite de site gratuite en direct à distance est organisée jeudi 3 juin de 18h30 à 19h15. De chez vous, découvrez ce site de traitement des ordures ménagères en vous connectant dès 18h15 sur la page Facebook [@SYTRAD2607]().

{{</bloc>}}{{</grid>}}

### Trois après-midi de jeux pour les enfants au centre de loisirs de Jaloine en juin

![](/media/chifoumi-2021-2.jpg)

### Lalouvesc dans le Magazine de la Chambre de Métiers et de l'Artisanat

![](/media/artisanat-juin-2021.jpg)

### Quelques actualités parues sur Lalouvesc.fr au mois de mai

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de mai qui restent d'actualité.

#### Stationnement réglementé du lundi 31 mai au jeudi 3 juin pour le grand nettoyage de printemps

27 mai 2021  
Tout stationnement interdit sur les rues et les routes [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/stationnement-reglemente-du-lundi-31-mai-au-jeudi-3-juin-pour-le-grand-nettoyage-de-printemps/ "Lire Stationnement réglementé du lundi 31 mai au jeudi 3 juin pour le grand nettoyage de printemps")

#### Conseil municipal le 7 juin à 20h

27 mai 2021  
Pour des raisons sanitaires, le Conseil se tiendra à huis-clos. Ordre du jour.... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/conseil-municipal-le-7-juin-a-20h/ "Lire Conseil municipal le 7 juin à 20h")

#### Robert Marchand nous a quittés à 109 ans

22 mai 2021  
Robert Marchand, champion cycliste centenaire, icône de l'Ardéchoise. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/robert-marchand-nous-a-quitte-a-109-ans/ "Lire Robert Marchand nous a quitté à 109 ans")

#### Circulation alternée entre le 7 et 18 juin sur la route de Saint Félicien

21 mai 2021  
Travaux de réfection de la chaussée entre le col du Marchand et Lalouvesc [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/circulation-alternee-entre-le-7-et-18-juin-sur-la-route-de-saint-felicien/ "Lire Circulation alternée entre le 7 et 18 juin sur la route de Saint Félicien")

#### Lalouvesc.fr, site pilote des sites web durables et accessibles pour les villages d’aujourd’hui et de demain

15 mai 2021  
Plateaux numériques a fait de lalouvesc.fr un démonstrateur pour la conception de sites responsables, adaptés aux enjeux de demain. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lalouvesc.fr-site-pilote-des-sites-web-durables-et-accessibles-pour-les-villages-d-aujourd-hui-et-de-demain/ "Lire Lalouvesc.fr, site pilote des sites web durables et accessibles pour les villages d’aujourd’hui et de demain")

#### Combien d'eau est tombée sur Lalouvesc lundi dernier ?

11 mai 2021  
L'alerte météo ne s'est pas trompée, les pluies ont été intenses... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/combien-d-eau-est-tombee-sur-lalouvesc-lundi-dernier/ "Lire Combien d'eau est tombée sur Lalouvesc lundi dernier ?")

#### La Mairie et l'Agence postale passent à l'horaire d'été

5 mai 2021  
Attention, à partir du 17 mai les horaires changent... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-mairie-et-l-agence-postale-passent-a-l-horaire-d-ete/ "Lire La Mairie et l'Agence postale passent à l'horaire d'été")

## Quelle déco pour cet été ? (Puzzle)

Le comité des fêtes s'active toujours à la décoration du village. L'atelier peinture a achevé son travail. Les décorations seront bientôt en place. A découvrir, le thème de cette année.

<iframe src="https://www.jigsawplanet.com/?rc=play&pid=3b4890be6349&view=iframe" style="width:100%;height:600px" frameborder=0 allowfullscreen></iframe>