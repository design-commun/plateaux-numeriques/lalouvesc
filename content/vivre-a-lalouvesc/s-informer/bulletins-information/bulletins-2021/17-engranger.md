+++
date = 2021-11-29T23:00:00Z
description = ""
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "n°17 Décembre 2021"
title = "17 - Engranger"
weight = 1

+++
Novembre a été un riche mois de récoltes pour la Commune : un nouveau chasse-neige, un nouveau City Park, une subvention pour le site web, un bon bilan financier du camping, un écolotissement qui se remplit, une saison satisfaisante pour l'Office du tourisme, des sapeurs-pompiers toujours en alerte... et aussi mois de réflexions pour préparer l'avenir avec la réunion du Comité de développement et le lancement d'un concours d'idées pour le jeu-monument. Voilà quelques unes des informations de ce numéro, mais vous en trouverez aussi bien d'autres, comme toujours... sans oublier, bien sûr, la suite du feuilleton des enfants de l'école St Joseph.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-17-1er-decembre-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Chers amis,

Ce n'est pas parce qu'en hiver on dit "fermez la porte, il fait froid dehors", qu'il fait moins froid dehors quand la porte est fermée, donc couvrez-vous car le froid est annoncé.

Nous sommes à deux pas de Noël, c’est le moment de décorer nos maisons, nos façades, nos jardins, nos vitrines de boutique. Je vous encourage tous à faire de votre mieux pour donner un air joyeux, festif à notre village. De notre côté à la mairie, avec quelques bénévoles et l’appui de notre comité des fêtes, nous avons élaboré tout un programme de décorations que nous allons mettre en place ces jours-ci.

Sur le plan pratique n’oubliez pas vos équipements automobiles, prenez soin de bien respecter le stationnement en vue du déneigement.

Prenez soin de votre santé pour finir l'année, nous sommes encore en période tendue face a l'épidémie, prenez toutes les précautions d'hygiène pour limiter les risques de contagion surtout dans vos rassemblements familiaux pour les fêtes de fin d'année.

La neige est là avec ses quelques désagréments, mais cela n'est rien au regard des plaisirs reçus, la beauté des paysages, la joie des enfants, nous fait vite oublier les quelques coups de pelle pour dégager nos trottoirs.

Je vous souhaite donc à toutes et à tous de très belles fêtes de Noël. Nous aurons l'occasion de nous réunir pour fêter cela ensemble, profitez bien de ce moment en famille et avec vos amis, c'est un moment de convivialité qu'il ne faut surtout pas manquer.

Amitiés sincères et joyeux Noël !

Votre dévoué Jacques Burriez

## Actualités de la Mairie

### City Park

Au cours des journées citoyennes du printemps dernier des Louvetous ont nettoyé à fond le tennis du camping qui en avait bien besoin.

{{<grid>}}{{<bloc>}}

![](/media/journee-citoyenne-jour-3-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-jour-3-6.jpg)

{{</bloc>}}{{</grid>}}

Le terrain a servi cet été pour quelques parties de balle au chasseur, mais il manquait un équipement plus adapté pour qu'il puisse répondre aux besoins sportifs de nos jeunes. Une hypothèse était d'y installer un terrain multi-sport, comprenant des cages de foot et des paniers de basket.

C'est pourquoi la Mairie a saisi l'occasion de la vente d'un terrain multi-sport, un _city park,_ par la Mairie d'Aubière près de Clermont-Ferrand. L'équipement était mis aux enchères et nous avons pu l'acquérir pour la somme de 8403 €, sachant qu'un tel équipement neuf coûte plusieurs dizaines de milliers d'euros.

{{<grid>}}{{<bloc>}}

![](/media/city-park-2021-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/city-park-2021-4.jpg)

{{</bloc>}}{{</grid>}}

L'idée est donc de l'installer sur le terrain de tennis du camping, une autre option serait le terrain de tennis de Grand Lieu, à côté de l'étang.

Cerise sur le gâteau, puisque les terrains de tennis sont déjà grillagés, le grillage qui entoure le terrain multi-sport pourra servir à border le terrain de foot du côté de la pente, comme le réclament légitimement les joueurs du club.

Il reste à démonter, récupérer et remonter tout le matériel... pour cela nous aurons sans doute encore besoin de quelques bonnes volontés. Mais, grâce à nos efforts conjugués, nous aurons pour l'année prochaine un nouvel équipement sportif. Cela nous rend fiers... et il y a un vrai retour sur investissement !

### Une excellente saison pour le camping

Nous avions fait le pari que les efforts des journées citoyennes, la réparation des hébergements, ainsi que l'achat de nouveaux équipements seraient récompensés.

{{<grid>}}{{<bloc>}}

![](/media/journee-citoyenne-1-2021-11.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-1-2021-21.jpg)

{{</bloc>}}{{</grid>}}

Pari gagné !

Nous avions mis au budget une prévision de recettes, optimiste mais prudente, de 53 k€ pour les rentrées du camping sur la saison 2021. Malgré la Covid 19 et une météo désastreuse au mois de juillet, les recettes ont atteint 60 k€ et les retours des campeurs sont élogieux. Mieux, nous avons constaté qu'il reste encore une marge de progression et, à moins d'imprévus, on peut s'attendre à des rentrées encore plus importantes l'année prochaine.

### On aménage l'écolotissement

Quatre compromis de vente signés, un cinquième en préparation ! La mairie a lancé la demande d'ouverture du "prêt in fine" auprès de la Banque Postale pour financer les travaux d'aménagement et de viabilisation qui se réaliseront début 2022 et devraient prendre quatre mois.

Rappelons qu'un prêt in fine permet de moduler les sommes empruntées et les remboursements en fonction des besoins en trésorerie. Le principe général est que l'aménagement doit être financé par la vente des lots.

### Le chasse-neige déjà au travail

![](/media/arrivee-chasse-neige-2021.jpg)

Il n'aura pas fallu longtemps pour mesurer l'importance pour le village et ses hameaux d'avoir un chasse-neige à sa disposition et un pilote dévoué et opérationnel pour le conduire. Le tracteur était livré jeudi 25 novembre. Dimanche 28, Dominique Balaÿ était à son volant dans les rues du village et sur les routes communales. La première neige de l'hiver était tombée...

![](/media/chasse-neige-28-nov-2021.jpg)

### Lancement du concours d'idées pour le jeu-monument

Grâce à l'appui et aux conseils des architectes amis du village, Philippe Bosseau, Jérôme Solari et Jacques Morel, une étape importante de notre projet de jeu monument vient d'être franchie avec le lancement du [concours d'idées](https://www.lalouvesc.fr/projets-avenir/opportunites/concours-d-idees-sur-un-jeu-monument-sur-le-parc-du-val-d-or/) sur le jeu-monument. Ouvert aux étudiants de dernière année, il a été envoyé aux écoles d'architecture, de design et de construction bois de la région. Il sera annoncé dans la presse locale et relayé par le CAUE et par Fibois, l'organisme représentant la filière bois.

![](/media/jeu-monument-article-dl-30-11-2021.jpg)

Plus nous aurons d'inscriptions, plus nos chances de recueillir des idées de qualité seront grandes, n'hésitez pas à le relayer et le faire savoir dans vos réseaux !

### Des subventions pour le site web

La nouvelle version du site lalouvesc.fr a été mise en ligne le 1er mars. Ce site a été conçu gratuitement par les designers de Plateaux numériques comme un démonstrateur de sites éco-responsables à destination de petites communes. A partir de cet exemple, les designers ont reçu une importante subvention de l'Agence nationale de la cohésion des territoires pour développer leur activité dans d'autres départements. De notre côté, nous venons d'être informés que nous allions recevoir une subvention de 20 k€ de la préfecture pour analyser l'utilisation du site, en développer des fonctionnalités et faire connaître cette démarche et ces méthodes en Nord-Ardèche. Il y aura donc quelques évolutions en 2022, pour le mieux.

En attendant, voici quelques chiffres sur le rythme de publication et sur la fréquentation du site.

Depuis mars dernier, 182 actualités ont été publiées, soit un peu plus de 20 par mois, si on ajoute celles, nombreuses, inclues dans les bulletins mensuels, on arrive à près de 350 textes d'information publiés au cours des 10 derniers mois. Cela fait beaucoup, cela reflète la vitalité du village.

Mais direz-vous, à quoi bon si personne ne les lit ? Pour en avoir le cœur net, tournons-nous maintenant vers les statistiques de consultations du site.

![](/media/stat-site-nov-2021.png "Consultations de lalouvesc.fr en novembre 2021")

Il y a aujourd'hui un peu plus de 290 abonnés qui reçoivent chaque lundi un message rappelant les actualités publiées la semaine précédente. Ce simple envoi produit un phénomène remarquable : chaque lundi, un pic des consultations des actualités varie entre 380 et plus de 600 vues dans la journée pour l'ensemble des actualités. Quant au bulletin mensuel, sa consultation va de 360 vues pour le bulletin de mai à plus de 600 pour le dernier bulletin de novembre, pourtant celui qui est en ligne depuis le moins longtemps. Pour ceux qui s'intéressent aux statistiques, tous les chiffres sont accessibles à partir de [cette page](https://simpleanalytics.com/lalouvesc.fr?period=day&count=1) (la page donne les statistiques du jour en temps réel, vous pouvez choisir votre période en cliquant sur <1d> qui ouvre un calendrier).

A l'évidence les amis de Lalouvesc sont friands des nouvelles de leur village... il va falloir nous organiser pour tenir le rythme.

### Encore une rupture de canalisation

Une importante fuite d'eau s'est déclarée vendredi 26 novembre soir montée du Chemin neuf. La réparation a été effectuée dans la journée par le changement d'un tronçon de canalisation sur 3 mètres.

Ce nouvel incident, pris en charge sur les finances de la Commune, témoigne une nouvelle fois de la vétusté des réseaux d'alimentation du village et de la nécessité de procéder à leur remplacement. Il est à espérer que les instances subventionnaires répondent favorablement à nos demandes insistantes.

### Rappel des obligations des propriétaires de chiens

Par arrêté du maire, **les chiens doivent impérativement être tenus en laisse à l'intérieur du village de Lalouvesc**.

Les déjections canines sont autorisées seulement dans les caniveaux, à l’exception des parties de ces caniveaux se trouvant à l’intérieur des passages pour piétons.

Nous avons constaté un laisser-aller ces derniers temps. Pour la sérénité de toutes et tous, petits et grands, et la propreté du village, il est impératif que ces obligations soient respectées.

### Fermeture de la Mairie et de l'Agence postale entre Noël et le jour de l'an

La Mairie et l'Agence postale seront fermées pour la trêve des confiseurs, soit du 24 décembre au soir au 3 janvier au matin.

## Zoom

### Comité de développement

La seconde réunion du Comité de développement s'est tenue vendredi 26 novembre. Ce Comité rassemble des élus et des non-élus deux fois dans l'année pour discuter de l'avenir du village. De nombreuses informations ont été échangées, un compte-rendu détaillé sera bientôt mis en ligne, mais voici déjà quelques éléments sur les quatre grands thèmes qui ont été débattus.

#### Beauséjour, parc du Val d’or, tourisme éco-responsable

Le processus menant à la démolition de l'hôtel suit son cours. Il faut penser au nouvel agencement qui ouvre la perspective sur les Cévennes. Un concours d'idées est lancé pour un jeu monument sur le parc du Val d'Or en contrebas.

Celui-ci pourrait être le point de départ des parcours thématiques : chemin des Afars, chemin botanique, champignons, lapins, gymnastique, chemin de croix. Il est proposé que les journées citoyennes de 2022 soient consacrées à la remise en état de ces parcours avec une implication de toutes les forces du village : citoyens, associations, école, pompiers, commerçants, etc, pilotées par un comité ad hoc.

De plus, le déménagement de l'Office du tourisme doit être mis à l'étude avec l'ouverture d'une maison ouverte sur un tourisme écoresponsable.

#### Quartier Basilique

Il s'agit du cœur historique du village. Il faut commencer à réfléchir à sa restructuration qui pourrait s'engager dans la seconde moitié du mandat municipal.

Il faut penser à la réparation définitive du toit de la basilique, à la démolition des maisons délabrées acquises par la municipalité, à l'utilisation des locaux laissés libres par l'OT et la Maison de la randonnée en cas de déménagement et donc un réaménagement global du quartier.

La maison St Régis, de son côté, réfléchit à l'ouverture de gîtes en location pour des familles et à une meilleure valorisation du patrimoine, notamment le musée Oudin.

Le colloque des Villes sanctuaires qui se tiendra au printemps à Lalouvesc pourrait être l'occasion d'amorcer une réflexion globale.

#### Animation et culture

L'animation et les événements culturels sont un des points forts de la saison estivale du village.

Outre le CAC, le bâtiment Ste Monique a joué un rôle important pour la préparation des événements sur le village, notamment pour les répétitions des Promenades musicales, la préparation des décorations ou l'entreposage du matériel du Comité des fêtes.

Mais quelle que soit l'issue de la vente, les coûts de la mise aux normes et de l'isolation du bâtiment sont trop importants pour envisager une utilisation rapide. Il faut donc réfléchir rapidement à une alternative pour ne pas mettre en difficulté la belle dynamique actuelle des associations. Une hypothèse serait la construction d'une salle de réception au CAC au-dessus d'un espace d'entreposage.

Par ailleurs, on peut regretter l'échec de la vente du Cénacle et du projet culturel qui y était associé. La mairie a repris contact avec les sœurs du Cénacle pour tenter d'organiser une présentation du bien plus visible pour d'éventuels acquéreurs.

Enfin, les membres du Comité se sont réjouis de l'ouverture envisagée d'un club des ados dans les locaux de l'ancienne école publique et formulent des vœux pour que le projet aboutisse vite.

#### Hébergements

Avec la rénovation du camping, de l'hôtel du Monarque, l'ouverture prochaine de l'écolotissement, la rénovation de l'Abri du pèlerin, les divers achats de résidences et, si ces projets aboutissent, la reprise du bâtiment Ste Monique et les travaux dans la Maison St Régis, on observe une nette amélioration de l'offre d'hébergements sur le village.

Il faut penser à une meilleure intégration des nouveaux arrivants, saisonniers ou définitifs. Cela passe par une attention particulière aux développements des services (garde, santé) et plus ponctuellement à un accent mis sur l'accueil. L'OT prévoit un kit d'information pour les touristes, la mairie pourrait le compléter par des éléments sur les services, les associations, les activités en direction des résidents permanents ou réguliers. Une journée de présentation des associations pourrait aussi être organisée.

### Retour sur une année des sapeurs-pompiers à Lalouvesc

Le mot du chef de centre :

_L’année 2021 a été marquée par une légère augmentation de l’activité opérationnelle et se termine avec une centaine d'interventions au total, incluant les missions qui nous ont été confiées à l'occasion de la crise sanitaire._

_Les sapeurs-pompiers de Lalouvesc, ont ainsi prêté main-forte à la maison de retraite du Balcon des Alpes, pendant une quinzaine de jours. Chaque jour, un sapeur-pompier s'est rendu à la maison de retraite, pour accompagner et aider le personnel dans ses missions quotidiennes._

{{<grid>}}{{<bloc>}}

![](/media/sapeurs-pompiers-2021-vasantt.jpg)

{{</bloc>}} {{<bloc>}}

_La saison feu de forêt  a été plutôt calme pour notre département. Mais cela ne nous a pas empêché de partir et de renforcer les départements voisins. Notre véhicule de secours à personne (VSANTT), a été envoyé dans le Var, pour l’incendie de 7.100 ha, qui a ravagé ce département. Il est resté du début jusqu’à la fin de l’incendie, le personnel quant à lui était relevé toutes les 72h. Grâce aux centres voisins de St Agrève, St Félicien et St Alban d’ Ay, nous avons pu assurer les relèves sur cet engin pour une continuité opérationnelle sur l'incendie. Ainsi,  le sapeur 1ère Classe KPONOUME Koffi est resté 72h sur ce sinistre._

{{</bloc>}}{{</grid>}}

_L’effectif des sapeurs-pompiers de Lalouvesc est actuellement de treize soldats du feu. Mais malheureusement ce ne sont pas treize sapeurs-pompiers qui peuvent intervenir tous les jours. Après les annonces de vaccination du gouvernement, nous avons une baisse du nombre de sapeurs-pompiers opérationnels. Aujourd’hui nous assurons la continuité opérationnelle à seulement neuf sapeurs-pompiers. Nous espérons rapidement retrouver nos collègues inactifs pour le moment._

{{<grid>}}{{<bloc>}}

_Ce mot est aussi pour moi une nouvelle opportunité pour lancer un appel aux jeunes comme au moins jeunes qui voudraient venir renforcer les rangs des soldats du feu de Lalouvesc. C’est avec joie que mon équipe et moi-même vous ouvrirons les portes du centre de secours, pour vous faire découvrir et apprécier la passion de sapeurs-pompiers. Vous pouvez demander à chacun des sapeurs-pompiers du centre des informations sur le recrutement, ils seront très heureux d’y répondre._

{{</bloc>}}{{<bloc>}}

![](/media/sapeurs-pompiers-recrutement-2021.jpg)

{{</bloc>}}{{</grid>}}

_En cette fin d’année, les sapeurs-pompiers de Lalouvesc, vont commencer leur tournée de calendriers. Celui-ci met en avant les sapeurs-pompiers de Lalouvesc et leur spécialité. Les sapeurs-pompiers de Lalouvesc se joignent à moi pour vous présenter nos vœux les plus sincères de santé et de bonheur pour 2022. Et nous vous remercions d’avance pour votre accueil et votre générosité lors de notre passage._

Lieutenant ACHARD Lionel, Chef de centre du CPIR Lalouvesc

{{<grid>}}{{<bloc>}}

![](/media/manoeuvre-pompiers-2021-2.jpg "Manoeuvre des pompiers de Lalouvesc")

{{</bloc>}}{{<bloc>}}

![](/media/manoeuvre-pompiers-2021-1.jpg "Manoeuvre des pompiers de Lalouvesc")

{{</bloc>}}{{</grid>}}

## Une saison touristique perturbée par la crise sanitaire et la météo

Séverine Moulin de l’Office du Tourisme du Val d’Ay nous a confié ses impressions sur la saison passée.

_Le confinement d’avril a beaucoup impacté les vacances de printemps. De notre côté, à défaut de salons pour faire la promotion du territoire, nous avons misé sur le virtuel : salon Etourisme en ligne, podcast, interview radio et micro-aventures sur l’application Prairy…._

_En juillet la météo a perturbé des visites et n’a pas favorisé la location de vélos (-25%). Combinée avec l’obligation du passe sanitaire et la faveur à la réservation en ligne, elle a aussi freiné l’achat de billets de Peaugres._

#### _De la randonnée au patrimoine_

_Dans les demandes de documentation, la randonnée est toujours très prisée. La météo ne favorisait pas les grandes randonnées, mais des propositions de 2-3h étaient bienvenues. Les parcours thématiques comme le chemin des Afars et du chapelet infini furent ainsi une alternative pour les familles dépourvues de passe sanitaire. Les circuits sur Cirkwi permettaient de préparer la venue, et le topo apporte la précision cartographique._

![](/media/office-du-tourisme-saison-2021.jpeg)

_Nous pensons avec David Besset, animateur du camping, que des balades pédestres accompagnées pourraient utilement élargir celles qu’offre par ailleurs le centre équestre. En effet la rencontre avec les habitants est attendue. La découverte des artistes louvetous, du Musée Saint Régis pendant les pots d’accueil en témoigne. La richesse patrimoniale de Lalouvesc partagée lors de visites complète l’offre de « balades ». Déjà cette année, les balades botaniques du Sanctuaire de Lalouvesc avec Françoise Kunstmann, la promenade clownesque à St Alban d’Ay (M.-Ch. Brunel) et la balade bucolique à Satillieu (S. Defour) pour la fête de la science ou encore la sortie Contes et champignons en novembre ont eu du succès._

![](/media/office-du-tourisme-saison-2021-2.jpeg)

#### _Importance des événements_

_Les opérations hors les murs sur les événements avec 71 publications pour la page Facebook et 14 stands de l’office de tourisme ont permis d’aller à la rencontre des résidents et des vacanciers et de distribuer diverses brochures. Merci aux organisateurs, qui nous ont toujours bien accueillis._

_Le Carrefour des arts, le programme du Cinéma le Foyer, les concerts… sont toujours attendus… Lalouvesc est un formidable bouillon de culture. Le Trail des sapins relie les richesses du Val d’Ay._

_Merci aussi aux intervenants louvetous (Pierre Roger, Marie Christine Brunel, les Pères) de la fête de la science, au public présent, et aux cinq établissements scolaires ayant bénéficié d’interventions._

#### _L’apport de l’association Villes sanctuaires de France_

_Malgré la crise sanitaire qui affecte grandement les sanctuaires, nous bénéficions du travail des commissions de l’association : promotion à l’étranger (trois pays européens, et trois pays hors zone Europe), en France, presse, observatoire, circuits, site Internet et réseaux sociaux._

_Le colloque de l’association est prévu en 2022 à Lalouvesc… beaucoup de travail en perspective pour préparer l’accueil des participants._

## Suivi

### Un jeune louvetou, champion de tir à l'arc

Nathan Celette a 14 ans. En plein dans la cible dès sa première compétition ! Il est désormais champion départemental de tir à l'arc dans sa catégorie depuis le 22 novembre dernier à Privas avec 408 point sur 420 !

![](/media/nathan-celette-tir-a-l-arc-2021.jpg)

**Depuis quand fais-tu du tir à l'arc ?**  
_Je fais du tir à l'arc depuis un an et demi. J'ai été sélectionné cette saison pour intégrer l'équipe nationale de formation des jeunes._  
**Quels aménagements seraient nécessaires pour faire du tir à l'arc à Lalouvesc ?**  
_Il faudrait une salle de 30 m de profondeur en hiver et un terrain long de 100 m en été, ainsi que des cibles stramits, des blasons et le matériel de l'archer._  
**Quelles sont les meilleures qualités d'un bon archer ?**  
_Il faut être appliqué et concentré. Savoir maîtriser son stress. S'adapter à la météo._  
**Et pour la suite ?**  
_Ma prochaine étape sera de valider les niveaux. Ensuite passer à la coupe régionale et j'espère un jour des compétitions nationales..._

Nous souhaitons une bonne concentration et beaucoup de victoires à Nathan !

### Le Comité des fêtes et le Carrefour des Arts remercient leurs bénévoles

{{<grid>}} {{<bloc>}}

![](/media/carte-poetique-m-jaquemet-2021.jpg)

{{</bloc>}} {{<bloc>}}

Après le succès du Trail malgré une météo difficile, dès le 4 décembre les bénévoles du Comité des fêtes seront au travail pour installer [les décorations de fin d'année](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/decorations-le-village-sera-en-fete-pour-la-fin-d-annee/) dans le village.

Le Comité des fêtes organise le 11 décembre une soirée de remerciements pour les bénévoles, au programme : restauration, musique, karaoké, danses.

De son côté, le Carrefour des Arts a remercié ses bénévoles en leur offrant une carte poétique numérotée et dédicacée personnellement par l'artiste Martine Jaquemet (voir ci-contre).

{{</bloc>}} {{</grid>}}

### Le Club des deux clochers renouvelle son bureau

Le club des deux clochers a renouvelé son conseil d'administration au cours de son assemblée générale du 25 novembre. Ci-dessous les nouveaux responsables de l'association :

* Président : Georges Ivanez, vice-président : Frédéric Brunel ;
* Trésorière : Renée Bossy, vice-trésorière : Annie Besset ;
* Secrétaire : Paul Besset, vice-secrétaire : Geneviève Foriel.

### Changements de lits à l'Abri du pèlerin

![](/media/abri-du-pelerin-2021-1.JPG)

Les lits en fer des dortoirs, qui rappelaient un peu l'hôpital qui les avaient fournis, ne seront bientôt qu'un souvenir. Ils seront remplacés par des lits en bois, donnés par un internat d'un établissement scolaire des jésuites de Valence.

### La cabane des marmots à Jaloine

Malgré les difficultés issues des mesures sanitaires indispensables, le Centre de loisir de la CCVA à St Romain d'Ay a pu accueillir les enfants du territoire en 2021.

![](/media/centre-de-loisir-jaloine-2921.jpg)

\- **Temps extra-scolaire :**

Centre de loisirs de Jaloine : ouverture lors de toutes les vacances scolaires (sauf Noël), de 7h30 à 18h00, pour les enfants de 3 à 11 ans. Pour plus de renseignements (permanences d’inscriptions, programmes, informations diverses…) : [cdl.jaloine@gmail.com](mailto:cdl.jaloine@gmail.com) ou au 07.66.49.07.90

\- **Temps** **scolaire :**

Les « Mercredis Loisirs » à Jaloine : ouverture toute la journée, de 7h30 à 18h00. Possibilité de demi-journée (avec ou sans repas) et de journée (avec ou sans repas). Pour plus de renseignements : [cdl.jaloine@gmail.com](mailto:cdl.jaloine@gmail.com) ou au 07.66.49.07.90

### Réseau d'accueil citoyen pour les femmes victimes de violence en nord-Ardèche

Le Centre d'information sur les droits des femmes et des familles (CIDFF) de l’Ardèche crée le Réseau d’Accueil Citoyen (RAC), afin de proposer des solutions d’hébergement d'urgence pour femmes victimes de violences.

![](/media/rac-cidff-2021.jpg)  
Après l’intégration d’une première famille sur le secteur Nord-Ardèche au cours d’une commission technique, le CIDFF est toujours à la recherche de citoyen-ne-s bénévoles pouvant intégrer le **Réseau d'Accueil Citoyen** sur le secteur Nord et Sud Ardèche !  
Le Réseau d’Accueil Citoyen a pour vocation l’accueil et l’hébergement en urgence de femmes en situation de violences conjugales ou intrafamiliales, avec ou sans enfant-s, permettant une mise à l’abri rapide dans un temps limité avec un accompagnement spécifique.  
L’hébergement est bénévole, temporaire et discret, dans un esprit de solidarité citoyenne, avec le soutien des professionnelles du CIDFF de l'Ardèche.  
**Vous avez envie de vous renseigner et de vous engager, rien de plus simple !**  
Contact : CIDFF de l’Ardèche 04.75.93.31.70 cidff07@cidff07.fr  
Secteur Nord Ardèche : Mme BERGOGNON Alisson ;  
Mail : alisson .bergognon@cidff07.fr / Téléphone : 06.03.11.33.67

### Dans l'actualité le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de novembre qui restent d'actualité.

#### Ouverture d'un centre de vaccination à Satillieu

23 novembre 2021  
A la Maison de Santé à partir du 3 décembre [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/ouverture-d-un-centre-de-vaccination-a-satillieu/ "Lire Ouverture d'un centre de vaccination à Satillieu")

#### Covid 19 : Ardèche, département le plus touché de France

22 novembre 2021  
Lettre du préfet, et courbe alarmante. Appliquez et maintenez les mesures barrières ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/covid-19-ardeche-le-departement-le-plus-touche-de-france/ "Lire Covid 19 : Ardèche, département le plus touché de France")

#### Les cavalières du Domaine Fontcouverte brillent dans toutes leurs épreuves

15 novembre 2021  
Nouvelle saison... nouveaux succès ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-cavalieres-du-domaine-fontcouverte-brillent-dans-toutes-leurs-epreuves/ "Lire Les cavalières du Domaine Fontcouverte brillent dans toutes leurs épreuves")

#### 2022 : à Lalouvesc, la fête continue

15 novembre 2021  
Notez et faites connaître les principales manifestations et événements de l'année à venir [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/2022-a-lalouvesc-la-fete-continue/ "Lire 2022 : à Lalouvesc, la fête continue")

#### Trail des Sapins : le Comité des fêtes salue l'engagement des bénévoles

11 novembre 2021  
Merci à tous les bénévoles bien présents à toutes les étapes de la course et de l'événement [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/trail-des-sapins-le-comite-des-fetes-salue-l-engagement-des-benevoles/ "Lire Trail des Sapins : le Comité des fêtes salue l'engagement des bénévoles")

#### Lalouvesc se réunit pour la cérémonie du 11 novembre

10 novembre 2021  
Office religieux, défilé, Monument aux morts, les Louvetous étaient présents [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/lalouvesc-se-reunit-pour-la-ceremonie-du-11-novembre/ "Lire Lalouvesc se réunit pour la cérémonie du 11 novembre")

#### Décorations : le village sera en fête pour la fin d'année

9 novembre 2021  
Quatre lieux, des galets, un effort de tous et un concours [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/decorations-le-village-sera-en-fete-pour-la-fin-d-annee/ "Lire Décorations : le village sera en fête pour la fin d'année")

## Feuilleton : Les quatre éléments

### Chapitre 3 : A la recherche du FEU !

Les enfants sont maintenant en possession de l'élément **TERRE**, déjà une pierre !

Il faut maintenant aller récupérer l’élément FEU à la Réunion au Piton de la Fournaise. Les enfants se demandent comment ils vont pouvoir se rendre dans ce lieu qui est à environ 10.000 km de la forêt Amazonienne où ils sont actuellement.

La fourmi, qui connaît bien le gardien du temple du **FEU**, leur propose d’envoyer un signal au phénix flamboyant pour qu’il vienne les chercher. Mais le phénix précise qu’il ne peut pas porter sur son dos les 2 enfants, il fait donc appel à son ami le dragon rouge de Chine.

{{<grid>}} {{<bloc>}}

![](/media/feuilleton-ch3-2021-12.jpg)

{{</bloc>}}{{<bloc>}}

> ![](/media/feuilleton-ch3-2021-8.jpg)

{{</bloc>}}{{</grid>}}

Le phénix et le dragon arrivent rapidement sur place dans la forêt amazonienne.

En route pour la Réunion maintenant ! Les deux amis volent au-dessus de l’océan Atlantique puis passent au-dessus du sud de l’Afrique, puis de Madagascar et arrivent enfin à la Réunion !

![](/media/feuilleton-ch3-2021-1.jpg)

Le phénix et le dragon déposent les enfants au pied du piton de la Fournaise et le phénix annonce :

{{<grid>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-3.jpg)

{{</bloc>}}{{</grid>}}

« Maintenant vous allez devoir monter au sommet du volcan pour pouvoir atteindre mon temple ! Mais pendant la montée, vous devrez répondre à des questions. Si vous répondez mal, je vous donnerai un gage : je rajouterai des cailloux dans votre sac à chaque mauvaise réponse pour vous ralentir»

{{<grid>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-6.jpg)

{{</bloc>}}{{<bloc>}}

Voici votre 1er défi :  
« Quel est le plus haut sommet de la Réunion : Le piton des Neiges ou le Piton de la Fournaise ? »  
Les enfants réfléchissent et se souviennent du cours de géographie de l’école et répondent en cœur :  
« Le piton des Neiges, il culmine à 3070 m alors que le piton de la Fournaise n’est qu’à 2632 m d’altitude »  
« Bravo les enfants, remarquable, vous pouvez avancer !! » dit le phénix.

{{</bloc>}}{{<bloc>}}

Maintenant voici votre 2ème défi :  
Combien d’heures d’avion faut-il pour aller à la Réunion (à partir de Paris)  
Les enfants répondent : « Il faut 5 heures »  
Le phénix répond : « Mauvaise réponse ! La bonne réponse était 11 h ! Je vous mets 1 kilo de cailloux dans votre sac, vous irez moins vite, c'est votre gage !

{{</bloc>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-11.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-10.jpg)

{{</bloc>}}{{</grid>}}

Dernier défi :  
Le département de la Réunion fait partie de la France, mais est-il deux fois plus petit ou deux fois plus grand que l’Ardèche ?  
Les enfants répondent : deux fois plus petit que l’Ardèche !  
Bravo !!! dit le phénix.

{{<grid>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/feuilleton-ch3-2021-4.jpg)

{{</bloc>}}{{</grid>}}

Après ces défis, les enfants arrivent enfin au temple.

Le phénix les félicite, leur ouvre le temple, leur offre une tasse de thé brûlant et leur remet la pierre qui abrite l’élément **FEU**.

![](/media/feuilleton-ch3-2021-7.jpg)

Le phénix leur souhaite bon courage pour trouver maintenant la pierre qui abrite l’élément **EAU**…

A suivre…

Et à votre tour de visiter, le bien nommé piton de la Fournaise qui était en éruption au printemps dernier... il abrite l'élément **Feu**  !

<iframe width="560" height="315" src="https://www.youtube.com/embed/znUMhiLNMkc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>