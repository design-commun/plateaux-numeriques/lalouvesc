+++
date = 2021-07-31T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°13 - Août 2021"
title = "13 - Sérieux et sourires"
weight = 1

+++
Pour avancer, il faut du sérieux. Il faut s'organiser dans le bon ordre et un peu de discipline pour, par exemple, circuler correctement dans le village, pour limiter la pandémie, pour chercher et trouver des financements. Mais il ne faut pas trop de sérieux, ne pas se prendre trop la tête, il faut aussi savoir sourire, s'émerveiller et s'émouvoir.

On trouvera ces deux volets dans ce bulletin avec un zoom sur les mesures sanitaires et un autre sur les poètes qui enchantent le Carrefour des Arts et bien d'autres informations qui mêlent chaque fois esprit de sérieux et rires louvetous.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce[ ](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/13-serieux-et-sourires-lalouvesc.pdf)[fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-13-1er-aout-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Bonjour les amis

Au cœur de la saison, nous avançons pour recevoir nos estivants dans les meilleures conditions possibles. Je remercie la population pour sa contribution au bon accueil touristique. Cette année est encore difficile. Entre les mises en application des contraintes sanitaires et le temps qui n'est pas complètement au rendez-vous, nous devons faire face aux conditions au jour le jour.

Notre travail en amont offre des résultats positifs. Le Camping répond pleinement à sa demande. L'embellissement, le fleurissement du village contribuent à la qualité de l'accueil. Le site Internet est très apprécié, il permet aux utilisateurs d'avoir une vision rapide des différents pôles d'actions et par conséquent de pouvoir orienter leurs choix concrètement.

Nous avons réorganisé dans une partie du village les sens de circulation et le stationnement. Le système avait vieilli et, pour faire face au surcroît d'automobiles, de piétons et des nouveaux outils de mobilité (vélos électriques, trottinettes), il était nécessaire de réviser plusieurs espaces. Nous sommes en train de peaufiner le dossier. La majorité des modifications sont en application, c'est l'affaire de tous de bien veiller à son respect. Les touristes sont notre fond de commerce. Il faut leur apporter le plus de confort possible, la première règle avant d'agir consiste à se mettre à la place de l'autre, nulle vraie recherche du bien commun ne sera possible hors de là.

Il nous reste le mois d'août et puis c'est la rentrée, le temps passe très vite. Je souhaite à toutes les personnes en vacances un très bon séjour. Les services de la mairie, de la poste sont à votre disposition, les amplitudes d'ouverture sont consultables sur le site Internet [lalouvesc.fr](http://lalouvesc.fr).

Merci à toutes et tous pour votre implication dans la vie du village, Continuons ainsi ensemble pour l'amélioration des conditions de vie dans un monde dont l'actualité n'est pas toujours réjouissante.

Vive les vacances, vive Lalouvesc !

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Le plan de circulation et de stationnement se met en place

Les premiers marquages au sol et les premiers panneaux ont été installés. Le plan vise à proposer un meilleur accueil, une circulation facilitée et mieux sécurisée pour tous dans le village. Chacun doit maintenant le respecter. Attention, la période de tolérance prendra bientôt fin.

#### Rue de la fontaine

Une réunion s'est tenue avec les riverains de la rue de la Fontaine à leur demande pour expliquer la logique des choix effectués. Le Maire a détaillé le plan de circulation et de stationnement. Pierre Roger a commenté au nom des riverains un diaporama faisant ressortir la situation particulière de la rue.

Un groupe de travail a été mis en place pour affiner les choix pris pour cette rue.

{{<grid>}} {{<bloc>}}

![](/media/circulation-2021-15.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/rue-de-la-fontaine-p-roger.png "Extrait d'un diaporama présenté par P. Roger")

Extrait du diaporama présenté par P. Roger

{{</bloc>}} {{</grid>}}

### Les lamas, c'est fini !

![](/media/lamas.jpg)

Suite à une prospection du maire, une éleveuse des Monts du lyonnais a été trouvée, intéressée par le troupeau de lamas errants, qui donnaient un air exotique au col de Lalouvesc, mais devenaient dangereux pour la circulation et gênants pour les plantations.

Le transport a été effectué par leur propriétaire. Il n'y a désormais plus de lamas sur le territoire de la Commune. Archivez les photos, elles témoigneront pour vos petits-enfants d'une diversité passée de la faune louvetonne !

### Le Jeu-monument, ça démarre !

Nous commençons à recevoir les premières suggestions, comme cet exemple, envoyé par Jean-Claude Régal, d'un[ jeu-chauve souris](https://www.mairie-saintremydeprovence.com/de-nouveaux-jeux-denfants-a-theme-au-square-joseph-mauron/) construit avec les habitants à Saint Rémy de Provence ou des[ idées suggérées](https://drive.google.com/file/d/1Lj5BZ3cfdGC6S_rDVdWepA_EmcMfWJ0q/view?usp=sharing) par Séverine Moulin (un labyrinthe, du mobilier, etc.), l'Agence du Développement Touristique de l'Ardèche nous a aussi fait parvenir un [diaporama](https://drive.google.com/file/d/12DcPFD-4xohGZXwhkp2Noa7ueShGpxQ_/view?usp=sharing) avec de nombreuses illustrations de jeux monumentaux.

N'hésitez pas à enrichir, vous-mêmes, la réflexion collective par des exemples ou des idées en remplissant la carte postale à votre disposition ou simplement en envoyant un mél à la mairie. Nous compilons toutes les idées sur un [document partagé](https://docs.google.com/document/d/1z96dGEIOf9s6DfGHUBKkVFcojypus5WdpgR52yDRQgk/edit?usp=sharing). Plus nous échangerons idées, témoignages et suggestions, plus riche sera notre réflexion et éclairées seront nos décisions.

![](/media/boite-jeu-monument.jpg)

Un groupe de travail, comprenant des élus et des habitants concernés, s'est déjà réuni plusieurs fois pour préparer une demande de subvention qui sera déposée fin août au programme européen Leader.

Calendrier prévisionnel :

* hiver 2021 : concertation, participation, intervention des habitants pour la conception,
* printemps - été 2022 : construction avec les professionnels du bois d'un premier volet du jeu-monument, qui servira d'amorce et de démonstration pour le projet définitif,
* automne - hiver 2022 : conception du projet définitif,
* 2023 : construction du jeu-monument complet.

Ce calendrier n'est qu'indicatif. Il dépend du succès que nous obtiendrons dans nos recherches de partenariats et de financements. De ce côté, des démarches actives sont en cours. Nous vous tiendrons au courant des résultats.

### La 4G, on suit

L'antenne qui couvre le village est opérationnelle, mais certains abonnés à Free ont été frustrés : leurs communications sont toujours déficientes. Cette situation n'est pas normale.

Le maire a contacté l'opérateur responsable de l'antenne, puis les services de la préfecture concernés. Il s'agit d'un mauvais paramétrage de Free. Ce dernier a été alerté par la préfecture pour régler ce désagrément. Tout devrait bientôt rentrer dans l'ordre. La mairie reste vigilante.

### Accessibilité du site lalouvesc.fr

En collaboration avec nos partenaires de Plateaux numériques, nous allons procéder à de nouvelles améliorations du site lalouvesc.fr dès cet automne.

Il est prévu notamment de rendre le site accessible aux personnes souffrant de certains handicaps selon la [norme RGAA](https://www.numerique.gouv.fr/publications/rgaa-accessibilite/obligations/). Par ailleurs, l'interface avec la base de données touristique ADIPAE sera finalisée et des outils de publication pour le bulletin pourront être développés.

Pour cela, nous sommes en discussion avec la Préfecture de l'Ardèche et Plateaux numériques est en relation avec l'Agence nationale de la cohésion des territoires. Le site lalouvesc.fr joue son rôle de démonstrateur comme "site pilote" pour les petites villes et c'est à ce titre qu'il sera développé et soutenu.

## Zoom

### A Lalouvesc, des mesures de prévention avec le sourire

Chacun le sait, la pandémie continue à sévir en France et tout autour de la planète. La vaccination est essentielle pour atténuer les effets d'une quatrième vague de contagion qui malheureusement nous guette une nouvelle fois. [85% des personnes actuellement hospitalisées en France n'étaient pas vaccinées](https://www.francebleu.fr/infos/sante-sciences/coronavirus-environ-85-des-personnes-hospitalisees-sont-non-vaccinees-selon-une-etude-1627649639). Ci-dessous, l'actualisation de la courbe des tendances des hospitalisations en France et en Ardèche. On observe que la courbe a cessé de baisser. Il y avait 16 personnes hospitalisées en Ardèche au 30 juillet.

![](/media/hospitalisation-covid-30-07-2021.png)

Pour ralentir les effets du redémarrage de la pandémie, le gouvernement a pris des mesures sévères qui doivent être appliquées avec sérieux.

#### Mesures louvetonnes

A Lalouvesc, comme ailleurs, il a fallu s'adapter. Nous l'avons fait avec rigueur et aussi avec le sourire.

Certains événements, comme les concerts classiques des Promenades musicales, ont du être annulés. Pour les autres, l'organisation que demandait la vérification d'un passe sanitaire a paru trop lourde pour un village comme le nôtre s'appuyant sur le bénévolat. Aussi il a été décidé au cinéma de l'Abri du pèlerin, comme au Carrefour des arts de limiter la fréquentation à un ensemble de quarante-neuf personnes. Pour cela le Carrefour a mis au point une astucieuse distribution de jetons.

Le camping, n'ayant pas de piscine, ni d'équipement concentrant les individus, n'est pas soumis à une vérification du passe sanitaire.

#### Le sérieux n’exclut pas un sourire

Quand on fait la queue pour entrer au Carrefour des arts, on ne s'ennuie pas !

![](/media/beatrix-poesie-carrefour-des-arts-2021-2.jpg "Les visiteurs du Carrefour patientent en écoutant le poète Michel Béatrix")

Et les 4 et 6 août à 19h au camping, nous rirons de nos travers de Français confinés en assistant au spectacle en plein air du comédien Philippe Bosseau.

![](/media/ph-bosseau-2021.jpg)

### Carrefour de la poésie, Carrefour des Arts

La poésie est à l'honneur cette année au Carrefour des Arts.

#### Michel Béatrix

_(...)  
J’ai semé des cailloux, de petits et des gros ;  
J’en ai semé des vrais, j’en ai semé des faux ;  
J’ai semé des cailloux de toutes les couleurs,  
J’en ai semé des noirs, j’en ai semé des blancs.  
Entre nous soit dit pour être tout à fait franc :  
Tous ces cailloux c’étaient des morceaux de mon cœur._

![](/media/beatrix-poesie-carrefour-des-arts-2021-5.jpg)

Dimanche 25 juillet, Michel Béatrix a réalisé une balade poétique dans les salles d’exposition du Carrefour des Arts. Vous l'avez raté ? Ne ratez pas la prochaine ! Le 13 août à 15 heures à la chapelle St Ignace Michel Béatrix et son complice Hervé Tharel au chant vous proposent une _déambulation poétique_.

#### Jorge Pimentel

{{<grid>}} {{<bloc>}}

![](/media/livre-d-artiste-jaquelet-carrefour-des-arts-2021-5.jpg)

{{</bloc>}} {{<bloc>}}

_Quitter une ville  
et le saule pleureur  
où soufflait  
le vent  
portant  
les âmes  
tourmentées_

Ce poème, et quatre autres mis en musique par Felipe Carrasco, a été chanté pour la première fois à la Basilique au cours des Promenades musicales 2020. le concert a été enregistré et vous pouvez [écouter de nouveau ici](https://youtu.be/2Xa5sBC15Ek) chacun des cinq poèmes, c'est un enchantement.

{{</bloc>}}{{</grid>}}

La poésie de Jorge Pimentel, poète péruvien, est toujours au rendez-vous du Carrefour des Arts en 2021 grâce à Martine Jaquemet qui a mis en peinture quelques-uns de ses poèmes Le 16 août, elle peindra en public, inspirée par les mêmes poèmes toujours mis en musique par Felipe Carrasco.

De plus le 17 août, un concert sera joué à 20h à la Basilique, parallèlement à l'exposition des quinze livres présentant quatorze poèmes du recueil de Jorge Pimentel, _Quatorze brefs dons d'amour et de déracinement_ et leur prologue.

Rencontres de la poésie, du chant, de la peinture, les Muses sont au rendez-vous... en 2021, le Carrefour des arts n'aura pas usurpé son nom !

### Pendant tout le mois d'août Lalouv'estivales continue

On ne s'ennuie pas l'été à Lalouvesc, il se passe toujours quelque chose ! Ci-dessous le programme des principales manifestations du mois.

{{<grid>}} {{<bloc>}}

* **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Action Communale tous les jours de 14h30 à 18h30
* **Exposition _La matière habitée_** à la chapelle Saint Ignace
* Les lundis : **Visite du jardin en permaculture** au Mont Besset
* Les lundis, mercredis, jeudis, samedis et dimanches à 21h : **Cinéma** à l'Abri du Pèlerin
* Les mercredis 15h - 17h et samedis 9h30 - 11h30 :  empruntez des livres à la **bibliothèque**
* Du 24 juillet au 6 août l'AFTL propose des stages de **tennis**

##### Concerts, cinéma et événements

* 2 août : cinéma, **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)
* 3 août : théâtre, **Confinement, quoi d’autres ?**, Camping
* 4 août : conférence, **Sainte Hildegarde de Bingen, une bénédictine allemande inspirée**, _Dominique ESTRAGNAT_ à la Maison St Régis
* 4 et 7 août : cinéma, **Le sens de la fête**, [bande annonce](https://youtu.be/-fycN9kNaXk)
* 5 et 8 août : cinéma, **Les Croods 2 : une nouvelle ère**, [bande annonce](https://youtu.be/h43qitmhLxQ)
* 6 août : conférence, **La médecine de Sainte Hildegarde de Bingen** _Dominique ESTRAGNAT_ à la Maison St Régis
* 6 août : théâtre, **Confinement, quoi d’autres ?**, Camping
* 8 août : **kermesse** au Sanctuaire
* 8 août : concert, **Eugène Electre** à la Grange de Polly, hommage à Geny Detto

  {{</bloc>}} {{<bloc>}}
* 9 août : cinéma, **Adieu les cons**, [bande annonce](https://youtu.be/9uotsv-vf6I)
* 11 août : concert, **Hommage à Gershwin**, jazz à la Grange de Polly
* 11 et 14 août : cinéma, **Présidents**, [bande annonce](https://youtu.be/XFNicafCq_8)
* 12 et 15 août : cinéma, **Ibrahim**, [bande annonce](https://youtu.be/ohh14axZm-c)
* 13 août : **Déambulation poétique**, Miche! Béatix et Hervé Tharel, à la chapelle St Ignace
* 13 août : concert, **Hommage à Armstrong**, jazz à la Grange de Polly
* 16 août : **Réalisation en public et en musique d’une œuvre** par la peintre Martine Jaquemet ( [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) )
* 16 août : cinéma, **Un triomphe** (avant-première) [bande annonce](https://youtu.be/N_q15dNbWtA)
* 17 août : concert à la Basilique **autour des peintures de Martine Jaquemet** ( [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) )
* 18 et 21 août : cinéma, **Les 2 Alfred**, [bande annonce](https://youtu.be/ZzGhk632cYg)
* 19 et 22 août : cinéma, **OSS 117 : alerte rouge en Afrique noire**, [bande annonce](https://youtu.be/I0aviu-FqNo)
* 23 août : cinéma, **Eiffel**, [bande annonce](https://youtu.be/0TX59C_hzYs)
* 25 et 28 août : cinéma, **C'est la vie**,[ bande annonce](https://youtu.be/udZq8XdXqd4)
* 26 et 29 août : **Kaamelott**, [bande annonce](https://youtu.be/j7RrsdP-WuM)
* 30 août : cinéma, **Bonne mère**, [bande annonce](https://youtu.be/fTIpmsWfADs)

{{</bloc>}}{{</grid>}}

## Suivi

### Invitation des sœurs du Cénacle

**Portes Ouvertes pour les habitants de Lalouvesc**

Les Sœurs du Cénacle invitent les habitants de Lalouvesc, à découvrir les trois salles qui retracent l’histoire de la Congrégation à Lalouvesc

**Samedi 4 Septembre 2021 de 15h à 18h30**

Ce parcours historique est destiné à la famille Cénacle (Sœurs, fraternité etc…) et ne sera pas public par la suite.

Louvetous, soyez les bienvenus !

### Isolation des vestiaires du stade

{{<grid>}}{{<bloc>}}

![](/media/vestiaire-foot-2021-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/vestiaire-foot-2021.jpg)

{{</bloc>}}{{</grid>}}

La municipalité a acheté au cours du mandat précédent les matériaux pour isoler les vestiaires du terrain de foot. Le club de foot s'était engagé à réaliser les travaux.

Cela a un peu tardé, mais c'est maintenant chose faite !

### Le temps des émerveilleurs

Ci-dessous un résumé du compte-rendu de la journée du 2 juillet, envoyé par une participante.

_Tout c'est merveilleusement déroulé : La météo, superbe ; l'organisation, impeccable ; les œuvres d'art des artistes louvetous, saisissantes de beauté et d'originalité ; et, cerise sur le gâteau, Groucho, notre clown au féminin (M.-Ch. Brunel) qui nous a joyeusement terminé la soirée !_

{{<grid>}}{{<bloc>}}

![](/media/emerveilles-2021-photo-ot-2.jpg)

Photos Office du tourisme

{{</bloc>}}{{<bloc>}}

![](/media/emerveilles-2021-photo-ot-1.jpg)

{{</bloc>}}{{</grid>}}

_En avant-première, nous avons visité le Carrefour des Art, sidérant de beauté et d'originalité et par petits groupes nous sommes partis visiter les coins insolites de Lalouvesc. Les hauts de la Basilique et le musée St.Régis ont eu bien du succès. Puis tout s'est prolongé en chansons et en musique guidés par notre Groucho et son nez rouge. Tout le monde a joyeusement participé à cette collective chorégraphie. Nous sommes arrivés à l'Abri du Pèlerin dont le Père Barthe-Dejean a tracé l'historique. Fr. Brunel, président du Théâtre de la Veillée, en flamboyant costume du Malade imaginaire a présenté la troupe qui fête ses 4O ans._

_L'ensemble s'est conclu au Camping-municipal avec la Lyre Louvetonne et le verre de l'amitié._

_Merci à "Emerveillés par l'Ardèche"! Et vive Lalouvesc !_

Edith Archer

### Dans l'actualité le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de juillet qui restent d'actualité.

#### Spectacles et animations de la semaine du 2 au 8 août

29 juillet 2021  
Tous les jours d'été, il se passe quelque chose à Lalouvesc [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/spectacles-et-animations-de-la-semaine-du-2-aout-au-8-aout/ "Lire Spectacles et animations de la semaine du 2 au 8 août")

#### Déplorable !

26 juillet 2021  
Un bac à fleur a été volé à l'entrée du village. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/deplorable/ "Lire Déplorable !")

#### Jeu-monument : l'imagination au pouvoir !

22 juillet 2021  
Une carte postale pour recueillir les idées... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/jeu-monument-l-imagination-au-pouvoir/ "Lire Jeu-monument : l'imagination au pouvoir !")

#### Recrutement d'animateurs

20 juillet 2021  
Familles rurales recrute six animateurs à partir du 1er septembre [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/recrutement-d-animateurs/ "Lire Recrutement d'animateurs")

#### Prochaines dates de Parentibulle

19 juillet 2021  
Accueil de parents avec leur enfant de moins de 6 ans en Ardèche Verte [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/prochaines-dates-de-parentibulle/ "Lire Prochaines dates de Parentibulle")

#### Camping : un prix exceptionnel de lancement pour la location de la tente Lodge

19 juillet 2021  
Profitez du confort canadien... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/camping-un-prix-exceptionnel-de-lancement-pour-la-location-de-la-tente-lodge/ "Lire Camping : un prix exceptionnel de lancement pour la location de la tente Lodge")

#### La connexion 4G est enfin là !

15 juillet 2021  
L'antenne couvrant le village est opérationnelle. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-connexion-4g-est-enfin-la/ "Lire La connexion 4G est enfin là !")

#### Mini-golf, gym et animation pour les enfants

14 juillet 2021  
Activités situées sur le camping, mais ouvertes à tous les estivants et les Louvetous [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/mini-golf-gym-et-animation-pour-les-enfants/ "Lire Mini-golf, gym et animation pour les enfants")

#### Je trie mes déchets, même en vacances !

12 juillet 2021  
Pas de vacances pour les poubelles... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/je-trie-mes-dechets-meme-en-vacances/ "Lire Je trie mes déchets, même en vacances !")

#### Les inscriptions pour le Trail des Sapins sont ouvertes

8 juillet 2021  
C'est le 30 octobre, des parcours sportifs pour tous les niveaux dans une ambiance Halloween [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-inscriptions-pour-le-trail-des-sapins-sont-ouvertes/ "Lire Les inscriptions pour le Trail des Sapins sont ouvertes")

#### La brocante ne prend plus d'inscription...

8 juillet 2021  
C'est complet... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-brocante-ne-prend-plus-d-inscription/ "Lire La brocante ne prend plus d'inscription...")

#### SLIME 2021 : Des visites à domicile gratuites pour réduire vos factures d'énergie et d'eau

5 juillet 2021  
Conseils personnalisés sur les consommations et la remise gratuite de petit matériel économe [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/slime-2021-des-visites-a-domicile-gratuites-pour-reduire-vos-factures-d-energie-et-d-eau/ "Lire SLIME 2021 : Des visites à domicile gratuites pour réduire vos factures d'énergie et d'eau")