+++
date = 2021-09-29T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°15 Octobre 2021"
title = "15 - Une rentrée au pas de course"
weight = 1

+++
Dans le bulletin du mois d’octobre : une rentrée au pas de course avec le Trail des Sapins, une épreuve sportive et un défi organisationnel pour les bénévoles du Comité des fêtes ; une rentrée sur les chapeaux de roue avec trois rallyes automobiles à Lalouvesc ce mois ; une rentrée pleine de surprises aussi avec deux cadeaux en fin de bulletin : le premier épisode d’un nouveau feuilleton signé par de très jeunes auteurs louvetous et l’album des saisons photographiées par le Père Olivier… et, bien sûr, comme toujours plein d’autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce[ ](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/13-serieux-et-sourires-lalouvesc.pdf)[fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-15-1er-octobre-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Chers amis,

Les feuilles tournoient dans le ciel. C’est un petit arbre tout rouge, un d’une autre couleur et puis, partout, les feuilles qui tombent sans que rien ne bouge.

Eh oui ! Nous y sommes. Nos activités ont repris, les vacances sont passées, nous allons vers la fin de l'année.

Le bilan de la saison n'a pas encore été totalement bouclé, mais il semble moins affligeant que nous l'avions craint fin juillet à cause de la météo et de la pandémie conjuguées. Août et septembre se sont avérés bien meilleurs.

Les travaux sont repartis : la rampe d'accès pour les personnes à mobilité réduite pour atteindre la bibliothèque vient d'être terminée, les travaux de remise en état des murs de soutènement du cimetière se poursuivent, c'est un travail long et délicat, il doit s'inscrire dans le temps. Après la fermeture de tous nos commerces saisonniers nous espérons pouvoir démarrer les travaux de démolition de l'hôtel Beauséjour.

C'est le moment de tailler nos arbres et nos haies, nous nous y attelons à la commune, je vous engage d'en faire de même dans vos propriétés sur les côtés voie publique.

La rentrée scolaire s'est bien passée nous avons 20 élèves à l'école St Joseph. Nous avons confirmé notre volonté de soutien, moral et financier, à l'établissement. Un voyage scolaire est prévu, cela changera l'horizon de nos enfants, nous en sommes ravis.

Les ventes des terrains du lotissement du Bois de Versailles ont bien démarré, nous en sommes à trois compromis de vente signés et nous avons encore trois personnes potentiellement intéressées.

Bientôt le Trail des Sapins, organisé par le comité des fêtes, qui marque la fin de saison. Quoique la saison des champignons semble prometteuse, la météo est favorable, nous pourrions avoir encore un peu de monde pour les mois d'octobre et de novembre, au moins jusqu'à la foire à Saint Bonnet le froid.

Je vous souhaite à tous une bonne arrière saison, période propice pour préparer l'hiver. Recevez toutes et tous l'expression de mes sentiments les plus dévoués.

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Fleurissement

Au mois du juin le souhait d’un village propre, coloré et fleuri avait été émis. Une commission fleurissement a été créée. Après le souhait, le constat : une réussite ! Ce n’est pas des fées qui se sont penchées sur les jardinières mais de véritables mains vertes, peut-être tout de même des mains de… fées !

Aucun point du village n’a été oublié. De nouvelles jardinières sont venues renforcer le parc existant. Les plants ont été choisis avec goût et harmonie. Ils ont été choyés tout au long de l’été pour un résultat des plus remarquables. Il n’a pas été rare d’entendre : _que les fleurs sont belles cette année !_ Dame nature a même été favorable en remplaçant (peut-être même trop souvent) les arrosoirs…

Un inventaire a été fait des plants qui pourraient être hivernés et replantés l’année prochaine. La commission réfléchit à cette possibilité. Un autre axe de réflexion serait de s’orienter vers un élargissement du fleurissement par des plantes vivaces.

Un grand merci à tous ceux et celles qui ont contribué à cet embellissement.

Le Comité fleurissement

{{<grid>}}{{<bloc>}}

![](/media/fleurissement-sept-2021-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-sept-2021-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-sept-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-sept-2021-5.jpg)

{{</bloc>}}{{</grid>}}

### Élagage et haies

C'est la saison de l'élagage, la Mairie a démarré ces travaux au camping et sur la place du Lac. Mais elle a observé qu'en plusieurs endroit la végétation des particuliers débordait sur la voie publique et que les réglementations en termes de plantation n'étaient pas toujours respectées.

#### Rappel des obligations des propriétaires

L’élagage des branches qui dépassent sur le domaine public est à la charge des riverains.

Faute de ne pas respecter ces obligations les propriétaires peuvent être mis en demeure par la Mairie. Ils s'exposent à une amende et à payer les travaux commandés en leur place par la Mairie.

Par ailleurs, en matière de plantation de végétaux, le Code civil (articles 670 à 673) impose des règles de distance par rapport au terrain voisin :

* 0,50 mètre de distance **si l’arbre mesure moins de 2 mètres de haut**.
* 2 mètres de distance **si l’arbre mesure plus de 2 mètres de haut**.

Cette obligation ne s'applique pas si l'arbre a plus de 30 ans.

### Goudronnage

Les travaux de goudronnage se sont poursuivis en septembre sur les routes communales au mois de septembre.

{{<grid>}}{{<bloc>}}

![](/media/goudronnage-sept-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/goudronnage-sept-3.jpg)

{{</bloc>}}{{</grid>}}

![](/media/goudronnage-sept-2.jpg)

### Beauséjour

![Ruine Hotel Beauséjour](/media/beausejour-photo-o-de-framond.jpg "Photo O. de Framond")

dernières étapes avant la démolition :

* les travaux de transfert de la ligne électrique par un sous-traitant Enedis démarreront le 4 octobre. La ligne qui courrait le long du bâtiment doit maintenant traverser la rue, dans un sens puis, plus loin, dans l'autre pour retrouver son chemin initial.
* une opération est prévue pour capturer et faire stériliser les chats qui ont trouvé domicile dans le bâtiment. Celle-ci est subventionnée par la fondation Brigitte Bardot et l'association _La clé des chats_.

Il faudra ensuite que l'entreprise concernée ajuste son planning pour démarrer le chantier.

### Vers un club pour les jeunes

Le village n'a pas d'équipements pour satisfaire les besoins des 12-18 ans. Le désœuvrement peut être source de comportements dommageables. Pour trouver les bonnes initiatives à prendre, une réunion s'est tenue à la Mairie avec quelques jeunes.

Une de leurs suggestions pourrait déboucher rapidement, même si elle mérite d'être précisée et réglementée : un club pour les jeunes, disposant d'un local avec babyfoot et ping-pong. Il pourrait s'ouvrir dans l'ancienne école publique qui a des salles disponibles et même un préau. Le babyfoot et la table de ping-pong ont été trouvés.

Il faut évidemment préciser les règles et les responsabilités dans le club, en accord avec les parents. Le club doit être sous la responsabilité d'une personne majeure et sera régulièrement contrôlé par la Mairie. Aucun écart ne pourra être toléré.

Restera ensuite aux jeunes à aménager les lieux à leur convenance. C'est une opportunité forte pour les jeunes qui en automne et en hiver n'ont pas de lieu pour se retrouver sur le village.

Une prochaine réunion doit être organisée à l'école publique pour avancer avec les intéressés dans la bonne direction.

### Synthèse des cartes postales sur le jeu-monument

35 cartes postales ont été reçues,

* dont 12 **habitants** toute l’année, 19 **visiteurs** pendant l’été (et souvent pendant les vacances), 4 **résidents** six mois par an ou “très souvent”;
* dont 26 femmes, 6 hommes et 3 collectifs ;
* dont 8 enfants (estimés).

![](/media/carte-postale-enfant-jeu-monument-2021.jpg)

Une présentation détaillée est [accessible ici](/media/jeu-monument-2021-synthese-des-cartes-postales-recues.pdf).

Il y a une différence sensible, mais assez naturelle et explicable, entre les réponses des habitants qui privilégient le calme et celles des visiteurs qui font de très nombreuses suggestions de jeux. Ils n’ont ni le même profil sociologique, ni la même utilisation de l’espace.

Même si le projet de jeu-monument vise à augmenter l’attractivité du village et privilégie donc la demande des visiteurs, on ne doit pas pour autant négliger la demande de jardin des habitants.

Les suggestions de jeux sont très variées. La demande d’une piscine ressort, mais elle semble hors de portée pour notre Commune. L’ensemble montre une différence de propositions de jeux selon les âges avec une demande particulière pour les ados.

Il faudra pouvoir répondre à cette demande variée et parfois contradictoire. La variété des réponses montre aussi qu’il n’y a pas encore un concept fort qui émerge du projet et que celui-ci est à construire. Nous ne sommes qu’au tout début de la démarche.

## Zoom

### Trail des Sapins : une petite entreprise

{{<grid>}}{{<bloc>}}

![](/media/trail-des-sapins-affiche_2021.jpg)

{{</bloc>}}{{<bloc>}}

Le Trail des Sapins, qui fêtera sa cinquième édition le 30 octobre, se développe d'année en année pour faire partie des temps forts de Lalouvesc. Il y avait 800 participants à la course en 2019, atteindra-t-on le millier en 2021 après l'interruption de l'année dernière pour cause de pandémie ?

Derrière cette belle journée de fête et de sport se cachent beaucoup de paramètres à anticiper et à organiser afin de limiter au maximum les imprévus. Bien entendu il en reste toujours. Les imprévus font partie du jeu, mais il faut les contenir dans des proportions raisonnables. De longs mois de travail sont alors nécessaires, une année en réalité et, sitôt la fête terminée, nous avons déjà la tête dans l'année suivante.  
De nombreuses et précieuses compétences sont mises à contribution pour arriver au succès escompté. Une commission Trail a été créée pour soutenir et orchestrer cette préparation. L’implication de chacun des membres est indispensable et efficace.

{{</bloc>}} {{</grid>}}

#### Une direction solide

Il faut un pilote à bord pour gérer tout ce joyeux bazar organisé. Rappelons que le Trail des Sapins est organisé par le Comité des Fêtes, qui propose divers évènements dont l'envergure n'est plus à prouver. Cela demande énormément d'énergie qui doit être canalisée et déployée au bon endroit et au bon moment. Nathalie, notre Présidente y œuvre au quotidien. Sa bonne humeur et son sourire permanents font que chaque demande de sa part est accueillie avec enthousiasme.

#### Tracer des itinéraires

{{<grid>}}{{<bloc>}}

L’un des plus gros morceaux de l'organisation est le tracé des [parcours](https://www.traildessapins-lalouvesc.fr/infos-pratiques/parcours/). Nous ne nous facilitons pas la tâche en proposant chaque année des parcours inédits, mais cela est apprécié et contribue à la renommée de l’épreuve.

Il faut d'abord identifier un secteur de jeu à explorer. Sur les Communes identifiées, nous essayons de trouver des lieux d'intérêt, notamment pour l'accueil des ravitaillements. Cette année, par exemple, nous avons jeté notre dévolu sur le site de Notre Dame d'Ay à St Romain d'Ay et sur la belle et méconnue maison forte de Milagro à Lalouvesc.

Le défi est d'arriver à relier tous les sites dans des distances équivalentes d'une année sur l'autre (7, 12, 21 et 46 km). Le traileur haïssant au plus haut point le bitume et tout chemin carrossable, au grand dam de nos baliseurs, il faut lui trouver l'itinéraire le plus ludique, tout en y ajoutant une touche de diversité pour ne pas laisser s'installer la monotonie.

{{</bloc>}}{{<bloc>}}

![](/media/trail-des-sapins-parcours-2021.jpg)

{{</bloc>}}{{</grid>}}

Enfin, il y a des règles à respecter, sinon ce ne serait pas drôle. Nous privilégions les chemins balisés par la Communauté de Communes ainsi que les chemins ruraux. D'ailleurs, depuis la création de l'épreuve, nous menons une véritable action dans la restauration du patrimoine commun puisque de nombreux vieux chemins oubliés ont revu le jour au prix de longues heures de sueur.

Parfois, le passage en terrain privé semble la meilleure option. Il faut donc rechercher à qui appartient la parcelle, prendre contact, négocier une autorisation au travers d'une convention de passage. Nous tenons à remercier très sincèrement les nombreux propriétaires, qui depuis le début et dans la grande majorité des cas, nous permettent d’emprunter l'espace d'une journée leur(s) parcelle(s) pour le plus grand bonheur des participants. Lorsque cela coince, et cela arrive, il faut discuter, faire appel aux relais locaux, et trouver le meilleur compromis afin de pouvoir boucler la boucle.

Une fois tout cela résolu, il reste la Sécurité à organiser pour pouvoir intervenir le plus rapidement en cas de souci. Toute l'information doit parvenir aux centres de secours concernés. Chaque parcours est découpé en tronçons, avec des repères que le participant pourra transmettre pour le localiser facilement en cas de problème, et avec surtout un accès pour se rendre sur place. Une partie de la course se déroule de nuit, tout est par conséquent plus compliqué, et pourtant, en cas d’incident, l’urgence est toujours au rendez-vous. Pour Damien, notre leader du Trail des Sapins, toute cette partie peut quelques fois tourner au cauchemar !

#### Décorer

{{<grid>}}{{<bloc>}}

![](/media/trail-des-sapins-deco.jpg)

{{</bloc>}}{{<bloc>}}

Pourquoi faire au plus simple quand on peut se donner un peu de travail supplémentaire ? Le Trail des Sapins se déroule toutes les années le week-end d’Hallowen.

Courges décorées, fantômes et autres araignées sont mis en scène pour apporter un côté ludique voire effrayant à l’évènement. Des petites mains cousent, décorent, peignent pour orchestrer ce tableau. Là, ce sont de véritables artistes qui entrent en jeu pour nous offrir toujours plus d'émerveillement.

{{</bloc>}}{{</grid>}}

#### Financer et administrer

La bourse demeure et reste le nerf de la guerre. L'objectif n'est pas de s'enrichir, mais il ne faut pas aller à la banqueroute non plus. Le Comité des Fêtes a besoin de rentrées d'argent pour investir dans des équipements facilitant l'organisation des nombreuses idées de ses adhérents. Notre trésorière, Martine, veille au grain avec rigueur et responsabilité. Chaque année, le budget est maîtrisé et le Trail des Sapins fait partie des évènements rentables de l'année.

Pour pouvoir garder un niveau de tarifs attractifs, nous ne pouvons rien faire sans l'aide de nos [partenaires](https://www.traildessapins-lalouvesc.fr/partenaires/). Tout d'abord institutionnels avec la Commune, en soutien financier et logistique, le Département et la Région. Un grand merci à nos élus, véritables acteurs de terrain et au soutien indéfectible. Et puis il y a les entreprises privées. Tout ce riche tissu économique toujours prêt à répondre à l'attractivité de leur territoire. Pierre après pierre, ils nous permettent de constituer un budget solide permettant d'offrir le meilleur aux participants et de contrôler tout imprévu. Bravo à Agnès, notre secrétaire, qui, avec son bâton de pèlerin magique, arrive à rallier un maximum d'acteurs à notre beau projet.

C’est elle qui assure aussi une grande partie du secrétariat. Elle gère les mails, la réception des appels téléphoniques, le classement des dossiers, le lien avec les instances, les comptes rendus de réunions... Un travail de fourmi qui demande beaucoup de rigueur pour arriver à s'y retrouver. Le tout avec toujours beaucoup d'enthousiasme.

#### Piloter la logistique

Il y a tellement de détails techniques tout au long du week-end et en amont qu'il nous faut de véritables spécialistes pour répondre à toutes nos demandes, qui peuvent être farfelues par moment, il faut l'avouer. Dominique est notre _géotrouvetout_. Il pilote toutes les demandes, en plus de la gestion des ravitaillements. Chaque détail est peaufiné pour arriver au meilleur des résultats.

Nous pouvons aussi compter sur toute l'équipe terrain. Une équipe masculine présente avec force et courage ne rechignant jamais à la tâche.

![](/media/trail-des-sapins-traces-itineraires.jpg)

Il faut, certes, saluer la performance de tous ces sportifs se surpassant sur nos parcours exigeants. Mais le travail de ces hommes de l'ombre est aussi un véritable marathon où la course du soleil ne dicte plus le rythme des journées. C'est près de 72h non-stop sur le pont entre le balisage, l'installation du site départ/arrivée jusqu'au démontage avec l'enlèvement du moindre bout de plastique dans les bois.

#### Communiquer

Il n'y a pas de réussite sans communication. Le travail s'opère là aussi sur toute l'année, avec des rappels réguliers sur les [réseaux sociaux](https://www.facebook.com/traildessapins), le[ site Internet](https://www.traildessapins-lalouvesc.fr/), les temps forts de communication. Un vrai travail de _community manager_ où chaque temps de communication est prévu au jour et à l'heure près.

<iframe width="560" height="315" src="https://www.youtube.com/embed/QWu2jLWtxKg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Quant à l'image, nous ne pouvons qu'admirer la qualité artistique de la prise d'images et de montage de notre grand chef visuel, Matthieu. Chaque année, il rivalise d'ingéniosité pour nous proposer toujours plus d'émerveillement, digne des plus grandes courses. Appareils photos et caméra dernier cri, drone pour des vidéos embarquées vues du ciel, choix des spots les plus photogéniques, application de la réglementation. Un vrai travail d'orfèvre.

#### Accueillir

L'Accueil avec un grand A. C'est peut être bien la compétence dont nous pouvons avoir la plus grande fierté. L’accueil, un acte banal, mais qui a pourtant tellement d'importance.

![](/media/trail-des-sapins-accueil.jpg)

Il est si spontané, si enjoué, si généreux à Lalouvesc, qu'il est difficile de définir qui entre le participant et le bénévole est le plus heureux d'être présent ce jour-là. La communion et la bienveillance sont à leur paroxysme durant ces quelques heures où pourtant la météo nous joue souvent de mauvais tours. Le froid, parfois la pluie voire même la neige, n'ont jamais entamé la bonne humeur et la qualité d'Accueil.

Cela passe par l'Accueil aux inscriptions, où chaque demande est traitée avec le meilleur niveau de réponse, l'Accueil réconfortant sur les ravitaillements, l'Accueil éphémère du signaleur au bord de la route, l'Accueil de nos bonnes fées servant la soupe chaude tellement appréciée à l'arrivée.

Si une entreprise cherche du coaching en Accueil, c'est à Lalouvesc qu'elle doit venir !

#### Respecter l’environnement

Le terrain de jeu de cet évènement est vaste et se situe au cœur d’une magnifique nature. Nous menons de nombreuses actions pour la respecter. Ce point est largement abordé dans la préparation et l’organisation du Trail des Sapins.

Pour le balisage, nous utilisons des bombes à base de craie ; les circuits sont débalisés avec rigueur aussitôt la course terminée ; nous privilégions le circuit court pour l’approvisionnement des ravitaillements et du repas d’après course ; nous effectuons le tri des déchets ; etc.

Chaque année, nous essayons de progresser dans ce vaste chantier.

#### … ensemble, on va plus loin

Sans toutes ces femmes et ces hommes qui donnent un nombre d'heures incalculable, le Trail des Sapins n’existerait pas. Si nous devions payer toutes ces compétences offertes, tout ce temps apporté, le tarif des inscriptions serait inabordable.

![](/media/trail-lalouvesc-1.jpg)

Nous ne pouvons malheureusement pas citer ces quelques 100 personnes, mais nous sommes bien conscients de la valeur que représente leur aide. Nous sommes heureux et fiers d’avoir embarqué, au fil des années, des bénévoles des Communes traversées. La création de ces liens montre que l’ambiance autour de cet évènement est des plus conviviales.

C'est épuisés, mais avec beaucoup d'émotion et de fierté, que nous refermons le week-end où un ouragan s'abat dans notre quotidien durant ces quelques journées. Finalement, tout ceci se résume en un simple proverbe africain : _Tout seul, on va plus vite, ensemble, on va plus loin_.

💪🏻 _Damien à la structure de l’article  
_👍🏻 _Nathalie au rajout de quelques points  
_✍🏻 _Agnès à la frappe et correction_

### Trois rallyes automobiles

A la mi-octobre, Lalouvesc accueillera d'abord des voitures, celles d'hier et celles de demain, depuis le luxe, jusqu'aux électriques, en passant par les voitures de collection.

#### 3e Croisière Rallye Porsche

On commence par un rallye réservé aux Porsches avec le 14 octobre la 3e spéciale  de la [Croisière Rallye Porsche](https://www.croisieurope.com/croisiere/3e-porsche-rallye-croisiere-romain-dumas-denis-giraudet-porsche-partenariat-chopard-classique#way) qui partira de Lalouvesc en début d'après-midi jusqu'à Lamastre.

#### Team des balcons

Puis, le 17 octobre, place aux voitures anciennes.

Nous n'avons pu avoir la [Ronde Louvetonne](/decouvrir-bouger/lalouv-estivals/la-ronde-louvetonne-des-vieilles-voiture-avril/) en avril, mais nous pourrons admirer les vieilles voitures de collection en octobre. Les Routes d'Exbrayat organisent leur [Team des Balcons](http://teamdesbalcons.com/index.php/2021/07/07/2eme-randonnee-automnale/) le 17 octobre et celui-ci s'arrête pour déjeuner à Lalouvesc.

![](/media/ronde-louvetonne-2.jpg)

On pourra donc y admirer les voitures à partir de midi. La Mairie en profitera pour vendre des brioches au profit de l'ADAPEI ((Association départementale de parents et d'amis des personnes handicapées mentales).

#### E-rallye Monte-Carlo

Enfin, le 20 octobre nous accueillerons encore une troisième Spéciale, mais moins bruyante et polluante que la précédente, celle du [E-rallye Monte Carlo](https://acm.mc/edition/e-rallye-monte-carlo-2021/), réservé aux voitures électriques ou à hydrogène.

Il s'agit d'une étape de régularité, départ de Lalouvesc, arrivée à Labatie-d’Andaure.

## Suivi

### Quelques nouvelles des Salers du Crouzet

La saison s’est bien terminée avec une grange bien remplie. Foin, blé, paille, tout est là pour passer un hiver serein. Ca faisait longtemps !

![](/media/salers-du-crouzet-sept-2021-1.jpg)

Vous avez peut-être vu des marshmallows géants dans nos prés, c’est une nouveauté cette année. Dommage, ce ne sont pas des bonbons mais bien de l’enrubannage ! Normalement, ils sont de couleurs verte ou noire. Nous avons décidé de payer plus chers nos films plastiques de couleur rose afin de reverser une partie du prix pour la lutte contre le cancer du sein.

Les vêlages vont bientôt commencer. Deux veaux sont nés pour l’instant, nous en attendons encore une trentaine. Cet été, nous avons pu fournir trois veaux à la boucherie De Jésus. J’espère que vous en avez profité...

La fin de l’année, ainsi que l’année prochaine, seront chamboulées. Mon installation « officielle » se rapproche. Après quelques formations, j’ai plein d’idées en tête.

![](/media/salers-du-crouzet-sept-2021-3.jpg)

J’ai hâte de faire découvrir mes nouveaux produits aux Louvetous !

Aline Delhomme

### Les rendez-vous "randonnée du mardi" reprennent

Le rendez-vous pour la randonnée du mardi à 14H00 devant le Vival est de retour. Enfilez vos chaussures de marche, prenez vos bâtons en main, apportez votre bonne humeur et venez découvrir notre fabuleux village avec ses paysages magnifiques. D’un côté, vous avez une vue sur le Vercors et les Alpes et, de l’autre, vous avez le Gerbier des Joncs et le Mézenc.

![](/media/balade-du-mardi-2021-1.jpg)  
Ce mardi 28 septembre, nous sommes allés aux Allées de Versailles à la sortie du village direction St Bonnet le froid.  
Nous avons traversé la forêt de mélèzes. De nombreux champignons poussent sur les tapis de mousse. C’est la saison des mûres et notre gourmandise nous a poussés à la cueillette.  
La balade nous a menés près des cascades de la scie à proximité du parcours des Afars. Des sentiers sont encombrés par la végétation mais rien ne nous arrête.  
Chacun marche à son rythme et la convivialité règne dans le groupe. Enfin nous sommes arrivés à La Croix des sept Fayards ou une pause bien méritée s’est imposée.

![](/media/balade-du-mardi-2021-2.jpg)  
Cela vous tente ? Toutes les personnes désirant marcher sont les bienvenues.  
Rendez vous mardi prochain.

Nathalie Fourezon

### Deux pèlerins belges sur le chemin Ste Thérèse Couderc

A la veille de la fête de notre sainte ardéchoise, le 29 septembre, deux jeunes pères de familles se sont lancés sur ce nouveau chemin de randonnée. Arrivés la veille tout droit de Bruxelles, partis en TGV à 10h du matin, ils descendaient à 18h50 du car qui arrive d Annonay. Comme quoi on ne peut pas dire que notre Ville Sanctuaire est à mille miles de tout !

{{<grid>}}{{<bloc>}}

![](/media/pelerins-chemin-ste-therese.jpeg)

{{</bloc>}}{{<bloc>}}

Edith la créatrice de cet itinéraire pédestre les a accueillis et conduits à l'Abri du Pèlerin, avant un fraternel dîner, vue sur les Alpes, où ils ont été émerveillés par la qualité du silence environnant ! Le lendemain, après une rapide visite du musée St Régis et un temps de recueillement à la basilique, photo de départ sous le porche par Séverine de l'O.T. suivie par un dynamique brin de conduite jusque au Col du Faux.

Et de là : _Ultrèia !_ (cri d encouragement des pèlerins qui veut dire : _En avant, toujours plus loin !_). Dans huit jours, ils seront à l'Abbaye de N.D. des Neiges au sud-ouest du département d'où ils reprendront un train qui les ramènera chez eux le soir mème.

{{</bloc>}}{{</grid>}}

Munis du livret-guide _Le chemin sainte Thérèse Couderc raconté aux futurs Pèlerins_, de cartes I.G.N. et d'une boussole, la montagne ardéchoise n'aura plus de secrets pour eux. Mais comment les avez-vous connus me demande-t-on ? Par les méandres de l'Internet semble-t-il et la Providence a fait le reste. Merci à sainte Thérèse Couderc et "Bon Chemin" à eux !

Edith Archer

### Pneus d'hiver obligatoires à partir du 1er novembre

Même si la préfecture n'a pas encore publié la liste des communes concernées, il est très vraisemblable que les pneus d'hiver seront obligatoires à Lalouvesc à partir du 1er novembre prochain en application de la loi Montagne. Prenez vos dispositions.

![](/media/pneu-neige-loi-montagne.jpg)

### Les bons réflexes du tri

La CCVA précise les déchets qui doivent être triés

* Les emballages plastiques : 100 % des emballages plastiques  
  Exemples : bouteilles et flacons en plastique, films, sacs et sachets, pots, boîtes, barquettes, tubes
* Les emballages métalliques : 100 % des emballages métalliques  
  Exemples : boîtes de conserve, barquettes en métal, aérosols non-dangereux, bidons de sirop, canettes, gourdes de compote, tubes, capsules de café, feuilles d’aluminium froissées, opercules, couvercles et capsules, plaquettes de médicaments vidées

Les recommandations habituelles restent inchangées :  
\- JETER les emballages en VRAC dans la colonne de tri jaune  
\- NE PAS LAVER les emballages, simplement bien les vider  
\- NE PAS IMBRIQUER les emballages les uns dans les autres

* Les papiers/cartons : pas de changement !  
  \- Les papiers et cartonnettes dans la colonne de tri bleue  
  \- Rappel : les gros cartons bruns sont toujours à déposer en déchetterie
* Le verre : pas de changement ! Le verre dans la colonne de tri verte

{{<grid>}}{{<bloc>}}

![](/media/tri-dechet-1-2021.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/tri-dechet-2-2021.jpg)

{{</bloc>}}{{</grid>}}

### Dans l'actualité le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois de septembre qui restent d'actualité.

#### Fermeture exceptionnelle de la déchetterie lundi 4 octobre

29 septembre 2021

#### Le camping reste ouvert jusqu'au 3 novembre

28 septembre 2021  
Avec un tarif à la journée défiant toute concurrence pour les hébergements disponibles. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-camping-reste-ouvert/ "Lire Le camping reste ouvert jusqu'au 3 novembre")

#### Y'a plus de saison !

19 septembre 2021  
Évolution de la pluviométrie à Lalouvesc sur une année [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/y-a-plus-de-saison/ "Lire Y'a plus de saison !")

#### La Mairie recrute

17 septembre 2021  
Un agent technique polyvalent, CDD 6 mois [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-mairie-recrute/ "Lire La Mairie recrute")

#### Fête de la science du 25 septembre au 9 octobre

14 septembre 2021  
Tout le programme [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/fete-de-la-science-du-25-septembre-au-9-octobre/ "Lire Fête de la science du 25 septembre au 9 octobre")

#### Redémarrage des chantiers

13 septembre 2021  
La saison estivale se termine, les réparations du village reprennent : routes, Beauséjour, accès handicapés... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/redemarrage-des-chantiers/ "Lire Redémarrage des chantiers")

#### Bref retour sur la bonne saison du Cinéma Le Foyer

10 septembre 2021  
Une programmation remarquable, plus de 1.300 spectateurs encore cette année malgré la Covid-19 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/bref-retour-sur-la-bonne-saison-du-cinema-le-foyer/ "Lire Bref retour sur la bonne saison du Cinéma Le Foyer")

#### La vaccination en France, en Ardèche et à Lalouvesc

7 septembre 2021  
Quelques chiffres... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-vaccination-en-france-en-ardeche-et-a-lalouvesc/ "Lire La vaccination en France, en Ardèche et à Lalouvesc")

#### Bref retour sur la belle brocante 2021

7 septembre 2021  
Succès, convivialité et météo favorable pour une belle brocante dans un village gaulois [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/retour-sur-la-belle-brocante-2021/ "Lire Bref retour sur la belle brocante 2021")

## Deux cadeaux

Pour conclure ce bulletin et démarrer l’automne dans la beauté, voici les deux cadeaux promis.

### _Les Quatre éléments_

(premier épisode du nouveau feuilleton louvetou)

#### **_Chapitre 1_**

> ![](/media/les-quatre-elements-1-ecole-st-joseph.jpg)
>
> Il était une fois deux enfants qui galopaient sur des poneys. Lors de leur balade, ils rencontrent un cheval blessé qui se trouve à l'orée d'une forêt.
>
> Les enfants voudraient bien soigner le cheval... mais comment faire ?
>
> {{<grid>}}{{<bloc>}}
>
> Ils aperçoivent soudain à côté du cheval une croix rouge. Ils décident alors de creuser à cet endroit et ils tombent rapidement sur un parchemin et une carte. Sur le parchemin est écrit qu'il faut rassembler les quatre éléments - la Terre, le Feu, l'Eau et le Vent - pour pouvoir soigner le cheval. Sur la carte, on peut voir que les quatre éléments se trouvent dans des pierres cachées aux quatre coins du monde dans des temples.
>
> {{</bloc>}}{{<bloc>}}
>
> ![](/media/les-quatre-elements-2-ecole-st-joseph.jpg)
>
> {{</bloc>}}{{</grid>}}
>
> Chaque élément est protégé dans un temple par un animal.
>
> * Le gardien de la Terre est la fourmi des galeries. Elle garde le temple dans forêt Amazonienne.
> * Le gardien du Feu est le phénix flamboyant. Il garde le temple au pied du piton de la Fournaise.
> * Le gardien de l'Eau est le dauphin des tourbillons. Il garde le temple dans l'océan Pacifique.
> * Et le gardien du Vent est l'aigle royal. Il garde le temple au sommet du mont Everest.
>
> Les enfants vont donc partir à la recherche de ces quatre éléments afin de pouvoir sauver le cheval. Mais comment protéger le cheval blessé pendant leur quête ?
>
> A ce moment l'ours de la forêt arrive et leur propose de veiller sur le cheval pendant qu'ils seront à la recherche des pierres.
>
>                           ... à suivre

Histoire écrite collectivement par les élèves des classes CE1//CE2/CM1 de l'école St Joseph spécialement pour le Bulletin d'information de Lalouvesc.

Vous pouvez suivre toute l'actualité de l'école St Joseph sur [sa page Facebook](https://www.facebook.com/Ecole-St-Joseph-Lalouvesc-107859627383105/).

### Les saisons à Lalouvesc sur l'album du Père Olivier

Cela fait maintenant un an que le Père Michel Barthe Dejean et le Père Olivier de Framond ont rejoint Lalouvesc pour prendre la responsabilité du Sanctuaire. Ils ont pu prendre la mesure de l'ampleur de leur tâche et aussi découvrir le village et ses alentours. Ils ont été rejoints récemment par le frère Yves Stoesel, bienvenu à lui !

Le Père Olivier a un appareil photo et il sait s'en servir pour capter la lumière, le cadre, la beauté et l'instant. Il nous a confié une série de ses photos sur le village et sur le déroulé des saisons. Plusieurs nous ont servi et nous serviront à l'avenir pour illustrer le site lalouvesc.fr, comme[ ici](/vivre-a-lalouvesc/histoire-identite/geographie-paysage-biodiversite/), [là](/decouvrir-bouger/sanctuaire/lalouvesc-haut-lieu-spirituel/) ou encore [là](https://www.lalouvesc.fr/decouvrir-bouger/sanctuaire/).

Nous avons aussi confectionné avec sa complicité un album avec celles qui témoignent du passage des saisons.  Vous pouvez le feuilleter ci-dessous sous forme d'un ebook (pour bien en profiter mettez l'image en plein écran en cliquant sur le petit carré en bas à droite de la fenêtre). Vous pouvez aussi, si vous préférez, en télécharger une [version Pdf](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/une-annee-a-lalouvesc-o-de-framond-2021.pdf).

<iframe src="https://cdn.flipsnack.com/widget/v2/widget.html?hash=hcf2mpt6rh" width="100%" height="480" seamless="seamless" scrolling="no" frameBorder="0" allowFullScreen></iframe>