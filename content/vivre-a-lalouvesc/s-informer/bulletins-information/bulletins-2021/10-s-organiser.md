+++
date = 2021-04-29T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Mai 2021"
title = "10 - S'organiser"
weight = 8

+++
Malgré les difficultés de la situation sanitaire, l'hiver nous a permis collectivement d'engager les premiers travaux et les premières réparations. Au mois de mai, le temps des semailles est arrivé. Il faut organiser les finances, prévoir les investissements et commencer à imaginer l'avenir. On trouvera dans ce bulletin un point sur les propriétés municipales et leur avenir, la présentation des artistes du Carrefour des Arts, un projet de "jardin idéal" et comme toujours plein d'autres informations.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer) ](/media/bulletin-de-lalouvesc-n-10-1er-mai-2021.pdf)et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Bonjour à toutes et à tous,

Nous voilà pratiquement à l'ouverture de la saison estivale 2021. Nous avons travaillé dur pour finaliser tous les travaux entrepris dans la commune. Le nettoyage printanier du village est prévu. Nous serons fin prêts pour l'ouverture de la saison.

L'ouverture des bars, des restaurants et d'autres établissements culturels s'assouplit. Mais bien des incertitudes restent, en particulier pour la date d'ouverture du camping.

Nous avons tout organisé pour que la saison se passe bien, sans tenir compte d'un éventuel affaiblissement touristique dû à la pandémie. Les associations ont prévu leurs spectacles, le Carrefour des arts est prêt avec un panel d'artistes éblouissants, il y a également un programme très fourni chez nos amis des Promenades musicales. L'ardéchoise étant malheureusement annulée, les membres du comité des fêtes s'emploient à fabriquer des figurines pour, malgré tout, décorer le village pour l'été. Nous accueillerons tous ensemble nos estivants dans les meilleures conditions possibles.

Sur le plan technique, les travaux de réparation du mur du cimetière vont commencer vers le 15 mai. La tâche est délicate, il faut déconstruire et reconstruire le mur au fur et à mesure en prenant toutes les précautions relatives à la stabilité du terrain. L'entreprise choisie est très compétente en la matière.

Nous avons réuni tous les éléments nécessaires à la démolition de l'hôtel Beauséjour. Le permis de démolir nous est revenu accepté, vous pouvez prendre connaissance de l'arrêté affiché en mairie. Il nous reste quelques détails périphériques à la démolition à résoudre : le déplacement des lignes électrique (Enedis) et téléphonique (France Télécom).

Je souhaite à tous les professionnels de la restauration une prochaine et très bonne réouverture. La tâche ne sera pas simple, mais il faut pouvoir rebondir et reprendre une activité normale. J'ai constaté que l'ensemble de nos commerçants ont conscience de la situation sanitaire toujours précaire et qu'ils prennent et prendront toutes les précautions utiles. Bon courage à toutes et tous !

Bravo à l'ensemble de nos associations, toujours mobilisées pour l'amélioration de notre village, merci encore à l'ensemble de la population pour leur implication aux tâches citoyennes. J'ai grandement apprécié votre intervention, qui a permis de remettre en ordre notre terrain de camping. Il s'agit là d'un élément essentiel pour les ressources communales et les travaux effectués contribuent aussi à l'amélioration de l'état général du village. Merci également à l'ensemble des membres de mon conseil municipal tous mobilisés aux diverses tâches communales.

Tous ensemble et unis, nous formons un groupe œuvrant pour l'amélioration de notre vie quotidienne et du bien commun.

Amitiés,

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Circulation

{{<grid>}}

{{<bloc>}}

![](/media/limitation-grange-neuve-1.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/limitation-grange-neuve-2.jpg)

{{</bloc>}}

{{</grid>}}

A la demande des habitants des panneaux de limitation de vitesse ont été posés à Grange Neuve.

### Plan de circulation et fleurissement

En prévision de la saison estivale, il est important de réviser l'ensemble du plan de circulation et de stationnement dans le Centre du village afin de conforter l'accueil dans le village et aussi d'assurer la sécurité des piétons et la fluidité d'un trafic à vitesse réduite. A cette fin le Comité Vie locale organise une réunion comprenant des élus et des non-élus concernés, début mai pour statuer sur cette question. Nous serons aussi attentifs à toutes les remarques que vous voudrez nous envoyer sur ce sujet à mairie@lalouvesc.fr.

Un autre groupe de travail du Comité Vie locale prépare le fleurissement du village pour la saison à venir. Si vous désirez y participer, n'hésitez pas à contacter la Mairie.

### Bornes Wifi et accès public à Internet

Deux nouvelles bornes wifi ont été installées. L'une couvre un rayon de 50 mètres autour de la Mairie, l'autre couvre l'ensemble du camping. Ces réseaux sont librement accessibles sans mot de passe. Tout le monde peut les utiliser sans contrainte, mais évidemment dans les limites de la loi. Avec la borne installée à l'Office du tourisme, il y a donc maintenant trois points de connexion en libre-service pour des terminaux mobiles (tablettes, smartphones...) sur le village.

Les bornes municipales ont été installées par Numerian et utilisent le réseau Cigale. Elles sont conformes à la réglementation en vigueur.

Par ailleurs, un ordinateur pour la consultation d'internet en libre-service est enfin opérationnel aux heures d'ouverture de l'agence postale. N'hésitez pas à l'utiliser, la connexion est excellente.

### Le point sur les travaux

##### Camping

Après l'important effort réalisé au cours des journées citoyennes, l'aménagement se poursuit sur le camping. Certains citoyens poursuivent les travaux de peinture et de menuiserie. Pour le mini golf, ils en sont maintenant aux derniers fignolages, merci à eux !

![](/media/mini-golf-final.jpg)

Des opérations de terrassement ont eu lieu pour dégager les abords du tennis,  et pour préparer l'arrivée de la tente Lodge.

Deux mobil-homes situés en bas du camping ont trouvé une meilleure place, tout en haut, à côté de leurs congénères. Il y aura ainsi un village de mobil-homes au sommet du camping. Ce fut un impressionnant déménagement ! {{<grid>}}{{<bloc>}}

![](/media/deplacement-mobil-homes-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/deplacement-mobil-homes-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/deplacements-mobil-home-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/deplacement-mobil-homes-4.jpg)

{{</bloc>}}{{<bloc>}}

Maintenant, l'emplacement destiné aux deux nouveaux équipements, tente Lodge et Cottage, est prêt pour les accueillir.

![](/media/camping-place-cottage.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-place-lodge.jpg)

{{</bloc>}}

{{</grid>}}

Bravo pour ce spectaculaire et délicat travail !

##### Cimetière et Beauséjour

Le chantier du cimetière démarre ce mois. L'ouverture des plis suite au marché public pour l’hôtel Beauséjour aura lieu dans la seconde semaine de mai. Nous sommes aussi tributaires des délais d'Enedis pour le déplacement de la ligne électrique.

Comme déjà indiqué dans le précédent Bulletin, le chantier de démolition engendrera nécessairement quelques désagréments de circulation, de la poussière et du bruit. Ce sera un épisode sans doute un peu pénible pour les Louvetous, mais incontournable. On ne se débarrasse pas d'une telle verrue sans nuisances ponctuelles. Nous ferons en sorte qu'elles soient les plus courtes possibles.

Nous souhaitons vivement pouvoir montrer aux estivants de 2021 que le village change et ce chantier du centre ville en est la manifestation la plus visible. Mais, bien sûr, il ne doit pas contrarier l'activité commerciale. Si les délais sont respectés, le bâtiment ne sera plus qu’un souvenir d’ici le début de l’été. Sinon, il nous faudra attendre l'automne.

### Le point sur les finances

Nous avons enfin pu mettre en place les indicateurs de gestion que nous avions prévus avant les élections. Ainsi, nous pourrons suivre de plus près et maîtriser mieux les dépenses de la Commune.

Les indicateurs seront présentés aux élus au cours du prochain Conseil et ils seront régulièrement présentés sur le site Lalouvesc.fr.

##### Emprunt

Nous avions indiqué dans le Bulletin du mois de mars une hypothèse d'un emprunt de 375.000 € comprenant une renégociation des emprunts déjà engagés dans les mandats précédents.

Des négociations ont été engagées avec plusieurs banques. La proposition de la Caisse d'épargne ne nous a pas paru satisfaisante. Le ré-échelonnement de notre emprunt initial faisait perdre un total de 60 k€ à la Commune, si l'on additionne l'indemnité demandée pour le remboursement anticipé et les intérêts perçus par la banque sur l'étalement du nouveau prêt ouvert pour couvrir ce remboursement.

Nous avons donc choisi la proposition de la Banque postale dont le taux était plus avantageux sans renégociation de l'ancien prêt qui viendra à échéance dans 10 ans. Afin de ne pas alourdir les remboursements annuels, nous avons alors réduit nos ambitions pour un emprunt de 250 k€. Ce choix a l'avantage de préserver l'avenir avec un endettement raisonnable de la Commune.

Il a évidemment l'inconvénient de réduire nos ambitions d'investissement à court terme. La somme empruntée sera prioritairement réservée aux chantiers du cimetière, de Beauséjour et du camping.

Il y a, bien sûr, d'autres chantiers à venir. Nous ferons le point en 2022 sur nos capacités d'autofinancement et donc les conséquences qui en découlent : les possibilités d'investissements et d'emprunts éventuels dans des limites raisonnables. Bien des événements sont encore à venir dont nous ne maîtrisons pas aujourd'hui les conséquences.

### Signature de la convention OGEC-Mairie

![](/media/convention-mairie-ogec.JPG)

La convention entre la Mairie et l'OGEC fixant le montant du forfait municipal par élève de l'école Saint Joseph versée par la Mairie a été signée le 23 avril.

Par la suite, le président de l'OGEC a envoyé un "droit de réponse" à la Mairie. Il sera annexé au prochain compte-rendu du Conseil municipal. Vous pouvez en lire une [copie ici](/media/20210425_droit-reponse.pdf).

## Zoom

### Penser et décider l'aménagement du village

Les élus travaillent depuis plusieurs mois avec le CAUE à une réflexion sur l'aménagement du village, lancée d'abord à l'occasion de notre projet de jeu-monument qui a nécessité un inventaire général des propriétés municipales, bâties et non bâties et prenant en compte évidemment les nombreuses études déjà réalisées sous les mandats précédents. Il s'agit maintenant, au-delà des réparations et rénovations obligées, de prendre des décisions importantes qui engageront l'avenir.

Les exigences de la situation sanitaire nous ont forcé a repoussé la consultation du Comité de développement, réunissant élus et non-élus. Le Conseil souhaite pourtant que la réflexion s'élargisse pour mieux éclairer ses choix. C'est pourquoi nous avons décidé de réunir le Comité dans la seconde quinzaine de mai en invitant des habitants représentatifs des diverses sensibilités et activités à rejoindre les élus qui siègent déjà dans le Comité. Il faudra, bien sûr, trouver des modalités de réunion compatibles avec la réglementation sanitaire du moment.

Pour préparer cette réunion, un dossier, comprenant plusieurs scénarios de développement ouverts et amendables, sera mis en ligne courant mai et chacun pourra réagir en envoyant ses remarques à la Mairie. Nous ferons une synthèse de ces remarques.

La réunion du Comité permettra d'affiner et de donner des appréciations sur les scénarios. Les décisions finales reviennent au Conseil municipal, mais plus elles seront éclairées par des avis pertinents, meilleures elles seront.

Pour concrétiser ces propos généraux, voici déjà un bref inventaire non exhaustif assorti de quelques questions.

##### 1 Bâtiments et propriétés communaux et intercommunaux fonctionnels

* Mairie. _Une réflexion reste à mener concernant l’utilisation et donc l’aménagement de la salle de réunion et de la salle du Conseil._
* Camping. _L’aménagement du camping est en bonne voie. Les principaux investissements ont été réalisés. Les journées citoyennes ont permis de nombreuses réparations. Il reste beaucoup à finaliser, mais l’orientation générale est fixée et nous en ferons le bilan en fin de saison._
* Centre d’animation communal. _Le foyer de ski de fond est en réflexion sur son avenir. Par ailleurs, l’ensemble des utilisations (hors Carrefour des Arts) mériterait une meilleure coordination des activités.  
  L'avenir du terrain de basket et le terrain attenant reste encore à étudier._
* Quatre gîtes communaux sur deux sites. _Une promotion plus forte pourrait augmenter le taux d’occupation._
* Ancienne école publique. _Sans doute un potentiel à exploiter, mais d'importants travaux à prévoir._
* Office du Tourisme. _Un déménagement_ à _prévoir de l’OT._
* Basilique. _Réparations à prévoir._
* Locaux techniques. _Une mise en conformité est en cours._

##### 2. Bâtiments non fonctionnels

* Sainte Monique. _Un potentiel à exploiter._
* Hôtel Beauséjour. _Organiser l'aménagement de l'espace avec l’éventuel déplacement de l’OT et le projet de Jeu monument._
* Bâtiments entrée rue St Régis. _Financement à trouver pour la démolition et pour préciser le projet d’urbanisme._
* Ecolotissement. _Une promotion à poursuivre._

##### 3. Terrains et espaces verts

* Cimetière. _Chantier prévu._
* Fontaine St Régis. Une réfection prévue. _Trouver une solution simple d'aménagement._
* Parc du Val d’Or. _Réparation et entretien nécessaires. Aménagement en réflexion : jeu monument, jardin idéal…_
* Parc du Grand Lieu (face à la Samov). _Espace vert du quartier Grand Lieu à valoriser._
* Terrain de foot. _Espace à mieux exploiter. Bâtiment des vestiaires à mieux exploiter._
* Terrain à Bobigneux. _Possibilité d’un espace de pique-nique._
* Autres terrains. _Inventaire à compléter._

##### 4. Autres bâtiments non municipaux, mais d’importance sur le village

Ci-dessous quelques propriétés qui ne sont pas sous la responsabilité de la Mairie, mais qui sont d'importance pour la vie du village.

* EHPAD, le Balcon des Alpes. _Bâtiment récent, bien adapté. Accueille un cabinet médical._
* École Saint Joseph. _Bâtiment fonctionnel._
* Maison St Régis.  _Accueille les activités du Sanctuaire. Bâtiments en partie à rénover._
* Abri du Pèlerin. _Bâtiment fonctionnel mais qui mériterait une rénovation._
* Cénacle. _Perle du village. Vente en cours. A accompagner._
* La vie Tara. _Vente en cours. A suivre._

##### 5. Autres espaces et terrains d’importance

* Place Marrel. _Mériterait un aménagement._
* Place du Lac. _Accueille le marché qui mériterait un développement. Marché bio ?_
* Parc des Pèlerins. _Trop méconnu._

##### Plan des pôles d'animation actuels et potentiels

En rouge les pôles existants ou en développement à court terme, en jaune, les pôles qui mériteraient d'être développés dans l'avenir.

![](/media/plan-poles-d-animation.jpg)

### Un festival poétique de formes et de couleurs pour le Carrefour des Arts 2021

Le programme du Carrefour des Arts est maintenant bouclé. Voici pour vous en avant-première, une courte présentation. Comme déjà indiqué dans le précédent Bulletin, cette année, le Carrefour réunira en juillet et août sept artistes hors-normes dans un véritable festival poétique de formes et de couleurs.

Il reste évidemment à préparer l'écrin qui les recevra. Un programme de renouvellement des tentures est prévu, le week-end du 5 et 6 juin aura lieu un atelier couture, tous les talents de couturiers et de couturières y seront les bienvenus.

{{<grid>}}

{{<bloc>}}

![](/media/carrefour-des-arts-2021-suhai-1.gif)

{{</bloc>}}

{{<bloc>}}

![](/media/carrefour-des-arts-2021-shasha-1.jpg)

{{</bloc>}}

{{</grid>}}

La première salle sur fond noir donnera le ton avec les époustouflantes sculptures de papier de Suhail Shaihk qui entreront en résonance avec les couleurs des batiks de Shasha Shaikh.

![](/media/carrefour-des-arts-2021-tharel-2.png)

Plus haut, la lumière sera à la fête avec le travail du verre de Xavier Carrère qui invite à jouer avec les diffractions colorées de ses œuvres dont certaines, monumentales, seront installées dans le village.

![](/media/carrefour-des-arts-2021-carrere-2.jpg)

Après ces premières émotions, la poésie prendra le relais avec la pierre et la résine colorée d’Hervé Tharel dont s’inspire, et réciproquement, le poète Michel Béatrix. Celui-ci proposera durant l’exposition quelques lectures et improvisations.

![](/media/carrefour-des-arts-2021-jacquemet-1.gif)

Les peintures abstraites de Martine Jaquemet, dans lesquelles le poète Lucien Giraudo voit les monstres se dévorer sous forme de fleurs, poursuivront le geste poétique sur le même étage.

![](/media/carrefour-des-arts-2021-gregoire-1.jpg)

Présentée dans la dernière salle de l’exposition, la série de peintures haute en couleur  « les imaginaires » de Claude Grégoire conclura ce parcours poétique comme une invitation au partage.

Ainsi balayant la morosité des épisodes Covid, le Carrefour des arts sera en 2021 plus vivant que jamais !

### Un jardin idéal en 2022

Il existe déjà le "Palais idéal", construit par le facteur Cheval à Hauterives dans la Drôme avec des galets ramassés sur son chemin, pourquoi pas un "Jardin idéal" à Lalouvesc en Ardèche ? C'est en tous cas le projet de Xavier Pourteau Cazalet et de son association les Jardins du Haut-Vivarais. Voici comment il nous l'a présenté.

![](/media/jardins-du-haut-vivarais.JPG)

_Le projet est d’aménager à Lalouvesc à l’endroit le plus stratégique du village un petit potager d’une vingtaine de m2 qui sera opérationnel dès l’été 2022. Ce potager d’inspiration permaculturelle sera une véritable vitrine pour le village ; il encouragera les visiteurs à penser leur propre potager et pourra faire de Lalouvesc l’exemple du village ardéchois sur la voie de la résilience. Un petit composteur de 3 m3 viendra compléter la réalisation ; il assurera l’autonomie du potager pour sa fertilisation et constituera l’exemple parfait de la gestion des déchets organiques au niveau d’une petite commune._

_Le projet du potager a plusieurs buts :_

* _Promouvoir le village et en faire une vitrine de l’autonomie alimentaire, un thème majeur de la résilience._
* _Le potager idéal sera une structure unique avec très peu d’équivalents sur notre territoire ; une bonne communication en fera un atout majeur pour le village._
* _Inciter les habitants et les visiteurs à créer leur propre potager en prouvant que l’on peut produire énormément et localement sur des toutes petites surfaces. Inutile de préciser que nous sommes assaillis depuis un demi-siècle par la malbouffe et que les perspectives d’avenir ne sont pas très réjouissantes (climat, nourriture…)._
* _Accorder une place privilégiée aux enfants._

Le "Jardin idéal" sera parrainé par Perrine et Charles Hervé-Gruyer, fondateurs de la [ferme biologique du Bec Hellouin](https://www.fermedubec.com/), vitrine mondiale de la permaculture. Sa réalisation sera prise en charge par [l'association des Jardins du Haut Vivarais](/projets-avenir/changement-climatique/vers-un-village-resilient/) qui propose déjà des visites de potagers et des stages de semis. Plusieurs conférences sont prévues cet été.

## Suivi

### Un atelier pour décorer le village

L'annulation de l'Ardèchoise, n'a pas découragé le Comité des fêtes. Les décorations qu'il devait construire à cette occasion serviront, comme chaque année, à égayer le village tout au cours de la saison touristique.

{{<grid>}}

{{<bloc>}}

![](/media/comite-des-fetes-deco-1.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/comite-des-fetes-deco-2.jpg)

![](/media/comite-des-fetes-deco-3.JPG)

{{</bloc>}}

{{</grid>}}

Dans le cadre de la préparation des décorations du village, un atelier peinture est organisé par le Comité des fêtes mercredi 5 mai à 9h à Ste Monique. Dans la mesure du possible nous travaillerons en extérieur dans le strict respect de la distanciation.

Venez nombreux et nombreuses avec votre masque et votre bonne humeur !

Agnès 07.66.67.94.59

### Ouverture de l'Abri du pèlerin

L'Abri du pèlerin a été remis en eau et nettoyé grâce à l'appui de Louvetonnes et Louvetous. Un nouveau témoignage de l'engagement total d'un village tourné résolument vers l'accueil.

Symbole de l'arrivée des beaux jours, cette institution louvetonne est maintenant ouverte.

![](/media/abri-du-pelerin.jpg)

En 2020 malgré la pandémie qui avait retardé son ouverture l'Abri a cumulée 1.200 nuitées, soit l'équivalent de l'année précédente, ce qui en fait, de loin,  le plus gros service d'hôtellerie du village, même si, il y a 20 ans, on en comptait le double.

Il comprend 100 lits. Des groupes, des familles, beaucoup de randonneurs trouvent là un toit et un accueil simple mais chaleureux pour un prix défiant toute concurrence. [Plus d'informations](https://www.ardeche-guide.com/hebergement/gites-d-etape-et-de-sejour/gite-d-etape-l-abri-du-pelerin-162969).

### Quelques actualités parues sur Lalouvesc.fr au mois d'avril

Ci-dessous, un rappel des informations publiées sur le site au cours du mois d'avril qui restent d'actualité.

#### Vaccinations sans rendez-vous les dimanches 2 et 9 mai

28 avril 2021  
Les pompiers assurent les vaccinations les week-end à la clinique des Cévennes à Annonay. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vaccinations-sans-rendez-vous-les-dimanches-2-et-9-mai/ "Lire Vaccinations sans rendez-vous les dimanches 2 et 9 mai")

#### La pêche est ouverte à Lalouvesc pour les enfants !

23 avril 2021  
Des truites ont été lâchées dans l'étang du Grand Lieu. A vos cannes, les enfants ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/la-peche-est-ouverte-a-lalouvesc-pour-les-enfants/ "Lire La pêche est ouverte à Lalouvesc pour les enfants !")

#### Programme 2021 des Promenades musicales (actualisé)

20 avril 2021  
Le programme complet 2021 des promenades [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/programme-2021-des-promenades-musicales/ "Lire Programme 2021 des Promenades musicales (actualisé)")

#### Covid-19 : dépistage sans rendez-vous à Satillieu et vaccination à Annonay

19 avril 2021  
Dépistage mardi 27 avril Satillieu, vaccination à Annonay pour les plus de 55 ans [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/covid-19-depistage-sans-rendez-vous-a-satillieu-et-vaccination-a-annonay/ "Lire Covid-19 : dépistage sans rendez-vous à Satillieu et vaccination à Annonay")

#### L'Ardéchoise 2021 annulée

17 avril 2021  
Pour la deuxième année consécutive, la course cycliste mythique, l'Ardéchoise, est annulée en raison de la situation sanitaire. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/l-ardechoise-2021-annulee/ "Lire L'Ardéchoise 2021 annulée")

#### A Lalouvesc, la neige n'arrête pas le citoyen

17 avril 2021  
Le tennis est nettoyé, les refuges sont réparés, le grillage du mini-golf est posé... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/a-lalouvesc-la-neige-n-arrete-pas-le-citoyen/ "Lire A Lalouvesc, la neige n'arrête pas le citoyen")

#### Etes-vous intéressé(e)s par le tir à l'arc ?

16 avril 2021  
Votre avis sur l'ouverture d'une nouvelle section d'Ardèche Sport Nature [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/etes-vous-interesse-e-s-par-le-tir-a-l-arc/ "Lire Etes-vous intéressé(e)s par le tir à l'arc ?")

#### Vaccination pour les plus de 55 ans et les plus de 60 ans

15 avril 2021  
Le point sur l'élargissement des vaccinations... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vaccination-pour-les-plus-de-55-ans-et-les-plus-de-60-ans/ "Lire Vaccination pour les plus de 55 ans et les plus de 60 ans")

#### Ecole/APEL : Vente de plants de légumes et fleurs

14 avril 2021  
Une opération locale et de saison pour récolter des fonds pour les activités des enfants de l'école Saint Joseph de Lalouvesc... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/ecole-apel-vente-de-plants-de-legumes-et-fleurs/ "Lire Ecole/APEL : Vente de plants de légumes et fleurs")

#### Compte rendu du Conseil municipal

14 avril 2021  
Séance du 7 avril 2021 [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/compte-rendu-du-conseil-municipal/ "Lire Compte rendu du Conseil municipal")

#### N'attendez plus pour réserver votre hébergement au Camping du Pré du Moulin !

7 avril 2021  
Le camping a largement renouvelé son offre. L'ensemble des tarifs sont maintenant disponibles... Les réservations se prennent en ce moment... [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/n-attendez-plus-pour-reserver-votre-hebergement-au-camping-du-moulin/ "Lire N'attendez plus pour réserver votre hébergement au Camping du Pré du Moulin !")

#### Biblio'chouette est ouverte

7 avril 2021  
Vous êtes confinés ? Petits et grands, profitez-en pour lire un bon livre ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/biblio-chouette-est-ouverte/ "Lire Biblio'chouette est ouverte")