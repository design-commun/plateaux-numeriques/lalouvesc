+++
date = 2021-08-29T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n° 14 Septembre 2021"
title = "14 - Les beaux gestes"
weight = 1

+++
On trouvera dans ce Bulletin,  la relation de quelques beaux gestes : une journée particulière du Carrefour des Arts, deux concerts de la ferme de Polly, des échos du Sanctuaire les 8 et 15 août, du club de foot, de la brocante à venir  et, comme toujours, plein d'autres informations.

Sans ces beaux gestes, ceux des bénévoles qui donnent de leur temps pour que le village vive et accueille avec le sourire les visiteurs, ceux des bricoleurs qui, sans compter non plus leur temps, réparent et construisent pour que le village soit présentable et élégant, enfin ceux des artistes qui nous ont enchantés tout au long été, sans tous ces beaux gestes, Lalouvesc perdrait son âme.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce[ ](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/13-serieux-et-sourires-lalouvesc.pdf)[fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-14-1er-septembre-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Chers amis

L’heure de la rentrée a bientôt sonné, nous sommes fin août, les vacances d'été sont pour beaucoup malheureusement terminées.

Il est trop tôt pour tirer un vrai bilan de cette saison. Globalement, on peut dire qu'elle s’est bien passée, Le temps n’a pas toujours été de la partie, surtout en juillet où la pluie a joué les trouble-fête. Le bal du 14 juillet a pu tout de même se tenir. Le mois d’août a été meilleur.

Beaucoup de monde est passé aux permanences de la mairie, toutes vos remarques et suggestions ont été prises en compte, elles alimenteront nos réflexions.

Maintenant il faut penser à la rentrée et réactiver les actions en cours. La démolition de l’hôtel Beauséjour, la réfection de la chaussée en quelques endroits du village les plus abîmés, l'installation définitive du stationnement et des sens de circulation, globalement bien accepté par les villageois, bien des dossiers restent à traiter et finaliser.

Je tiens personnellement à vous annoncer la démission du Conseil municipal de mon ami Jacky Verger pour des raisons personnelles. Fidèle compagnon, nous étions ensemble conseillers municipaux sous l’ancienne mandature, beaucoup de souvenirs nous lient. Je tiens à lui réitérer mon inaltérable amitié.

Le 1er septembre est là, c’est la rentrée générale, c’est la rentrée des classes, la reprise du travail pour l’ensemble de nous tous, merci à tous les bénévoles impliqués dans diverses associations cet été, bonne rentrée scolaire pour nos enfants, bon courage à vous tous.

Votre dévoué, Jacques Burriez.

## Actualités de la Mairie

### Démission d'un conseiller

Jacky Verger a souhaité pour des raisons personnelles quitter l'équipe municipale. Le Maire, regrettant son départ, le remercie pour son appui et sa contribution chaleureuse à la vie du village à la Mairie et plus largement.

### Secrétariat, départ et arrivée

Gladys Durieux, ayant trouvé un poste correspondant mieux à ses souhaits de carrière, a présenté sa démission de son emploi de secrétaire de mairie à mi-temps.  
Elle a été remplacée dès le 1er septembre par Olivier Denis pour un contrat de un an.

### Ecolotissement

{{<grid>}} {{<bloc>}}
Deux nouveaux compromis de vente doivent être signés tout début septembre (parcelles 1 et 5). D'autres sont en vue. Nous devrions atteindre bientôt le nombre de signatures attendues pour lancer les travaux d'aménagement.

Attention, si vous souhaitez construire à Lalouvesc, il est grand [temps de réserver](https://www.lalouvesc.fr/projets-avenir/changement-climatique/ecolotissement-du-bois-de-versailles/) ! Ce sont les derniers terrains constructibles disponibles.

{{</bloc>}} {{<bloc>}}
![](/media/ecolotissemet-plan.jpg)
{{</bloc>}} {{</grid>}}

### Rue de la fontaine, une rue partagée

{{<grid>}} {{<bloc>}}

![](/media/panneau-rue-partagee.jpg)

{{</bloc>}} {{<bloc>}}

Des représentants des résidents et des élus se sont réunis pour trouver les meilleures modalités de circulation et de stationnement dans la rue de la fontaine.  
Il a été proposé que la rue soit déclarée "rue partagée", où cohabitent plusieurs moyens de déplacement en donnant la priorité aux plus vulnérables : les piétons.

La circulation automobile serait limitée à 20km/h, le stationnement automobile interdit dans les zones étroites, des places étant délimitées sur les zones autorisées.

{{</bloc>}} {{</grid>}}

Dans la rue des Cévennes, les bacs de fleurs ont remplacé les voitures, pour le plus grand bonheur de tous.

![](/media/15-aout-2021-1.jpg)

### Demande de mise aux normes à Milagro

Suite au passage de la commission de sécurité, un arrêté va être promulgué par le Maire pour une demande de mises aux normes sur plusieurs points. Les travaux devront être réalisés avant la fin de l'année.

### Jeu Monument : un petit avant un grand

L'été a été propice au mûrissement du projet de jeu-monument grâce au groupe de travail dédié et aux conseils de trois architectes, amis du village : Philippe Bosseau, Jérome Solari et Jacques Morel. Un document présentant le projet a été rédigé dont voici un résumé (le[ document complet est consultable ici](/media/un-jeu-monument-pour-celebrer-la-foret-sur-le-parc-du-val-d-or-a-lalouvesc.pdf)) :

{{<grid>}} {{<bloc>}}

> La commune de Lalouvesc souhaite produire un geste symbolique fort dont l’écho profitera à toute l’Ardèche verte en érigeant un jeu-monument célébrant la forêt pour privilégier un tourisme respectueux de l’environnement et conscient des défis de la transition écologique. Le village pourra ainsi accroître son attractivité, en profitant de l’ouverture nouvelle de son quartier central vers les Cévennes et renouer avec le tourisme vert, pendant du tourisme religieux traditionnel au village de St Régis.
>
> La conception du monument est prévue en deux étapes. La première s’appuiera sur une participation des écoles primaires du territoire, une réflexion environnementale menée avec des experts et un concours proposé aux étudiants en architecture et en design de la région pour un “petit monument” qui préfigurera le monument définitif dont la conception et la construction seront lancées dans un second temps sur la base d’un financement participatif.

{{</bloc>}} {{<bloc>}}

![](/media/jeu-monument-couverture.jpg)

{{</bloc>}} {{</grid>}}

A la suite de cette réflexion, une demande de subvention pour la première étape (petit monument) a été déposée fin août au programme Leader.

Nous avons aussi commencé à recueillir des cartes postales avec des suggestions de jeux. Beaucoup d'enfants se sont exprimés. Quelques rares cartes se sont inquiétées, considérant un tel projet superflu face à d'autres autres priorités jugées plus urgentes.

La préoccupation est légitime. Réparer le village doit rester la priorité des priorités. Mais réparer ne sera pas suffisant, une population permanente de moins de 400 habitants ne permet pas, à elle seule, de maintenir sur place les commerces ou les services essentiels. La prospérité du village a toujours reposé sur l'apport des visiteurs, pèlerins et touristes, dont malheureusement l'afflux s'est réduit au cours des années. Il est vital pour le village de renverser la tendance. Le village restera d'une grande fragilité si nous n'augmentons pas son attractivité.

Le dynamisme des associations et des bénévoles est un apport essentiel, mais il faut le conforter et l'encourager par des initiatives fortes qui fassent parler du village pour retrouver l'élan qui profitera à tous.

Pour autant, il ne faut pas fragiliser les finances de la commune. C'est pourquoi un processus en deux étapes a été imaginé. La première, modeste, devrait trouver, dès cette année espérons-le, les subventions pour aboutir. La seconde ne sera lancée que si les fonds suffisants sont réunis, notamment par l'appui de fondations privées.

## Zoom

Le zoom de ce mois met l'accent sur deux moments des manifestations artistiques de cette saison, qui ont eu lieu malgré la situation sanitaire difficile : une performance au Carrefour des Arts, deux concerts de jazz à la grange de Polly.

### Une journée d'exception au Carrefour des Arts

En dépit des très mauvaises conditions météo et sanitaires, le succès du Carrefour des Arts ne se dément pas. La fréquentation a été à l'image de celle de l'année dernière : largement plus de 8.000 visiteurs.

Pour tous ceux qui n'ont pu y assister, voici un retour sur une journée particulière, celle du 16 août, un moment étonnant, inédit, une première mondiale, un cadeau pour les Louvetous et les estivants de passage !

{{<grid>}} {{<bloc>}}

![](/media/carrefour-16-aout-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-16-aout-2021-3.jpg)
{{</bloc>}} {{</grid>}}

Sous un chapiteau contigu au CAC, les musiciens fidèles des Promenades musicales, le quatuor de Gilles Lefevre et le compositeur Felipe Carrasco ont déroulé leurs talents, simultanément avec la peintre Martine Jaquemet qui exposait au Carrefour. Le geste du violoniste guidant l'archet s'est uni à celui de la peintre guidant le pinceau, comme on peut le voir et l'entendre sur la vidéo ci-dessous.

<iframe width="560" height="315" src="https://www.youtube.com/embed/Auq5GsrHWTk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

La performance a atteint son sommet au moment de l'interprétation, pour la première fois en public, d'un morceau de musique, inspiré par un tableau, envoyé par la peintre au compositeur, un vrai défi pour lui ! Une œuvre globale, parachevée ce jour-là par la peintre, inspirée à son tour par la musique,  jouée en direct sous nos yeux et nos oreilles conquises. Son titre : "Traces", dialogue sans parole, tout en images et sons, entre la peintre et le compositeur. Nul doute que cet exploit résonnera longtemps et loin !..

Voici le témoignage de Felipe Carrasco sur son expérience de composition d'une musique à partir d'une peinture :

> L'œuvre que la magnifique artiste peintre Martine Jaquemet m'a proposée, a éveillé et réuni, avec sa construction, les éléments principaux de l'œuvre que j'ai composée. Nous trouvons dans son livre d'artiste des blancs désolateurs qui peuvent correspondre à une musique glaciale, des traces superposées qui invitent au contrepoint. Je vois du sang et de la boue, qui me donnent un sentiment de claustrophobie. Je vois des énormes griffes, qui suggèrent du mouvement, un mouvement agressif, violent.  
> Des couleurs chaudes suggèrent des harmonies plus rondes, calmes, qui consolent. Et puis les couleurs fortes peuvent éclater sous la forme de feux d'artifices musicaux où l'harmonie ne bouge pas, mais il y a une accumulation harmonique et où les articulations sont toutes différentes. En regardant son œuvre de plus loin, je peux faire la part des choses et prendre de la conviction avec mon vécu de l'œuvre, et ainsi proposer une fin musicale libératrice, comme une synthèse.
>
> L'expérience a été absolument fabuleuse grâce à la magnifique interprétation du quatuor Solystelle dirigé par Gilles Lefevre. Les musiciens ont pu apprécier l'œuvre de Martine et s'en sont aussi inspirés. Et Martine, ensuite, s'est inspirée elle-même de l'interprétation des musiciens pour finir son livre d'artiste, en refermant le cycle. La démarche artistique est inoubliable, nous avons réussi à casser les barrières que la société impose en nous spécialisant dans un seul métier - ou genre artistique dans notre cas.
>
> Une grande victoire qui nous donne envie de répéter l'expérience, d'aller plus loin et de la partager avec le public, ce qui est au cœur de notre vocation artistique.

Pour apprécier à sa juste mesure le propos du compositeur, il reste à regarder et écouter la vidéo ci-dessous.  
L'image qui apparait dans la vidéo correspond au livre d'artiste de Martine Jaquemet. En haut : l'intérieur. En bas : l'extérieur, qui en se pliant laisse comme "titre" la troisième section (qui dit en lettres blanches "Traces"), utilisée ici comme vignette pour la vidéo.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YQPZjSW1TP4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Du jazz à la grange de Polly

Jacques Trébuchet nous a envoyé deux articles rédigés par le pianiste Stéphane Matthey, sur les deux derniers concerts de la saison à la grange de Polly. Extraits :

_Cette année, les désordres causés par le virus COVID ont conduit les Promenades musicales à annuler l'ensemble de la programmation de l'été 2021._

_Par conséquent, une autre association, mais de Jazz cette fois, la Noja New Orleans Jazz Association) présidée par le musicien Jacky Boyadjian, a donc repris à son compte les deux concerts de Jazz annulés (…)_

{{<grid>}} {{<bloc>}}

**_Concert du 11 août :_ Hommage à Gershwin**

![](/media/grange-de-polly-2021-1.JPG)

_Le défi était de taille, deux seuls musiciens sur scène pour capter l'attention d'un public qui les découvrait : le pianiste Philippe Carment venu de Rouen et le spécialiste des vents, Jean-François Bonnel. (…) Pour la circonstance, nos deux musiciens avaient convenu de présenter un répertoire en guise d'hommage à George Gershwin dont les compositions ont beaucoup alimenté les thèmes de Jazz. (…)_

_Au total, une quinzaine de morceaux brillamment interprétés par des artistes dont les improvisations permettaient de retrouver le thème de la mélodie d'une manière irréfutable et dont la complicité sur scène rendait la communication avec le public évidente et prospère._

{{</bloc>}} {{<bloc>}}

**_Concert 13 août :_ Hommage à Armstrong**

![](/media/grange-de-polly-2021-2.JPG)

_Un public conséquent était au rendez-vous et une chaude ambiance a prévalu. (…)_

_Ainsi donc, la formation, dirigée par le jeune et brillant trompettiste de 31 ans Hippolyte Fevre, s’était associée à un musicien non moins brillant et régulier à la Grange de Polly, Jean-François Bonnel (saxos et clarinette)._

_Venu de Paris, comme Hippolyte Fevre, le trombone Benoit de Flamesnil complétait merveilleusement cette ligne des vents._

_Enfin, une rythmique soutenait l'ensemble avec Elise Sut au Tuba, Jacky Boyadjian au banjo et Stéphane Matthey au piano._

{{</bloc>}} {{</grid>}}

_Ainsi donc s'est achevée, en apothéose, une courageuse entreprise du maintien des concerts de Jazz à Lalouvesc malgré l'application stricte des mesures sanitaires._

Stéphane Matthey, pianiste

## Suivi

### Deux fêtes au Sanctuaire sous le soleil

#### Le 8 août

Saint Jean François Régis a bien travaillé le 8 août, car entre deux jours de mauvais temps, le soleil brillait miraculeusement sur la fête du sanctuaire et le beau jardin potager de la maison des Jésuites.

Comme chaque année, les habitants du village, et ceux de l’été ont uni leurs joyeuses volontés pour que continuent, malgré le contexte sanitaire restrictif, la vente des confitures, des livres, le tir à l’arc, la tombola et surtout le repas, de plus en plus gastronomique, à prendre sur place ou à emporter. Cette année, c’est Nice qui était à l’honneur !

Les recettes des activités que les bénévoles déploient participent aux actions mettant en valeur le sanctuaire.

L’équipe des bénévoles, gourmande et enthousiaste, compte sur l’imagination des villageois pour trouver le thème de l’année prochaine.

#### le 15 août

En ce jour de grâce du 15 août, la montagne des Pardons avait mis son habit de soleil.

Ainsi qu’ils le font depuis des siècles, les pèlerins des environs, et parfois de plus loin encore sont montés à La Louvesc. Ils le font, le sachant ou non, dans la plus pure tradition biblique qui veut que les révélations les plus importantes se manifestent au sommet d’une montagne. C’est pourquoi en l’honneur de Saint Jean-François Régis, La Louvesc a été nommée Montagne des Pardons.

![](/media/15-aout-2021-4-photo-ot.jpg "Photo OT")

![](/media/15-aout-2021-5-photo-ot.jpg "Photo OT")

Dans le Parc avec les prêtres de la Compagnie de Jésus et deux prêtres de passage, les attendait la fête de l’Assomption. Cet évènement liturgique, bien ancré dans la tradition, est tiré d’un évangile apocryphe, mais répond à la ferveur profonde de l’humanité qui cherche la douceur et la tendresse d’une mère universelle.

Voilà pourquoi un millier de personnes sont venues, disciplinées d’une manière exemplaire aux contraintes sanitaires exigées, chanter les louanges de la Vierge Marie, et confier dans un bel élan de communion leurs inquiétudes, leurs espoirs, et aussi leur confiance en la vie.

Tout le monde s’est ensuite égayé dans le village et dans les forêts, profitant du bon air et de la fraîcheur de La Louvesc.

Grâce à l’organisation de la circulation et du stationnement des voitures par la mairie, la vigilance de la gendarmerie locale et l’engagement de nombreux bénévoles, la fluidité et la sécurité de la foule des pèlerins ont pu être assurées.

Ces deux fêtes, de longue tradition, unissent et animent le village, et apportent un afflux de visiteurs qui demeure essentiel pour l’activité économique de La Louvesc.

Michel Kuntzmann

### 4 septembre : une invitation des sœurs du Cénacle (rappel)

{{<grid>}} {{<bloc>}}

**Portes Ouvertes pour les habitants de Lalouvesc**

Les Sœurs du Cénacle invitent les habitants de Lalouvesc, à découvrir les trois salles qui retracent l’histoire de la Congrégation à Lalouvesc

**Samedi 4 Septembre 2021 de 15h à 18h30**

Ce parcours historique est destiné à la famille Cénacle (Sœurs, fraternité etc…) et ne sera pas public par la suite.

{{</bloc>}} {{<bloc>}}

Louvetous, soyez les bienvenus !

![](/media/le-cenacle.png)

{{</bloc>}} {{</grid>}}

### Le vestiaire du stade isolé

Le vestiaire du club de foot de Lalouvesc a été construit courant 1986 sous la municipalité de monsieur Mialon. 

Cela fait presque six ans que les clubs de Lalouvesc et de Pailharès ont fusionné afin de maintenir un club de foot dans nos villages. A partir de cette année, les matchs du club se dérouleront uniquement à Lalouvesc en raison des dimensions du terrain de Pailharès. De ce fait, le terrain de Pailharès sera utilisé seulement pour les entrainements.

Suite à des aides du Comité Drôme Ardèche, nous avons décidé de restaurer les vestiaires en réalisant l’isolation par l’extérieur. Cela faisait presque quatre ans que les matériaux pour l’isolation extérieure du local du foot de Lalouvesc étaient stockés dans l’entrepôt de Guillaume Besset. Après avoir voté à l’assemblée générale le 16 juillet 2021, les joueurs ont décidé d’attaquer les travaux début juillet. 

![](/media/vestiaire-foot-2021-1.jpg)

Depuis chaque week-end les travaux ont avancé rapidement dans une ambiance conviviale et amicale. En ce 30 août nous sommes sur la fin des travaux. Il aura fallu sept à huit jours pour tout finir.

![](/media/vestiaire-foot-2021-3.jpg)

Nous remercions Guillaume Besset d’avoir stocké tous les matériaux pendant tout ce temps, ainsi que toutes les personnes ayant contribué au bon déroulement des travaux.

Romain Fourezon

### Quand la Brocante fait son cinéma

Depuis bientôt 20 ans la brocante de notre village fait son show. A présent c’est un peu l’ambiance CAMPING (chacun son emplacement) – Camping _LES FLOTS BLEUS_. Certes nous n’avons pas la mer, pas Claude Brasseur mais l’ambiance est là. Presque la même !!!...

{{<grid>}} {{<bloc>}}

![](/media/brocante-1.jpg)

« _Allo, je suis Monsieur Dubreuil, je voudrais réserver le n° 1. Comment il n’est plus disponible ? Je suis MONSIEUR DUBREUIL. Je viens depuis des années. J’ai toujours cet emplacement..._ »

{{</bloc>}} {{<bloc>}}

![](/media/brocante-4.jpg)

« _Merci de me placer à l’ombre, au soleil il fait trop chaud à Lalouvesc !_ »

{{</bloc>}} {{</grid>}}

Des anecdotes comme celles-là nous en avons tous les jours, au téléphone, par mail, sur les bulletins d’inscription. Au final ces exigences se transforment en joyeux bazar. L’ambiance et la convivialité sont telles que nous prenons presque plaisir à jongler au travers des quelques 200 emplacements pour satisfaire le plus grand nombre ! Avouons-le, quelques fois les prises de tête sont telles que nous aurions bien envie de…. NON, NON, cherchons encore un peu LA solution pour une demande impossible dans l’organisation de ce bel évènement !

Ces deux dernières années la fête aurait pu être un peu moins ludique mais nous sommes tellement assaillis de demandes que les bons souvenirs priment sur les désagréments.

A TOUS nous souhaitons une belle journée le 5 septembre dans notre petit village pour la brocante 2021 !

> **_Pas de buvette, pas de restauration (pour satisfaire aux mesures sanitaires) ne rime néanmoins pas avec moins de bénévoles. Vous serez les bienvenus pour un créneau horaire à votre convenance dans la journée. Un point accueil sera organisé, une « brigade anti COVID » en place aux différentes entrées pour proposer du gel et rappeler le port du masque etc... Pour les lève-tôt nous recherchons des placeurs pour conduire les exposants à leur emplacement._**
>
> **_N’hésitez pas à vous manifester._**

Le Comité des Fêtes est toujours joignable :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 Lalouvesc.

Agnès G.

### Urbanisme : hier, aujourd'hui et demain

La démolition de l'hôtel Beauséjour devrait démarrer ce mois. Même si ce chantier, rendu nécessaire par la carence de ses anciens propriétaires, est attendu depuis longtemps, démolir un bâtiment de cette taille, emblématique de l'urbanisation du village de la première moitié du 20e siècle au moment le plus haut de sa prospérité, n'est pas un geste anodin. Il a des conséquences sur l'esthétique générale du village.

On peut déjà en tirer plusieurs leçons. La première est que les propriétaires qui n'entretiennent pas leur bien ne font pas seulement un mauvais calcul personnel, mais dégradent aussi l'aspect général du village, notre bien commun et entraînent des dépenses qui pèsent sur tous. La seconde est qu'il nous faudra prendre le temps de réfléchir collectivement à l'aménagement de l'espace laissé libre par la démolition et décider ensemble des volumes et fonctionnalités qu'il pourrait contenir, cohérents avec les enjeux actuels du village.

Si vous n'êtes pas convaincus du choc que peut produire une telle démolition, voici ci-dessous un autre exemple illustré par deux images tirées d'une vidéo encore inachevée de Matthieu Fraysse publiée sur le [Facebook des Amis de Lalouvesc](https://www.facebook.com/groups/47779411002/?hc_ref=ARRri9HzpWSzmvEopvqV1uScuE0ILWIiKrK6iMqrqzuUzbBMJLEP3k12H-4HCBha-bU&ref=nf_target) où il a intégré des photos anciennes sur des photos contemporaines du village. On ne peut que l'encourager à continuer. C'est un document amusant, une curiosité, mais c'est aussi un document bien utile pour mieux nous faire percevoir les particularités de l'urbanisation du village et de son évolution, heureuse ou malheureuse.

![](/media/video-mat-fraysse-2021-2.jpg)![](/media/video-mat-fraysse-2021-1.jpg)

### Trois mariages à Lalouvesc cet été

La nouvelle salle du Conseil a accueilli trois mariages cet été. Tous nos vœux de bonheur aux époux !

* Cédric Combier et Laura Vey le 2 juillet
* David Letellier et Florence Buisson, le 24 juillet
* Joël Pariente et Marie Jullien, le 14 août

{{<grid>}} {{<bloc>}}

![](/media/mariage-2021-1.jpg)

{{</bloc>}} {{<bloc>}}

![](/media/mariage-2021-2.jpg)

{{</bloc>}} {{</grid>}}

### Dans l'actualité le mois dernier

Ci-dessous, un rappel des informations publiées sur le site au cours du mois d'août qui restent d'actualité.

**Les gendarmes de l'Ardèche informent via l'appli PanneauPocket**  
21 août 2021  
Si vous avez l'application sur votre smartphone, vous pouvez vous abonner [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/les-gendarmes-de-l-ardeche-informent-via-l-appli-panneaupocket/ "Lire Les gendarmes de l'Ardèche informent via l'appli PanneauPocket")

**Compte rendu du Conseil municipal du 16 août 2021**  
19 août 2021[  
Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/compte-rendu-du-conseil-municipal-du-16-aout-2021/ "Lire Compte rendu du Conseil municipal du 16 août 2021")

**Vers l'immunité collective, campagne de vaccination de l'ARS**  
12 août 2021  
100 personnes témoignent [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/vers-l-immunite-collective-campagne-de-vaccination-de-l-ars/ "Lire Vers l'immunité collective, campagne de vaccination de l'ARS")

**Que d'eau ! Que d'eau !**  
3 août 2021  
La pluie n'a pas épargné Lalouvesc en juillet. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/que-d-eau-que-d-eau/ "Lire Que d'eau ! Que d'eau !")