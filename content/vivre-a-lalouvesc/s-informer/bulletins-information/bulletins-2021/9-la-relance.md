+++
date = 2021-03-31T22:00:00Z
description = ""
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Avril 2021"
title = "9 - La relance, malgré tout"
weight = 9

+++
Les incertitudes sanitaires sont toujours devant nous. Pourtant notre village est confiant en son avenir. Le budget 2021 ouvre enfin la porte aux investissements et, surtout, les Louvetonnes et les Louvetous ont montré par leur engagement citoyen sur le camping au mois de mars qu'ils n'étaient pas prêts à laisser se dégrader leur bien commun. Parallèlement, la saison estivale se prépare et nous disposons d'un outil de communication efficace. Voilà quelques sujets développés dans ce bulletin.  
Vous y trouverez aussi, comme toujours plein d'autres informations et quelques sourires.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-8-1er-avril-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Chers habitants de notre joli village

Tout d’abord, je tiens à vous remercier vivement pour votre présence aux journées citoyennes. Félicitations ! Votre importante mobilisation et votre implication nous ont permis d’avancer grandement sur la remise en état de notre camping, Le résultat est très significatif, Il fait écho dans tout le village et au-delà.

Le temps est au beau fixe, tout laisse présager ’une belle saison, mais la situation sanitaire est, comme l’épée Damoclès, une menace qui à tout moment peut réduire nos efforts. Celle-ci oblige à un renforcement des restrictions sanitaires. Il faut faire en sorte que nous puissions ralentir la progression et la circulation du virus, car la saturation des hôpitaux guette.

La menace est réelle, nous devons l'accepter, puis rebondissons, motivons-nous.  La perspective d’une belle saison est toujours là. Préparons, peaufinons nos actions pour réussir notre été, Puisque nous serons prêts, que l'attente aura été plus grande et pressante, nous en profiterons pleinement, bien au-delà encore des saisons précédentes !

Nous vivons dans une belle région. Nombre de personnes souhaiteraient vivre chez nous. Et nous allons sans doute accueillir une vague de personnes voulant se confiner dans notre village. Le désir de campagne est une opportunité nouvelle pour les territoires ruraux.

Sans doute, le tableau n’est pas totalement idyllique. La vie en général est parfois plus dure dans notre milieu rural qu’en ville. Pour une bonne intégration, les nouveaux arrivants doivent faire l'effort de participer à la vie du village, inversement les villageois doivent leur faire bon accueil. Il s'agit de s'impliquer dans la vie associative très active chez nous, de participer aux travaux d’intérêt collectif, de se prévenir contre la dureté du climat, d'anticiper les difficultés liées aux services ou au transport,. Si vous repérez une personne isolée ou qui rencontre des difficultés, n'hésitez pas à la rediriger vers nous, La Mairie est ouverte à tout le monde, L'équipe municipale sera, dans la limite de ses moyens, à la disposition de quiconque pour tout aide ou renseignement.

Je vous souhaite à toutes et à tous de très bonnes fêtes de Pâques, Profitez bien de ce long week-end avec les membres de votre famille, mais en appliquant scrupuleusement les précautions indispensables.

Sincères amitiés

Jacques Burriez, Maire de Lalouvesc

## Actualités de la Mairie

### Vaccinations et mesures sanitaires

L'ensemble des pensionnaires de l'EHPAD est vacciné contre la Covid 19. La mise en place prochaine d'un vaccinobus par la Préfecture avec l'aide de la Communauté de Communes a permis de faire le point sur le nombre de vaccinés de plus de 75 ans dans le village.  
Une trentaine de Louvetonnes et de Louvetous de plus de 75 ans ont pu se faire vacciner par leurs propres moyens. Une vingtaine d'autres ont pris rendez-vous pour le vaccinobus, notamment par l'intermédiaire de la Mairie et se feront vacciner entre le 6 et le 9 avril à Satillieu. Pour ceux qui ne pourraient s'y rendre par leurs propres moyens, la Mairie organisera une navette.  
Rappelons qu'aujourd'hui la vaccination est ouverte au plus de 70 ans. Il est prévu que la vaccination soit ouverte aux plus de 60 ans à partir du 16 avril, au plus de 50 ans à partir du 15 mai et pour tous à partir de la mi-juin.

Voici un rappel des principales mesures nouvelles concernant la lutte contre la pandémie :

* les déplacements sont limités à un rayon de 10km pour les loisirs et de 30km avec [une attestation](https://media.interieur.gouv.fr/attestation-deplacement-derogatoire-covid-19/) pour les divers achats et démarches ; ils sont limités au département et interdits entre 19h et 6h ;
* les commerces non-essentiels et les établissements de loisir sont fermés ; la vente d'alcool est interdite ;
* les écoles, collèges et lycées sont fermés.

### Lancement des travaux

Le lancement des travaux importants est toujours plus long qu'on ne le voudrait et les subventions reçues moins généreuses qu'attendues. Mais les deux dossiers les plus urgents, le cimetière et l'hôtel Beauséjour, avancent.

#### Mur du cimetière

Trois devis ont été demandés à des professionnels de la pierre, Deux réponses ont été reçues. Le choix de l'entreprise la moins disante, c'est-à-dire la moins chère, avec des critères identiques a été validée par le conseil municipal pour un démarrage des travaux mi-mai pour une période de 12 mois. Un constat d'huissier sera établi dans les jours qui viennent. suivi de l'ouverture du chantier avec l'application des règles en  
vigueur.

![](/media/cimetiere-lalouvesc-2.jpg)  
Le chantier est subventionné par l’État (DETR) à hauteur de 35%, le reste étant pour l'instant à la charge de la commune pour un total de coût du chantier de 50 k€ HT.

#### Hôtel Beauséjour

La commune est devenue propriétaire de ce bien depuis fin d'année 2020. Ce dossier compliqué a demandé et demande encore beaucoup de préparations entre les demandes insistantes de subventions (pour le moment seul l’État a répondu favorablement à hauteur de 35%), le diagnostic, le montage de l'appel d'offres, le constat d'huissier, les contacts avec les opérateurs de réseau (ERDF, France télécom), le permis de démolir, et, cerise sur le gâteau, les squatters (nos amis les chats).

![](/media/beausejour-3.JPG)

Le chantier de démolition engendrera nécessairement quelques désagréments de circulation, de la poussière et du bruit:.

Si tout va bien, ce bâtiment qui avait autrefois une bonne notoriété dans le village, ne sera plus qu'un souvenir d'ici le début de l'été. Ce sera un changement radical dans le centre du village avec une ouverture sur le Mézenc et le Gerbier de Joncs. L'équipe municipale travaille en parallèle avec l'aide du CAUE sur l'aménagement à venir de cet espace, notamment avec le projet d'un jeu-monument. La population sera associée à cette réflexion dès cet été et, pourquoi pas ?, à sa réalisation au cours de l'année 2022.

La Mairie devra assumer sur son budget propre la partie non-subventionnée. Elle attend toujours la réponse de la Région à sa sollicitation.

### Subventions aux associations, convention avec l'école

Le Conseil municipal a voté les subventions aux associations pour l'année 2021. Il y a quelques différences par rapport aux années antérieures.

L’Amicale des pompiers n’ayant pu avoir d'activité en 2020 n'a pas demandé le renouvellement de sa subvention en 2021. Par ailleurs, dans un souci de cohérence, il a été décidé d'harmoniser les subventions des trois principales associations de la Commune : le Comité des fêtes, le Carrefour des arts et les Promenades musicales. Une somme de 1 000 € est proposée pour 2021 pour chacune de ces associations. Enfin, le Conseil a proposé une subvention de 750 € pour l’association du Cinéma Le Foyer (voir plus loin l'article sur le cinéma).

* A.C.C.A : 90€
* A.D.A.P.E.I. : 100 €
* ALAUDA : 150 €
* A.S.P.L (Club de foot) : 200€
* BIBLIOTHÈQUE : 400€
* CARREFOUR DES ARTS : 1000€
* COMITÉ DES FÊTES : 1000 €
* CLUB DES 2 CLOCHERS : 215€
* F.N.A.C.A : 90 €
* LES MONTS DU BALCON : 215€
* LYRE LOUVETONNE : 215€
* PROMENADES MUSICALES : 1000€
* CINEMA LE FOYER : 750€

L'équipe municipale et l'OGEC se sont mis d'accord sur une convention pluriannuelle précisant le soutien de la Mairie à l'école. Il s'agissait de sortir du processus de "subventions exceptionnelles" trop longtemps renouvelé. La rédaction de cette convention a été délicate car elle est soumise à de nombreuses contraintes réglementaires et a, évidemment, des conséquences importantes. Il faut remercier tous ceux qui ont fait des efforts pour trouver le meilleur compromis pour soutenir l'école.

## Zoom

### Un budget pour investir

Il nous a bien fallu une année pour nous roder à l'ensemble des procédures comptables. Nous allons enfin pouvoir mettre en place les indicateurs de suivi de gestion que nous avions proposés avant les élections.

Les comptes 2020 ont été approuvés et le budget primitif 2021 a été voté par le Conseil municipal. Vous trouverez dans le compte-rendu du Conseil tous les détails. Voici ci-dessous quelques éléments saillants.  
Concernant le bilan 2020, par rapport à 2019, les dépenses de fonctionnement ont diminué d’environ 41.300 € essentiellement résultant des économies réalisées grâce à une meilleure maîtrise de nos achats et également des services extérieurs (contrats de prestation de services). A noter que les postes énergie et carburant ont baissé de 24.600 € par rapport à 2019.

Quant aux recettes de fonctionnement, la baisse des recettes réelles de 21.000 € par rapport à 2019 s'explique principalement par un remboursement de salaires et charges de 27.500 € en 2019 qui n'a pas été réitéré en 2020. A noter toutefois le tassement des produits issus des gîtes et du camping de l'ordre de 5.000 € dû à la période de confinement du printemps.

Pour le budget principal 2021, nos prévisions de dépenses sont de 461.828 € contre 401.007€ en 2020. Cette augmentation est principalement due au poste des charges de personnel et frais assimilés.

Nos prévisions de recettes sont de 499.946 € contre 519. 851 € en 2020. Cette diminution est due à la baisse des dotations de l'Etat que nous prévoyons en l’absence d’informations officielles.

Pour réaliser nos projets de rénovation et de développement, il faut compter sur la constitution de notre autofinancement, l’octroi de tout ou partie des subventions et la mise en place des emprunts. Pour cela, il nous faut poursuivre notre "chasse au gaspi", qui a été efficace en 2020, et compter sur la rénovation du camping pour augmenter nos recettes (croisons les doigts pour que la pandémie ne bride pas nos efforts). Nous devons toujours constituer et suivre de près les dossiers de demande de subvention. Et enfin, comme déjà exposé dans le dernier bulletin, nous avons prévu et négocié des modalités d'emprunt qui pourront nous permettre d'investir sans grever l'avenir de nos finances. Ce dossier sera très bientôt entre les mains des banquiers pour obtenir leur accord définitif.

### Un coup de pouce au cinéma

Les séances de cinéma à Lalouvesc se tiennent tous les lundis, mercredis, jeudis, samedis et dimanches en juillet et août dans la salle de l'Abri du Pèlerin et rencontrent un joli succès grâce à une programmation à la fois populaire et exigeante.

Cette programmation de films très récents, voire d’avant-premières, « Art et Essai », de films familiaux et de films d’animation, permet de créer un lieu d’animation bien prisé de la population locale et des villages voisins. En 2019, les séances ont touché 2.435 spectateurs et 1.376 en 2020, malgré la situation sanitaire délicate.

Le cinéma estival est une belle opportunité pour notre village. Peu de villages de cette taille disposent de cette activité. Mais l'association aurait besoin de bénévoles supplémentaires sur Lalouvesc pour sa bonne organisation. Un appel est lancé aux amateurs intéressés (tél 04 75 67 82 63).

#### L'association Cinéma Le Foyer

Les responsables de l'association Cinéma Le Foyer ont rencontré la Mairie au mois de mars. L'association a son siège principal à Bourg-Argental où elle est propriétaire du cinéma. Elle organise aussi des tournées dans des villages de haute Ardèche et des animations dans les écoles. Elle réunit 65 membres bénévoles dont 10 à Lalouvesc. A Lalouvesc, elle utilise la salle de l'Abri du pèlerin depuis 2008 qu'elle a entièrement rénovée et équipée grâce à ses fonds propres et à l'appui de la Mairie.

![](/media/cinema-le-foyer.jpg)

Cette année l'association a fait des demandes de subventions pour pouvoir recruter un animateur salarié, médiateur du cinéma pour l'ensemble de ses activités.

De son côté, le Conseil municipal a voté, pour la première fois, une subvention de 750 €, soit l'équivalent du forfait payé par l'association pour la location de la salle de l'Abri du pèlerin.

### Le camping a repris des couleurs

Quand les Louvetous décident de réagir... eh bien, ça se voit !

![](/media/minigolf-18-trous.jpg)

Hier le minigolf n'était plus qu'un terrain vague d'où émergeaient dispersées quelques plaques de ciment. Aujourd'hui les 18 pistes ont été nettoyées, dans quelques jours elles auront toutes pris des couleurs. Il ne restera plus qu'à installer des numéros, des socles pour les balles, à acheter des clubs et des balles, et Lalouvesc aura, de nouveau, le plus beau et le plus grand des minigolfs de haute-Ardèche. Un grand merci aux citoyennes et citoyens qui ont donné de leur temps pour arriver à ce magnifique résultat !

![](/media/journee-citoyenne-27-03-2021-9.jpg)

Hier le tennis, n'était plus qu'une friche misérable qui nous faisait honte. Plus de dix camions de terre ont déjà été évacués, plus de la moitié du terrain est dégagé. Il faudra encore un peu d'huile de coudes et quelques équipements, mais, pour sûr, nous aurons un terrain multi-sports tout neuf pour les enfants et les ados à l'ouverture du camping. Cela encore grâce à l'énergie et au dévouement de citoyens et de citoyennes de Lalouvesc, merci à eux !

Hier le réservoir d'eau qui alimente le village depuis le haut du camping n'était plus protégé. Demain, il sera de nouveau entouré d'un grillage, grâce à des citoyens louvetous. Merci à eux !

![](/media/journee-citoyenne-1-2021-23.jpg)

Hier plusieurs chalets menaçaient de tomber et les balustrades étaient pourries. C'étaient sans compter sur les citoyens professionnels du bois de Lalouvesc. Ils sont aujourd'hui d'aplomb. Merci à eux !

![](/media/journee-citoyenne-1-2021-19.jpg)

Hier les refuges étaient perdus dans un bois envahi de buissons et de ronces. Ce n'était plus que de misérables cabanons sales et dégradés. Grâce à tout un bataillon de citoyennes et de citoyens, le bois est nettoyé, bien des planches ont été remplacées, et un grand ménage a été réalisé dans chaque refuge.

Que dire de plus ? En faisant appel aux Louvetonnes et Louvetous, nous savions que nous ne serions pas déçus. Mais leur efficacité, et leur engagement, qui se poursuit encore après les deux jours de mobilisation officiels, nous a bluffé ! Lalouvesc sera relancé grâce à la volonté de ses citoyens. Personne ne peut plus en douter aujourd'hui.

### Lalouvesc.fr : un lancement réussi

Le nouveau site web de Lalouvesc (lalouvesc.fr) a été ouvert le 1er mars et, depuis le 17 mars, l'adresse lalouvesc.com renvoie automatiquement sur le nouveau site. Un message est envoyé à chaque abonné tous les lundis avec les actualités publiées au cours de la semaine précédente.

Rappelons que ce site se veut [exemplaire](/projets-avenir/changement-climatique/des-sites-web-a-faible-impact-environnemental/). Il doit être utile, peu coûteux, d'une grande accessibilité et d'un faible poids écologique. Il n'utilise aucun moyen de traçage ou de cookies. Il a été entièrement financé sur un fond européen et sert de démonstrateur pour l'équipe de designers qui l'a conçu en collaboration avec l'équipe municipale. Notre site fait maintenant partie de la liste des [projets à soutenir](https://mon.incubateur.anct.gouv.fr/processes/transformation-numerique/f/2/proposals/65?filter%5Bcategory_id%5D%5B%5D=&filter%5Bsearch_text%5D=plateaux&order=random) de l'incubateur des territoires de l'Agence nationale de la Cohésion des Territoires.

Le succès d'un site web se mesure principalement aux statistiques de fréquentation. Vous pouvez suivre les [statistiques de fréquentation en direct]() (le système de statistique choisi est, lui aussi, sans traçage des internautes). Voici ci-dessous, quelques chiffres et graphiques que nous en avons tirés.

#### Le capitaine Marleau crée une nouvelle fois la surprise

![](/media/satistiques-lalouvesc-fr-mars-2021-2.png)

Le graphique ci-dessus retrace l'évolution quotidienne de la fréquentation du site lalouvesc.fr au cours de son premier mois. La courbe du bas en bleu ciel représente le nombre de visites (chaque fois que quelqu'un est arrivé sur le site), la courbes du haut représente le nombre de pages vues (un visiteur peut consulter plusieurs pages).

On peut y remarquer plusieurs phénomènes. Tout d'abord un fort effet de curiosité puisque le premier jour de l'ouverture du site pas moins de 1.300 pages ont été consultées au cours de 191 visites. Ensuite, on observe des pics de fréquentation hebdomadaire qui correspondent à l'envoi du message aux abonnés et qui confirment qu'il y a bien une attente de leur part et qu'un outil d'informations locales est apprécié.

{{<grid>}}

{{<bloc>}}

Le 22 mars démarre un pic de fréquentation plus important. On le doit à la popularité du capitaine Marleau ! En effet, l'annonce de la prochaine programmation de l'épisode tourné à Lalouvesc a été reprise sur la page Facebook de l'Office du tourisme, elle-même relayée plus de 120 fois. Ainsi la page la plus consultée dans le mois de mars est, de très loin, celle du capitaine avec 1.300 vues, vient ensuite le Bulletin de février, consulté 346 fois.

{{</bloc>}}

{{<bloc>}}

![](/media/marleau-1.jpg)

{{</bloc>}}

{{</grid>}}

#### Un vrai succès

Avec plus de 13.000 pages vues dans le mois, le lancement du site lalouvesc.fr est un vrai succès et, au-delà des pics conjoncturels, le plus important est l'augmentation progressive de la fréquentation au cours du mois.

Pour se rendre compte du cap qui a été franchi avec le nouveau site lalouvesc.fr, il faut comparer sa fréquentation avec celle de l'ancien site lalouvesc.com.

![](/media/statistiques-comparaison-lalouvesc-com-lalouvesc-fr-2.png)

A droite sur le graphique, on retrouve la fréquentation de lalouvesc .fr au mois de mars 2021, à sa gauche sont présentées celles de lalouvesc.com au mois de mars de l'année dernière, au mois le plus fréquenté (juillet) ou encore au mois précédent la bascule sur le nouveau site (février). On constate, par exemple, que l'audience du site du village a été multipliée par cinq en une année, soit entre mars 2020 et mars 2021.

Tous ces éléments et les nombreux messages sympathiques d'encouragement que nous avons reçus nous confortent dans nos choix. Mais restons prudents et modestes. Le premier mois d'un site web est celui de la découverte. Attendons l'été pour pavoiser.

Nous sommes encore en rodage, n'hésitez pas à nous contacter si vous repérez des manques, des bugs ou simplement si vous voulez faire une suggestion.

La plateforme de réservation en ligne est prête, mais elle ne peut être opérationnelle tout de suite. Deux opérations doivent préalablement être réalisées : le regroupement des régies municipales (nous avons sept régies, ce n'est pas très raisonnable) ; l'ouverture d'un système de paiement en ligne qui demande quelques formalités administratives.

## Suivi

### Le comité des fêtes ne connaît pas la déprime !

«_Qui ne tente rien, n’a rien_»… Nous avons tenté nous n’avons rien eu !!! La Ronde louvetonne a été annulée par la Préfecture. Nous attendrons donc avril 2022 pour offrir au village une journée haute en couleur et en beauté.

Touchés mais pas coulés, les bénévoles du Comité des Fêtes sont tenaces. Hop nous rebondissons aussitôt sur le **TRAIL ÉPHÉMÈRE – ACTE 2.**

![](/media/trail-ephemere-3.jpg)

C’est ainsi que du 10 avril au 09 mai un nouveau parcours de 10 km sera balisé. Nous communiquons largement sur le strict respect des consignes sanitaires actuelles. Celles que nous connaissons tous depuis de longs mois et bien évidemment sur la dernière née qui interdit tout déplacement au-delà d’un rayon de 10 km depuis chez soi.

Sportifs aguerris, randonneurs, tous les amoureux de la nature et de notre région se voient ainsi proposer une belle opportunité de s’évader le temps de lutter contre la morosité qui tente de nous envahir mais que nous refusons.

LALOUVESC est un bel atout et un bon remède pour gagner en positivité. Profitons-en, suivons la rubalise posée par notre leader, Damien, et cheminons dans la forêt !

* Site [https://www.traildessapins-lalouvesc.fr/](https://www.traildessapins-lalouvesc.fr/ "https://www.traildessapins-lalouvesc.fr/")
* Facebook [https://www.facebook.com/traildessapins](https://www.facebook.com/traildessapins "https://www.facebook.com/traildessapins")

Agnès 07.66.67.94.59  
[comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)

### Les artistes du Carrefour des Arts

L'assemblée générale du Carrefour des Arts s'est tenue en 2021 pour la première fois par correspondance. La participation a été exemplaire. Nous remercions chaleureusement tous nos adhérents et donateurs.

![](/media/cac-3.jpg)

Le choix des artistes est bouclé. Nous en avons sélectionnés sept :

* Shasha SHAIKH, peintre batik,
* Suhail SHAIKH, sculpteur papier
* Xavier CARRERE, verrier
* Martine JAQUEMET, peintre
* Hervé THAREL, sculpteur
* Michel BEATRIX, poète
* Claude GREGOIRE , peintre

Voir plus de détail sur le site du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/).

L'équipe continue le travail sur l'aménagement des salles, la mise en scène des œuvres, et tous les travaux à réaliser avant l'ouverture. Nous vous présenterons bientôt l'affiche et le plan communication de l'édition 2021.

L'ouverture est prévue du dimanche 4 juillet au dimanche 22 août.

### Quelques actualités parues sur Lalouvesc.fr au mois de mars

Cette nouvelle rubrique rappelle quelques informations publiées sur le site au cours du mois de mars et qui restent d'actualité.

#### Journée citoyenne, épisode 2

27 mars 2021

Nettoyage du tennis, peinture sur le mini-golf, réparations et ménage des refuges... une journée productive [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/journee-citoyenne-episode-2/ "Lire Journée citoyenne, épisode 2")

#### Faites vos semis comme un pro

26 mars 2021

L’association Les Jardins du Haut Vivarais lance ses stages de printemps. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/faites-vos-semis-comme-un-pro/ "Lire Faites vos semis comme un pro")

#### Les jeunes de Lalouvesc à l'honneur

16 mars 2021

Aline passe à la radio, Kévin a reçu son diplôme [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/remise-de-diplome/ "Lire Les jeunes de Lalouvesc à l'honneur")

#### Emplois pour l'été et buvettes éphémères, appels

14 mars 2021

Le camping municipal développe son activité, Pour y faire face, la Mairie recrute deux postes à temps partiels et propose l'ouverture d'une buvette éphémère. Une seconde buvette est envisagée au parc du Val d'Or. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/buvettes-et-emplois-pour-l-ete-appels/ "Lire Emplois pour l'été et buvettes éphémères, appels")

#### Première journée citoyenne 2021, premiers résultats

13 mars 2021

Il ne faisait pas chaud, samedi 13 mars au camping, et pourtant une trentaine de Louvetonnes et Louvetous ont répondu présents, dont les Pères Olivier et Michel du Sanctuaire. Et à la fin de la journée, le résultat fut impressionnant ! [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/premiere-journee-citoyenne-2021-premiers-resultats/ "Lire Première journée citoyenne 2021, premiers résultats")

#### L'Ardéchoise, malgré tout !

10 mars 2021

Les 18 et 19 juin les cyclistes pourront bénéficier de toutes les animations habituelles sur l’Ardéchoise classique et l’Ardèche Verte. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/l-ardechoise-malgre-tout/ "Lire L'Ardéchoise, malgré tout !")

#### Réduire les incivilités

8 mars 2021

Rien ne sert d'embellir, si par ailleurs, on salit faute d'une discipline minimale. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/reduire-les-incivilites/ "Lire Réduire les incivilités")

#### Le Père Philippe Hermelin nous a quitté

2 mars 2021

Nous venons d'apprendre avec tristesse le décès du Père Philippe Hermelin qui a été Recteur du pèlerinage à Lavouvesc entre 1987 et 1999. [Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/le-pere-philippe-hermelin-nous-a-quitte/ "Lire Le Père Philippe Hermelin nous a quitté")

## Coloriage

Vous n'avez pas l'occasion de vous déplacer pour repeindre le minigolf, mais vous voudriez néanmoins l'admirer en couleur, ou même en choisir vous-mêmes la couleur. La municipalité ne reculant devant aucun sacrifice, vous donne ci-dessous l'occasion de repeindre quelques pistes encore en chantier à votre goût :

* [Mini-golf Piste 2](http://scrapcoloring.fr/coloriage-photos-et-dessins?image=Minigolf_2.1617351872)
* [Mini-golf Piste 6](http://scrapcoloring.fr/coloriage-photos-et-dessins?image=Minigolf_6.1617352042)
* [Mini-golf Piste 8](http://scrapcoloring.fr/coloriage-photos-et-dessins?image=Minigolf_8.1617352155)
* [Mini-golf Piste 9](http://scrapcoloring.fr/coloriage-photos-et-dessins?image=Minigolf_9.1617352215)
* [Mini-golf Piste 14](http://scrapcoloring.fr/coloriage-photos-et-dessins?image=Minigolf_14.1617352290)