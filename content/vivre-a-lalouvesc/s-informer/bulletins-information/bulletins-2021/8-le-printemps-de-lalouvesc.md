+++
date = 2021-02-28T23:00:00Z
description = ""
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "Mars 2021"
title = "8 - Le printemps de Lalouvesc"
weight = 10

+++
Nous sommes encore en hiver, mais il semblerait que Lalouvesc est en avance. Dans le village, c'est déjà le printemps ! De nombreuses transactions immobilières ont été signées et les nouveaux arrivants ont d'intéressants projets pour le village. Le camping se transforme avec des hébergements de qualité, des réservations en ligne et, bientôt, grâce à l'énergie des Louvetous, sa rénovation au cours des journées citoyennes. Un site web complètement renouvelé et exemplaire rajeunit l’image du village et, par un effet de miroir, renforce les liens à l’intérieur et avec l’extérieur. Le pari est que nous récoltions tous, dès cet été, les fruits de nos efforts de ce printemps précoce.

Mais nous n’oublions pas non plus que, ici comme ailleurs, la pandémie a frappé durement les plus fragiles, tout particulièrement à l’Ehpad, malgré le dévouement remarquable du personnel.

Voilà quelques-uns des sujets développés dans ce bulletin bien fourni.

> Ce Bulletin est aussi le vôtre. Il accueille les articles ou informations proposés par les associations ou les lecteurs attachés au village. N’hésitez pas à envoyer vos remarques, textes, informations, documents, photos, clins d’œil à l’adresse de la Mairie : mairie@lalouvesc.fr, vous contribuerez ainsi activement au bien-être du village.
>
> Si vous disposez d’une imprimante, vous pouvez imprimer chez vous un exemplaire papier grâce à ce [fichier pdf (cliquer)](/media/bulletin-de-lalouvesc-n-8-1er-mars-2021.pdf) et le passer à ceux qui ne disposent pas d’une connexion internet. Mais la version papier sera toujours moins riche que la version en ligne qui comprend des liens, des diaporamas et des vidéos.

## Le mot du Maire

Bonjour à toutes et à tous,

Ces beaux jours nous donnent de l'enthousiasme et nous motivent pour préparer notre saison estivale avec ferveur, n'oublions pourtant pas les contraintes sanitaires. Gardons la tête froide, agissons mais en suivant les consignes de prudence !

Notre sujet principal, c'est le camping. Remaniement de l'implantation des mobile-homes, extension du parc locatif par l'ajout d'un cottage normalisé avec accès handicapé et d'une grande tente à structure bois pour 4/6 personnes, réparation des chalets et cabanes perchées. Pour nous aider dans ces travaux de remise en état, longs et prenants, nous organisons des journées citoyennes, vous pourrez en prendre connaissance sur notre site, dans ce bulletin mensuel ainsi que sur des affichettes chez nos commerçants. Je vous demande de venir nombreux, quelles que soient vos compétences, il y aura toujours quelque chose à faire qui apportera une plus-value au bien commun. Je sais que je peux compter sur vous, vous l'avez déjà prouvé à plusieurs reprises et je vous remercie par avance pour votre contribution. La réfection et le développement du camping nous permettra d'augmenter son chiffre d'affaires et par conséquent, ramènera à la Commune, c'est-à-dire à nous tous, de l'argent supplémentaire. Mais, bien sûr, il faudra rester très prudent en matière sanitaire et bien respecter les obligations que nous connaissons tous : distanciation, port du masque, nettoyage au gel hydroalcoolique.

Pour ceux qui sont pénalisés par l'éclairage public, plusieurs réverbères ne fonctionnant pas, sachez que j'ai écrit à l'entreprise pour leur signifier mon mécontentement, et leur demander d'intervenir très rapidement pour les remettre en état, voir changer les lampes.

Nous venons de clôturer nos comptes 2020, nous sommes en pleine élaboration du compte primitif 2021, nous avons mis en avant nos perspectives de développement. Nous ne manquerons pas de vous tenir informés par ce bulletin de l'avancée de nos projets au fur et à mesure de leur déroulement.

Je vous souhaite à toutes et à tous un bon début de saison estivale, profitez bien de ce printemps, mais restez prudents, nous sommes toujours vulnérables face à la maladie, il faut continuer à se protéger.

Lalouvesc un village qui a du caractère, illustré ce mois par des travaux collectifs au camping avec tous ses habitants qui n'en n'ont pas moins....

Amitiés à toutes et tous

Jacques Burriez, Maire de Lalouvesc

## Actualité de la Mairie

### Travaux de voirie

Des trous ont été creusés dans les rues du village. Le premier devant le magasin Vival pour réparer une importante fuite d'eau suite à la rupture d'une canalisation. Le second au début de la route de Satillieu, pour répondre à un effondrement du trottoir dû à une ancienne cave dont le comblement n'avait pas été correctement réalisé. Tout est maintenant rentré dans l'ordre.

Cet été le Département va refaire les routes à l'entrée du village :

* la 532 depuis la pancarte du village jusqu'à la route de Pailharès (3,5 km) ;
* la 518A vers Satillieu, après Milagro sur 2,5 km;
* la D214 vers Rochepaule sur 3,5km jusqu'au col des Grands, la partie à l'intérieur du village est laissée provisoirement en attente jusqu'à la fin des travaux d'assainissements prévus sur cette portion.

Le calendrier précis des travaux sera communiqué prochainement.

### De nouveaux hébergements au camping

Comme prévu la Mairie a acheté deux nouveaux équipements de qualité pour le camping. Leur installation est prévue pour une ouverture début juin.

#### Un cottage PMR

![](/media/camping-pmr-2.jpg)©Céline Vautey

Un nouveau cottage, accessible aux personnes à mobilité réduite, sera installé à l'entrée du camping. 35m2, deux chambres PMR, 4 à 6 personnes tout équipé, faisant du camping de Lalouvesc un des rares en Ardèche à s'ouvrir à cette catégorie de la population.

![](/media/camping-pmr-3.jpg)

#### Une tente Lodge chauffée

![](/media/tente-lodge.jpg)

Cinq places, deux chambres, sur plancher bois, poêle à bois, entièrement équipé, coin cuisine sur la terrasse de 10m2.  
Cette tente de luxe sera placée en hauteur dans le camping, au-dessus des chalets.

### Plateforme de réservation

Grâce aux judicieux conseils de Séverine Moulin de l'OT, nous avons pu adhérer à la plateforme de réservation en ligne de l'Agence de tourisme (A.T.) de l'Ardèche. Nous bénéficierons ainsi de nombreux avantages. Les réservations et les paiements pour le camping se feront désormais en ligne, dégageant ainsi un temps de gestion précieux pour le personnel communal. Nous rejoignons ainsi les pratiques normales des vacanciers préparant aujourd'hui leurs séjours. Notre offre sera aussi présente sur la plateforme très consultée de l'AT. Et, cerise sur le gâteau, cette année le service est gratuit, inclus dans le plan de relance de la Région (l'année prochaine le coût annuel de l'abonnement à la plateforme nous reviendra à environ 500 €). Bien sûr le téléphone sera toujours là pour renseigner les demandeurs qui souhaitent des renseignements.

Une telle plateforme ne se paramètre pas d'un claquement de doigt. Régine Vigouroux de l'AT a passé une journée entière à nous former à ce service. Il reste encore à entrer toutes les données de descriptions de nos différentes offres. Aurélie Desbos a réactualisé les photos. Gladys Durieux a déjà entré les tarifs et Pascal Vayssié a devancé l'appel aux journées citoyennes et c'est lui qui entrera les descriptions et photos de locations dans la plateforme. Merci à toutes et tous !

Une fois les derniers réglages terminés et vérifiés, les réservations seront directement accessibles sur le nouveau site web, sans doute vers la fin du mois de mars.

### Fleurir le village

Un Comité de fleurissement est en cours de constitution. toutes les personnes intéressées peuvent s'inscrire au secrétariat de la mairie, si possible avant le 30 mars.  
Le Comité de Fleurissement sera chargé de **mettre en valeur le petit patrimoine local et de fleurir notre village**.

Nous souhaitons que effort d’embellissement initié par notre équipe soit porté par des habitants motivés.

## Zoom

### Bienvenue aux nouveaux arrivants !

Qui a dit que Lalouvesc était un village rural en déshérence ? Depuis notre arrivée à la Commune, nous faisons un constat inverse : Lalouvesc attire. Un signe qui ne trompe pas : le nombre de transactions immobilières. Nicole Porte, agent immobilier bien connue des Louvetous, nous indiquait récemment qu'elle n'avait presque plus d'offres en portefeuille !

Les nouveaux arrivants ne sont pas simplement de passage. Ils ont des projets pour le développement du village. Voici, parmi d'autres, trois témoignages que nous avons recueillis pour ce bulletin.

* Stéphane Porras, adjudant-chef à la gendarmerie à Satillieu a acheté avec sa femme le Bar du Lac ;
* Eric Dalverny, hôtelier, a acheté le Relais du Monarque ;
* Philippe Bosso, architecte, a acheté plusieurs maisons rue de la Fontaine.

**Pourquoi avoir acheté un commerce ou une maison à Lalouvesc ?**

S. Porras : Avec mon épouse nous avons été séduits par l'exposition du village, le cadre est très joli, c'est un village de montagne très typique, près de la ville avec le soleil et la neige l'hiver. Un super coin de nature.

E. Dalverny : Je suis ardéchois, né à Annonay. Tout petit avec mes parents nous venions à Lalouvesc, c'est le côté affectif et spirituel qui nous lie à ce village. C'est un lieu naturel privilégié, un environnement vert enclin aux promenades, nous souhaitons au travers de notre acquisition faire partager aux personnes que nous accueillerons cette richesse naturelle.

![](/media/relais-du-monarque.jpg)

Ph. Bosso : Tout d'abord parce que j'y ai retrouvé un vieux copain, le docteur Didier Chieze. Il m'a fait venir chez lui et j'ai découvert un village splendide où vivent des gens formidables. Du coup je suis revenu plusieurs fois, ce qui m'a donné envie de participer à la vie du village. De par mes fonctions d'architecte j'ai travaillé sur un dossier pour la mairie. Mes fréquentes visites m'ont donné envie d'acheter un bien dans le village et développer des actions diverses pendant toute l'année. Des actions en lien avec les habitants, essentiellement culturelles et si possible urbanistiques et architecturales de par mon métier. Mon fils veut également s'installer au village et y développer des stages de théâtre,

**Comptez-vous, vous y installer définitivement ou venir de temps en temps ?**

S.P. : C'est une installation définitive nous allons faire de gros frais de rénovation dans le bar restaurant. Au-dessus nous comptons faire un appartement destiné à la location.

![](/media/bar-du-lac-lalouvesc.jpg)

E.D. : Je compte y venir le plus souvent possible, nous habitons dans la Drôme des collines, de par l'importance du bâtiment nous serons souvent au village, puis suivant l'évolution des choses, à ce moment-là, nous prendrons les dispositions nécessaires.

Ph.B. : Nous comptons venir de temps en temps, nous vivons dans la banlieue parisienne, nous viendrons au moment de nos actions programmées et le plus possible. Nous ne pouvons pas abandonner notre vie parisienne d'un coup, du moins pour le moment.

**Comptez-vous vous investir au sein du village dans les associations comme bénévole ?**

S.P. : Nous comptons regarder un petit peu ce qu'il y a comme associations, nous verrons, nous pensons que notre activité professionnelle nous prendra beaucoup de temps, mais nous pensons pouvoir nous investir dans la vie du village.

E.D. : Oui et non, d'une certaine façon, nous avons déjà pris contact avec les pères jésuites pour nous impliquer dans des actions religieuses liées à la vie du village.

Ph.B. : Bien sûr je compte m'investir dans les associations, les associations culturelles à titre privé et également dans les autres associations comme bénévole pour aider l'ensemble des acteurs associatifs. Nous souhaitons également développer des activités diverses et variées en investissant les jeunes du village.

**Que comptez-vous voir se développer au sein du village ?**

S.P. : Le village semble dynamique, il faut développer la partie sportive, le VTT, le ski de fond l'hiver si l'enneigement le permet.

E.D. : Nous souhaiterions voir se développer les activités liées principalement à la vie du village, les activités vertes, en rapport avec l'environnement, toutes les activités naturelles. Nous souhaitons voir se développer des commerces, déjà bien implantés dans le village. Assurer le maintien d'un développement sûr et durable nécessaire à l'évolution du village.

Ph.B. : La 5G ! L'Internet haut débit (fibre) ! Le maximum de confort numérique, ensuite tout ce qui a un attrait au développement de l'environnement naturel, à ce sujet nous comptons mener des actions dans des sites campagnards. Mon fils très investi dans le monde théâtral veut développer des spectacles natures sur différents thèmes reliés à l'environnement rural naturel.

### L'Ehpad en temps de pandémie

![](/media/ehpad-lalouvesc.jpg)

A l’Ehpad, tout le monde est vacciné contre la Covid depuis la mi-janvier mais la deuxième vague de la pandémie a été cruelle avec plusieurs décès. Nous pensons aux familles et aux proches et leur envoyons toute notre sympathie.

Nous pensons aussi à tous les employés de la maison de retraite qui, tous à leur poste, ont dû et doivent encore tenir dans des conditions très difficiles physiquement et moralement. Nous leur disons toute notre admiration et nous nous associons aux remerciements que nous avons reçus d’une famille et que nous leur transmettons :

"Nous tenons particulièrement à remercier le personnel de la maison de retraite le Balcon des Alpes pour son implication et attention apportées tant à notre mère atteinte du Covid qu'à nous-mêmes lors de nos visites. Nous avons ressenti un profond soutien pendant cette période pourtant difficile pour tous.

Encore merci aux aides-soignantes, infirmières, cadre de santé, animatrice et secrétaire."

Même si la vaccination avance, la pandémie n’est pas terminée et il ne faut surtout pas relâcher notre vigilance et notre application des consignes sanitaires. On trouvera ci-dessous l’évolution du nombre d’hospitalisations dues à la Covid dans le département (les chiffres ne sont pas absolus, mais relatifs pour permettre une comparaison avec l'ensemble de la France). On observe clairement la deuxième vague, suivie d’un rebond, puis d’une chute brutale, sans doute due aux effets conjugués du couvre-feu et des premières vaccinations. Il ne tient qu’à nous que cette tendance se poursuive.

![](/media/evolution-covid-28-02-2021.png)

Source : [https://geodes.santepubliquefrance.fr](https://geodes.santepubliquefrance.fr "https://geodes.santepubliquefrance.fr")

### Un site web tout neuf

Lalouvesc a été parmi les premières Communes de sa catégorie à disposer d'un site web digne de ce nom lalouvesc.com. Ce site a été un atout très important pour la promotion et l'attractivité du village. Nous devons tous à Matthieu Fraysse une reconnaissance à la hauteur des efforts qu'il a fourni bénévolement, des compétences et du talent qu'il a mis (et qu'il met encore) pour la conception et l'entretien pendant de nombreuses années de ce site et plus généralement de l'animation du numérique louvetou par la réalisation de vidéos et l'animation des réseaux sociaux. Un très grand merci à lui !

Il ne pouvait plus, depuis déjà quelques années, actualiser le site lalouvesc.com, c'est pourquoi nous avons cherché quelle serait la meilleure solution de remplacement. Une opportunité s'est ouverte à nous grâce à un collectif de designers "Plateaux numériques" qui cherchait un village pour réaliser le projet européen dont ils avaient la charge : réaliser un modèle de site web à faible impact environnemental pour les petites communes.

Contrairement à des idées reçues l['impact environnemental du numérique](https://fr.wikipedia.org/wiki/Impact_environnemental_du_num%C3%A9rique) est important. Sans entrer dans trop de considérations techniques, un site web conscient de cet enjeu doit être robuste dans le temps, c'est-à-dire être fondé sur des techniques simples aussi bien pour son alimentation que sa consultation : pages statiques qui ne sont pas recalculées à chaque clic, peu de vidéos, pas de cartes dynamiques, images compressées, emojis, pas de cookies, accessibles même avec des équipements anciens, etc. C'est dans cet esprit que nous avons travaillé avec les designers.

![](/media/nouveau-site-lalouvesc-fr-2.jpg)

Nous sommes fiers de vous présenter maintenant le nouveau site du village qui sera accessible par l'adresse lalouvesc.fr (le .com ayant plutôt vocation à abriter des sites commerciaux). Nous espérons qu'il vous plaira, qu'il vous rendra service et qu'il renforcera l’attractivité du village.

Nous sommes encore en période de rodage, celle-ci est prévue pour durer jusqu'à la fin du mois de mars, cela signifie que :

* La partie "annuaire" (associations, commerces hébergements) est encore provisoire. Elle sera, à l'avenir, couplée avec la base Apidae de l'agence départementale du tourisme de l'Ardèche. L'OT et la mairie vont collaborer pour actualiser la base afin qu'elle soit opérationnelle pour la saison estivale.
* Il existe sûrement encore des erreurs et des coquilles. Soyez indulgents et n'hésitez pas à nous les signaler. Nous les corrigerons.
* Pendant quelques semaines les sites lalouvesc.fr et lalouvesc.com vont cohabiter, puis l'adresse lalouvesc.com renverra automatiquement et de façon transparente sur le nouveau site lalouvesc.fr.
* Si vous étiez abonnés aux actualités du site lalouvesc.com, votre abonnement est automatiquement transféré sur celles de lalouvesc.fr.

#### Participez à l'amélioration du site

Nous avons besoin de vous pour évaluer et améliorer la consultation du site. Il est fait pour vous et pour qu'il vous soit vraiment utile, il est important d'avoir votre retour afin de corriger les défauts que nous n'aurions pas repérés.

Pour pouvoir recueillir votre avis, nous vous proposons de répondre à quelques questions sur [ce formulaire](https://framaforms.org/lalouvescfr-tests-utilisateurs-1614354330). Plus vous serez nombreux à répondre, meilleure sera la consultation future du site. Parallèlement, certains d'entre vous seront sollicités pour venir à la mairie réaliser quelques exercices de navigation que nous enregistrerons.

Une fois le rodage terminé et les derniers réglages effectués, le site lalouvesc.fr servira donc aussi de démonstrateur pour les designers engagés dans ces développements. Il a déjà retenu l'attention du Ministère de la Cohésion des territoires.

La conception et la réalisation de ce projet fut un long travail. Il nous faut remercier Gauthier Roussilhe et Timothée Goguely, les designers, qui nous ont permis de profiter de [leur projet européen](projets-avenir/changement-climatique/des-sites-web-a-faible-impact-environnemental/), sans coût pour la Commune. Outre la réalisation du site et la promotion qu'il offre au village, cela nous a permis aussi de rattraper nombre de retards accumulés dans l'actualisation des informations, leur mise en cohérence avec nos partenaires (l'OT, l'ADT, le sanctuaire...) et l'ouverture de nouveaux services comme la plateforme de réservations.

## Suivi

### Les journées citoyennes

![](/media/chantiers-journees-citoyennes.jpg)

Une première rencontre avec les chefs de chantier bénévoles s'est tenue au camping samedi 27 février. Elle a permis de faire le point sur les (nombreux) travaux à effectuer et de préciser l'organisation des journées. En voici les principes :

#### Un calendrier moins contraint

Nous avions prévu les premières journées le week-end du 6 et 7 mars. Le délai paraît trop court pour que les chefs de chantiers précisent le détail des réparations à effectuer et donc les matériaux à réunir pour leur réalisation et aussi pour constituer les équipes de bénévoles. Nous avons donc décidé de décaler ces premières journées d'une semaine. Elles se tiendront les 13 et 14 mars.

Par ailleurs, s'en tenir à des week-ends fixes a paru trop contraignant. Selon les types de travaux et les équipes, des moments de travaux pendant la semaine pourront être organisés à l'initiative des chefs de chantiers.

#### Une charte sur les précautions sanitaires

La période particulière dans laquelle nous nous trouvons nous oblige à la prudence. En concertation avec la Préfecture, une charte sera signée par tous les participants aux journées pour qu'ils s'engagent à respecter les mesures sanitaires.

Les travaux se feront par groupes séparés de six personnes maximum et des masques et du gel hydroalcoolique seront à disposition.

#### Une inscription indispensable

Pour que ces journées puissent s'organiser sereinement et efficacement, il est **INDISPENSABLE de vous inscrire** avant le 11 mars, soit [en ligne](https://docs.google.com/forms/d/e/1FAIpQLSffAQE20gzFLi55Lp_YDhu1oJZji9dF2GTguOOC4sWlu1BHsw/viewform), soit à la Mairie.

### Un trail éphémère... et fréquenté !

Peut-on dire que **_pour un coup d’essai c’est un coup de maître ?_** D’une part nous n’en aurions pas la prétention et d’autre part nous n’avons pas la possibilité de jauger précisément la fréquentation de ce parcours. Mais force est de constater que tant la version trail que la version rando ont attiré de nombreux sportifs et randonneurs. Sur la période nous avons bénéficié d’une météo tantôt hivernale tantôt printanière. C’est un atout supplémentaire pour satisfaire le plus grand nombre.

Encouragés par les retours que nous souhaitons vous partager, nous renouvellerons l’expérience pour les vacances de printemps avec un parcours différent et un peu plus long.

Agnès

{{<grid>}}

{{<bloc>}}

![](/media/trail-ephemere-4.jpg)

{{</bloc>}}
{{<bloc>}}

**Séquence « _on en veut encore_ »  
Maxime** Bonjour, une photo de nous cette après-midi (Eline, Davy, Marie et Maxime). Merci pour ce trail éphémère. J'espère qu'il y en aura d'autres! Le principe est génial!!  
**RunSept** On remet ça dimanche prochain. On verra ce que ça donne sans neige !\[😄\]  
**Ludovic** _Super idée ! Je ferai 2 voire 3 tours !!!_
{{</bloc>}}

{{<bloc>}}

**Séquence « _émotion_ »  
Thierry 2** _Bonjour, hier mercredi j'ai fait mon comeback sur le parcours avec mon fiston. La montée derrière le cimetière était bien à l'ombre donc neige et glace au programme, mais malgré cela et surtout sans "sortie de route" ce coup-ci, j'ai pulvérisé mon 1er chrono de presque 6mn ! Bien mieux pour le balisage, aucune hésitation, alors encore BRAVO pour le suivi et le travail accompli. A la prochaine !\[😉\] Pour info, je suis atteint depuis 5 ans maintenant d’une polyneuropathie démyélinisante associée à une gamapathie monoclonale. C'est une maladie dégénérative de mon système nerveux périphérique, membres inférieurs et supérieurs. Donc pour lutter contre cette dégénérescence je dois pratiquer beaucoup d'activités physiques, VTT, trail running… en mode"handisport" malheureusement pas reconnu par la Fédération Française d' Ahlétisme… Mon grand plaisir est de pouvoir encore participer à ce genre d'événement grâce à vous… et pour cela je vous en remercie vivement et vraiment sincèrement. À très bientôt, très "handi"sportivement..._

{{</bloc>}}

{{<bloc>}}

![](/media/trail-ephemere-1.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/trail-ephemere-2.jpg)

{{</bloc>}}

{{<bloc>}}

**Séquence « _top chrono_ »  
Thierry 1** Cool, merci, oui première partie très glissante et technique, du coup il faudra revenir pour refaire péter mon chrono !! ... toujours avec beaucoup de plaisir !\[😁\]!\[😉\]  
**Sandrine** _Bonjour. Nous avons fait le parcours ce matin avec une amie... Merci pour le tracé GPX et le balisage au top... On en a ch**, on n’est pas des traileuses... Mais fières de nous quand même !_

{{</bloc>}}

{{<bloc>}}

**Séquence « _randonnée_ »  
Lucie** Nous l'avons fait en rando aujourd'hui avec les enfants.!\[👍\]  
**Aline** Même de Saint Victor nous sommes montés faire la rando. Au top  
_**Gégé** Super bon parcours… trop bien ce matin… merci aux organisateurs !\[👍\]!\[👍\]!\[🌲\]!\[🌲\]!\[🏃‍♀️\]!\[🏃‍♂️\] Merci à vous toutes et tous au Trail des Sapins – Lalouvesc de nous permettre de vivre des expériences comme celle-ci en ce moment ! Je me suis vraiment amusé sur vos chemins, le parcours est très sympa et le balisage au top ! Et oui j’ai raté une rubalise dans la précipitation en montée !\[😅\] et malgré ma reco du parcours en vélo juste avant, il faut que j’apprenne à lever la tête dans les montées et ça ira mieux !\[🙃\] ! Je reviendrai jouer si possible avant début mars. Bonne course à toutes et tous ! Amusez-vous bien !\[🤘\] !\[🌲\]!\[🎃\]

{{</bloc>}}

{{<bloc>}}

![](/media/trail-ephemere-3.jpg)

{{</bloc>}}

{{</grid>}}

**Séquence « _bravo & merci_ »**

**Sandrine** Bravo à vous pour ce Trail éphémère 2021 !!! Dans un décor magnifique de carte postale pour la Saint Valentin. L’intelligence de savoir s’adapter même en ces périodes compliquées. Ballade en famille et trail... Au top !\[👌\] Encore merci pour le parcours on a adoré !\[🥰\]  
**Mathieu**  Super sympa votre petit trail éphémère ! Merci !\[👍\]  
**Cyrille**  !\[💪\]!\[👏\] _Bravo, belle organisation et superbe initiative. Plein de monde qui découvre, redécouvre ou approfondit la découverte de notre belle Commune. En plus aujourd'hui le beau temps est au rendez-vous._ Un [**_Comité des fêtes de Lalouvesc - 07_**](https://www.facebook.com/comitedesfeteslalouvesc/) au TOP !  
**Benjamin** C'est chouette ce que vous faites ! Merci pour les coureurs. Bravo l'organisation !\[❤️\]  
**Lydia** _Super merci de ne rien lâcher._

### La Ronde Louvetonne

![](/media/ronde-louvetonne-2021-affiche.jpg)

Le règlement et le bulletin d'inscription sont [accessibles ici](/media/ronde-louvetonne-2021-bulletin_inscription_2_3.jpg).

## Cache-cache (jeu)

Les photos et images ci-dessous illustrent des articles du nouveau site lalouvesc.fr. Saurez-vous retrouvez les pages où elles se trouvent ?

{{<grid>}}

{{<bloc>}}

![](/media/ferme-crouzet-2.JPG)

{{</bloc>}}

{{<bloc>}}

![](/media/plan-jeu-monument.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/pompiers-lalouvesc-3.JPG)

{{</bloc>}}

{{<bloc>}}

1. ![](/media/parc-des-pelerins-2.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/la-louvesq-cassini.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/fustes-couette-1.jpeg)

{{</bloc>}}

{{<bloc>}}

![](/media/sylvie.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/lyre-louvetonne-17-juillet-2020.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/brocante-2.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/vente-lalouvesc-etalab.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/carrefour-des-arts-ewa-karpinska.jpg)

{{</bloc>}}

{{</grid>}}