+++
date = 2021-03-15T23:00:00Z
description = "Même si c'est le premier jour du printemps, la météo ne s'annonce pas assez clémente samedi prochain (20 mars) pour autoriser les travaux de peinture prévus sur le camping."
header = "/media/journee-citoyenne-1-2021-32.jpg"
icon = ""
subtitle = ""
title = "Prochaine journée citoyenne : le samedi 27 mars"
weight = 1

+++
Même si c'est le premier jour du printemps, la météo ne s'annonce pas assez clémente samedi prochain (20 mars) pour autoriser les travaux de peinture prévus sur le camping. Il nous faut, par exemple, plusieurs jours de temps sec pour réaliser les joints en ciment et, ensuite, pouvoir peindre le ciment des pistes du mini-golf .

Nous avons donc décidé de reporter la journée citoyenne d'une semaine, soit le samedi 27 mars. Il est prévu que le soleil brille la semaine prochaine. Mais c'est le dernier samedi libre avant les vacances de Pâques. Il faudra mettre les bouchées doubles pour pouvoir boucler tout ce que nous avons prévu, pour redonner son éclat au camping et retrouver les services et animations oubliés.

Réservez la date et venez nombreux, avec votre énergie et votre bonne humeur ! Il est possible que la presse soit présente, c'est aussi une très bonne occasion de montrer que Lalouvesc a vraiment du caractère et de faire la promotion de notre village.

Si vous en doutez encore, regardez donc la [vidéo tournée](https://youtu.be/XAsf-qFauNU) par Gladys Durieux le 13 mars dernier.