+++
date = 2023-01-05T23:00:00Z
description = "Vous êtes tous invités..."
header = "/media/voeux-2023-lalouvesc.png"
icon = ""
subtitle = ""
title = "Voeux du Maire, dimanche 15 janvier 17h30 au CAC"
weight = 1

+++
Venez partager la galette et discuter des projets de la Commune aux vœux du Maire le dimanche 15 janvier à 17h30 au CAC.

![](/media/voeux-du-maire-2023.jpg)