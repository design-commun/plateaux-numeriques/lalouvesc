+++
date = 2022-05-31T22:00:00Z
description = "Heures de passage et routes privatisées"
header = "/media/ardechoise-6.jpg"
icon = ""
subtitle = ""
title = "Conditions de circulation pour l'Ardéchoise"
weight = 1

+++
Les conditions de stationnement et de circulation à l'intérieur du village ont été [présentées dans le Bulletin de juin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#lard%c3%a9choise-bicycle-et-recycle).

De son côté, l'organisation de l'Ardéchoise va distribuer dans toutes les boîtes aux lettres des riverains un courrier précisant les conséquence de l'épreuve sur les conditions de circulation. Vous pouvez le [consulter en ligne](/media/lettre-riverains-ardechoise-zone-privatisee-2022.pdf).  
Ci-dessous un rappel des principaux éléments :

## Heures de passage des cyclistes dans les communes

| COMMUNES |  | HEURES DE PASSAGE |  |
| --- | --- | --- | --- |
|  | mercredi | vendredi | samedi |
| PAILHARES | de 6h45 à 10h15 |  | de 7h40 à 20h10 |
| NOZIERES | de 7h10 à 11h30 |  | de 8h à 10h15 |
| LAMASTRE | de 7h25 à 12h |  | de 8h10 à 10h40 |
| ST-BASILE | de 7h35 à 12h15 |  | de 8h15 à 10h50 |
| ST-PRIX | de 7h40 à 15h |  | de 8h20 à 11h |
| LES NONIERES - BELSENTES | de 7h55 à 15h |  | de 8h30 à 11h15 |
| ST-CIERGE-SOUS-LE-CHEYLARD | de 8h à 15h30 |  | de 8h40 à 11h35 |
| LE CHEYLARD | de 8h20 à 16h |  | de 8h45 à 11h50 |
| ST-JULIEN-D'INTRES |  | de 7h15 à 17h |  |
| ST-MARTIN-DE-VALAMAS | de 12h15 à 20h |  | de 6h30 à 16h30 |
| ST-AGREVE |  | de 7h50 à 17h30 |  |
| ROCHEPAULE |  | de 8h10 à 18h40 |  |
| LALOUVESC | de 10h20 à 18h30 |  | de 9h30 à 19h45 |
| LAFARRE |  | de 9h40 à 19h50 |  |

## Horaires des routes privatisées le samedi 18 juin

* D273 de Saint-Félicien au Col du Buisson (soit 12 km) de 6h30 à 20h30* dans les 2 sens
* D236 du Col du Buisson à Lamastre (soit 14 km) de 6h30 à 12h*
* D578 de Lamastre au Cheylard (soit 20 km) de 7h à 13h*
* D120 de Saint-Martin-de-Valamas (caserne du SDIS) à Saint-Agrève carrefour D120/D21 (soit 15 km) de 8h30 à 18h*
* D214 du carrefour D214/D9 lieu-dit Freydaparet à l’entrée de Lalouvesc (soit 24 km) de 8h30 à 20h30*
* D532 de Lalouvesc jusqu’à l’intersection D532/D236 (soit 3 km) de 8h30 à 20h30*
* D236 de l’intersection D532/D236 jusqu’au Col du Buisson D236/D273 (soit 9 km) de 8h30 à 20h30*

\* Heure de fin de privatisation théorique, suite au passage du véhicule de la Gendarmerie.

![](/media/carte-ardechoise-2022.jpg)