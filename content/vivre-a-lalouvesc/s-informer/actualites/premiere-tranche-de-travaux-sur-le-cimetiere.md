+++
date = 2021-06-16T22:00:00Z
description = "Le mur de soutènement se répare, deux mètres par deux mètres"
header = "/media/debuts-travaux-cimetiere-2.jpg"
icon = ""
subtitle = ""
title = "Première tranche de travaux sur le cimetière"
weight = 1

+++
La première tranche de travaux sur le cimetière a démarré. Comme prévu, il fallait démonter le mur sur quelques mètres, l'étayer, ferrailler profond dans le sol pour éviter toute bascule future, prévoir le drainage avec galets et drains, reconstruire le mur en moellons en attendant l'habillage en pierre... puis laisser sécher avant le démarrage de la tranche suivante.

![](/media/debuts-travaux-cimetiere-1.jpg)

Il faudra encore quelques mois avant que le cimetière retrouve sa fière allure.