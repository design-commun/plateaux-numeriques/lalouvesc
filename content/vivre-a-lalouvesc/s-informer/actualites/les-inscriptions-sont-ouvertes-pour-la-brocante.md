+++
date = 2022-06-26T22:00:00Z
description = "C'est le 4 septembre, il est prudent de réserver dès maintenant"
header = "/media/brocante-4.jpg"
icon = ""
subtitle = "C'est le 4 septembre, il est prudent de réserver dès maintenant"
title = "Les inscriptions sont ouvertes pour la brocante"
weight = 1

+++
Téléchargez le [bulletin d'inscription](/media/bulletin_inscription_brocante-2022.pdf).

Il vous appartient de l’imprimer, le remplir et le retourner dans les meilleurs délais accompagné de votre règlement. Auparavant nous vous incitons à prendre connaissance du règlement dans son intégralité.

**Comme l’année dernière l’inscription au préalable est obligatoire. Aucune inscription, aucun placement ne seront possibles le jour même. Les emplacements sont attribués dans l'ordre d'arrivée des bulletins.**

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès, Secrétaire, 07.66.67.94.59