+++
date = 2021-11-13T23:00:00Z
description = "Hospitalisations en hausse, vaccinations à la traîne"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Covid 19 : le point sur l'Ardèche"
weight = 1

+++
Sommes-nous à la veille d'une cinquième vague ? L'évolution des hospitalisations pour cause de Covid-19 en Ardèche est préoccupante (en rouge sur le graphique ci-dessous). Nous sommes en tendance passé au dessus de la moyenne nationale (en bleu) pour la première fois depuis mars dernier avec 33 hospitalisations, soit le double d'il y a une semaine.

![](/media/covid-19-hospitalisation-14-11-2021.png)

Source : Santé publique France

Sachant que, selon la Direction de la recherche, des études, de l'évaluation et des statistiques, il y a maintenant [onze fois plus de décès parmi les personnes non vaccinées que parmi les vaccinées](https://drees.solidarites-sante.gouv.fr/communique-de-presse/exploitation-des-appariements-entre-les-bases-si-vic-si-dep-et-vac-si-des), il est utile de connaître le pourcentage d'Ardéchois vaccinés selon les tranches d'âge. 

Le tableau ci-dessous réserve quelques surprises et de quoi, là encore, être préoccupé. On y constate, par exemple, une différence de 10 points avec la moyenne française pour les vaccinations des Ardéchois entre 25 et 59 ans.

![](/media/covid-19-vaccination-14-11-2021.png)

Source : Santé publique France