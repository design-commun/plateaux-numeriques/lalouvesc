+++
date = 2022-11-30T23:00:00Z
description = "La neige peut arriver"
header = "/media/saleuse-2022-1.jpg"
icon = ""
subtitle = ""
title = "La nouvelle saleuse est arrivée"
weight = 1

+++
L'ancienne saleuse vieillie était hors service. Il en fallait une nouvelle, elle est arrivée et a été montée sur le tracteur.

Il peut geler ou neiger, nous sommes prêts.

![](/media/saleuse-2022-2.jpg)