+++
date = 2022-12-10T23:00:00Z
description = "Un aperçu..."
header = "/media/decoration-noel-2022-4.jpg"
icon = ""
subtitle = ""
title = "Les décorations de Noël ont été installées"
weight = 1

+++
Depuis plusieurs semaines, on s'affairait dans les garages et les maisons. Ce week end, presque tout est en place.

Bravo et merci à tous les artistes et décorateurs ! Grâce à vous le village est en fête une nouvelle fois. Avec de nouveaux tableaux et clins d’œil à découvrir...

Merci aussi pour sa générosité à René Sabatier qui nous a donné 35 sapins, répartis dans le village.

Ci-dessous un aperçu.

{{<grid>}}{{<bloc>}}

![](/media/decoration-noel-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-11.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-7.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-8.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-10.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-12.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2022-1.jpg)

{{</bloc>}}{{</grid>}}

Plus de photos sur le [FB du Comité des fêtes](https://www.facebook.com/comitedesfeteslalouvesc/).