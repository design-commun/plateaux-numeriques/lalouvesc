+++
date = 2022-08-24T22:00:00Z
description = "Dépêchez-vous, faites venir vos amis"
header = "/media/carrefour-des-arts-2022-ecole-3.jpeg"
icon = ""
subtitle = ""
title = "Carrefour des Arts : derniers jours"
weight = 1

+++
Si vous n'avez pas encore visité les quatre niveaux et les 750m2 d'exposition du Carrefour des Arts, si vous voulez revoir les œuvres qui vous ont enchantés lors de votre dernière visite ou si vous voulez acheter l'oeuvre-coup-de-coeur qui hante encore vos rêves et que vous risquez de regretter, dépêchez-vous ! Il ne reste plus que quelques jours d'exposition. De plus dimanche 28 août dernier jour, la plupart des artistes seront présents.

Ci-dessous un petit rappel des artistes.

##### Judith Chancrin, peintre

{{<grid>}}{{<bloc>}}

![](/media/judith-chancrin-1-carrefour-des-arts-2022.png)

> Je pars souvent de photographies que je recadre et travaille ensuite de manière à n’utiliser qu’un fragment suffisamment mystérieux pour être débarrassé de toute certitude, de toute vérité.

{{</bloc>}}{{<bloc>}}

![](/media/judith-chancrin-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Isabelle Tapie, doreuse, premier étage

{{<grid>}}{{<bloc>}}

![](/media/isabelle-tapie-1-carrefour-des-arts-2022.jpg)

> La dorure sur bois (technique complexe et subtile, inchangée depuis le XVIIème siècle) voyage au gré des branches, fragments de troncs d’arbres et racines, sculptures naturelles, rencontrées avec émotion lors de mes promenades.

{{</bloc>}}{{<bloc>}}

![](/media/isabelle-tapie-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### **François Lelièvre, sculpteur**

{{<grid>}}{{<bloc>}}

![](/media/francois-lelievre-1-carrefour-des-arts-2022.png)

> J'ai appris à travailler le bois. Sans doute, mais on peut dire cela autrement et avec davantage de justesse : j'ai appris à aimer le bois, à l’écouter comme à le contraindre, à le caresser comme à le violenter.

{{</bloc>}}{{<bloc>}}

![](/media/francois-lelievre-3-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Cécile Windeck, peintre

{{<grid>}}{{<bloc>}}

![](/media/cecile-windeck-1-carrefour-des-arts-2022.png)

> Depuis longtemps, je peins des crépuscules. Les ‘Heures Bleues’ sont un moment incertain où le ciel prend cette teinte bleue si particulière, où tout devient plus contrasté durant quelques minutes.

{{</bloc>}}{{<bloc>}}

![](/media/cecile-windeck-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Erica Stéfani, mosaïste

{{<grid>}}{{<bloc>}}

![](/media/erica-stefani-1-carrefour-des-arts-2022.png)

> Je choisis d’utiliser les techniques gréco-romaines dans la création de mes mosaïques, auxquelles j’intègre l’exploration de chemins contemporains, par le biais de matériaux différents et formes plus libres.

{{</bloc>}}{{<bloc>}}

![](/media/erica-stefani-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Malika Ameur, céramiste

{{<grid>}}{{<bloc>}}

![](/media/malika-ameur-1-carrefour-des-arts-2022.jpg)

> Chaque pièce est choisie avec soin pour sa texture, sa forme, sa couleur au plus proche de la chaleur de la brique et de la rouille. Puis, j’y mêle des clous, des métaux, chargés d’histoire pour créer un mariage cosmopolite et harmonieux.

{{</bloc>}}{{<bloc>}}

![](/media/malika-ameur-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}

##### Françoise Papail, peintre

{{<grid>}}{{<bloc>}}

![](/media/francoise-papail-3-carrefour-des-arts-2022.jpg)

> Souvent ce sont les paysages du bord de mer qui couvrent la toile... Là où je vis, je respire chaque jour l’odeur d’iode et de goémon... et m’absorbe dans ces vastes horizons de mer, de dunes...

{{</bloc>}}{{<bloc>}}

![](/media/francoise-papail-2-carrefour-des-arts-2022.jpg)

{{</bloc>}}{{</grid>}}