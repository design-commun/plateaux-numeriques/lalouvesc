+++
date = 2022-07-19T22:00:00Z
description = "Arrêté préfectoral"
header = "/media/sanglier-wkp.jpg"
icon = ""
subtitle = ""
title = "Le sanglier déclaré nuisible en Ardèche"
weight = 1

+++
Un arrêté préfectoral vient d'être publié déclarant le sanglier comme nuisible dans le département.

Il peut désormais être piégé et abattu toute l'année, sous des conditions strictement encadrées.

 Voir[ l'arrêté](/media/ap-nuisible-_2022-2023_sanglier.pdf).