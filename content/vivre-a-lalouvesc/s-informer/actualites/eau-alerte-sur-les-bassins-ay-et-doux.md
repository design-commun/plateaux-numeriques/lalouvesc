+++
date = 2023-03-07T23:00:00Z
description = "Restrictions d'usages"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "Eau : alerte sur les bassins Cance et Doux"
weight = 1

+++
La préfecture communique :

{{<grid>}}{{<bloc>}}

> La situation de sécheresse météorologique impose de prendre les premières mesures de restriction des usages de l’eau sur les bassins versants de la Cance et du Doux
>
> Les quelques précipitations de la semaine dernière restent insuffisantes pour atténuer le déficit global de précipitation en ce début d’année et deux cours d'eau du département connaissent de manière très précoce des niveaux inférieurs au seuil d’alerte (les bassins de la Cance et du Doux).  
> Face à cette situation hydrologique inédite pour un début du mois de mars, le préfet de l’Ardèche a classé ces bassins **au niveau « ALERTE » et y impose les limitations des usages** de l’eau.  
> Tous les autres bassins versants sont placés en **« vigilance ».** 
>
> **La précocité de la sécheresse cette année doit inciter tous les usagers à réduire autant que possible leurs consommations.  
> Les principaux usages du moment concernent surtout l’eau potable. Chacun de nous doit donc être attentif à ses consommations et adopter une attitude économique en eau.**

{{</bloc>}}{{<bloc>}}

![](/media/alerte-eau-mars-2023.jpg)

{{</bloc>}}{{</grid>}}

Voir l'[arrêté préfectoral](/media/ap_06mars2023_cance-doux.pdf)

## Restrictions d’usages

### Usages de l’eau domestique (particuliers et collectivités territoriales)

* L’alimentation en eau des plans d’eau, des canaux d’agrément et des béalières ne disposant pas de règlement d’eau autorisé par le préfet (arrêté préfectoral) et le prélèvement d’eau depuis ces ouvrages sont interdits. Une attention particulière sera portée lors des opérations de fermeture des canaux afin de ne pas porter préjudice à la faune piscicole présente. L’alimentation en eau des plans d’eau, des canaux d’agrément et des béalières autorisés par arrêté préfectoral et le prélèvement d’eau depuis ces ouvrages doivent respecter les prescriptions fixées dans l’arrêté.
* L’arrosage des pelouses, ronds points, espaces verts publics et privés, jardins d’agrément et des espaces sportifs n’est autorisé que trois jours par semaine (lundi, mercredi et vendredi) entre 20h et 9h.
* Le lavage des voitures est interdit hors des stations professionnelles recyclant l’eau et sauf pour les véhicules ayant une obligation réglementaire (véhicules sanitaires, alimentaires ou techniques) et pour les organismes liés à la sécurité.
* Le remplissage des piscines est interdit (sauf piscines de volume inférieur à 1 m³) ; toutefois le premier remplissage des piscines nouvellement construites et le remplissage complémentaire des piscines sont autorisés entre 20 h et 9 h.
* Le lavage à l’eau des voiries est interdit, sauf impératifs sanitaires et à l’exception des lavages effectués par des balayeuses laveuses automatiques.
* Les fontaines publiques en circuits ouverts doivent être arrêtées.
* Les tests de capacité des hydrants et points d’eau incendie (PEI) sont interdits.

### Usages industriels

Les installations classées pour la protection de l’environnement (ICPE) appliquent les prescriptions fixées dans leur arrêté d’autorisation, leur enregistrement ou leur déclaration pour les épisodes d’alerte. Les besoins prioritaires et indispensables des autres activités industrielles doivent être portés à la connaissance du service de police de l’eau ou de contrôle des installations classées.

### Stations d’épuration des eaux usées

Les gestionnaires d’installations signalent préalablement aux services de police des eaux les interventions susceptibles de générer un rejet dépassant les normes autorisées, notamment les opérations de maintenance sur des organes de traitement ou les opérations d’entretien des réseaux (curages…).