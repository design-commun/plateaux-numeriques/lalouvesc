+++
date = 2021-07-26T22:00:00Z
description = "Un bac à fleur a été volé à l'entrée du village."
header = "/media/20210727_115844.jpg"
icon = ""
subtitle = ""
title = "Déplorable !"
weight = 1

+++
Déplorable ! Un bac à fleur a été volé à l'entrée du village.

De plus un panneau a disparu à l'entrée du village.![](/media/degradation.jpg)

Une plainte est en cours de dépôt auprès de la gendarmerie pour vol et dégradation de bien public. Tout témoignage est à déposer à la mairie.

A bon entendeur...