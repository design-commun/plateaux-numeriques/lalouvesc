+++
date = 2021-12-09T23:00:00Z
description = "Alerte de la gendarmerie"
header = ""
icon = ""
subtitle = ""
title = "Attention aux arnaques sur internet"
weight = 1

+++
Nous avons reçu ce courrier d'alerte :

_Bonjour,_

_Depuis la rentrée, de nombreux habitants dans l'Hexagone ont signalé avoir reçu un courriel du Ministère de l'Intérieur, voir signé de la part du Directeur Général de la Gendarmerie.  
Le département de l'Ardèche n'est pas épargné._

_Dans ce courriel se trouve en pièce jointe une soi-disant convocation en justice, le plus souvent pour des faits très graves.  
La Gendarmerie ne procède pas à ce type de convocation de la sorte.  
=> Ne surtout pas l'ouvrir, il s’agit d’une tentative d'escroquerie par la technique dite de "phishing" (hameçonnage) !_

_Signalez ce courriel sur la plateforme gouvernementale dédiée :_ [_https://www.signal-spam.fr/_](https://www.signal-spam.fr/ "https://www.signal-spam.fr/") _et supprimez le._

_N'hésitez pas à partager cette information.  
Cordialement__

_Major Le Gaillard Ludovic_

## Les 10 règles de base pour vous protéger sur Internet

C'est l'occasion de rappeler les 10 règles de base pour bien se protéger tous les détails sur [cette page](https://www.cybermalveillance.gouv.fr/tous-nos-contenus/actualites/les-10-regles-de-base-pour-la-securite-numerique)) :

#### 1. Adopter une politique de mot de passe rigoureuse

#### 2. Sauvegarder ses données régulièrement

#### 3. Sécurité numérique : faire ses mises à jour régulièrement

#### 4. Se protéger des virus et autres logiciels malveillants

#### 5. Évitez les réseaux Wifi publics ou inconnus

#### 6. Sécurité numérique : Bien séparer ses usages professionnels et personnels

#### 7. Éviter de naviguer sur des sites douteux ou illicites et être vigilant lors du téléchargement d’un fichier

#### 8. Contrôler les permissions des comptes utilisateurs

#### 9. Sécurité numérique : Être vigilant sur les liens ou les pièces jointes contenus dans des messages électroniques

#### 10. Faire attention aux informations personnelles ou professionnelles que l’on diffuse sur Internet