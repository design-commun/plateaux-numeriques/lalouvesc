+++
date = 2023-03-02T23:00:00Z
description = "Le premier tour se tiendra le 19 mars"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Candidats à l'élection municipale partielle complémentaire"
weight = 1

+++
Deux candidatures ont été retenues par la préfecture pour l'élection municipale partielle complémentaire :

* Madame Nicole Porte
* Monsieur Corentin Serayet

Voir l'[arrêté préfectoral](/media/ap_07_2023_03_03_00002_lalouvesc.pdf).

Le premier tour se tiendra le 19 mars. Pour être élu au premier tour un candidat doit obtenir plus de 50% des voix des votants et plus de 25% des inscrits sur la liste électorale de la Commune.

Si ces deux conditions ne sont pas atteintes au premier tour, un second tour se tiendra le 26 mars, sans condition particulière pour l'élection.

### Procuration

Si vous ne pouvez vous déplacer le jour du vote, vous pouvez demander à quelqu'un d'autre de voter à votre place.

Pour donner procuration à un électeur, vous aurez besoin soit de son [numéro d'électeur](https://www.maprocuration.gouv.fr/#FAQ) et de sa date de naissance, soit de ses données d'état civil et de sa commune de vote.

Vous effectuez votre demande de procuration en ligne en toute simplicité, puis vous vous déplacez au commissariat, à la gendarmerie ou au consulat pour faire vérifier votre identité et valider votre procuration

Tout est expliqué [ici](https://www.maprocuration.gouv.fr/).