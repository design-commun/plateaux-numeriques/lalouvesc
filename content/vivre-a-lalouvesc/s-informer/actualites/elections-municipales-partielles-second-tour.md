+++
date = 2023-03-25T23:00:00Z
description = "Résultats : trois élu(e)s"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Elections municipales partielles : second tour"
weight = 1

+++
Le second tour des élections municipales partielles s'est tenu à Lalouvesc le dimanche 26 mars. Résultats :

* Inscrits : 339
* Votants : 159 soit 47% des inscrits
* Votes nuls : 3
* Votes blancs : 2
* M. Gérard Guironnet : : 92 voix, 61,7% des suffrages exprimés, **élu**
* Mme Nicole Porte : 90 voix, 61,2% des suffrages exprimés, **élue**
* M. Corentin Serayet : 73 voix, 49,6% des suffrages exprimés
* Mme Christine Trébuchet : 100 voix, 68% des suffrages exprimés, **élue**

**Le Conseil municipal est maintenant complet avec 11 conseillers élus.**

**Merci à toutes les citoyennes et les citoyens qui se sont déplacés pour voter !**