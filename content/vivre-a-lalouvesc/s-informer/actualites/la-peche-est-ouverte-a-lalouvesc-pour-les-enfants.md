+++
date = 2021-04-23T22:00:00Z
description = "Des truites ont été lachées dans l'étang du Grand Lieu. A vos cannes, les enfants ! "
header = "/media/peche-samov-1.jpg"
icon = ""
subtitle = ""
title = "La pêche est ouverte à Lalouvesc pour les enfants !"
weight = 1

+++
Samedi 24 Avril, à 13h30 à l’étang du Grand Lieu (ou étang de la Samov), a eu lieu un lâché de truites.

Sur place, Mr Faure de La pisciculture des Sources, accompagné de Sébastien Lacoste vice-président de l’association _Les amis de la ligne_ basé à Satillieu et moi-même.

Mr Faure a lâché 25 kg de truites farios (au-dessus de 23 cm) et 5 kg de truitelles entre 10 et 15 cm, le but étant de développer la découverte de la pêche pour les enfants de – 12 ans.

![](/media/peche-samov-4.jpg)

Petit rappel pour la pêche à l’étang de Lalouvesc, seuls les enfants de – de 12 ans ont le droit de pêcher, accompagnés bien sûr. Ils doivent être munis d’un permis de pêche qui coûte 6 euros pour une année complète et qui leur permet de pêcher sur tous les cours d'eau autorisés du département de l'Ardèche pour une année complète en tenant compte des périodes d'ouverture et de fermeture du poisson pêché.

Pour se le procurer direction : [https://www.cartedepeche.fr/](https://www.cartedepeche.fr/ "https://www.cartedepeche.fr/"), il faut sélectionner le permis découverte – de 12 ans et bien choisir dans la liste, l’association _les amis de la ligne_ sur la commune de Satillieu.

La pêche des truites est réglementée, la maille est de 23 cm. En dessous la truite doit être obligatoirement relâchée.

Un panneau sera installé bientôt près de l’étang rappelant la réglementation.

La saison de pêche court du 13 mars au 19 septembre 2021, cours d'eau et plans d'eau de 1ère catégorie.

Alors à vos cannes !

Aline Delhomme