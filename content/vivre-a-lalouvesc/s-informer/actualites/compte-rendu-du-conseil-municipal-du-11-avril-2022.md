+++
date = 2022-04-14T22:00:00Z
description = ""
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte-rendu du Conseil municipal du 11 avril 2022"
weight = 1

+++
Le compte-rendu est accessible[ ici](/media/2022-2-conseil-municipal-lalouvesc.pdf). Bonne lecture !

## Ordre du jour

1. COMMISSION FINANCES
   1. Document de valorisation financière et fiscale 2021
   2. Approbation du compte de gestion 2021
   3. Approbation du compte administratif 2021
   4. Affectation des résultats 2021
   5. Approbation du budget primitif 2022
   6. Information sur le budget CCVA
2. COMMISSION GESTION
   1. Recrutement d’employés municipaux
   2. Annulation de la délibération de l’adressage votée en 2020
3. COMITÉ VIE LOCALE
   1. Date des journées citoyennes
4. COMITÉ DÉVELOPPEMENT
   1. Réunion avec le SCOT
5. DIVERS