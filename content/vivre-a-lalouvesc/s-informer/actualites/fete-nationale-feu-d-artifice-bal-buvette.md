+++
date = 2021-07-05T22:00:00Z
description = "Ouverture de la buvette à 19h, bal et feu d'artifice à partir de 21h"
header = "/media/feu-d-artifice-2021-2.jpg"
icon = ""
subtitle = ""
title = "Fête nationale : 13 juillet, feu d'artifice, bal, buvette"
weight = 1

+++
![](/media/feu-d-artifice-2021.jpg)