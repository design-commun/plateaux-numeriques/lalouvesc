+++
date = 2021-09-10T22:00:00Z
description = "Une programmation remarquable, plus de 1.300 spectateurs encore cette année malgré la Covid-19"
header = "/media/cinema-le-foyer.jpg"
icon = ""
subtitle = ""
title = "Bref retour sur la bonne saison du Cinéma Le Foyer"
weight = 1

+++
Lalouvesc, village de moins de 400 habitants, a son cinéma qui fonctionne tout l’été avec [une programmation remarquable](/decouvrir-bouger/lalouv-estivals/a-l-affiche-en-2021/), comprenant des films de qualité pour tous publics et même chaque année trois ou quatre avant-premières. Une situation tout à fait exceptionnelle qui fait l’admiration des estivants qui le fréquentent assidûment, même si on pourrait souhaiter trouver plus de Louvetous parmi les spectateurs.

La salle de l'Abri du Pèlerin est gérée par l'[association Ciné le Foyer de Bourg l'Argental](https://cinelefoyer.com/) qui en assure la programmation. Annie Besset, qui coordonne son activité sur Lalouvesc, nous a fait parvenir les chiffres de trois dernières années. Ils sont très bons.

![](/media/spectateurs-2019-2020-2021-du-cinema-le-foyer.png)

Avec 1356 spectateurs en 2021, soit pratiquement le même chiffre que l'année précédente, la salle a certes accueilli moins de monde qu'en 2019 (2435 spectateurs), mais dans une situation radicalement différente, où les mesures sanitaires ont pesé lourd : une ouverture plus tardive, moins de films (22 contre 30) et surtout pendant un moment une jauge réduite à 49 spectateurs dans la salle, puis la vérification du passe sanitaire.

Dans ces conditions, c'est un vrai succès qu'il faut saluer. Si l'on compare avec la situation sinistrée du cinéma en France ces deux dernières années, Lalouvesc, là encore, fait exception, un village gaulois qui résiste.

Cette réussite est le résultat d'une coordination exemplaire entre une association culturelle, le Sanctuaire qui loue la salle pour une somme symbolique et la Mairie qui la subventionne. 

Reste que son fonctionnement est fragile. Il s’appuie sur le dévouement de bénévoles qui, cette année, n'ont pas été très nombreux. Il faudra impérativement, l'année prochaine, élargir les troupes. L'appel est lancé : si le cinéma vous intéresse, si vous pensez qu'il a sa place à Lalouvesc, rejoignez l'association !