+++
date = 2022-09-08T22:00:00Z
description = "Dimanche 18 sept de 10 à 18h, auteurs invités et animations"
header = "/media/biblio-chouette.jpg"
icon = ""
subtitle = ""
title = "Fête du livre : demandez le programme !"
weight = 1

+++
### Les auteurs invités

* [Dominique Errante](https://www.facebook.com/people/Dominique-Errante/1607531865/),
* [Anne-Marie Quintard](https://annemariequintard.fr/),
* Patrick Fraysse,
* [Éliane Gastaud](http://e-gastaud.me/contact.html).

### Les animations

* 11h45 et 16h : lecture de poèmes de Dominique Errante,
* 15h : animation pour les enfants avec Anne-Marie Quintard et Marie-Christine Brunel,
* Tout au long de la journée : expo photos, animation pliage.

![](/media/fete-du-livre-2022.jpg)