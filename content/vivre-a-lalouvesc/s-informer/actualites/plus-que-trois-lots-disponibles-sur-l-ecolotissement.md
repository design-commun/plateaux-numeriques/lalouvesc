+++
date = 2022-05-16T22:00:00Z
description = "Faîtes-le savoir aux intéressés. Ce sont les derniers terrains constructibles sur le village..."
header = "/media/ecolotissement-mai-2022-3.jpg"
icon = "🏗️"
subtitle = ""
title = "Plus que trois lots disponibles sur l'écolotissement"
weight = 1

+++
Finalement, l'écolotissement du Bois de Versailles rencontre le succès. Après les premières signatures qui ont permis le lancement des travaux sans mettre en péril les finances de la Commune, d'autres acheteurs se sont présentés... si bien qu'il ne reste plus que trois lots encore disponibles sur les neuf mis en vente.

**Avis aux amateurs, si vous voulez profiter de la douceur de vivre louvetonne dans la maison de vos rêves, ce sont les derniers terrains constructibles !**

Tout est expliqué[ ici](https://www.lalouvesc.fr/projets-avenir/changement-climatique/ecolotissement-du-bois-de-versailles/).

![](/media/ecolotissement-mai-2022-3.jpg)

Les compromis de vente se signent, les permis de construire se déposent et les travaux de viabilisation vont bon train..

{{<grid>}}{{<bloc>}}

![](/media/ecolotissement-chantier-reseaux-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecolotissement-chantier-reseaux-1.jpg)

{{</bloc>}}{{</grid>}}