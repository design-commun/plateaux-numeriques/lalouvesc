+++
date = 2022-02-10T23:00:00Z
description = "Piquetage de la façade sur l'espace Beauséjour, jeu-monument, ouverture du chantier sur l'écolotissement"
header = "/media/demolition-beausejour-2022-78.jpg"
icon = ""
subtitle = ""
title = "Des nouvelles des chantiers"
weight = 1

+++
## Beauséjour

Les travaux ont repris sur l'espace Beauséjour avec la mise en place d'un échafaudage devant la façade restante et son piquetage.

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-76.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-77.jpg)

{{</bloc>}}{{</grid>}}

## Jeu-monument

L'équipe d'étudiants sélectionnés pour le concours d'idées sur le jeu-monument est passée le week-end dernier pour découvrir le terrain et rencontrer des habitants. De leur côté les élèves de l'école St Joseph ont fait un atelier pour donner leurs idées.

![](/media/jeu-monument-ecole-st-joseph-2022-1.jpg)  
Des nouvelles dans le Bulletin de mars.

## Ecolotissement

Une réunion de chantier s'est tenue pour lancer les travaux d'aménagements sur l'écolotissement.

Le chantier démarrera mi-mars. Il sera conduit en deux étapes : tout d'abord les travaux de voirie et de réseaux, puis, dans un second temps, les finitions (murets et espaces verts). L'objectif est de permettre aux premiers acquéreurs de démarrer leur travaux de construction sans risquer d'abîmer les finitions de l'aménagement de l'espace.