+++
date = 2021-08-10T22:00:00Z
description = "Avec la semaine prochaine le Comité des fêtes à la buvette."
header = "/media/marches_st_pierre_2021-2.jpg"
icon = ""
subtitle = ""
title = "Marché nocturne de St Pierre sur Doux"
weight = 1

+++
Le Comité des fêtes a accepté la proposition du Comité d’animation de Saint Pierre sur Doux pour tenir la buvette lors du dernier marché nocturne de la saison.

Au cœur de l’été les bénévoles vous proposent de venir les rejoindre pour un moment de convivialité : flâner et apprécier les étals proposés par des producteurs et artisans locaux, prendre un verre et déguster une crêpe… une jolie soirée en perspective le mercredi 18 août ! Le tout en musique et karaoké.

![](/media/marches_st_pierre_2021.jpg)