+++
date = 2022-03-05T23:00:00Z
description = "Le 19 mars à 13h30 au CAC. La dernière ?"
header = "/media/promenades-musivales-lalouvesc-1.jpeg"
icon = ""
subtitle = ""
title = "Assemblée générale des Promenades musicales"
weight = 1

+++
Extrait de la lettre du président envoyée aux adhérents de l'association :

![](/media/promenades-musicales-logo.jpg)

_Chers amis_

_La dernière assemblée générale des Promenades Musicales en "présentiel" a eu lieu le 18 février 2019. Les conditions étant maintenant réunies pour une nouvelle assemblée générale, nous vous invitons à y participer le **samedi 19 mars à 13h30** au Centre d'Animation Communal de Lalouvesc._

_Votre participation à cette assemblée est particulièrement importante en raison des décisions majeures à valider._

_Trois des membres du bureau (Le Président, le secrétaire, et le secrétaire adjoint-webmestre) ont décidé de ne pas se représenter et n'ont pas trouvé de volontaires pour leur succéder._

_Cela entraine "de facto" la dissolution de l'association._

_(...)_

_En vous remerciant par avance pour votre participation particulièrement importante cette année je vous adresse mes plus cordiales salutations_

_Le Président:_

_Dr. Didier CHIEZE_

**Pour autant, les activités musicales (classique et jazz) des Promenades Musicales ne devraient pas s'arrêter... à suivre !**