+++
date = 2022-08-04T22:00:00Z
description = "Il se passe toujours quelque chose aux Lalouv'estivales"
header = "/media/jazz-1.jpg"
icon = ""
subtitle = ""
title = "Que faire à Lalouvesc du 7 au 14 août ?"
weight = 1

+++
A part le farniente, les [**innombrables balades**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/), le [**vélotourisme**](https://www.lalouvesc.fr/decouvrir-bouger/activites/velotourisme/), le[ **cheval**](https://www.lalouvesc.fr/decouvrir-bouger/activites/centre-equestre/)... les pique-niques à l'ombre des arbres, les [**jeux pour les enfants**](https://www.lalouvesc.fr/decouvrir-bouger/activites/parcs-terrains-de-sport-et-jeux-d-enfants/) (se renseigner au camping sur les animations encadrées gratuites proposées) ou une baignade dans le Doux, voici quelques suggestions :

### Dimanche 7 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* aller à la **fête du Sanctuaire**
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Top Gun_** ( [bande annonce](https://youtu.be/V4gQdk1nAn0) ) à l’Abri du Pèlerin

### Lundi 8 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_L’innocent_** ( [bande annonce](https://youtu.be/6wl4B5lJgBM) ), avant-première à l’Abri du Pèlerin

### Mardi 9 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 20h30 : **Théâtre** **_Sous les ponts de Paris_** par Les Bretelles à Bascule à l’Abri du Pèlerin

### Mercredi 10 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 15h - 17h emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* 17h partager un **Apéro découverte** à l’Office du tourisme
* à l’Abri du Pèlerin **Cinéma**
  * 17h30 : **_De folies en folies_**, documentaire, Expo photos et dégustation de produits locaux entre les séances
  * 21h : **_Les folies fermières_** ( [bande annonce](https://youtu.be/KD5sB_BPLZI) )

### Jeudi 11 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_L’affaire Collini_** ( [bande annonce](https://youtu.be/4buWqDrWMBo) ) à l’Abri du Pèlerin

### Vendredi 12 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 19h30 : **Concert _Fats Waller & His Rhythm_** [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr) , jazz à la Grange de Polly

### Samedi 13 août

* 9h30 - 11h30  emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 19h30 : **Concert _Jazz Manouche_** [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr) , jazz à la Grange de Polly
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Les folies fermières_** ( [bande annonce](https://youtu.be/KD5sB_BPLZI) ) à l’Abri du Pèlerin

### Dimanche 14 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_L’affaire Collini_** ( [bande annonce](https://youtu.be/4buWqDrWMBo) ) à l’Abri du Pèlerin