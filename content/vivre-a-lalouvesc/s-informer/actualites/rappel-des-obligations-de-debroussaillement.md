+++
date = 2023-03-20T23:00:00Z
description = "Courrier du Préfet"
header = "/media/pompiers-2022-3.jpg"
icon = ""
subtitle = ""
title = "Rappel des obligations de débroussaillement"
weight = 1

+++
Le préfet de l'Ardèche rappelle les obligations de débroussaillement des terrains et abords des habitations pour assurer la sécurité des riverains face au risque de feux de forêt et de végétation.

> Le feu de forêt est une préoccupation omniprésente dans notre département. Le changement climatique intensifie ce risque : la saison des feux s'allonge et le risque s'étend progressivement vers des territoires jusqu'alors relativement épargnés.
>
> Aussi, il convient de se prémunir contre les incendies qui viendraient menacer les habitations ou de ceux induits par les habitants eux-mêmes.
>
> Pour limiter les dommages que pourrait causer le feu à notre patrimoine, le code forestier (article L.134-6) oblige les propriétaires situés en zone boisée et à moins de 200 mètres des bois, landes, maquis, garrigues, à débroussailler et à maintenir en état débroussaillé conformément aux prescriptions suivantes :
>
> * aux abords des constructions, chantiers et installations de toute nature, sur une profondeur de 50 mètres ; le maire pouvant porter cette obligation à 100 mètres,
> * la totalité du terrain si celui-ci fait partie d'un lotissement,
> * la totalité du terrain si celui-ci se trouve dans la zone urbaine (zone U) du PLU en vigueur (plan local d'urbanisme),
> * la totalité du terrain s'il fait partie d'une AFU ou d'une ZAC (association foncière urbaine ou zone d'aménagement concerté articles L. 322-2 ou L. 3111 du code de l'urbanisme),
> * la totalité des terrains si ce sont des terrains de camping ou de stationnement de caravanes.

Le non-respect de ces obligations est passible d'amendes.