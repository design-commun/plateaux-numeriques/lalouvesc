+++
date = 2021-10-12T22:00:00Z
description = "La société chargée de réviser les coordonnées GPS des adresses postales est intervenue en début de semaine"
header = "/media/adressage-2021.jpg"
icon = ""
subtitle = ""
title = "Adressage : révisions et précisions"
weight = 1

+++
La démarche de validation de l'adressage dans le village a été initiée par la municipalité précédente, mais il restait encore certaines décisions à préciser et le prestataire engagé n'a pas répondu à notre attente.

Un nouveau prestataire a donc été contacté. Il est passé en début de semaine pour prendre l'ensemble des coordonnées GPS. Préalablement, les élus avaient révisé et validé l'ensemble des appellations de voies sur la commune, le bourg et les hameaux. Il a été décidé que pour le bourg, les numérotations se suivraient, paires et impaires selon le côté de la rue comme c'est la pratique dans les villes en France, tandis que pour l'extérieur et les hameaux, il s'agirait d'une numérotation métrique (nombre de mètres à partir du départ de la route) de façon à mieux visualiser les distances.

Une fois cette étape bouclée, les données seront envoyées à l'administration pour actualiser adresses et cartes des différents services publics et ceux de de l'Insee.

Il restera enfin à poser des panneaux avec les noms de rues, ruelles, impasses, routes, là où ils ne sont pas encore présents et les numéros sur les façades. Cette opération sera facilitée par les photos prises au moment des relevés GPS. Elle aura lieu au printemps prochain.