+++
date = 2022-11-17T23:00:00Z
description = "Nous les retrouverons l'année prochaine."
header = "/media/fleurs-nov-2022-2.jpg"
icon = ""
subtitle = ""
title = "Les fleurs sont à l'abri"
weight = 1

+++
Les gelées approchent. Toutes les fleurs qui pouvaient être sauvées resteront au chaud.

![](/media/fleurs-nov-2022-1.jpg)

Merci au comité de fleurissement et à toutes les personnes qui ont installé les plantations,  à ceux qui les ont entretenues et enfin à ceux qui les ont mises à l'abri !

Grâce à vous, nous avons eu un village magnifique cette année !

![](/media/fleurissement-2021-9.jpg)