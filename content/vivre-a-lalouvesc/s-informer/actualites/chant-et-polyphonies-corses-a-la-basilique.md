+++
date = 2023-02-03T23:00:00Z
description = "Retenez la date :14 mai"
header = "/media/polyphonies-corses.jpg"
icon = ""
subtitle = ""
title = "Chant et polyphonies corses à la Basilique"
weight = 1

+++
Le Comité des Fêtes vous propose un rendez-vous avec : **_A VUCIATA,_ CHANTS & POLYPHONIES CORSES, Un concert exceptionnel le dimanche 14 mai à 15h00 à la basilique de Lalouvesc**

Depuis 2001, **_A VUCIATA_** ne cesse d'évoluer, pour acquérir une maturité reconnue sur le continent avec plus de 600 concerts à son actif.

Le groupe se produit dans les plus grands festivals en France et à l'étranger, ainsi que dans des lieux réputés pour l'art vocal : l'Abbaye de Gellone à Saint Guilheim le Désert (34), l'Abbaye de Sylvanès (12), l’Abbaye de Fontfroide (11) ainsi que le Festival de Cominges (Occitanie).

Les qualificatifs sont nombreux pour évoquer le talent de ces artistes, et qu'est-il de plus beau que de perpétuer le savoir, sublimer l'empreinte et plus que de lui rendre hommage, lui donner l'occasion de partager la mémoire au-delà du présent ?

Un aperçu de leur talent :
<iframe width="560" height="315" src="https://www.youtube.com/embed/TzYYLv0EjQ4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>