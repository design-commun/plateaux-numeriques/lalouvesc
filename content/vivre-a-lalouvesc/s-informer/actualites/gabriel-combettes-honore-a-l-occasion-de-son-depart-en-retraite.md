+++
date = 2022-10-08T22:00:00Z
description = "Les 110 ans de la menuiserie Besset se sont accompagnés  de remises de médaille"
header = "/media/110-ans-menuiserie-besset-2022-5.jpg"
icon = ""
subtitle = ""
title = "Gabriel Combettes honoré à l'occasion de son départ en retraite"
weight = 1

+++
Près de deux cents personnes sont venues à l'anniversaire des 110 ans de la menuiserie Besset le 30 septembre, Vous en trouverez un compte-rendu fidèle sur le [FB de l'Office du tourisme](). nous aurons l'occasion d'y revenir dans le Bulletin de novembre.![](/media/110-ans-menuiserie-besset-2022-1.jpg)

Ce fut aussi le moment du départ à la retraite d'un de ses plus fidèles employés, Gabriel Combettes, un départ fêté d'une façon exceptionnelle et chaleureuse avec la remise de deux médailles par les responsables de la Confédération de l'Artisanat et des Petites Entreprises du Bâtiment (CAPEB07), Benoit Gauthier et Laurent Barruyer, et la députée de l'Ardèche-nord, Laurence Heydel Grillere, devant un aréopage d'élus.

{{<grid>}}{{<bloc>}}

![](/media/110-ans-menuiserie-besset-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/110-ans-menuiserie-besset-2022-3.jpg)

{{</bloc>}}{{</grid>}}

Extrait du discours de François Besset :

> (...) Je me rappellerai toujours l'arrivée de Gaby (...) devant les portes de l'atelier en présence de mon père Louis.
>
> Il nous dit : « Vous ne cherchez pas quelqu'un ? »  
> Je lui réponds : « Pourquoi pas, mais que savez-vous faire ? »  
> « Moi c'est simple actuellement je fais des parcs à sangliers, mais si vous voulez je peux commencer tout de suite. »
>
> C'était en quelques sorte l'entretien d'embauche de l'époque. (...)
>
> Du point de vue enseignement et pratique tout s'est fait sur le tas au fil des semaines, des mois et des années. Pas de bagages en poche mais une maîtrise totale de la confection des menuiseries, de la pose en passant par les toitures, couvertures, maçonnerie, soudure etc...
>
> Je ne vais pas énumérer les nombreux travaux, ce serait trop long, mais l'on peut dire que partout où Gaby est passé il peut y retourner et sera bien accueilli avec reconnaissance pour son travail, son honnêteté, son sens de la vie et le respect des autres. (...)
>
> Gaby, ne t'arrêtes pas, continue il y a toujours quelque chose à faire. Nous te souhaitons au nom de l'entreprise une retraite bien méritée, à ton rythme et surtout très longue car tu as programmé pleins de travaux. Profites aussi de ta famille !

Tout le village s'associe à ces vœux.