+++
date = 2023-01-30T23:00:00Z
description = "Conseil du 25 janvier 2023"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Procès-verbal du premier Conseil de l'année"
weight = 1

+++
Le procès-verbal est accessible [ici](/media/2023-2-conseil-municipal-lalouvesc.pdf), la liste des délibérations est [là](/media/liste-des-deliberations-25-01-2023.pdf).

### Rappel de l'ordre du jour

1\. COMMISSION FINANCES

a. Demande de subventions pour l’aménagement de l’espace Beau-Séjour

b. Demande de subventions pour la sécurisation du réseau d’eau

c. Tarifs de l’eau et de l’assainissement 2023 (abonnement, m3, taxes de raccordement)

d. Subvention 2023 pour l’OGEC (école St Joseph)

e. Subventions pour les associations

f. Reliures et archives

g. Contrat assistance et maitre d’œuvre pour la réfection des routes SDEA

2\. COMMISSION GESTION

a. Avancement de l’adressage

b. Actions pour la qualité de l’eau

c. Validation localisation et mise en place du city-Stade

3\. COMITÉ VIE LOCALE

a. Conventions pour l’utilisation des locaux communaux par les associations

b. Avancement du recensement

c. Logo et sacs

4\. COMITÉ DÉVELOPPEMENT

a. Avancement de l’étude sur Ste Monique et le Cénacle (SCOT)

b. Composition du comité de développement 2023

c. Grand atelier des maires ruraux pour la transition écologique

5\. DIVERS

a. Élection partielle

b. Panneau Pocket

c. Déneigement

d. Atlas de la biodiversité communale (ABC) 2023