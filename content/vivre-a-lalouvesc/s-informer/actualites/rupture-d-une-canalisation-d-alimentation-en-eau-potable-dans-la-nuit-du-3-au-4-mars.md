+++
date = 2021-03-03T23:00:00Z
description = "Vers 3h du matin, une canalisation principale d’alimentation d’eau du village s’est rompue rue Chemin neuf..."
header = "/media/source-du-perrier2.jpg"
icon = ""
subtitle = ""
title = "Rupture d'une canalisation d'alimentation en eau potable dans la nuit du 3 au 4 mars"
weight = 1

+++
Vers 3h du matin, une canalisation principale d’alimentation d’eau du village s’est rompue rue Chemin neuf. Monsieur Alain Chatagnon s’en est rendu compte à 4h du matin et a prévenu par téléphone le Maire. Le premier adjoint a pris contact avec le service technique. Il a fallu couper l’eau sur la canalisation, malheureusement la réserve d’eau était déjà vide et donc tout le village sans eau...

Après avoir prévenu la maison de retraite le Balcon Des Alpes de cet incident temporaire, l’équipe technique a basculé l’alimentation du village sur le deuxième réservoir de réserve.

Nous avons fait intervenir un réparateur pour la remise en état de la canalisation qui est à pied d’œuvre et l’incident devrait être réparé dans la journée.

Veuillez nous excuser pour le dérangement causé par ce cas de force majeure qui montre, une fois de plus, la vétusté de notre réseau et l'impérieuse nécessité d'investir dans son entretien.