+++
date = 2021-08-05T22:00:00Z
description = "Tous les jours d'été, il se passe quelque chose à Lalouvesc"
header = "/media/copie-de-20210703_112053.jpg"
icon = ""
subtitle = ""
title = "Spectacles et animations de la semaine du 9 au 16 août "
weight = 1

+++
En plus des [nombreuses activités](/decouvrir-bouger/activites/) possibles, voici les spectacles et manifestations proposés à Lalouvesc du 9 au 16 août.

## Tous les jours

* 14h30 à 18h30 **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal
* 10h à 12h et de 15h à 18h **Exposition _La matière habitée_**  sculptures à la chapelle Saint Ignace

## 9 août

* 9h **Gym**,  au camping
* 10h30 **Visite du jardin en permaculture** au Mont Besset
* 21h cinéma, **Adieu les cons**, [bande annonce](https://youtu.be/9uotsv-vf6I) Abri du Pèlerin

## 10 août

* 9h **Gym**, au camping
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 18h visites estivales **Paroles d'habitants**, départ à l'Office du tourisme

## 11 août

* 15h-17h empruntez des livres à la **bibliothèque**
* 17h **Apéro découverte** devant l'Office du tourisme
* 20h concert, **Hommage à Gershwin**, jazz à la Grange de Polly
* 21h : cinéma, **Présidents**, [bande annonce](https://youtu.be/XFNicafCq_8) Abri du Pèlerin

## 12 août

* 9h **Gym**, au camping
* 14h-16h **Animation enfants**, au camping (sur inscription)
* 21h cinéma, **Ibrahim**, [bande annonce](https://youtu.be/ohh14axZm-c) Abri du Pèlerin

## 13 août

* 15h  **Déambulation poétique**, Miche! Béatix et Hervé Tharel, à la chapelle St Ignace
* 20h concert, **Hommage à Armstrong**, jazz à la Grange de Polly

## 14 août

* 9h30-11h30 empruntez des livres à la **bibliothèque**
* 21h : cinéma, **Présidents**, [bande annonce](https://youtu.be/XFNicafCq_8) Abri du Pèlerin

## 15 août

* Fête de l'**Assomption,** Sanctuaire
* 21h cinéma, **Ibrahim**, [bande annonce](https://youtu.be/ohh14axZm-c) Abri du Pèlerin

## 16 août

* à partir de 14h30 : **Réalisation en public et en musique d’une œuvre** par la peintre Martine Jaquemet ([Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/)), devant le CAC
* 21h : cinéma, **Un triomphe** (avant-première) [bande annonce](https://youtu.be/N_q15dNbWtA) Abri du Pèlerin