+++
date = 2022-05-15T22:00:00Z
description = "Les bacs à fleurs vont reprendre leur place sur la rue des Cévennes"
header = "/media/15-aout-2021-4.jpg"
icon = ""
subtitle = ""
title = "Les fleurs vont être installées rue des Cévennes"
weight = 1

+++
Après avoir planté les persistantes dans les différents bacs qui les attendaient dans le village, les bénévoles du Comité fleurissement vont passer aux fleurs annuelles. Ainsi les bacs qui avaient été retirés pour l'hiver vont reprendre leur place dès lundi 23 mai. Le village continue de se faire beau pour l'été. Un grand merci aux bénévoles !

![](/media/15-aout-2021-1.jpg)

Les Louvetous, qui ont pris la mauvaise habitude de garer leur voiture sur des places interdites gênant la mise en place des bacs, sont priés de les ranger sur les parkings qui ne manquent pas dans le village. Attention, il n'y aura pas d'autre avertissement et les propriétaires des voitures en infraction risquent des désagréments !