+++
date = 2021-10-02T22:00:00Z
description = "Araignées, courges, fantômes ont remplacé les vaches"
header = "/media/trail-des-sapins-2021-deco-5.jpg"
icon = ""
subtitle = ""
title = "Square… changement de décor !"
weight = 1

+++
Est-ce les prévisions météo qui ont incité les bénévoles du Comité des Fêtes à rentrer les vaches à l’étable ?

Bref, place a été donnée samedi à une décoration encore plus colorée et presque effrayante tant elle est si bien réalisée.

{{<grid>}}{{<bloc>}}

![](/media/trail-des-sapins-2021-deco-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/trail-des-sapins-2021-deco-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/trail-des-sapins-2021-deco-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/trail-des-sapins-2021-deco-7.jpg)

{{</bloc>}}{{</grid>}}

Halloween se profile à l’horizon et le TRAIL DES SAPINS aussi. Araignées, courges, fantômes sont donc en place et guettent la ligne de départ.

Rendez-vous à la fin du mois pour encore plus de décos, de couleurs et d’animations. Tout le monde peut jouer le jeu en décorant vitrines, jardins et maisons et arborer, le 30 octobre, une tenue adéquate !![😀](https://fonts.gstatic.com/s/e/notoemoji/13.1.1/1f600/32.png)

Nous attendons avec impatience de savoir quels déguisements choisiront le Président du Conseil Départemental et le Sénateur qui nous feront le plaisir et la fierté de se présenter sur la ligne de départ !

Bravo à tous ceux et celles qui ont œuvré pour cette mise en scène.

Agnès, secrétaire Comité des Fêtes