+++
date = 2021-07-22T22:00:00Z
description = "Une carte postale pour recueillir les idées..."
header = "/media/carte-postale-jeu-monument-1.jpg"
icon = ""
subtitle = ""
title = "Jeu-monument : l'imagination au pouvoir !"
weight = 1

+++
Une carte_postale est disponible à la mairie, au camping, à l'Office du tourisme et chez des commerçants pour recueillir les idées des habitants, mais aussi de tous les amis du village et des estivants en résidence ou simplement de passage, avec cette simple interrogation : 

![](/media/carte-postale-jeu-monument-2.jpg)

## Quel nouveau jeu-monument sur le parc du Val d'Or ?

Il s'agit d’illustrer par un jeu monumental pour les enfants la thématique de la forêt, qui entoure le village et participe à son économie avec tous les métiers de la filière bois. 

La représentation de la forêt traduit aussi la transformation du tourisme, tourné vers la nature, éco-responsable et bienveillant. Elle trouve aussi un écho dans les propos de l’encyclique _Laudati Si’_.

Pour une présentation plus détaillée du [projet ici](/projets-avenir/gerer-le-bien-commun/un-jeu-monument-sur-le-parc-du-val-d-or/).

## Une première étape

Le recueil de ces cartes postales est une première étape. La synthèse des idées recueillies permettra de repérer des pistes pour les discussions qui devraient être engagées cet hiver avec les habitants. Puis tous ces éléments seront confiés à un maître d’œuvre qui pilotera la conception et la réalisation

L’ambition de faire de ce projet un projet collectif pourrait aller jusqu’à la fabrication de certains éléments par la population lors d’ateliers participatifs encadrés par des professionnels ou corporations afférents à la filière bois.

Parallèlement, l'équipe municipale a lancé la dynamique de recherche de subventions. 