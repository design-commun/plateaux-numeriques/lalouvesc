+++
date = 2021-06-18T22:00:00Z
description = "Tout le programme de juillet. Excellents choix !"
header = "/media/cinema-le-foyer.jpg"
icon = ""
subtitle = ""
title = "Aller au ciné à Lalouvesc !"
weight = 1

+++
Que des excellents films dont deux avant-premières et un Oscar du meilleur film, félicitations aux programmateurs ! Tous les séances ont lieu à 21h au Cinéma Le Foyer à l'Abri du pèlerin.

* Samedi 10 et lundi 12 : **Poly**, [bande annonce](https://youtu.be/_RWTmSpJoLg)
* Dimanche 11 : **Adieu les cons**, [bande annonce](https://youtu.be/hVV1BpNV6m0)
* Jeudi 15 et dimanche 18 : **The Father** (vf), [bande annonce](https://youtu.be/HVCs_ahGpls)
* Samedi 17 : **Antoinette dans les Cévennes**, [bande annonce](https://youtu.be/qsbBWaCKlW4)
* Lundi 19 : **La fine fleur**, [bande annonce](https://youtu.be/Fy2_VLFItHs).
* Jeudi 22 et dimanche 25 : **Nomadland** (vf), Lion d'or à la Mostra de Venise 20201, Golden Globe du meilleur film dramatique et Oscar du meilleur film en 2021. [bande annonce](https://youtu.be/mGjTyjGHnyE)
* Mercredi 21 et samedi 24 : **Envole-moi**, [bande annonce](https://youtu.be/rVckCelfruE)
* Lundi 26 : **Profession du père,** avant-première, de Jean-Pierre Ameris avec Benoit Poelevorde, Jules Lefebvre.. [bande annonce](https://youtu.be/mlvzDyqCZWM)
* Jeudi 29 et dimanche 1er août : **Médecin de nuit**, [bande annonce](https://youtu.be/FandAD1hTbY)
* Mercredi 28 et samedi 31 : **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)
* Lundi 2 août : **Dream Horse**, avant-première, [bande annonce](https://youtu.be/qh3c0mGsKkI)

{{<grid>}} {{<bloc>}}

![](/media/profession-du-pere.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/dream-horse.jpg)

{{</bloc>}}{{</grid>}}