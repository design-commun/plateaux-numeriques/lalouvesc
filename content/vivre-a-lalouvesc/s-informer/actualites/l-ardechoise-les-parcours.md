+++
date = 2022-05-10T22:00:00Z
description = "Heures de passage à Lalouvesc, routes privatisées"
header = "/media/ardechoise-6.jpg"
icon = ""
subtitle = ""
title = "L'Ardéchoise, les parcours"
weight = 1

+++
Ça sent l'été ! L'Ardéchoise s'annonce. Elle aura lieu du mercredi 15 au samedi 18 juin. Plus de 10.000 cyclistes sont attendus.

Pour Lalouvesc, les jours et heures de passage prévus sont :

* mercredi 15 juin de 10h20 à 18h30,
* samedi 18 juin de 9h30 à 19h45.

Comme chaque fois, plusieurs routes seront privatisées le samedi (voir carte ci-dessous). La route de Rochepaule-Lalouvesc-Col du Buisson sera privatisée de 8h30 à 20h30.

Plus de détails dans le Bulletin de juin.

![](/media/carte-ardechoise-2022.jpg)