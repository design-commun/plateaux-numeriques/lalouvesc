+++
date = 2022-04-16T22:00:00Z
description = "Très bientôt des nouvelles..."
header = "/media/teaser-jeu-monument-avril-2022.jpg"
icon = ""
subtitle = ""
title = "Jeu-monument : report de la présentation du 22 avril"
weight = 1

+++
Suite à des problèmes familiaux, les lauréats du concours d'idées sur le Jeu-monument ne pourront malheureusement pas assurer leur présentation prévue le 22 avril.

Nous allons organiser d'autres modalités pour vous montrer leur projet et pour recueillir vos impressions. Très bientôt des nouvelles...