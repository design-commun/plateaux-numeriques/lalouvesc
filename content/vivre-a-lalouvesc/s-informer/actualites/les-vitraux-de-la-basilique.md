+++
date = 2022-01-19T23:00:00Z
description = "Une visite sans quitter vos pantoufles"
header = "/media/vitraux-basilique-site-l-begule.jpg"
icon = ""
subtitle = ""
title = "Les vitraux de la Basilique"
weight = 1

+++
Avec un thermomètre au plus bas, on vous propose une visite virtuelle des vitraux de la basilique de Lalouvesc sans quitter vos pantoufles grâce au site consacré à leur auteur : le verrier Lucien Bégule.
Cliquez sur l'image :

[![](/media/vitraux-basilique-site-l-begule.jpg)](http://www.vitraux-begule.com/pages/lieux_regions/la_louvesc/plan_la_louvesc.htm)