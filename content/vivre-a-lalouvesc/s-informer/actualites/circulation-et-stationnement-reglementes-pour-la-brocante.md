+++
date = 2022-08-23T22:00:00Z
description = "Organisation du 4 septembre"
header = "/media/brocante-3.jpg"
icon = ""
subtitle = ""
title = "Circulation et stationnement réglementés pour la brocante"
weight = 1

+++
La brocante approche et, en concertation avec le Comité des fêtes, la mairie a pris les dispositions suivantes pour une organisation optimale de la circulation et des stationnements des véhicules.

### Stationnements interdits

Le stationnement de tout véhicule sera interdit du vendredi 2 septembre à 20h au dimanche 4 septembre à 22h sur le parking en dessous de la place des marronniers.

Le stationnement de tout véhicule sera interdit du samedi 3 septembre à 20h au dimanche 4 septembre à 20h :

* chemin Grosjean,
* tout le lotissement du Val d'Or :
* impasse du lotissement du « Val d'Or »
* route de Bobignieux à partir du chemin Grosjean jusqu'à la basilique
* place des Trois Pigeons
* dans toutes les rues déjà en stationnement interdit (Alpes, Cévennes, Fontaine, Annonay; etc)

### Circulation interdite

Les voies mentionnées ci-dessus seront fermées à la circulation le dimanche 4 septembre 2022 de 4h à 20h.

### Parkings

Des parkings seront mis à la disposition des visiteurs : place du Lac, montée du camping, parking Camping, parking Fontaine, Ste Monique, terrain de foot, tennis route de Rochepaule, terrain Coste, scierie Poinard (voir fléchage).

### Emplacements pour la vente

En dehors des 270 emplacements payants et réservés pour la brocante et des devantures des commerces payant un droit de place tout déballage sur les voies publiques sera interdit.