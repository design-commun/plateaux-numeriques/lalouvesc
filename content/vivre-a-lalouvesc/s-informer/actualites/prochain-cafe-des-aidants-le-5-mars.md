+++
date = 2022-03-01T23:00:00Z
description = "A 10h, Auberge de St Félicien"
header = "/media/admr-aide_seniors2.jpg"
icon = ""
subtitle = ""
title = "Prochain Café des aidants le 5 mars"
weight = 1

+++
Le prochain café des aidants de St Félicien aura lieu samedi 5 mars 2022 de 10h à 11h30 à l'Auberge de St Félicien sur le thème « Ces inquiétudes qui ne me quittent plus »  
Les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).

Vous avez la possibilité de venir avec la personne que vous aidez, un relais sera mis en place par le collectif KANLARELA (actions artistiques et de bien-être à destination de personnes ayant des maladies neuro-évolutives, en situation de handicap ou d'isolement, ainsi qu'à leurs proches aidants) - gratuitement - sur inscription.

Renseignements auprès de Carole Guilloux : [cguilloux@fede07.admr.org](mailto:cguilloux@fede07.admr.org) ou 06 81 50 19 26

![](/media/cafe-des-aidants-2022-1.jpg)