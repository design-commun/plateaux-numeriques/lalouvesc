+++
date = 2021-03-08T23:00:00Z
description = "Rien ne sert d'embellir, si, par ailleurs, on salit faute d'une discipline minimale."
header = "/media/dechets-sauvages-4.jpg"
icon = ""
subtitle = ""
title = "Réduire les incivilités"
weight = 1

+++
La Mairie, appuyée par une grande partie des Louvetous, a lancé un mouvement pour le renouveau et l'embellissement du village. Ce mouvement doit passer aussi par la réduction des incivilités. Rien ne sert d'embellir, si, par ailleurs, on salit faute d'une discipline minimale. La rue est notre bien commun, le prolongement de notre maison. Nous devons en prendre soin.

### Déchets encombrants

Depuis quelque temps nous remarquons des dépôts d’encombrants dans les conteneurs d’ordures ménagères (pneus, grillages, tôles), cela est fortement **INTERDIT**. Le tri des déchets est un devoir pour tous. Il évite des pollutions et des coûts excessifs pesant sur tout le monde.

Nous vous rappelons que la déchèterie communautaire du Val d’Ay est largement ouverte : lundi, mardi, vendredi, samedi, 9h-12h et 13h-16h. Si vous rencontrez des difficultés réelles pour y amener vos déchets encombrants ou dangereux, contactez la Mairie. Nous essaierons de trouver ensemble une solution.

![](/media/dechets-sauvages-1.jpg)

### Déjections canines

Si les chiens sont les bienvenus sur l'espace public, leurs déjections le sont beaucoup moins : elles salissent trottoirs ou espaces verts et participent à la prolifération des microbes. Bien que n'importe quel déchet déposé sur le sol puisse gâcher une chaussée ou un parterre fleuri, rien n'égale des crottes de chien non ramassées. De quoi mettre tout le monde de mauvaise humeur et rebuter les visiteurs du village...

J'aime mon chien, j'en suis responsable et j'en prends soin !

**Le chien n’est pas responsable de ses crottes laissées sur le trottoir**

**LE VRAI COUPABLE C’EST LE PROPRIÉTAIRE !**

Toutes ces incivilités sont passibles d'amendes. Si elles se poursuivent, le Mairie prendra des mesures plus sévères.