+++
date = 2022-11-29T23:00:00Z
description = "Renseignements"
header = "/media/logo-restos-du-coeur.jpg"
icon = ""
subtitle = ""
title = "Les restos du coeur passent par Lalouvesc"
weight = 1

+++
Les restos du cœur d'Annonay font la route et passeront  prochainement à Lalouvesc.

Pour plus d'information contacter la mairie : mairie@lalouvesc.fr, tél 04 75 67 83 67.

... ou directement les restos : ad07.annonay@lesrestosducoeur.org, tél 04 75 69 71 85 (n'hésitez pas à laisser un message).