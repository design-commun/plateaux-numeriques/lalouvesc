+++
date = 2021-12-30T23:00:00Z
description = "25 photos remarquables du Val d'Ay. Allez les voir..."
header = "/media/calendrier-de-l-avent-2021-ot-2.jpg"
icon = ""
subtitle = ""
title = "Le calendrier de l'Avent de l'Office du tourisme"
weight = 1

+++
L'Office du tourisme a décliné un calendrier de l'avent tout au long du mois de décembre à la fois sur sa [page Facebook](https://www.facebook.com/OTValDAy/) et dans sa vitrine. Allez le voir !

![](/media/calendrier-de-l-avent-2021-0t.jpg)

Saurez-vous retrouver tous les lieux photographiés ? [Réponses sur ce document](/media/les-photosducalendrierdel-avent.pdf).

C'est aussi l'occasion de souhaiter **une bonne année et un grand merci** à Camille Roberjot apprentie à l'office de tourisme et en alternance depuis le 16 juillet, Thibault Montchalin en stage à l'office de tourisme du 16 novembre au 24 décembre et, bien sûr, à Séverine Moulin, directrice de l'Office du tourisme dont l'infatigable promotion du Val d'Ay nous aide beaucoup.