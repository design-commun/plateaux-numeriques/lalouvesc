+++
date = 2022-10-27T22:00:00Z
description = "Ue canalisation a rompu"
header = "/media/robinet-eau.jpg"
icon = ""
subtitle = ""
title = "Un nouveau souci avec l'eau"
weight = 1

+++
Une canalisation principale d'alimentation du réservoir du mont Chaix s'est rompue dans la nuit. Une partie du réseau alimentant quelques maisons du quartier de la fontaine St Régis a dû être coupée.

La réparation est en cours. Nous sommes désolés pour ces difficultés qui montrent encore une fois combien notre réseau mérite une rénovation d'ensemble.