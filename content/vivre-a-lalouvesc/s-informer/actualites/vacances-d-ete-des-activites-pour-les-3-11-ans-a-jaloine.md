+++
date = 2022-06-06T22:00:00Z
description = "Trois semaines au mois de juillet, deux au mois d'août"
header = "/media/centre-de-loisir-jaloine-2921.jpg"
icon = ""
subtitle = ""
title = "Vacances d'été : des activités pour les 3-11 ans à Jaloine"
weight = 1

+++
Envie d'aventure, de découvrir l'école des loisirs avec Harry Potter ou de folie avec Mario Party ?

Venez vite découvrir le programme du centre de loisirs de Jaloine, "La cabane des marmots", pour les enfants de 3 à 11 ans.

Infos et renseignements par mail [cdl.jaloine@gmail.com](mailto:cdl.jaloine@gmail.com) ou 07.66.49.07.90

[Bulletin d'inscription](/media/fiche-inscriptions-ete-2022-jaloine.pdf).

Le centre est ouvert tous les mercredis et les petites vacances (sauf à Noël, cela évoluera sans doute) de 7h30 à 18h pour les enfants de 3 à 11 ans.

Le centre est ouvert également tout l'été sauf les trois premières semaines d'août. Possibilités d'inscription à la carte.

Renseignement au : 07.66.49.07.90 [cdl.jaloine@gmail.com](mailto:cdl.jaloine@gmail.com)

![](/media/jaloine-1-ete-2022.png)![](/media/jaloine-2-ete-2022.png)