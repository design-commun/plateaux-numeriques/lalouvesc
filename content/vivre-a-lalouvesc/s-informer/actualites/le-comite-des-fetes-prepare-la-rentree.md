+++
date = 2022-08-03T22:00:00Z
description = "Une nouvelle affiche pour la brocante, une nouvelle vidéo pour le trail"
header = "/media/affiche-brocante-2022-2.jpg"
icon = ""
subtitle = ""
title = "Le Comité des fêtes prépare la rentrée..."
weight = 1

+++
La saison bat son plein, les visiteurs viennent à Lalouvesc se rafraichir sur les terrasses, dans la forêt, à l'intérieur des murs en pierre du CAC pour l'exposition du Carrefour des Arts, ou ceux de la Basilique pour les concerts ou encore ceux de l'Abri du pèlerin pour le théâtre et le cinéma (voir le [programme du mois d'août](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/25-musique-maestro/#programme-des-festivit%c3%a9s-du-mois-dao%c3%bbt)).

Et pendant ce temps, le Comité des fêtes prépare la rentrée. Découvrez l'affiche de la brocante et la vidéo du Trail 2022 :

![](/media/affiche-brocante-2022-1.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/HH68ibYv6po" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>