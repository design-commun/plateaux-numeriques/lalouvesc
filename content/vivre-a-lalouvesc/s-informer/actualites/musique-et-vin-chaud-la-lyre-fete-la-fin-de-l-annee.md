+++
date = 2022-12-13T23:00:00Z
description = "Rendez-vous devant la Mairie le 30 décembre à 17h30"
header = "/media/lyre-louvetonne-17-juillet-2020.jpg"
icon = ""
subtitle = ""
title = "Musique et vin chaud : la Lyre fête la fin de l'année"
weight = 1

+++
## Avis à la population louvetonne !

Pour fêter dignement la fin d'année, la Lyre louvetonne propose un concert gratuit devant la Mairie suivi d'un vin chaud... le vendredi 30 décembre à 17h30.

Tout le monde est bienvenu. En cas d'intempéries, un repli est prévu salle du Conseil.