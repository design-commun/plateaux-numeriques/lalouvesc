+++
date = 2021-04-15T22:00:00Z
description = "Le point sur l'élargissement des vaccinations... "
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Vaccination pour les plus de 55 ans et les plus de 60 ans"
weight = 1

+++
L'Association des Maires de France a fait une synthèse sur les nouvelles mesures de vaccination dont nous extrayons les élément ci-dessous.

#### Elargissement de la vaccination

**Le ministère a élargi cette semaine les tranches d’âge éligibles à la vaccination**  
A partir du 12 avril, la vaccination en ville avec AstraZeneca est autorisée pour les plus de 55 ans sans co-morbidité.  
A partir du 15 avril , le vaccin Pfizer sera accessible pour les plus de 60 ans sans co-morbidité.

Par ailleurs, le **délai entre les deux injections pour les vaccins à ARN messager est rallongé et passe à 42 jours** à partir des rendez-vous du 14 avril . cette décision vise à augmenter le nombre de premières injections.

Il est décidé la nécessité d’une 3e injection pour les personnes immuno-déprimées.

#### Renforcement pour les plus de 75 ans

Depuis le 31 mars la CNAM déploie son dispositif « aller vers » pour permettre aux personnes âgées de plus de 75 ans non encore vaccinées d’avoir un accès à un rendez-vous de vaccination.

2 actions ont été mises en place :

* Envoi de sms avec un lien vers un téléopérateur qui doit orienter ces personnes vers la vaccination. 790 sms envoyés avec une relance vers les mêmes personnes, pour un taux d’appel de 5%.
* Appels sortants par les CPAM pour proposer des rdv de vaccination aux + de 75 ans non vaccinées : 200.000 appels effectués par les CPAM, dont 50% d’appels échoués.

Concernant les refus d’un rendez-vous de vaccination, les principaux motifs avancés sont liés à l’éloignement des centres de vaccination ou le souhait de se faire vacciner en même temps que le conjoint.

Outre ces deux actions, **un courrier sera envoyé par la CPAM aux plus de 75 ans avec un numéro coupe-file à appeler pour prendre un rdv. 5% des rdv en centres de vaccination doivent être réservés à cette cible des personnes de plus de 75 ans non vaccinées.** Cette nouvelle consigne est en cours d’application.

#### Vaccinations en Ardèche

Enfin, voici l'état de l'évolution de la vaccination en Ardèche. Le taux de ce jour en Ardèche est de 19,1% contre 17,4% sur toute la France (source Santé publique France).

![](/media/vaccination-ardeche-covid-19.jpg)