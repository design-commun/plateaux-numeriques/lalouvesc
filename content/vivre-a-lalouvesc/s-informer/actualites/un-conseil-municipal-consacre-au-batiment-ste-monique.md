+++
date = 2021-06-21T22:00:00Z
description = "Quelle réponse donner à l'offre d'achat d'un promoteur ?"
header = "/media/sainte-monique.JPG"
icon = ""
subtitle = ""
title = "Un Conseil municipal consacré au bâtiment Ste Monique le 24 juin"
weight = 1

+++
Un Conseil municipal est convoqué pour le 24 juin à 20h. Compte tenu des mesures sanitaires toujours en vigueur, il se tiendra à huis-clos.

## Ordre du jour

1\. VIE MUNICIPALE  
2\. COMMISSION FINANCES  
\- Modification tarifs nouveaux hébergements du camping.

3\. COMMISSION GESTION  
\- Décision sur la vente du bâtiment Ste Monique

4\. COMITÉ VIE LOCALE

5\. COMITÉ DÉVELOPPEMENT  
\- Compte-rendu de la première réunion

6\. DIVERS  
\- Point travaux de la salle de conseil (décoration)  
\- Questions diverses.

## Communiqué du Maire

_Chers concitoyens,_

_Comme vous le savez, la récente crise sanitaire à laquelle s'ajoute la crise environnementale est en train de changer la donne sur le marché immobilier dans les villages comme le nôtre. Témoin de ce retournement de tendance, nous avons été contactés par un promoteur qui, ayant fait une étude de marché sur notre territoire, était à la recherche d'investissements._

_Il a visité le bâtiment Sainte Monique et nous a fait une offre qui doit être maintenant discutée par le Conseil municipal. C'est pourquoi j'ai convoqué un Conseil dédié à cette question importante. Vendre ou non Sainte Monique est une décision qui ne doit pas être prise à la légère._

_D'un côté, la vente du bâtiment procurera à la Commune une capacité d'autofinancement bienvenue pour l'avenir et les demandes de subvention sur les nombreux dossiers indispensables à l'entretien et au développement du village. Le Comité de développement qui s'est réuni la semaine dernière a commencé à réfléchir aux orientations à prendre, et les dossiers ne manquent pas. Vous en aurez très bientôt le compte rendu et chacun, évidemment, pourra participer à cette réflexion._

_D'un autre côté, se défaire d'un patrimoine est toujours une perte, surtout lorsque ce dernier a marqué l'histoire du village. De ce point de vue, si la vente est conclue il nous faudra être attentifs aux projets du promoteur quant à la transformation et l'usage du bâtiment._

_Quel que soit le résultat de la discussion du Conseil, soyez sûr que l'équipe saura prendre ses responsabilités comme elle l'a toujours fait depuis un an maintenant._

_Nous œuvrons tous pour le bien de la Commune._

_Bien à vous_

Jacques Burriez, Maire de Lalouvesc