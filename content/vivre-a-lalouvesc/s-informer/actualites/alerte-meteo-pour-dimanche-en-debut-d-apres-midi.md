+++
date = ""
description = "La vigilance orange pour orages violents a été étendue à l'Ardèche."
header = "/media/vigilance-orange-juin-2021.png"
icon = ""
subtitle = ""
title = "Alerte météo pour dimanche en début d'après-midi"
weight = 1

+++
La vigilance orange pour orages violents a été étendue à l'Ardèche. Pour notre département les orages sont prévus plutôt dimanche 20 juin en début d'après-midi. Ils pourront être très violents localement avec fortes pluies ou grêle et des rafales pouvant atteindre 80 à 100 km/h.

Soyez prudents !