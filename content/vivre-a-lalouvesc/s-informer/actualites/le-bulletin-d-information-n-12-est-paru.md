+++
date = 2021-06-30T22:00:00Z
description = "Bulletin de Juillet 2021 - Une page est tournée"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Juillet 2021 - Une page est tournée"
title = "Le Bulletin d'information n°12 est paru"
weight = 1

+++
Avec l'ouverture de la saison estivale 2021, une saison au programme bien rempli, c'est aussi une page de l'histoire du village qui se tourne. Avec la vente du bâtiment Sainte Monique, les premiers travaux du Comité de développement, la rénovation complète du camping, l'appel à imaginer les contours d'un jeu-monument sur le Parc du Val d'Or et tous les autres projets encore dans les cartons, c'est un nouveau chapitre de l'histoire du village que nous écrivons tous ensemble. Dans ce bulletin vous en trouverez quelques prémisses, et, comme toujours, aussi plein d'autres informations...

[Lire la suite](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/12-une-page-est-tournee/)