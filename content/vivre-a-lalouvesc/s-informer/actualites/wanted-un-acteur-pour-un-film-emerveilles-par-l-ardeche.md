+++
date = 2022-07-29T22:00:00Z
description = ""
header = "/media/office-de-tourisme-sitra-400x266.jpg"
icon = ""
subtitle = ""
title = "Wanted : un acteur pour un film \"Émerveillés par l'Ardèche\""
weight = 1

+++
L'ADT communique :

Dans le cadre de nos actions pour valoriser le "Vivre et travailler en Ardèche", nous allons produire 3 nouveaux films dans la suite de notre série ["Ils ont choisi l'Ardèche"](https://www.youtube.com/watch?v=v5YfCXeSYBM&t=1s) produite en 2019. Ces films auront le même format, environ 2 minutes, avec en plus la réalisation d'un petit teaser dynamique (30sec max). 

Pour répondre au mieux aux besoins du territoire, nous souhaitons orienter une des 3 vidéos vers le secteur touristique, en faisant le portrait d'un saisonnier qui fait le choix de revenir en Ardèche depuis plusieurs saisons. De nombreux hébergeurs nous ayant fait part de leurs problématiques de recrutement accélérées depuis la crise COVID.

Quelques éléments de cadrage :

* Nous recherchons quelqu'un qui n'a pas de lien familial fort avec l'Ardèche. Quelqu'un qui a un regard "neuf", et plutôt objectif sur le territoire. L'idée étant de montrer des gens qui choisissent l'Ardèche. 
* Nous recherchons quelqu'un qui passe bien à l'image et qui a des choses à raconter: un parcours original, une passion, ...
* Nous recherchons quelqu'un qui travaille chez un hébergeur de qualité qui montre de belles valeurs (produits locaux, respect environnement, ...), et qui soit valorisant à l'image.
* Nous recherchons un profil sur le centre/nord Ardèche pour valoriser un tourisme autre que les têtes de gondoles du sud. 
* Cette personne doit être disponible pour une journée de tournage, sur son lieu de travail en Septembre/Octobre (en fonction des ouvertures). 
* Ce film sera l'occasion pour l'employeur de valoriser son activité et pour l'OT de mettre un coup de projecteur sur son secteur géographique.

S'adresser à l'Office du tourisme : 04 75 67 84 20