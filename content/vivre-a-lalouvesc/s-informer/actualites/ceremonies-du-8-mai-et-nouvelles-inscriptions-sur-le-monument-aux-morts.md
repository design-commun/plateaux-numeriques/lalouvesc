+++
date = 2022-05-07T22:00:00Z
description = "Défilé, allocutions, Lyre louvetonne et inscription de trois Louvetous morts pour la France au cours de la guerre de 1870"
header = "/media/8-mai-2022-4.jpg"
icon = ""
subtitle = ""
title = "Cérémonie du 8 mai et nouvelles inscriptions sur le Monument aux morts"
weight = 1

+++
La commémoration de la fin de la seconde guerre mondiale a été célébrée à Lalouvesc comme dans toute la France. A partir de la Basilique les représentants des anciens combattants, accompagnés de la Lyre louvetonne et des pompiers ont défilé jusqu'au Monument aux morts où les attendait le Maire.

{{<grid>}}{{<bloc>}}

![](/media/8-mai-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/8-mai-2022-2.jpg)

{{</bloc>}}{{</grid>}}

![](/media/8-mai-2022-3.jpg)

Celui-ci a lu le message de la ministre déléguée auprès de la ministre des Armées, chargée de la mémoire et des anciens combattants. Il a ajouté un mot plus particulier au village, attirant l'attention sur l'inscription sur le Monument de trois nouveaux noms de Louvetous morts pour la France au cours de la guerre de 1870. Inscription réalisée sur la suggestion de Paul Besset (voir ci-dessous) et applaudie par les Louvetous présents.

Une gerbe a été déposée. Après une minute de silence, la Lyre a joué la _Sonnerie aux morts_, puis la _Marseillaise_.

{{<grid>}}{{<bloc>}}

![](/media/8-mai-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/8-mai-2022-6.jpg)

{{</bloc>}}{{</grid>}}

![](/media/8-mai-2022-lalouvesc-7.jpg)

{{<grid>}}{{<bloc>}}

![](/media/inscription-monument-aux-morts-1870-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/inscription-monument-aux-morts-1870-2022-2.jpg)

{{</bloc>}}{{</grid>}}

### Message de Paul Besset

> **« 1870, Année terrible »** _Victor Hugo_.
>
> Dressé sur ses ergots à la lecture de la dépêche d’Ems du 13 juillet 1870 expédié par le chancelier Bismarck, le coq gaulois se lança dans l’aventure hasardeuse de la guerre contre la Prusse.
>
> Le conflit de 1870-1871, qui engendra les deux suivants, a complètements disparu de la mémoire collective. Les noms des victimes (139 000 victimes françaises) de cette imbécile guerre ont été occultés par ceux des hécatombes de 1914-1918 (1 700 000 victimes françaises) et de 1939-1945 (570 000 victimes françaises).
>
> Victor Hugo composa en son temps l’Hymne à la gloire des hommes qui périrent dans cette lutte contre les Prussiens. Cet Hymne, que l’on apprenait dans les écoles de la République, est gravé en frontispice sur notre monument aux morts.
>
> _« Ceux qui pieusement sont morts pour la patrie…. »_
>
> Le département de l’Ardèche a dressé un monument à la gloire des mobiles à Privas. La ville d’Annonay a également un monument à la mémoire des combattants de 1870. Cependant, il nous a semblé injuste que ces soldats défaits, mais néanmoins valeureux, soient oubliés de nos concitoyens de Lalouvesc. C’est pour réparer cet oubli de 150 ans que nous avons jugé nécessaire de graver sur notre monument aux morts le nom des trois soldats louvetous tombés en servant la France.
>
> · BUISSON Joseph (26 ans) du village
>
> · DELHOME Euphème (29 ans) de Bobignieux
>
> · ENTRESSANGLE Jean-Marie (32 ans) du village
>
> _Ceux qui pieusement sont morts pour la patrie  
> Ont droit qu'à leur cercueil la foule vienne et prie.  
> Entre les plus beaux noms leur nom est le plus beau.  
> Toute gloire près d'eux passe et tombe éphémère ;  
> Et, comme ferait une mère,  
> La voix d'un peuple entier les berce en leur tombeau !_

Rappel : Le club des deux clochers a édité une plaquette mémoire sur ces évènements lors d’une exposition éphémère en 2020. Cette plaquette est toujours disponible.