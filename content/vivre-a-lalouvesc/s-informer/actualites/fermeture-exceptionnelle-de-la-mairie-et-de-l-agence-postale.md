+++
date = 2022-10-19T22:00:00Z
description = "Lundi 31 octobre"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Fermeture exceptionnelle de la Mairie et de l'Agence postale"
weight = 1

+++
En raison du week-end de la Toussaint, la Mairie et l'Agence postale resteront fermées le 31 octobre 2021.

Réouvertures le 2 novembre.