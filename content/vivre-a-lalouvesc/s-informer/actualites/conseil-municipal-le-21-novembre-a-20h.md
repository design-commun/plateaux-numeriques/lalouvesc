+++
date = 2022-11-14T23:00:00Z
description = "Ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 21 novembre à 20h"
weight = 1

+++
Le prochain Conseil municipal se tiendra le lundi 21 novembre 2022 à 20h à la mairie, salle du Conseil.

### Ordre du jour

1. COMMISSION FINANCES
   1. Décision modificative n°3 du budget 2022
   2. Tarifs 2023 du camping et des gîtes
   3. Tarif des photocopies
   4. Ajustement des charges des locations des logements appartenant à la Commune
   5. Passage à la nouvelle nomenclature M57 abrégée au 1er janvier 2023
   6. Proposition de prix de l’assurance SMACL
   7. Achat d’un véhicule
2. COMMISSION GESTION
   1. Point sur les embauches des employés communaux
   2. Vote pour le rattachement au comité de commande du SDE 07
   3. Délibération pour l’emploi d’un agent administratif avec accroissement temporaire d’activité
   4. Convention déneigement avec un agriculteur
   5. Point sur les travaux à prévoir pour la basilique (plancher, toiture)
3. COMITÉ VIE LOCALE
   1. Point sur l’organisation du recensement 2023
   2. Bilan de la saison 2022 du camping
   3. Cadeau de fin d’année pour les anciens (Repas/Babets)
   4. Point sur la mise en place de l’adressage
   5. Point sur la mise en place du nouveau système d’ordures ménagères
4. COMITÉ DÉVELOPPEMENT
   1. Point sur les difficultés constatées sur le réseau d’eau potable et sur les travaux prévus sur les réseaux d’eau et assainissement
   2. Nouveaux raccordements au réseau d’assainissement
   3. Point sur l’étude du SCOT sur l’avenir de St Monique et du Cénacle
   4. Point sur la proposition d’Archipolis sur l’aménagement du site Beauséjour
5. DIVERS
   1. Information sur le terrain « La Vialette »
   2. Candidature au Grand Atelier des maires ruraux pour la transition écologique (AMRF)