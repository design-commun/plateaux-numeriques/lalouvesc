+++
date = 2022-03-23T23:00:00Z
description = ""
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte rendu du Conseil municipal"
weight = 1

+++
Le [compte rendu du premier Conseil de l'année](/media/2022-1-conseil-municipal-lalouvesc.pdf) est consultable.

### Ordre du jour

1. COMMISSION FINANCES
   1. Autorisation donnée au maire à engager des crédits d'investissement 2021 dans l'attente du vote du budget primitif 2022
   2. Subventions aux associations
   3. Rémunération du travail d’un élu pour l’aménagement du local des employés municipaux
   4. Dénonciation d’un compromis de vente (bâtiment Ste Monique)
   5. Proposition d’achat de parcelles boisées par l’EHPAD Le Balcon des Alpes
   6. Provisions pour créances à recouvrer
   7. Point sur la préparation du budget primitif 2022
   8. Taux de la taxe foncière pour 2022
   9. Devis de la SDE07 pour l’éclairage et le réseau de télécom pour l’écolotissement.
2. COMMISSION GESTION
   1. Informations sur les emplois communaux et ouverture de postes pour la saison à venir
   2. Point sur les chantiers
   3. Point sur la récupération du City Park
   4. Aménagements complémentaires du camping
   5. Point sur l’adressage
3. COMITÉ VIE LOCALE
   1. Organisation des journées citoyennes
   2. Point sur le fleurissement
   3. Point sur le club des ados
   4. Transformation numérique des territoires - Subvention France Relance
4. COMITÉ DÉVELOPPEMENT
   1. Point sur le jeu monument
   2. Rencontre avec EPORA
   3. Parcelle de M. BOL (ancien gérant de la Vie Tara)
   4. Indivision Simone CHAVET - rue St Jean François Régis/EPORA
5. DIVERS