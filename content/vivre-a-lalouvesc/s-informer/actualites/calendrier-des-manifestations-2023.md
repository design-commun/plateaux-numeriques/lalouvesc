+++
date = 2022-11-08T23:00:00Z
description = "Déjà bien fourni, mais à compléter"
header = "/media/carrefour-des-arts-2022-ecole-5.jpeg"
icon = ""
subtitle = ""
title = "Calendrier des manifestations 2023"
weight = 1

+++
Le [calendrier des manifestations 2023](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/manifestations-et-evenements-a-l-affiche-2023/) est en ligne. Il est encore incomplet car bien des événements sont en cours de programmation et d'autres ont peut-être été oubliés.

Cette page est très consultée sur le site. Si vous repérez des erreurs ou des oublis, si vous prévoyez des événements publics pour 2023, même si la date est encore incertaine, n'hésitez pas à le faire savoir à mairie@lalouvesc.fr