+++
date = 2021-10-07T22:00:00Z
description = "Programme du Centre de loisir, inscriptions du 11 au 15 octobre"
header = "/media/ccva.jpg"
icon = ""
subtitle = ""
title = "Vacances de la Toussaint : des activités pour les 3-11 ans à Jaloine"
weight = 1

+++
Le programme du centre de loisirs de Jaloine (3-11 ans) pour les vacances de la Toussaint est [disponible](/media/prg-automne-2021-jaloine.pdf).

Les inscriptions débuteront la semaine prochaine, du 11 au 15 octobre. 

Plus d'information : [cdl.jaloine@gmail.com](mailto:cdl.jaloine@gmail.com) ou par téléphone 07.66.49.07.60