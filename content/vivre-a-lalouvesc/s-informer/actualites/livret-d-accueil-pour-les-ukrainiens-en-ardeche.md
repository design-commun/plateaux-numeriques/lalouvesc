+++
date = 2022-04-07T22:00:00Z
description = "Publié par la Préfecture"
header = "/media/ukraine-ardeche-2022.jpg"
icon = ""
subtitle = ""
title = "Livret d'accueil pour les Ukrainiens en Ardèche"
weight = 1

+++
La Préfecture met à disposition un[ livret bilingue Ukrainien/Français](/media/livret-bienvenue-en-ardeche-ukrainiens-2022.pdf) pour les ressortissants ukrainiens accueillis en Ardèche.

Si vous accueillez un ou des ressortissants ukrainiens sur le village, merci de bien vouloir le signaler à la Mairie.