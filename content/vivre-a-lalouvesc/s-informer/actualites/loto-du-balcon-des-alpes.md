+++
date = 2023-02-20T23:00:00Z
description = "26 mars à 14h au CAC, au profit des résidents de l'EHPAD"
header = "/media/ehpad-lalouvesc.jpg"
icon = ""
subtitle = "26 mars à 14h au CAC, au profit des résidents de l'EHPAD"
title = "Loto du Balcon des Alpes"
weight = 1

+++
2€ le carton, possibilité d'acheter les cartons à l'avance à l'EHPAD si vous n'êtes pas disponible le 26 mars. Nombreux lots à gagner !

* Tél 06 48 32 84 86 / 06 83 04 47 60
* assoc.mdr.lalouvesc@orange.fr

![](/media/loto-ehpad-mars-2023.jpg)