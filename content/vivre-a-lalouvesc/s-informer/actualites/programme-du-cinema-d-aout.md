+++
date = 2022-07-19T22:00:00Z
description = "Toujours au top (gun), avec dégustations et avant-premières"
header = "/media/cinema-le-foyer.jpg"
icon = ""
subtitle = ""
title = "Programme du cinéma d'août"
weight = 1

+++
L’été est chaud alors poursuivons notre saison cinéma avec un mois d’août rafraîchissant de nouveautés et de bonnes surprises !

Nous vous présentons deux films d’Art et Essai à ne manquer sous aucun prétexte. Tout d’abord _L’École du bout du monde_, bouleversant, qui rappelle à notre société connectée que dans les régions les plus reculées du monde, des enfants ont soif d’enseignement. _L’Affaire Collini_, un film passionnant issu histoire vraie, qui met l’accent sur les ravages psychologiques et la mise en cause des pratiques d’après guerre pour protéger les criminels nazis.

Pas moins de trois avant-premières, _L’Innocent_, signé par Louis Garrel avec Lyon comme décor, _Les Volets Verts_, adapté du roman de G. Simenon et Revoir Paris un film coup de poing inspiré des attentats du 13 novembre. Une bonne surprise avec _Les Folies Fermières_, l’histoire d’un éleveur qui ouvre un cabaret pour sauver sa ferme. Un documentaire sur la réalisation du film sera projeté à 17h, suivi d’un buffet proposé par des artisans charcutiers, fromagers avant la diffusion du film à 21h.

Nous n’oublions pas les plus jeunes avec _Buzz l’Eclair_, _Jurassic World 3_, _La petite Bande_ et les inconditionnels de _Top Gun_.

Alors venez nombreux pour continuer à faire vivre votre cinéma. Bel été à tous !

L'équipe du Cinéma Le Foyer

## Tarifs

* Plein tarif : 6€ 50
* Tarif réduit (-de 18 ans et étudiants / sur présentation d'un justificatif) : 5€50

## Adresse

Abri du pèlerin

## Programme

* Jeudi 4 août 21h TOP GUN ([bande annonce](https://youtu.be/V4gQdk1nAn0))
* Samedi 6 août 21h L'ÉCOLE DU BOUT DU MONDE ([bande annonce](https://youtu.be/hca-7AKq07w))
* Dimanche 7 août 21h TOP GUN ([bande annonce](https://youtu.be/V4gQdk1nAn0))
* Lundi 8 août 21h L'INNOCENT · AVANT-PREMIÈRE ([bande annonce](https://youtu.be/6wl4B5lJgBM))
* Mercredi 10 août
  * 17h30 DE FOLIES EN FOLIES, documentaire, Expo photos et dégustation de produits locaux entre les séances


  * 21h LES FOLIES FERMIÈRES ([bande annonce](https://youtu.be/KD5sB_BPLZI))
* Jeudi 11 août 21h L'AFFAIRE COLLINI ([bande annonce](https://youtu.be/4buWqDrWMBo))
* Samedi 13 août 21h LES FOLIES FERMIÈRES ([bande annonce](https://youtu.be/KD5sB_BPLZI))
* Dimanche 14 août 21h L'AFFAIRE COLLINI ([bande annonce](https://youtu.be/4buWqDrWMBo))
* Lundi 15 août 21h BUZZ L'ÉCLAIR [(bande annonce](https://youtu.be/q41VoF95fmI))
* Mercredi 17 août 21h TÉNOR ([bande annonce](https://youtu.be/-rhY0fonYwM)[)]()
* Jeudi 18 août 21h JURASSIC WORLD : LE MONDE D'APRÈS ([bande annonce](https://youtu.be/4R87S4YCtI8))
* Samedi 20 août 21h TÉNOR ([bande annonce](https://youtu.be/-rhY0fonYwM)[)]()
* Dimanche 21 août 21h JURASSIC WORLD : LE MONDE D'APRÈS ([bande annonce](https://youtu.be/4R87S4YCtI8))
* Lundi 22 août 21h LES VOLETS VERTS · AVANT-PREMIÈRE ([bande annonce](https://youtu.be/zLbz061gLzw))
* Mercredi 24 août 21h JOYEUSE RETRAITE 2 ([bande annonce](https://youtu.be/TgNiqEKEydw))
* Jeudi 25 août 21h LA PETITE BANDE ([bande annonce](https://youtu.be/npLm1oPOY14))
* Samedi 27 août 21h JOYEUSE RETRAITE 2 ([bande annonce](https://youtu.be/TgNiqEKEydw))
* Dimanche 28 août 21h LA PETITE BANDE ([bande annonce](https://youtu.be/npLm1oPOY14))
* Lundi 29 août 21h REVOIR PARIS · AVANT-PREMIÈRE ([bande annonce](https://youtu.be/idHIUQeF_cw))