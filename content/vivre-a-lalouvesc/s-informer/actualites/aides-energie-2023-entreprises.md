+++
date = 2023-01-19T23:00:00Z
description = "Toutes les aides aux TPE et PME"
header = ""
icon = ""
subtitle = ""
title = "Aides énergie 2023 - entreprises"
weight = 1

+++
La préfecture communique :

Le soutien de l’État pour les entreprises victimes de la crise énergétique se matérialise à travers cinq dispositifs selon la taille de l'entreprise et le niveau de sa consommation électrique.

1. L’abaissement de la taxe intérieure sur la consommation finale d'électricité à son minimum légal européen.
2. Une nouvelle garantie au profit des quelques 600 000 TPE, ne bénéficiant pas des tarifs réglementés et ayant renouvelé leur contrat de fourniture d'électricité au second semestre 2022, qui permet de limiter leur facture en moyenne à 280 euros maximum / MWh sur l'année 2023.
3. Cette nouvelle garantie complète le bouclier tarifaire qui limite à 15% la hausse des factures de gaz et d’électricité des TPE soumises aux tarifs réglementés, dont la consommation électrique est inférieure à 36 kVA.
4. Pour les TPE ayant un compteur électrique d’une puissance supérieure à 36 kVA, depuis le 1er janvier 2023, le dispositif de l’amortisseur électricité se traduit par une réduction appliquée directement par les fournisseurs d’énergie sur la facture d’électricité lorsque le prix du MWh souscrit dépasse 180 euros.
5. Enfin, pour l'ensemble des entreprises grandes consommatrices d’énergie, le guichet d’aide au paiement des factures de gaz et d’électricité permet de compenser la hausse des coûts d’approvisionnement de gaz naturel et d’électricité dès lors que le coût de l'énergie dépasse 3% de leur chiffre d'affaires en 2021 et que leur facture a augmenté de 50% par rapport à 2021. Ces dispositifs sont cumulables sous certaines conditions.

Les attestations à remplir par les entreprises et les démarches à effectuer pour demander à bénéficier du guichet d’aide au paiement des factures de gaz et d’électricité sont accessibles sur impots.gouv ou [ici](/media/modele_attestation_aides_energie_entreprise.pdf).

![](/media/aides-entreprises-2023.jpg)

Renseignements :

* [Informations détaillées](/media/cp_aides-energie-2023_entreprises.pdf)
* sites impots.gouv.fr,  economie.gouv.fr
* un simulateur
* un numéro de téléphone unique : 0.806.000.245 (9h00-12h00 et 13h00-18h00)
* les services instructeurs de la DGFiP via la messagerie sécurisée de l’espace professionnel

Accompagnement :

* la Conseillère Départementale à la Sortie de Crise (CDSC) de la DDFiP de l’Ardèche Laurianne LAINE : 06.27.55.75.79 / 04.75.65.55.23 / codefi.ccsf07@dgfip.finances.gouv.fr
* la Chambre de Commerce et d’Industrie (CCI) : Pour l’Ardèche, 04.75.88.07.07
* la Chambre de Métiers et de l’Artisanat (CMA) : Pour l’Ardèche, 04.75.07.54.00
* les correspondants des espaces France Service de l’Ardèche, pour Lalouvesc : SAINT ROMAIN D’AY Emmanuelle VALLON 04 75 69 18 01, saint-romain-d-ay@france-services.gouv.fr