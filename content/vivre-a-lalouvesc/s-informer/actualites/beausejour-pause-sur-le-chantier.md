+++
date = 2022-01-20T23:00:00Z
description = "La démolition est terminée"
header = "/media/demolition-beausejour-2022-72.jpg"
icon = ""
subtitle = ""
title = "Beauséjour : pause sur le chantier"
weight = 1

+++
La première phase de démolition est terminée. L'hôtel a disparu laissant la place à une large esplanade lumineuse.

Une seconde phase reprendra en mars quand la température sera plus clémente pour la réfection de la façade. Et puis, il faudra s'occuper des aménagements, du jardin et du futur du lieu et de ses alentours. On vous raconte tout dans le Bulletin de février...

![](/media/demolition-beausejour-2022-74.jpg)

![](/media/demolition-beausejour-2022-73.jpg)

![](/media/demolition-beausejour-2022-71.jpg)

Voir aussi toutes les photos dans :

* L'[album de la première semaine](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition/)
* L'[album de la deuxième semaine](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition-deuxieme-semaine/)