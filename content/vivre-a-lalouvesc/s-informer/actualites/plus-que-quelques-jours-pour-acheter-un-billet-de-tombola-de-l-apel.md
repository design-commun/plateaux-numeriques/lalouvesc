+++
date = 2021-03-23T23:00:00Z
description = "Pour financer les activités des enfants de l’école Saint Joseph de Lalouvesc, l’association des parents d’élèves organise une TOMBOLA. Les lots à gagner sont"
header = "/media/tombola-apel2021.jpg"
icon = ""
subtitle = ""
title = "Plus que quelques jours pour acheter un billet de tombola de l'APEL"
weight = 1

+++
Pour financer les activités des enfants de l’école Saint Joseph de Lalouvesc, **l’association des parents d’élèves organise une TOMBOLA**. Les lots à gagner sont :

1. Une TV Full HD 81 cm
2. un aspirateur balais 2 en 1
3. un appareil raclette multifonction
4. une friteuse 3L
5. une cafetière Tassimo
6. un set de tasse

Les tickets sont en vente dans nos commerces de Lalouvesc au prix de 2€ l’unité jusqu’au 5 Avril. Nous remercions vivement les commerçants de leur soutien permanent pour les événements de l’école : Epicerie Vival, Boulangerie, Pharmacie, Salon de coiffure, Boucherie, Brocanteur Fanget.

**Le tirage aura lieu le 9 avril** et se fera directement à l’école en raison de la crise sanitaire actuelle. Nous remercions d'ores et déjà tous ceux qui ont acheté leurs tickets et à ceux qui s’apprêtent à le faire !

_APEL Lalouvesc_