+++
date = 2022-10-06T22:00:00Z
description = "Deux jours d'échanges sur les attentes des territoires ruraux sur le numérique"
header = "/media/ruralite-et-numerique-2022-1.jpg"
icon = ""
subtitle = ""
title = "lalouvesc.fr montre l'exemple"
weight = 1

+++
Plateaux numériques, collectif de designers engagés dans une démarche écoresponsable pour le numérique, et la commune de Lalouvesc dont le site lalouvesc.fr sert de démonstrateur, avaient invité à une journée-et-demi d'échanges et de réflexions sur les expériences en cours.

Institutionnels, chercheurs, designers, journalistes et élus ont présenté les résultats de leurs travaux et de leurs réalisations dans une ambiance conviviale.

![](/media/ruralite-et-numerique-2022-2.jpg "Photo Plateaux numériques")

Un numérique adapté aux villages et au monde rural, répondant efficacement aux attentes locales, est possible, loin des injonctions des grands groupes ou des start-ups, pressés de vendre leurs services en captant l'attention des internautes par la multiplication des applications. D'un côté un numérique simple, accessible, qui prend le temps de mesurer en profondeur les attentes, de l'autre des alertes continues, l'obligation d'avoir un smartphone dernier cri pour une information et des échanges déconnectés des territoires et anxiogènes.

Plateaux numériques développe maintenant le site de Rocamadour et a engagé d'autres réalisations dans le Béarn, en Anjou ou dans le Beaujolais. Parmi les autres expériences décoiffantes, la newsletter [Cancan média](https://www.kisskissbankbank.com/fr/projects/cancan-studio-collecte) dans la Manche mérite d'être citée.

Plusieurs articles rendant compte des rencontres ont été publiés dans le Dauphiné Libéré, élargissant l'audience de la manifestation.

![](/media/numerique-et-ruralite-2022-dl-2.jpg)

Un compte-rendu détaillé des journées et de la richesse des échanges est en cours de rédaction par Plateaux numériques. Par ailleurs les observateurs de l'Agence nationale de la cohésion des territoires doivent aussi en rendre compte. Vous pourrez y avoir accès à tout le détail dans le Bulletin de novembre.