+++
date = 2022-02-21T23:00:00Z
description = "Recherche de fuites"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "Coupures d'eau possible dans la nuit du 23 février"
weight = 1

+++
Suite au schéma directeur du réseau d'eau, une recherche de fuite sera organisée dans la nuit du 23 au 24 février de 21h à 6h du matin. Patrick Mazoyer et la société Naldeo participeront à l'opération.

Plusieurs coupures de courtes durées seront effectuées le long du réseau tout au long de la nuit. Il est préférable d'éviter de faire couler de l'eau domestique au cours de cette nuit.