+++
date = 2023-02-27T23:00:00Z
description = ""
header = "/media/cenacle-ste-monique-scot-nov-2022.png"
icon = ""
subtitle = ""
title = "Rappel : la réunion sur Ste Monique et le Cénacle est reportée"
weight = 1

+++
Contrairement à ce qui a été annoncé dans le Dauphiné Libéré, nous vous rappelons que la réunion initialement prévue le 28 février sur l'étude menée par le SCOT a été reportée. Cette information a été donnée dans le Bulletin de février.

Des discussions et réflexions sont en cours avec des porteurs de projet sont en cours et il faut qu'elles arrivent à leur terme sans perturbation.

Tout sera expliqué dans le Bulletin de mars.