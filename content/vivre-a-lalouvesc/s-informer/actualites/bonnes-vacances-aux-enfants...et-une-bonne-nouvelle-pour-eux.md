+++
date = 2021-07-06T22:00:00Z
description = "Une animation ouverte aux enfants les lundis, mardis et jeudis"
header = "/media/ecole-st-joseph.jpg"
icon = ""
subtitle = ""
title = "Bonnes vacances aux enfants !... et une bonne nouvelle pour eux"
weight = 1

+++
Les enfants de l'[école Saint Joseph](https://www.facebook.com/Ecole-St-Joseph-Lalouvesc-107859627383105/?ref=page_internal) et tous les enfants de la région sont en vacances ce jour. Nous leur souhaitons de bonnes vacances !

Pour ceux qui restent sur Lalouvesc ou ceux qui arrivent à Lalouvesc et qui ont entre 8 et 12 ans, une animation est organisée sur le camping les lundis, mardi de 14h à 17h et jeudis de 13h à 17h à partir du lundi 19 juillet, encadrée par David Besset, titulaire du BAFA.

Au programme : jeux de ballon, balades, mini-golf, pêche, etc. Inscription gratuite, mais obligatoire au camping par un adulte responsable de l'enfant au plus tard le dimanche précédent.