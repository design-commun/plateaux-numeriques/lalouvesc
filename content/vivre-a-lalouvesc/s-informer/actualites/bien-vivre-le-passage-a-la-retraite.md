+++
date = 2022-10-04T22:00:00Z
description = "Une journée d'échanges"
header = "/media/journee-citoyenne-2022-25.jpg"
icon = ""
subtitle = ""
title = "Bien vivre le passage à la retraite"
weight = 1

+++
**Vous avez pris votre retraite depuis moins de 3 ans ou vous allez la prendre prochainement. Vous résidez dans l’Ardèche ? Vous avez des envies, des projets, des questions plein la tête : cette journée est faite pour vous !**

Ce changement de vie est un moment important : davantage de temps pour soi, création de nouveaux liens sociaux, … cette transition de vie suscite à la fois des attentes et des interrogations pour tous.

**COMMENT BIEN VIVRE VOTRE PASSAGE A LA RETRAITE ?**  
**Mercredi 19 octobre 2022**  
**De 9h30 à 17h**_  
_**DOMAINE LA GENTILHOMMIERE – 635 rue Emile Glaizal – SATILLIEU  
\**_Inscription obligatoire :_ [_a.plenet@mfara.fr_](mailto:a.plenet@mfara.fr) _– Nombre de places limité_

PROGRAMME

* 09h30 – Accueil des participants
* 09h45 – Présentation de la journée
* 10h00 – Atelier d’expression autour des représentations liées au passage à la retraite  
  _animé par le Service Prévention et Promotion de la Santé et Solange Vialle (Psychologue)_
* 12h30 – Déjeuner
* 14h15 – Liens sociaux et engament citoyen. Comment préserver le plaisir du bénévolat ?
* 15h30 – Initiation aux techniques de Bien-être et de détente  
  _avec Corinne COMBE (Professeure de Yoga)_
* 17h00 – Fin de la journée

Aurélia PLENET Responsable d’Activité, Mutualité française.

**NDLR : A Lalouvesc, les jeunes retraités (et même les plus anciens...) sont un des piliers de l'animation et du développement du village. Ils participent largement et activement aux** [**associations**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/annuaire-des-associations/) **et aux** [**journées citoyennes**](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/des-journees-citoyennes-pour-le-camping/)**. N'hésitez pas à les rejoindre, c'est festif, chaleureux... et indispensable.**