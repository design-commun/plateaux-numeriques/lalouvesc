+++
date = 2022-07-07T22:00:00Z
description = "13 juillet au soir : organisation des festivités nationales à Lalouvesc"
header = "/media/feu-st-jean-2021-3.jpg"
icon = ""
subtitle = ""
title = "Feu d'artifice, stationnement réglementé et interdictions"
weight = 1

+++
## Fête nationale

La fête se tiendra le **13 juillet au soir** à Lalouvesc.

La buvette, hot-dogs, frites, crêpes, sera tenue par l'APEL et l'USPL à partir de 19h. Bal à partir de 21h sur le parking sous la Mairie.

Le feu d'artifice sera lancé vers 22h depuis l'espace d'agrément sur le parc du Val d'Or

## Stationnement réglementé

Pour une bonne organisation de la fête, le **stationnement sera interdi**t :

* Sur le parking sous la place des marronniers du mardi 12 juillet 2022 à 14 h00 jusqu'au jeudi 14 juillet 2021 à 11h00. Cet emplacement sera réservé à l'association « l'U.S.P.L. » gérant le bal du 13 juillet 2022.
* Sur le parking pour les places habituellement réservées aux camping-cars et aux véhicules (début de la voie du lotissement le Val d'Or), le mardi 12 juillet 2022 à partir de 14h00 ;
* Sur le parking chemin Grosjean face à la zone de tir et sur la voie communale du Val d'Or, face à la zone de tir (matérialisation de la zone par des barrières et rue-balises) le mercredi 13 juillet 2022 à partir de 18h00 et jusqu'à 23h00.

## Feux par des particuliers interdits

La préfecture annonce par précaution quelques interdictions. Les mesures s'appliquent sur l'ensemble du département, **à compter du mercredi 13 juillet jusqu'au vendredi 15 juillet 2022 inclus** :

* l'interdiction d'achat et d'utilisation d'articles pyrotechniques par les particuliers ;
* l'interdiction de distribution, de vente et d'achat de carburants dans tout récipient transportable.