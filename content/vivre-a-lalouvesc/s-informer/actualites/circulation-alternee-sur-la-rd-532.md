+++
date = 2023-02-14T23:00:00Z
description = "Du 23 février au 10 mars"
header = "/media/circulation-alternee.jpg"
icon = ""
subtitle = "Du 23 février au 10 mars"
title = "Circulation alternée sur la RD 532"
weight = 1

+++
Voir l'[arrêté préfectoral](/media/019-adc-nb-23-rd0532-entreprise-ensio-de-caso-yohann-travaux-de-remise-a-la-cote-de-chambre-ft-lalouvesc.pdf). Extrait :

> Afin de permettre à l'entreprise DE CASO Yohann 50 chemin des Roquelles 07170 LAVILLEDIEU d'effectuer des travaux de remise à la cote de chambre FT, la circulation sera temporairement réglementée dans les conditions ci-après définies sur la RD 532  entre les PR 11+680 et PR 11+7000 hors agglomération de Lalouvesc.
>
> La circulation des véhicules de toutes natures sera réglementée comme suit :
>
> Du 23 février au 10 mars 2023
>
> ➢ Circulation alternée commandée par feux tricolores.
>
> ➢ Limitation de vitesse à 50 km/h au droit du chantier pendant toute la durée des travaux.
>
> ➢ Interdiction de stationner au droit du chantier.

Le chantier se trouvera près de Satillieu.

Voir l'actualité des chantiers routiers en Ardèche sur l[a carte](https://geoids.geoardeche.fr/infostrafic07/index.html).