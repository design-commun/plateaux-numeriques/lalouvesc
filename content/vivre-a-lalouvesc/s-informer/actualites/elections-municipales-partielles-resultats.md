+++
date = 2023-03-18T23:00:00Z
description = "Second tour le dimanche 26 mars 2023"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Elections municipales partielles 1er tour : ballotage"
weight = 1

+++
Les élections municipales partielles se sont tenues à Lalouvesc le dimanche 19 mars. Résultats :

* Inscrits : 339
* Votants : 77 soit 22,7% des inscrits
* Votes nuls : 4
* Votes blancs : 9
* Nicole Porte : 56 voix, 87,5% des suffrages exprimés, 16,5% des inscrits, ballotage
* Corentin Serayet :  61 voix, 95% des suffrages exprimés, 18,4% des inscrits, ballotage

Aucun des candidats n'ayant recueilli les suffrages de 25% des inscrits, il y aura un second tour le dimanche 26 mars.