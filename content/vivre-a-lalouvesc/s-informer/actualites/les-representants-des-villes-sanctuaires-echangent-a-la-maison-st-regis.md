+++
date = 2022-06-08T22:00:00Z
description = "Le colloque de l'association des Villes Sanctuaires en plein travail"
header = "/media/villes-sanctuaires.png"
icon = ""
subtitle = ""
title = "Les représentants des Villes Sanctuaires échangent à la Maison St Régis"
weight = 1

+++
Réunions plénières et ateliers, les échanges de réflexions, analyses et retours d'expériences vont bon train au [colloque de l'association de Villes Sanctuaires](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#colloque-de-lassociation-des-villes-sanctuaires-en-france)... en attendant les leçons tirées pour le village par la Maison St Régis dans le prochain Bulletin.

{{<grid>}}{{<bloc>}}

![](/media/colloque-villes-sanctuaires-2-2022.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/colloque-villes-sanctuaires-1-2022.JPG)

{{</bloc>}}{{</grid>}}