+++
date = 2021-09-30T12:00:00Z
description = "Octobre 2021 - Une rentrée au pas de course !"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Octobre 2021 - Une rentrée au pas de course !"
title = "Le Bulletin d'information n°15 est paru"
weight = 1

+++
Dans le bulletin du mois d’octobre : une rentrée au pas de course avec le Trail des Sapins, une épreuve sportive et un défi organisationnel pour les bénévoles du Comité des fêtes ; une rentrée sur les chapeaux de roue avec trois rallyes automobiles à Lalouvesc ce mois ; une rentrée pleine de surprises aussi avec deux cadeaux en fin de bulletin : le premier épisode d’un nouveau feuilleton signé par de très jeunes auteurs louvetous et l’album des saisons photographiées par le Père Olivier… et, bien sûr, comme toujours plein d’autres informations. 

[Lire la suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/)