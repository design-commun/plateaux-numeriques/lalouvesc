+++
date = 2022-01-23T23:00:00Z
description = "Départ de la spéciale dimanche 30 janvier à 13h15"
header = "/media/rallye-monte-carlo-2022-3.jpg"
icon = ""
subtitle = ""
title = "130 voitures au Rallye Monte-Carlo historique !"
weight = 1

+++
Cette année le Rallye Monte-Carlo historique a fait le plein : 130 voitures au départ, c'est un record !

Rendez-vous dimanche au square du Grand-Lieu à 13h15 pour le départ de la spéciale Lalouvesc-Labathie d'Andaure pour une spéciale de 20km. Vous pouvez aussi admirer les voitures tout au long du parcours.

![](/media/rallye-monte-carlo-2022-2.png)