+++
date = 2023-01-03T23:00:00Z
description = "Contacter le secrétariat"
header = "/media/logo-college-satillieu.jpg"
icon = ""
subtitle = ""
title = "Inscriptions au collège St Joseph de Satillieu"
weight = 1

+++
Les inscriptions sont en cours au collège St Joseph en Val d'Ay à Satillieu.

Merci de contacter le secrétariat au 04 75 34 96 42