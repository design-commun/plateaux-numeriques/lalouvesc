+++
date = 2023-04-06T22:00:00Z
description = "Quand le Carrefour des arts entre à l'école et au collège de Satillieu..."
header = "/media/expo-satillieu-avril-2023-2.jpg"
icon = ""
subtitle = ""
title = "Vernissage de l'exposition \"D'une promenade à l'art abstrait\""
weight = 1

+++
Le 6 avril, dans la cour de l'école primaire de Satillieu, 80 enfants et parents ont assisté au vernissage de l'exposition des dessins et peintures réalisés dans trois classes de sixième du collège St Joseph et deux classes de l'école primaire de St Jean-Baptiste de la Salle.

![](/media/expo-satillieu-avril-2023-6.jpg)

![](/media/expo-satillieu-avril-2023-5.jpg)

Deux artistes, Judith Chancrin et Shasha Shaikh, missionnées par le Carrefour des arts, ont animé plusieurs ateliers avec les enfants. 

Des ateliers créatifs durant le mois de mars 2023 avec les élèves : prises de photos d’inspiration dans la nature et création en dessins et peintures d’une œuvre « abstraite » inspirée par les photos.

{{<grid>}}{{<bloc>}}

![](/media/expo-satillieu-avril-2023-1.jpg)

![](/media/expo-satillieu-avril-2023-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/expo-satillieu-avril-2023-4.jpg)

![](/media/expo-satillieu-avril-2023-3.jpg)

{{</bloc>}}{{</grid>}}

Toutes les œuvres originales étaient exposées et ont pu être admirées dans la cour de l'école.

Elles ont été reproduites sur trois panneaux qui seront réunis sous forme d'un totem érigé sur la place du marché de Satillieu jusqu'à la fin du mois.

![](/media/totem-expo-satillieu-avril-2023.jpg)