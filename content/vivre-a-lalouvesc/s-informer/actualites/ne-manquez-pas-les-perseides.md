+++
date = 2021-08-07T22:00:00Z
description = "Quelques conseils pour observer la pluie d'étoiles filantes à Lalouvesc, lieu idéal"
header = "/media/perseid_meteor_2007.jpg"
icon = ""
subtitle = ""
title = "Ne manquez pas les Perséides !"
weight = 1

+++
Il va faire (enfin) beau la semaine prochaine. C'est le temps idéal pour observer les Perséides la pluie d'étoiles filantes. Elle a lieu entre le 14 juillet et le 24 août, mais le pic sera dans la nuit du 12 au 13 août, avec 100 météores par heure, soit plus d'une par minute !

Lalouvesc, sur un col, est un lieu idéal pour l'observation.

Elles partent de la constellation de Persée. Donc pour une observation optimale, il faut attendre son arrivée dans le ciel, vers minuit à l'horizon dans le quart nord-est. L'apogée sera vers 3-4h du matin.

La Lune en croissant est présente en début de soirée mais elle devrait être peu gênante car elle est basse sur l’horizon. Le 12 août, elle se couchera à 23h15.

[Pour tout savoir sur les Perséides](https://fr.wikipedia.org/wiki/Pers%C3%A9ides).