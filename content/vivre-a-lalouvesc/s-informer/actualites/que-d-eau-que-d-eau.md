+++
date = 2021-08-03T22:00:00Z
description = "La pluie n'a pas épargné Lalouvesc en juillet. "
header = "/media/trou-aout-2021.jpg"
icon = ""
subtitle = ""
title = "Que d'eau ! Que d'eau !"
weight = 1

+++
Mesurés par le Père Olivier, il est tombé 166 mm d'eau au mois de juillet à Lalouvesc ! Et la pluie ne semble pas s'arrêter au mois d'août. La saison 2021 est exceptionnellement humide.

L'eau a causé quelques dégâts : un effondrement accompagné un trou profond est apparu au bord de la route du lotissement Chante Aucel. La Mairie a sécurisé les abords. Ne vous approchez-pas. La stabilisation est incertaine.

Des travaux sont prévus très vite pour mieux comprendre les causes du phénomène.