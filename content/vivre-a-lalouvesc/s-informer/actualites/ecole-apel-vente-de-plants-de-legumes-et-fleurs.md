+++
date = 2021-04-14T22:00:00Z
description = "Une opération locale et de saison pour récolter des fonds pour les activités des enfants de l'école Saint Joseph de Lalouvesc..."
header = "/media/apel-operation-plants.jpg"
icon = ""
subtitle = ""
title = "Ecole/APEL : Vente de plants de légumes et fleurs"
weight = 1

+++
L’APEL a lancé une nouvelle opération pour récolter des fonds pour les activités des enfants de l'école Saint Joseph de Lalouvesc.

Une opération locale et de saison, il s’agit d’une vente de plants de légumes et de fleurs avec l’horticulteur Richard DAVY de Saint romain d'Ay.

Des bons de commande seront distribués en cette fin de semaine dans les boîtes aux lettres du village en raison du contexte sanitaire. Ils seront également mis à disposition au Vival et sont [accessibles en ligne](/media/bondecommandefleurs20210414.pdf). Toutes les informations concernant l’opération sont sur le bon de commande.

Les bons doivent être déposés avec le règlement au plus tard le 7 mai au soir dans la boîte aux lettres de l’école. La distribution des plants aura lieu le mercredi 12 mai au matin à l’école.

**Merci à tous de votre participation pour les enfants de l’école !**

APEL Lalouvesc