+++
date = 2022-10-18T22:00:00Z
description = "C'est le 29 octobre"
header = "/media/basilique-photo-o-de-framond.jpg"
icon = ""
subtitle = ""
title = "Le Sanctuaire fête la 25ème heure"
weight = 1

+++
Le 29 octobre dans le cadre de la 25ème heure des Villes Sanctuaires en France, l'Office de tourisme du Val d’Ay et le Sanctuaire de Lalouvesc s'associent pour un programme d'animations. La 25ème heure est fêtée tous les deux ans en lien avec le passage à l’heure d’hiver.

Habitants, personnes en résidence secondaire ou familiale, accompagnants des participants et participants eux-même du Trail enfin toutes les personnes intéressées pourront participer gratuitement à la visite du village, pendant laquelle nous ferons quelques pas sur le chemin de Saint Régis (nouveau tracé) entre 14h et 15h15.

Il y aura aussi à 18h la messe traditionnelle. A 20h les cloches de la basilique sonneront comme dans les dix-neuf autres sanctuaires adhérents au réseau Villes Sanctuaires en France. En lien avec le Trail, le Père Michel Barthe-Dejean proposera alors un temps spirituel autour de "Saint Régis, marcheur de Dieu", suivi d'un concert _a capella_ de Rachel Julie Petit, soprano lyrique à la basilique vers 20h20/20h30.

Participants et accompagnants du Trail, habitants, personnes en résidence familiale ou secondaire, pèlerins de passage… toutes et tous sont conviés.