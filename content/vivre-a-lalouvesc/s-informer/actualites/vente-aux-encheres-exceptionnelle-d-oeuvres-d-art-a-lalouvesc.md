+++
date = 2022-10-12T22:00:00Z
description = "Organisée le 30 octobre à l'espace Beauséjur par le Carrefour des arts et l'association La Source"
header = "/media/vente-aux-encheres-carrefour-2022-1.jpg"
icon = ""
subtitle = ""
title = "Vente aux enchères exceptionnelle d'oeuvres d'art à Lalouvesc"
weight = 1

+++
Madame, Monsieur,

Julien BESSET, président de l'association **Carrefour des Arts** de Lalouvesc, et Louis HOUDAYER, président de l'association **La Source Annonay**, ont le plaisir de vous inviter à la **Vente aux Enchères**  de photographies de Serge Roussé, et de "robes durcies" de Danielle Stéphane le dimanche 30 octobre 2022 à 11h à l'espace Beauséjour à Lalouvesc.

_Nous vous demandons de nous faire part de votre présence avant le 24 octobre 2022._ [_contact@carrefourdesarts-lalouvesc.fr_](mailto:contact@carrefourdesarts-lalouvesc.fr) _ou 06 21 69 06 20_

Le Carrefour des Arts proposera aux enchères dix photographies du photographe annonéen Serge Roussé. Ces photographies imprimées sur dibond de 150 x 100 cm sont issues de l’exposition « d’Encre et d’Eau » installée en plein air au centre de Lalouvesc depuis le 23 mai.

La Source Annonay, pour sa part, proposera aux enchères dix « robes durcies », œuvres de l’artiste plasticienne Danielle Stéphane qui font partie de la collection de l’association.

Le prix de départ de chaque pièce est fixé à 90 €, avec des enchères par tranche de 20 €.

![](/media/vente-aux-encheres-carrefour-2022-2.jpg)

Cette vente aux enchères s'inscrit dans le développement rapide du Carrefour des arts qui, après avoir repris en 2022 les activités des Promenades musicales, se rapproche aujourd'hui de l'[association La Source](https://www.associationlasource.fr/) et vient d'être lauréat du dispositif de musée virtuel [Micro-folie](https://lavillette.com/page/micro-folie_a405/1). On vous expliquera tout cela dans le numéro du Bulletin de novembre.