+++
date = 2022-06-21T22:00:00Z
description = "Invitation à la journée du jardin le 1er juillet"
header = "/media/la-vie-tara-invitation-2022-3.jpeg"
icon = ""
subtitle = "Vendredi 1er juillet de 9h00 à 17h00 La Vie Tara, journée du jardin !"
title = "Appel de la vie Tara"
weight = 1

+++
Toute personne souhaitant donner un coup de main est la bienvenue, même si c'est juste pour une heure, nous sommes très heureux de toute aide.

Comme toujours, les choses prennent plus de temps que prévu... La tondeuse à gazon ne peut pas encore être réparée, alors nous coupons l'herbe avec la débroussailleuse. Nous mettons les plantes en pots, nous les désherbons, nous enlevons les branches taillées, nous faisons toutes sortes de petits travaux pour que tout soit à nouveau en ordre.

Si vous avez un peu de temps pour nous aider à mettre le jardin en ordre, venir avec une tondeuse ou même un tracteur tondeuse serait génial.

Le tout dans une ambiance conviviale. Si vous souhaitez vous joindre à nous pour le petit déjeuner et/ou le déjeuner, faites-nous le savoir.

Nous attendons avec intérêt de faire votre connaissance. On espère vous voir le 1 juillet !

Romain, Ine et l'équipe de La Vie Tara

\+336 66 99 28 37 et [management@lavietara.eu](mailto:management@lavietara.eu)

![](/media/la-vie-tara-invitation-2022-4.jpeg)