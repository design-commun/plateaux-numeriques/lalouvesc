+++
date = 2022-02-09T23:00:00Z
description = "Vous êtes toutes et tous invités"
header = "/media/ag-2022-comite-des-fetes-2.jpg"
icon = ""
subtitle = ""
title = "Assemblée générale du Comité des fêtes"
weight = 1

+++
Le Comité des Fêtes vous invite à son **Assemblée Générale qui aura lieu le :**

**JEUDI 24 FÉVRIER 2022 à 20H00 au CAC.**

Dans le respect des règles sanitaires en vigueur : distanciation, port du masque, gel hydroalcoolique à disposition.

**_N'hésitez pas à vous rendre sur notre_** [**_page Facebook Comité des fêtes de Lalouvesc - 07_**](https://www.facebook.com/comitedesfeteslalouvesc/) **_; aimez-la pour suivre notre activité et faites-la aimer à vos amis !_**

![](/media/ag-2022-comite-des-fetes.jpg)