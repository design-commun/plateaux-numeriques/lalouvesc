+++
date = 2021-06-21T22:00:00Z
description = "Le \"Temps des émerveilleurs\", une balade riche en surprises..."
header = "/media/plan-lalouvesc-mairie.jpg"
icon = ""
subtitle = ""
title = "Emerveillés le 2 juillet à Lalouvesc"
weight = 1

+++
Une balade le 2 juillet pour découvrir dans la convivialité la richesse culturelle du village, mais aussi des passages secrets, des trésors cachés, des pépites et des saveurs de Lalouv'estivals avec le Temps des Emerveilleurs, organisé par l'Office du Tourisme avec l'aide de nombreux bénévoles.

Le programme est [ici](http://www.valday-ardeche.com/agenda/fiche_5837596_le-temps-des-emerveilleurs/).

Pour s'inscrire, c'est [là](https://docs.google.com/forms/d/e/1FAIpQLScxTA9FNb1efuwRxgCvxpcx3sKmDaGchmgXWm58n7fr4_EmkA/viewform). 