+++
date = 2021-04-26T22:00:00Z
description = "C'est ce vendredi 15h pour un premier nettoyage de printemps avant ouverture..."
header = "/media/abri-du-pelerin.jpg"
icon = ""
subtitle = ""
title = "Rendez-vous à l'Abri du Pèlerin"
weight = 1

+++
Le Père Michel Barthe-Dejean donne rendez-vous à tous ceux qui le désirent ce vendredi 30 avril à 15h pour un premier nettoyage de l'Abri du Pèlerin en vue de sa réouverture après l'hiver.

Avis aux volontaires !