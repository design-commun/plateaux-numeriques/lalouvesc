+++
date = 2021-03-23T23:00:00Z
description = "Pour cette journée, il est prévu au programme : la peinture des pistes du mini-golf, le nettoyage des chalets et des refuges, le nettoyage du tennis, l'élagage du ruisseau longeant le bois, selon le nombre que nous serons."
header = "/media/journee-citoyenne-1-2021-30.jpg"
icon = ""
subtitle = ""
title = "Journée citoyenne 27 mars : un camping rénové, des refuges pour les Afars"
weight = 1

+++
Samedi 27 mars, nous nous retrouvons à partir de 8h30 pour une deuxième journée citoyenne. Plus nous serons nombreux, plus le camping sera à la hauteur de la tradition d'accueil de Lalouvesc.

Pour cette journée, il est prévu au programme : la peinture des pistes du mini-golf, le nettoyage des chalets et des refuges, le nettoyage du tennis, l'élagage du ruisseau longeant le bois, selon le nombre que nous serons.

Pour tous ces travaux, amenez vos outils et tout particulièrement :des rouleaux et pinceaux pour la peinture, balais, brosses et seaux pour le nettoyage, pelles, râteaux, racloirs, brouettes pour le tennis, etc.

Comme pour la journée précédente, en accord avec la préfecture, l'ensemble des conditions sanitaires seront respectées : masques, gel, groupes de six personnes, etc.

![](/media/journee-citoyenne-1-2021-15.jpg)

Si vous pensez que les journées citoyennes sont superflues ou une idée théorique, allez-donc lire le [retour sur la journée du 13 mars ](/vivre-a-lalouvesc/s-informer/actualites/premiere-journee-citoyenne-2021-premiers-resultats/) ou encore visionnez [la vidéo](https://youtu.be/XAsf-qFauNU) de Gladys Durieux. Chacun participera selon ses forces et sa disponibilités. Montrons, une nouvelle fois que Lalouvesc est un village qui a du caractère.

## Pourquoi rénover le camping

Un camping attractif enrichit directement le village, grâce aux dépenses des campeurs chez les commerçants et par les recettes de la location des hébergements. Le camping est la principale source d'auto-financement de la Commune et donc sa capacité de financement à venir.

Les lourds changements sociétaux actuels sanitaires et environnementaux rendent plus attrayants notre village. La saison 2020 l'a montré. La saison 2021 risque d'être encore plus favorable. Il faut donc penser à un hébergement plus large et mieux adapté à la demande actuelle.

![](/media/cottage-pmr.jpg)

La municipalité a prévu de [nouveaux hébergements](/decouvrir-bouger/hebergement-restauration/camping/) : un cottage PMR et une tente lodge neufs et des "refuges" rénovés. Les enfants de l'école St Joseph sont mis à contribution pour [trouver un nom pour chacun des refuges des Afars](/media/trouver-un-nom-pour-les-refuges-des-afars.pdf). Une offre adaptée est prévue pour ces refuges.

Les équipement obsolètes sont renouvelés : matelas, plaques de cuisson, vaisselle, etc.

Une nouvelle animation est aussi proposée. Un [appel a été lancé](/vivre-a-lalouvesc/s-informer/actualites/buvettes-et-emplois-pour-l-ete-appels/) pour ouvrir une buvette et recruter une animatrice. Le mini-golf à nouveau d’aplomb et un tennis transformé en terrain multisport participent à cette relance.

**Le camping est notre bien commun. C'est tous ensemble que nous pourrons en faire une source de richesses pour le village.**