+++
date = 2021-08-11T22:00:00Z
description = "Fortes rafales et grêle possible, soyez prudents"
header = "/media/vigilance-orange-meteo-france-12-08-2021.png"
icon = ""
subtitle = ""
title = "Alerte orage pour le 12 août après-midi et soirée"
weight = 1

+++
## Bulletin de Météo France

En contexte de forte chaleur, une puissante dégradation orageuse devrait se produire cet après-midi et ce soir de l'est du Massif central au Alpes et au Jura en passant par le Lyonnais et le val de Saône, et jusqu'à la moitié nord de la Drôme et de l'Ardèche. Les orages attendus se caractérisent avant tout par un risque prononcé de très fortes rafales, souvent comprises entre 100 et 120 km/h, voire plus localement. Le risque de grêle est également bien présent : les grêlons pourraient être de grosse taille localement. Enfin, les intensités de précipitations pourraient atteindre des niveaux très importants avec des cumuls parfois supérieurs à 15 mm en moins de 10 mm et occasionner du ruissellement, notamment en milieu urbain.

### CANICULE

Les maximales de ce jeudi après-midi seront en hausse par rapport à celles d'hier : on attend de 34 à 36°C voire 38/39°C sur le sud de la Drôme. Cet épisode de canicule, même s'il n'est pas extrême, se poursuivra jusqu'en fin de semaine.

Sur les départements limitrophes et placés en vigilance jaune, les maximales seront de 32 à 35/36°C sur la Haute Loire, l'Isère et le Rhône, et jusquà 34 à 38°C sur l'Ardèche.

Par la suite, les minimales sont en hausse, mais petite baisse des maximales sur la Haute Loire, l'Isère et le Rhône, tandis que les fortes valeurs se maintiennent sur l'Ardèche.