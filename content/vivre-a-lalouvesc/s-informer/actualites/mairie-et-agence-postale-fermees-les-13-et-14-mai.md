+++
date = 2021-05-10T22:00:00Z
description = "En raison du pont de l'ascension les services municipaux seront fermés"
header = "/media/Mairie-Lalouvesc-4x1-1280w-dithered.jpg"
icon = ""
subtitle = ""
title = "Mairie et agence postale fermées les 13 et 14 mai"
weight = 1

+++
Attention, fermeture exceptionnelle de la mairie et de l'agence postale ce vendredi 14 mai !

Jeudi 13 mai est férié en raison de la fête de l'Ascension.

N'attendez plus pour vos démarches, ou remettez-les à la semaine prochaine...