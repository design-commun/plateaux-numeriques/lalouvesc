+++
date = 2022-02-27T23:00:00Z
description = "n°20 - mars 2022 - Place aux jeunes"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "n°20 - mars 2022 - Place aux jeunes"
title = "Le Bulletin n°20 est paru"
weight = 1

+++
Les jeunes ont la part belle dans ce numéro : les écoliers, leur voyage-découverte et leurs idées pour le jeu-monument, les étudiants, chargés d’y réfléchir, qui nous ont rendu visite et aussi le voyage du city-parc pour les jeunes sportifs, acheté sur une autre Commune et qu’il a bien fallu démonter et transporter. On vous raconte tout cela … et, comme toujours, bien d’autres choses encore.

[Lire le Bulletin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/)

### **Accès par le sommaire**

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#mot-du-maire)
* [Actualités de la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#actualités-de-la-mairie)
  * [Chantier de l’écolotissement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#chantier-de-lécolotissement)
  * [Changements chez les employés municipaux](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#changements-chez-les-employés-municipaux)
  * [Un local pour les employés municipaux](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#un-local-pour-les-employés-municipaux)
  * [Journées citoyennes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#journées-citoyennes)
* [Zoom](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#zoom)
  * [Musique et cirque](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#musique-et-cirque)
  * [Des idées pour le jeu-monument](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#des-idées-pour-le-jeu-monument)
  * [Le city-stade a été récupéré](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#le-city-stade-a-été-récupéré)
* [Suivi](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#suivi)
  * [Ronde Louvetonne : les anciennes de retour à Lalouvesc](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#ronde-louvetonne--les-anciennes-de-retour-à-lalouvesc)
  * [Nouvelles sorties pour le club du Carrefour des Arts](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#nouvelles-sorties-pour-le-club-du-carrefour-des-arts)
  * [Visite du centre de secours des pompiers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#visite-du-centre-de-secours-des-pompiers)
  * [Hommage à Albert Morino](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#hommage-à-albert-morino)
  * [Attention, les élections présidentielles sont pour bientôt !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#attention-les-élections-présidentielles-sont-pour-bientôt-)
  * [Du répit pour les aidants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#du-répit-pour-les-aidants)
  * [L’IGN autorisé à entrer sur les propriétés privées](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#lign-autorisé-à-entrer-sur-les-propriétés-privées)
  * [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#dans-lactualité-le-mois-dernier)
* [Puzzle](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/20-place-aux-jeunes/#puzzle)