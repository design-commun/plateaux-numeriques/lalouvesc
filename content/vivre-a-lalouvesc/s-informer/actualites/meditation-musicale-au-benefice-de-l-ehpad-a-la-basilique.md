+++
date = 2021-08-09T22:00:00Z
description = "Dans la prolongation des festivités de l'Assomption, Henri Pourtau (orgue) et Raphaël Yacoub (chantre),  donneront une méditation musicale samedi 21 août à 2030"
header = "/media/h-pourteau-r-yacoub-orgue-2021-2.jpg"
icon = ""
subtitle = "Samedi 21 août 20h30, entrée libre"
title = "Méditation musicale au bénéfice de l’EHPAD à la Basilique"
weight = 1

+++
Dans la prolongation des festivités de l'Assomption, Henri Pourtau (orgue) et Raphaël Yacoub (chantre), donneront une méditation musicale en la basilique de Lalouvesc au bénéfice des activités d’animation du « Balcon des Alpes, », EHPAD du village.  
  
L’organiste Henri Pourtau est un familier de Lalouvesc puisque sa mère, Régine Faurie était de souche louvetoune. Organiste bien connu, il exerce à Cannes où il est professeur d’orgue au Conservatoire, titulaire des grandes orgues de l’église Notre-Dame de Bon Voyage, en même temps que conservateur des orgues de la ville ainsi que directeur artistique des Amis de l’Orgue de Cannes. Une intense activité au service de l’instrument à tuyaux qui le conduit aussi dans la plupart des pays d’Europe et aux Etats-Unis où il est régulièrement sollicité pour des concerts ou des « master-classes ».  
  
Le jeune chantre Raphaël Yacoub, quant à lui, accomplit ses études musicales sous la direction d’Henri Pourtau. Après sa licence de musicologie, il a été engagé comme chantre et organiste dans la Paroisse Saint-Nicolas de Cannes où il est également, professeur de musique en collège.  
  
Les deux artistes interpréteront des pages musicales inspirées par la Fête de l’Assomption. On pourra ainsi entendre des pièces d’orgue de Bach (Fugue sur le Magnificat), Boëllmann (Suite gothique), ainsi que les immortels « Ave Maria » (Schubert, Gounod, Caccini…). Au programme, également des chants d’assemblée : Salve Regina grégorien ainsi que des chants à la Vierge.  
  
 Un beau moment de ferveur musicale et populaire à ne pas manquer…  
  
 Entrée libre dans le respect des contraintes sanitaires.