+++
date = 2022-07-11T22:00:00Z
description = "Composez le 15 avant de vous déplacer à l'hôpital"
header = "/media/urgences-ete-2022.jpg"
icon = ""
subtitle = ""
title = "En été, ne saturez pas les urgences médicales..."
weight = 1

+++
Extrait du communiqué de l'ARS :

Les services d’urgences ont vocation à prendre en charge en priorité, les urgences vitales qui nécessitent une réponse immédiate en terme de soins médicaux.

Une grande partie des personnes qui se rendent aux urgences pour une prise en charge non vitale doivent ainsi attendre de nombreuses heures avant une consultation, les urgences vitales étant gérées en priorité.

Aussi, si vous avez un problème de santé qui ne peut pas attendre l’ouverture habituelle des cabinets médicaux, **ne vous déplacez pas directement aux urgences : appelez en priorité le 15** ou le 114 pour les personnes malentendantes.

Les régulateurs du Centre 15 **pourront vous proposer une orientation adaptée à votre état de santé :**

* conseils médicaux nécessaires par un médecin régulateur ;
* orientation vers un médecin de garde ou organisation d’une visite à domicile ;
* en cas d’urgence, un régulateur vous orientera vers un service d’urgence ou organisera l’intervention d’une équipe adaptée (ambulance, SMUR, etc.).

Le message à retenir est le suivant : **en cas de problème de santé en l’absence de son médecin traitant, ne pas se déplacer directement aux urgences, mais appeler le 15.**

![](/media/urgences-ete-2022.jpg)