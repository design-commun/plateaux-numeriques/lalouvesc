+++
date = 2022-10-12T22:00:00Z
description = "Les 14 et 20 octobre"
header = "/media/rue-de-la-fontaine-partagee-2022-1.jpg"
icon = ""
subtitle = ""
title = "Rue de la Fontaine barrée"
weight = 1

+++
Vu la demande formulée par la société VATD en date du 12 octobre 2022, Considérant la sécurité à mettre en place relative aux travaux de démantèlement d'une cuve de fioul dont les travaux imposent l'interdiction de circuler sur la partie basse de la rue de la Fontaine, et l'interdiction de stationner à la place « dépose minute » de 10 h à 18 h le vendredi 14 octobre et le jeudi 20 octobre 2022 

[Arrêté municipal](/media/arrete-n-2022_036_a.pdf).