+++
date = 2021-10-20T22:00:00Z
description = "Réductions pour le Safari de Peaugres et pour la location de vélos électriques"
header = "/media/office-de-tourisme.jpg"
icon = ""
subtitle = "Réductions pour le Safari de Peaugres et pour la location de vélos électriques"
title = "Des offres intéressantes de l'Office du tourisme"
weight = 1

+++
L'[office de tourisme](http://www.valday-ardeche.com/) propose des offres pour les vacanciers de la Toussaint concernant la location de vélos à assistance électrique mais aussi la vente de tickets pour le Safari de Peaugres, qui à partir du 22 octobre propose avec l'entrée au parc animalier le festival Lumières Sauvages.

Une offre à tarifs préférentiel réservée aux habitants du Val d'Ay et membre d'associations du territoire est proposée pour une plus longue période : concernant la location de vélos à assistance électrique elle s'étend jusqu'au 2 mai 2022 et concernant la vente de tickets pour le Safari de Peaugres, elle est valable jusqu'en mars 2022.

{{<grid>}}{{<bloc>}}

![](/media/peaugres-reduction-ot-toussaint-2021.jpg)

![](/media/peaugres-reduction-ot-2021.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/velo-electrique-reduction-ot-toussaint-2021.jpg)

![](/media/velo-electrique-reduction-ot-2021-2.jpg)

{{</bloc>}} {{</grid>}}