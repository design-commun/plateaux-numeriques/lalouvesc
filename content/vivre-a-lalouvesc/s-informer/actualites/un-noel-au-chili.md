+++
date = 2022-12-27T23:00:00Z
description = "De nos envoyés spéciaux à l'île Chiloe"
header = "/media/noel-a-chiloe-2022-3.jpg"
icon = ""
subtitle = ""
title = "Un Noël au Chili"
weight = 1

+++
L'année dernière nous avions raconté un [Noël au Guatemala](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/un-noel-au-guatemala/). La surprise de cette année est plus au sud, au Chili.

{{<grid>}}{{<bloc>}}

![](/media/ile-chiloe.png)

{{</bloc>}}{{<bloc>}}

Dans la région de Los Lagos, Chiloé borde le sud du Chili aux portes de la Patagonie. Cette île authentique est peuplée de marins qui se disent Chilotes avant d’être Chiliens et comme tout pays de marins elle ne manque pas de légendes.

### Une île fascinante

A Castro la capitale, les palafitos, ces petites maisons colorées sur pilotis sont indissociables de Chiloé. Construites au 19eme siècle, elles étaient occupées par les pêcheurs, aujourd’hui elles font le bonheur des visiteurs.

{{</bloc>}}{{</grid>}}

L’architecture rappelle le lien étroit qui unit les Chilotes à l’océan. Les tuiles et les bardages en bois font penser aux écailles de poisson ou aux vagues de l’océan.

L'île compte une soixantaine d’églises en bois construites selon les techniques utilisées par les Huilliches pour la construction de leurs bateaux. Les premières furent construites au 16 ème siècle avec l’arrivée des jésuites. Seize sont classées au Patrimoine culturel mondial de l’UNESCO. Elles n’ont souvent qu'un unique clocher sur le devant qui sert de repère aux pêcheurs.

La plus ancienne, Santa Maria de Loreto a été bâtie en 1730, sa voute étonnante entièrement en bois ressemble à la coque d'un bateau renversé. Son clocher est couvert de tuiles de mélèze et les murs extérieurs sont habillés de bardeaux.

La mythologie très particulière de Chiloé façonne la culture et la vie de ce peuple étonnant et les croyances traditionnelles restent bien vivantes aujourd’hui. Elles se mélangent à celles du catholicisme pour tisser une cosmogonie originale et raconter l’histoire de la création des îles. Les jésuites en mission pour évangéliser Chiloé ont laissé libre cours à ces légendes.

![](/media/noel-a-chiloe-2022-2.jpg)

### Quelques légendes chilotes

**Les Brujos** sont des guerriers investis des pouvoirs de la magie noire. ils cherchent à corrompre les Chilotes et à leur nuire. ils vivent dans une grotte secrète gardée par **El Invunche**, un être avec la tête, le nez, la bouche et les membres déformés qui n'émet que des sons gutturaux.

**El caleuche** est un bateau pirate incandescent piloté par des brujos qui dansent et chantent. Leur mélodie attire les embarcations vers le piège d’El Caleuche. Ce vaisseau fantôme peut naviguer contre le vent et même sous la surface des eaux.

**Cai-cai vilú** dieu serpent (méchant) esprit des eaux qui lutta contre **Ten-ten vilú**, dieu serpent (bon) esprit de la terre pour s’assurer la suprématie sur son territoire. Cai-Cai Vilú a fini par perdre, mais il est parvenu à ensevelir assez de terre sous les eaux pour que Chiloé soit à jamais séparée du continent.

**El Coo** est un oiseau qui ressemble à une chouette qui pendant les nuits sans lune va frapper aux fenêtres attirant l’attention des malades. Les proches terrifiés savent que cette figure sinistre a été désigné par les brujos pour annoncer qu'il sera cloué au lit.

### Un plat délicieux

Nous ne pouvons conclure sans aborder le plat traditionnel de l’Île de Chiloé, le fameux Curanto al hoyo. Il s'agit d’un plat entre terre et mer composé de coquillages, de porc, de poulet, de pommes de terre, de petits pois et de choux, le tout, cuit dans un four creusé dans le sol dans lequel sont installées des pierres et du bois.

Lorsque le four est très chaud, on y dépose les aliments par couches successives et dans un ordre bien précis. Ils sont ensuite recouverts de feuilles de Nalca et de sacs humides pour un effet "cocotte minute".

Cuisson 2 heures Toutes les saveurs sont ainsi conservées. Un vrai délice..

Buen provecho !

Brigitte Bober, envoyée spéciale de Lalouvesc au Chili pour ce reportage.