+++
date = 2022-03-10T23:00:00Z
description = "Permanence des élus certains jours"
header = "/media/ukraine-ardeche-2022.jpg"
icon = ""
subtitle = ""
title = "Collecte de dons pour l'Ukraine"
weight = 1

+++
Une permanence d'élus pour la collecte de dons pour l'Ukraine est organisée en Mairie le lundi de 14 à 16 h et le jeudi de 17 à 19 h.

Liste des dons matériels attendus :

![](/media/liste-dons-ukraine-2022.jpg)

Pour les dons en argent, il faut s'adresser à la Croix-Rouge : [https://donner.croix-rouge.fr/urgence-ukraine/](https://donner.croix-rouge.fr/urgence-ukraine/ "https://donner.croix-rouge.fr/urgence-ukraine/") ou par chèque à l’attention de : « Croix-Rouge française – Urgence Ukraine » à l’adresse suivante : Croix-Rouge française, CS 20011 - 59895 Lille cedex 9

Pour toute information concernant les dons : 09 70 82 05 05