+++
date = 2022-10-29T22:00:00Z
description = "28 - Novembre 2022 - La richesse d'un village"
header = "/media/entete-automne.jpg"
icon = ""
subtitle = "28 - Novembre 2022 - La richesse d'un village"
title = "Le Bulletin n°28 est paru"
weight = 1

+++
_La richesse d’un village, c’est d’abord la capacité de ses habitants à construire un avenir en commun. Dans ce Bulletin, nous mettons à l’honneur deux associations dont les bénévoles portent haut les couleurs de Lalouvesc : le Comité des fêtes et le Carrefour des arts. Vous y trouverez aussi des nouvelles des pompiers, de l’école, de la Vie Tara, de l’Abri du pèlerin et encore, comme toujours, bien d’autres informations._

### [**Lire le Bulletin**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/)

### Accès par le sommaire

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#mot-du-maire)
* [Actualités de la Mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#actualités-de-la-mairie)
  * [Une excellente saison pour le camping](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#une-excellente-saison-pour-le-camping)
  * [Un cimetière enfin d’aplomb](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#un-cimetière-enfin-daplomb)
  * [Des étangs nettoyés](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#des-étangs-nettoyés)
  * [Numérique et ruralité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#numérique-et-ruralité)
  * [Raccordez-vous à l’assainissement collectif](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#raccordez-vous-à-lassainissement-collectif)
  * [Eau trouble, on avance](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#eau-trouble-on-avance)
* [Zoom : La réussite spectaculaire de deux associations louvetonnes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#zoom--la-réussite-spectaculaire-de-deux-associations-louvetonnes)
  * [Le Comité des fêtes fait des étincelles](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#le-comité-des-fêtes-fait-des-étincelles)
  * [Le Carrefour des arts étend son activité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#le-carrefour-des-arts-étend-son-activité)
* [Sanctuaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#sanctuaire)
  * [Une bonne saison pour l’Abri du pèlerin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#une-bonne-saison-pour-labri-du-pèlerin)
* [Octobre à l’école](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#octobre-à-lécole)
* [Culture - Loisir](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#culture---loisir)
  * [La vie Tara, monastère moderne](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#la-vie-tara-monastère-moderne)
  * [Une vidéo pour le chemin de Saint Régis](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#une-vidéo-pour-le-chemin-de-saint-régis)
* [Commerçants - artisans](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#commerçants---artisans)
  * [Café du Lac](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#café-du-lac)
  * [Salon de coiffure](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#salon-de-coiffure)
* [Santé -sécurité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#santé--sécurité)
  * [Porte ouverte chez les pompiers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#porte-ouverte-chez-les-pompiers)
  * [Café des aidants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#café-des-aidants)
  * [Prévention des cambriolages](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#prévention-des-cambriolages)
  * [Portes ouvertes au Sytrad](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#portes-ouvertes-au-sytrad)
  * [Suite du calendrier Santé-Environnement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#suite-du-calendrier-santé-environnement)
* [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#dans-lactualité-le-mois-dernier)
* [Puzzle : le marché de Lalouvesc dans les années cinquante](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/28-la-richesse-d-un-village/#puzzle--le-marché-de-lalouvesc-dans-les-années-cinquante)