+++
date = 2023-04-19T22:00:00Z
description = ""
header = "/media/la-vie-tara-avril-2023.jpg"
icon = ""
subtitle = ""
title = "La Vie Tara recherche un photographe"
weight = 1

+++
Nous sommes à la recherche d'un photographe qui puisse prendre de belles photos lors d'événements à La Vie Tara. De préférence un professionnel, mais tous les conseils sont les bienvenus. Qui connaît quelqu'un ?

Contacter : [info@lavietara.eu](mailto:info@lavietara.eu) ou +33 765 66 06 77

Merci beaucoup !

Ine & team La Vie Tara