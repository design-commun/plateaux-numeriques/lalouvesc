+++
date = 2022-03-09T23:00:00Z
description = "Bonne selon les analyses de l'ARS"
header = "/media/robinet-eau.jpg"
icon = ""
subtitle = ""
title = "Qualité de l'eau en 2021"
weight = 1

+++
Selon l'Agence Régionale de Santé (ARS), l'eau de Lalouvesc était de bonne qualité bactériologique et conforme aux limites réglementaires pour les paramètres chimiques mesurés en 2021.

Bonne qualité bactériologique, nitrate et arsenic. Mais, comme toujours, une eau très peu calcaire susceptible de dissoudre les métaux des canalisations.

![](/media/qualte-de-l-eau-2021.jpg)