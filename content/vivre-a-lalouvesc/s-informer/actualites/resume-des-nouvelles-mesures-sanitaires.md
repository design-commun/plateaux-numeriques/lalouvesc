+++
date = 2021-05-17T22:00:00Z
description = "Couvre feu, port du masque, rassemblements... "
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Résumé des nouvelles mesures sanitaires"
weight = 1

+++
Extraits et résumé d'un document envoyé par la préfecture.

## Couvre feu

* 19 mai au 9 juin : 21h à 6h
* 9 juin au 30 juin : 23h à 6h
* A partir du 30 juin : fin du couvre-feu

Déplacements autorisés :

* entre le domicile et le lieu de travail ou d’enseignement ou de formation.
* professionnels ne pouvant être différés, livraisons
* missions d’intérêt général sur demande de l’autorité administrative.
* consultations, examens, actes de prévention.
* motif familial impérieux, pour l’assistance aux personnes vulnérables ou précaires, ou pour la garde d’enfants.
* personnes en situation de handicap et de leur accompagnant.
* convocation judiciaire ou administrative.
* transferts ou transits depuis des gares ou aéroports dans le cadre de déplacements de longue distance.
* brefs pour les besoins des animaux de compagnie (dans un rayon de 1 kilomètre autour du domicile).

## Port du masque

Le port du masque n'est obligatoire à partir du 19 mai et jusqu’au 29 juin inclus que pour les personnes de 11 ans et plus, partout où la fréquentation est importante et ne permet pas le respect des gestes barrières, notamment dans un périmètre de 50 m aux abords des écoles.

## Rassemblements

A compter du 19 mai, les rassemblements autorisés sur la voie publique et dans les lieux ouverts au public passent de 6 à 10 personnes (sauf exception : manifestations revendicatives, rassemblements à caractère professionnel, services de transport de voyageurs, cérémonies publiques).

Plus aucune limitation à partir du 30 juin.

Toutes les activités ou rassemblements ayant lieu dans les établissements recevant du public et quelque soit la phase de déconfinement, sont soumis à un protocole sanitaire adapté, au respect des mesures barrières et à l’utilisation du pass sanitaire s’ils concernent 1000 personnes ou plus.