+++
date = 2021-05-09T22:00:00Z
description = "Forte pluiie prévue à partir de lundi 10 mai midi"
header = "/media/vigilance-meteo-10-mai-2021.jpg"
icon = ""
subtitle = ""
title = "Alerte météo sur l'Ardèche"
weight = 1

+++
Mise en place d'un épisode de fortes pluies en flux de sud à la mi-journée de lundi concernant un large axe central de la région orienté sud-nord. La durée et la stationnarité de la perturbation pluvio-orageuse entraîneront des cumuls qui pourront atteindre 70 mm en 12h, parfois 100 mm en 24h et même plus de 150 mm en Ardèche.

Une incertitude inhérente à ce genre de situation reste toutefois de mise par exemple sur la durée qui pourra s'avérer plus longue que prévue et l'emplacement des plus forts cumuls plus ou moins précis.

La fin de l'épisode est pour l'instant envisagée en matinée de mardi, bien que les précipitations se poursuivent longtemps plus à l'est.