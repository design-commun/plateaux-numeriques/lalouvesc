+++
date = 2022-09-21T22:00:00Z
description = "La chasse est ouverte, sauf le mardi"
header = "/media/sanglier-wkp.jpg"
icon = ""
subtitle = ""
title = "Périodes d'ouverture de la chasse (rappel)"
weight = 1

+++
Rappel de l'information parue dans le [Bulletin du mois de juillet](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/24.amenager-pour-accueillir/) :

La période d’ouverture générale de la chasse pour le département de l’Ardèche est fixée **du 11 septembre 2022 à 7 heures au 28 février 2023 au soir**. La pratique de la chasse reste **interdite le mardi**, sauf jours fériés.

Attention, de nombreux cas particuliers sont prévus selon les types de chasse et les territoires. Pour toutes précisions, consulter [l’arrêté préfectoral.](https://www.lalouvesc.fr/media/ap07ouverture-et-fermeture-de-la-chasse_-2022_2023.pdf)