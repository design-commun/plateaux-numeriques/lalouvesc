+++
date = 2021-08-29T22:00:00Z
description = "Septembre 2021 - Les beaux gestes"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "14 - Les beaux gestes"
title = "Le Bulletin d'information n°14 est paru"
weight = 1

+++
On trouvera dans ce Bulletin,  la relation de quelques beaux gestes : une journée particulière du Carrefour des Arts, deux concerts de la ferme de Polly, des échos du Sanctuaire les 8 et 15 août, du club de foot, de la brocante à venir  et, comme toujours, plein d'autres informations.

Sans ces beaux gestes, ceux des bénévoles qui donnent de leur temps pour que le village vive et accueille avec le sourire les visiteurs, ceux des bricoleurs qui, sans compter non plus leur temps, réparent et construisent pour que le village soit présentable et élégant, enfin ceux des artistes qui nous ont enchantés tout au long été, sans tous ces beaux gestes, Lalouvesc perdrait son âme.

[Lire la suite](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/14-les-beaux-gestes/)