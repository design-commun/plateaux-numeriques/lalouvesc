+++
date = 2021-09-28T22:00:00Z
description = "Avec un tarif à la journée défiant toute concurrence pour les hébergements disponibles."
header = "/media/camping-chalets-5.png"
icon = ""
subtitle = ""
title = "Le camping reste ouvert jusqu'au 3 novembre"
weight = 1

+++
**Qu'on se le dise et qu'on le fasse savoir : désormais, le camping du Pré du Moulin reste ouvert jusqu'au 3 novembre !**

Ainsi des hébergements seront disponibles pour le Trail des Sapins et le week-end de la Toussaint.

Bien sûr à cause du risque du gel, certains équipements doivent fermer : les mobil-homes et le cottage. Mais l'ensemble des autres restent disponibles et un bloc sanitaire est hors gel.

Pour la période du 1er octobre au 3 novembre, pour tous les équipements le tarif est à la journée.

#### Tarifs hors saison (octobre-novembre)

|  | Tarifs à la journée |
| --- | --- |
| Refuge des Afars (avec petit déjeuner) | 75 |
| Tente Lodge | 105 |
| Chalet | 40 |

#### Tarifs emplacements, campeurs, caravanes, camping-cars 

|  | Tarifs à la journée |
| --- | --- |
| Campeur adulte | 3 |
| Campeur enfant | 2 |
| Emplacement de tente | 2.5 |
| Caravane | 3.5 |
| Voiture | 2.5 |
| Branchement eau / EDF | 4 |
| Garage mort juillet / août | 7 |
| Garage mort hors-saison | 1 |

#### Pour réserver

* la [page du camping]()
* ou directement

<script type="text/javascript" src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"></script>

<script type="text/javascript">

( function() {

    var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"saexsbE", idIntegration:1195, langue:"fr", ui:"OSCA-116693" } );
    
    widgetProduit.Initialise();

})();

</script>

<div id="widget-produit-OSCA-116693"></div>