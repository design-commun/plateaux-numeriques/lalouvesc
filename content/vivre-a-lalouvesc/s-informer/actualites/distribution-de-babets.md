+++
date = 2023-01-08T23:00:00Z
description = "Bonne année à toutes et tous les seniors !"
header = "/media/babet-2023.png"
icon = ""
subtitle = ""
title = "Distribution de babets et repas festif (actualisé)"
weight = 1

+++
Bonne année à tous les seniors !

98 seniors recevront cette année un lot de cinq bons d'achat de six babets chacun (6 €), soit l'équivalent de 30 € à dépenser dans les commerces du village.

Voici les commerces qui acceptent les babets en 2023 :

* Au Pavé de St Régis
* Coiffure mixte Ondu’ligne
* Natura Stella
* Epicerie VIVAL
* Boucherie - charcuterie
* Pharmacie
* Café du Lac

42 seniors ont préféré le repas festif qui s'est tenu au CAC le 14 janvier.

{{<grid>}}{{<bloc>}}

![](/media/repas-aines-2023-1.jpg){{</bloc>}}{{<bloc>}}

![](/media/repas-aines-2023-2.jpg){{</bloc>}}{{<bloc>}}

![](/media/repas-aines-2023-3.jpg){{</bloc>}}{{<bloc>}}

![](/media/repas-aines-2023-4.jpg){{</bloc>}}{{</grid>}}