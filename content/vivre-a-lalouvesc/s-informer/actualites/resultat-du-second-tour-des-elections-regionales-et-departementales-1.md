+++
date = 2021-06-26T22:00:00Z
description = "Abstention toujours forte, résultats confirmés"
header = "/media/regionales-2nd-tour-2021.png"
icon = ""
subtitle = ""
title = "Résultat du second tour des élections régionales et départementales"
weight = 1

+++
## Régionales second tour

### Lalouvesc

* Inscrits : 321
* Votants : 156, soit 48.6 % de votants
* Blancs : 4
* Nuls : 3
* WAUQUIEZ (LUD) 114 voix
* KOTARAC (RN) 12 voix
* GREBERT (EELV/PS/PCF) 23 voix

![](/media/regionales-2nd-tour-2021.png)

### Ardèche

* WAUQUIEZ (LUD) 54.36 % des suffrages exprimés
* KOTARAC (RN) 11.56 %
* GREBERT (EELV/PS/PCF) 34.08 %

### Région

* WAUQUIEZ (LUD) 55.17 % des suffrages exprimés, soit 136 sièges
* KOTARAC (RN) 11.18 %, 17 sièges
* GREBERT (EELV/PS/PCF) 33.65%, 51 sièges

## Départementales second tour

### Lalouvesc

* Inscrits : 321
* Votants : 155, soit 48.3 % de votants
* Blancs : 7
* Bourjat & Vallon (DVD) : 116
* Mourier & Vercasson (DVG) : 32

![](/media/departementales-2nd-tour-2021.png)

### Canton du Haut-Vivarais

* Bourjat & Vallon (DVD) : 59.46 % des suffrages exprimés, élus
* Mourier & Vercasson (DVG) : 40.54 %

[Résultats complets sur l'Ardèche](https://fr.wikipedia.org/wiki/%C3%89lections_d%C3%A9partementales_de_2021_en_Ard%C3%A8che)