+++
date = 2021-06-03T22:00:00Z
description = "Le port du masque obligatoire, nombre d'hospitalisations en baisse"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Covid : Port du masque et nombre de personnes hospitalisées en Ardèche"
weight = 1

+++
Le port du masque est obligatoire dans l'espace public en Ardèche jusqu'au 29 juin. [Arrêté préfectoral](/media/20210602_ap-portmasque.pdf).

Ci-dessous l'évolution du nombre des hospitalisations dues au Covid en Ardèche comparée à celle de la France entière.

![](/media/covid-hospitaliisation-04-06-2021.jpg)

On constate que le département a été durement touché par la deuxième et la troisième vague de l'épidémie. Aujourd'hui l'évolution est nettement plus favorable. Début juin 2021, le nombre d'hospitalisations est équivalent à celui de la première vague à la même époque l'année dernière avant un été très calme.

Souhaitons que la tendance se poursuive et qu'avec la vaccination, cette période difficile soit maintenant derrière nous.