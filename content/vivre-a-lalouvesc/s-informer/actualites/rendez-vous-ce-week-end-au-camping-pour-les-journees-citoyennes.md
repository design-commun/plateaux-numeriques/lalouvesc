+++
date = 2021-03-09T23:00:00Z
description = "Toutes les bonnes volontés louvetonnes sont attendues au camping le week-end prochain !  Plus nous aurons de bras, plus beau sera le camping !"
header = "/media/journee-citoyenne-lalouvesc-11-juillet-2020.jpg"
icon = ""
subtitle = ""
title = "Rendez-vous ce week-end au camping pour les journées citoyennes"
weight = 1

+++
Toutes les bonnes volontés louvetonnes sont attendues au camping le week-end prochain ! Plus nous aurons de bras, plus beau sera le camping ! Chacun, selon ses forces, ses disponibilités et le temps qu'il pourra consacrer à la collectivité, est bienvenu à partir de 8h30 et jusqu'à 17h, samedi 13 et dimanche 14 mars.  Nous espérons vous trouver aussi nombreux que le 11 juillet dernier pour le désherbage du jeu de boules.

**La seule exigence est la bonne humeur !**

Différents chantiers sont prévus et seront organisés sur place en fonction des arrivées : débroussaillage, réparation des chalets et cabanes, rénovation du mini golf, nettoyage du tennis, etc. Ils démarreront ce week-end et pourront se poursuivre par la suite selon une programmation mise en place par les chefs de chantiers.

Apportez des gants, un masque et si vous en avez à disposition : des râteaux, pelles, binettes, brouettes.

Pour faciliter l'organisation, merci de vous inscrire sur [ce formulaire](https://docs.google.com/forms/d/e/1FAIpQLSffAQE20gzFLi55Lp_YDhu1oJZji9dF2GTguOOC4sWlu1BHsw/viewform) ou en Mairie.

Pour respecter les mesures sanitaires et en accord avec la préfecture, le travail se fera en équipe de six personnes maximum, suivant la charte ci-dessous. Des masques et du gel hydro-alcoolique seront à disposition sur place.

![](/media/chantiers-journees-citoyennes.jpg)

## Règles à respecter par toutes personnes engagées dans nos journées d'action citoyenne

#### Gestes barrières

Face aux infections respiratoires, des gestes simples permettent de préserver votre santé et celle de votre entourage en limitant la transmission du virus :

* Lavez-vous les mains très régulièrement (avec de l'eau et du savon, ou du gel hydro-alcoolique)
* Toussez ou éternuez dans votre coude ou dans un mouchoir
* Restez toujours à plus de deux mètres les uns des autres
* Utilisez un mouchoir à usage unique et jetez-le
* Saluez sans vous serrer la main, arrêtez les embrassades
* Evitez de vous toucher le visage en particulier le nez et la bouche

#### Lavage des mains

Bien se laver les mains (avec du savon et de l'eau) minimise le risque d'être contaminé.

Le lavage des mains doit se faire plusieurs fois par jour à l'eau et au savon pendant 30 secondes puis il faut les sécher avec une serviette propre ou à l'air libre.

Il faut se laver les mains après toute manipulation d'un masque (tout type de masque), avant de préparer les repas, de les servir et de manger et avant de sortir de chez soi, après s'être mouché, avoir toussé ou éternué, avoir rendu visite à une personne malade, chaque sortie à l'extérieur, avoir pris les transports en commun (ou partagés), être allé aux toilettes.

#### Gel hydro-hydro-alcoolique

L'usage du gel hydro-alcoolique s'est largement répandu depuis la pandémie de Covid-19 en France.La friction avec du gel hydro-alcoolique est toujours recommandée par les autorités de santé.Pour être efficace, il faut avoir les mains visuellement propres et respecter les 6 étapes : paume contre paume/dos des mains/entre les doigts/les dos des doigts/les pouces/le bout des doigts et des ongles. Il faut se laver les mains plusieurs fois par jour.

#### Distanciation

Compte-tenu d'une transmissibilité accrue des variantes du virus Sars-Cov-2 - principalement anglaises et sud-africaines- le Haut Conseil de la Santé Publique a fait évoluer ses recommandations en janvier 2021. Désormais, il est recommandé de respecter une distance de sécurité entre 2 individus de 2 mètres contre 1 mètre auparavant. Les groupes doivent être de 6 personnes maximum.

**Nous vous remercions de respecter ces recommandations, elles sont essentielles au bon déroulement de nos journées d'action citoyenne afin de préserver la santé de tous**.

Jacques Burriez