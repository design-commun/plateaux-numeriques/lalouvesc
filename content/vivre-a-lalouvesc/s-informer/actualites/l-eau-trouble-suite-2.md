+++
date = 2022-10-15T22:00:00Z
description = "Le point de la situation"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "L'eau trouble (suite 2)"
weight = 1

+++
Le problème de l'eau trouble au robinet perdure malheureusement dans une quinzaine de maisons situées dans les parties nord des quartiers Chante Ossel, Grand Lieu et Bellevue. Et la solution reste incertaine.

Nous avions fait un point dans le[ Bulletin d'octobre](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#leau-trouble-suite). Voici un nouveau point sur ce problème difficile qui préoccupe de façon légitime la population du village et, au premier chef, la mairie.

On sait que cette pollution provient d'une portion métallique trop ancienne du réseau qui se désagrège progressivement. On connait approximativement sa situation, mais, malgré les différents tests effectués, le tronçon concerné n'est pas encore précisément repéré.

Nous avons aussi maintenant un diagnostic de l'ARS précis :

> La consommation d'une eau contenant plus de 200 µg/L de fer ne génère pas de risque pour la santé en l'état actuel des connaissances.
>
> Cependant, **le fer entraine à des concentrations généralement supérieures à 300 µg/L des effets indirects gênants pour l’usager** :
>
> * Neutralisation des désinfectants pouvant générer la prolifération des micro-organismes dans les réseaux,
> * Coloration rouille qui tache le linge et les installations de plomberie,
> * Dégradation du goût et de la couleur de l’eau.

Que faire pour résoudre ce problème ? La réponse a deux temps : réduire, au moins en partie, la nuisance à court terme et, plus fondamentalement, éliminer définitivement la source de la nuisance.

### Quelle option pour une amélioration ?

Nous pensions avoir trouvé une solution provisoire en coupant l'alimentation directe du réseau par le réservoir du St Père. Malheureusement, cela n'a pas été concluant. La pression issue du seul réservoir du Mont Chaix n'était pas suffisante pour alimenter les maisons les plus hautes dans le village. Il a fallu donc rouvrir l'alimentation par les deux réservoirs avec toutes les conséquences sur la nature de l'eau.

La seule option provisoire trouvée aujourd'hui pour réduire la difficulté est de purger régulièrement la partie concernée du réseau. Mais cela n'a qu'un temps et conduit à un gaspillage d'eau peu recommandable à terme.

### Quelle solution pour régler le problème ?

La difficulté est d'autant plus grande que l'apparition d'une eau trouble dans une partie du village n'est qu'une partie d'une difficulté structurelle. L'ensemble du réseau du village d'eau potable et d'assainissement, vieilli, mérite réparation, entretien et élargissement (sans parler la station d'épuration dont le choix n'était pas peut-être optimal).

Un schéma directeur du réseau a été réalisé l'année dernière avec la société Naldeo. Sur cette base, un programme  pluriannuel de travaux sur les réseaux d'un coût de près de 350 k€ HT a été envisagé. Une première étape doit démarrer prochainement. Même si une part (environ 50%) sera supportée par des subventions, il s'agit déjà d'un coût très lourd pour une commune comme la nôtre. Et pourtant, le programme ne comprenait pas le problème de l'eau trouble apparu après son élaboration.

Venant encore compliquer l'équation, rappelons que la compétence eau et assainissement doit être confiée à la Communauté de communes au plus tard en 2026.

Devant l'urgence, il nous faut réagir rapidement, mais nous n'avons pas les moyens d'engager les travaux indispensables. C'est pourquoi le maire va prendre rendez-vous avec le sous-Préfet pour trouver avec lui le meilleur moyen de sortir de cette impasse.

Ce mauvais feuilleton n'est donc malheureusement pas terminé. Espérons que les prochains épisodes débouchent rapidement sur une issue satisfaisante pour tous.