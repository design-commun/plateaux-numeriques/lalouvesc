+++
date = 2022-08-05T22:00:00Z
description = "Stationnement interdit au centre bourg, parkings réservés"
header = "/media/15-aout-2021-4.jpg"
icon = ""
subtitle = ""
title = "Stationnements réglementés pour le 15 août"
weight = 1

+++
Le 15 août approche. Traditionnellement, le village accueille de très nombreux fidèles pour la cérémonie religieuse qui se tient au parc des Pèlerins à 10h30.

Comme chaque année un plan de circulation et de stationnement est prévu pour que les nombreux piétons soient en sécurité et que la circulation des voitures reste fluide.

### Circulation

**Le stationnement de tout véhicule sera interdit dans tout le centre bourg de 7h à 19h** :

* route d'Annonay
* route des Alpes
* route de Bobigneux
* rue des Cévennes
* route de Rochepaule
* route de Saint-Bonnet-le-Froid
* place des Trois Pigeons (réservée au stationnements-handicapés)

La place Marrel et la place des Trois Pigeons seront dégagées de tout véhicule ainsi que tous les abords autour de la Basilique et le parc des Pèlerins. **Tout véhicule restant fera l'objet d'une mise en fourrière immédiate**.

Quatre emplacements seront réservés aux pompiers à proximité de la caserne, chemin Grosjean.

### Parking

Des parkings seront prévus dans le village pour accueillir les participants à la cérémonie :

* place du Lac
* montée du camping
* place des Trois Pigeons (réservée handicapés)
* chemin Grosjean et terrain de boules
* fontaine St Régis,
* terrain de football,
* terrain de Ste Monique (à côté de l'EHPAD)
* boulevard des élégants (stationnement en file)
* vers le tennis route de Rochepaule/aire de pique-nique (priorité aux autocars)

Une signalétique appropriée sera mise en place pour indiquer ces parkings et, comme chaque année, des bénévoles orienteront les automobilistes.

![](/media/parking-15-aout-2022.jpg)