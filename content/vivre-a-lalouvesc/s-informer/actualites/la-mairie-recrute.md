+++
date = 2021-09-17T22:00:00Z
description = "Un agent technique polyvalent, CDD 6 mois"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = "Un agent technique polyvalent"
title = "La Mairie recrute"
weight = 1

+++
Votre poste consistera à :   
Réalisation des travaux d'entretien :   
\- Gestion du réseau d'eau potable en régie communale,   
\- Stations d'épuration, postes de relevage et du réseau d'assainissement en régie communal,   
\- Entretien voirie (fauchage, travaux de réfection, déneigement),   
\- Bâtiments communaux (maçonnerie, pose de faïence et carrelage, peinture, plomberie et électricité),   
\- Espaces verts (tonte, fleurissement, désherbage, arrosage).   
\- Pose du mobilier urbain   
\- Conduite des véhicules (camion, tracteur) avec attelage de godet ou épareuse, lame de déneigement ou saloir, conduite de mini-pelle 

Activités annexes :   
\- Suivi du réseau d'eau potable (notamment des réservoirs d'eau potable), réparation mineure sur le réseau et relevé annuel des compteurs d'eau des particuliers   
\- Entretenir de manière hebdomadaire et suivre régulièrement le bon fonctionnement de la station d'épuration et les postes de relevage le tout en relation avec les services du Département (SATESE)   
\- Entretenir la voirie : faucher, épandre de l enrobé ou du gravier, saler et déneiger   
\- Entretenir les bâtiments communaux : construire des murs en béton et/ou moellons, monter des cloisons, poser des plafonds, rénover, peindre   
\- Entretenir les espaces verts : tondre, fleurir, désherber et arroser les parterres   
\- Aménager les espaces publics communaux - Installer le mobilier urbain   
\- Accompagner les différents prestataires de la commune lors de leurs interventions (éclairage public, stations d épuration, bâtiments communaux, voirie)   
\- Mettre en place les équipements adaptés sur les véhicules selon les besoins (fauchage, déneigement)   
\- Entretenir l'outillage 

CDD de 6 mois du 15/10/2021 au 15/04/2022  
Salaire : Horaire de 10,66 Euros