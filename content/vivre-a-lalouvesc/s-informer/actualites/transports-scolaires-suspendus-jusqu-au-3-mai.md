+++
date = 2021-04-04T22:00:00Z
description = "Suite à la fermeture des établissements scolaires, la Région suspend les transports scolaires... "
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Transports scolaires suspendus jusqu'au 3 mai"
weight = 1

+++
Suite à la fermeture des établissements scolaires, la Région suspend les transports scolaires jusqu'au 3 mai.

Pour les lignes régulières, dessertes à compter du mardi 6 avril, les horaires seront prévus comme en période de vacances scolaires (type « autres vacances » ) jusqu’au lundi 3 mai