+++
date = 2023-01-16T23:00:00Z
description = "Gardien de déchetterie"
header = "/media/ccva.jpg"
icon = ""
subtitle = ""
title = "Offre d'emploi CCVA : Agent technique polyvalent"
weight = 1

+++
* Activité principale (32 h/semaine) : gardien de déchetterie
* Activité secondaire ponctuelle (dans la limite de 3h/semaine) : entretien et maintenance des bâtiments communautaires

Voir la [fiche de poste](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/fiche-de-poste-gardien-dechetterie.pdf).