+++
date = 2022-05-08T22:00:00Z
description = "Le mini-golf nettoyé, le bois des refuges débroussaillé, les fleurs vivaces plantées, un chalet réparé... les citoyens n'ont pas chômé..."
header = "/media/journee-citoyenne-2022-7.jpg"
icon = ""
subtitle = ""
title = "Une journée citoyenne efficace... comme toujours"
weight = 1

+++
Les Louvetonnes et les Louvetous qui se sont déplacés pour la journée citoyenne du 7 mai avait du cœur, de la bonne humeur... et de l'énergie !

Grâce à elles et eux, le mini-golf est maintenant débarrassé de ses mauvaises herbes, nettoyé et prêt à accueillir les estivants. Le bois qui cache les refuges dans les arbres s'est éclairci. Un chalet dont la terrasse se délabrait a été remis en état et les fleurs vivaces commencent à trouver leur place dans les bacs du village.

Et ce n'est pas tout, d'autres ou les mêmes, à leur rythme et selon leurs jours de disponibilité, ont ramené le citypark (on vous raconte bientôt où il sera placé...), ont fait le tour du parcours des lapins et celui des champignons et ont imaginé et réalisé de nouveaux panneaux (qui remplaceront bientôt les vieux défraichis).

**Merci à toutes et tous ces citoyens exemplaires ! Sans elles et eux, nous l'avons dit souvent mais il faut le répéter, un village comme le nôtre n'aurait pas les moyens de s'embellir et de se rendre accueillant pour l'été.**

{{<grid>}}{{<bloc>}}

**![](/media/journee-citoyenne-2022-13.jpg)**

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-19.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-23.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-21.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-17.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-25.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-26.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/journee-citoyenne-2022-6.jpg)

{{</bloc>}}{{</grid>}}