+++
date = 2022-12-10T23:00:00Z
description = "Offert par les Petits Frères des Pauvres"
header = "/media/illumination-2021-10.JPG"
icon = ""
subtitle = ""
title = "Noël pour les seniors isolés"
weight = 1

+++
Depuis 1946, les Petits Frères des Pauvres luttent contre l'isolement des personnes âgées de plus de 50 ans, prioritairement les plus démunies.

Comme l’isolement peut être encore plus douloureusement vécu au moment de Noël, les bénévoles des petits frères des pauvres d’Annonay proposent deux actions aux personnes qui sont seules les 24 et 25 décembre et qui souhaitent être entourées au moment de cette fête : **une visite le 24 ou 25 décembre** avec un colis de Noël **OU** **une invitation au déjeuner du 24 décembre** à midi au restaurant La Truffolie à Saint-Alban-d’Ay. Le repas leur est offert quels que soient leurs revenus ainsi que le transport si elles n’ont pas de moyen de locomotion.

Cette initiative des Petits Frères des Pauvres pour les seniors de notre territoire n’a pour l’instant pas eu beaucoup d’écho car il est difficile d’identifier les personnes isolées. Si vous connaissez des personnes dans ce cas merci de remplir[ le formulaire](/media/2022_annonay_noel.docx).