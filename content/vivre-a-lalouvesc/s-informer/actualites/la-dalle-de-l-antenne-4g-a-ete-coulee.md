+++
date = 2021-06-16T22:00:00Z
description = "L'antenne doit être opérationnelle le 14 juillet au plus tard."
header = "/media/dalle-antene-4g.jpg"
icon = ""
subtitle = ""
title = "La dalle de l'antenne 4G a été coulée"
weight = 1

+++
Les Louvetous ne devrait plus pester contre le manque de connexion pour les téléphones portables après le 14 juillet. SFR a, en effet, l'obligation d'avoir installé une antenne opérationnelle avant cette date.

Bonne nouvelle : les travaux ont commencé et la dalle de béton a été coulée.

Nous pousserons tous un ouf ! de soulagement. La dégradation des connexions a déjà beaucoup trop duré, de nos jours les téléphones portables nous accompagnent à tout instant.