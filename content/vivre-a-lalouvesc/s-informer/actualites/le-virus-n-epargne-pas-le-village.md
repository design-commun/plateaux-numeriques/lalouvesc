+++
date = 2021-12-09T23:00:00Z
description = "Des cas de Covid ont été détectés, respectez les gestes barrières et n'attendez pas pour les rappels de vaccination !"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Le virus n'épargne pas le village"
weight = 1

+++
On a beau avoir du caractère à Lalouvesc, cela ne protège pas contre les virus. L'Ardèche est le département le plus touché de France et Lalouvesc n'est malheureusement pas épargné. Le virus est signalé dans le village.

Si vous n'êtes pas convaincus, jetez donc, une nouvelle fois, un œil sur la courbe actualisée des hospitalisations pour cause de Covid. La tendance de l'Ardèche (en rouge) a très largement dépassé celle de la France. Il y a actuellement 108 hospitalisés en Ardèche... ils étaient 8 en octobre.

![](/media/covid-hospitalisation-10-12-2021.png)

Soyez raisonnables et prudents. Faites faire sans tarder vos rappels de vaccination. Si nous n'êtes pas encore vaccinés, **il est plus que temps de le faire**. Et surtout appliquez les gestes barrières : distances, lavage des mains, masques, aération.