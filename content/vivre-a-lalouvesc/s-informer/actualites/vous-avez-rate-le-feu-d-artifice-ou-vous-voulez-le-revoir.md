+++
date = 2022-07-13T22:00:00Z
description = "C'est cadeau !"
header = "/media/feu-d-artifice-13-juillet-2022.jpg"
icon = ""
subtitle = ""
title = "Vous avez raté le feu d'artifice ou vous voulez le revoir..."
weight = 1

+++
Fallait-il tirer le feu d'artifice prévu à Lalouvesc le 13 juillet au soir ? En raison de la sécheresse et de la canicule, la question se posait. Les consignes de la Préfecture étaient pour le moins ambigües. Les avis étaient partagés au sein du Conseil.

Le Maire, après avoir hésité, passé plusieurs coups de fil et s'être renseigné auprès de professionnels, s'est d'abord assuré de la présence de pompiers sur place et d'un tirage moins en hauteur, mais pas moins spectaculaire. Il a pris alors la décision de l'autoriser.

Et le public présent nombreux ne l'a pas regretté, il fut magnifique éclairant la maison St Régis dans une nuit noire, la pleine lune ayant eu le bon goût de rester cachée par le mont Besset !

En voici quelques extraits :

<iframe width="560" height="315" src="https://www.youtube.com/embed/JzkuzJzZNHs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/BNOEv45bKIc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Auparavant, la Lyre Louvetonne, très en forme, avait lancé la soirée, qui s'est poursuivie, comme de coutume, par un bal, le tout accompagné et alimenté par une buvette tenue dans la bonne humeur par l'APEL et l'équipe de foot.

{{<grid>}}{{<bloc>}}

![](/media/13-juillet-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/13-juillet-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/13-juillet-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/13-juillet-2022-2.jpg)

{{</bloc>}}{{</grid>}}