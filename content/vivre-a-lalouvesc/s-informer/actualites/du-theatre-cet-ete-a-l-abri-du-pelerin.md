+++
date = 2022-06-01T22:00:00Z
description = "Un festival de troupes amateurs invitées par le Théâtre de la Veillée"
header = "/media/theatre-de-la-veillee-2022.jpg"
icon = ""
subtitle = ""
title = "Du théâtre cet été à l'Abri du pèlerin"
weight = 1

+++
Lalouv'estivales s'enrichit de nouveaux événements. Le Théâtre de la Veillée organise son **premier festival de théâtre amateur** en invitant quatre troupes d’amateurs d Haute-Ardèche dans sa salle de l’Abri du pèlerin tout au long de l’été. Les représentations auront toutes lieu à 20h30.

Contact : Frédéric Brunel 06 75 42 75 89

## Mardi 12 juillet : _Politiquement correct_

{{<grid>}}{{<bloc>}}

**Une comédie romantique et politique**

Élection présidentielle 1er tour, l’extrême droite est en tête, gauche et droite suivent loin derrière dans un mouchoir de poche. Mado et Alexandre se rencontrent par hasard, coup de foudre, ils n’ont surtout pas parlé politique. Mais, mais, Mado ne sait pas de qui elle vient de tomber amoureuse et Alexandre ne pensait pas tomber amoureux d’une “bobo” de gauche.

{{</bloc>}}{{<bloc>}}

![](/media/festival-theatre-2022-3.jpg)

Troupe d'Ardoix

{{</bloc>}}{{</grid>}}

## Vendredi 29 juillet : _Donc y jette ou Y’a Nicole au hublot_

![](/media/festival-theatre-2022-4.jpg)

Troupe de Vinzieux

## Mardi 9 août : _Sous les ponts de Paris_

{{<grid>}}{{<bloc>}}

![](/media/festival-theatre-2022-2.jpg)

Troupe de Boulieu

{{</bloc>}}{{<bloc>}}

Depuis de nombreuses années, Paluche et Mélie vivent en squat. Ils sont entourés de leurs amis Tintin et Fernand. Les visites au bistrot rythment leur quotidien et leur équilibre… Jusqu'au jour où Paluche fait une découverte qui pourrait bien bouleverser leurs habitudes.

C'est une pièce de 60 minutes accompagnée par un accordéoniste et qui se clôture par une chanson.

{{</bloc>}}{{</grid>}}

## Mardi 23 août : _Building_

{{<grid>}}{{<bloc>}}

LE THÉÂTRE D’EN FACE

_BUILDING_ de Léonore CONFINO

Un building. 13 étages. Nous sommes chez _Consulting Conseil_, une entreprise qui a pour absurde mission de coacher les coachs, de conseiller les conseillers. Le président directeur général amorce la pièce en motivant ses employés avec un discours démagogique, superposant avec éloquence, banalités et techniques de communication. Puis, suivant la chronologie d’une journée de travail, on se hisse dans le building au rythme soutenu d’une scène par étage : hôtesses, comptables, agents d’entretien, cadres, directeurs des ressources humaines, chargés de communication s’agitent, déjeunent, prospectent, brainstorment au rythme des crashs de pigeons sur leurs baies vitrées.  
L’écriture de la pièce, piquante, caustique et ponctuée de passages musicaux et chorégraphiés, met en relief la noirceur des thèmes abordés : la perte de notre identité et, avec elle, celle de nos idéaux. Une comédie performante sur le monde du travail.

{{</bloc>}}{{<bloc>}}

![](/media/festival-theatre-2022-1.jpg)

Troupe d'Annonay

{{</bloc>}}{{</grid>}}

## Rappel : toutes les infos sur les manifestations de l'été sont sur [cette page](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/a-l-affiche/).