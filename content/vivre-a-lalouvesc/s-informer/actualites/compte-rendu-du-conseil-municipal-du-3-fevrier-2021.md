+++
date = 2021-02-07T23:00:00Z
description = "Compte-rendu du premier conseil de l'année et archives des comptes-rendus depuis 2008."
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte-rendu du Conseil municipal du 3 février 2021"
weight = 1

+++
Le compte-rendu du premier conseil de l'année est [consultable en ligne](/media/2021-1-conseil-municipal-lalouvesc.pdf).

Les archives de tous les comptes-rendus depuis 2008 sont accessibles sur [cette page](/mairie-demarches/vie-municipale/comptes-rendus/comptes-rendus-des-conseils-municipaux/).