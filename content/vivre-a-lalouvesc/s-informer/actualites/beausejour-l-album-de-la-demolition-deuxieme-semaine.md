+++
date = 2022-01-10T23:00:00Z
description = "Dans cette actualité, les photos au jour le jour..."
header = "/media/demolition-beausejour-2022-67.jpg"
icon = ""
subtitle = ""
title = "Beauséjour : l'album de la démolition, deuxième semaine"
weight = 1

+++
Voir aussi : l'[album de la première semaine](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition/).

## Quatrième journée, 10 janvier 2022

![](/media/demolition-beausejour-2022-43.jpg)

![](/media/demolition-beausejour-2022-40.jpg)

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-41.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-42.jpg)

{{</bloc>}}{{</grid>}}

![](/media/demolition-beausejour-2022-39.jpg)

## Cinquième journée, 11 janvier 2022

![](/media/demolition-beausejour-2022-49.jpg)

![](/media/demolition-beausejour-2022-52.jpg)

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-51.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-47.jpg)

{{</bloc>}}{{</grid>}}

## Sixième journée, 12 janvier 2022

![](/media/beausejour-le-reveil-janvier-2022.jpg)

## Septième journée, 13 janvier 2022

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-54.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-53.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-59.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-60.jpg)

{{</bloc>}}{{</grid>}}

![](/media/demolition-beausejour-2022-61.jpg)

![](/media/demolition-beausejour-2022-56.jpg)

![](/media/demolition-beausejour-2022-55.jpg)

## Huitième journée, 14 janvier 2022

![](/media/demolition-beausejour-2022-62.jpg)

![](/media/demolition-beausejour-2022-65.jpg)

![](/media/demolition-beausejour-2022-67.jpg)

![](/media/demolition-beausejour-2022-63.jpg)

## Souvenir...

{{<grid>}}{{<bloc>}}

![](/media/beausejour-cheneau-2021-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/beausejour-3.JPG)

{{</bloc>}}{{</grid>}}