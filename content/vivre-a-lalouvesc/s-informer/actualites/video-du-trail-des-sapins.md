+++
date = 2022-11-04T23:00:00Z
description = "Réalisation Matthieu Fraysse et Christelle Lebeau, merci à eux !"
header = "/media/trail-2022-depart-2.jpg"
icon = ""
subtitle = ""
title = "Vidéo du Trail des Sapins"
weight = 1

+++
<iframe width="560" height="315" src="https://www.youtube.com/embed/YSrT_4q2T5Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Réalisation Matthieu Fraysse et Christelle Lebeau, merci à eux !