+++
date = 2022-11-23T23:00:00Z
description = "Enregistrement au SIA du 25 novembre au 2 décembre 2022"
header = "/media/fusil-britannique-utilise-pendant-la-seconde-guerre-mondiale-wikipedia.jpg"
icon = ""
subtitle = ""
title = "Opération d'abandon d'armes"
weight = 1

+++
La préfecture communique :

_Du 25 novembre au 2 décembre 2022, le ministère de l’Intérieur et des Outre-mer organise une opération d’abandon simplifié d’armes à l’État, pilotée par le Service central des armes et explosifs (SCAE)._

_Si la France compte près de 5 millions de détenteurs légaux d’armes, on estime qu’au moins 2 millions de nos concitoyens détiendraient sans titre des armes, principalement de chasse ou issues des Première et Seconde Guerres mondiales._

_La plupart de ces armes détenues illégalement sur le territoire national ont été acquises par héritage, le plus souvent sans connaître le cadre légal de détention de ces armes. Par cette opération, les détenteurs de bonne foi auront la possibilité de s'en dessaisir simplement ou de les conserver légalement._

_Ces abandons d’armes et de munitions (1) se feront sans formalités administratives auprès des policiers et gendarmes. Si les usagers souhaitent les conserver, des agents de la préfecture seront également présents pour les aider à enregistrer leurs armes (2) dans le système d’information sur les armes (SIA)._

_En Ardèche, deux sites de collecte seront mis en place afin de leur permettre de restituer les armes dont ils souhaiteraient se séparer :_

_• Commissariat de police d’Aubenas - 6 rue Léon Rouveyrol   
• Brigade de gendarmerie de Tournon-sur-Rhône - 22 chemin de la Pichonnière_

_Leurs horaires d'ouverture des deux sites sont les suivants :_

_• de 9h00 à 16h00 les jours ouvrés ;   
• dispositions spécifiques pour le week-end : seul le site de Tournon-sur-Rhône sera ouvert le samedi 26 novembre de 9h00 à 14h00 ; le site d'Aubenas ouvrira quant à lui seulement le dimanche 27 novembre de 9h00 à 14h00._

### _1 - Abandon d’armes_

_Les particuliers pourront abandonner tout type d’armes : armes à feu, armes de poing, armes blanches, petites munitions (< 20 mm)._

_Attention : l’opération ne concerne pas les objets dangereux : munitions de guerre (obus, grenades) explosifs, munitions de calibre égal ou supérieur à 20 mm. Si vous en détenez, il convient de contacter la préfecture au 04.75.66.50.29 pour organiser un enlèvement sécurisé._

### _2- Création de compte et enregistrement des armes à feu « héritées » ou « trouvées » dans le nouveau système d’information sur les armes (SIA)_

_Les particuliers qui se rendront sur un site de collecte pour la création de compte et l’enregistrement d’armes devront être munis des pièces justificatives suivantes :_

_Pour les détenteurs d’armes « héritées » ou « trouvées » :  
\- une pièce d’identité obligatoire en cours de validité (carte nationale d’identité, passeport, permis de conduire, carte vitale)  
\- un justificatif de domicile de moins de trois mois (si vous n’en disposez pas, vous pourrez insérer cette pièce dans votre compte personnel SIA dans un délai de 3 mois après la création de votre compte)  
\- des photographies de l’arme de bonne qualité permettant son identification, ce afin de visualiser les différents marquages (marque, modèle, fabricant, calibre, longueur de l’arme et du canon) et dans toute la mesure du possible, son numéro de série  
\- les informations éventuelles concernant le précédent détenteur.  
Dans un délai de 3 mois après la création du compte, les personnes devront insérer un certificat médical sur leur compte.  
L’arme ne devra en aucun cas être amenée pour l'enregistrement de sa détention._

_Pour les chasseurs :  
\- une pièce d’identité en cours de validité,  
\- un justificatif de domicile de moins de trois mois,  
\- votre permis de chasser,  
\- la validation 2022/2023 de votre permis de chasser_

_Pour les personnes qui sont dans l’incapacité de se rendre sur un site de collecte, un numéro de téléphone est mis à leur disposition : 04.75.66.51.29 (heures ouvrables uniquement). Les forces de l’ordre se déplaceront, uniquement sur rendez-vous, au domicile de ces personnes afin d’enlever les armes à feu en leur possession._

### Rappel

La création d'un [compte SIA](https://www.service-public.fr/particuliers/vosdroits/R61698) **avant le 1er juillet 2023** est **obligatoire pour tous les détenteurs d'armes**. À partir de l'ouverture de votre compte SIA, vous avez **6 mois** pour compléter les informations relatives aux armes qui sont dans votre râtelier numérique provisoire.

Il n'y aura aucune poursuite judiciaire ou administrative pendant cette semaine de collecte. Pour cette opération, une large gamme d’armes est éligible : les armes blanches, armes à feu, armes de poing ainsi que les munitions d’une taille inférieure à 20 millimètres pourront être remises.

La détention illégale d’armes de catégorie A ou B, qui sont des armes de guerre, automatiques, semi-automatiques ou de poing, peut être sanctionnée jusqu’à sept ans d’emprisonnement et de 100 000 € d’amende. Les armes de catégorie C regroupent principalement des armes à feu semi-automatiques ou manuelles, utilisées pour chasser ou faire du tir sportif. Leur détention illégale est punie jusqu’à 30 000 € d’amende et deux ans d’emprisonnement. Les armes de catégorie D peuvent être possédées sans autorisation. Il s’agit notamment d’objets tranchants, de tasers, bombes lacrymogènes, de reproductions d’armes ou d'armes de collection par exemple. Il est toutefois indispensable de disposer d’un motif légitime, jugé au cas par cas, pour les détenir. Sinon, la sanction peut aller jusqu’à un an d’emprisonnement et 15 000 € d’amende.