+++
date = 2021-02-28T23:00:00Z
description = "Nous sommes encore en hiver, mais il semblerait que Lalouvesc est en avance. Dans le village, c’est déjà le printemps !"
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = ""
title = "Bulletin n°8 Le printemps de Lalouvesc"
weight = 1

+++
Nous sommes encore en hiver, mais il semblerait que Lalouvesc est en avance. Dans le village, c’est déjà le printemps ! De nombreuses transactions immobilières ont été signées et les nouveaux arrivants ont d’intéressants projets pour le village. Le camping se transforme avec des hébergements de qualité, des réservations en ligne et, bientôt, grâce à l’énergie des Louvetous, sa rénovation au cours des journées citoyennes. Un site web complètement renouvelé et exemplaire rajeunit l’image du village et, par un effet de miroir, renforce les liens à l’intérieur et avec l’extérieur. Le pari est que nous récoltions tous, dès cet été, les fruits de nos efforts du printemps.

Mais nous n’oublions pas non plus que, ici comme ailleurs, la pandémie a frappé durement les plus fragiles, tout particulièrement à l’Ehpad, malgré le dévouement remarquable du personnel.

Voilà quelques-uns des sujets développés dans ce bulletin bien fourni.

[Lire la suite...](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/8-le-printemps-de-lalouvesc/)