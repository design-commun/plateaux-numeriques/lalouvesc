+++
date = 2022-01-18T23:00:00Z
description = "Toutes les dates pour la présidentielle et les législatives"
header = "/media/elections.jpg"
icon = ""
subtitle = ""
title = "Elections et inscriptions sur les listes électorales"
weight = 1

+++
## **Élection Présidentielle** (10 et 24 avril 2022)

Dates limites d'inscription sur les listes électorales : **mercredi 2 mars 2022 pour les inscriptions en ligne**, et **jusqu'au vendredi 4 mars 2022 pour les inscriptions par courrier ou en mairie.**

## **Élections Législatives** (12 et 19 juin 2022)

Dates limites d'inscription sur les listes électorales : **mercredi 4 mai 2022 pour les inscriptions en ligne**, et **jusqu'au vendredi 6 mai 2022 pour les inscriptions par courrier ou en mairie**

**Pour participer aux élections politiques**, il faut être inscrit sur les listes électorales. 

L’inscription n'a pas besoin d'être renouvelée et est automatique pour les jeunes de 18 ans. Si vous vous trouvez dans une autre situation (déménagement, première inscription…), vous devez prendre l’initiative de la demande.

Vous pouvez à tout moment [vérifier votre inscription sur les listes électorales et votre bureau de vote](https://www.service-public.fr/particuliers/vosdroits/R51788) .

**L’inscription peut se faire** [**en ligne**](https://www.service-public.fr/particuliers/vosdroits/R16396) **ou à la mairie**. [formulaire de demande d’inscription](https://www.service-public.fr/particuliers/vosdroits/R16024) sur les listes électorales. Pièces demandées en complément : carte d’identité en cours de validité, justificatif de domicile.