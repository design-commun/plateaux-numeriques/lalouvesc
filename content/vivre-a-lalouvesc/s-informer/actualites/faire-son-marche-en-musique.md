+++
date = 2021-08-31T22:00:00Z
description = "Retour sur le concert de jeudi dernier"
header = "/media/musique-sur-le-marche-2021.jpg"
icon = ""
subtitle = ""
title = "Faire son marché en musique..."
weight = 1

+++
Voici un retour du groupe de musique qui s'est produit jeudi dernier, jour du marché devant un public conquis... avec une suggestion : pourquoi pas la saison prochaine une peu de musique sur la Place du Lac chaque jeudi matin ?

"C'est à Eclassan, au printemps de cette année 2021 que tout a commencé. Voix, doigts, pieds, engourdis par l'hiver et muselés, ils avaient besoin de s'exprimer !

Un petit groupe est né, pas encore baptisé mais cela ne saurait tarder. Accordéon, cuivres, guitares et percussions nous accompagnent dans un répertoire varié : _Nino Ferrer, Cow-boys fringants, HK et les saltimbanques, la célèbre chanson des Débardeurs (composée à Eclassan !) et bien d'autres..._

Hier, la joie de jouer, danser, chanter nous a rassemblés. Aujourd'hui, c'est cet élan et le plaisir de la musique que l'on veut partager.

Nous remercions Lalouvesc pour son accueil, le soleil retrouvé et le public qui a bien voulu écouter nos morceaux préférés !"