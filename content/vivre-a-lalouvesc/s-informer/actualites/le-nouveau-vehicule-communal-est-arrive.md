+++
date = 2023-02-08T23:00:00Z
description = "C'est un berlingo électrique"
header = "/media/arrivee-brerlingot-2023.jpg"
icon = ""
subtitle = ""
title = "Le nouveau véhicule communal est arrivé"
weight = 1

+++
Le berlingo citroën électrique acquis par la municipalité vient d'arriver. Il a une puissance de 136 ch (10 kw) et une autonomie de 270 km.

Une voiture électrique de plus dans les rues et les chemins du village.

Rappel :

Il a été acquis sous forme de crédit-bail. La prime de l’État pour l’achat d’un véhicule électrique (5.000 €) plus la récupération d’un véhicule ancien (7.000 €) couvre la première mensualité du crédit-bail. Les mensualités sont d’un peu plus de 500 € TTC sur 60 mensualités avec une valeur de rachat de 10% en fin de contrat. Ces sommes comprennent l’entretien du véhicule sauf le changement des pneus.