+++
date = 2022-03-05T23:00:00Z
description = "Le 19 mars à 15h au CAC"
header = "/media/beatrix-poesie-carrefour-des-arts-2021-2.jpg"
icon = ""
subtitle = ""
title = "Assemblée générale du Carrefour des Arts"
weight = 1

+++
L'assemblée générale du Carrefour des Arts aura lieu le **samedi 19 mars à 15H00** au Centre d'animation Communal.

Beaucoup de nouvelles pour vous tous :

* une présentation des artistes qui exposeront l'été prochain,
* une nouveauté avec une exposition hors les murs sur un temps plus long,
* une programmation enrichie par l’organisation de concerts classiques et de jazz (dans la suite des Promenades Musicales),
* une nouvelle organisation de travail en commissions ouvertes à tous.

**Participez aux activités du Carrefour ! Venez nombreux !**