+++
date = 2021-03-26T23:00:00Z
description = "L’association Les Jardins du Haut Vivarais lance ses stages de printemps. "
header = "/media/jardins-du-haut-vivarais.JPG"
icon = ""
subtitle = ""
title = "Faites vos semis comme un pro"
weight = 1

+++
L’association Les Jardins du Haut Vivarais lance ses stages de printemps « Faites vos Semis comme un Pro ». animés par Xavier du Mont Besset et Carine chef de culture de la maison Marcon. Venez découvrir ou perfectionner vos techniques de semis.

Sur 1/2 journée, théorie, pratique et convivialité pour repartir avec vos semis (les graines, cellules, etc. sont fournis par l’association). Chaque mois ce sont de nouveaux semis.

Les stages ont lieu de 8h à 12h, les samedis 3 et 10 avril, 1er et 8 mai et les lundis 7 et 14 juin, principalement au Mont Besset.

Attention, les places sont limitées Une réservation est obligatoire à [jardinsduvivarais@gmail.com](mailto:jardinsduvivarais@gmail.com)