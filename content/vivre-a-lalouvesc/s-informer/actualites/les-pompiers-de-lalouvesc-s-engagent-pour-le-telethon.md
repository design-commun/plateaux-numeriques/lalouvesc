+++
date = 2021-11-16T23:00:00Z
description = "26 novembre : démonstration de manoeuvres, buvette et vente à emporter"
header = "/media/telethon.jpg"
icon = ""
subtitle = ""
title = "Les pompiers de Lalouvesc s'engagent pour le Téléthon"
weight = 1

+++
A l'occasion du Téléthon, les sapeurs-pompiers de Lalouvesc organisent une mise en situation ouverte au public : manoeuvre de secours routier.

Rendez-vous vendredi 26 novembre à 18h devant le centre de secours (sous la mairie). Il y aura une buvette avec en vente à emporter : boissons, frites, hot dogs...

![](/media/telethon-2021-pompiers.jpg)