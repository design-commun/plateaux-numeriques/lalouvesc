+++
date = 2021-07-08T22:00:00Z
description = "C'est le 30 octobre, des parcours sportifs pour tous les niveaux dans une ambiance Halloween"
header = "/media/trail-lalouvesc-5.jpg"
icon = ""
subtitle = ""
title = "Les inscriptions pour le Trail des Sapins sont ouvertes"
weight = 1

+++
Le [Trail des Sapins à Lalouvesc](/decouvrir-bouger/lalouv-estivals/le-trail-des-sapins-fin-octobre/), c'est 1.000 participants de tous niveaux.

Il y a 100 tee-shirts distribués pour les premiers inscrits, ne tardez pas : [Bulletin et réglement](/media/trail-des-sapins-flyer_2021.pdf).

![](/media/trail-des-sapins-affiche_2021.jpg)