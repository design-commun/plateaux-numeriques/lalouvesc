+++
date = 2023-04-19T22:00:00Z
description = "Changement de CMS sur lalouvesc.fr,  un site toujours très fréquenté"
header = "/media/nouveau-site-lalouvesc-fr-2.jpg"
icon = ""
subtitle = ""
title = "Quelques jours de pause pour les actualités"
weight = 1

+++
Le site web de Lalouvesc change d'outil d'édition (CMS). Pendant quelques jours, il n'y aura pas de nouvelles actualités, le temps de bien caler le nouvel outil. Par la suite, plusieurs améliorations progressives du site sont prévues.

Ce renouveau s'inscrit toujours dans le travail du collectif de designers [Plateaux numériques](https://plateaux-numeriques.fr/) dont lalouvesc.fr reste le démonstrateur. La Commune profite de leur savoir-faire et accompagne leur démarche. Elle dispose ainsi d'un outil exceptionnel, adapté à sa taille et à sa population.

Et le succès est encore au rendez-vous après plus de deux ans d'exercice (le nouveau site a ouvert en mars 2021) comme le montre ci-dessous les statistiques de fréquentation des trois derniers mois : **10.000 vues mensuelles, pas mal pour une Commune de moins de 400 habitants !**

Dans les 10 pages les plus consultées, on trouve toujours le camping et les Bulletins (592 vues pour le Bulletin du mois d'avril encore inachevé...). On repère aussi les pics des abonnés du lundi et le rayonnement du site très au-delà du territoire de la Commune.

![](/media/stats-lalouvesc-fr-20-04-2023.png)