+++
date = 2022-05-24T22:00:00Z
description = "Neuf candidats validés pour le premier tour du 12 juin"
header = "/media/elections.jpg"
icon = ""
subtitle = ""
title = "Candidatures à l'élection législative sur le nord-Ardèche"
weight = 1

+++
## Il y a neuf candidats sur la 2e circonscription d'Annonay

Voici le nom des candidats, par ordre du tirage au sort :

* Claire Guilmin, Les Patriotes ; remplaçant François Rouby ;
* Michèle Gaillard, Lutte Ouvrière ; remplaçant Agelino Zanchi :
* Cyrille Grangier, Rassemblement national ; remplaçante Elisabeth Clot ;
* Sébastien Gladieux, le Mouvement de la Ruralité ; remplaçant Stéphan Denis ;
* Marc-Antoine Quenette, Les Républicains ; remplaçant Pascal Balaÿ ;
* Martin Chaize, mouvement Agir Unis, enregistré sous l'étiquette Régionalistes ; remplaçante Maëlys Dunand ;
* Christophe Goulouzelle, Nouvelle Union Populaire Ecologique et Sociale (Nupes) ; remplaçante Estelle Berger ;
* Philippe Bory, Reconquête ; remplaçante Delphine Dragon
* Olivier Dussopt, ancien député de la circonscription, nouveau ministre du travail, Ensemble Majorité Présidentielle ; remplaçante Laurence Heydel Grillere.

Premier tour le 12 juin.

[Démarche pour faire une procuration.](https://www.service-public.fr/particuliers/vosdroits/N47#3)