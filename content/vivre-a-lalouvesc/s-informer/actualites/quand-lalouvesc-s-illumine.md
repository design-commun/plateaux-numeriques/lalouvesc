+++
date = 2021-12-12T23:00:00Z
description = "C'est encore plus beau la nuit ! Un petit aperçu"
header = "/media/illumination-2021-8.JPG"
icon = ""
subtitle = "C'est encore plus beau la nuit !"
title = "Quand Lalouvesc s'illumine..."
weight = 1

+++
Sur le square...
![](/media/illumination-2021-27.JPG)

{{<grid>}}{{<bloc>}}

![](/media/illumination-2021-16.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/illumination-2021-28.JPG)

{{</bloc>}}{{</grid>}}

![](/media/illumination-2021-29.JPG)

Et la place Marrel...

{{<grid>}}{{<bloc>}}

![](/media/illumination-2021-9.JPG)

{{</bloc>}}{{<bloc>}}

![](/media/illumination-2021-10.JPG)

{{</bloc>}}{{</grid>}}

... et ce n'est pas fini. Cette semaine on termine l'installation du coin des enfants devant la mairie. Bientôt des nouvelles.

La France est morose, l'Ardèche se confine, mais notre village s'embellit toujours grâce la solidarité et l'ingéniosité de ses habitants !