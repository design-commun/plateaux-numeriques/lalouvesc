+++
date = 2021-04-29T22:00:00Z
description = "Bulletin du mois de mai : S'organiser"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Mai 2021 - S'organiser"
title = "Le Bulletin d'information n°10 est paru"
weight = 1

+++
Malgré les difficultés de la situation sanitaire, l’hiver nous a permis collectivement d’engager les premiers travaux et les premières réparations. Au mois de mai, le temps des semailles est arrivé. Il faut organiser les finances, prévoir les investissements et commencer à imaginer l’avenir. On trouvera dans ce bulletin un point sur les propriétés municipales et leur avenir, la présentation des artistes du Carrefour des Arts, un projet de “jardin idéal” et comme toujours plein d’autres informations.

[Lire la suite](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/10-s-organiser/)