+++
date = 2022-12-10T23:00:00Z
description = "Une maison France services a ouvert à St Romain d'Ay"
header = "/media/vignette-france-service3.png"
icon = ""
subtitle = ""
title = "Aide aux démarches administratives"
weight = 1

+++
Si vous rencontrez des difficultés dans vos démarches administratives ou numériques, des personnes sont là pour vous aider dans ce service récemment ouvert :

**France Services de Saint Romain d'Ay**  
40 Place de la Fontaine 07290 SAINT ROMAIN D'AY  
Tel : 04 7 5 69 18 01  
Horaires d’ouverture :

* Du lundi au jeudi 8h-12h
* Vendredi 8h-12h / 14h-18h

![](/media/franceservice.png)