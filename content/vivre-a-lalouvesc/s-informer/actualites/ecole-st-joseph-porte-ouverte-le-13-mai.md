+++
date = 2023-04-13T22:00:00Z
description = "Présentation de l'école, exposition de photos anciennes, verre de l'amitié..."
header = "/media/porte-ouverte-ecole-avril-2023-2.jpg"
icon = ""
subtitle = ""
title = "Ecole St Joseph : portes ouvertes le 13 mai"
weight = 1

+++
9h-12h : Visite guidée de l'école, présentation de l'équipe pédagogique, réception des plans de légumes et de fruits, exposition photos, verre de l'amitié.

**L'APEL recherche des anciennes photos de classe pour l'exposition du 13 mai !** Fouillez dans vos tiroirs et envoyez-leur.

![](/media/porte-ouverte-ecole-avril-2023.jpg)