+++
date = 2022-11-11T23:00:00Z
description = "Une cérémonie émouvante réunissant l'ensemble des générations"
header = "/media/11-nov-2022-8.jpg"
icon = ""
subtitle = ""
title = "11 novembre : le village se souvient"
weight = 1

+++
La cérémonie du 11 novembre ne commémore pas seulement l'armistice de la guerre de 14-18, mais honore tous les soldats morts pour la France. Le 11 novembre 2022 à Lalouvesc a réuni toutes les générations pour une cérémonie émouvante.

Elle a commencé par un moment de recueillement dans la crypte de la basilique, où se trouve en plus d'une plaque rappelant les noms des soldats morts au combat, une autre inaugurée en 2018 à la mémoire des fusillés pour l'exemple. Après une messe du souvenir, un défilé des anciens combattants, des pompiers volontaires et de la Lyre louvetonne a traversé tout le village, depuis la basilique jusqu'au monument aux morts.

Devant les villageois rassemblés, les pompiers au garde-à-vous, le Maire a lu le message du ministre, puis le conseiller Michel Bober a énoncé chacun des noms des Louvetous morts pour la France figurant sur le monument. Après une minute de silence et la sonnerie aux morts, la Lyre louvetonne a joué la Marsellaise.

Les enfants ont lu quelques strophes du [poème _Liberté_](https://www.poetica.fr/poeme-279/liberte-paul-eluard/) de Paul Eluard [écrit pendant la guerre de 39-45](https://fr.wikipedia.org/wiki/Libert%C3%A9_(po%C3%A8me)). Puis ils ont déposé une gerbe de fleurs accompagnée d'un panneau qu'ils avaient réalisé à l'école St Joseph.

La matinée s'est conclue par un dernier morceau de la Lyre louvetonne très en forme et un verre de l'amitié offert en Mairie.

{{<grid>}}{{<bloc>}}

![](/media/11-nov-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-nov-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-nov-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-nov-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-nov-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-nov-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-nov-2022-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-nov-2022-7.jpg)

{{</bloc>}}{{</grid>}}