+++
date = 2022-12-19T23:00:00Z
description = "Veillée, messes et repas partagé"
header = "/media/basilique-photo-o-de-framond.jpg"
icon = ""
subtitle = ""
title = "Voeux de Noël du Sanctuaire"
weight = 1

+++
En ces dernier jour de décembre nous souhaitons un beau temps de Noël à toute la communauté villageoise. 

La veille du jour de Noël, ce samedi 24 décembre à 23 heures, Julie Petit introduira notre veillée par de beaux chants lyriques, suivis de la messe de la Nativité. Le 31 décembre nous fêterons l'anniversaire (le 382ème !) de la mort de Saint Régis : messe à 11h à la chapelle Saint Régis. Et le soir de ce même samedi, à 18h, messe anticipée du dimanche à la Basilique. Pour ceux qui le désirent, un repas partagé suivra (vers 19h 30 : chacun apporte quelque chose !), à la Maison Saint- Régis.

Avec nos vœux que personne ne reste isolé en ce tournant de l'année : que chacun goûte quelque chose de la fraternité que le Christ apporte, comme un petit enfant qui rassemble autour de lui !

Michel Barthe-Dejean  
Yves Stoesel