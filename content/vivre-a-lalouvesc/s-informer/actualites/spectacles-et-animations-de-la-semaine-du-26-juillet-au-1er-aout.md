+++
date = 2021-07-22T22:00:00Z
description = "En été, il se passe tous les jours quelque chose à Lalouvesc"
header = "/media/copie-de-20210703_112053.jpg"
icon = ""
subtitle = "Que faire cette semaine ?"
title = "Spectacles et animations de la semaine du 26 juillet au  1er août"
weight = 1

+++
En plus des [nombreuses activités](/decouvrir-bouger/activites/) possibles, voici les spectacles et manifestations proposés à Lalouvesc du 19 au 25 juillet :

### Tous les jours

* 14h30 à 18h30 **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal
* 10h à 12h et de 15h à 18h **Exposition _La matière habitée_**  sculptures à la chapelle Saint Ignace
* Du 24 juillet au 6 août l’AFTL propose des stages de **tennis**

### 26 juillet

* 9h **Gym**,  au camping
* 10h30 **Visite du jardin en permaculture** au Mont Besset
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 17h concert, **Guy Angelloz** à la Basilique
* 21h cinéma, **Profession du père,** avant-première, de Jean-Pierre Ameris avec Benoit Poelevorde, Jules Lefebvre.. [bande annonce](https://youtu.be/mlvzDyqCZWM)

### 27 juillet

* 9h **Gym**, au camping
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 18h : visites estivales **Paroles d'habitants**, départ à l'Office du tourisme

### 28 juillet

* 15h-17h empruntez des livres à la **bibliothèque**
* 17h **Apéro découverte** devant l'Office du tourisme
* 17h conférence, **La matière habitée**, _Marie-Odile LAFOSSE-MARIN_ à la Maison St Régis
* 21h cinéma, **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)

### 29 juillet

* 9h **Gym**, au camping
* 13h-16h **Animation enfants**, au camping (sur inscription)
* 17h conférence, **Art et vérité**, _Marie-Odile LAFOSSE-MARIN_ à la Maison St Régis
* 21h cinéma, **Médecin de nuit**, [bande annonce](https://youtu.be/FandAD1hTbY)

### 31 juillet

* 9h30-11h30 empruntez des livres à la **bibliothèque**
* 21h cinéma, **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)

### 1er août

* 21h cinéma, **Médecin de nuit**, [bande annonce](https://youtu.be/FandAD1hTbY)