+++
date = 2021-08-05T22:00:00Z
description = "Un stationnement très réglementé pour accueillir les nombreux participants aux fêtes religieuses"
header = "/media/plan-15-aout-2021.jpg"
icon = ""
subtitle = ""
title = "Appel pour le 15 août"
weight = 1

+++
Chaque année Lalouvesc reçoit entre 1000 et 1500 visiteurs souhaitant assister à la messe du 15 août sur le Parc des Pèlerins. Comme l'année dernière où l'organisation a été satisfaisante, tout stationnement sera rigoureusement interdit dans le village et des parkings sont prévus à la périphérie, pour que la circulation automobile et pédestre se passe en toute convivialité et sécurité.

Afin de guider les automobilistes vers les parkings et les fidèles sur le parc des pèlerins (masque obligatoire, mais passe sanitaire non-réclamé), la Mairie et le sanctuaire ont besoin de volontaires bénévoles. 

Merci d'avance à ceux qui voudront bien donner un peu de temps pour que le village tienne toujours son rang dans l'excellence de l'accueil.

{{<grid>}} {{<bloc>}}

![](/media/plan-15-aout-2021.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/parking-15-aout-2021.jpg)

{{</bloc>}} {{</grid>}}