+++
date = 2022-04-04T22:00:00Z
description = "Tous les lundis de 14h à 15h"
header = "/media/logo-club-2-clochers.jpg"
icon = ""
subtitle = ""
title = "Apprivoiser l'ordinateur grâce au Club des deux clochers"
weight = 1

+++
Le Club des deux clochers propose une séance d'initiation à l'informatique pour grands débutants tous les lundis.

Si vous connaissez des personnes intéressées, faites-le leur savoir. 

Évidement la séance est réservée aux adhérents, mais il suffit de prendre sa carte.

Renseignements auprès du président : Georges Ivanez 

* [georges.ivanez@orange.fr](mailto:georges.ivanez@orange.fr)
* 06 62 00 45 69