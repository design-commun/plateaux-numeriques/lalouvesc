+++
date = 2021-07-14T22:00:00Z
description = "Activités situées sur le camping, mais ouvertes à tous les estivants et les Louvetous"
header = "/media/mini-golf-final.jpg"
icon = ""
subtitle = ""
title = "Mini-golf, gym et animation pour les enfants"
weight = 1

+++
A partir du 19 juillet, les lundis, mardis et jeudis (gratuit sur inscription) :

* 9h-10h : une heure de gym, mise en forme pour adultes
* 14h-17h : animation encadrée pour les enfants (8-12 ans)

et dès maintenant tous les jours :

![](/media/mini-golf-2021.jpg)