+++
date = 2023-03-06T23:00:00Z
description = "Cérémonie à 12h au Monument aux morts"
header = "/media/18-mars-2022-monument-au-mort-1.jpg"
icon = ""
subtitle = ""
title = "19 mars 1962, fin de la guerre d'Algérie"
weight = 1

+++
La cérémonie commémorant le 61ème anniversaire de la [fin de la guerre d'Algérie](https://fr.wikipedia.org/wiki/Accords_d%27%C3%89vian) se tiendra à 12h le dimanche 19 mars 2023 au Monument aux morts place des marronniers.