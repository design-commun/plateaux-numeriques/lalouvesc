+++
date = 2021-12-07T23:00:00Z
description = "Le comité des fêtes a une fois encore magnifiquement égayé le square du Lac"
header = "/media/decoration-noel-2021-9.jpg"
icon = ""
subtitle = ""
title = "Décorations : la collection 2021-22 s'installe"
weight = 1

+++
Le froid et la neige n'ont pas découragé les artistes du Comité des fêtes ! La collection 2021-22 des décorations du village est installée sur le square de la place du Lac. Vous pouvez l'admirer et suivre l'exemple des enfants de l'école St Joseph en y déposant des galets décorés. Merci à toutes et tous !

{{<grid>}}{{<bloc>}}

![](/media/decoration-noel-2021-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2021-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-noel-2021-8.jpg)

{{</bloc>}}{{</grid>}}

Plus d'images sur le [Facebook du Comité des fêtes](https://www.facebook.com/comitedesfeteslalouvesc/).

Ça prend tournure aussi devant la mairie et bientôt sur la place Marrel... si la neige veut bien. En attendant, on installe les illuminations.

![](/media/decoration-noel-2021-4.jpg)

En cette fin d'année, le village sera en fête. On a bien besoin !