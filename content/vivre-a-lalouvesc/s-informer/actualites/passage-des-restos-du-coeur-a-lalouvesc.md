+++
date = 2023-01-03T23:00:00Z
description = "Mardi 10 janvier, 14h-17h"
header = "/media/logo-restos-du-coeur.jpg"
icon = ""
subtitle = ""
title = "Passage des restos du coeur à Lalouvesc"
weight = 1

+++
Les restos du cœur seront présents au CAC le mardi 10 janvier de 14h à 17h. Il s'agit d'une première prise de contact pour mesurer les besoins. Il n'y aura pas de distribution ce jour.

Pour plus d'information contacter la mairie : mairie@lalouvesc.fr, tél 04 75 67 83 67.

... ou directement les restos : ad07.annonay@lesrestosducoeur.org, tél 04 75 69 71 85 (n'hésitez pas à laisser un message).