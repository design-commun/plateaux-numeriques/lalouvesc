+++
date = 2022-03-15T23:00:00Z
description = "Ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 21 mars 2022"
weight = 1

+++
Un conseil municipal est convoqué à la mairie pour le lundi 21 mars à 20h;

Ordre du jour :

1. COMMISSION FINANCES
   1. Autorisation donnée au maire à engager des crédits d'investissement 2022 dans l'attente du vote du budget primitif 2022 (Délibération)
   2. Subventions aux associations (Délibération)
   3. Rémunération du travail d’un élu pour l’aménagement du local des employés municipaux
   4. Dénonciation d’un compromis de vente (bâtiment Ste Monique)
   5. Proposition d’achat de parcelles boisées par l’EHPAD Le Balcon des Alpes
   6. Provisions pour créances à recouvrer
   7. Point sur la préparation du budget primitif 2022
2. COMMISSION GESTION
   1. Informations sur les emplois communaux et ouverture de postes pour la saison à venir (Délibération)
   2. Point sur les chantiers
   3. Point sur la récupération du City Park
   4. Aménagements complémentaires du camping
   5. Point sur l’adressage (Délibération)
3. COMITÉ VIE LOCALE et COMMUNICATION
   1. Organisation des journées citoyennes
   2. Point sur le fleurissement
   3. Point sur le club des ados
   4. Transformation numérique des territoires - Subvention France Relance (Délibération)
4. COMITÉ DÉVELOPPEMENT ECONOMIQUE, TOURISTIQUE et URBANISME
   1. Point sur le jeu monument
   2. Rencontre avec EPORA
   3. Parcelle de M. BOL (ancien gérant de la Vie Tara)
   4. Indivision Simone CHAVET - rue St Jean François Régis/EPORA (Délibération)

5. DIVERS