+++
date = 2023-02-22T23:00:00Z
description = "Un agent communal polyvalent à plein temps"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "La mairie recrute"
weight = 1

+++
La commune de Lalouvesc recherche **un agent communal polyvalent** homme ou femme, 35h par semaine, CDD pouvant déboucher sur un CDI.

Parmi ses missions :

* entretien des espaces verts, débroussaillage, élagage, taille, arrosage des massifs,
* entretien du cimetière,
* entretien du camping,
* entretien de la voirie communale,
* tous travaux relatifs à l'ensemble de la commune.

Poste à pouvoir début mars.
Candidature à envoyer la Mairie de Lalouvesc :
Rue des Cévennes
07520 Lalouvesc
04 75 67 83 67
mairie@lalouvesc.fr