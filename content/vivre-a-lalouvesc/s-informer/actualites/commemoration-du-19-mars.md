+++
date = 2022-03-19T23:00:00Z
description = "Journée nationale du souvenir pour les victimes de la guerre d'Algérie"
header = "/media/18-mars-2022-monument-au-mort-2.jpg"
icon = ""
subtitle = ""
title = "Commémoration du 19 mars"
weight = 1

+++
Le 19 mars est journée nationale du souvenir et de recueillement à la mémoire des victimes civiles et militaires de la guerre d’Algérie et des combats en Tunisie et au Maroc. Le 19 mars est le jour anniversaire du cessez-le-feu en Algérie.

Une cinquantaine de personnes étaient présentes pour cette commémoration devant le Monument aux morts de Lalouvesc en ce samedi soir accompagnées par la Lyre louvetonne.

![](/media/18-mars-2022-monument-au-mort-1.jpg)