+++
date = 2021-03-31T22:00:00Z
description = "Votre équipe municipale a des ambitions..."
header = "/media/journee-citoyenne-27-03-2021-8.jpg"
icon = ""
subtitle = ""
title = "Golf : du mini au maxi !"
weight = 1

+++
La réussite impressionnante de la rénovation du mini-golf a donné des idées et de nouvelles ambitions à l'équipe municipale.

![](/media/journee-citoyenne-1-2021-31.jpg)

Pourquoi pas un vrai golf à 18 trous comme le mini pour concurrencer le Puy en Velay qui n'en a que neuf ? Cela pourrait être sur le camping même.. Il faudra néanmoins imaginer un parcours précis et sinueux pour éviter les tentes.

![](/media/journee-citoyenne-1-2021-1.jpg)

Les volontaires des journées citoyennes sont déjà à l'entraînement.

1er avril 2021.