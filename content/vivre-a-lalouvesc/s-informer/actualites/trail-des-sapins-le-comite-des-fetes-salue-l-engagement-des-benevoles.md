+++
date = 2021-11-11T23:00:00Z
description = "Merci à tous les bénévoles bien présents à toutes les étapes de la course et de l'événement"
header = "/media/trail-des-sapins-2021-course-1.JPG"
icon = ""
subtitle = "Le Comité des fêtes"
title = "Trail des Sapins : le Comité des fêtes salue l'engagement des bénévoles"
weight = 1

+++
Le comité des fêtes remercie l’ensemble des bénévoles qui ont aidé de près ou de loin à l’organisation du Trail des sapins du 30 octobre 2021 sous la pluie et avec la danse du vent.

**Merci** à la Commission Trail [qui œuvre toute l’année](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/15-une-rentree-au-pas-de-course/#trail-des-sapins--une-petite-entreprise) pour le bon déroulement du Trail. Si vous désirez la rejoindre vous serez les bienvenus.

![](/media/trail-2021-montage_chapiteaux_1.JPG "Montage des chapiteaux")

**MERCI** aux personnes ayant joué un rôle important dans la logistique : montage des quatre chapiteaux, mise en place de plus de 50 tables …

![](/media/trail-2021-inscriptions.JPG "Les inscriptions")

**MERCI** à l’équipe des inscriptions qui assure la remise des dossards et s’occupe du ravitaillement arrivé.

{{<grid>}}{{<bloc>}}

![](/media/trail-2021-equipe_buvette.jpg "La buvette")

{{</bloc>}}{{<bloc>}}

![](/media/trail-2021-martine.JPG)

{{</bloc>}}{{</grid>}}

**MERCI** à ceux qui s’occupe de la buvette qui ont improvisé un déménagement de dernière minute avec vin chaud et crêpière sous les bras.

{{<grid>}}{{<bloc>}}

![](/media/trail-2021-gerard.jpeg "La cuisson")

{{</bloc>}}{{<bloc>}}

![](/media/trail-2021-nicole_yveline.jpg "La disribution")

{{</bloc>}}{{</grid>}}

**MERCI** aux serveurs de repas qui ont assuré toute la soirée la distribution de plus de 900 repas.

![](/media/trail-2021-passe_sanitaire.JPG)

**MERCI** à la brigade anti-Covid qui a contrôlé les pass sanitaires sous la pluie.

![](/media/trail-2021-atelier-courges-2.jpg "Atelier courges")

**MERCI** aux personnes ayant découvert leur talent de décoration sur le thème d’Halloween : squelettes, courges creusées, feuillages…

{{<grid>}}{{<bloc>}}

![](/media/trail-2021-ravito_milagro.jpg "Milagro")

{{</bloc>}}{{<bloc>}}

![](/media/trail-2021-ravito__nd_4.JPG "Notre Dame d'Ay")

{{</bloc>}}{{</grid>}}

**MERCI** aux responsables des ravitaillements de Milagro, du Sardier, La Croix du Lionnet et Notre Dame d’Ay qui ont bravé le froid chapeauté par le pro du ravitaillement.

{{<grid>}}{{<bloc>}}

![](/media/trail-2021-michel-astier.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/trail-2021-thomas_leo.jpg)

{{</bloc>}}{{</grid>}}

**MERCI** à ceux assurant la sécurité : les baliseurs, les ouvreurs, les fermeurs, les signaleurs et les responsables du PC course.

Je suis fière de la réussite de ce Trail qui a mobilisé non seulement l’ensemble du village de Lalouvesc mais aussi des habitants de communes voisines : St Alban d’Ay, St Romain d’Ay, St Symphorien de Mahun et Rochepaule, heureuse aussi de l’implication des jeunes qui se sont engagés de leur propre initiative dans cette aventure.

_La présidente du comité des fêtes, Desgrand Fourezon Nathalie_