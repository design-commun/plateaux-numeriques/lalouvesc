+++
date = 2022-06-02T22:00:00Z
description = "Premier tour le 12 juin, candidatures et règles"
header = "/media/elections.jpg"
icon = ""
subtitle = ""
title = "Premier tour des élections législatives"
weight = 1

+++
Le premier tour des élections législatives se tiendra le 12 juin. Le bureau de vote sera ouvert à la mairie de Lalouvesc de 8h à 18h.

## [Rappel des candidatures. dans la 2e circonscription de l'Ardèche](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/candidatures-a-l-election-legislative-sur-le-nord-ardeche/)

## Rappel des règles du scrutin

Les députés sont élus au scrutin uninominal majoritaire à deux tours. Bien qu’élus dans une circonscription, ils sont investis d’un mandat national.

Pour être élu au premier tour de scrutin, il faut recueillir la majorité absolue des suffrages exprimés et un nombre de voix égal au quart des électeurs inscrits. Au second tour, la majorité relative suffit. En cas d’égalité de suffrages, le plus âgé des candidats est élu.

Pour qu’un candidat ait le droit de se présenter au second tour, il doit avoir obtenu au premier tour un nombre de voix au moins égal à 12,5% du nombre des électeurs inscrits dans la circonscription. Si un seul candidat remplit cette condition, le candidat ayant obtenu le plus grand nombre de suffrages après lui peut se maintenir au second tour. Dans le cas où aucun candidat ne remplit cette condition, seuls les deux candidats arrivés en tête peuvent se maintenir au second tour.