+++
date = 2021-05-19T22:00:00Z
description = "Déjà du monde pour la réouverture des bars et des restaurants..."
header = "/media/ouverture-terrasse-2021-1.jpg"
icon = ""
subtitle = ""
title = "Ouverture des terrasses à Lalouvesc"
weight = 1

+++
Il faisait encore frisquet à Lalouvesc ce 19 mai 2021, mais, comme partout en France, les terrasses des bars et des restaurants ont ouvert avec un grand sourire après ce trop long hiver sanitaire.

Au Vivarais, on a servi onze couverts.

{{<grid>}}

{{<bloc>}}

![](/media/ouverture-terrasse-2021-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ouverture-terrasse-2021-4.jpg)

{{</bloc>}}

{{<grid>}}