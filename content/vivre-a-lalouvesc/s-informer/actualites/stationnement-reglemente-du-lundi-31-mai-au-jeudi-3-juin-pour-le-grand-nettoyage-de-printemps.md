+++
date = 2021-05-27T22:00:00Z
description = "Tout stationnement interdit sur les rues et les routes"
header = "/media/nettoyage-2021.jpg"
icon = ""
subtitle = ""
title = "Stationnement réglementé du lundi 31 mai au jeudi 3 juin pour le grand nettoyage de printemps"
weight = 1

+++
Nous sortons de l'hiver, les estivants seront bientôt là, le village se fait propre.

Pendant trois jours, lundi, mardi et mercredi prochains, les employés communaux vont se consacrer exclusivement au nettoyage des rues, destruction des mauvaises herbes, balayage. Jeudi la société AVBL passera dans toutes les rues avec un camion balayeur et aspirateur. Ainsi en une fois, l'ensemble du village sera nettoyé.

Pour pouvoir être correctement menée à bien, cette opération nécessite de dégager complètement l'accès aux chaussées. Nous comptons sur votre collaboration. A cette fin **tout stationnement dans les rues est interdit par arrêté municipal du lundi 31 mai à 7h au jeudi 3 juin 2021 à 21h**. Le stationnement sera concentré sur les places des parkings (Place du Lac, Chemin Grosjean, Camping, Ste Monique, Maison de retraite).

Attention, le stationnement dans les rues sera considéré comme gênant. Nous vous remercions d'en tenir compte. La gendarmerie est chargée de faire respecter l'arrêté et tout contrevenant s'expose à des conséquences désagréables.

[Arrêté du Maire](/media/99_ar-007-210701280-20210527-2021_008_a-ar-1-1_1.pdf).