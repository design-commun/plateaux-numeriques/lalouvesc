+++
date = 2022-12-21T23:00:00Z
description = "Carburant à emporter, alcool sur la voie publique, pétards et feux d'artifice"
header = ""
icon = ""
subtitle = ""
title = "Interdictions sur la période du 30 déc 2022 au 2 janvier 2023"
weight = 1

+++
[Arrêté préfectoral](/media/fetes-de-fin-d-annee-ap-diverses-mesures-de-police-administrative.pdf) :

Du **vendredi 30 décembre 2022 au lundi 2 janvier 2023 inclus,** sont interdits sur l'ensemble du département de l'Ardèche :

\- **la détention, le transport, la distribution, la vente et l'achat de carburant à emporter dans tout récipient transportable,** sauf nécessité dûment justifiée par le client et vérifiée, en tant que de besoin, avec le concours des services de police et de gendarmerie locaux.

Les détaillants, gérants et exploitants de stations services, notamment de celles qui disposent d'appareils automatisés permettant la distribution de carburants, devront prendre les dispositions nécessaires pour faire respecter cette interdiction.

**- la consommation de boissons alcoolisées sur la voie publique** en dehors des lieux spécialement réservés à cet effet.

\- **la détention, le transport, la vente, l'achat et l'usage de feux d'artifice et pétards de catégories F2, F3 et T1 sur la voie publique.**

Cette interdiction ne s'applique pas aux spectacles pyrotechniques dûment déclarés dans les délais réglementaires et tirés par des artificiers titulaires d'un certificat de qualification en cours de validité.