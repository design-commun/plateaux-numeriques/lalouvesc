+++
date = 2021-10-01T22:00:00Z
description = "L'OT présente 50 ans de météo France à l'occasion de la fête de la science... "
header = "/media/meteo-france-ot-evolution-des-temperatures-2.jpg"
icon = ""
subtitle = "Plus possible d'être climatosceptique à Lalouvesc !"
title = "Le réchauffement climatique mesuré à Lalouvesc"
weight = 1

+++
Les pères de la Maison St Régis accueillent la station météo du village. Grâce à cela, Séverine Moulin de l'Office du tourisme avec la complicité de Météo-France propose pendant quelques jours une exposition sur l'évolution des températures et de la pluviométrie sur 50 ans dans le cadre de la fête de la science.

Nous en avons tiré deux graphiques.

Le premier montre la différence de la moyenne annuelle des températures minimales quotidiennes mesurées chaque année par rapport à une référence fixe (la moyenne de ces températures sur 30 ans, de 1981 à 2010).

![](/media/meteo-france-ot-evolution-des-temperatures-1.jpg)

Le second suit la même logique, mais pour la moyenne annuelle des températures maximales.

![](/media/meteo-france-ot-evolution-des-temperatures-2.jpg)

Le constat est sans appel : il fait de moins en moins froid et de plus en plus chaud à Lalouvesc, même si quelques années s'écartent de la tendance générale. 2020 est l'année la plus chaude depuis 50 ans.

Pour tout savoir, allez vite voir l'exposition à la Maison St Régis.