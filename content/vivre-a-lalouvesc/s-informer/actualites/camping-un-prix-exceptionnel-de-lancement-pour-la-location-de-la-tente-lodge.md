+++
date = 2021-07-19T22:00:00Z
description = "Profitez du confort canadien..."
header = "/media/lodge-2.jpg"
icon = ""
subtitle = ""
title = "Camping : un prix exceptionnel de lancement pour la location de la tente Lodge"
weight = 1

+++
Mariez les joies du camping et le confort cosy...

**Exceptionnel : pour la première saison d'été un prix exceptionnel de 400 € est proposé pour chaque location d'une semaine de ce produit. Celle-ci apparaîtra automatiquement au moment de la réservation.**

{{<grid>}}

{{<bloc>}}

![](/media/lodge-2.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/lodge-3.jpg)

{{</bloc>}}

{{</grid>}}

Cinq places, deux chambres, sur plancher bois, poêle à bois, entièrement équipée, coin cuisine sur la terrasse de 10m2. Le confort canadien en Ardèche. Location disponible à partir du 1er juin 2021.

Vous pouvez vérifier la disponibilité de cet hébergement et effectuer votre réservation à l'aide de l'application ci-dessous. Attention, les forfaits d'une semaine démarrent nécessairement un samedi.

<script type="text/javascript" src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"></script>

<script type="text/javascript">

( function() {

    var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"saexsbE", idIntegration:1195, langue:"fr", ui:"OSCA-116693-11" } );
    
    widgetProduit.Initialise();

})();

</script>

<div id="widget-produit-OSCA-116693-11"></div>