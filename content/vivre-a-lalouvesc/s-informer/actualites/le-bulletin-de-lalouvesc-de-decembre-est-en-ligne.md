+++
date = 2022-11-30T23:00:00Z
description = "n°29 - décembre 2022 - D'un chantier à l'autre"
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "n°29 - décembre 2022 - D'un chantier à l'autre"
title = "Le Bulletin de Lalouvesc de décembre est en ligne"
weight = 1

+++
_Dans ce Bulletin, deux chantiers qui redessinent le village sont présentés, un qui s’achève (l’écolotissement du Bois de Versailles) et un qui va démarrer (l’espace Beauséjour). On y trouve aussi des nouvelles des employés municipaux, du recensement, du centre équestre, du Club des deux clochers, le premier épisode du nouveau feuilleton des enfants de l'école et encore, comme toujours, bien d’autres informations._

[**Lire le Bulletin**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/)

### Sommaire

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#mot-du-maire)
* [Actualités de la Mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#actualités-de-la-mairie)
  * [De nouveaux employés municipaux](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#de-nouveaux-employés-municipaux)
  * [Un véhicule électrique pour la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#un-véhicule-électrique-pour-la-mairie)
  * [Les plaques de rues s’installent](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#les-plaques-de-rues-sinstallent)
  * [Le recensement se rapproche](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#le-recensement-se-rapproche)
  * [L’étude sur le Cénacle et Ste Monique avance](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#létude-sur-le-cénacle-et-ste-monique-avance)
* [Zoom sur deux chantiers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#zoom-sur-deux-chantiers)
  * [Le Bois de Versailles est un succès](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#le-bois-de-versailles-est-un-succès)
  * [L’espace Beauséjour est dessiné](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#lespace-beauséjour-est-dessiné)
* [Sanctuaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#sanctuaire)
  * [Répétitions de chants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#répétitions-de-chants)
* [Commerces](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#commerces)
  * [Atouts Val d’Ay](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#atouts-val-day)
* [Culture - Loisir](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#culture---loisir)
  * [Appel pour les décorations de Noël](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#appel-pour-les-décorations-de-noël)
  * [Assemblée générale de la Lyre louvetonne](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#assemblée-générale-de-la-lyre-louvetonne)
  * [Succès pour les cavalières de Fontcouverte et labels de qualité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#succès-pour-les-cavalières-de-fontcouverte)
  * [Le Téléthon au Club des deux clochers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#le-téléthon-au-club-des-deux-clochers)
  * [Une belle année à la Cabane des marmots](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#une-belle-année-à-la-cabane-des-marmots)
* [Santé - sécurité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#santé---sécurité)
  * [Suite du calendrier Santé-Environnement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#suite-du-calendrier-santé-environnement)
* [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#dans-lactualité-le-mois-dernier)
* [Feuilleton : _Le tour du monde de Gulli_](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/29-d-un-chantier-a-l-autre/#feuilleton--_le-tour-du-monde-de-gulli_)