+++
date = 2022-03-13T23:00:00Z
description = "Lalouvesc sur TF1 (lien direct sur la vidéo)"
header = "/media/tf1-mars-2022-1.jpg"
icon = ""
subtitle = ""
title = "Lalouvesc sur TF1 à 13h, le 14 mars"
weight = 1

+++
Une équipe de tournage de TF1 est passée mardi dernier à Lalouvesc pour réaliser un reportage sur la distance entre les villages et les services de l'Etat. Ils ont interviewé le Maire et rencontré plusieurs commerçants.

Le reportage est passé au journal de 13h du mardi 14 mars, à voir en [replay.](https://www.tf1.fr/tf1/jt-13h/videos/le-13-heures-du-14-mars-2022-56567970.html) (à la 24ème minute), ou directement ci-dessous :

<iframe width="560" height="315" src="https://www.tf1.fr/tf1/jt-13h/videos/acces-aux-services-publics-de-grandes-inegalites-57788403.html" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



{{<grid>}}{{<bloc>}}

![](/media/tf1-mars-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/tf1-mars-2022-3.jpg)

{{</bloc>}}{{</grid>}}