+++
date = 2023-04-05T22:00:00Z
description = "Temps partiel Annonay/Tournon"
header = "/media/cidf-2023.png"
icon = ""
subtitle = ""
title = "Le CIDFF recrute"
weight = 1

+++
Le [CIDFF de l’Ardèche](https://ardeche.cidff.info/), Centre d'information sur les droits des femmes et des familles, recrute **UN(E) CHARGÉ(E) DE MISSIONS TRANSVERSALES** sur **ANNONAY/TOURNON SUR RHÔNE :**

* CDI,
* Temps partiel de 24 heures mensuelles,
* Diplôme CESF ou AS (Bac+3) et débutant.e accepté.e
* 13.50 € de l’heure + prime de sujétion pendant entretiens et droits de visite de 10,08 € de l’heure
* Poste évolutif en terme de temps de travail.

Trois missions :

* **Mission 1 :** Ecrivain.e public.que
* **Mission 2** : Intervenant.e en espace de rencontres des familles
* **Mission 3** : Animateur de groupe d’échanges mère/enfants

Possibilité de recevoir la fiche de poste par mail sur demande auprès du secrétariat.

Vous voulez postuler ??

Merci d'envoyer une lettre de motivation et un Curriculum Vitae, par mail, au CIDFF de l'Ardèche à [cidff07@cidff07.fr](mailto:cidff07@cidff07.fr).