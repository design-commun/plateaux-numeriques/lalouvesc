+++
date = 2021-06-11T22:00:00Z
description = "Bureau de votes, compétences des collectivités, programmes des candidats"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Elections régionales et départementales dans la nouvelle salle du Conseil"
weight = 1

+++
Dimanche 20 juin, nous votons pour renouveler les conseillers régionaux, et les conseillers départementaux. Deux bulletins de vote donc à déposer dans deux urnes différentes.

Les bureaux de vote seront ouverts de 8h à 18h dans la nouvelle salle du Conseil à la Mairie. Une bonne occasion pour tous les citoyens louvetous de la découvrir.

Ci dessous quelques liens pour éclairer votre choix.

#### Élection régionale

* [Compétences](https://www.vie-publique.fr/eclairage/38411-les-competences-des-regions-apercu-apres-la-loi-notre) des régions
* [Programmes](https://programme-candidats.interieur.gouv.fr/elections-regionales-2021/listes-candidates.html?region=84&tour=1) des candidats aux régionales pour Auvergne-Rhone-Alpes

#### Élection départementale

* [Compétences](https://www.vie-publique.fr/fiches/19620-quelles-sont-les-competences-exercees-par-les-departements) des départements
* [Programmes](https://programme-candidats.interieur.gouv.fr/elections-departementales-2021/candidats.html?canton=08&departement=07&tour=1) des candidats aux départementales pour le Haut-Vivarais

#### Vote par procuration

Si vous ne pouvez vous déplacer le jour du vote, vous pouvez demander à quelqu'un d'autre de voter à votre place. Mais attention, cela demande quelques démarches, notamment de se déplacer dans un commissariat ou une gendarmerie. [Tout est expliqué ici](https://www.service-public.fr/particuliers/vosdroits/F1604).