+++
date = 2021-11-10T23:00:00Z
description = "Office religieux, défilé, Monument aux morts, les Louvetous étaient présents"
header = "/media/11-novembre-2021-2.jpg"
icon = ""
subtitle = ""
title = "Lalouvesc se réunit pour la cérémonie du 11 novembre"
weight = 1

+++
Avec la réduction des restrictions sanitaires, les Louvetous étaient conviés, cette année, aux différentes étapes de la cérémonie du 11 novembre : présentation dans la crypte de la Basilique, office religieux, défilé de la Lyre louvetonne et des pompiers sur la rue principale et le moment de recueillement au Monument aux morts : dépôt de gerbe, lecture par le Maire de la lettre de la Ministre rappelant le sens de cette journée du souvenir pour les morts pour la France et enfin musique.

Un moment émouvant et de retrouvailles pour le village qui s'est terminé par un verre de l'amitié à la Mairie. Les Louvetous étaient présents en nombre, ce fut sympathique.

{{<grid>}}{{<bloc>}}

![](/media/11-novembre-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-novembre-2021-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-novembre-2021-7.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/11-novembre-2021-5.jpg)

{{</bloc>}}{{</grid>}}

Rappel : pour mesurer l'importance du souvenir, on peut lire ou relire le mémoire de Paul Besset sur la Guerre de 14-18 à Lalouvesc : [_Morts pour la France_](/media/morts-pour-la-france-de-la-louvesc-paul-besset.pdf).

[![](/media/memoire-14-18-p-besset.jpg)](/media/morts-pour-la-france-de-la-louvesc-paul-besset.pdf)