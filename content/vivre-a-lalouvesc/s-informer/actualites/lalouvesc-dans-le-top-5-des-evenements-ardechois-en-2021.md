+++
date = 2022-04-05T22:00:00Z
description = "Publication du bilan de l'ADT"
header = "/media/trail-des-sapins-2021-course-5.jpeg"
icon = ""
subtitle = ""
title = "Lalouvesc dans le top 5 des événements ardéchois en 2021"
weight = 1

+++
Ardèche Tourisme vient de publier le[ bilan 2021 de l'événementiel culturel et sportif](https://pro.ardeche-guide.com/l-evenementiel-sportif-et-culturel) et Lalouvesc se trouve particulièrement bien placé.

![](/media/adt-2021-classement-evenements.jpg)

On trouve le Trail des Sapins en quatrième position pour les événements sportifs et Le Carrefour des Arts aussi en quatrième position pour les événements culturels. Sans doute la situation sanitaire a fait de cette année une année particulière avec l'annulation de certains événements dont l'Ardéchoise. Mais cette situation a pesé sur notre village tout comme sur l'ensemble de l'Ardèche

Et à Lalouvesc, on a du caractère et on ne lâche rien ! Les programmes de 2022 se concoctent en ce moment et laissent présager une affluence encore plus nombreuse.

**Bravo aux organisateurs et aux bénévoles ! Rejoignez les équipes qui gagnent : adhérez au Comité des fêtes et au Carrefour des arts.**

[Annuaire des associations](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/annuaire-des-associations/)