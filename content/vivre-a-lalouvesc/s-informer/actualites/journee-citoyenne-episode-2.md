+++
date = 2021-03-27T23:00:00Z
description = "Nettoyage du tennis, peinture sur le mini-golf, réparations et ménage des refuges... une journée productive "
header = "/media/journee-citoyenne-27-03-2021-15.jpg"
icon = ""
subtitle = ""
title = "Journée citoyenne, épisode 2"
weight = 1

+++
![](/media/journee-citoyenne-27-03-2021-2.jpg)

![](/media/journee-citoyenne-27-03-2021-6.jpg)

![](/media/journee-citoyenne-27-03-2021-4.jpg)

![](/media/journee-citoyenne-27-03-2021-8.jpg)

![](/media/journee-citoyenne-27-03-2021-9.jpg)

Plus de dix camions évacués, la moitié du tennis nettoyé...

![](/media/journee-citoyenne-27-03-2021-5.jpg)

![](/media/journee-citoyenne-27-03-2021-7.jpg)

![](/media/journee-citoyenne-27-03-2021-16.jpg)

![](/media/journee-citoyenne-27-03-2021-12.jpg)

Du rouge et du vert, une spectaculaire transformation du minigolf... jamais il n'aura été aussi beau !

Et aussi un peu plus bas dans le bois, six refuges des Afars entièrement nettoyés et réparés !

Une vingtaine de citoyens ont participé. Une journée de travail dans la bonne humeur, sympathique, productive. Certains qui ne pouvaient être présents avaient préparé des cakes, amené des mignardises et des sandwichs qui ont été fort appréciés. Merci à toutes et tous !

Mais nous n'étions pas assez nombreux pour tout finir.  Il faudra donc encore poursuivre pour que le camping soit prêt à l'ouverture de la saison. Plusieurs ont indiqué qu'ils allaient continuer durant les semaines prochaines. Nous devrons aussi prévoir une journée supplémentaire au mois d'avril.

Nous avons besoin de renouveler les troupes, il faudra de la relève, que plus de Louvetous-citoyens et citoyennes s'engagent pour que notre bien commun, le camping, retrouve toutes ses couleurs grâce à un vrai effort collectif. Pour paraphraser John Fitzgerald Kennedy dans le discours inaugural de son investiture, l'un des plus célèbres de l'histoire américaine : "**_ne vous demandez pas ce que votre village peut faire pour vous, mais demandez-vous ce que vous pouvez faire pour votre village_**".

Voir aussi l'article de la Commère 43 : "[A Lalouvesc, les villageois redonnent de l'éclat au camping](https://www.lacommere43.fr/dans-le-07/item/37399-a-lalouvesc-les-villageois-redonnent-de-l-eclat-au-camping.html)"