+++
date = 2022-07-05T22:00:00Z
description = "Route de Bobigneux coupée au niveau du hameau"
header = "/media/panneau-route-barree.gif"
icon = ""
subtitle = ""
title = "Réparation d'un mur de soutènement au Besset"
weight = 1

+++
Afin de réaliser l'enrochement nécessaire à la réparation du mur écroulé qui soutenait la route, la route de Bobigneux sera coupée pendant quelques jours au niveau du hameau du Besset.

Les travaux seront réalisés par l'entreprise Ducoin.

L'accès à partir du village sera toujours possible.