+++
date = 2022-05-05T22:00:00Z
description = "Mise en place de la signalisation à partir du 9 mai"
header = "/media/panneau-rue-partagee.jpg"
icon = ""
subtitle = ""
title = "Rue de la Fontaine, une rue partagée"
weight = 1

+++
La Mairie et les habitants de la rue de la Fontaine se sont [mis d'accord cet automne](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/14-les-beaux-gestes/#rue-de-la-fontaine-une-rue-partag%C3%A9e) pour faire de la rue de la fontaine une "rue partagée". Il est temps avant l'ouverture de la saison de placer la signalisation qui concrétisera cette décision.

Qu'est-ce qu'une rue partagée ?

![](/media/rue-de-la-fontaine-p-roger.png)

La société Stineo Signalisation interviendra du 9 au 11 mai pour mettre en place la nouvelle signalisation. Attention, après la mise en place de cet signalisation, tout véhicule contrevenant sera en infraction et pourra être sanctionné.