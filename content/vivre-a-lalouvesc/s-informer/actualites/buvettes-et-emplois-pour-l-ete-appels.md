+++
data_specificites = ""
data_type = ""
date = 2021-03-14T23:00:00Z
description = "Le camping municipal développe son activité, Pour y faire face, la Mairie recrute deux postes à temps partiels et propose l'ouverture d'une buvette éphémère. Une seconde buvette est envisagée au parc du Val d'Or."
header = "/media/camping-apero-test.jpg"
icon = ""
layout = ""
subtitle = ""
title = "Emplois pour l'été et buvettes éphémères, appels"
weight = 1

+++
Le camping municipal développe largement son activité. Pour y faire face, la Mairie recrute deux postes à temps partiel et propose l'ouverture d'une buvette éphémère. Une seconde buvette est envisagée au parc du Val d'Or.

## Emplois d'été

Deux postes à temps partiel en CDD sont proposés :

* un poste de préposé·e au ménage pour le camping, les gîtes et les bâtiments municipaux. Remplacements ponctuels à l'accueil du camping. Période : mi-avril, mai, juin, juillet, août.
* un poste d'animateur·trice-enfants titulaire du BAFA. Mi-temps juillet-août.

Envoyer CV et lettre de motivation à la Mairie mairie@lalouvesc.fr.

# Appel à manifestation d’intérêts pour deux bar-buvettes éphémères

La mairie de Lalouvesc souhaite développer l’attractivité du village pendant la saison estivale. Pour cela il lui paraît judicieux que deux points provisoires de petite restauration (glaces, jus, cafés, viennoiseries, etc.) viennent s’ajouter à l’offre existante : l’un sur le camping, l’autre sur le parc du Val d’Or.  
La Commune fournira le local et les branchements (eau, électricité), les opérateurs intéressés auront la responsabilité de l’offre et du service commercial. Un prix de location raisonnable sera appliqué.  
Les deux propositions sont indépendantes. Les propositions peuvent comprendre l’un ou l’autre emplacement ou les deux.  
Les opérateurs intéressés peuvent prendre contact avec la Mairie par mél [mairie@lalouvesc.fr](mailto:mairie@lalouvesc.fr) ou par téléphone : [04 75 67 83 67](tel:+330475678367)