+++
date = 2022-09-01T22:00:00Z
description = "Rendez-vous au CAC le 8 septembre"
header = "/media/logo-club-2-clochers.jpg"
icon = ""
subtitle = ""
title = "Le club des deux clochers reprend ses activités"
weight = 1

+++
Après avoir libéré la salle de réunion Louis BESSET au Carrefour des Arts durant la torpeur de l'été, le Club des deux clochers reprend ses réunions hebdomadaires à partir du jeudi 8 septembre.

Nous espérons vous retrouver nombreux pour une nouvelle saison pleine de rencontres et d'activités joyeuses.

Le Club des deux clochers est une ancienne association louvetonne. Elle a pour objectif de réunir les habitants de Lalouvesc et même au delà pour:

* de fraternelles et récréatives rencontres du jeudi,
* de rompre le néfaste isolement individuel qui s'installe parfois,
* de participer collectivement aux divers ateliers proposés et aux manifestations qui animent la vie communale,
* d'aborder et apprendre les nouvelles technologies et même plus...

Nous sommes une quarantaine et chacun y trouve sa place.

Le Club des deux clochers souffre de l'image éculée de "vieux racornis" jouant aux cartes. Pour casser ce mythe, vous qui hésitez à nous rejoindre, venez goûter avec nous ce jeudi 8 septembre ou un autre. Ainsi vous serez libre de savoir si l'ambiance du Club vous plait ou pas. Quoiqu'il en soit, votre visite nous sera agréable.

En ces moments de fortes incertitudes, les parenthèses du jeudi du Club des deux clochers apportent ce brin d'espoir de notre devise "Ensemble et solidaire".

![](/media/gateau-club-2-clochers-2022.jpg)