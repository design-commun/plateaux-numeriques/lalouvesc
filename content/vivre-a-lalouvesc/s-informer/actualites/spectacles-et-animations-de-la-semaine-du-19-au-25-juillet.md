+++
date = 2021-07-16T22:00:00Z
description = "En juillet, il se passe tous les jours quelque chose à Lalouvesc"
header = "/media/copie-de-20210703_112053.jpg"
icon = ""
subtitle = "Que faire cette semaine ?"
title = "Spectacles et animations de la semaine du 19 au 25 juillet"
weight = 1

+++
En plus des [nombreuses activités](/decouvrir-bouger/activites/) possibles, voici les spectacles et manifestations proposés à Lalouvesc du 19 au 25 juillet :

### Tous les jours

* 14h30 à 18h30 **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal
* 10h à 12h et de 15h à 18h **Exposition _La matière habitée_**  sculptures à la chapelle Saint Ignace

### 19 juillet

* 9h **Gym**,  au camping
* 10h30 **Visite du jardin en permaculture** au Mont Besset
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 21h cinéma, **La fine fleur**, [bande annonce](https://youtu.be/Fy2_VLFItHs) à l'Abri du pèlerin

### 20 juillet

* 9h **Gym**, au camping
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 17h conférence, **Balades, contes et patrimoine : histoires paysannes et vie des arbres à l’ère Laudato Si’**, _Paul ROY_ à la Maison St Régis
* 18h : visites estivales **Paroles d'habitants**, départ à l'Office du tourisme

### 21 juillet

* 15h-17h empruntez des livres à la **bibliothèque**
* 17h **Apéro découverte** devant l'Office du tourisme
* 17h conférence, **Aux sources de la civilisation occidentale : les apports grecs** _Dominique ESTRAGNAT_ à la Maison St Régis
* 20h concert, **Groupe Kimya**, _Between mist and sky_ à la Grange de Polly
* 21h cinéma, **Envole-moi**, [bande annonce](https://youtu.be/rVckCelfruE) à l'Abri du pèlerin

### 22 juillet

* 9h **Gym**, au camping
* 10h **Balade à la découverte des plantes comestibles et leurs usages culinaires**, rdv devant la Maison St Régis (sur inscription Office du tourisme)
* 13h-16h **Animation enfants**, au camping (sur inscription)
* 21h cinéma, **Nomadland** (vf), Lion d'or à la Mostra de Venise 20201, Golden Globe du meilleur film dramatique et Oscar du meilleur film en 2021. [bande annonce](https://youtu.be/mGjTyjGHnyE) à l'Abri du pèlerin

### 23 juillet

* 17h conférence, **Aux sources de la civilisation occidentale : les apports romains** _Dominique ESTRAGNAT_ à la Maison St Régis

### 24 juillet

* 9h30-11h30 empruntez des livres à la **bibliothèque**
* 21h cinéma, **Envole-moi**, [bande annonce](https://youtu.be/rVckCelfruE) à l'Abri du pèlerin

### 25 juillet

* 21h cinéma, **Nomadland** (vf), Lion d'or à la Mostra de Venise 20201, Golden Globe du meilleur film dramatique et Oscar du meilleur film en 2021. [bande annonce](https://youtu.be/mGjTyjGHnyE) à l'Abri du pèlerin