+++
date = 2021-11-09T23:00:00Z
description = "Quatre lieux, des galets, un effort de tous et un concours"
header = "/media/deco-noel-2.jpg"
icon = ""
subtitle = ""
title = "Décorations : le village sera en fête pour la fin d'année"
weight = 1

+++
Une réunion s'est tenue à la mairie entre des représentant(e)s du Comité des fêtes et des élus pour imaginer et préparer la construction des décorations du village pour Noël, la fin d'année et la nouvelle année 2022. Et les idées n'ont pas manquées... il ne reste plus que la mise en pratique. Sans divulgacher, voici quelques pistes et un appel à toutes et tous pour que le village soit en fête.

### Quatre lieux

![](/media/deco-noel.jpg)

Quatre lieux seront décorés par le Comité avec l'aide de la Mairie : le square de la place Marrel, devant la Basilique, privilégiera Noël, le thème de la nativité ; devant la Mairie, ce sera le coin des enfants ; sur le square de la place du Lac, on retrouvera le père noël, son traîneau et tout le chemin qu'il doit parcourir pour arriver à Lalouvesc et exaucer les vœux des petits et grands... et enfin un dernier lieu : l'hôtel Beauséjour pour un adieu avant disparition, le thème sera une surprise... disons simplement qu'on devrait retrouver quelques-uns de ses derniers occupants.

### Des galets, un village entièrement décoré par tous

![](/media/deco-noel-4.jpg)

Vous pouvez tous participer à cet effort :

* dès à présent en ramassant des galets et en les décorant avec le nom du village selon le principe de [trouve mon galet 07](https://www.facebook.com/groups/368185797160953/), nous en aurons besoin le moment venu sur le square ;
* en répondant au prochain appel du Comité des fêtes pour la construction et l'installation ;
* et, bien sûr en décorant vos maisons, jardins, magasins dès début décembre.

L'installation des décors démarrera début décembre et se poursuivra sur les quinze premiers jours du mois.

### Et un concours...

Il y aura aussi un concours ouvert à tous les visiteurs... mais gardons quelques infos pour le Bulletin de décembre.