+++
date = 2023-03-31T22:00:00Z
description = "n°33 Beauté et sourires"
header = "/media/entete-ete.jpg"
icon = ""
subtitle = ""
title = "Le Bulletin d'information du mois d'avril est paru"
weight = 1

+++
# 33 - Beauté et sourires

## n°33 avril 2023

En ces périodes de crises nationales et internationales, nous avons bien besoin de beauté et de sourires. C’est ce que nous proposent pour cet été le Carrefour des Arts, le Théâtre de la veillée, le Comité des fêtes, le centenaire de l’Abri du pèlerin. Consultez un avant-goût du programme dans ce Bulletin et, comme toujours, vous y trouverez bien d’autres informations. 

[**Lire le Bulletin**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/)

### Sommaire

* [Mot du Maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#mot-du-maire)
* [Actualités de la Mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#actualités-de-la-mairie)
  * [Bienvenue aux nouveaux conseillers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#bienvenue-aux-nouveaux-conseillers)
  * [Un nouvel employé municipal](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#un-nouvel-employé-municipal)
  * [Stade et City-Park](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#stade-et-city-park)
  * [Les constructions démarrent sur l’écolotissement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#les-constructions-démarrent-sur-lécolotissement)
  * [Rencontrez notre Députée !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#rencontrez-notre-députée-)
* [Zoom : Demandez le programme](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#zoom--demandez-le-programme)
  * [Le menu alléchant du Carrefour des arts](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#le-menu-alléchant-du-carrefour-des-arts)
  * [Théâtre de la Veillée : dialogue avant la représentation](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#théâtre-de-la-veillée--dialogue-avant-la-représentation)
  * [Les prochaines manifestations du Comité des fêtes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#les-prochaines-manifestations-du-comité-des-fêtes)
* [Nouvelles du Sanctuaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#nouvelles-du-sanctuaire)
  * [Ordination d’un prêtre à la Basilique](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#ordination-dun-prêtre-à-la-basilique)
  * [Les 100 ans de l’Abri du pèlerin se préparent](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#les-100-ans-de-labri-du-pèlerin-se-préparent)
* [Office du tourisme : les premiers fruits d’une collaboration](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#office-du-tourisme--les-premiers-fruits-dune-collaboration)
* [Commerces - braderie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#commerces---braderie)
  * [Le foyer de skis ferme… et brade](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#le-foyer-de-skis-ferme-et-brade)
  * [Bientôt un bar à bière à Lalouvesc](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#bientôt-un-bar-à-bière-à-lalouvesc)
  * [_Le plaisir des yeux_ réouverture](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#_le-plaisir-des-yeux_-réouverture)
* [Deux appels à participation](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#deux-appels-à-participation)
* [Dans l’actualité du mois dernier de lalouvesc.fr](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#dans-lactualité-du-mois-dernier-de-lalouvescfr)
* [Pour vous entraîner…](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/33._beaute_et_sourires/#pour-vous-entraîner)