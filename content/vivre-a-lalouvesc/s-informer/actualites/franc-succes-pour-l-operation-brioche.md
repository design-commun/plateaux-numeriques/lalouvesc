+++
date = 2022-10-08T22:00:00Z
description = "Au profit de l'ADAPEI"
header = "/media/operation-brioches-2022-2.jpg"
icon = ""
subtitle = ""
title = "Franc succès pour l'opération brioche"
weight = 1

+++
{{<grid>}}{{<bloc>}}

L'opération brioche a rencontré un franc succès .  
Le montant reversé à l ADAPEI est de 821 euros.  
Les quelques brioches restantes seront offertes à l'Ehpad et à l'école.  
Merci à tous ceux qui ont contribué à cette belle réussite !

{{</bloc>}}{{<bloc>}}

![](/media/operation-brioches-2022.jpg)

{{</bloc>}}{{</grid>}}