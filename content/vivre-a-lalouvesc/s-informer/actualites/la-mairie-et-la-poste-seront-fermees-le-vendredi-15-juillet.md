+++
date = 2022-07-06T22:00:00Z
description = "Fermeture exceptionnelle en raison de la fête nationale"
header = "/media/Mairie-Lalouvesc-4x1-1280w-dithered.jpg"
icon = ""
subtitle = ""
title = "La Mairie et la poste seront fermées le vendredi 15 juillet"
weight = 1

+++
La Mairie et l'agence postale communale seront fermées exceptionnellement le vendredi 15 juillet, lendemain de la fête nationale.