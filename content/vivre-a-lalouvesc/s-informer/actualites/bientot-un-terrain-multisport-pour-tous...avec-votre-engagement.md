+++
date = 2021-04-09T22:00:00Z
description = "Le mini-golf est presque terminé, le nettoyage du tennis a encore besoin de bras. Si nous sommes assez nombreux, une demi-journée de travail citoyen devrait suffire. Rendez-vous le samedi 17 avril au matin à partir de 8h30. Amenez vos pelles, pioches, bêches et brouettes !"
header = "/media/journee-citoyenne-27-03-2021-8.jpg"
icon = ""
subtitle = ""
title = "Bientôt un terrain multisport pour tous... avec votre engagement"
weight = 1

+++
Même s'ils sont situés en bordure du camping, le mini-golf et le terrain multisport que nous souhaitons installer sur l'ancien tennis pourront profiter à tous les Louvetous et à tous les estivants, petits ou grands.

Le mini-golf est presque terminé, le nettoyage du tennis a encore besoin de bras. Si nous sommes assez nombreux, une demi-journée de travail citoyen devrait suffire. Nous vous proposons de nous retrouver le samedi 17 avril au matin à partir de 8h30. Amenez vos pelles, pioches, bêches et brouettes !

![](/media/journee-citoyenne-27-03-2021-9.jpg)

Bien sûr, ces travaux se réaliseront toujours dans la stricte observation des mesures sanitaires et avec l'approbation de la préfecture. Des masques et du gel hydroalcoolique seront disponibles et les distances seront respectées au cours des travaux. Nous en avons maintenant une bonne pratique.

Certaines Louvetonnes et certains Louvetous ont déjà beaucoup donné. La solidarité d'une communauté se mesure aussi au partage des efforts. **Nous serions heureux de voir des têtes nouvelles à cette dernière demi-journée.**

{{<grid>}}

{{<bloc>}}

![](/media/minigolf-18-trous.jpg)

{{</bloc>}}

{{<bloc>}}

Les enfants pourront tester le mini-golf rénové pendant que les parents participeront au nettoyage du tennis. Il nous faut, en effet, vérifier que toutes les pistes fonctionnent correctement et mesurer le temps de réalisation d'une partie complète de 18 trous pour pouvoir ajuster notre offre. Ce sera leur contribution ludique à l'effort citoyen.

{{</bloc>}}

{{</grid>}}

**Au plaisir de nous retrouver une dernière fois le 17 avril au matin !**