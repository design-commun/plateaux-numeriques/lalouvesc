+++
date = 2022-06-26T22:00:00Z
description = "Vendredi 1er juillet 18h Abri du pèlerin"
header = "/media/spectacle-ecole-st-joseph-2022-2.jpg"
icon = ""
subtitle = "Vendredi 1er juillet 18h Abri du pèlerin"
title = "Invitation au spectacle de fin d'année de l'Ecole St Joseph"
weight = 1

+++
![](/media/spectacle-ecole-st-joseph-2022.jpg)