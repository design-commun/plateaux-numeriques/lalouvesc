+++
date = 2022-12-14T23:00:00Z
description = "Adresse, horaires"
header = "/media/logo-emmaus-complet-1-768x768.png"
icon = ""
subtitle = ""
title = "Un nouvel Emmaüs à Davézieux"
weight = 1

+++
Emmaüs s’installe au 402 avenue de la République à Davézieux. Ouverture :

* mardi, jeudi et vendredi de 14h00 à 17h30 
* mercredi et samedi de 10h00 à 12h00 et 14h00 à 17h30 

  Accueil des dons tous les jours  

Tél : 04 75 67 32 26