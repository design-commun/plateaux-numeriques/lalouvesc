+++
date = 2021-03-19T23:00:00Z
description = "Journée nationale du souvenir et de recueillement à la mémoire des victimes civiles et militaires de la guerre d’Algérie et des combats en Tunisie et au Maroc"
header = "/media/19-mars-2021-monument-aux-morts.jpg"
icon = ""
subtitle = ""
title = "Dépôt d'une gerbe au monument aux morts le 19 mars"
weight = 1

+++
La journée nationale du souvenir et de recueillement à la mémoire des victimes civiles et militaires de la guerre d’Algérie et des combats en Tunisie et au Maroc a eu lieu, comme chaque année le 19 mars.

A Lalouvesc à 11 heures précises quatre anciens combattants de la guerre d'Algérie (Jean Poinard, Léopold Abrial,Joseph Alborghetti et Roger Roche), accompagnés par le père Olivier de Framond, Jacques Burriez et François Besset, Maire et premier adjoint de Lalouvesc, se sont retrouvés devant le monument aux morts.

Une gerbe a été déposée par Mr le Maire, suivie de la lecture du manifeste national. Puis le président Jean Poinard a lu le message de la FNACA. Une minute de silence a été respectée, puis un Notre Père a été récité par ceux qui le souhaitaient, et enfin la Marseillaise a été reprise en chœur.  
  
Une pensée toute particulière a été émise en début de cérémonie pour Rémi Deygas ancien combattant décédé récemment.

Pour savoir [pourquoi nous célébrons le 19 mars 1962](https://www.vie-publique.fr/questions-reponses/273915-guerre-dalgerie-quelle-celebration-du-19-mars-1962).