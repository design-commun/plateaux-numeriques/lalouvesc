+++
date = 2021-05-21T22:00:00Z
description = "Travaux de réfection de la chaussée entre le col du Marchand et Lalouvesc"
header = "/media/circulation-alternee.jpg"
icon = ""
subtitle = ""
title = "Circulation alternée entre le 7 et 18 juin sur la route de Saint Félicien"
weight = 1

+++
Comme annoncé dans le [Bulletin n°8](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/8-le-printemps-de-lalouvesc/#travaux-de-voirie), les travaux de réfection des routes autour du village démarrent. La première tranche concerne la partie entre la route de Pailharès et l'entrée du village.

Une circulation alternée sera mise en place du 7 au 18 juin. 

[Arrêté du Département](/media/077-adc-nb-21-rd0532-evtp-travaux-de-reprofilage-de-chaussee-satillieu-et-lalouvesc.pdf).