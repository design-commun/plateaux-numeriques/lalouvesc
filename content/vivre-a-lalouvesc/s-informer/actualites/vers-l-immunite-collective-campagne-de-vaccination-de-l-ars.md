+++
date = 2021-08-12T22:00:00Z
description = "100 personnes témoignent"
header = "/media/campagne-vaccination-ars-2021.jpg"
icon = ""
subtitle = ""
title = "Vers l'immunité collective, campagne de vaccination de l'ARS"
weight = 1

+++
L’Agence régionale de santé Auvergne-Rhône-Alpes en lien avec les 12 préfectures de département, lance **« #JeMeVaccine #JeJoueCollectif »,** une campagne de communication pour encourager toutes les personnes encore non vaccinées à franchir le pas et à jouer collectif.

Pour cela, chaque vaccination compte.

Avec le concours d’établissements hospitaliers volontaires, 100 personnes venues se faire vacciner ont accepté de participer à cette campagne en se faisant photographier.   
Enfants accompagnés de leurs parents, jeunes adultes, retraités, professionnels de santé, footballers amateurs ou professionnels, personnels d’établissements de santé ou de centres de vaccination… tous ont partagé la raison de leur démarche.

Chacun de ces visages sera visible sur Snapchat, Instagram et Facebook du 16 août au 26 septembre.

Par ailleurs, une campagne d'affichage est en cours.

![](/media/campagne-vaccination-ars-2021.PNG)

Pour ceux qui sont encore sceptiques, nous suivons régulièrement sur lalouvesc.fr l'évolution de la courbe des hospitalisations pour la Covid 19 en Ardèche. Ci-dessous, son dernier état, les hospitalisations sont reparties brutalement à la hausse. Aujourd'hui 85% des hospitalisés n'ont pas été vaccinés et leur âge moyen baisse. Il ne faut pas que la courbe de cette automne ressemble à celle de l'automne dernier. N'oublions pas que le village n'a pas été épargné.

![](/media/courbe-hospitalisations-13-08-2021.png)