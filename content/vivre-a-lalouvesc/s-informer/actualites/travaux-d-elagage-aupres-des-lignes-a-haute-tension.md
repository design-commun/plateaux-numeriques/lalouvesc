+++
date = 2021-06-10T22:00:00Z
description = "Les travaux commencent à partir du 14 juin"
header = "/media/enedis.jpg"
icon = ""
subtitle = ""
title = "Travaux d'élagage auprès des lignes à haute tension"
weight = 1

+++
Enedis nous informe que des travaux d’élagage et d’abattage d’arbres entrepris dans le cadre du plan d’entretien et maintenance des lignes électriques 20.000 volts, vont débuter sur le territoire de la commune à compter du 14 juin 2021.

Ces travaux ont été confiés par ENEDIS, à l’entreprise : G.R.D.E. – 38130 ECHIROLLES. tél : 04 76 29 07 59.