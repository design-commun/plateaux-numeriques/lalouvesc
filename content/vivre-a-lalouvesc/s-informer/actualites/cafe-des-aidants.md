+++
date = 2021-11-25T23:00:00Z
description = "Samedi 4 décembre à St Félicien"
header = "/media/admr-aide_seniors2.jpg"
icon = ""
subtitle = ""
title = "Café des aidants"
weight = 1

+++
Le prochain café des aidants de St Félicien aura lieu samedi 4 décembre de 10h à 11h30 à l'Auberge de St Félicien sur le thème « Relation aidant/aidé : sortir du devoir ».  
 Les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).  
Organisé par l'ADMR  
Renseignements auprès de Carole Guilloux : [cguilloux@fede07.admr.org](mailto:cguilloux@fede07.admr.org) ou 06 81 50 19 26