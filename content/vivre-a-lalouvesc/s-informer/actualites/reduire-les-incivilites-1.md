+++
date = 2022-02-22T23:00:00Z
description = "Ne mettez pas n'importe quoi dans les poubelles..."
header = "/media/dechets-sauvages-4.jpg"
icon = ""
subtitle = ""
title = "Réduire les incivilités"
weight = 1

+++
Les agents du service de collecte de ordures ménagères nous ont signalé plusieurs incivilités récentes à Lalouvesc. Ils ont trouvé :

* un bac rempli de bâches plastiques
* un radiateur électrique
* accessoires de salle de bain porcelaine + verre + CD + cafetière + mixeur + plastiques divers - etc.

Tout cela n'a pas été collecté et a été laissé sur place... merci aux responsables de récupérer les objets et de les amener à la déchetterie, comme ils en ont le devoir.

Il est important de respecter les consignes, pour maintenir un village propre et vivable pour tous et éviter des augmentations d'impôts induites par des charges de nettoyages supplémentaires. Si vous rencontrez des difficultés, venez en parler à la Mairie, nous trouverons ensemble une solution.