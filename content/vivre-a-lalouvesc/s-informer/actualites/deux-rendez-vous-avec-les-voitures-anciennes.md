+++
date = 2023-04-11T22:00:00Z
description = "Dimanche La Ronde Louvetonne, Jeudi suivant le Tour Auto"
header = "/media/ronde-louvetonne-2.jpg"
icon = ""
subtitle = "Dimanche La Ronde Louvetonne, Jeudi suivant le Tour Auto"
title = "Deux rendez-vous avec les voitures anciennes"
weight = 1

+++
Dimanche 16 avril, c'est bien sûr **La Ronde Louvetonne**, le lancement de la saison pour le village avec plus de cent voitures anciennes et un concert gratuit l'après-midi.

![](/media/ronde_louvetonne-affiche-concert_2023.jpg)

Et jeudi 20 avril vers 15h, le **Tour Auto 2023** passe par Lalouvesc. [Le Tour Auto](https://newsdanciennes.com/guide-complet-du-tour-auto-2023-le-parcours-les-horaires-et-les-engages/) est le principal rallye automobile des voitures anciennes qui réunit les voitures engagées dans le rallye Monte-Carlo depuis l'origine. Un spectacle exceptionnel pour les amoureux des vieilles machines.

[![](/media/tour-auto-2023.jpg)](https://newsdanciennes.com/guide-complet-du-tour-auto-2023-le-parcours-les-horaires-et-les-engages/)