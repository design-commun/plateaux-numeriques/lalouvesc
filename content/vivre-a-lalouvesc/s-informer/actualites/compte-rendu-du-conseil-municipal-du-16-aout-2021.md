+++
date = 2021-08-19T22:00:00Z
description = ""
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte rendu du Conseil municipal du 16 août 2021"
weight = 1

+++
Le compte rendu du dernier Conseil municipal est téléchargeable[ ici](/media/2021-6-conseil-municipal-lalouvesc.pdf)

Rappel: tous les comptes-rendus des Conseils depuis 2008 sont archivés sur[ cette page](/mairie-demarches/vie-municipale/comptes-rendus/comptes-rendus-des-conseils-municipaux/).