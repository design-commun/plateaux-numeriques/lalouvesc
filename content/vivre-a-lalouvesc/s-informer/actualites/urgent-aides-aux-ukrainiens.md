+++
date = 2022-03-24T23:00:00Z
description = "Lits, tables, chaises recherchés pour recevoir des familles à Lalouvesc"
header = "/media/ukraine-ardeche-2022.jpg"
icon = ""
subtitle = ""
title = "Urgent : aides aux Ukrainiens"
weight = 1

+++
Lalouvesc s’apprête à recevoir deux ou trois familles d'Ukrainiens. Des meubles sont nécessaires pour les recevoir décemment : 5 lits avec matelas en bon état, 2 grandes tables, 10 chaises.

Adressez-vous à Xavier 06 86 04 55 71 / 04 75 67 61 33

Merci d'avance pour votre aide !