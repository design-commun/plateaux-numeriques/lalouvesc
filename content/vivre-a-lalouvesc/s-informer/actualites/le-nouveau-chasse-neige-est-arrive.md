+++
date = 2021-11-24T23:00:00Z
description = "... juste à temps. On prévoit de la neige pour ce week end."
header = "/media/arrivee-chasse-neige-2021.jpg"
icon = ""
subtitle = ""
title = "Le nouveau chasse-neige est arrivé"
weight = 1

+++
La Commune a fait l'acquisition d'un tracteur pour remplacer l'ancien chasse-neige qui était mal adapté à ses besoins et avait mal vieilli dans son garage. L'avantage de celui-ci, acquis d'occasion et avec reprise de l'ancien, est qu'il est multifonction et pourra donc servir à des travaux divers (débroussaillage, terrassement, portage, remorquage, etc). Il sera donc utilisé toute l'année.

Mais l'urgence, c'est l'arrivée de l'hiver. Il a été reçu ce jeudi. Dominique Balaÿ va pouvoir le prendre en main. Il a déjà reçu une formation pour le déneigement. Il est temps, on annonce de la neige pour dimanche !