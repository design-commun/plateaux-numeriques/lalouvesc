+++
date = 2022-02-01T23:00:00Z
description = "Courrier des organisateurs"
header = "/media/ardechoise-6.JPG"
icon = ""
subtitle = ""
title = "Edition de l'Ardéchoise des Jeunes reportée en 2023"
weight = 1

+++
Extrait du courrier des organisateurs de l'Ardéchoise :

> Après une longue réflexion collégiale et avec beaucoup de regrets, il a été décidé à l’unanimité des membres du comité directeur, le report de cette épreuve tant appréciée par nos jeunes cyclistes.
>
> En effet, plusieurs raisons liées à la situation actuelle que nous connaissons tous, nous contraignent à devoir annuler cette édition 2022, qui devait se dérouler les mercredi 15 et jeudi 16 juin :
>
> * La planification des préparatifs est trop importante et indépendante de notre contrôle interne.
> * Des professeurs des écoles sont trop affairés à assurer la continuité éducative de leurs classes.
> * La difficulté de composer dans les délais impartis « Une belle Ardéchoise des Jeunes ».
> * Des ressources humaines insuffisantes à la préparation de cette animation.
> * Notre situation financière fragilisée par deux années d’interruption.
> * Le protocole sanitaire du mois de juin qui nous sera ordonné qu’au dernier moment.
>
> Nous sommes les premiers déçus de ne pas pouvoir accueillir tous ces jeunes et comprenons leur déception.