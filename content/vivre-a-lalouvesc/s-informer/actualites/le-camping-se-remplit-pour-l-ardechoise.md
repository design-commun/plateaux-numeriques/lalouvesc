+++
date = 2022-02-15T23:00:00Z
description = "Suggestion : réserver un refuge dans les arbres !"
header = "/media/refuge-des-afars-7.jpg"
icon = ""
subtitle = ""
title = "Le camping se remplit pour l'Ardéchoise"
weight = 1

+++
Presque tous les hébergements du camping sont réservés pour l'Ardéchoise, mi-juin. Mais il reste un bon plan : les refuges des Afars, des cabanes dans les arbres : 75 € la nuit pour deux, petits déjeuners compris.

Camping du Pré du Moulin  
07520 Lalouvesc  
Tél : 04 75 67 84 86  
camping.lalouvesc@orange.fr

**Réservations :** Par email & par téléphone ou directement en ligne.

<script type="text/javascript" src="//gadget.open-system.fr/widgets-libs/rel/noyau-1.0.min.js"></script>

<script type="text/javascript">

( function() {

    var widgetProduit = AllianceReseaux.Widget.Instance( "Produit", { idPanier:"saexsbE", idIntegration:1195, langue:"fr", ui:"OSCA-116693" } );
    
    widgetProduit.Initialise();

})();

</script>

<div id="widget-produit-OSCA-116693"></div>

**Attention, les réservations ne sont confirmées qu'une fois les arrhes encaissées.**