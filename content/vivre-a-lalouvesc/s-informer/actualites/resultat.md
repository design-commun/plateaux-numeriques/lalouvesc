+++
date = 2021-09-19T22:00:00Z
description = "Ils ont perdu, mais le coeur y était !"
header = "/media/foot-match-18-09-2021.jpg"
icon = ""
subtitle = ""
title = "Match de foot Pailhares/Lalouvesc contre Desaignes"
weight = 1

+++
Dimanche 19 septembre premier match, Pailhares/Lalouvesc contre Desaignes à 14H30 le coup de sifflet se faisait entendre.  
Nos footballeurs ont perdu 2-0 mais l’envie de se retrouver était plus forte que la défaite. La convivialité et le plaisir de se revoir pour échanger planaient sur le terrain.  
De nombreux spectateurs étaient présents pour les soutenir affrontant une météo pluvieuse.

![](/media/foot-match-18-09-2021-2.jpg)