+++
date = 2021-05-15T22:00:00Z
description = "Plateaux numériques a fait de lalouvesc.fr un démonstrateur pour la conception de sites responsables, adaptés aux enjeux de demain."
header = "/media/plateaux-numeriques-lalouvesc-210116.jpg"
icon = ""
subtitle = ""
title = "Lalouvesc.fr, site pilote des sites web durables et accessibles pour les villages d’aujourd’hui et de demain"
weight = 1

+++
Sur l'image, deux designers de Plateaux numériques en visite à Lalouvesc, décembre dernier.

[Plateaux numériques](http://plateaux-numeriques.fr/) est un service de création de sites web pour les mairies de village. L’objectif est d’accompagner les équipes municipales dans la création de sites web utiles, accessibles, abordables, ancrés dans le territoire et à faible impact environnemental.

Le site Lalouvesc.fr sert de démonstrateur pour la conception de ces sites responsables, adaptés aux enjeux de demain. Le service de Plateaux numériques est maintenant ouvert aux collectivités qui voudraient suivre son exemple.

Jacques Burriez, Maire de Lalouvesc : "c’est l’occasion de souligner qu’il ne se passe rarement une journée sans que l’on me fasse l’éloge de notre site internet."

L'ouverture du nouveau site de Lalouvesc, garanti sans cookies, a permis de multiplier par cinq sa fréquentation, si l'on compare le même mois d'une année sur l'autre. Les intéressés peuvent en suivre [les statistiques](https://simpleanalytics.com/lalouvesc.fr?period=day&count=1) en direct.