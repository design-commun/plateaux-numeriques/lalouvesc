+++
date = 2021-10-25T22:00:00Z
description = "Plus de 800 inscrits. N'oubliez pas votre passe sanitaire et votre costume d'Halloween. Quelques restrictions de circulation à cette occasion."
header = "/media/trail-2021-tee-shirt.jpeg"
icon = ""
subtitle = ""
title = "Trail, on affiche complet !"
weight = 1

+++
Ils nous ont manqué. De toute évidence nous leur avons manqué également !!!

Notre village, nos parcours forestiers, l’ambiance proposée pour cet évènement… LE TRAIL DES SAPINS est bel et bien un incontournable après seulement cinq ans d’existence.

C’est un peu plus de **800** coureurs et randonneurs qui s’aligneront sur la ligne de départ samedi.  
Après l’effort le réconfort : c’est autant de repas qui seront préparés par les commerçants du village et servis sous chapiteaux pour les réconforter. Moment fort de la soirée qui sonne… le clap de fin !

L’organisation bat son plein. Une centaine de bénévoles sont dans les _starting blocks_ pour faire de cette journée une nouvelle journée d’exception. Dame météo joue à nous faire trembler un peu quand même… Rendez-vous samedi.... en costume d'Halloween bien sûr !

**Il est rappelé qu'un contrôle du passe sanitaire sera effectué pour toutes personnes (à partir de 12 ans) voulant accéder au site du camping.**

Agnès, secrétaire du Comité des fêtes

### Règles de stationnement et de circulation entre samedi 15h et dimanche 3h

Le stationnement et la circulation seront interdits :

* de l'intersection de la place du Lac avec le chemin de l'Hermuzière jusqu'au café du Lac,
* sur toute la longueur du chemin de l'Hermuzière,
* parking du camping municipal,
* sur toute la longueur du Pré du Moulin,
* chemin des Crozes à partir du 32 rue des Cévennes (M. Abrial)
* route de Bobignieux (en dessous du CAC) jusqu'à l'intersection du chemin Grosjean.

La rue de la Fontaine et la rue du Chemin Neuf seront mises en double sens.