+++
date = 2022-06-07T22:00:00Z
description = "13 juin à 19h, tout le monde est invité"
header = "/media/cafe-du-lac-2022-2.jpg"
icon = ""
subtitle = ""
title = "Tombola d'Atout Val d'Ay et crémaillère au Café du Lac"
weight = 1

+++
_Le lundi 13 juin à 19h il y aura le tirage de la tombola organisée par Atout Val d'Ay qui se déroulera au Café du Lac. Par la même occasion j'en profite pour inaugurer enfin ma crêperie._

_Je vous y attend avec plaisir._ 

_Cordialement_

_Caroline Porras_