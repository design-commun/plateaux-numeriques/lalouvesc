+++
date = 2021-10-23T22:00:00Z
description = "Une dizaine d'adhérents ont visité le Musée d'art moderne et contemporain et des galleries et ateliers d'artistes à St Etienne"
header = "/media/sortie-1-carrefour-des-arts-2021-1.jpg"
icon = ""
subtitle = ""
title = "Premières visites du club du Carrefour des arts"
weight = 1

+++
Samedi 23 octobre a eu lieu la première sortie du club du Carrefour des arts. Au programme le matin, la visite guidée de deux expositions du Musée d'art moderne et contemporain de St Etienne :

* [_Eclosion_](https://mamc.saint-etienne.fr/fr/expositions/lionel-sabatte) de l'étonnant Lionel Sabatté,
* [_L'énigme autodidacte _](https://mamc.saint-etienne.fr/fr/expositions/lenigme-autodidacte)qui réunit plusieurs dizaine d'artistes du XXe et XX1e siècles aux parcours très divers.

L'après-midi a été consacrée à la visite de galeries, très nombreuses à St Etienne, parfois poursuivies par un atelier artiste.

Une dizaine d'adhérents du Carrefour des arts avaient répondu présents à l'invitation de Jacques Morel organisateur de cette sortie. Un dizaine de chanceux, ce fut un émerveillement !

![](/media/sortie-1-carrefour-des-arts-2021-2.jpg)

D'autres visites sont prévues dans les mois à venir, les covoiturages sont organisés. Si vous êtes intéressés et pas encore adhérents, il est encore temps de [prendre votre adhésion](http://www.carrefourdesarts-lalouvesc.com/contact.html), vous recevrez ainsi toutes les informations sur les programmes.