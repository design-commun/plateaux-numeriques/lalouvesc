+++
date = 2021-09-14T22:00:00Z
description = "Tout le programme"
header = "/media/fete-de-la-science-2021-2.jpg"
icon = ""
subtitle = "« Nos pupilles, reflets de nos émotions »"
title = "Fête de la science du 25 septembre au 9 octobre"
weight = 1

+++
Organisée par l'Office du tourisme du Val d'Ay

### Samedi 25 septembre

* 13h30 : Office de tourisme Lalouvesc : **Exposition enjeux des pelouses** et observation de la flore du Val d’Ay et de Lalouvesc
* 15h : **Le sol une merveille sous nos pieds,** un patrimoine à protéger, Maison Saint Régis avec Pierre Armand Roger

### Samedi 2 octobre

* 14h : **30 ans de relevés de pluviométrie à Lalouvesc** avec Météo France, Maison Saint Régis
* 15h : **Les Bactéries de l’extrême,** Maison Saint Régis avec Pierre Armand Roger

### Mercredi 6 octobre

* 14h : **Groucho clown exploratrice à la Forêt du Grandbeau** à Saint Alban d’Ay

### Samedi 9 octobre

* 15h : **Balade bucolique Satillieu Veyrines** avec Sandrine Defour (RDV Point info à 15h et covoiturage)

Des animations sont également prévues en écoles et collèges![](/media/fete-de-la-science-2021-1.jpg)

Possibilité de contact par mail : [contact@valday-ardeche.com](mailto:contact@valday-ardeche.com)  
SMS uniquement sur le mobile au 04 75 67 84 20 ou en cas d'absence 06 31 09 98 46