+++
date = 2021-10-06T22:00:00Z
description = "Les brioches seront vendues devant la Mairie de 8h30 à 12h"
header = "/media/adapei-brioches-2021.jpg"
icon = ""
subtitle = ""
title = "Opération Brioche le 17 octobre pour l'ADAPEI"
weight = 1

+++
Acheter une brioche, c’est se faire un petit plaisir, mais c’est surtout soutenir les actions concrètes et locales menées par l’Adapei 07.

A Lalouvesc, les bénévoles vous attendent le dimanche 17 octobre devant la mairie de 8 h30 à 12 h .  
 "Nous aider à gagner nos combats, c est simple comme une brioche".

Vous pourrez déguster votre bioche en admirant les voitures anciennes du [Team des Balcons](http://teamdesbalcons.com/index.php/2021/07/07/2eme-randonnee-automnale/) qui s'arrêteront à Lalouvesc ce même jour

## Qu'est-ce que l'Adapei ?

[L'Adapei 07](https://www.adapei07.fr/) accueille et accompagne tout au long de de la vie, des personnes en situation de handicap, dans ses établissements comme en milieu ordinaire. Elle agit pour que toute personne déficiente intellectuelle, avec ou sans troubles associés, dispose d'une solution d'accueil, d'accompagnement et qu'elle soit partie prenante d'une société inclusive et solidaire.  
L'Adapei 07 défend également les droits des familles pour que la survenue du handicap ne soit pas synonyme d'exclusion sociale.

Chaque année, en octobre, des bénévoles se mobilisent pour vendre des brioches au profit des personnes handicapées. Grâce à la générosité des habitants, des investissements peuvent être réalisés au profit des personnes prises en charge dans les structures de l'ADAPEI.

Cette année, les dons permettront de participer au financement des opérations suivantes:

* ROIFFIEUX: agrandissement de la blanchisserie, aménagement du service d’accueil de jour,
* EMPURANY: participation à l’isolation du bâtiment,
* LALEVADE: restructuration de la cuisine,
* LE TEIL: aménagement d’une salle de réunion/formation
* PRIVAS: aménagement de locaux.