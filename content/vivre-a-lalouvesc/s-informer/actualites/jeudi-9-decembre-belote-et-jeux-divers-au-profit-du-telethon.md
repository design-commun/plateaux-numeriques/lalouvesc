+++
date = 2021-12-01T23:00:00Z
description = "à 14h au CAC, organisé par le Club des deux clochers"
header = "/media/logo-club-2-clochers.jpg"
icon = ""
subtitle = ""
title = " Jeudi 9 décembre : belote et jeux divers  au profit du Téléthon"
weight = 1

+++
Le jeudi 9 décembre a 14h, Le Club des deux clochers organise au CAC : belote et jeux divers  **au profit du Téléthon**

VENEZ NOMBREUX !

Georges Ivanez, président du Club des deux clochers  
tel : 06 62004569

![](/media/club-des-2-clochers-telethon-2021.jpg)