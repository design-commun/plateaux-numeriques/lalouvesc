+++
date = 2022-06-13T22:00:00Z
description = "Le bassin versant du Doux passe en alerte renforcée et l'Ardèche est en vigilance orange canicule"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "Sécheresse : alerte renforcée... Canicule : vigilance"
weight = 1

+++
## Alerte renforcée pour la sécheresse

La préfecture vient de placer le bassin versant du Doux en vigilance renforcée sécheresse, soit le niveau 3 sur 4.

![](/media/secheresse-ardeche-juin-2022.jpg)

Les consignes suivantes de restriction des usages de l'eau sont applicables :

**Usage de l'eau domestique (particuliers et collectivités territoriales)**

* L'alimentation en eau et le prélèvement depuis des plans d'eau, des canaux d'agrément et béalières ne disposant pas de règlement d'eau sont interdits.
* L'arrosage des pelouses, ronds points, espaces verts publics et privés,jardins d'agrément est interdit, sauf pour les arbres et arbustes plantés en pleine terre depuis moins de deux ans, pour lesquels il est autorisé trois jours par semaine (lundi, mercredi et vendredi) entre 20 h et 9 h.
* L'arrosage des jardins potagers hors prélèvement dans canaux ou béalières, est autorisé trois jours par semaine (mercredi, vendredi et dimanche) et trois heures par jour (entre 19 h et 22 h).
* L'arrosage des espaces sportifs est autorisé deux jours par semaine (lundi et jeudi) et trois heures par jour (entre 19 h et 22 h).
* Le lavage des voitures est interdit hors des stations professionnelles recyclant l'eau et sauf pour les véhicules ayant une obligation réglementaire (véhicules sanitaires, alimentaires ou techniques) et pour les organismes liés à la  
  sécurité.
* Le premier remplissage des piscines d'un volume de plus de 1 m3 est interdit. Le remplissage complémentaire des piscines à usage public n'est autorisé qu'entre 22 h et 6 h.
* Le lavage à l'eau des voiries est interdit, sauf impératifs sanitaires et à l'exception des lavages effectués par des balayeuses laveuses automatiques.
* Les fontaines publiques en circuits ouverts doivent rester arrêtées.
* Les tests de capacité des hydrants et points d'eau incendie (PEI) sont interdits.

**Usages industriels**

Les ICPE appliquent les directives contenues dans leur arrêté d'autorisation, leur enregistrement ou leur déclaration pour les épisodes d'alerte renforcée. Les autres industries limitent leurs prélèvements aux besoins indispensables.

**Usages agricoles**

Des mesures particulières sont applicables. Se renseigner à la Mairie.

## Vigilance orange canicule sur le département

Lalouvesc à 1.000 m d'altitude devrait être moins touché. Voici néanmoins un rappel des bonnes attitudes à avoir :

![](/media/fortes-chaleurs-2022.jpg)