+++
date = 2022-06-06T22:00:00Z
description = "Beaucoup de monde à l'Ayclipse et dans les rues de Satilieu pour applaudir les fanfares"
header = "/media/fanfares-delires-2022-3.jpg"
icon = ""
subtitle = ""
title = "La Lyre Louvetonne aux 17ème Fanrares Délires "
weight = 1

+++
La Lyre a défilé avec les Muguets de Quintenas.

Toutes les informations sur l'histoire et les photos de la manifestation sur le [Facebook de l'Office du Tourisme](https://www.facebook.com/OTValDAy/posts/pfbid023RWd7RutuZtkGZKvkVtqGidpZaUXsijTwUo2rXMdSvF3MgZ61TwfPpYqXioGhXFnl).