+++
date = 2023-02-01T23:00:00Z
description = "Focus sur les ordures ménagères"
header = "/media/ccva.jpg"
icon = ""
subtitle = ""
title = "Lettre d'information n°1 de la CCVA"
weight = 1

+++
La Communauté de communes de Val d'Ay publie une lettre d'information trimestrielle, distribuée dans toutes les boîtes aux lettres. Pour ceux qui ne sont pas présents au village, voici une copie du premier numéro.

![](/media/val-d-ay-1-2023.jpg)

![](/media/val-d-ay-2-2023.jpg)