+++
date = 2022-05-10T22:00:00Z
description = "Du 25 au 29 mai à St Dizier"
header = "/media/azelle-stand-2022.jpg"
icon = ""
subtitle = ""
title = "L'Azelle à la Rencontre nationale des 2cv"
weight = 1

+++
Lalouvesc sera présent à la 27e Rencontre Nationale des 2CV clubs de France qui se tiendra du 25 au 29 mai 2022 à Saint-Dizier (52 – Haute-Marne, Grand Est).

Rappelons qu'Alain Le Bihan, installé dans le village, est le [seul constructeur](https://www.lalouvesc.fr/projets-avenir/opportunites/trois-aventures-a-suivre/) pouvant homologuer des coupés cabriolets 2cv.