+++
date = 2022-05-10T22:00:00Z
description = "Offert par Vival et la Boucherie de Jésus, devant la Mairie à 11h"
header = "/media/apero-vival-de-jesus-2022-2.jpg"
icon = ""
subtitle = ""
title = "Un apéro pour le plaisir, dimanche 22 mai !"
weight = 1

+++
Rendez-vous devant la Mairie à 11h dimanche 22 mai pour trinquer. Merci à Vival et à la Boucherie de Jésus !

S'il pleut, ce sera le dimanche suivant.

![](/media/apero-vival-de-jesus-2022-1.jpg)