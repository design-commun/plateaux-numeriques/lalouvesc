+++
date = 2022-07-01T22:00:00Z
description = "Spectacles et animations de la semaine"
header = "/media/carrefour-des-arts-2022-inauguration-4.jpg"
icon = ""
subtitle = "Spectacles et animations de la semaine"
title = "Que faire à Lalouvesc du 3 au 10 juillet ?"
weight = 1

+++
A part le farniente, les [**innombrables balades**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/), le [**vélotourisme**](https://www.lalouvesc.fr/decouvrir-bouger/activites/velotourisme/), le[ **cheval**](https://www.lalouvesc.fr/decouvrir-bouger/activites/centre-equestre/)... voici quelques suggestions :

### Dimanche 3 juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 14h30 à 19h visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 14h30 à 16h30 assister à la démonstration de **réalisation de mosaïques** au [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt) **_Qu’est ce qu’on a tous fait au bon dieu ?_** ( [bande annonce](https://youtu.be/UkXUfWFiiao) ) à l’Abri du Pèlerin

### Lundi 4 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Illusions perdues_** ( [bande annonce](https://youtu.be/KGQn_mWDWrA) ) à l’Abri du Pèlerin

### Mardi 5 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Mercredi 6 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 15h - 17h emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* 17h partager un **Apéro découverte** à l’Office du tourisme

### Jeudi 7 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Vendredi 8 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Samedi 9 juillet

* 9h30 - 11h30  emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Maison de retraite_** ( [bande annonce](https://youtu.be/bLFIhJUpmNk) ) à l’Abri du Pèlerin

### Dimanche 10 juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 20h30 aller au **Concert Let’s Goldman**, Parc des Pèlerins
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Mystère_** ( [bande annonce](https://youtu.be/nmfMMrijOzU) ) à 21h à l’Abri du Pèlerin