+++
date = 2022-04-22T22:00:00Z
description = "Poster et vidéo dans le hall de la mairie"
header = "/media/jeu-monument-rendu-etudiants-1.jpg"
icon = ""
subtitle = ""
title = "Jeu monument : découvrez le projet en allant voter !"
weight = 1

+++
Le résultat du concours d'idées sera présenté dimanche 24 avril dans le hall de la mairie.

Comme déjà indiqué, malheureusement les étudiants lauréats n'ont pu le présenter de vive voix, mais ils ont réalisé une vidéo qui sera projetée et des posters. Les élus pourront aussi répondre à vos questions et recueillir vos réactions.

Si vous ne votez pas à Lalouvesc, pas d'inquiétude, tout sera expliqué en détail dans le Bulletin d'information du mois de mai et vous pourrez aussi réagir.