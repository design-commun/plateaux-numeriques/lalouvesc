+++
date = 2021-03-31T22:00:00Z
description = "Pour des raisons sanitaires, le Conseil se tiendra à huis-clos. Ordre du jour...."
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 7 avril à 20h"
weight = 1

+++
Un conseil municipal est convoqué pour le 7 avril à 20h. Pour des raisons sanitaires, le Conseil se tiendra à huis-clos.

Ordre du jour :

COMMISSION FINANCES

a- Demande de subvention déneigement

b- Taux des taxes locales directes 2021

c- Tarif de l’eau et l’assainissement

COMMISSION GESTION

a- Permis de démolition

b- Assurance risque statutaire

COMITÉ VIE LOCALE

a- Convention OGEC

DIVERS