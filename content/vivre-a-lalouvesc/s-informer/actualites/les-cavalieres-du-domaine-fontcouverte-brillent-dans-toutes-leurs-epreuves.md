+++
date = 2021-11-15T23:00:00Z
description = "Nouvelle saison... nouveaux succès !"
header = "/media/fontcouverte-2021.jpg"
icon = ""
subtitle = ""
title = "Les cavalières du Domaine Fontcouverte brillent dans toutes leurs épreuves"
weight = 1

+++
C’est reparti pour une nouvelle saison de concours ! Dimanche 7 novembre 2021, le centre équestre Domaine Fontcouverte s’est illustré au concours de sauts d’obstacles organisé aux écuries O’Hara, à l’Hôpital-le-Grand (42). Cette manifestation a rassemblé 171 participants dont les quatre cavalières du Domaine Fontcouverte.

![](/media/fontcouverte-2021.jpg)

Marie Evrard, Maé Pappo et Louise Dumas montent sur la plus haute marche dans leur épreuve respective (Poney 4, Poney 3 et Club 4). Marie décroche également une troisième place dans le Club 4. Léna Pappo complète ce beau palmarès en se classant troisième dans les épreuves Club 3 et Poney 2.

Des succès rendus possibles par l’implication de la directrice du centre équestre, Françoise DUPRÉ, et de sa monitrice, Anne-Blandine ZAK.

Fr. Dupré