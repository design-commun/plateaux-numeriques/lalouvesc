+++
date = 2022-12-11T23:00:00Z
description = "De nombreux lots"
header = "/media/tombola-ecole-dec-2022-3.jpg"
icon = ""
subtitle = ""
title = "La tombola de l'école est lancée"
weight = 1

+++
L'équipe de l'APEL de l'école St Joseph lance sa grande tombola 2022-2023 !

Vous pouvez dès aujourd'hui y participer, chez nos commerçants : boulangerie, bar du Lac, Vival et pharmacie, et auprès des enfants de l'école.

A vos agendas : le tirage aura lieu le 18 février lors du concours de Belote de l'école.

D'avance merci pour votre contribution !

{{<grid>}}{{<bloc>}}

![](/media/tombola-ecole-dec-2022-2.jpg)*

{{</bloc>}}{{<bloc>}}

![](/media/tombola-ecole-dec-2022-1.jpg)

{{</bloc>}}{{</grid>}}