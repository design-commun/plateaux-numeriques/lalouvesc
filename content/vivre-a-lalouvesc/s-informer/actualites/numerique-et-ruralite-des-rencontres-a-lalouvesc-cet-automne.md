+++
date = 2022-07-10T22:00:00Z
description = "3-4 octobre : deux jours pour pointer les problèmes et évoquer des solutions"
header = "/media/affiche-rencontres-numerique-et-ruralite-2022-2.jpg"
icon = "📱"
subtitle = "3 - 4 octobre à Lalouvesc : deux jours pour pointer les problèmes et évoquer des solutions"
title = "Numérique et ruralité : des rencontres à Lalouvesc cet automne"
weight = 1

+++
Le collectif de designers Plateaux numériques et la Mairie de Lalouvesc, avec l'appui de la préfecture de l'Ardèche, se sont associés pour organiser des rencontres, tables rondes et ateliers, inviter des experts, des expérimentateurs, des témoins, pour débattre avec les élus, les professionnels du numérique et tous les intéressés, pointer les problèmes et évoquer les solutions posés par le numérique dans le monde rural.

Les crises sanitaires, environnementales et politiques, ont produit un renversement récent du rapport entre urbanité et ruralité : l’urbain était un modèle pour le rural sommé de s’industrialiser et de singer la ville dans son organisation et ses comportements ; et les urbains, touristes, venaient consommer de la campagne pendant les vacances et les fins de semaine.

Aujourd’hui le rural, qui entretient une relation de proximité à la nature et propose de plus en plus d'autres modèles de production (circuits courts, low techs...), devient une inspiration pour l'urbain. Ce renversement devrait trouver aussi sa traduction dans un raisonnement différent face aux développements des services numériques.

### [Présentation détaillée, programme et inscriptions](http://rencontres.plateaux-numeriques.fr/)

![](/media/affiche-rencontres-numerique-et-ruralite-2022.jpg)