+++
date = 2023-02-05T23:00:00Z
description = "n° hors-série - février 2023"
header = "/media/entete-logo-hiver.png"
icon = ""
subtitle = "n° hors-série - février 2023"
title = "Bulletin d'information : Spécial élections partielles"
weight = 1

+++
De toutes nos forces pour Lalouvesc ! Telle est la maxime que nous avons mis en œuvre dans la première partie du mandat. Pour cette seconde partie, nous souhaitons compléter l’équipe pour la renforcer encore et donner le maximum de chance de réussite à notre village.

#### [**Lire le Bulletin spécial**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/)

[Version imprimable](/media/bulletin-de-lalouvesc-hors-serie-special-elections-partielles-fevrier-2023.pdf)

### Sommaire

* [Pourquoi des élections partielles](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#pourquoi-des-élections-partielles)
  * [Rénover le village…](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#réparer-le-village)
  * [… le développer…](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#-le-développer)
  * [… et relever les défis à venir.](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#-et-relever-les-défis-à-venir)
* [Qui peut se présenter comme conseiller municipal ?](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#qui-peut-se-présenter-comme-conseiller-municipal-)
* [Portrait robot d’un(e) conseiller(e) municipal(e) idéal(e)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#portrait-robot-dune-conseillere-municipale-idéale)
  * [Qualités du conseiller idéal](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#qualités-du-conseiller-idéal)
  * [Compétences utiles](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/#compétences-utiles)