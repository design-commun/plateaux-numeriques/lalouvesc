+++
date = 2021-06-15T22:00:00Z
description = "Place de l'église à St Félicien le 26 juin, concert gratuit"
header = "/media/orchestre-symphonique-departemental-de-l-ardeche.jpg"
icon = ""
subtitle = ""
title = "L'orchestre philarmonique départemental de l'Ardèche en concert les 26 juin et 6 juillet "
weight = 1

+++
![](/media/osda-en-concert-les-26-juin-et-2-juillet.jpg)