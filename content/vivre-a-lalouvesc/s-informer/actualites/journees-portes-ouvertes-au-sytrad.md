+++
date = 2022-02-17T23:00:00Z
description = "Les 19 et 22 mars, sur inscription"
header = "/media/sytrad-2022-2.PNG"
icon = ""
subtitle = ""
title = "Journées portes ouvertes au SYTRAD"
weight = 1

+++
Le SYTRAD organise ses journées portes ouvertes sur le site de Portes-Lès-Valence les 19 et 22 mars 2022.

[Lien d’inscription](https://docs.google.com/forms/d/e/1FAIpQLSft3Vb2DYlM89eIa_Znf0guct17HIS3aUSFZ8vHc-qyVIExDg/viewform)

Il y a trois visites de 10h à 13h le samedi matin et 2 visites de 17h à 19h le mardi soir (avec possibilité de rajouter un créneau si besoin).

![](/media/sytrad-2022-1.jpg)