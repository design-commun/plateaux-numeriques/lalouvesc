+++
date = 2022-05-02T22:00:00Z
description = "Vous êtes tous et toutes attendu(e)s à partir de 9h au camping"
header = "/media/journee-citoyenne-lalouvesc-11-juillet-2020.jpg"
icon = ""
subtitle = ""
title = "Rendez-vous le 7 mai pour la journée citoyenne 2022"
weight = 1

+++
Débroussaillage et réparations au camping, nettoyage du mini-golf, fleurissement du village, débroussaillage de l'espace Beauséjour, fignolage des parcours... les idées et les souhaits ne manquent pas pour rendre Lalouvesc encore plus attractif. Bien des travaux ont déjà démarré, sous l'impulsion de citoyens engagés. Mais nous avons encore besoin de bras !

**Une journée citoyenne est organisée pour nous retrouver toutes et tous, petits et grands, le 7 mai à partir de 9h au camping**. Des équipes seront faites au fur et à mesure des arrivées.

Ce sera aussi l’occasion de fêter joyeusement tous ensemble le printemps, l’ouverture de la saison et de discuter de l’avenir du village ! La mairie offrira aux bénévoles le repas de midi.

Rappelez-vous, en 2020 et en 2021, les citoyens ont retroussé leurs manches et ils [ont fait des merveilles](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/des-journees-citoyennes-pour-le-camping/) !