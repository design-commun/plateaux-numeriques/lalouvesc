+++
date = 2021-07-05T22:00:00Z
description = "Conseils personnalisés sur les consommations et la remise gratuite de petit matériel économe"
header = "/media/slime-2021.jpg"
icon = ""
subtitle = ""
title = "SLIME 2021 : Des visites à domicile gratuites pour réduire vos factures d'énergie et d'eau"
weight = 1

+++
_Le département de l'Ardèche propose aux habitants des visites à domicile pour aider à réduire les factures d'eau et d'énergie à travers des conseils personnalisés sur les consommations et la remise gratuite de petit matériel économe (LED, programmateur, mousseur pour économiser l'eau...). Ce temps d'échange vous permettra de mieux comprendre vos consommations et apportera des pistes pour faire des économies. Vous pouvez bénéficier de ce dispositif si vous êtes sous les plafonds modestes, que vous soyez locataire ou propriétaire._

_Pour en savoir plus et pour vous inscrire, rendez-vous sur_ [**_slime.alec07.org_**](http://slime.alec07.org/)