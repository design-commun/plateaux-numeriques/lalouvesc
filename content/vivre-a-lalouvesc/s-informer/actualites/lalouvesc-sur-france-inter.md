+++
date = 2021-12-29T23:00:00Z
description = "Le concours pour le jeu-monument présenté dans l'émission \"Carnets de campagne\""
header = "/media/journee-citoyenne.jpg"
icon = ""
subtitle = ""
title = "Lalouvesc sur France-Inter !"
weight = 1

+++
Vous pouvez réécouter l'[émission sur le site de France-inter](https://www.franceinter.fr/emissions/carnets-de-campagne/carnets-de-campagne-du-jeudi-30-decembre-2021).

Merci à Philippe Bertrand pour ce relai !