+++
date = 2021-05-17T22:00:00Z
description = "Tableau explicatif des vaccinations"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Qui peut se faire vacciner ?"
weight = 1

+++
La préfecture a fait parvenir ce tableau récapitulatif des possibilités de vaccination en fonction de l'âge.

![](/media/vaccinations-mai-2021.jpg)

Pour faciliter la vaccination des personnes qui ne sont pas en capacité de se déplacer, l’Assurance maladie prend en charge à 100 % le transport des patients sur prescription médicale. Le transport doit concerner un trajet vers le centre de vaccination le plus proche du lieu de prise en charge du patient.

![](/media/evolution-de-la-vaccination-mai-2021.png)

Source Santé Publique France