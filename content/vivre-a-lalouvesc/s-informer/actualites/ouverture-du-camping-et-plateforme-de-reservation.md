+++
date = 2021-05-06T22:00:00Z
description = "Ouverture le 12 mai, réservations en ligne disponibles..."
header = "/media/camping-chalets-5.png"
icon = ""
subtitle = ""
title = "Ouverture du camping et plateforme de réservation"
weight = 1

+++
Le camping du Pré du Moulin rénové ouvre ses portes le 12 mai. Bienvenus à tous les amis campeurs de Lalouvesc !

La plateforme de réservation en ligne est maintenant opérationnelle. Il n'est pas encore possible de payer directement en ligne, mais c'est pour bientôt.

Les nouveaux hébergements de 2021 (refuges, cottage PMR et tente lodge) et les deux mobil-homes qui ont été déplacés en haut du terrain ne sont ouverts à la location qu'à partir de début juin, dès maintenant les réservations sont possibles.

Si vous êtes intéressés, ne tardez pas, les créneaux disponibles se réduisent vite...

Tout est présenté sur la [page du camping](/decouvrir-bouger/hebergement-restauration/camping/).