+++
date = 2021-10-14T22:00:00Z
description = "La reconnaissance des parcours est faite, les inscriptions... et les décorations se poursuivent."
header = "/media/trail-reconnaissance-2021-4.jpg"
icon = ""
subtitle = ""
title = "Plus qu'une semaine pour s'inscrire au Trail des Sapins !"
weight = 1

+++
Déjà plus de 470 inscrits au Trail des Sapins ! Clôture des inscriptions le 25 octobre. **Attention, aucune inscription ne sera prise sur place**.

La reconnaissance du parcours des 12 km a été réalisée par les trailers locaux...

{{<grid>}}{{<bloc>}}  
![](/media/trail-reconnaissance-2021-1.jpg){{</bloc>}}{{<bloc>}}

![](/media/trail-reconnaissance-2021-2.jpg)

{{</bloc>}} {{</grid>}}

... et le Comité des fêtes peaufine les décorations d'Halloween.

{{<grid>}}{{<bloc>}}

![](/media/trail-deco-2021-2.jpg)

![](/media/trail-deco-2021-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/trail-deco-2021-3.jpg)

![](/media/trail-deco-2021-6.jpg)

{{</bloc>}} {{</grid>}}

Ce sera une belle fête !