+++
date = 2021-11-03T23:00:00Z
description = ""
header = "/media/repas-secretaires-2021.JPG"
icon = ""
subtitle = ""
title = "Repas annuel des secrétaires des mairies de la CCVA"
weight = 1

+++
Comme toutes les années, les secrétaires de Mairie de la communauté de communes ainsi que le personnel de la CCVA se retrouvent au restaurant. Nous choisissons à chaque fois un lieu différent sur la communauté de communes. Nous le faisons d'ordinaire avant les vacances d'été mais la pandémie et le passe sanitaire ont perturbés les habitudes. Un moment convivial et sympathique ou nous parlons un peu du travail mais pas que....  
Françoise ARENZ-FAURIE