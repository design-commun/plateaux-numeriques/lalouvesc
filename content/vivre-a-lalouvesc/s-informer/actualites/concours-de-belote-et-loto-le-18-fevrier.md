+++
date = 2023-02-06T23:00:00Z
description = "Organisé par l'APEL au CAC"
header = "/media/belote-apel-2023-2.jpg"
icon = ""
subtitle = "Organisé par l'APEL au CAC"
title = "Concours de Belote et loto le 18 février"
weight = 1

+++
Bonjour, le concours de belote annuel, organisé par l'apel au profit des enfants de l'école, aura lieu samedi 18 février au CAC.

Nous vous attendons nombreux !  
Information complémentaire : le tirage de la tombola aura lieu ce jour là !

![](/media/belote-apel-2023.jpg)