+++
date = 2021-08-12T22:00:00Z
description = "Tous les jours d'été, il se passe quelque chose à Lalouvesc"
header = "/media/copie-de-20210703_112053.jpg"
icon = ""
subtitle = ""
title = "Spectacles et animations de la semaine du 16 au 22 août "
weight = 1

+++
En plus des [nombreuses activités](/decouvrir-bouger/activites/) possibles, voici les spectacles et manifestations proposés à Lalouvesc du 16 au 22 août.

## Tous les jours

* 14h30 à 18h30 **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal
* 10h à 12h et de 15h à 18h **Exposition _La matière habitée_**  sculptures à la chapelle Saint Ignace

## 16 août

* à partir de 14h30 **Réalisation en public et en musique d’une œuvre** par la peintre Martine Jaquemet ([Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/)) à coté du CAC
* 21h cinéma, **Un triomphe** (avant-première) [bande annonce](https://youtu.be/N_q15dNbWtA) Abri du pèlerin

## 17 août

* 9h **Gym**, au camping
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 18h : visites estivales **Paroles d'habitants**, départ à l'Office du tourisme
* 20h30 concert à la Basilique **autour des peintures de Martine Jaquemet** ([Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/))

## 18 août

* 15h-17h empruntez des livres à la **bibliothèque**
* 17h **Apéro découverte** devant l'Office du tourisme
* 21h cinéma, **Les 2 Alfred**, [bande annonce](https://youtu.be/ZzGhk632cYg) Abri du pèlerin

## 19 août

* 9h **Gym**, au camping
* 13h30 **Animation enfants**, au camping (sur inscription)
* 21h cinéma, **OSS 117 : alerte rouge en Afrique noire**, [bande annonce](https://youtu.be/I0aviu-FqNo)  Abri du pèlerin

## 21 août

* 9h-12h **Partie de pêche** pour les enfants au camping
* 9h30-11h30 empruntez des livres à la **bibliothèque**
* 20h30 concert **Méditations musicales** Henri Pourtau (orgue) et Raphaël Yacoub (chantre) à la Basilique
* 21h cinéma, **Les 2 Alfred**, [bande annonce](https://youtu.be/ZzGhk632cYg) Abri du pèlerin

## 22 août

* 21h : cinéma, **OSS 117 : alerte rouge en Afrique noire**, [bande annonce](https://youtu.be/I0aviu-FqNo)  Abri du pèlerin