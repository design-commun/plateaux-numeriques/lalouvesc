+++
date = 2021-08-15T22:00:00Z
description = "Lalouvesc a accueilli plus de 1500 personnes pour la fête de l'Assomption"
header = "/media/15-aout-2021-3.jpg"
icon = ""
subtitle = ""
title = "Un 15 août réussi sous le soleil et dans la sérénité"
weight = 1

+++
Tandis que plus de 1500 fidèles assistaient à la messe au Parc des Pèlerins, le village a confirmé la pertinence du dispositif mis en place l'année dernière : rues dégagées, laissées largement aux piétons, ralentissement et distribution de plans à toutes les entrées, parkings réservés. Merci à tous les bénévoles qui ont donné de leur temps pour rendre cette organisation possible.

{{<grid>}} {{<bloc>}}

![](/media/15-aout-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/15-aout-2021-2.jpg)

{{</bloc>}} {{</grid>}}

Cette année la gendarmerie aussi était très présente. Benoît Terrier, colonel de gendarmerie, nouveau responsable pour tout le département de l'Ardèche, a profité de la manifestation pour visiter le village. Nous avons ainsi pu faire connaissance.