+++
date = 2022-10-17T22:00:00Z
description = "Samedi 5 novembre"
header = ""
icon = ""
subtitle = ""
title = "Fermeture exceptionnelle de la déchetterie"
weight = 1

+++
**FERMETURE EXCEPTIONNELLE DE LA DÉCHETTERIE DU VAL D’AY SAMEDI 5 NOVEMBRE 2022**

**(réouverture lundi 7 novembre)**

**Merci de votre compréhension**

**Communauté de communes du Val d'Ay - « Espace Jaloine » - 380 route de Jaloine - 07290 SAINT-ROMAIN-D’AY**

 **04 75 34 91 83 - secretariat@val-d-ay.fr**