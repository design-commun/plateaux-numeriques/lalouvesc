+++
date = 2022-06-18T22:00:00Z
description = "60% de participation, résultat tranché à Lalouvesc"
header = "/media/elections.jpg"
icon = ""
subtitle = ""
title = "Résultats du second tour des élections législatives à Lalouvesc et partout"
weight = 1

+++
Le second tour des législatives s'est tenu dimanche 19 juin, ci-dessous les résultats.

**Olivier Dussopt réélu dans la 2e circonscription de l'Ardèche avec 58,86 % des voix.**

Rappel des [résultats du premier tour.](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/resultats-du-premier-tour-des-elections-legislatives/)

## Résultats du second tour des législatives de 2022

### Lalouvesc

| Liste des candidats | Nuances | Voix | % Inscrits | % Exprimés |
| --- | --- | --- | --- | --- |
| M. Christophe GOULOUZELLE | NUP | 59 | 17,20 | 28,64 |
| M. Olivier DUSSOPT | ENS | 122 | 35,57 | 59,22 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 343 |  |  |
| Abstentions | 137 | 39,94 |  |
| Votants | 206 | 60,06 |  |
| Blancs | 20 | 5,83 | 9,70 |
| Nuls | 5 | 1,46 | 2,43 |
| Exprimés | 181 | 52,77 | 87,86 |

### 2e circonscription d’Ardèche

Le ministre du travail Olivier Dussopt (Ensemble) remporte ce second tour des élections législative avec 58,86 % des suffrages sur la 2ème circonscription de l'Ardèche. Christophe Goulouzelle pour la Nupes a récolté 41,14 % des voix.

## Rappel des résultats du second tour des législatives de 2017

### Lalouvesc

| Liste des candidats | Nuances | Voix | % Inscrits | % Exprimés |
| --- | --- | --- | --- | --- |
| Mme Laurette GOUYET-POMMARET | REM | 100 | 29,76 | 62,11 |
| M. Olivier DUSSOPT | SOC | 61 | 18,15 | 37,89 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 336 |  |  |
| Abstentions | 150 | 44,64 |  |
| Votants | 186 | 55,36 |  |
| Blancs | 16 | 4,76 | 8,60 |
| Nuls | 9 | 2,68 | 4,84 |
| Exprimés | 161 | 47,92 | 86,56 |

### 2e circonscription d’Ardèche

| Liste des candidats | Nuances | Voix | % Inscrits | % Exprimés | Elu(e) |
| --- | --- | --- | --- | --- | --- |
| M. Olivier DUSSOPT | SOC | 22 132 | 23,78 | 56,30 | Oui |
| Mme Laurette GOUYET-POMMARET | REM | 17 179 | 18,46 | 43,70 | Non |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 93 064 |  |  |
| Abstentions | 49 718 | 53,42 |  |
| Votants | 43 346 | 46,58 |  |
| Blancs | 2 839 | 3,05 | 6,55 |
| Nuls | 1 196 | 1,29 | 2,76 |
| Exprimés | 39 311 | 42,24 | 90,69 |

## Tous les résultats des législatives

Retrouvez tous les résultats de toutes les élections législatives de tous les départements sur le [site du Ministère de l’intérieur](https://www.interieur.gouv.fr/Elections/Les-resultats/Legislatives) .