+++
date = 2021-12-03T23:00:00Z
description = "Réservation, présentation et historique"
header = "/media/site-abri-du-pelerin-2021.jpg"
icon = ""
subtitle = ""
title = "Un nouveau site web pour l'Abri du pèlerin"
weight = 1

+++
Désormais l'Abri du pèlerin a [son site web](http://www.abridupelerin-lalouvesc.fr/), superbe !, réalisé par le frère Yves Stoesel.

Vous y trouverez son historique avec l'histoire de son fondateur [Pierre Bonnard](http://www.abridupelerin-lalouvesc.fr/index.php/historique/), des informations sur le village et sur le sanctuaire et, bien sûr, les randonneurs les pèlerins, les familles pourront [réserver en ligne](http://www.abridupelerin-lalouvesc.fr/index.php/reservations/) une chambre (12 ou 16 € par nuit) ou un lit dans le dortoir (9 € dans le dortoir) du 1er mai au 31 octobre.

Bravo pour cette réalisation qui enrichit la présence du village sur le numérique !