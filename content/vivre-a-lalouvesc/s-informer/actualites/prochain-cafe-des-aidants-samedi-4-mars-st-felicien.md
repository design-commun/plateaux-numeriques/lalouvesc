+++
date = 2023-02-28T23:00:00Z
description = "Voir aussi le programme de l'année"
header = "/media/cafe-des-aidants-2022-3.jpg"
icon = ""
subtitle = ""
title = "Prochain café des aidants samedi 4 mars St Félicien"
weight = 1

+++
Le prochain café des aidants de St Félicien aura lieu samedi 4 mars 2023 de 10h à 11h30 à l'Auberge de St Félicien sur le thème « Positionnement avec les professionnels, trouver ma place »

Vous accompagnez un proche malade, en situation de handicap ou dépendant du fait de l'âge ? Autour d'un café, venez échanger votre expérience avec d'autres aidants.  
Animés par des professionnels, les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).

Renseignements auprès de Carole Guilloux : [cguilloux@fede07.admr.org](mailto:cguilloux@fede07.admr.org) ou 06 81 50 19 26

### Programme de l'année

![](/media/cafe-des-aidants-programme-2023.jpg)