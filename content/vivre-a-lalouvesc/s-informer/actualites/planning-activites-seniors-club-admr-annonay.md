+++
date = 2023-04-13T22:00:00Z
description = "Avril - juillet 2023, transport gratuit possible"
header = "/media/senior-admr-avril-2023-2.jpg"
icon = ""
subtitle = ""
title = "Planning activités Seniors, Club ADMR Annonay"
weight = 1

+++
Au programme : 

* Activité Physique Adaptée
* Atelier mémoire
* Atelier d’expression artistique
* Atelier Nature et Bien-être
* Danse avec la Compagnie La Baraka
* Après-midi intergénérationnels
  

Renseignements et inscriptions au 06 81 50 19 26 - Transport aux activités possible gratuitement - Ouvert à tous

![](/media/senior-admr-avril-2023.jpg)