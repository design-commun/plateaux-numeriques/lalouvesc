+++
date = 2021-05-27T22:00:00Z
description = "Pour des raisons sanitaires, le Conseil se tiendra à huis-clos. Ordre du jour...."
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 7 juin à 20h"
weight = 1

+++
Un conseil municipal est convoqué pour le 7 juin à 20h. Pour des raisons sanitaires, le Conseil se tiendra à huis-clos.

Ordre du jour :

1. VIE MUNICIPALE
2. COMMISSION FINANCES

   a - Fusion des budgets  
   b - Décision modificative n°1  
   c - Fonds de solidarité crise COVID  
   d - Subvention exceptionnelle OGEC
3. COMMISSION GESTION  
   a - Avenant au contrat de travail de Sylvie Deygas  
   b - Création de deux postes de travail pour le camping  
   c - Constat de désaffection et déclassement d'un terrain en vue de sa vente  
   d - Suivi de l'appel d'offres pour la démolition de l'hôtel Beauséjour  
   e - Délégation de la maitrise d’ouvrage des travaux de voirie communale à la CCVA
4. COMITE VIE LOCALE
5. COMITE DEVELOPPEMENT
6. DIVERS