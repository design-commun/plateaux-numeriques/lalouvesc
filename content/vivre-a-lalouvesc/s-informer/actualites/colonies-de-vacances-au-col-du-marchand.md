+++
date = 2023-01-24T23:00:00Z
description = "Cinq places disponibles"
header = "/media/colonie-col-du-marchand-2023.jpg"
icon = ""
subtitle = ""
title = "Colonie de vacances au col du Marchand"
weight = 1

+++
> Nous organisons, comme tous les ans, une colonie de vacances cette année du samedi 8 juillet au dimanche 16 juillet pour des enfants de 6 à 13 ans, sous tente au col du Marchand.  
>    
>  Nous nous adressons habituellement à des enfants de la région lyonnaise que nous suivons durant l’année.  
>    
> Nous ne sommes pas à la recherche de publicité (car souvent complets), mais souhaitons cette année pouvoir proposer à des enfants (5 pour le moment) de la région ardéchoise de venir passer ces quelques jours avec nous.  
>    
>  Nous avons entendu dire que des parents rencontraient des difficultés pour inscrire leurs enfants en centre aéré et colonie dans les environs (dues aux crises de recrutement dans l’animation).  
>    
> C’est une façon de rendre un peu à cette belle région tout le bonheur qu’elle nous donne !  
>    
>  Si vous rencontrez des familles en difficultés pour cette période là, n'hésitez pas à leur communiquer nos coordonnées.  
>    
>  Vous pouvez visiter la page web de notre association   
>  [https://uncoeurpourbron.fr/](https://uncoeurpourbron.fr/ "https://uncoeurpourbron.fr/") (qui est presque à jour !). Nous sommes à votre disposition pour en savoir plus (rencontre, téléphone …).  
>    
>  Frédéric pour l'association un cœur pour Bron  
>  Tel : 06 74 111 850