+++
date = 2022-01-30T23:00:00Z
description = "19 - février 2022 - Voir plus loin"
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "19 - février 2022 - Voir plus loin"
title = "Le Bulletin n° 19 est paru"
weight = 1

+++
Avec la démolition de l'hôtel Beauséjour de nouvelles perspectives, au sens propre comme au figuré, se sont ouvertes pour le village. L'hôtel Beauséjour, son histoire et l'avenir de la place qu'il laisse, occupent une bonne part de ce Bulletin. Mais on y parle aussi de l'adressage, des prochaines journées citoyennes, de la Vie Tara, du dernier épisode du feuilleton des enfants... et, comme toujours, de bien d'autres choses encore.

[Lire le Bulletin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/)

## Entrée directe par le sommaire

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#mot-du-maire)
* [Actualités de la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#actualités-de-la-mairie)
  * [L’adressage est bouclé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#ladressage-est-bouclé)
  * [Préparation des journées citoyennes 2022 : réparer les parcours thématiques](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#préparation-des-journées-citoyennes-2022--réparer-les-parcours-thématiques)
  * [Lancement du chantier de l’écolotissement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#lancement-du-chantier-de-lécolotissement)
  * [Enquête sur l’information locale](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#enquête-sur-linformation-locale)
* [Zoom](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#zoom)
  * [Disparition d’un hôtel de légende](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#disparition-dun-hôtel-de-légende)
  * [Beauséjour : fin de partie et nouvelle donne](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#beauséjour--fin-de-partie-et-nouvelle-donne)
* [Suivi](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#suivi)
  * [La Vie Tara change de mains](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#la-vie-tara-change-de-mains)
  * [Alphonse Abrial nous a quittés](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#alphonse-abrial-nous-a-quittés)
  * [Spectacle des enfants à la basilique](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#spectacle-des-enfants-à-la-basilique)
  * [La Parentibulle](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#la-parentibulle)
  * [Café des aidants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#café-des-aidants)
  * [Passer son BAFA et trouver un job d’été](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#passer-son-bafa-et-trouver-un-job-dété)
  * [Prévention du suicide](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#prévention-du-suicide)
  * [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#dans-lactualité-le-mois-dernier)
* [Feuilleton : _Les quatre éléments_](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#feuilleton--_les-quatre-éléments_)
  * [Résumé des épisodes précédents](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#résumé-des-épisodes-précédents)
  * [Chapitre 5 : Le temple du vent](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/19-voir-plus-loin/#chapitre-5--le-temple-du-vent)