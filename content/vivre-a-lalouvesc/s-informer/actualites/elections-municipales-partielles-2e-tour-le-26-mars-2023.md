+++
date = 2023-03-21T23:00:00Z
description = "Liste des quatre candidats"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Elections municipales partielles : 2e tour le 26 mars 2023"
weight = 1

+++
Les candidats retenus par la Préfecture pour le second tour des élections municipales partielles sont :

* M. Gérard GUIRONNET,
* Mme Nicole PORTE,
* M. Corentin SERAYET,
* Mme Christine TREBUCHET.

Il se tiendra en Mairie le 26 mars 2023 de 8h à 18h.

Lire l'[arrêté préfectoral](/media/ap_07_2023_03_22_00001_lalouvesc.pdf).