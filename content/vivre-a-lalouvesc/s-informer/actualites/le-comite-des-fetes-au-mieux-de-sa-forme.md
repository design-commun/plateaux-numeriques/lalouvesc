+++
date = 2023-02-11T23:00:00Z
description = "Des résultats impressionnants"
header = "/media/ag-comite-des-fetes-2023-1.jpg"
icon = ""
subtitle = "Des résultats impressionnants"
title = "Le Comité des fêtes au mieux de sa forme"
weight = 1

+++
Le comité des fêtes a tenu son assemblée générale annuelle vendredi 10 février au CAC devant une cinquantaine d'adhérents. Le bilan de l'année 2022 et les perspectives 2023 ont été présentés par la présidente, Nathalie Desgrand Fourezon, et la trésorière, Martine Deygas Poinard, appuyées par l'ensemble du bureau.

En 2022 ont été organisés : une Ronde louvetonne (sous la neige...), deux concerts qui ont réuni chacun 4 à 500 spectateurs, des décorations originales et appréciées tout l'été pour l'Ardéchoise et un accueil au top pour les coureurs, un record de 270 exposants pour la brocante qui a fait monter près de 5.000 badauds sur le village, un autre record pour le Trail avec 1.200 coureurs et enfin un village décoré pour les fêtes de fin d'année.

Côté finances, les dépenses ont atteint la somme de 68 k€, heureusement compensées par des recettes qui laissent un résultat net de près de 8.000 € à l'association. Cette somme lui permet d'investir dans du matériel : chapiteau, friteuse, sono..., matériel qui est aussi prêté aux autres associations du village selon les besoins.

L'ampleur de la réussite de l'année impressionne d'autant qu'elle a été obtenue dans la bonne humeur, la décontraction, le calme et la facilité apparente, mais sans doute trompeuse : il en a fallu de l'énergie et de l'astuce aux bénévoles mobilisés pour atteindre ce résultat !

Ainsi l'activité de l'association profite à tous : à la population heureuse des animations proposées, aux autres associations, aux commerçants qui voient les visiteurs affluer et à l'image du village, vivant et accueillant. Tout cela dans le plaisir et la bonne humeur, c'est gagnant-gagnant !

Pour l'année qui s'ouvre, nous retrouvons les mêmes manifestations avec deux anniversaires à noter : la participation au centenaire de l'Abri du pèlerin les 8 et 9 juillet par un spectacle d'une humoriste et la brocante qui fêtera ses 20 ans, avec les démonstrations d'un forgeron, de dentelières et une grande tombola pour les exposants.

**Bravo et longue vie au Comité des fêtes de Lalouvesc !**