+++
date = 2021-07-17T22:00:00Z
description = "Jeudi 22 juillet 10h, sur inscription"
header = "/media/botanique-2021.JPG"
icon = ""
subtitle = ""
title = "Balade à la découverte des plantes comestibles et de leurs usages culinaires"
weight = 1

+++
Dans le cadre des actions du sanctuaire en partenariat avec l'office de Tourisme, est organisée **ce jeudi 22 juillet à 10h une balade à la découverte des plantes comestibles et de leurs usages culinaires,** animée par Françoise Kunstmann passionnée des plantes et des fleurs de la cueillette à l'assiette.

RDV devant la maison St Régis à proximité immédiate de l'office de tourisme.

Sur réservation : 04 75 67 84 20 [contact@valday-ardeche.com](mailto:contact@valday-ardeche.com)