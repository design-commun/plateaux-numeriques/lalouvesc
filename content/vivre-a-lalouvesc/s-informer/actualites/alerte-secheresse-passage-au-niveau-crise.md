+++
date = 2022-07-26T22:00:00Z
description = "Nombreuses restrictions, risque d'amendes"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "Alerte sécheresse : passage au niveau crise !"
weight = 1

+++
En raison de la sécheresse nous sommes passés au niveau crise, le niveau 4, plus haut niveau d'alerte. Il convient de bien respecter des mesures de prudence concernant la consommation d'eau rappelées dans l'image ci-dessous.

![](/media/crise-eau-2022.jpg)