+++
date = 2021-09-07T22:00:00Z
description = "Succès, convivialité et météo favorable pour une belle brocante dans un village gaulois"
header = "/media/brocntae-2021-1.jpg"
icon = ""
subtitle = ""
title = "Bref retour sur la belle brocante 2021"
weight = 1

+++
Accueillis par des bénévoles à peine réveillés les premiers exposants sont arrivés samedi matin à 04h00 dans un village gaulois encore tout endormi !

La pression est montée d’un cran au fil des minutes. Jusqu’à 08h00, « à gauche », « tout droit », « dans le pré », « à l’aire de jeux » ont été les injonctions entendues à l’entrée du site afin que tout le monde trouve rapidement son emplacement.

Succès, convivialité et météo ont été les trois ingrédients bien présents dimanche dernier pour faire de cette journée une belle brocante !

![](/media/brocntae-2021-2.jpg)

**Adaptation** est aussi le mot sur lequel à présent il faut réfléchir et agir. La situation sanitaire impose une nouvelle organisation de nos évènements. Alors réfléchissons, agissons et adaptons-nous en conséquence afin de conserver le maintien de ces journées qui donnent à Lalouvesc un air de fête dont nous ne nous lassons pas.

Pas de répit pour les bénévoles du Comité des Fêtes qui déjà travaillent sur l’organisation du TRAIL DES SAPINS !

Le Comité des Fêtes est toujours joignable soit:

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.