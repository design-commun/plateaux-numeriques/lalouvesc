+++
date = 2022-07-02T22:00:00Z
description = "Les enfants sont sur scène, la maîtresse dit au revoir"
header = "/media/spectacle-ecole-2022-3.jpg"
icon = ""
subtitle = "Les enfants sont sur scène, la maîtresse dit au revoir"
title = "Ecole : un spectacle de fin d'année plein d'émotions"
weight = 1

+++
> Que d'émotions en cette soirée, des larmes des enfants à celles d'Amandine, qui a fait un discours expliquant son départ pour un poste d'enseignante à la réunion, un projet de famille qui se concrétise ! Elle a aussi évoqué ses difficultés, son arrivée en juin 2019 à Lalouvesc, (au décès de son papa), les tâches invisibles et pourtant chronophages relevant des missions de directrice, ses doutes, des désespoirs, son désarroi, même si elle reconnait ne rien avoir lâché. Le COVID ne lui a certes pas facilité la tâche.
>
> Elle a aussi évoqué ses joies, le plaisir d'avoir apporté aux enfants un mode d'enseignement, que sa remplaçante, Sacha Haon, a dit vouloir poursuivre.
>
> Amandine a remercié ses collègues pour le travail avec l'équipe pédagogique et d'autres interlocuteurs louvetous

Séverine Moulin

![](/media/spectacle-ecole-2022-4.jpg)