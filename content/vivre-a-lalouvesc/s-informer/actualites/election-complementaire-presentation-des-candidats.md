+++
date = 2023-03-10T23:00:00Z
description = "Panneau de la campagne électorale"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Election complémentaire : présentation des candidats"
weight = 1

+++
Présentation des candidatures, affichée sur le panneau de la campagne électorale.

![](/media/affiche-candidats-election-mars-2023.jpg)