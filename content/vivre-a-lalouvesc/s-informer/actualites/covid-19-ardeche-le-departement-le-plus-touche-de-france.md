+++
date = 2021-11-22T23:00:00Z
description = "Lettre du préfet, et courbe alarmante. Appliquez et maintenez les mesures barrières !"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Covid 19 : Ardèche, département le plus touché de France"
weight = 1

+++
Extrait de la lettre du Préfet de l'Adèche reçue en mairie ce jour :

> Le taux d'incidence de la Covid 19 est passé au-dessus de la barre des 240 cas pour 100.000 habitants à la mi-novembre et fait de l'Ardèche le département actuellement le plus touché de France. (...)
>
> Le relâchement des gestes barrières, signe d'une certaine lassitude après deux ans de pandémie, explique en bonne partie ce phénomène.
>
> Dans ce contexte de sur-incidence et de circulation quasi-exclusive de virus "delta" plus contagieux que le virus d'origine, j'en appelle à la responsabilité collective, afin que nous observions tous scrupuleusement ces mesures barrières (...) :
>
> * se laver les mains très régulièrement ;
> * tousser ou éternuer dans son coude ;
> * utiliser des mouchoirs à usage unique et les jeter à la poubelle ;
> * ne pas se toucher le visage ;
> * respecter une distance physique d'au moins 2 mètres ;
> * porter un masque chirurgical ou en tissu de catégorie 1 quand la distance de deux mètres ne peut pas être respectée ;
> * _aérer les pièces le plus souvent possible_, au minium quelques minutes toutes les heures ;
> * saluer sans se serrer la main, éviter les embrassades.

> (...) **J'ai demandé aux forces de sécurité intérieure du département de renforcer significativement le contrôle des règles relatives au passe sanitaire** (...)
>
> **La vaccination complète est le moyen le plus efficace pour prévenir les hospitalisations et les formes grave de la Covid. Elle permet de se protéger et de protéger les autres.** Toutefois, nous constatons globalement une plus faible adhésion de la population ardéchoise à la vaccination contre la Covid-19.

Pour ceux qui resteraient encore sceptiques, nous avions alerté la semaine dernière sur la hausse inquiétante du nombre d'hospitalisations dues à la Covid 19. L'actualisation des données montre que la tendance s'est dangereusement et radicalement accélérée ces derniers jours. Il n'y avait que 6 hospitalisés mi-octobre. Il y en a aujourd'hui plus de 11 fois plus (courbe rouge) ! La courbe est pratiquement verticale.

![](/media/covid-19-hospitalisation-21-11-2021.png)