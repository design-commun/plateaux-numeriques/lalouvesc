+++
date = 2021-06-09T22:00:00Z
description = "Les fleurs ont été récupérées et ont commencé à être installées"
header = "/media/fleurissement-2021-1.jpg"
icon = ""
subtitle = ""
title = "Fleurissement du village en cours"
weight = 1

+++
Les bénévoles du Comité fleurissement ont commencé à installer les plantes dans tout le village. Celui-ci a été divisé en trois zones, chacune sous la responsabilité de bénévoles pour leur entretien.

Ils attendent que l'ensemble de la population fasse, elle aussi, un effort de fleurissement pour avoir un village estival à la hauteur de la tradition d'accueil louvetoune.

![](/media/fleurissement-2021-9.jpg)

{{<grid>}} {{<bloc>}}

![](/media/fleurissement-2021-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-2021-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-2021-7.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/fleurissement-2021-3.jpg)

{{</bloc>}}{{</grid>}}