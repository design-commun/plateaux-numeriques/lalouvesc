+++
date = 2022-05-03T22:00:00Z
description = "Une pédagogie adaptée, des activités variées..."
header = "/media/inscription-ecole-st-joseph-2022-2.jpg"
icon = ""
subtitle = "Une pédagogie adaptée, des activités variées..."
title = "Ouverture des inscriptions à l'école St Joseph"
weight = 1

+++
Inscriptions sur rendez-vous : 04 75 67 82 51, ecolestjosephlalouvesc@gmail.com

![](/media/inscription-ecole-st-joseph-2022.jpg)