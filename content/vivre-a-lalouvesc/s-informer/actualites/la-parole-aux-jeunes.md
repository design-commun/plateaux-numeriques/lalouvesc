+++
date = 2021-09-15T22:00:00Z
description = "Une réunion à la Mairie pour écouter les jeunes : samedi 18 septembre à 10h30 "
header = "/media/reunion-jeunes-2021-2.jpg"
icon = ""
subtitle = ""
title = "La parole aux jeunes"
weight = 1

+++
La Mairie souhaite recueillir les suggestions des jeunes Louvetous.

![](/media/reunion-jeunes-2021.jpg)