+++
date = 2022-12-24T23:00:00Z
description = "Pour tous les Louvetous et tous les amis du village"
header = "/media/creche-marrel-2022-4.jpg"
icon = ""
subtitle = "Pour tous les Louvetous et tous les amis du village"
title = "Joyeux Noël à toutes et tous !"
weight = 1

+++
## Deux crèches louvetonnes

#### Place Marel

![](/media/creche-marrel-2022-5.jpg "Réalisation François Besset")

#### et celle en gâteaux de l'école St Joseph

![](/media/creche.jpg)

## Et pour le plaisir, cette jolie comptine britannique

<iframe width="560" height="315" src="https://www.youtube.com/embed/1UHmQANFtNs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Les douze jours de Noël sont les douze jours entre Noël et l'Épiphanie (6 janvier). Elle a été publiée pour la première fois en 1780.

Une interprétation de la chanson fait correspondre les douze jours de Noël aux douze mois de l'année, chaque (nombre) jour en relation avec le mois correspondant. Les gens prêtaient même attention au temps de chaque jour et pensaient que ce serait la météo du mois correspondant pour l'année à venir. Mais d'autres interprétations ont aussi été avancées. Plus d'[informations sur cette chanson](https://fr.wikipedia.org/wiki/The_Twelve_Days_of_Christmas). (Wikipédia).

#### Traduction

_Le premier jour après Noël,  
Mon amour m'a envoyé  
Une perdrix dans un poirier._

_Le deuxième jour après Noël,  
Mon amour m'a envoyé,  
Deux tourterelles  
Et une perdrix dans un poirier._

Puis sur le même tempo :

* troisième jour : _Trois poules françaises,_
* quatrième jour : _Quatre merles noirs_,
* cinquième jour : _Cinq anneaux d'or_,
* sixième jour : _Six oies pondant_,
* septième jour : _Sept cygnes nageant_,
* huitième jour : _Huit filles de ferme trayant_,
* neuvième jour : _Neuf dames dansant_,
* dixième jour : _Dix messieurs sautant_,
* onzième jour : _Onze joueurs de fifre jouant_,
* douzième jour : _Douze tambours battant_.

## Joyeuses fêtes à toutes et tous !