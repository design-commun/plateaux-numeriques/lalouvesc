+++
date = 2022-10-17T22:00:00Z
description = "17-19 novembre"
header = "/media/college-st-joseph-satillieu-2022.jpg"
icon = ""
subtitle = ""
title = "Portes ouvertes au collège de Satillieu"
weight = 1

+++
Portes ouvertes

[Collège St Joseph](https://sites.google.com/site/stjosephenvalday/home) en Val d'Ay à SATILLIEU

17, 18 et 19 novembre sur rendez-vous

contactez le secrétariat 04 75 34 96 42