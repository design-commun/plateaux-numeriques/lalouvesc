+++
date = 2021-03-02T23:00:00Z
description = "Nous venons d'apprendre avec tristesse le décès du Père Philippe Hermelin qui a été Recteur du pèlerinage à Lavouvesc entre 1987 et 1999."
header = "/media/basilique_lalouvesc.JPG"
icon = ""
subtitle = ""
title = "Le Père Philippe Hermelin nous a quitté"
weight = 1

+++
Nous venons d'apprendre avec tristesse le décès du Père Philippe Hermelin qui a été Recteur du pèlerinage à Lavouvesc entre 1987 et 1999. On l’appelait à Pau, son dernier ministère où il est décédé, "le bon Philippe". Le Père Olivier de Framond nous indique, en suivant son itinéraire : "Avec son départ, Franklin (école à Paris) perd un père spirituel, l'église Saint Ignace (Paris) un chapelain, Lalouvesc un directeur de pèlerinage, le MEJ (Mouvement Eucharistique des Jeunes) un ardent défenseur, Lyon-Sala un supérieur et Penboc'h (Bretagne) un animateur de retraite." 