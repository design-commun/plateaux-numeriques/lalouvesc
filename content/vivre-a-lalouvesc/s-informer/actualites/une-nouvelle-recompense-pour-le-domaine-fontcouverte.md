+++
date = 2022-12-04T23:00:00Z
description = "Décidément, l'année se termine en beauté pour Fontcouverte"
header = "/media/coupe-fontcouverte-dec-2022-1.jpg"
icon = ""
subtitle = ""
title = "Une nouvelle récompense pour le Domaine Fontcouverte"
weight = 1

+++
Les Automnales du Comité régional de tourisme équestre (CRTE) Auvergne-Rhône-Alpes, journées de rencontre, d’échange et de réflexion autour du cheval et de l’équitation d’extérieur, se sont déroulées les 3 et 4 décembre 2022, à Saint-Genest-Malifaux (42). A cette occasion, le Domaine Fontcouverte a été récompensé pour sa deuxième place au challenge régional de longues-rênes Auvergne-Rhône-Alpes (concours qui s’est déroulé sur 8 journées au cours de la saison 2021-2022).

![](/media/coupe-fontcouverte-dec-2022-2.jpg)

Le Domaine Fontcouverte ne donne en effet pas que des cours de travail monté, mais propose également des séances de travail à pied. Ces dernières constituent un excellent moyen de consolider la relation entre le cheval et son cavalier et sont très complémentaires au travail monté. Elles permettent notamment de travailler la main des cavaliers, de leur faire comprendre, par exemple, que le cheval tourne non pas parce que l’on tire sur la rêne intérieure, mais bien parce que l’on cède de celle extérieure et, ainsi, de cheminer vers une équitation de légèreté plus respectueuse de son cheval, une équitation vers le bien-être animal.

Si vous êtes intéressé par le travail à pied, n’hésitez pas à contacter le Domaine Fontcouverte au 04.75.34.99.40.