+++
date = 2022-10-17T22:00:00Z
description = "Objectif atteint avec 15 jours d'avance"
header = "/media/deco-trail-2022-6.jpg"
icon = ""
subtitle = ""
title = "Trail des Sapins : 1.000 inscrits"
weight = 1

+++
La jauge maximale de 1.000 participants au TRAIL DES SAPINS a été atteinte dès lundi matin 17 octobre !

L'inscription n'est donc plus possible malgré de nombreuses demandes qui arrivent encore par mail ou par téléphone. Les retardataires devront attendre l'édition 2023.

Est-ce la diffusion des décorations sur les réseaux sociaux qui a ainsi suscité un ultime engouement ? Il faut dire que le square, haut en couleur, ne manque pas de mordant... Cela vaut le détour.  
{{<grid>}}{{<bloc>}}

![](/media/deco-trail-2022-2.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/deco-trail-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/deco-trail-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/deco-trail-2022-5.jpg)

{{</bloc>}}{{</grid>}}

"Rien n'est impossible à ceux qui osent", telle est la maxime sérigraphiée sur le tee-shirt de cette année.

Les bénévoles ont osé, il a été possible d'atteindre les 1.000 participants attendus.

**BRAVO !**

![](/media/tee-shirt-trail-2022.jpg)

Rappel des départs des parcours du Trail : 48 km L'infernale à 15h30, 22 km La Louvetrouille à 17h30, 12 km, Le Val des Sorcières ou Rando des Sorcières à 18h, 7 km La Forêt des Afars ou la Rando des Afars à 18h15. Le repas Ardéchois du Trail des Sapins clôture le Trail de manière festive et conviviale.

[Site du Trail](https://www.traildessapins-lalouvesc.fr/), [Facebook](https://www.traildessapins-lalouvesc.fr/).

Agnès, secrétaire 07.66.67.94.59