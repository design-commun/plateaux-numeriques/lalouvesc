+++
date = 2021-12-03T23:00:00Z
description = "Coffrets de noël, papiers cadeaux, billets de tombola, calendriers sont en vente dans les commerces de Lalouvesc"
header = "/media/apel-calendrier-enfants-2021.jpg"
icon = ""
subtitle = ""
title = "Les actions de l'APEL, au bénéfice des enfants de l'école redémarrent"
weight = 1

+++
Bonjour à tous,

Les actions de l'APEL, au bénéfice des enfants de l'école redémarrent.

Nous mettons en vente des rouleaux de papier cadeaux au prix de 2 euros, ainsi que les calendriers des Œuvres Chrétiennes au prix de 6 euros, vous pouvez les trouver au Vival.

Lancement de la grande tombola !

Depuis le week-end dernier, vous pouvez trouver vos tickets auprès des enfants de l'école ainsi que chez nos commerçants : Bar du lac, Vival, Pharmacie, Boulangerie.

Et une surprise n'arrivant jamais seule !

Les calendriers 2022 (vendus au prix de 5 €) à l'effigie de nos Loulous ainsi que des coffrets de noël sont aussi disponibles dès ce week-end auprès des enfants et dans nos commerces.

Merci d'avance pour votre participation qui, comme vous le savez, permet aux enfants de l'école de bénéficier de sorties, matériels, animations et cette année d'un voyage scolaire.

APEL

{{<grid>}}{{<bloc>}}

![](/media/apel-calendrier-tombola.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/apel-coffret-2021.jpg "Contenu du coffret")

{{</bloc>}}{{</grid>}}