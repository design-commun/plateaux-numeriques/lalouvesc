+++
date = 2022-11-17T23:00:00Z
description = "Réunion mardi 22 novembre 19h30"
header = "/media/decoration-noel-2021-9.jpg"
icon = ""
subtitle = ""
title = "Des idées pour les décorations de Noël ?"
weight = 1

+++
Une réunion pour les décorations de Noël est prévu le mardi 22 novembre à 19h30 à la mairie.  
Vous avez des idées, vous voulez vous investir dans la décoration du village... vous êtes bienvenus.

Chaque année les visiteurs montent à Lalouvesc pour les admirer... et aussi s'en inspirer. Quelques exemples des années passées :

{{<grid>}}{{<bloc>}}

![](/media/decorations-noel-ephad-2021-1.jpeg)

![](/media/decoration-noel-2021-3.jpg)

![](/media/deco-noel-4.jpg)

![](/media/decoration-noel-2021-1.jpg)

![](/media/illumination-2021-10.JPG)

![](/media/decorations-noel-mairie-2021-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/deco-noel3.jpg)

![](/media/deco-noel-2.jpg)

![](/media/deco-noel-5.jpg)

![](/media/deco-noel-6.jpg)

![](/media/deco-noel.jpg)

![](/media/illumination-2021-27.JPG)

{{</bloc>}}{{</grid>}}