+++
date = 2021-10-16T22:00:00Z
description = "A 10h30 ce dimanche, elles étaient toutes vendues !"
header = "/media/adaoei-brioches-2021-4.jpg"
icon = ""
subtitle = ""
title = "Succès pour les brioches de l'Adapei"
weight = 1

+++
49 brioches vendues devant la mairie dès 10h30 pour un total de 355 € récoltés à l'occasion de la [journée de solidarité de l'Adapei](https://www.adapei07.fr/operation-brioches-du-11-au-17-octobre-2021/), soit une moyenne de 7,25 € par brioche, très largement au-dessus de la moyenne nationale de 2020 (5,85 €).

Merci à tous les acheteurs pour leur générosité !