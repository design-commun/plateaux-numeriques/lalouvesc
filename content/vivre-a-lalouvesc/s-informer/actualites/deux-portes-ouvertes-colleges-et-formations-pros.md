+++
date = 2023-03-06T23:00:00Z
description = "18 mars à Marlhes (42), 31 mars au Chambon sur Lignon (43)"
header = "/media/mfr-marlhes-mars-2023.jpg"
icon = ""
subtitle = ""
title = "Deux portes ouvertes : collèges et formations pros"
weight = 1

+++
* **18 mars** :  Formation par alternance, [MFR Marlhes](https://www.mfr-marlhes.com/) 04 77 51 81 87 (MFR = [Maison familiale rurale](https://fr.wikipedia.org/wiki/Maison_familiale_rurale))
* **31 mars** : SEGPA et [Collège du Lignon](https://lignon-le-chambon.ent.auvergnerhonealpes.fr/) 04 71 65 82 00 (SEGPA = [Section d'enseignement général et professionnel adapté](https://eduscol.education.fr/1184/sections-d-enseignement-general-et-professionnel-adapte))

{{<grid>}}{{<bloc>}}

![](/media/mfr-marlhes-mars-2023.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/college-chambon-mars-2023.jpg)

{{</bloc>}}{{</grid>}}