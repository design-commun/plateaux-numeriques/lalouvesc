+++
date = 2022-11-04T23:00:00Z
description = "Première étape, place du Lac"
header = "/media/bac-a-ordure-2022-4.jpg"
icon = ""
subtitle = ""
title = "Les bacs à ordure arrivent"
weight = 1

+++
Non, ce n'est pas un nouveau clin d’œil post-Halloween du Comité des fêtes... les nouveaux bacs à ordure ont été entreposés sur la place du Lac en attendant d'être répartis sur les six points de récolte prévus :

* Place du Lac,
* Chemin Grosjean,
* Maison de retraite,
* Camping,
* Place des Trois Pigeons,
* Aire du Grand Lieu.

{{<grid>}}{{<bloc>}}

![](/media/bac-a-ordure-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/bac-a-ordure-2022-2.jpg)

{{</bloc>}}{{</grid>}}

![](/media/bac-a-ordure-2022-1.jpg)