+++
date = 2022-06-18T22:00:00Z
description = "Plus de 6.000 coureurs ce samedi sous un soleil de plomb... "
header = "/media/ardechoise-2022-samedi-3.jpg"
icon = ""
subtitle = "Plus de 6.000 coureurs ce samedi sous un soleil de plomb..."
title = "Chaud, chaud l'Ardéchoise !"
weight = 1

+++
Ils ont pu se rafraichir à Lalouvesc dans la bonne humeur avant la dernière descente sur Saint-Félicien.

Une organisation impeccable, un participant surprise, merci à toutes et tous.

![](/media/ardechoise-2022-samedi-1.jpg)

![](/media/ardechoise-2022-samedi-7.jpg)

![](/media/ardechoise-2022-samedi-5.jpg)

![](/media/ardechoise-2022-samedi-6.jpg)

![](/media/ardechoise-2022-samedi-3.jpg)

Plus de photos sur la [page FB de l'Office du tourisme](https://www.facebook.com/OTValDAy/posts/pfbid0A6ZQmSBr5Rn9hcjBkid6RXyvkshFTYE7eShc3K7aTo1Yep6WCgo2pfVf35soYnjPl).