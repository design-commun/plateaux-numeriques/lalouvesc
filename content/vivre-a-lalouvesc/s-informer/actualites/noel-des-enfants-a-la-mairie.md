+++
date = 2022-12-16T23:00:00Z
description = "Un goûter pour l'école St Joseph"
header = "/media/ecole-noel-2022-6.png"
icon = ""
subtitle = ""
title = "Noël des enfants à la Mairie"
weight = 1

+++
La Mairie a reçu vendredi les vingt-deux enfants de l'école Saint Joseph pour un goûter joyeux et apprécié.

{{<grid>}}{{<bloc>}}

![](/media/ecole-noel-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecole-noel-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecole-noel-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecole-noel-2022-3.jpg)

{{</bloc>}}{{</grid>}}

Venez admirer le calendrier de l'avent réalisé par les enfants. Chaque jour un nouveau dessin devant la Mairie.

Et n'oubliez pas de déposer votre lettre au père Noël dans la boîte adhoc devant la mairie. Déjà plusieurs courriers ont atteint le destinataire...

![](/media/ecole-noel-2022-8.jpg)