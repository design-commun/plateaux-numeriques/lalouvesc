+++
date = 2023-03-22T23:00:00Z
description = "Arrêté préfectoral"
header = "/media/robinet-eau.jpg"
icon = ""
subtitle = ""
title = "Abrogation des limites d'usage de l'eau"
weight = 1

+++
Suite aux dernières précipitations, le Préfet a décidé par arrêté préfectoral du 21 mars 2023 d’abroger toutes les restrictions d’usage de l’eau qui étaient en vigueur sur les bassins versants de la Cance et du Doux.

L’ensemble du département est maintenu en VIGILANCE.

**La précocité de la sécheresse cette année doit inciter tous les usagers à réduire autant que possible leurs consommations.**

lire l'[arrêté préfectoral](/media/20230321_ap2_abrogation_secheresse.pdf)