+++
date = 2021-09-24T22:00:00Z
description = "Des orages sont prévus dans la nuit du 25 au 26 septembre, soyez prudents."
header = "/media/meteo-orage-25-sept-2021.png"
icon = ""
subtitle = ""
title = "Alerte orages sur l'Ardèche"
weight = 1

+++
L'Ardèche a été placée en vigilance orange-orage-pluie-inondation par Météo France. Même si le plus fort de la tempête devrait concerner le sud du département, il pourrait tomber plus de 50mm de pluie en moins de 3 heures sur notre territoire, accompagnée de rafales de vent à partir du milieu de la nuit. La vigilance est prévue sur une durée de 6 à 12h.  
Soyez prudents !

Voici les consignes à respecter dans les cas d'alerte : s'informer, ne pas sortir en voiture, se soucier des proches, s'éloigner des cours d'eau, ne pas sortir, éviter les sous-sols...

![](/media/pluie-inondation.jpg)