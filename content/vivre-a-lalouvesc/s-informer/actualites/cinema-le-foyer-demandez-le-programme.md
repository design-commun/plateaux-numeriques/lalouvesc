+++
date = 2022-06-22T22:00:00Z
description = "Les films et les séances de juillet, bandes annonces"
header = "/media/cinema-le-foyer.jpg"
icon = ""
subtitle = ""
title = "Cinéma Le Foyer : demandez le programme !"
weight = 1

+++
Amis spectateurs et cinéphiles,

Toute l'équipe du cinéma le foyer est heureuse de vous accueillir pour une nouvelle saison à Lalouvesc qui, cette année, va démarrer le 3 juillet 2022 avec la fête du cinéma.

Après deux ans de restrictions sanitaires le festival de Cannes à retrouvé sa version originelle en déployant tous les talents sur le tapis rouge.. Et il était temps, car plus que jamais nous avons tous envie de nous divertir et de rêver.

Nous vous avons donc réservé un programme haut en couleur afin de satisfaire tous les publics, comédies, drames, polar, dessin animé... Ne ratez pas l'avant première du film **_La Nuit du 12_** ! Découvert lors du dernier festival de Cannes, ce polar tourné à Grenoble et dans la Maurienne ne vous laissera pas de marbre.

Deux rencontres sont également prévues pour ce mois de juillet. Tout d’abord autour du film **_Elvis_**, le Cinéma aura le plaisir d’accueillir Claude, un passionné du King, intarissable sur le sujet et qui installera une expo dans le cinéma.

Enfin, Christophe Tardy, réalisateur de **_Là où le temps s’est arrêté_**, sera présent à l’issue des séances pour échanger avec vous.

Nous espérons que le cru 2022 vous réjouira et que vous serez nombreux !

L'équipe du cinéma

Tarif : 6€50 ; -18 ans et étudiants : 5€50 ; du 3 au 6 juillet c’est la Fête du Cinéma : 4€ la séance.

### Séances à 21h à l'Abri du pèlerin

* Dimanche 3 juillet **_Qu'est ce qu'on a tous fait au bon dieu ?_** ([bande annonce](https://youtu.be/UkXUfWFiiao))
* Lundi 4 juillet **_Illusions perdues_** ([bande annonce](https://youtu.be/KGQn_mWDWrA))
* Samedi 9 juillet **_Maison de retraite_** ([bande annonce](https://youtu.be/bLFIhJUpmNk))
* Dimanche 10 juillet **_Mystère_** ([bande annonce](https://youtu.be/nmfMMrijOzU))
* Lundi 11 juillet **_La nuit du 12_** ([bande annonce](https://youtu.be/nC6AbWkkULc)) · AVANT-PREMIÈRE
* Samedi 16 juillet **_En corps_** ([bande annonce](https://youtu.be/WMqIkiI6fAA))
* Dimanche 17 juillet **_Le Temps des Secrets_** ([bande annonce](https://youtu.be/RFpGLeipEBA))
* Lundi 18 juillet **_En corps_** ([bande annonce](https://youtu.be/WMqIkiI6fAA))
* Mercredi 20 juillet **_Notre Dame brûle_** ([bande annonce](https://youtu.be/YlDXdPSEtgk))
* Jeudi 21 juin **_C'est Magnifique_** ([bande annonce](https://youtu.be/z-MlOwkqhDY))
* Samedi 23 juillet **_Notre Dame brûle_** ([bande annonce](https://youtu.be/YlDXdPSEtgk))
* Dimanche 24 juillet **_C'est Magnifique_** ([bande annonce](https://youtu.be/z-MlOwkqhDY))
* Lundi 25 juillet **_Elvis_** ([bande annonce](https://youtu.be/S2nQInzyqvM)) · Expo et rencontre avec un spécialiste
* Mercredi 27 juillet **_Irréductible_** ([bande annonce](https://youtu.be/-xXDfZBHV4E))
* Jeudi 28 juillet 17h et 21h **_Là où le temps s'est arrêté_** ([bande annonce](https://youtu.be/zUH1K3JIGzI)) en présence de Christophe Tardy, réalisateur du film
* Samedi 30 juillet **_Irréductible_** ([bande annonce](https://youtu.be/-xXDfZBHV4E))
* Dimanche 31 juillet **_Les Minions 2 : Il était une fois Gru_** ([bande annonce](https://youtu.be/Cm6wpegvRpA))
* Lundi 1er août **_À l'ombre des filles_** ([bande annonce](https://youtu.be/0qezBv7i-bc))