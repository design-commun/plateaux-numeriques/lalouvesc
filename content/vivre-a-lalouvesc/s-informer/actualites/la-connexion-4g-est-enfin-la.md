+++
date = 2021-07-15T22:00:00Z
description = "L'antenne couvrant le village est opérationnelle."
header = "/media/fernand-raynaud-1966.jpg"
icon = ""
subtitle = ""
title = "La connexion 4G est enfin là !"
weight = 1

+++
Avec deux jours de retard sur le calendrier prévu, la 4G est enfin opérationnelle sur le village. C'est un jour à marquer d'une pierre blanche, un pas important pour le désenclavement du village !

De nos jours, le téléphone portable est devenu un compagnon indispensable pour contacter la famille, les services les plus divers et même pour naviguer sur internet. Ainsi, le tiers des connexions sur notre site lalouvesc.fr depuis son ouverture est venu directement d'un téléphone portable.

L'antenne a été dressée par SFR. Elle est provisoirement alimentée en électricité par un groupe électrogène en attendant d'être reliée au réseau électrique dans le courant de l'hiver. Le maire s'est assuré régulièrement de la programmation et de l'avancement des travaux pour qu'ils ne prennent pas de retard.

Mais le retard dans l'installation du réseau téléphonique est malheureusement une vieille tradition française. Les plus anciens se souviendront que, dans les années soixante, la moitié des Français attendaient une ligne, tandis que l'autre moitié attendait la tonalité... et que [Fernand Raynaud réclamait en vain le 22 à Asnières](). Les plus jeunes attendent la fibre qui a sur le village déjà plusieurs années de retard.