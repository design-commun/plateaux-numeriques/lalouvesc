+++
date = 2022-11-03T23:00:00Z
description = "En vente dans les commerces de Lalouvesc"
header = "/media/ecole-oct-2022-4.jpg"
icon = ""
subtitle = ""
title = "Papiers cadeaux et chocolats pour l'APEL"
weight = 1

+++
Voici les deux premières actions menées par l'équipe de l'APEL au profit des enfants de l'école St Joseph !

1. Vous pouvez trouvez dès aujourd'hui les rouleaux de papier cadeau (2 euros/l'un) à la boulangerie et au Vival.
2. Nous lançons une commande de chocolats pour la fin d'année, vous pouvez trouver les catalogues et bons de commande au Vival, à la boulangerie, à la pharmacie et au bureau de poste (à rendre avec le règlement dans la boite aux lettres de l’école pour le 22 novembre) et [ici en ligne](/media/catalogue-chocolats-apel-2022.pdf) .

D'avance merci pour votre participation.

Et un grand merci également à nos commerçants partenaires !