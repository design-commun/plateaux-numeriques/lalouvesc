+++
date = 2022-06-16T22:00:00Z
description = "La Préfecture interdit le feu sous toutes ses formes sur le département"
header = "/media/feux-interdits-2022.jpg"
icon = ""
subtitle = "La Préfecture interdit le feu sous toutes ses formes sur le département"
title = "Feux interdits"
weight = 1

+++
Le feu est interdit jusqu'au 30 juin.

Consultez l'[arrêté préfectoral](/media/ap_interdit_emploi_feu_juin2022.pdf).