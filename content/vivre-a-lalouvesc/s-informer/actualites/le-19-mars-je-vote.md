+++
date = 2023-03-07T23:00:00Z
description = "Jour de l'élection municipale partielle, rappel des modalités"
header = "/media/entete-mars-2023.jpg"
icon = ""
subtitle = ""
title = "Le 19 mars, je vote !"
weight = 1

+++
Nous avons expliqué en détail dans un [Bulletin spécial](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/special-elections-partielles/) pourquoi nous avions besoin de compléter le Conseil municipal pour pouvoir traiter tous les dossiers ouverts d'ici la fin du mandat.

Deux candidatures ont été retenues par la préfecture pour l’élection municipale partielle complémentaire :

* Madame Nicole Porte
* Monsieur Corentin Serayet

Rappel : [arrêté préfectoral](https://www.lalouvesc.fr/media/ap_07_2023_03_03_00002_lalouvesc.pdf).

### Pourquoi faut-il voter ?

En votant, vous ferez votre devoir de citoyen, bien sûr. A chaque élection Lalouvesc montre l'exemple.

Vous marquerez aussi votre solidarité avec le Conseil, avec l'orientation qu'il a pris pour le village. Vous approuverez et appuierez l'engagement des candidats au Conseil. C'est important, les membres du Conseil sont bénévoles, ils ont besoin de se sentir soutenus.

Pour être élu au premier tour un candidat doit obtenir plus de 50% des voix des votants et **plus de 25% des inscrits sur la liste électorale de la Commune**. Il est donc aussi essentiel de venir voter dès le premier tour. En effet à moins que d'autres candidats se manifestent plus tard, si nous atteignons ce quorum, nous n'aurons qu'un tour à organiser.

**Le 19 mars le bureau de vote sera ouvert à la Mairie de 8h à 18h.**

### Rappel sur les procurations

Si vous ne pouvez vous déplacer le jour du vote, vous pouvez demander à quelqu’un d’autre de voter à votre place.

Pour donner procuration à un électeur, vous aurez besoin soit de son [numéro d’électeur](https://www.maprocuration.gouv.fr/#FAQ) et de sa date de naissance, soit de ses données d’état civil et de sa commune de vote.

Vous effectuez votre demande de procuration en ligne en toute simplicité, puis vous vous déplacez au commissariat, à la gendarmerie ou au consulat pour faire vérifier votre identité et valider votre procuration

Tout est expliqué [ici](https://www.maprocuration.gouv.fr/) .

![](/media/affiche-election-mars-2023.jpg)