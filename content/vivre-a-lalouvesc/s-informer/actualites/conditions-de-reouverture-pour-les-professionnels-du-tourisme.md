+++
date = 2021-05-06T22:00:00Z
description = "Synthèse des mesures applicables  "
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Conditions de réouverture pour les professionnels du tourisme"
weight = 1

+++
L'agence de développement touristique de l'Ardèche nous a fait parvenir cette synthèse des conditions de réouverture des professions du tourisme suite à l'évolution des mesures sanitaires.

### Restauration 

\- Réouverture des terrasses de bars et restaurants à partir du 19 mai avec des tables de 6 personnes maximum.  
\- Réouverture des bars et restaurants en intérieur à partir du 9 juin, avec une limite de 6 personnes par table (application d'une jauge en cours de discussion).  
\- Hébergements (hôtels, centres de vacances, chambres d'hôtes, campings...) autorisés à ouvrir leur activité de restauration en intérieur pour leurs clients à compter du 19 mai.

### Lieux culturels autorisés à rouvrir le 19 mai 

\- Musées et sites de visites : jauge de 8 m2 par visiteur le 19 mai, 4 m2 le 9 juin et suppression de la jauge le 30 juin.   
\- Cinémas : jauge de 35 % de l'effectif le 19 mai, puis jauge évolutive jusqu'à suppression le 30 juin.  
\- Spectacles : jauge 800 personnes en intérieur et 1 000 en extérieur à partir du 19 mai, puis évolution des jauges à 5 000 pers. à partir du 9 juin (sous réserve de protocoles sanitaires en cours de définition).

### Divers

**Parcs animaliers, parcs et jardins**, autorisés à ouvrir à compter du 19 mai.  
  
**Parcs accrobranches** autorisés à ouvrir à compter du 19 mai.  
  
**Activités accompagnées** (VTT, canyoning, randonnée...) autorisées dès aujourd'hui avec une jauge maximale de 6 pers. accompagnées, puis 10 pers. maximum à compter du 19 mai.  
  
**Locations de bateaux** autorisées dès aujourd'hui avec autorisation de transport de clients dans le cadre du protocole sanitaire de 2020.  
  
**Piscines extérieures d'hôtels et de campings** autorisées à ouvrir à compter du 19 mai.  
  
**Piscines intérieures** autorisées à rouvrir le 9 juin avec une jauge de 35 %.  
  
**Visites guidées** : visites en extérieur limitées à 6 pers. jusqu'au 19 mai. A partir du 19 mai, les visites réalisées par les guides sans carte professionnelle sont limitées à 10 pers. A cette même date, les guides conférenciers titulaires d'une carte professionnelle ne sont pas contraints par des limites de participants, aussi bien en extérieur qu'en intérieur.  
  
Possibilité d'accueillir des **clientèles étrangères** à partir du 9 juin (pass sanitaire en cours de définition).