+++
date = 2021-04-19T22:00:00Z
description = "Dépistage mardi 27 avril Satillieu, vaccination à Annonay pour les plus de 55 ans  "
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Covid-19 : dépistage sans rendez-vous à Satillieu et vaccination à Annonay"
weight = 1

+++
## Dépistage pour tous

![](/media/covid-19-depistage.jpg)

## Vaccination

Pour la vaccination, vous pouvez-aussi prendre [rendez-vous sur doctolib](https://partners.doctolib.fr/cabinet-pluridisciplinaire/annonay/maison-de-sante-des-cevennes?speciality_id=5494&enable_cookies_consent=1) à partir de 55 ans pour la clinique des Cévennes à Annonay.