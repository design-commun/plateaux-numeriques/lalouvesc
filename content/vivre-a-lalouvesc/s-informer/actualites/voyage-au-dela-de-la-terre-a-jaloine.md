+++
date = 2023-01-04T23:00:00Z
description = "Programme et inscription pour les enfants, vacances de février"
header = "/media/centre-de-loisir-jaloine-2921.jpg"
icon = ""
subtitle = ""
title = "Voyage au delà de la terre à Jaloine"
weight = 1

+++
Le centre de loisirs sera ouvert du 6 au 17 février.  
Mardi 7 février, veillée étoilée pour les + de 7ans.

NOUVEAUTÉ : veillée pour les 9-13 ans le 16 février.

Renseignements et inscriptions du 11 au 18 janvier dans la limite des places disponibles.

[Programme](/media/prg-vs-fevrier-2023-jaloine.pdf) et [inscriptions](/media/fiche-d-inscription-vs-fevrier-2023-jaloine.pdf). 

07 66 49 07 90  
[cdl.jaloine@gmail.com](mailto:centredeloisirs.roiffieux@gmail.com)

On ne vous répond pas tout de suite, pas de panique, dès que possible nous vous répondrons. Hors vacances scolaires, nous répondons au mail les mardis, mercredis et jeudi dès que nous pouvons. Nous mettons tout en œuvre pour proposer aux enfants le meilleur accueil qu'il soit, cela nous demande du temps, merci de votre compréhension.