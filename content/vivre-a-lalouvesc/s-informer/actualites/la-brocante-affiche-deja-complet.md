+++
date = 2022-08-17T22:00:00Z
description = "Si vous vouliez réserver un stand... c'est trop tard."
header = "/media/affiche-brocante-2022-3.jpg"
icon = ""
subtitle = ""
title = "La brocante affiche déjà complet !"
weight = 1

+++
Comme chaque année, la brocante organisée par le Comité des fêtes rencontre un franc succès.

Plus de place pour les exposants, [malgré un forte augmentation de l'espace cette année](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/25-musique-maestro/#pour-r%C3%A9pondre-%C3%A0-la-demande-brocante-s%C3%A9tend-dans-la-bonne-humeur), les 250 emplacements ont été pris d'assaut. 

De bon augure pour chiner le 4 septembre à Lalouvesc !

![](/media/affiche-brocante-2022-1.jpg)