+++
date = 2022-12-02T23:00:00Z
description = "3 X 15 jours, printemps-été 2023 région d'Aubenas"
header = "/media/snu-2022.png"
icon = ""
subtitle = ""
title = "Offres d'emploi : tutrice ou tuteur pour le Service national universel"
weight = 1

+++
Le Service national universel organisera en 2023 trois séjours de cohésion en Ardèche :

* du 9 avril au 21 avril - centre SNU à Aubenas (à confirmer)
* du 11 au 23 juin - centre SNU à Meyras
* du 4 au 16 juillet - centre SNU à Meyras

À chaque fois, ce sont une centaine de jeunes de 15 à 17 ans venus d’autres départements qui découvriront notre département.

Dans le cadre de la préparation de ces séjours, **nous recherchons des tutrices et des tuteurs pour encadrer ces jeunes.**

Ils devront également participer à la semaine de formation qui se déroulera la semaine précédant les séjours (obligatoire).

Consulter [la fiche descriptive du poste et des profils recherchés](/media/fiche-de-poste-snu_tuteur-de-maisonnee_ardeche.docx).

Il y a une **vingtaine de postes à pourvoir** sur la totalité des séjours, mais il est possible de candidater sur plusieurs séjours.