+++
date = 2021-04-01T22:00:00Z
description = "Le compte-rendu est accessible ici  "
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte-rendu du Conseil municipal du 24 mars 2021"
weight = 1

+++
Le compte-rendu est [accessible ici](/media/2021-2-conseil-municipal-lalouvesc.pdf).  Bonne lecture !