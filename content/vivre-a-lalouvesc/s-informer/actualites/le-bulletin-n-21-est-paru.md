+++
date = 2022-03-31T22:00:00Z
description = "21 - Avril 2022 - Au printemps, on sème..."
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "21 - Avril 2022 - Au printemps, on sème..."
title = "Le Bulletin n°21 est paru"
weight = 1

+++
Le printemps est là, il est temps de préparer la saison estivale. Le Carrefour des arts prend un nouvel élan, le Comité des fêtes fait son plein de jus, la Vie Tara et l’Abri du pèlerin se rénovent, les journées citoyennes préparent les parcours de balade et pourquoi pas prendre un bain de forêt ? On vous raconte tout cela … et, comme toujours, bien d’autres choses encore.

[Lire le Bulletin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/)

### Entrée directe par le sommaire

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#mot-du-maire)
* [Actualités de la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#actualités-de-la-mairie)
  * [En avril, on vote !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#en-avril-on-vote-)
  * [Ecolotissement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#ecolotissement)
  * [Camping](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#camping)
  * [Nouvelle secrétaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#nouvelle-secrétaire)
  * [Beauséjour et jeu monument : présentation publique](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#beauséjour-et-jeu-monument--présentation-publique)
  * [Fibre optique](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#fibre-optique)
* [Zoom](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#zoom)
  * [Une nouvelle ambition pour le Carrefour des arts](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#une-nouvelle-ambition-pour-le-carrefour-des-arts)
  * [Le Comité des fêtes toujours au jus](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#le-comité-des-fêtes-toujours-au-jus)
  * [Vous prendrez bien un bain de forêt !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#vous-prendrez-bien-un-bain-de-forêt-)
* [Culture - loisirs](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#culture---loisirs)
  * [Carnaval et plongeons à l’école St Joseph](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#carnaval-et-plongeons-à-lécole-st-joseph)
  * [Centre de loisirs de Jaloine](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#centre-de-loisirs-de-jaloine)
  * [Les coulisses du Théâtre de la veillée](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#les-coulisses-du-théâtre-de-la-veillée)
* [Rénovations](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#rénovations)
  * [Du neuf à l’Abri du pèlerin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#du-neuf-à-labri-du-pèlerin)
  * [La Vie Tara en construction](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#la-vie-tara-en-construction)
  * [Appel à projet et étude pour le couvent du Cénacle](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#appel-à-projet-et-étude-pour-le-couvent-du-cénacle)
  * [Journées citoyennes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#journées-citoyennes)
* [Santé - sécurité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#santé---sécurité)
  * [Maintenir l’offre de santé sur le village](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#maintenir-loffre-de-santé-sur-le-village)
  * [Non au cyberharcèlement !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#non-au-cyberharcèlement-)
  * [Café des aidants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#café-des-aidants)
  * [Gendarmerie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#gendarmerie)
  * [Calendrier Santé-environnement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#calendrier-santé-environnement)
* [Mariage](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#mariage)
* [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#dans-lactualité-le-mois-dernier)
* [Poissons](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/21-preparer-la-saison/#poissons)