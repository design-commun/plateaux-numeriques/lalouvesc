+++
date = 2022-10-20T22:00:00Z
description = "Fiche de poste"
header = "/media/gites-de-france.png"
icon = ""
subtitle = ""
title = "Gîtes de France recherche un chargé de développement pour l'Ardèche-nord"
weight = 1

+++
Dans le cadre d’une restructuration de nos services, nous recherchons un(e) chargé(e) de développement habitant prioritairement sur la partie nord de l’Ardèche. Bien que le poste soit basé dans nos locaux situés à Privas, cette personne sera amenée à travailler au nord de Privas presque tous les jours.

Nous ouvrons ce poste sur un CDD de quelques mois avant la possibilité d’un emploi pérenne.

Vous trouverez ci-joint [la fiche de poste correspondant à notre offre](/media/charge-de-developpement-gite-de-france-2022.pdf).

![](/media/gites-de-france-2022.png)