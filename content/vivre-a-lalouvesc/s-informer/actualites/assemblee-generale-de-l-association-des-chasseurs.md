+++
date = 2022-07-07T22:00:00Z
description = "22 juillet 18h30, salle du camping. Ordre du jour"
header = "/media/sanglier-wkp.jpg"
icon = ""
subtitle = ""
title = "Assemblée générale de l'association des chasseurs"
weight = 1

+++
L'assemblée générale de l'ACCA (chasseurs) de Lalouvesc est convoquée pour le 22 juillet à 18h30 à la salle du camping. Ordre du jour :

1. Rapport d'activité du Président pour la saison 2020/2021
2. Rapport financier : - Validation Bilan 2020/2021 - Examen saison 2021/2022 - Validation Budget Prévisionnel 2022/2023
3. Adoption du Règlement Intérieur et de Chasse 2022/2023 (RIC avec partie mesures annuelles) - Tarif des cartes 2022/2023 - liste nominative des chasseurs extérieurs (« étrangers ») retenus pour la saison 2022/2023 par le Conseil d'Administration - jours de chasse - Validation des chefs de battues, des équipes de chasse grands gibiers et éventuellement du dispositif cahier de battues « détenteur » - organisation de la chasse grands gibiers - limitation prélèvements petits gibiers
4. Programme d'actions pour la saison 2022/2023
5. Questions diverses

Le Président : SONIER JÉRÔME