+++
date = 2021-08-11T22:00:00Z
description = "En raison de la situation sanitaire seules cinq personnes maximum pourront être admises à y assister."
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 16 août à 20h"
weight = 1

+++
Un Conseil municipal est convoqué le 16 août à 20h dans la salle de la mairie.

Attention : en raison de la situation sanitaire seules cinq personnes maximum pourront être admises à y assister.

### Ordre du jour

1. VIE MUNICIPALE
2. COMMISSION FINANCES
   *  Décisions Modificatives
3. COMMISSION GESTION 
   * Contrat de Sylvie Deygas
   * Taxe de raccordement aux réseaux
   * Taxe foncière sur les propriétés bâties, construction nouvelle à usage d’habitation
   * Vente terrain Donain
4. COMITÉ VIE LOCALE
   * Adressage
5. COMITÉ DÉVELOPPEMENT
6. QUESTION DIVERSES