+++
date = 2022-11-02T23:00:00Z
description = ""
header = "/media/11-novembre-2021-7.jpg"
icon = ""
subtitle = ""
title = "Cérémonie du 11 novembre"
weight = 1

+++
Les cérémonies **du 11 novembre 2022** se dérouleront de la manière suivante :

* **10 H 30** : Rendez-vous à la crypte de la Basilique
* **11 H 00** : Office religieux à la mémoire des victimes de la guerre **en la Basilique**
* **11 h 45** : Défilé au départ de la basilique & Cérémonie du Souvenir **au Monument aux Morts**

{{<grid>}}{{<bloc>}}

![](/media/memoire-14-18-p-besset.jpg)

{{</bloc>}}{{<bloc>}}

Rappel : le mémoire de Paul Besset sur les Louvetous morts au champ d'honneur de la Grande Guerre est[ consultable ici](/media/morts-pour-la-france-de-la-louvesc-paul-besset.pdf).

{{</bloc>}}{{</grid>}}