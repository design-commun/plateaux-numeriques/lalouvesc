+++
date = 2023-04-06T22:00:00Z
description = "Le 10 avril départ St Romain d'Ay, parcours sur le Val d'Ay"
header = "/media/usva-logo.jpg"
icon = ""
subtitle = ""
title = "La \"COQ'IN\" randonnée VTT ou pédestre"
weight = 1

+++
Lundi de Pâques 10 avril 2023, la « COQ’IN » Randonnée VTT ou pédestre organisée par l’USVA (Union sportive du Val d'Ay) :

* Quatre parcours pédestre : 6-9-14-24km
* Jeu de piste pour les 4-10 ans sur le 6km
* Trois parcours VTT pour tous niveaux : 23km (facile) - 32km - 52km

**Pasta-party non stop ouverte à toutes et tous, gratuite pour les inscrits**.
Départ de la salle des fêtes de St Romain d'Ay entre 8h00 et 15h00.

![](/media/carte-vtt-coqu-in-2023.jpg)