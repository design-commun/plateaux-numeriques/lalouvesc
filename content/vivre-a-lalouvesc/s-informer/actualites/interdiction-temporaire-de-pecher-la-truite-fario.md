+++
date = 2022-08-17T22:00:00Z
description = "Arrêté préfectoral"
header = "/media/truite-fario-wkp.jpg"
icon = ""
subtitle = ""
title = "Interdiction temporaire de pécher la truite fario"
weight = 1

+++
Par arrêté préfectoral :

_Afin d'assurer la pérennité des populations halieutiques dans le département de l'Ardèche, le nombre **de captures autorisées de truite fario est fixé à zéro** jusqu'au 18 septembre 2022 inclus sur l'ensemble des rivières, cours d'eau et plan d'eau du département, et jusqu'au 9 octobre 2022 inclus sur les lacs de Coucouron, Issarlès et Devesset._

Voir [l'arrêté](/media/ap_interdiction-temporaire-peche-fario-ardeche.pdf)