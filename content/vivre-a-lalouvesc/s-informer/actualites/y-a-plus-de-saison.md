+++
date = 2021-09-19T22:00:00Z
description = "Evolution de la pluviométrie à Lalouvesc sur une année"
header = "/media/arc-en-ciel-1-o-de-framond-2021.jpg"
icon = ""
subtitle = "Evolution de la pluviométrie à Lalouvesc sur une année"
title = "Y'a plus de saison !"
weight = 1

+++
Dans le cadre de la fête de la science, Météo France propose le samedi 2 octobre à 14h à la Maison St Régis une présentation de 30 ans de pluviométrie à Lalouvesc.

Voici en avant-première, grâce au père Olivier de Framond, un état sur l'année écoulée.

![](/media/pluie-en-2021.png)

Entre début septembre 2020 et fin août 2021, il est tombé à Lalouvesc 1,14 m d'eau.

On le savait, mais c'est confirmé par les chiffres : les mois de mai et juillet ont été désastreux avec respectivement 243 et 166mm de pluie, tandis que les mois de novembre (36,4mm) et mars (26,3) ont été particulièrement secs.

Il n'est pas rare qu'un ou deux jours seulement fassent l'essentiel de la pluie mensuelle. Ainsi, rappelez-vous, nous avions constaté dans [une actualité](/vivre-a-lalouvesc/s-informer/actualites/combien-d-eau-est-tombee-sur-lalouvesc-lundi-dernier/) qu'il était tombé 172mm d'eau le 10 mai !