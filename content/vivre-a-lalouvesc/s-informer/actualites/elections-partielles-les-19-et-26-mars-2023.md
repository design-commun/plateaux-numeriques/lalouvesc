+++
date = 2023-02-02T23:00:00Z
description = "Arrêté préfectoral"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Elections partielles les 19 et 26 mars 2023"
weight = 1

+++
Comme annoncé des élections partielles sont prévues pour compléter l'équipe municipale.

L'[arrêté préfectoral](/media/ap_07_2023_02_02_00002_lalouvesc.pdf) a été publié. Extraits :

> Article 1 : Les électeurs de la commune de LALOUVESC sont convoqués le **dimanche 19 mars 2023** pour procéder à l'élection de trois conseillers municipaux. Si un deuxième tour de scrutin est nécessaire, il aura lieu le **dimanche 26 mars 2023**.
>
> Article 2 : Les déclarations de candidatures, isolées ou regroupées, sont obligatoires.
> Les candidats ou leurs mandataires devront se présenter à la sous-préfecture de Tournon-sur-Rhône, 16 quai Marc Seguin à TOURNON-SUR-RHÔNE. Il est recommandé de prendre préalablement rendez-vous en téléphonant au 04.75.07.07.70.
>
> **Le dépôt des candidatures sera ouvert aux dates suivantes :**
>
> **Pour le premier tour de scrutin :**
>
> * **du lundi 27 février 2023 au mercredi 1er mars 2023 de 9 h à 11 h 30 et de 13 h 30 à 16 h;**
> * le jeudi 2 mars de 9 h à 11 h 30 et de 14 h à 18 h.
>
> Pour le second tour de scrutin (si nécessaire) :
>
> * le lundi 20 mars 2023 de 9 h à 11 h 30 et de 13 h 30 à 16 h ;
> * le mardi 21 mars 2023 de 9 h à 11 h 30 et de 13 h 30 à 18 h.
>
> N.B. : en cas de second tour, les candidats présents au premier tour n'auront pas à déclarer à nouveau leur candidature. Seuls pourront se présenter au second tour de scrutin, les candidats présents au premier tour, sauf si le nombre de candidats au premier tour est inférieur au nombre de sièges à pourvoir. Une déclaration de candidature sera alors obligatoire, au second tour, pour les candidats qui ne se seront pas présentés au premier tour.

**Un Bulletin d'information spécial sera publié pour tout vous expliquer dans le courant de la semaine.** Il peut être résumé par l'image ci-dessous... N'hésitez pas à interroger un élu pour toute précision.

![](/media/vous-elections-2023.jpg)