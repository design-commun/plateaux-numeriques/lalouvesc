+++
date = 2022-04-14T22:00:00Z
description = "Récupérez le bon de commande."
header = "/media/apel-operation-plants.jpg"
icon = ""
subtitle = ""
title = "Vente de plants et légumes par l'APEL"
weight = 1

+++
L'association des parents d'élèves de l'école St Joseph réitère sa vente de plants de fleurs et de légumes. L'argent récoltée sert à financer les animations des enfants.

Cette année, elle a décidé de travailler local avec un producteur de St André-en-Vivarais : Mickaël Fournel.

Les bons de commande sont disponibles auprès des enfants de l'école. Vous pouvez également vous les procurer dans nos commerces : bar du lac, boulangerie, Vival, pharmacie, boucherie ou les imprimer à partir de ce [fichier](/media/bon-de-commande-plants-apel-2022.pdf).