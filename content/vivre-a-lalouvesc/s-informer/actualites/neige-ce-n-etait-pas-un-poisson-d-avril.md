+++
date = 2022-04-01T22:00:00Z
description = "Il est tombé entre 15cm et 20cm de neige les 1er et 2 avril à Lalouvesc..."
header = "/media/neige-2-avril-2022-1.jpg"
icon = ""
subtitle = ""
title = "Neige : ce n'était pas un poisson d'avril !"
weight = 1

+++
Il est tombé entre 15cm et 20cm de neige les 1er et 2 avril à Lalouvesc...

![](/media/neige-2-avril-2022-3.jpg)

{{<grid>}}{{<bloc>}}

![](/media/neige-2-avril-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/neige-2-avril-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/neige-2-avril-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/neige-2-avril-2022-5.jpg)

{{</bloc>}}{{<grid>}}