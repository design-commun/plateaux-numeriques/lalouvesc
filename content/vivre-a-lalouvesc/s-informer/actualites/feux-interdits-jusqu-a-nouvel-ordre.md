+++
date = 2022-02-01T23:00:00Z
description = "Arrêté préfectoral"
header = ""
icon = ""
subtitle = ""
title = "Feux interdits jusqu'à nouvel ordre"
weight = 1

+++
Par arrêté préfectoral, toute forme de feu est interdite sur l'ensemble du département en raison de la pollution et des risques d'incendie.

[Lire l'arrêté préfectoral](/media/arrete_interdit_emploi_feu_fev2022.pdf)