+++
date = 2022-07-22T22:00:00Z
description = "Comme toujours plein de découvertes sur l'histoire du village"
header = "/media/alauda-2022-2.jpg"
icon = ""
subtitle = ""
title = "Le Bulletin de l'Alauda 2022 est paru"
weight = 1

+++
En vente à l'Office du tourisme et chez le brocanteur. 

Plein de découvertes sur l'histoire de Lalouvesc. Une présentation plus développée dans le prochain Bulletin d'information du mois d'août...

![](/media/alauda-2022.jpg)