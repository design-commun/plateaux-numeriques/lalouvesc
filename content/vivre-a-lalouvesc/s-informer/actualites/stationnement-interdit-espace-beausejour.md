+++
date = 2022-10-26T22:00:00Z
description = "De jeudi 27 oct à mardi 1er nov"
header = "/media/vente-aux-encheres-carrefour-2022-1.jpg"
icon = ""
subtitle = ""
title = "Stationnement interdit espace Beauséjour"
weight = 1

+++
En raison de la vente aux enchères proposée par le Carrefour des arts, le stationnement des voitures est interdit sur la place de l'ancien hôtel Beauséjour du jeudi 27 octobre à 7h jusqu'au mardi 1er novembre 14h.