+++
date = 2021-02-27T23:00:00Z
description = ""
header = "/media/promenades-musicales-lalouvesc-2.jpeg"
icon = ""
subtitle = ""
title = "Artistes des Promenades musicales"
weight = 1

+++
![](/media/promenades-musicales-logo.jpg)

## Les créations présentées par les artistes des Promenades Musicales

**2007** **: Pierre-André ATHANE** : Cantate de la Loire.

**2008** : **Greco CASADESUS** : « Supplément d’âme ». Michel BOSC : « Concertino op 149 pour violon ». P.A. ATHANEE : « Tango Espiègle ».

**2009** : **Greco CASADESUS** : « Le passeur dans la brume », pour saxophone alto et quatuor à cordes.

**Pierre-André ATHANE** : « Sonate pour violon solo » : décidé, dolce espressivo, vif et enlevé. « Quatuor à cordes : Pour la cime des arbres, Pour chaque jour qui passe, Pour le ballet des ombres ».

**Pierre-André ATHANE, Baudîmes JAM, Greco CASADESUS** : « L’hospitalité » pour soprano et cordes (dédiée à Lalouvesc).

**2010** : « Les tableaux d’une exposition » de A.P. MOUSSORGSKI, arrangement de **G. BEYTELMANN** pour accordéon, violon, contrebasse, percussions, et piano.

**2011** : **Pierre André AT**HANE : « Prélude pour violon, violoncelle solo et cordes ».

**2013** : **Albert FASCE** : « Variations sur un thème de Paganini ». **Saeed KHAVARNEJAD** (Iran) : « The Sky Dome » œuvre pour kamancheh et orchestre à cordes et « Aporia nel mondo fisso » avec le compositeur à l’instrument. **Alain PAYETTE** (Canada) : « Quatuor PYRAMIDE » (création française).

**2014** : **Pierre-André ATHANÉ** : « Création pour quatuor à cordes et soprano ».

**2015** : **Pierre-André ATHANÉ** : Duo pour violons : « Un ange passe ». **Alain PAYETTE** : « Ave Maria » création dédiée à Lalouvesc. « Quatuor n°2 » ; « Sonate pour piano et violon » ; « Duos pour violoncelle et piano » ; « Quatuor n° 2 en version pour orchestre à cordes » (créations françaises).

**2016** : **Alain PAYETTE** : « Âme du Christ » et « Douce mémoire » création pour chœur, orchestre, soprane et baryton

**2017** **: Pierre André A**THANÉ : Héritages

**2018** : **Felipe CARRASCO** : « Réconciliation », **Alain PAYETTE** : « Tryptique sacré »

**2019** : **Felipe CARRASCO** : « Cantate » **; Pierre-André ATHANE** « Hommage » pour chœur et orchestre.

**2020** **: Felipe CARRASCO** : « _Entregas breves de amor y desarraigo » ;_ **Patrick BUSEUIL** : _« Little variantes on Spanish Dance, pour flûte et violon ». **Patrick BUSSEUIL__ : Un jardin pour Théodore, pour récitant, soprano, flûte et violon (création**)**_

## Les artistes classiques des Promenades Musicales depuis 2015

Cinq artistes étrangers internationaux (**Alain PAYETTE**, Canada ; **Saeed KHAVARNEJAD**, Iran ; **Manuel CAMPOS** (équateur), **Felipe CARRASCO** (équateur), **Adrian MACDONNEL**, USA),

• **Guillaume ARRIGNON** (contrebasse) : 1er prix de contrebasse de la ville de Paris. Professeur au CNSMD d’Asnières-sur-Seine. Soliste à l'Orchestre Symphonique de Mulhouse. Membre des ensembles de Philippe Jaroussky "Artaserse" et MATHEUS.

**Michel ARRIGNON** (clarinette) Premiers prix de clarinette et de musique de chambre du CNSMD de Paris. Soliste de l’orchestre de l’Opéra de Paris. Professeur au CNSMD de Paris, essayeur chez Buffet Crampon

• **Frédéric AUDIBERT** (violoncelle) Soliste du Philharmonique E. Krivine et du Musikfestspiele de Dresde

• **Jean-Philippe BARDON** (alto) Premier prix de Musique de chambre et d’alto du CRR de Paris ; Alto solo de l’Orchestre symphonique d'Orléans. Professeur titulaire du CA au Conservatoire d'Orléans

• **Elena BONNAY** (Piano) : Diplômée de l’Académie de Musique de Moscou. Premier prix du Concours International de Voronej, Pianiste-chef de chant pour le Ballet, à l’Opéra de Paris

• **Max BONNAY** (Bandonéon, Accordéon) : Diplômé de l'Institut GNESSIN de Moscou. Quatre premiers Prix des concours internationaux. Professeur au CNSM de Paris

• **Michaël BONNAY** (violon) chef d’attaque à l’orchestre Pasdeloup, orchestre de l’Opéra de Paris, membre de Colori Tempi,

• **Jean-Baptiste BRUNIER (alto)** soliste de l’orchestre de Radio France, Professeur au conservatoire du XXème arrondissement à Paris et de plusieurs académies nationales et internationales.

• **Patrick BUSSEUIL (accordéon)** Compositeur et Professeur certifié (CA) à l’ENM de Romans.

• **Simon CHEMBRI (guitare)** Licencié de la Royal Academy of Music de Londres, Professeur au conservatoire de Sevran, premier prix à l’unanimité au C de Paris.

• **Claire CORNELOUP** **(Clavecin**) 1er Prix de clavecin du CNSMD de Paris

• **Émilie GASTAUD (harpe)** : 1er Prix du CNSMD de Paris, soliste à l’orchestre de Radio France.

• **Michel GASTAUD** (Percussions) : 1er Prix du CNSMD de Paris. Soliste de l’orchestre National de l’Opéra de Paris. Professeur au Conservatoire du XII arrondissement de Paris.

• **Léa GRAMUSSET** (violoncelle) : Soliste de l’Orchestre des jeunes de l’Orchestre National de Lyon

• **Laurent HACQUARD** (hautbois) : 1er Prix de hautbois et de musique de chambre du CNSMD de Paris. Ex-soliste des Concerts Pasdeloup et de la Comédie-Française. Professeur des Conservatoires Gabriel-Fauré et Maurice-Ravel de la Ville de Paris.

• **Gildas HARNOIS** (chef d’orchestre) : Chef de la Musique des gardiens de la paix. Professeur de musique de chambre et de direction d’orchestre au Conservatoire de Tours de 2005 à 2014.

• **Amandine LEFEVRE** (violoncelle) : Premier prix de musique de chambre du CNSM de Lyon. Professeur titulaire du CA au Conservatoire de l'Isère, Membre de l'orchestre des pays de Savoie.

• **Gilles LEFEVRE** (violon) : 1er prix de violon et 1er prix de musique de chambre du CNSMD de Paris. Professeur titulaire du CA. Il a enseigné dans les conservatoires de Paris, Versailles, Chartres, Tour, Évreux…Violon solo de l’orchestre symphonique d’Orléans, Directeur artistique des Promenades Musicales.

• **Gwendeline LUMARET** (violoncelle) : Concertiste. Professeur titulaire du CA au conservatoire de Bourges. Violoncelliste de l'Ensemble "Appassionata".

• **Lys NORDET** (Soprano) : 1er prix de chant et 1ère médaille de théâtre musical et d’art lyrique du conservatoire du Xe à Paris. Professeur de technique vocale et de chant au conservatoire Hector BERLIOZ de PARIS. Chef de chant aux CHORALIES DE VAISON LA ROMAINE

• **Arnaud SANS (guitare)** Diplômé de l’école normale de musique de Paris, Professeur titulaire du CA au conservatoire de Cabriès. Directeur de l’ensemble COPLA.

• **Guillaume PERNES (saxophone)** Premier prix du CNSMD de Paris, Concertiste, Professeur titulaire du CA, Saxophone Solo des orchestres de la Garde Républicaine.

• **Serge SOUFFLARD** (alto) : Premier Prix du CNSMD de Paris. Professeur aux conservatoires d'Évreux et de Paris 7e. Alto solo de l’ensemble Orchestral de Paris.

## Artistes de jazz et de variétés

Nous avons reçu 45 musiciens de jazz et artistes de variétés.

Parmi les musiciens et groupes de jazz présentés on peut citer :

• L’orchestre de **Jean-François BONNEL**, Prix Sidney Bechet de l'Académie du jazz, Professeur au Conservatoire d’Aix en Provence

• L’orchestre de **Jean DIONISI**, 1er prix du Concours international de Jazz New Orléans de St. Raphaël.

• L’harmoniciste américain **William GALLISON**. Diplômé du College of Music de Boston et de la Wesleyan University du Connecticut.

• Le trio du pianiste compositeur **Karim MAURICE**, Diplômé du CNSMD de Lyon. Professeur de musiques actuelles au Conservatoire de Grenoble.

• Les **Voleurs de Swing**. Un jeune groupe enthousiasmant au succès grandissant et qui est devenu un groupe fétiche des Promenades Musicales.

• La virtuose Caroline **BUGALA** Disciple de Didier Lockwood/ Médaille d'or du CNR de Lyon 2001/ Gagnante du concours national de musique, à Lyon en 1994, avec mention d'honneur.