+++
date = 2022-11-24T23:00:00Z
description = "Auberge de St Félicien"
header = "/media/cafe-des-aidants-2022-3.jpg"
icon = ""
subtitle = ""
title = "Prochain café des aidants le 3 décembre"
weight = 1

+++
Le prochain café des aidants de St Félicien aura lieu samedi 3 décembre 2022 de 10h à 11h30 à l'Auberge de St Félicien sur le thème « Être aidant c'est aussi du positif ! »  
   
Les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).  
   
Renseignements auprès de Carole Guilloux : cguilloux@fede07.admr.org ou 06 81 50 19 26