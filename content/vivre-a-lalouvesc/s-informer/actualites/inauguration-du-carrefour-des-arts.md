+++
date = 2021-07-03T22:00:00Z
description = "Quelques photos, mais une visite s'impose. Le Carrefour des Arts est devenu un rendez-vous incontournable de l'art contemporain en Ardèche."
header = "/media/inauguration-carrefour-des-arts-2021-7.jpg"
icon = ""
subtitle = ""
title = "Inauguration du Carrefour des Arts"
weight = 1

+++
La saison 2021 est lancée. 

Les "émerveillés" ont découvert les trésors de Lalouvesc grâce à l'[Office du tourisme](https://www.facebook.com/OTValDAy/). Les enfants de l'école Saint Joseph ont découvert, les yeux écarquillés, en avant-première les nouvelles salles du Carrefour des arts.

Et les élus du territoires ont répondu présents à l'invitation pour l'inauguration de l'exposition. Le premier week end a accueilli plus de 300 visiteurs. "2021, un très bon cru. Les oeuvres exposées sont remarquables" a conclu l'un d'eux.

Ci-dessous quelques images pour vous donner envie d'aller les admirer.

{{<grid>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-8.jpg)

Suhail Shaikh

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-14.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-17.jpg)

Shasha Shaikh

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-15.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-16.jpg)

Xavier Carrère

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-12.jpg)

Hervé Tharel

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-2.jpg)

Michel Béatrix

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-6.jpg)

Martine Jaquemet

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/copie-de-20210703_112053.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/inauguration-carrefour-des-arts-2021-13.jpg)

Claude Grégoire

{{</bloc>}} {{</grid>}}