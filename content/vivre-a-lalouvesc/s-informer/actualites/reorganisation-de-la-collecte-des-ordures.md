+++
date = 2022-09-20T22:00:00Z
description = "Centralisation des bacs de tri"
header = "/media/sytrad-tri.jpg"
icon = ""
subtitle = ""
title = "Réorganisation de la collecte des ordures"
weight = 1

+++
Progressivement la nouvelle politique du SYTRAD se met en place. Cela commence par la suppression des bacs de 770 litres qui récoltaient les ordures ménagères dans les hameaux et les différents quartiers du village au profit de six points de collecte centralisés de tri :

* Place du Lac,
* Chemin Grosjean,
* Maison de retraite,
* Camping,
* Place des Trois Pigeons,
* Aire du Grand Lieu.

De nouvelles colonnes de tri devraient y être placées courant octobre. Le tri dans les bacs centralisés était déjà effectif. L'objectif maintenant est de le rendre systématique. Le mélange des ordures dans les bacs gris est en réalité très coûteux et écologiquement néfaste, comme le montre l'image ci-dessous.

![](/media/sytrad-tri.jpg)