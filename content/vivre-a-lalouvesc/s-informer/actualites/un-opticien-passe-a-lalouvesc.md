+++
date = 2023-01-04T23:00:00Z
description = "Tests, choix, ajustage et réparation de lunettes"
header = "/media/lunettes.png"
icon = ""
subtitle = ""
title = "Un opticien passe à Lalouvesc"
weight = 1

+++
Un camion optique ambulant du magasin "Illusion d'optique" sera présent à Lalouvesc le 30 janvier à partir de 9h30, café & croissants offerts.

Il proposera des :

* Tests de vue
* Choix de lunettes
* Ajustages et réparations de lunettes

Vous pouvez déjà appeler au 09 67 00 64 96 pour prendre rendez-vous ou avoir des renseignements.

![](/media/affiche-camion-lalouvesc.jpg)