+++
date = 2021-11-02T23:00:00Z
description = ""
header = "/media/19-mars-2021-monument-aux-morts.jpg"
icon = ""
subtitle = "Céréminies à la Basilique et au Monument aux morts, collecte des Bleuets"
title = "11 novembre : cérémonie et collecte"
weight = 1

+++
La cérémonie du 11 novembre à Lalouvesc, ouverte à tous les Louvetous, se déroulera en deux parties :

* le Sanctuaire vous donne rendez-vous à 10h30 à la crypte de la basilique, suivi d'un office religieux en mémoire des victimes de la guerre.
* Un défilé partira ensuite à 12h de la Basilique jusqu'au Monument aux morts pour la cérémonie du souvenir.

### Collecte des Bleuets

L’œuvre nationale du Bleuet de France lance sa nouvelle campagne de dons partout en France. Des milliers de bénévoles appelleront à la générosité des passants sur la voie publique.

Le Bleuet de France, c’est avant tout une mission de solidarité pour le soutien moral et financier des combattants d’hier et d’aujourd’hui, des victimes de guerres, des veuves et orphelins de guerres, des pupilles de la Nation et des victimes d'actes de terrorisme. Il concourt aussi à valoriser des actions afin de transmettre la mémoire vers les plus jeunes. Ce lien intergénérationnel que porte le Bleuet de France depuis un siècle est une nécessité citoyenne.

Ainsi, le Bleuet de France vient en aide aux militaires blessés en opération extérieure. Il encourage leur reconstruction physique et morale en partenariat avec les hôpitaux militaires ou les institutions sportives de blessés (achat de matériel médical et d’équipements sportifs adaptés par exemple). Il aide et soutient aussi sur le même principe les victimes de terrorisme. Il accompagne financièrement les pupilles de la Nation jusqu’à l’âge de 21 ans, pour les aider dans leurs études, un projet professionnel ou personnel.

Vous pouvez aussi acheter sur la boutique en ligne : [https://lnkd.in/d686XK5](https://lnkd.in/d686XK5 "https://lnkd.in/d686XK5") ou faire un don : [https://lnkd.in/dNtnSXh](https://lnkd.in/dNtnSXh "https://lnkd.in/dNtnSXh")