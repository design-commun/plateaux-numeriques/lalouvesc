+++
date = 2021-11-30T23:00:00Z
description = "Les sapins arrivent, le calendrier de l'Avent est installé"
header = "/media/decorations-noel-2021-sapins.jpg"
icon = ""
subtitle = ""
title = "Premières installations des décorations de Noël"
weight = 1

+++
Les employés municipaux sont allés couper les premiers sapins sur le terrain exploité par M. René Sabatier, qui a autorisé l'abattage des sapins, car ils sont malades. Les sapins seront progressivement déployés dans le village.

![](/media/decorations-noel-2021-avent.jpg)

Et, nous sommes le premier décembre. Il était temps d’installer le calendrier de l'Avent préparé par les enfants de l'école St Joseph devant la mairie. Le premier dessin a été affiché. Il y en aura un chaque jour, dessiné par un enfant différent. Venez les admirer !

Le week end prochain, le Comité des fêtes placera les décorations qu'il a préparées... si la météo le permet. Les illuminations viendront un peu plus tard.