+++
date = 2021-12-06T23:00:00Z
description = "Compte-rendu du dernier Conseil municipal de l'année"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte-rendu du Conseil municipal du 1er décembre"
weight = 1

+++
Le [compte-rendu du Conseil municipal du 1er décembre 2021](/media/2021-7-conseil-municipal-lalouvesc-annexe-dm4.pdf) est consultable sur le site. Il est accompagné d'une [annexe](/media/2021-7-conseil-municipal-lalouvesc-annexe-dm4.pdf) détaillant la décision modificative budgétaire n°4.

Rappel : tous les comptes-rendus des Conseils depuis 2008 sont accessibles en permanence sur [cette page](/mairie-demarches/vie-municipale/comptes-rendus/comptes-rendus-des-conseils-municipaux/).