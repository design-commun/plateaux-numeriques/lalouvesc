+++
date = 2022-04-23T22:00:00Z
description = "Lalouvesc vote à 81%"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Résultat du second tour de la présidentielle à Lalouvesc"
weight = 1

+++
## Résultat du second tour de la présidentielle de 2022

| Liste des candidats | Voix | % Inscrits | % Exprimés |
| --- | --- | --- | --- |
| M. Emmanuel MACRON | 127 | 37,24 | 51,00 |
| Mme Marine LE PEN | 122 | 35,78 | 48,99 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 341 |  |  |
| Abstentions | 64 | 18,77 |  |
| Votants | 277 | 81,23 |  |
| Blancs | 21 | 6,16 | 8,43 |
| Nuls | 7 | 2,05 | 2,81 |
| Exprimés | 249 | 73,02 | 89,89 |

## Rappel du résultat du second tour de la présidentielle de 2017

| Liste des candidats | Voix | % Inscrits | % Exprimés |
| --- | --- | --- | --- |
| M. Emmanuel MACRON | 140 | 40,94 | 56,68 |
| Mme Marine LE PEN | 107 | 31,29 | 43,32 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 342 |  |  |
| Abstentions | 66 | 19,30 |  |
| Votants | 276 | 80,70 |  |
| Blancs | 24 | 7,02 | 8,70 |
| Nuls | 5 | 1,46 | 1,81 |
| Exprimés | 247 | 72,22 | 89,49 |

### [Rappel résultats des premiers tours de 2022 et 20T7](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/resultats-premier-tour-de-l-election-presidentielle-a-lalouvesc/)