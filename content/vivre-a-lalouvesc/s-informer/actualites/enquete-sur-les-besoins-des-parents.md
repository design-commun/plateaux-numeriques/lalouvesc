+++
date = 2022-11-08T23:00:00Z
description = "Activités des enfants de 0 à 17 ans"
header = "/media/relais-petite-enfance.png"
icon = ""
subtitle = ""
title = "Enquête sur les besoins des parents"
weight = 1

+++
L'ACEPP ADeHL, fédération qui gère la crèche, le Relais petite enfance et le Parentibulle, travaille en partenariat avec la CAF et la CCVA sur un projet de services aux familles pour 2023 pour les enfants de 0 à 17 ans. 

Afin de mieux connaître les besoins des parents, l'association a élaboré un [questionnaire](https://docs.google.com/forms/d/e/1FAIpQLScPfJQULBNxXPoYdKmUXmgRYpRcgco4vWGJ1rlEVcViI3UMGQ/viewform). Il est important que les parents louvetous prennent le temps de le remplir pour que la situation de notre village, éloignée du centre de loisirs soit bien prise en compte.