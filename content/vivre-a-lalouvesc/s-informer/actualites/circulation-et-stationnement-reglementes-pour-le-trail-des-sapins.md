+++
date = 2022-10-18T22:00:00Z
description = "Du samedi 29 octobre 14h au dimanche 30 à 3h"
header = "/media/tee-shirt-trail-2022.jpg"
icon = ""
subtitle = ""
title = "Circulation et stationnement réglementés pour le Trail des Sapins"
weight = 1

+++
#### Interdictions

L'accès (stationnement et circulation) sera interdit (hors véhicule de secours) le samedi 29 octobre à partir de 14 heures jusqu'au dimanche 30 octobre à 3 heures :

* de l'intersection de la place du Lac avec le chemin de l'Hermuzière jusqu'au café du Lac'rêperie,
* sur toute la longueur du chemin de l'Hermuzière,
* parking du camping municipal,
* route de Bobignieux (en dessous du centre d'animation communal) jusqu'à l'intersection du chemin Grosjean,
* stationnement interdit sur le parking devant les toilettes de la fontaine St Régis

#### Parkings

Des parkings sont prévus dans le village : place du Lac, Grand Lieu, terrain de football (réservé pour 3 cars), chemin Grosjean, place des Trois Pigeons.