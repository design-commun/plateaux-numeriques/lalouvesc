+++
date = 2022-09-21T22:00:00Z
description = "Le programme"
header = "/media/carrefour-des-arts-s-rousse-2.jpeg"
icon = ""
subtitle = ""
title = "Fête de la science à Lalouvesc"
weight = 1

+++
Comme chaque année, l'Office du tourisme propose une série d'animation sur l'ensemble du Val d'Ay pour la fête de la science. Voici celles proposées sur la Commune :

#### Samedi 24 septembre

**14h : Balade plantes comestibles et autres trésors de la nature.** De plantes culinaires en arbres bienfaisants, Françoise Kunstmann est passionnée de la cueillette à l’assiette. Une exposition sur la Biodiversité de Natura 2000 sera présentée. A partir de 8 ans.  
Rdv devant la Maison St Régis

**16h : D’encres et d’eau : rencontre avec Serge Roussé.** Serge Roussé passionné par les rivières affectionne dans ses œuvres la matière eau dans son état solide. Couleurs et lumières illuminent son exposition in situ mise en place par le Carrefour des Arts en partenariat avec la Mairie. A partir de 10 ans.  
Espace Beauséjour

#### Samedi 1er octobre

**14h : Atelier créatif avec Marie-Christine Brunel.** Marie-Christine Brunel, artiste peintre propose un atelier créatif sur le thème des champignons peints, décorés au feutres ou par collages. Elle présentera ses créations ensuite dans son atelier au village. A partir de 5 ans (atelier limité à 10 personnes ou créations).  
Maison St Régis

Pour toutes les animations, la réservation est obligatoire : 04 75 67 84 20 [contact@valday-ardeche.com](mailto:contact@valday-ardeche.com)

Le programme complet est accessible sur la page [FB de l'Office du tourisme](https://www.facebook.com/OTValDAy/).