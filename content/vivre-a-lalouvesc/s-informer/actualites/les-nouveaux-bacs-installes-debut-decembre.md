+++
date = 2022-11-25T23:00:00Z
description = "Prenez les bons réflexes de tri"
header = "/media/bac-a-ordure-2022-4.jpg"
icon = ""
subtitle = ""
title = "Les nouveaux bacs installés début décembre"
weight = 1

+++
Les colonnes d'ordures ménagères seront mises en place cette semaine avec l'enlèvement des bacs existants sur les six points de collecte. 

* Place du Lac,
* Chemin Grosjean,
* Maison de retraite,
* Camping,
* Place des Trois Pigeons,
* Aire du Grand Lieu.

Le tri des ordures est indispensable, papiers, emballages, verres tout doit être trié et déposé dans le bac dédié. Tout est expliqué[ ici.](https://www.lalouvesc.fr/mairie-demarches/demarches/dechets-et-voirie/) Il y aura aussi trois colonnes réservées aux cartons bruns.

![](/media/bacs-de-tri-ordures-2022.png)
**Attention, dès lundi 5 décembre plus aucun bac particulier ne sera ramassé par le Sytrad.**
Seuls les six points de collecte seront opérationnels. Nous comptons sur votre participation pour cette nouvelle organisation.