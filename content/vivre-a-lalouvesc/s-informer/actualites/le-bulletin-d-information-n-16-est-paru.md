+++
date = 2021-10-30T22:00:00Z
description = "16 - novembre 2021 - Respire"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "16 - novembre 2021 - Respire"
title = "Le Bulletin d'information n°16 est paru"
weight = 1

+++
Ce mois d’octobre a été généreux pour les chercheurs de champignons, mais plus sévère pour les coureurs trempés du Trail des Sapins. On trouvera aussi dans ce numéro le compte-rendu d’une étude sur la pureté de l’air dans la région et, comme toujours, bien d’autres informations, sans oublier la suite du feuilleton des enfants de l’école St Joseph.

Lire le [Bulletin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/)

### Entrée directe par le Sommaire

* [Le mot du Maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#le-mot-du-maire)
* [Actualités de la Mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#actualités-de-la-mairie)
  * [Un nouvel employé municipal](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#un-nouvel-employé-municipal)
  * [Chasse-neige](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#chasse-neige)
  * [Rencontre avec les commerçants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#rencontre-avec-les-commerçants)
  * [Audit sur l’infrastructure des télécommunications (fibre et 4G)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#audit-sur-linfrastructure-des-télécommunications-fibre-et-4g)
* [Zoom](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#zoom)
  * [Les champignons… nature, plein air, environnement sauvegardé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#les-champignons-nature-plein-air-environnement-sauvegardé)
  * [Lalouvesc un air pur… sauf pour l’ozone](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#lalouvesc-un-air-pur-sauf-pour-lozone)
  * [Trail des Sapins : ils ont répondu présents !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#trail-des-sapins--ils-ont-répondu-présents-)
* [Suivi](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#suivi)
  * [Départ du père Olivier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#départ-du-père-olivier)
  * [Rallye Exbrayat](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#rallye-exbrayat)
  * [La saison à l’Abri du pèlerin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#la-saison-à-labri-du-pèlerin)
  * [Club des 2 clochers : AG extraordinaire et après-midi récréative](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#club-des-2-clochers--ag-extraordinaire-et-après-midi-récréative)
  * [Assises pour le développement de la vie associative en Ardèche](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#assises-pour-le-développement-de-la-vie-associative-en-ardèche)
  * [Naissance](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#naissance)
  * [Dans l’actualité le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#dans-lactualité-le-mois-dernier)
* [Feuilleton : Les quatre éléments](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#feuilleton--les-quatre-éléments)
  * [Chapitre 2 : La pierre TERRE](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/16-respire/#chapitre-2--la-pierre-terre)