+++
date = 2021-03-10T23:00:00Z
description = "Les 18 et 19 juin les cyclistes pourront bénéficier de toutes les animations habituelles sur l’Ardéchoise classique et l’Ardèche Verte."
header = "/media/ardechoise-6.jpg"
icon = ""
subtitle = ""
title = "L'Ardéchoise, malgré tout !"
weight = 1

+++
Nous avons reçu quelques nouvelles des responsables de l'Ardéchoise :

_L’incertitude qui plane actuellement, nous a en effet poussé à renoncer à l’Ardéchoise en plusieurs jours et aux excursions hors du département de l’Ardèche. Cette année nous ne pourrons pas organiser les circuits avec hébergements, repas et transports de bagages. En revanche les 18 et 19 juin les cyclistes pourront bénéficier de toutes les animations habituelles sur l’Ardéchoise classique et l’Ardèche Verte selon des parcours cent pour cent « ardéchoise » (afin de ne dépendre que d’une seule autorisation préfectorale)._

* _le vendredi 18 juin, nous organiserons l’Ardèche Verte habituellement programmée le mercredi au Nord de Saint-Félicien. Les cyclistes qui le souhaitent pourront également effectuer une boucle au cœur du pays de la châtaigne et tous se retrouveront à l’arrivée à Saint-Félicien._
* _Le samedi 19 juin, nous proposerons les six parcours classiques à réaliser en formule cyclotouriste ou cyclosportive avec la possibilité de choisir son circuit en cours de route._

Vous pouvez télécharger la [brochure complète](https://www.ardechoise.com/Actualites/Les-actualites-de-l-Ardechoise/Nouvelle-brochure-20212).

et aussi [visionner une vidéo](https://youtu.be/Tu6r2SUxy1w) retraçant les meilleurs moments de la course.