+++
aliases = ["/actualites"]
date = ""
description = "Tenez-vous informé·e des dernières nouvelles de votre village"
header = "/media/arc-en-ciel-2.jpg"
icon = "🥁"
layout = "news"
menuSection = false
subtitle = ""
title = "Actualités"
weight = 1

+++
Abonnez-vous et recevez le lundi les actualités de la semaine précédente en envoyant un message à sympa@numerian.fr avec dans l'objet la formule : **subscribe actualites-lalouvesc**.

D'autres actualités du village paraissent sur les[ réseaux sociaux et dans la presse](/vivre-a-lalouvesc/s-informer/reseaux-sociaux/).