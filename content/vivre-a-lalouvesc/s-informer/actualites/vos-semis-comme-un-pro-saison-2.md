+++
date = 2022-04-14T22:00:00Z
description = "Proposé par les Jardins du Haut Vivarais"
header = "/media/jardins-du-haut-vivarais-2022.jpg"
icon = ""
subtitle = ""
title = "Vos semis comme un Pro, saison 2"
weight = 1

+++
L 'association " les Jardins du Haut Vivarais" vous propose sa seconde édition :  "Apprenez à faire vos semis comme un Pro".

Au programme, tous les bons gestes pour un semi réussi !

La formation dure une demi-journée et s’adresse à tous ceux qui se posent des questions sur la réalisation des semis, constituée d’une partie théorique, (différentes méthodes seront abordées) et d’une partie pratique.

Vous repartirez avec vos semis, prêts à être bichonnés avant de vous régaler dans l’été de variétés que vous ne connaissez peut-être pas.

Tout le matériel est fourni : cellules, graines, terreau, matériel pédagogique.

La matinée se terminera par un joyeux repas partagé !

Dates :

* 30 avril à Pailharès
* 14 mai à Lalouvesc
* 28 mai au Mont Besset à Lalouvesc

Prix : 15€

Inscription : Monika, [malinka140683@gmail.com](mailto:malinka140683@gmail.com), 06 61 95 55 06.

Au plaisir de jardiner ensemble.