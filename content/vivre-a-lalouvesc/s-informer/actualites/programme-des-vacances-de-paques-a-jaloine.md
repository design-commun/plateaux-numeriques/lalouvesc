+++
date = 2023-03-16T23:00:00Z
description = "Pour enfants et ados"
header = "/media/centre-de-loisir-jaloine-2921.jpg"
icon = ""
subtitle = ""
title = "Programme des vacances de Pâques à Jaloine"
weight = 1

+++
Envie d’un voyage au cœur des cités Mayas, rejoins-nous au Centre de loisirs de Jaloine.  
L'équipe d'animation vous réserve quelques surprises …

Le centre de loisirs est réservé aux enfants de 3 à 13 ans, désolé pour les parents qui se voyaient déjà en vacances avec nous.

[Fiche d'inscription](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/fiche-inscriptions-vacances-printemps-jaloine.pdf) pour les cités d’or du 15 au 21 mars.  
Pour plus d’information contacter nous :  
07 66 49 07 90  
[cdl.jaloine@gmail.com](mailto:cdl.jaloine@gmail.com)

![](/media/jaloine-paques-2023.jpg)![](/media/jaloine-paques-2023-2.jpg)

PS : On ne vous répond pas tout de suite, pas de panique, dès que nous pourrons nous vous apporterons une réponse.

Hors vacances scolaires, nous répondons au mail les mardis, mercredis et jeudis dès que nous pouvons.

Nous mettons tout en œuvre pour proposer aux enfants le meilleur accueil qu'il soit, cela nous demande du temps, merci de votre compréhension.