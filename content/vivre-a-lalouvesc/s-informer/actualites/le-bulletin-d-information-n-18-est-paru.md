+++
date = 2021-12-30T23:00:00Z
description = "18 - janvier 2022 - Une année-et-demi bien remplie"
header = "/media/entete-bulletin.jpg"
icon = ""
subtitle = "18 - janvier 2022 - Une année-et-demi bien remplie"
title = "Le Bulletin d'information n°18 est paru"
weight = 1

+++
Le titre du premier numéro du Bulletin d'information de Lalouvesc, celui de juillet 2020, était : _Lalouvesc "un village résilient"._ C'était un souhait émis par un conseiller nouvellement élu. Après une année-et-demi, peut-on dire que le souhait a été exaucé ?

Sans doute pas encore, la période a été marquée par les vagues successives de la pandémie et le village n'a pas été épargné. Et pourtant, il s'est embelli, des travaux importants ont été lancés, plusieurs manifestations ont pu se tenir, tout cela grâce à l'engagement et la solidarité de ses habitants.

Ce Bulletin fait le point sur une année-et-demi bien remplie, vous pourrez juger par vous-mêmes. Il comprend aussi, bien sûr comme toujours, plein d'autres informations...

Lire le [Bulletin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/)

### Entrée directe par le Sommaire

* [Retour sur une année-et-demi, bien remplie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#retour-sur-une-année-et-demi-bien-remplie)
  * [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#mot-du-maire)
  * [Ce que nous avions annoncé, ce qui a été réalisé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#ce-que-nous-avions-annoncé-ce-qui-a-été-réalisé)
* [Actualités de la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#actualités-de-la-mairie)
  * [Audit sur la connexion dans le village](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#audit-sur-la-connexion-dans-le-village)
  * [Le retour des Babets](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#le-retour-des-babets)
  * [Rencontre avec les sœurs du Cénacle](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#rencontre-avec-les-sœurs-du-cénacle)
  * [Visites de la Commission de sécurité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#visites-de-la-commission-de-sécurité)
* [Zoom](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#zoom)
  * [Vœux pour 2022](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#vœux-pour-2022)
  * [Almanach 2021](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#almanach-2021)
  * [Le Comité des fêtes n’a rien lâché !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#le-comité-des-fêtes-na-rien-lâché-)
* [Suivi](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#suivi)
  * [Le Téléthon au club des deux clochers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#le-téléthon-au-club-des-deux-clochers)
  * [Retour sur le conseil municipal par une Louvetonne](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#retour-sur-le-conseil-municipal-par-une-louvetonne)
  * [Population](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#population)
  * [Prendre soin à la fois de notre santé et de l’environnement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#prendre-soin-à-la-fois-de-notre-santé-et-de-lenvironnement)
  * [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#dans-lactualité-le-mois-dernier)
* [Feuilleton : _Les quatre éléments_](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#feuilleton--_les-quatre-éléments_)
  * [Résumé des épisodes précédents](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#résumé-des-épisodes-précédents)
  * [Chapitre 4 : Le message codé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/18-une-annee-et-demi-bien-remplie/#chapitre-4--le-message-codé)