+++
date = 2021-03-16T23:00:00Z
description = "Aline passe à la radio, Kévin a reçu son diplôme"
header = "/media/remise-de-diplome-3.JPG"
icon = ""
subtitle = "Aline passe à la radio, Kévin a reçu son diplôme"
title = "Les jeunes de Lalouvesc à l'honneur"
weight = 1

+++
## Aline Delhomme, les circuits courts sur France Bleu Drôme-Ardèche

Jolie présentation de la ferme "les Salers du Crouzet" et de sa nouvelle orientation vers les veaux élevés sous la mère et les circuits courts par Aline Delhomme qui a repris l'exploitation de la ferme de son père sur France Bleu Drôme-Ardèche.

![](/media/ferme-crouzet-3.JPG)

**Du courage et de l'audace pour Aline et du bon et du beau pour Lalouvesc et les environs !**

C'était mardi à 7h55 [à lire ou réécouter ici](https://www.francebleu.fr/emissions/circuits-courts-en-drome-ardeche/drome-ardeche/a-lalouvesc-les-salers-du-crouzet-ne-sont-pas-frileuses?fbclid=IwAR1BQSOFLkbjouTYUGUJQsJMDyu-H5WKg03l399gxj0NhW4xv7hZhQgZfxQ)

La page [Facebook des Salers du Crouzet](https://www.facebook.com/lessalersducrouzet/)

## Kévin Astier, diplômé

Cette année, les remises de diplômes au sein de l'université Jean Monnet de Saint-Étienne n'ont pas pu se dérouler dans les conditions habituelles à cause du Covid 19. Les diplômes universitaires de nombreux jeunes ont alors été envoyés dans la mairie de leur lieu de résidence.  
Monsieur le maire Jacques Burriez a donc pu remettre en main propre le diplôme de Licence de Géographie et Aménagement (Parcours Territoire - Environnement) à Kévin Astier, jeune habitant du village.

**Félicitations à lui pour cette réussite !**

Durant son cursus il a pu acquérir de nombreuses connaissances et découvrir de nouveaux outils numériques, notamment les SIG "Systèmes d'Information Géographiques". Passionné par l'aménagement du territoire et les SIG, il souhaite intégrer Polytech Nice afin de poursuivre son cursus en apprentissage dans une formation d'ingénieur Génie de l'Eau.

![](/media/remise-de-diplome-4.JPG)