+++
date = 2022-04-02T22:00:00Z
description = "70 voitures ont bravé une météo hostile"
header = "/media/ronde-louvetonne-2022-3.jpg"
icon = ""
subtitle = ""
title = "Un paysage enneigé pour la Ronde Louvetonne"
weight = 1

+++
70 voitures, soit plus de la moitié des inscrites, étaient présentes pour la Ronde Louvetonne 2022, sous la neige, la température glaciale et le vent. Après les annulations de 2020 et 2021 pour cause sanitaire, le Comité des fêtes a tenu bon malgré une météo exécrable. Les amoureux de vieilles voitures ont pu se retrouver à Lalouvesc.  
Tout le monde s'est réchauffé après la balade au CAC pour manger et même danser...

{{<grid>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-10.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-8.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne-2022-7.jpg)

{{</bloc>}}{{</grid>}}