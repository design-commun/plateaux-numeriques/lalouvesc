+++
date = 2021-12-24T12:00:00Z
description = "Joyeux Noël à toutes et tous ! Un cadeau de Louvetous globe-trotters..."
header = "/media/noel-guatemala-2021-10.jpg"
icon = ""
subtitle = ""
title = "Un Noël au Guatemala"
weight = 1

+++
## Joyeux Noël à toutes et tous !

Une surprise pour ce Noël 2021 : découvrons comment se déroule la fête de Noël au Guatemala grâce nos envoyés spéciaux louvetous, Brigitte et Michel Bober.

> Le Guatemala est un petit pays situé en Amérique centrale à la frontière sud du Mexique, bordé par le Honduras et le Belize. Nous sommes à Flores dans la département du Petén, charmante petite ville au nord du pays, posée sur le lac Petén Itzá, dans un écrin de verdure.
>
> Les fêtes de Noël sont traditionnellement importantes en Amérique du Sud.
>
> Au Guatemala plus de 50% de la population est indigène, descendante des Mayas. Une vingtaine de dialectes propres à chaque ethnie sont toujours parlés dans les villages et nombre d'entre elles ne parlent pas l'español, langue officielle du pays.
>
> Les cérémonies de Noël commencent une dizaine de jours avant le 25 décembre, "les Posadas" traduisez "les auberges", en hommage à Joseph et à la Vierge Marie en quête d'un abri pour la naissance de l'enfant Jésus.
>
> Chaque jour des processions formées d'adultes et d'enfants ont lieu dans les rues avec des chants accompagnés de feux d'artifices et de tambourins. Tout le monde se regroupent enfin devant l'entrée d'une maison jusqu'à ce que la porte s'ouvre. On y entre pour déposer la Vierge, des bouquets de fleurs, chanter et prier.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PSphI1pxziM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

> Chaque soir ce rituel recommence jusqu'au soir 24 décembre.
>
> De même chaque foyer prépare sa maison et la décore. Le sapin ne peut être qu'artificiel, mais c'est une mode assez récente. Il n'est pas rare de voir des árboles locaux parés de décorations et illuminés de mille feux de toutes les couleurs.
>
> ![](/media/noel-guatemala-2021-3.jpg)
>
> La messe de la Navidad a souvent lieu à 20h, ici la nuit tombe tôt (17h45) . Elle dure deux heures pendant lesquelles on entend de nombreux chants, accompagnés de musique. Il faut dire que la musique est aussi une religion, elle est partout, tout le temps, l'ambiance est toujours festive.
>
> Après la cérémonie religieuse, il est temps de rentrer pour réveillonner en famille. Les tamales, spécialités à base de farine de maïs cuites au feu de bois et garnies de poulet, de porc ou de canard sont au menu.
>
> Le poulet ou le palo (dinde), cuit au vin très longtemps, servi parfois avec du môle, une sauce chocolat épicée, et accompagné de tortillas de maïs fait partie des mets de fête.
>
> Un punch de fruits cuits pendant plusieurs heures avec ou sans alcool est consommé pendant le réveillon, les fruits faisant souvent office de dessert.
>
> Ici pas de père noël, mais l'enfant Jésus est au cœur de la tradición, c'est lui qui remet aux enfants leurs cadeaux après minuit
>
> Noël dure jusqu'à l'arrivée des rois Mages !
>
> ![](/media/noel-guatemala-2021-8.jpg)