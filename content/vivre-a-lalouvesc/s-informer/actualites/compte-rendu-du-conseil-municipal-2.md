+++
date = 2021-06-27T22:00:00Z
description = "Conseil du 24 juin 2021"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte rendu du Conseil municipal"
weight = 1

+++
Le compte rendu du Conseil municipal du 24 juin 2021 est [accessible ici](/media/2021-5-conseil-municipal-lalouvesc.pdf). Ci-dessous l'ordre du jour :

1. COMMISSION FINANCES

   a. Modification des tarifs des nouveaux hébergements du camping

   b. Information sur l’appel d’offres eau-assainissement
2. COMMISSION GESTION

   a. Vente du bâtiment Sainte Monique

   b. Vente d’une parcelle au Grand Lieu

   c. Rachat d’un immeuble rue Saint Régis
3. COMITÉ DÉVELOPPEMENT

   a. Première réunion plénière

   b. Suivi de la demande d’un Conseiller numérique
4. DIVERS

   a. Point sur les travaux de la nouvelle salle du conseil

   b. Question sur le stationnement de la rue de la Fontaine