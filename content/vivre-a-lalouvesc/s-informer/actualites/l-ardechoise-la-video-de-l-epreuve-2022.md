+++
date = 2022-12-04T23:00:00Z
description = "Et découvrez les parcours de la 30e édition et le nouveau site 2023"
header = "/media/ardechoise_logo_30eme.png"
icon = ""
subtitle = ""
title = "L'Ardéchoise : la vidéo de l'épreuve 2022"
weight = 1

+++
Le nouveau site [www.ardechoise.com](http://www.ardechoise.com) est opérationnel pour s'inscrire, découvrir les parcours ainsi que le programme complet 2023, consacré à l'anniversaire de la 30ème édition de l'Ardéchoise.

Une grande fête à ne pas manquer pour ce millésime qui s'annonce exceptionnel !

<iframe width="560" height="315" src="https://www.youtube.com/embed/pVzHSCuLBjc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>