+++
date = 2022-05-13T22:00:00Z
description = "Un nouveau site web, une nouvelle expo, un recrutement, des visites de musée"
header = "/media/carrefour-des-arts-site-web-2022.jpg"
icon = ""
subtitle = ""
title = "Ca bouge au Carrefour des arts, saison 2022 !"
weight = 1

+++
## Nouveau site, nouveau permanent

Le Carrefour des arts vient de se doter d'un nouveau logo et de publier un site web, entièrement rénové, son adresse : [https://carrefourdesarts-lalouvesc.fr/](https://carrefourdesarts-lalouvesc.fr/ "https://carrefourdesarts-lalouvesc.fr/"). Il comprend notamment les actualités de l'association et celles des artistes qui y ont exposé récemment.

Pour aider les bénévoles à gérer toutes les nouveautés et le développement de l'association, un emploi a été créé et confié à Shasha Shaikh, artiste qui a été exposée au [Carrefour en 2021](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/inauguration-du-carrefour-des-arts/).

## Inauguration de l'exposition de photographies

Les photographies de Serge Rousse vont être présentées sur l'espace libéré par l'hôtel Beauséjour. **Tous les Louvetous sont invités à l'inauguration de** [**l'exposition**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/22-ce-qui-plait/#exposition-_dencre-et-deau_) **accompagnée d'un verre de l'amitié le lundi 23 mai à 18h.**![](/media/affiche-expo-encre-et-eau-2022.jpg)

## Quatrième visite réservée aux adhérents

La quatrième sortie s'est tenue samedi 14 mai. Une quinzaine d'adhérents de l'association ont eu droit à une présentation détaillée des expositions en cours au Musée d'Art contemporain de Lyon.

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-8.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-10.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-mac-lyon-2022-1.jpg)

{{</bloc>}}{{</grid>}}