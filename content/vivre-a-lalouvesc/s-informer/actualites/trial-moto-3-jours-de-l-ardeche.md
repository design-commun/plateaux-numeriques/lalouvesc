+++
date = 2022-06-01T22:00:00Z
description = "La course passera par Lalouvesc le samedi 4 juin"
header = "/media/trial-moto-3-jours-de-l-ardeche-2022-2.jpg"
icon = ""
subtitle = ""
title = "Trial moto : 3 jours de l'Ardèche"
weight = 1

+++
Le Trial Club de la Burle dont le siège est à Colombier le Vieux, organise pour la 8éme fois les « 3 Jours de l’Ardèche », les 4, 5 et 6 Juin 2022, compétition internationale de moto Trial et comptant pour le Championnat de France des Trials classiques. Les départs et arrivées se dérouleront à Colombier le Vieux et les parcours traverseront 19 autres communes.

Lalouvesc est concernée le premier jour, soit le 4 juin avec déjeuner pris sur place. La course se déroulera de 10h à 16h.