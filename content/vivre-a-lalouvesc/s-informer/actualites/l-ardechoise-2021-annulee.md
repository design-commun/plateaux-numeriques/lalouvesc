+++
date = 2021-04-17T22:00:00Z
description = "Pour la deuxième année consécutive, la course cycliste mythique, l'Ardéchoise, est annulée en raison de la situation sanitaire."
header = "/media/ardechoise-6.jpg"
icon = ""
subtitle = ""
title = "L'Ardéchoise 2021 annulée"
weight = 1

+++
Pour la deuxième année consécutive, la course cycliste mythique, l'Ardéchoise, est annulée. Extrait du communiqué des organisateurs :

_"La crise sanitaire et ses conséquences dramatiques auront eu raison une nouvelle fois des plus belles épreuves cyclistes du printemps et bien sûr de notre Superbe Ardéchoise que l’on croyait impérissable. La convivialité le cœur de notre concept en a pris un sérieux coup !"_

_"A 2 mois de l’événement, nous constatons que l’amélioration de la situation sanitaire n’est pas au rendez-vous espéré et que les incertitudes sont encore trop importantes_. _Malgré tous nos efforts et toutes nos espérances, nous n’aurions pas été capables de vous offrir ce qui fait la saveur de l’Ardéchoise et que vous appréciez tous : les animations dans les villages décorés, les rencontres avec les bénévoles qui vous ont préparé des ravitaillements maison et les sourires des habitants postés au bord des routes."_

Le Comité des fêtes de Lalouvesc avait, lui, commencé sérieusement la préparation des décorations et des animations en vue de l'événement. Le village et les estivants pourront malgré tout en profiter, espérons-le !