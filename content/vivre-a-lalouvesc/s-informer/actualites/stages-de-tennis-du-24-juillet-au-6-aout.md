+++
date = 2021-06-15T22:00:00Z
description = "L'AFTL propose des stages de formation ou de perfectionnement au tennis"
header = "/media/aftl-stage-2021-bandeau.jpg"
icon = ""
subtitle = ""
title = "Stages de tennis du 24 juillet au 6 août"
weight = 1

+++
![](/media/aftl-stage-2021.jpg)