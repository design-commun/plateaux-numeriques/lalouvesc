+++
date = 2021-08-10T22:00:00Z
description = "Un épisode caniculaire prévu dans le Rhône, la Drôme, l'Isère, l'Ardèche... mais pas à Lalouvesc où on ne s'ennuie pas"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = ""
title = "L'Ardèche en canicule, mais à Lalouvesc (alt 1000 m) on est bien !"
weight = 1

+++
Vous avez trop chaud ! Venez donc profiter de la fraîcheur louvetonne... et des [nombreuses activités proposées](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/spectacles-et-animations-de-la-semaine-du-9-au-16-aout/) cette semaine.

{{<grid>}} {{<bloc>}}

## Météo

<iframe id="widget_autocomplete_prev_w"  width="250" height="300" frameborder="0" src="https://meteofrance.com/widget/prevision/071280##6BB2CC"https://meteofrance.com/widget/prevision/071280##6BB2CC")"> </iframe>

{{</bloc>}} {{<bloc>}}

## Suggestion

![](/media/refuge-des-afars-5.jpg)

Pourquoi pas une nuit au frais dans les arbres ? Pour réserver, c'est [ici]().

Et c'est le moment et l' endroit idéal pour admirer [les Perséides](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/ne-manquez-pas-les-perseides/) !

{{</bloc>}} {{</grid>}}