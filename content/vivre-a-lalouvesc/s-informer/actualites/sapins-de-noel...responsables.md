+++
date = 2021-01-10T23:00:00Z
description = "Les réjouissances de Noël sont derrière nous. Ne jetez pas votre sapin dans les poubelles, elles ne sont pas faites pour cela."
header = "/media/sapins-tournon.jpg"
icon = ""
subtitle = ""
title = "Sapins de noël... responsables"
weight = 1

+++
Les réjouissances de Noël sont derrière nous. Ne jetez pas votre sapin dans les poubelles, elles ne sont pas faites pour cela. Vous pouvez le déposer à gauche devant la Mairie. Les employés municipaux s’en chargeront.

Par ailleurs, l’association des commerçants de Tournon avait distribué des sapins en pot aux divers commerçants de leur ville. Après les fêtes de Noël, ils les ont récupérés et les ont offerts à notre village afin que nous les replantions dans la forêt (voir photo).