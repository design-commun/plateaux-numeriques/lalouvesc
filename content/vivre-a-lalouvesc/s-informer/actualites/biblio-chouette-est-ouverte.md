+++
date = 2021-04-07T22:00:00Z
description = "Vous êtes confinés ? Petits et grands, profitez-en pour lire un bon livre !   "
header = "/media/bibliochouette-2021.jpg"
icon = ""
subtitle = ""
title = "Biblio'chouette est ouverte"
weight = 1

+++
Vous êtes confinés ? Profitez-en pour lire un bon livre !

Biblio’chouette vous attend les mercredis de 15h à 17h et les samedis de 9h30 à 11h30. La bibliothèque reste ouverte avec toutes les précautions sanitaires indispensables pendant cette période difficile.  
Vous y trouverez de nombreux ouvrages de fiction : romans, science fiction, policiers et bandes dessinées. Des ouvrages documentaires sur la région, les arts, la psychologie, le sport, la cuisine, l’histoire, la géographie et des ouvrages écrits en “GROS CARACTERES”.  
**Vous pouvez emprunter trois livres pour trois semaines.**  
L’inscription est individuelle et valable un an :

* Adultes : 5 € pour l’année.
* Hors commune : 7 €.
* Familles de “passage” : 7 € avec la carte d’adhérent valable toute l’année.
* Gratuité pour les moins de 18 ans.
* La consultation sur place est gratuite.

**Pour joindre la bibliothèque** :

* 04 75 67 83 67
* bm-lalou@inforoutes-ardeche.fr