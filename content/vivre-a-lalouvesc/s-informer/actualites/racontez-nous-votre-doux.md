+++
date = 2022-08-03T22:00:00Z
description = "Le Syndicat mixte du bassin versant du Doux recueille les témoignages"
header = "/media/bassin-du-doux-memoire-1.jpg"
icon = ""
subtitle = ""
title = "Racontez - nous votre Doux !"
weight = 1

+++
{{<grid>}}{{<bloc>}}
![](/media/carte-doux-2022.jpg)
{{</bloc>}}{{<bloc>}}

Vous habitez ce territoire, en intermittent fidèle ou en permanence. Vous y avez une partie de votre vie ou vos racines familiales, des souvenirs ou des anecdotes liées au Doux ou à ses affluents, à ses sources ou encore à l’usage de l’eau. Vous avez vu des évolutions du territoire et des rivières. Vous avez même, peut-être, des souvenirs enfouis dans un grenier ou une vieille valise, d’une génération précédente.
{{</bloc>}}{{</grid>}}

### Racontez - nous votre Doux !

Avec l’aide de _Voix Croisées_, _Les Films pour Demain_ et _Radio des Boutières_, le Syndicat du Doux souhaite recueillir les mémoires autour du Doux et de ses affluents – rivières et usages de l’eau. Nous avons besoin de vous pour raconter l’histoire intriquée de ces rivières dans leur territoire : comment elles ont façonné la vie sur le territoire et, réciproquement, comment les hommes les ont façonnées également.

Photographies, cartes postales, bandes sonores, bandes vidéos, poèmes, dessins, chansons, anciens articles de presse, … Votre témoignage est précieux également

Ces trésors pourront être valorisés, avec votre autorisation : dans un premier temps par une exposition puis sur le site internet du Syndicat. Par la suite, en fonction des budgets disponibles à partir de l’été 2023, sont envisagés un film, des vidéos courtes, un livre, des balades et débats

### Comment faire ?

Vous gardez vos documents originaux. Ils sont à scanner et communiquer par mail à Nelly CHATEAU : n.chateau@rivieredoux.fr. Vous pouvez les apporter au secrétariat de mairie pour numérisation et transmission au Syndicat

Pour tout renseignement, ou si vous avez besoin d’aide (par exemple pour scanner) : n.chateau@rivieredoux.fr. Pour témoigner : clairebouteloup@voixcroisees.fr

Syndicat mixte du bassin versant du Doux