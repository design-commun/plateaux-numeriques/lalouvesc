+++
date = 2022-01-04T23:00:00Z
description = "Dans cette actualité, les photos au jour le jour..."
header = "/media/beausejour-photo-o-de-framond.jpg"
icon = ""
subtitle = "Les photos, jour après jour"
title = "Beauséjour : l'album de la démolition, première semaine"
weight = 1

+++
Voir aussi l'[album de la deuxième semaine](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/beausejour-l-album-de-la-demolition-deuxieme-semaine/).

## Avant

![](/media/beausejour-cheneau-2021-4.jpg)

## Première journée, 5 janvier 2022

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-2.jpg)

{{</bloc>}}{{</grid>}}

![](/media/demolition-beausejour-2022-3.jpg)

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-7.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-10.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-13.jpg)

{{</bloc>}}{{</grid>}}

![](/media/demolition-beausejour-2022-12.jpg)

![](/media/demolition-beausejour-2022-15.jpg)

## Deuxième journée, 6 janvier 2022

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-17.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-18.jpg)

{{</bloc>}}{{</grid>}}

## Troisième journée, 7 janvier 2022

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-21.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-22.jpg)

{{</bloc>}}{{</grid>}}

![](/media/demolition-beausejour-2022-30.jpg)

![](/media/demolition-beausejour-2022-24.jpg)

## Week-end du  8 et 9 janvier 2022

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-34.jpg "Vue du jardin")

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-35.jpg "Vue du jardin"){{</bloc>}}{{</grid>}}

![](/media/demolition-beausejour-2022-36.jpg)

{{<grid>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-37.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/demolition-beausejour-2022-38.jpg)

{{</bloc>}}{{</grid>}}