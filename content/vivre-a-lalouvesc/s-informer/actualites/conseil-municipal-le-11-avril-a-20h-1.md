+++
date = 2023-04-03T22:00:00Z
description = "Ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 11 avril à 20h"
weight = 1

+++
Le deuxième Conseil municipal de l'année 2023 se tiendra le 11 avril à 20h en mairie. Il sera principalement consacré au vote du budget primitif.

### Ordre du jour

1\. COMMISSION FINANCES 

a. Approbation compte administratif (C.A.) 2022   
b. Approbation compte de gestion (C.G.) 2022  
c. Affectation des résultats 2022   
d. Taux taxes directes locales (TFB + TFNB + TH) 2023  
e. Approbation budget primitif (B.P.) 2023  
f. Demande de subvention « Fond Vert »

2\. DIVERS POINTS et AGENDA 

a. Panneau Pocket   
b. Adressage   
c. Qualité de l'eau   
d. City-park   
e. Sacem associations   
f. Conventions pour l'utilisation des locaux communaux par les associations   
g. Avancement de l'étude sur Ste Monique et le Cénacle (SCOT)   
h. Nouvel employé communal