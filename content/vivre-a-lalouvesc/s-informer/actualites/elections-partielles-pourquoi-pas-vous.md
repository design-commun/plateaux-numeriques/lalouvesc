+++
date = 2023-02-14T23:00:00Z
description = "Trois bonnes raisons de candidater"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = "Trois bonnes raisons de candidater"
title = "Elections partielles : pourquoi pas vous ?"
weight = 1

+++
Un [Bulletin spécial](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/bulletin-d-information-special-elections-partielles/) a expliqué l'importance et l'intérêt de proposer des élections partielles sur les trois postes qui restent à pourvoir au Conseil municipal à mi-mandat. La date limite pour le dépôt des candidatures est le 2 mars.

Tous les Louvetous, ceux qui sont là depuis toujours ou ceux qui sont arrivés plus récemment, peuvent le faire. Nous avons besoin de toutes les sensibilités. Voici quelques bonnes raisons pour franchir le pas :

* La gestion d'un petit village touche des **domaines très variés**. Vous pourrez choisir ceux qui vous intéressent.
* Rejoindre l'équipe à mi-mandat est une **opportunité rare** de découvrir la gestion d'une municipalité auprès d'élus expérimentés sans un engagement trop long (seulement trois années).
* Plusieurs **projets cruciaux pour l'avenir du village** sont lancés et d'autres sont à venir, auxquels vous participerez.

**N'hésitez plus, engagez-vous !**

Le maire et les élus sont disponibles pour toute précision.

![](/media/vous-elections-2023.jpg)