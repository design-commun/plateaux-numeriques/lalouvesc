+++
date = 2022-05-17T22:00:00Z
description = "Mairie fermée le 27 mai, la déchetterie fermée le 4 et 6 juin."
header = "/media/Mairie-Lalouvesc-4x1-1280w-dithered.jpg"
icon = ""
subtitle = ""
title = "Fermetures exceptionnelles"
weight = 1

+++
En raison du week-end de l'Ascension, la Mairie sera fermée le 27 mai.

En raison du week-end de la Pentecôte, la déchetterie sera fermée samedi 4 et lundi 6 juin.