+++
date = 2021-03-13T23:00:00Z
description = "Il ne faisait pas chaud, samedi 13 mars au camping, et pourtant une trentaine de Louvetonnes et Louvetous ont répondu présents, dont les Pères Olivier et Michel du Sanctuaire. Et à la fin de la journée, le résultat fut impressionnant !"
header = "/media/journee-citoyenne-1-2021-26.jpg"
icon = ""
subtitle = ""
title = "Première journée citoyenne 2021, premiers résultats"
weight = 1

+++
Il ne faisait pas chaud, samedi 13 mars au camping, et pourtant une trentaine de Louvetonnes et Louvetous ont répondu présents, dont les Pères Olivier et Michel du Sanctuaire. Et à la fin de la journée, le résultat fut impressionnant !

Toutes les pistes de mini-golf étaient dégagées et nettoyées, les chalets détériorés étaient réparés, le bosquet des cabanes entièrement débroussaillé, plusieurs cabanes dégagées et réparées.

Un résultat acquis grâce à l'addition de nos talents, de forces citoyennes, dans la bonne humeur et la distanciation nécessaire. Tous les présents peuvent en être fiers et verront cet été le fruit de leurs efforts dans l’œil heureux des vacanciers du camping et de celui de ceux qui fréquenteront ses installations ouvertes à tous.

Mais aussi, rappelons-le, le camping est la principale source d'autofinancement de la commune et donc de notre capacité collective à investir.

{{<grid>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-15.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-2.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-4.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-12.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-13.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-10.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-9.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-23.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-11.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-24.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-16.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-17.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-21.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-19.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-18.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-7.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-1.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-8.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-20.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-1-2021-30.jpg)

{{</bloc>}}

{{</grid>}}

Vous n'y étiez pas ? Pas de souci, il reste encore des travaux pour le week-end prochain (le 27 mars) : peinture des pistes du mini golf, peinture des chalets dégradés, nettoyage du ruisseau du bosquet des cabanes, nettoyage du terrain de tennis, etc. Pour vous donner du cœur à l'ouvrage, allez donc faire un tour cette semaine sur le camping, cela vaut le coup !

Plus il y aura de participants, plus nous montrerons notre capacité collective à gérer le bien commun de la commune et nous le ferons savoir !

Suggestion : le premier mai qui est un samedi, prévoir, dans la limite toujours des précautions sanitaires qui seront en vigueur, une grande fête pour l'ouverture du Camping du Pré du Moulin relooké avec concours de mini-golf, concert de la Lyre louvetonne, etc.

D'ici là, les enfants de l'école St Joseph pourraient, dans une de leur sortie, tester le mini-golf pour vérifier que les pistes sont bien opérationnelles.