+++
date = 2022-09-03T22:00:00Z
description = "Bravo et merci au Comité des fêtes !"
header = "/media/brocante-2022-9.jpg"
icon = ""
subtitle = ""
title = "Beau temps et affluence record pour la brocante 2022"
weight = 1

+++
Quelques 300 exposants comblés, près de 5.000 chineurs ravis... une organisation impeccable grâce à l'efficacité et au dévouement des bénévoles du Comité des fêtes.

**Chapeau ! Merci !**

Après les 2.600 spectateurs du cinéma Le Foyer, maintenant la brocante, 2022 est décidément l'année des records d'affluence pour le village.

{{<grid>}}{{<bloc>}}

![](/media/brocante-2022-12.jpg){{</bloc>}}{{<bloc>}}

![](/media/brocante-2022-8.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/brocante-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/brocante-2022-7.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/brocante-2022-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/brocante-2022-1.jpg)

{{</bloc>}}{{</grid>}}

![](/media/brocante-2022-2.jpg)

![](/media/brocante-2022-13.jpg)

Au nord de la Loire, on va à la braderie de Lille, au sud à la brocante de Lalouvesc ! &#128521