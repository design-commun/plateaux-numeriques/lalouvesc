+++
date = 2022-12-08T23:00:00Z
description = "Mairie, poste, déchetterie"
header = "/media/decoration-noel-2021-9.jpg"
icon = ""
subtitle = ""
title = "Fermetures de fin d'année"
weight = 1

+++
En raison des fêtes de fin d'année, des fermetures exceptionnelles sont prévues.

**La mairie** sera fermée du vendredi 23 décembre 2022 au dimanche 1er janvier 2023.

**L'agence postale** sera fermée du lundi 26 décembre 2022 au dimanche 1er janvier 2023.

**La déchetterie** sera fermée le vendredi 23 et le samedi 24 ainsi que le vendredi 30 et le samedi 31 décembre 2022.

**Bonnes fêtes à toutes et tous !**