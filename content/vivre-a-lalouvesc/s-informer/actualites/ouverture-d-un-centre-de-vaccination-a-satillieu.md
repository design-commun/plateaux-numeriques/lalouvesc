+++
date = 2021-11-23T23:00:00Z
description = "A la Maison de Santé à partir du 3 décembre"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Ouverture d'un centre de vaccination à Satillieu"
weight = 1

+++
Le Centre de Soins Infirmiers de Satillieu, en association avec le Dr Heyraud, ont créé un centre éphémère de vaccination COVID-19 qui répond au nom de Centre du Val d'Ay.  
   
Dans le but de répondre à une demande forte de la part de la population  du canton, ce centre ouvre ses portes le 03 décembre 2021 à la Maison de  Santé , 100 rue des Aygas, 07290 SATILLIEU.  
   
Les plages horaires sont uniquement le mardi matin, et le vendredi matin et après-midi, sur rendez-vous.  
   
La prise d'information et de rendez-vous se fait auprès du secrétariat du Centre de Soins Infirmiers de Satillieu au 04-75-34-97-97.