+++
date = 2022-07-14T22:00:00Z
description = "Il se passe toujours quelque chose aux Lalouv'estivales"
header = "/media/carrefour-des-arts-2022-ecole-4.jpeg"
icon = ""
subtitle = "Quelques suggestions"
title = "Que faire à Lalouvesc du 17 juillet au 24 juillet ?"
weight = 1

+++
A part le farniente, les [**innombrables balades**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/), le [**vélotourisme**](https://www.lalouvesc.fr/decouvrir-bouger/activites/velotourisme/), le[ **cheval**](https://www.lalouvesc.fr/decouvrir-bouger/activites/centre-equestre/)... les pique-niques à l'ombre des arbres, les [**jeux pour les enfants**](https://www.lalouvesc.fr/decouvrir-bouger/activites/parcs-terrains-de-sport-et-jeux-d-enfants/) (se renseigner au camping sur les animations encadrées gratuites proposées) voici quelques suggestions :

### Dimanche 17 juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Le Temps des Secrets_** ( [bande annonce](https://youtu.be/RFpGLeipEBA) ) à l’Abri du Pèlerin

### Lundi 18 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_En corps_** ( [bande annonce](https://youtu.be/WMqIkiI6fAA) ) à l’Abri du Pèlerin

### Mardi 19 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Mercredi 20 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 15h - 17h emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* 17h partager un **Apéro découverte** à l’Office du tourisme
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Notre Dame brûle_** ( [bande annonce](https://youtu.be/YlDXdPSEtgk) ) à l’Abri du Pèlerin

### Jeudi 21 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_C’est Magnifique_** ( [bande annonce](https://youtu.be/z-MlOwkqhDY) ) à l’Abri du Pèlerin

### Vendredi 22 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Samedi 23 juillet

* 9h30 - 11h30  emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Notre Dame brûle_** ( [bande annonce](https://youtu.be/YlDXdPSEtgk) ) à l’Abri du Pèlerin

### Dimanche 24 juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_C’est Magnifique_** ( [bande annonce](https://youtu.be/z-MlOwkqhDY) ) à l’Abri du Pèlerin