+++
date = 2023-04-18T22:00:00Z
description = "Sur numéros d'urgence zône Privas et Vallée du Rhône"
header = ""
icon = ""
subtitle = ""
title = "Panne SFR/Bouygues"
weight = 1

+++
19 avril à 11h, la préfecture communique :

Actuellement panne réseaux mobiles Bouygues/SFR signalée sur les numéros d'urgence : 15, 17 et 18 (secteurs Privas + Vallée du Rhône).

**En cas d'urgence, il convient de composer le 112**.