+++
date = 2023-01-26T23:00:00Z
description = "4 février à 10h"
header = "/media/cafe-des-aidants-2022-3.jpg"
icon = ""
subtitle = ""
title = "Prochain café des aidants"
weight = 1

+++
Le prochain café des aidants de St Félicien aura lieu samedi 4 février 2023 de 10h à 11h30 à la salle des ainés de St Félicien- à côté de la Mairie (L'auberge de St Félicien étant fermée à cette date) sur le thème « Et l'amour dans tout ça ? »  
   
Vous accompagnez un proche malade, en situation de handicap ou dépendant du fait de l'âge ? Autour d'un café, venez échanger votre expérience avec d'autres aidants.  
Animés par des professionnels, les Cafés des Aidants sont des lieux, des temps et des espaces d'information, pour échanger et rencontrer d'autres aidants. Ils sont ouverts à tous les aidants (non professionnels, quels que soient l'âge et la pathologie de la personne accompagnée).  
   
Renseignements auprès de Carole Guilloux : [cguilloux@fede07.admr.org](mailto:cguilloux@fede07.admr.org) ou 06 81 50 19 26