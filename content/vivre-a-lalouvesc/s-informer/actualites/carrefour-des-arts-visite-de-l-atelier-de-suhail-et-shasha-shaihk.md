+++
date = 2021-06-05T22:00:00Z
description = "Des bénévoles du Carrefour des Arts ont visité à Lamastre l'atelier des deux artistes..."
header = "/media/shaihk-atelier-2.jpg"
icon = ""
subtitle = ""
title = "Carrefour des Arts : visite de l'atelier de Suhail et Shasha Shaihk"
weight = 1

+++
Suhail et Shasha Shaihk sont venus au CAC ce week end pour prévoir l'installation de leurs œuvres au Carrefour des Arts 2021. Ils ont, en retour, invité les bénévoles présents à visiter leur atelier à Lamastre.

Dernière œuvre en cours de réalisation par Suhail Shaihk : des  extraordinaires maquettes en papier d'avions de la seconde  guerre mondiale pour un musée britannique en hommage aux soldats polonais ayant servi dans la Royal Air Force.

![](/media/shaihk-atelier8.jpg)