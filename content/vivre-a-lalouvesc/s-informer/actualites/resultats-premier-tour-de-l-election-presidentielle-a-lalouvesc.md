+++
date = 2022-04-09T22:00:00Z
description = "Lalouvesc a voté"
header = "/media/Mairie-Lalouvesc-4x1-1280w-dithered.jpg"
icon = ""
subtitle = ""
title = "Résultats du premier tour de l'élection présidentielle à Lalouvesc"
weight = 1

+++
### Résultats de la commune au 1er tour de la présidentielle de 2022

| Liste des candidats | Voix | % Inscrits | % Exprimés |
| :---: | :---: | :---: | :---: |
| M. Emmanuel MACRON | 83 | 24,34 | 30,07 |
| Mme Marine LE PEN | 57 | 16,71 | 20,65 |
| M. Jean-Luc MÉLENCHON | 35 | 10,26 | 12,68 |
| M. Eric ZEMMOUR | 32 | 9,38 | 10,66 |
| Mme Valérie PECRESSE | 18 | 5,27 | 8,46 |
| M. Nicolas DUPONT-AIGNAN | 15 | 4,40 | 5,43 |
| M. Jean LASSALLE | 12 | 3,52 | 4,35 |
| M. Yannick JADOT | 9 | 2,64 | 3,26 |
| M. Fabien ROUSSEL | 7 | 2,05 | 2,54 |
| Mme Anne HIDALGO | 3 | 0,9 | 1,09 |
| M. Philippe POUTOU | 2 | 0,06 | 0,07 |
| Mme Nathalie ARTHAUD | 0 | 0,00 | 0,00 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 341 |  |  |
| Abstentions | 65 | 19,06 |  |
| Votants | 276 | 80,93 |  |
| Blancs | 2 | 0,59 | 0,72 |
| Nuls | 1 | 0,23 | 0,36 |
| Exprimés | 273 | 80,05 | 98,91 |

### Rappel des résultats de la commune au 1er tour de 2017

| Liste des candidats | Voix | % Inscrits | % Exprimés |
| :---: | :---: | :---: | :---: |
| Mme Marine LE PEN | 72 | 21,05 | 26,47 |
| M. François FILLON | 70 | 20,47 | 25,74 |
| M. Emmanuel MACRON | 56 | 16,37 | 20,59 |
| M. Jean-Luc MÉLENCHON | 29 | 8,48 | 10,66 |
| M. Nicolas DUPONT-AIGNAN | 23 | 6,73 | 8,46 |
| M. Jean LASSALLE | 11 | 3,22 | 4,04 |
| M. François ASSELINEAU | 5 | 1,46 | 1,84 |
| M. Benoît HAMON | 3 | 0,88 | 1,10 |
| M. Philippe POUTOU | 3 | 0,88 | 1,10 |
| Mme Nathalie ARTHAUD | 0 | 0,00 | 0,00 |
| M. Jacques CHEMINADE | 0 | 0,00 | 0,00 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 342 |  |  |
| Abstentions | 65 | 19,01 |  |
| Votants | 277 | 80,99 |  |
| Blancs | 5 | 1,46 | 1,81 |
| Nuls | 0 | 0,00 | 0,00 |
| Exprimés | 272 | 79,53 | 98,19 |