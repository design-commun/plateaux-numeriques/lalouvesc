+++
date = 2022-03-21T23:00:00Z
description = "Vendredi 22 mars 15h20 devant l'école"
header = "/media/carnaval-ecole-2022-7.jpg"
icon = ""
subtitle = "Rendez-vous (déguisés) vendredi 22 mars à 15h20 devant l'école"
title = "Carnaval de l'école"
weight = 1

+++

![](/media/affiche-carnaval-ecole-2022.jpg)