+++
date = 2021-10-21T22:00:00Z
description = "Renseignez-vous. Portes ouvertes au collège de Satillieu du 18 au 20 novembre"
header = "/media/college-satillieu-logo.jpg"
icon = ""
subtitle = ""
title = "Votre enfant entre bientôt au collège..."
weight = 1

+++
Renseignez-vous. Portes ouvertes au collège St Joseph en Val d'Ay, 185 Chemin du Couvent à Satillieu les 18, 19 et 20 novembre sur rendez-vous.

Contactez le secrétariat au 04 75 34 96 42

[Site web du collège](https://sites.google.com/site/stjosephenvalday/home).