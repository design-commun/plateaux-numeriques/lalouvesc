+++
date = 2021-06-09T22:00:00Z
description = "La prise d'astreinte est aussi l'occasion d'une rencontre sportive et ce vendredi les pompiers vont investir l'hôtel Beauséjour. "
header = "/media/pompiers-foot-1.jpeg"
icon = ""
subtitle = ""
title = "Le vendredi soir pour les pompiers, c'est astreinte, sport et..."
weight = 1

+++
Le vendredi soir, les pompiers se retrouvent pour leur prise d’astreinte.

### Qu’est-ce que la prise d’astreinte ?

Les pompiers de Lalouvesc assurent des astreintes en semaine complète, chaque semaine une nouvelle équipe vient remplacer la précédente. Pour cela, l'équipe va à la caserne vérifier le matériel, les véhicules et prendre en compte les éventuels problèmes que l’équipe précédente a fait remonter.

Cette prise de fonction est assez rapide, environ 30 minutes.

### L'occasion d'une rencontre sportive

Récemment, les pompiers ont mis en place, après l'astreinte, une activité sportive sur les nombreux terrains mis à disposition par la Mairie (stade de foot, basket, tennis… et même mini-golf)

![](/media/pompiers-foot-3.jpeg)

Grâce à ces moments conviviaux et sportifs, les pompiers de Lalouvesc forment une équipe soudée et active, qui répond aux appels de la population.

### L'hôtel Beauséjour ponctuellement réinvestit

Vendredi 11 juin, les sapeurs-pompiers et la commune de Lalouvesc ont le plaisir d’accueillir pour une manœuvre bien particulière l’équipe Sauvetage-Déblaiement (SD) Drôme/Ardèche.

En effet, les sapeurs-pompiers de l’Ardèche ont beaucoup d’équipes spécialisées : le SD, le GRIMP (équipe sauvetage en montagne), le Cyno (équipe cynophile) et biens d’autres.

L’équipe SD viendra ce vendredi se former dans le bâtiment de l’hôtel Beauséjour. En effet ce genre de bâtiment est très recherché pour pouvoir s’entrainer sur une situation réelle. Les pompiers de Lalouvesc leur viendront en aide.

La suite et l’explication de cette manœuvre spectaculaire dans le prochain bulletin.