+++
date = 2022-07-01T22:00:00Z
description = "Inauguration de l'exposition 2022, de grandes ambitions..."
header = "/media/carrefour-des-arts-2022-inauguration-9.jpg"
icon = ""
subtitle = ""
title = "Carrefour des Arts, c'est parti... très bien parti !"
weight = 1

+++
## Une avant-première pour l'école

Les premiers visiteurs privilégiés ont été les enfants de l'école St Joseph qui ont eu droit à une présentation de l'exposition spéciale par les artistes le vendredi 1er juillet en avant première.

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-ecole-4.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-ecole-5.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-ecole-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-ecole-1.jpg)

{{</bloc>}}{{</grid>}}

![](/media/carrefour-des-arts-2022-ecole-3.jpeg)

## Une inauguration sous le soleil

Les nombreux invités présents pour l'inauguration ont pu découvrir et admirer l'exposition 2022 en présence des artistes. Un cru 2022 à la hauteur de la réputation aujourd'hui solidement acquise de la manifestation.

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-5.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-7.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-8.jpg)

{{</bloc>}}{{</grid>}}

## Une ambition affirmée

Une collation était offerte dans le jardin de la Maison St Régis, précédée par quelques mots du président de l'association, Julien Besset et des élus de la Région, du département de la CCVA et du Maire de Lalouvesc.

Le Carrefour des Arts ne s'endort visiblement pas sur ses lauriers : il s'ouvre aux arts du spectacles en relayant la tradition des Promenades musicales, il élargit son activité dans l'espace avec les photos installées sur l'espace Beauséjour dans le centre-bourg depuis déjà un mois et jusqu'en octobre, et il se prolonge sur l'année par des visites de musées et d'ateliers d'artistes et par des interventions dans des collèges... et son Président a même suggéré qu'il pourrait un jour s'installer au Cénacle. Les élus présents ont tous souligné la réussite et le dynamisme impressionnant de l'association et de ses bénévoles et affirmé leur volonté de soutenir son développement.

Les discours se sont conclus par une aubade offerte par les deux violoncellistes fidèles des Promenades : Gilles Lefèvre et Florent Couaillac.

{{<grid>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-10.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-11.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-12.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/carrefour-des-arts-2022-inauguration-13.jpg)

{{</bloc>}}{{</grid>}}

## Ouverture et atelier

L'exposition est gratuite, ouverte du 2 juillet au 28 août au Centre d’Animation Communal tous les jours de 14h30 à 18h30.

Déjà plus de 300 visiteurs en deux jours... et une démonstration de réalisation de mosaïque par Malika Ameur.

{{<grid>}}{{<bloc>}}
![](/media/carrefour-des-arts-2022-atelier-lalika-ameur-1.jpg)
{{</bloc>}}{{<bloc>}}
![](/media/carrefour-des-arts-2022-atelier-lalika-ameur-2.jpg)
{{</bloc>}}{{</grid>}}