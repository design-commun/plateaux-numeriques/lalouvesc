+++
date = 2022-07-28T22:00:00Z
description = "Il se passe toujours quelque chose aux Lalouv'estivales"
header = "/media/carrefour-des-arts-2022-ecole-3.jpeg"
icon = ""
subtitle = ""
title = "Que faire à Lalouvesc du 31 juillet au 7 août ?"
weight = 1

+++
A part le farniente, les [**innombrables balades**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/), le [**vélotourisme**](https://www.lalouvesc.fr/decouvrir-bouger/activites/velotourisme/), le[ **cheval**](https://www.lalouvesc.fr/decouvrir-bouger/activites/centre-equestre/)... les pique-niques à l'ombre des arbres, les [**jeux pour les enfants**](https://www.lalouvesc.fr/decouvrir-bouger/activites/parcs-terrains-de-sport-et-jeux-d-enfants/) (se renseigner au camping sur les animations encadrées gratuites proposées) voici quelques suggestions :

### Dimanche 31 juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Les Minions 2 : Il était une fois Gru_** ( [bande annonce](https://youtu.be/Cm6wpegvRpA) ) à l’Abri du Pèlerin

### Lundi 1er août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_À l’ombre des filles_** ( [bande annonce](https://youtu.be/0qezBv7i-bc) ) à l’Abri du Pèlerin

### Mardi 2 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 20h30 : écouter un **Concert** **_La voix et la musique_** [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr) , musique classique à la basilique

### Mercredi 3 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 15h - 17h emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* 17h partager un **Apéro découverte** à l’Office du tourisme
* 20h30 : écouter un **Concert** **_Bach et associés_** [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr) , musique classique à la chapelle de Notre Dame d’Ay
* 21h : **Cinéma** _L'école du bout du monde_ ( [bande annonce](https://youtu.be/hca-7AKq07w) ) à l'Abri du Pèlerin

### Jeudi 4 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 20h30 : écouter un **Concert** **_Souvenir de Florence_** [**Promenades musicales-Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr) , musique classique à la basilique
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Top Gun_** ( [bande annonce](https://youtu.be/V4gQdk1nAn0) ) à l’Abri du Pèlerin

### Vendredi 5 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Samedi 6 août

* 9h30 - 11h30  emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_L’école du bout du monde_** ( [bande annonce](https://youtu.be/hca-7AKq07w) ) à l’Abri du Pèlerin

### Dimanche 7 août

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* aller à la **fête du Sanctuaire**
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Top Gun_** ( [bande annonce](https://youtu.be/V4gQdk1nAn0) ) à l’Abri du Pèlerin