+++
date = 2022-03-01T23:00:00Z
description = "On a chanté et partagé les bugnes au club des Deux Clochers"
header = "/media/mardi-gras-club-des-2-clochers-2022-3.jpg"
icon = ""
subtitle = ""
title = "Mardi gras chez les ainés"
weight = 1

+++
Au club des deux clochers, avec les ainés, le mardi gras fut chantant, joyeux... et aussi goûteux. La chaleur des retrouvailles et de la bienveillance. On en a besoin.

{{<grid>}}{{<bloc>}}

![](/media/mardi-gras-club-des-2-clochers-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/mardi-gras-club-des-2-clochers-2022-1.jpg)

{{</bloc>}}{{</grid>}}