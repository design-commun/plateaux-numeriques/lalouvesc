+++
date = 2021-03-31T22:00:00Z
description = "La manifestation prévue le 11 avril est annulée à la demande de la préfecture pour des raisons sanitaires... "
header = "/media/ronde-louvetonne-2.jpg"
icon = ""
subtitle = ""
title = "Annulation de la Ronde louvetonne"
weight = 1

+++
Déception au Comité des fêtes, pour la seconde année consécutive et pour les mêmes raisons sanitaires, la Ronde louvetonne, rassemblement à Lalouvesc de voiture anciennes et d'exception prévu le 11 avril, est annulé par la préfecture.

Les retours, reçus par le Comité des fêtes, confirment que nombreux se faisaient une joie d'envisager une journée à Lalouvesc. Tous se doutaient de l'issue mais avaient envie d'y croire. Nombreux sont ceux qui félicitent et encouragent notre village à persister à vouloir organiser des évènements. Nombreux sont ceux qui affichent leur souhait de vite revenir passer une journée à Lalouvesc.

Beaucoup d'encouragements et de félicitations. Lalouvesc suscite toujours, c'est certain, beaucoup d'engouement.