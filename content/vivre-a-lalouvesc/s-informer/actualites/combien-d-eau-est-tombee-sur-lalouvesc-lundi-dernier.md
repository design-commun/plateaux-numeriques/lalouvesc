+++
date = 2021-05-11T22:00:00Z
description = "L'alerte météo ne s'est pas trompée, les pluies ont été intenses..."
header = "/media/inondation-10-mai-2021-3.jpg"
icon = ""
subtitle = ""
title = "Combien d'eau est tombée sur Lalouvesc lundi dernier ?"
weight = 1

+++
L'alerte météo avait été lancée et, effectivement, l'averse ne nous a pas déçus. Il est tombé 172 mm d'eau à Lalouvesc entre lundi 10 mai 8h du matin et mardi 8h. Nous le savons grâce au père Olivier de Framond qui relève chaque matin pour Météo-France le niveau de pluviométrie.

Nous avons eu peur pour les nouveaux aménagements du camping. Mais que les citoyens qui ont participé à leur rénovation soient rassurés : le tennis et le minigolf ont reçu une bonne douche, mais, une fois l'eau écoulée, ils ont retrouvé leur fière allure.