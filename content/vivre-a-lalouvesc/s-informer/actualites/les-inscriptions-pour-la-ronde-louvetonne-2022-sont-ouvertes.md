+++
date = 2022-02-06T23:00:00Z
description = "Ce sera le 3 avril. Rassemblement et balade."
header = "/media/ronde-louvetonne-2.jpg"
icon = ""
subtitle = "Ce sera le 3 avril. Rassemblement et balade"
title = "Les inscriptions pour la Ronde louvetonne 2022 sont ouvertes"
weight = 1

+++
  
Le Comité des Fêtes de Lalouvesc organise son rassemblement annuel de voitures anciennes, [La Ronde Louvetonne](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/la-ronde-louvetonne-des-vieilles-voiture-avril/), le dimanche 3 avril. Le rassemblement est statique et il sera possible de participer à une balade.  
Le prix d’inscription est fixé à 17 euros par voiture comprenant une plaque rallye de la Ronde Louvetonne, un « road book », un apéritif offert en cours de balade.  
L’inscription se fait par courrier : [règlement et bulletin](/media/ronde-louvetonne-2022-inscription.pdf)

![](/media/ronde-louvetonne-2022-affiche.jpg)