+++
date = 2022-06-07T22:00:00Z
description = "... aidez-nous à améliorer notre accueil !"
header = "/media/questionnaire-2022.jpg"
icon = ""
subtitle = "... aidez-nous à améliorer notre accueil"
title = "Vous êtes de passage à Lalouvesc ou sur le site lalouvesc.fr..."
weight = 1

+++
### **Nous avons besoin de votre avis, merci de prendre quelques minutes pour remplir ce**[ **court questionnaire**](https://forms.gle/Kyubi3S4d6W8XAdSA)**.** 

[![](/media/questionnaire-2022.jpg)](https://forms.gle/Kyubi3S4d6W8XAdSA)