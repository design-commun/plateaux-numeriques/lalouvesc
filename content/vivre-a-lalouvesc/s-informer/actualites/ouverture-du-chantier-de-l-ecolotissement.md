+++
date = 2022-03-09T23:00:00Z
description = "Avec quelques jours d'avance"
header = "/media/ouverture-chantier-ecolotissement-2022-6.jpg"
icon = ""
subtitle = ""
title = "Ouverture du chantier de l'écolotissement"
weight = 1

+++
Le chantier vient de démarrer. Il durera cinq semaines pour la viabilisation du lotissement. La terre dégagée servira à niveler les plateformes du camping qui en ont bien besoin.  
Un deuxième volet du chantier se tiendra à l'automne pour l'installation des murets et des espaces verts.

![](/media/ouverture-chantier-ecolotissement-2022-5.jpg)