+++
date = 2022-06-09T22:00:00Z
description = "Les bassins versants du Doux et de la Cance ont été placés en alerte"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "Alerte sécheresse"
weight = 1

+++
La préfecture a placé les bassins versants du Doux et de la Cance en niveau 2 alerte (sur 4 niveaux). Cette situation correspond au niveau d’alimentation des cours d’eau en dessous duquel la coexistence de tous les usages et le bon fonctionnement du milieu aquatique ne sont plus assurés dans des conditions satisfaisantes.

Dès lors des restrictions d'usages sont indispensables.

## Restrictions d’usages

### Usages de l’eau domestique (particuliers et collectivités territoriales)

* L’alimentation en eau des plans d’eau, des canaux d’agrément et des béalières ne disposant pas de règlement d’eau autorisé par le préfet (arrêté préfectoral) et le prélèvement d’eau depuis ces ouvrages sont interdits. Une attention particulière sera portée lors des opérations de fermeture des canaux afin de ne pas porter préjudice à la faune piscicole présente. L’alimentation en eau des plans d’eau, des canaux d’agrément et des béalières autorisés par arrêté préfectoral et le prélèvement d’eau depuis ces ouvrages doivent respecter les prescriptions fixées dans l’arrêté.
* L’arrosage des pelouses, ronds points, espaces verts publics et privés, jardins d’agrément et des espaces sportifs n’est autorisé que trois jours par semaine (lundi, mercredi et vendredi) entre 20h et 9h.
* Le lavage des voitures est interdit hors des stations professionnelles recyclant l’eau et sauf pour les véhicules ayant une obligation réglementaire (véhicules sanitaires, alimentaires ou techniques) et pour les organismes liés à la sécurité.
* Le remplissage des piscines est interdit (sauf piscines de volume inférieur à 1 m³) ; toutefois le premier remplissage des piscines nouvellement construites et le remplissage complémentaire des piscines sont autorisés entre 20 h et 9 h.
* Le lavage à l'eau des voiries est interdit, sauf impératifs sanitaires et à l’exception des lavages effectués par des balayeuses laveuses automatiques.
* Les fontaines publiques en circuits ouverts doivent être arrêtées.
* Les tests de capacité des hydrants et points d’eau incendie (PEI) sont interdits.

### Usages industriels

Les installations classées pour la protection de l’environnement (ICPE) appliquent les prescriptions fixées dans leur arrêté d’autorisation, leur enregistrement ou leur déclaration pour les épisodes d’alerte. Les besoins prioritaires et indispensables des autres activités industrielles doivent être portés à la connaissance du service de police de l’eau ou de contrôle des installations classées.

### Stations d’épuration des eaux usées

Les gestionnaires d'installations signalent préalablement aux services de police des eaux les interventions susceptibles de générer un rejet dépassant les normes autorisées, notamment les opérations de maintenance sur des organes de traitement ou les opérations d'entretien des réseaux (curages…).