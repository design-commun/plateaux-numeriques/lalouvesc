+++
date = 2021-04-20T22:00:00Z
description = "Le programme complet 2021 des promenades"
header = "/media/promenades-musivales-lalouvesc-1.jpeg"
icon = ""
subtitle = ""
title = "Programme 2021 des Promenades musicales (actualisé)"
weight = 1

+++
Attention, programme actualisé ! Le concert initialement prévu le 26 juillet a été déplacé au 3 août.

![](/media/promenades-musicales-2021-2.jpg)