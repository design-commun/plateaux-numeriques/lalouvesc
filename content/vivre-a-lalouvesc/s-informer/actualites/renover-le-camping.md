+++
date = 2021-02-15T23:00:00Z
description = "Comme annoncé dans le dernier Bulletin d’information, des journées citoyennes sont organisées les week-ends des 13-14 mars (et non 6-7 mars comme annoncé initialement)  et 20-21 mars. Elles ont pour objectif la rénovation du camping municipal."
header = "/media/camping-lalouvesc.jpg"
icon = "🏕️"
subtitle = ""
title = "Rénover le camping"
weight = 1

+++
Comme annoncé dans le dernier Bulletin d’information, des journées citoyennes sont organisées les week-ends des 13-14 mars (et non 6-7 mars comme annoncé initialement) et 20-21 mars. Elles ont pour objectif la rénovation du camping municipal. Toutes les énergies louvetonnes sont requises et mobilisées pour redonner son éclat à ce bien commun afin qu’il soit à la hauteur de la réputation d’accueil du village.  
La municipalité, de son côté, a prévu d’acheter des nouveaux hébergements adaptés à la demande actuelle.  
Pour vous inscrire à ces journées et nous permettre d’organiser au mieux les opérations, merci de remplir ce très court formulaire, même si vous n’êtes pas libre pour ces journées, vous pouvez nous aider ([cliquer ici](https://docs.google.com/forms/d/e/1FAIpQLSffAQE20gzFLi55Lp_YDhu1oJZji9dF2GTguOOC4sWlu1BHsw/viewform?usp=pp_url)).