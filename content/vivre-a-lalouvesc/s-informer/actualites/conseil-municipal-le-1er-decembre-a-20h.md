+++
date = 2021-11-25T23:00:00Z
description = "Ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 1er décembre à 20h"
weight = 1

+++
Un Conseil municipal est convoqué le 1er décembre à 20h dans la salle de la mairie.

Attention : en raison de la situation sanitaire masques et distanciation nécessaires.

### Ordre du jour

1. COMMISSION FINANCES

\- Affectation des résultats de l’année 2020 des budgets Commune, Eau et Assainissement, délibération.

\- Décision Modificative N°4, délibération.

\- OGEC : fixation du forfait communal par élève pour l’année 2022, délibération.

\- Acquisition par la Commune de la maison située 3 rue Saint Jean François Régis – indivision Simone Chavet – fixation d’un montant plafond pour l’acquisition, délibération.

\- Dossier de subvention LEADER – Jeu Monument ; information au Conseil.

\- Dossier de subvention Guichet numérique, information au Conseil.

\- Taxes communales de raccordement à l’assainissement et à l’eau potable, délibération.

\- Camping municipal : bilan de la saison estivale et fixation des tarifs pour la saison 2022, délibération.

\- Demande de subvention pour un séjour scolaire, enfant de Lalouvesc scolarisé à Saint Félicien, délibération.

\- Prêt « In fine » Banque postale pour l’écolotissement, délibération

1. COMMISSION GESTION

\- Ramassage des ordures ménagères, information au Conseil.

\- Rapport annuel sur le Prix et la Qualité du service SPANC pour l’année 2020, délibération.

\- Acquisition d’un tracteur multiservices, information au Conseil.

\- Achat d’un City Park, information au Conseil

\- Choix d’une entreprise pour l’aménagement de l’éco lotissement, information et discussion.

\- CNAS : résiliation de la convention d’adhésion, délibération.

1. COMMISSION VIE LOCALE

\- Lancement de l’opération Babets, information au Conseil.

\- Décorations de fin d’année, information au Conseil.

1. COMMISSION DÉVELOPPEMENT

\- Retour sur la réunion du Comité de développement du 26 novembre, information au Conseil.

\- Lancement d’un concours d’idées pour le Jeu monument, information au Conseil.

1. DIVERS

\- Questions diverses.

\- Cimetière : mise en place d’un colombarium