+++
date = 2023-03-18T23:00:00Z
description = "On récupère vêtements propres, chaussures, petite maroquinerie"
header = "/media/bac-vetements-mars-2023-1.jpg"
icon = ""
subtitle = ""
title = "Un conteneur à vêtements"
weight = 1

+++
**Un conteneur à vêtements vient d'être mis en place Chemin Grosjean à côté du point de collecte des déchets.**

Vous pouvez déposer dans le conteneur de petits sacs, remplis de vêtements et linge de maison, chaussures, petite maroquinerie (sacs à main, ceintures).

\- Utiliser des sacs de **30 litres maximum** (afin qu’ils puissent entrer dans les conteneurs).

\- Veiller à toujours **bien fermer** ces sacs (pour ne pas qu’ils se salissent, ne pas déposer les vêtements en vrac ou dans des cartons).

\- Donner des **vêtements propres et secs.**

\- Les vêtements souillés (peinture, graisse…), mouillés et moisis ne sont pas recyclables.

\- Attacher les chaussures par paires.

\- Séparer le textile des chaussures et de la maroquinerie.

![](/media/bac-vetements-mars-2023-3.jpg)

La récolte est assurée par [Le Relais](https://lerelais.org), entreprise sociale et solidaire engagée dans la lutte contre l’exclusion par la création d’emplois durables pour des personnes en difficulté.