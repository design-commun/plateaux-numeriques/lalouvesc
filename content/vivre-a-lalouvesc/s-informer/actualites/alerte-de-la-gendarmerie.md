+++
date = 2023-01-19T23:00:00Z
description = "Ne donnez pas d'informations sur vos habitudes par téléphone"
header = ""
icon = ""
subtitle = ""
title = "Alerte de la gendarmerie"
weight = 1

+++
_Bonjour,_

_Une victime de cambriolage nous a informé avoir été contactée peu de temps avant par une personne se prétendant technicien d'Orange pour installer la fibre._

_A posteriori, la victime s'est dit que ce faux technicien avait demandé beaucoup de précisions sur ses habitudes, comme le fait de demander quand elle était absente ou présente au domicile, quand celui-ci était vide..._

_Bien entendu, personne n'est passé pour la fibre._

**_Ne jamais communiquer, que ce soit par téléphone, sur le répondeur ou sur les réseaux, vos dates ou vos horaires d'absences du domicile, que ce soit pour les vacances ou une autre raison._**

_En cas de démarchage, assurez-vous auprès de l'entreprise de la réalité de la démarche, en utilisant les numéros officiels (certains malfaiteurs communiquent de faux numéros qui renvoient les victimes vers un complice)._

_N'hésitez pas à diffuser ce conseil à vos connaissances._

_Cordialement_  
 **Major LE GALLIARD Ludovic** Commandant la communauté de brigades SATILLIEU - SAINT FELICIEN