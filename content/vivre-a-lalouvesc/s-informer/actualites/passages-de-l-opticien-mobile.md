+++
date = 2023-02-15T23:00:00Z
description = "Quatre dates de mars à mai"
header = "/media/camion-optique-mars-2023-2.jpg"
icon = ""
subtitle = ""
title = "Passages de l'opticien mobile"
weight = 1

+++
Présence sur la place du Lac les 6 et 27 mars, le 17 avril et le 15 mai au matin. Prendre rendez-vous au 09 67 00 64 96.

 

 ![](/media/camion-optique-mars-2023-1.jpg)