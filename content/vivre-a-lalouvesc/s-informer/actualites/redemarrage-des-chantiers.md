+++
date = 2021-09-13T22:00:00Z
description = "La saison estivale se termine, les réparations du village reprennent : routes, Beauséjour, accès handicapés..."
header = "/media/beausejour-cheneau-2021-3.jpg"
icon = ""
subtitle = ""
title = "Redémarrage des chantiers"
weight = 1

+++
L'été, il faut bien accueillir ceux qui viennent nous rendre visite. Sauf en cas d'urgence, les gros travaux sont alors provisoirement interrompus. Mais avec l'arrivée de l'automne, il est temps de reprendre les réparations, le village en a bien besoin !

## Goudronnage des routes et chemins communaux

Les routes et chemins communaux sont en mauvais état, faute d'un entretien régulier. Tout ne pourra pas être réparé en une seule fois, mais une première tranche de travaux est en cours.

Elle concerne la montée de l'ancienne école publique, la route de Bobigneux, le chemin accédant chez Mme Charmetant reliant le tronçon goudronné et de très nombreux trous sur les chemins et routes de la Commune.

{{<grid>}} {{<bloc>}}

![](/media/goudronnage-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/goudronnage-2021-4.jpg)

{{</bloc>}}{{</grid>}}

## Rampe d'accès pour les personnes à mobilité réduite

{{<grid>}} {{<bloc>}}

![](/media/rampe-d-acces-mairie-2021.jpg)

{{</bloc>}}{{<bloc>}}

La pente de la rampe d'accès à la bibliothèque, à l'agence postale communale et à la Mairie n'était pas conforme aux normes en vigueur pour les personnes à mobilité réduite.

Le problème est en cours de résolution.

{{</bloc>}} {{</grid>}}

## Hôtel Beauséjour

Les conventions pour le déplacement des lignes électrique et de téléphone ont été enfin signées par les riverains concernés. Les travaux préparatoires à la démolition vont pouvoir démarrer. Bientôt des nouvelles sur cet important chantier.

Il est plus que temps qu'il démarre, comme le montre cette intervention d'urgence d'un cheneau, déboité par le vent, qui menaçait les passants. Elle a eu lieu cette semaine grâce à la nacelle de l'équipe d'ouvriers qui intervient actuellement sur l'hôtel Monarque. Merci à eux !

{{<grid>}} {{<bloc>}}

![](/media/beausejour-cheneau-2021-4.jpg)

{{</bloc>}} {{<bloc>}}

![](/media/beausejour-cheneau-2021-5.jpg)

{{</bloc>}} {{</grid>}}

Rappelons que tous ces travaux, quand ils ne sont pas pris en charge par un budget extracommunal, sont subventionnés à 80% grâce à l'appui de la Préfecture et de la Région.