+++
date = 2021-10-03T22:00:00Z
description = "4 -3 contre St Jean de Muzols 3"
header = "/media/foot-02-10-2021-1.jpg"
icon = ""
subtitle = ""
title = "Ils ont gagné !"
weight = 1

+++
Malgré une météo compliquée ce samedi, les joueurs de foot de Pailhares-Lalouvesc ont gagné 4 -3 contre St Jean de Muzols 3 sur le stade de Lalouvesc.

Les buteurs sont Lionel avec un triplet et Joris avec un corner rentrant.

![](/media/foot-02-10-2021-2.jpg)

Le match s est terminé avec un petit casse-croute bien mérité.

Félicitations à nos footballeurs !