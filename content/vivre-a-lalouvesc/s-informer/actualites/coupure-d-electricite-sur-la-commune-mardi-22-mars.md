+++
date = 2022-02-16T23:00:00Z
description = "Durée approximative : 2h30"
header = "/media/enedis.jpg"
icon = ""
subtitle = ""
title = "Coupure d'électricité sur la Commune, mardi 22 mars"
weight = 1

+++
Enedis nous informe qu'une coupure d'électricité aura lieu sur la Commune mardi 22 mars 2022 entre 13h30 et 17h00 d'une durée approximative de 2 heures et 30 minutes.

Extrait du communiqué :

> Votre interlocuteur : Seraphin BERNARDON  
>  Téléphone : 0972675069  
>    
>  Conscients de la gêne occasionnée, nous mettons tout en œuvre pour réduire autant que possible les perturbations liées à ces travaux. L’alimentation pourra être rétablie à tout moment avant la fin de la plage indiquée.  
>    
>  Si vous restiez exceptionnellement sans courant après la période indiquée, notre service dépannage est à votre disposition au : 0 972 67 50 .. (appel non surtaxé) suivi des deux chiffres de votre département.  
>    
> Enfin, pour protéger au mieux vos appareils sensibles, nous vous recommandons de les débrancher avant l'heure de début de coupure indiquée, et de ne les rebrancher qu’une fois le courant rétabli.