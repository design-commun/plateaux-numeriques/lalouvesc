+++
date = 2022-12-02T23:00:00Z
description = "Les inscriptions sont ouvertes pour les enfants de 3 à 11 ans"
header = "/media/centre-de-loisir-jaloine-2921.jpg"
icon = ""
subtitle = ""
title = "Programme des vacances de Noël à Jaloine"
weight = 1

+++
L'équipe d'animation a prévu plein de magie pour ces vacances et nous espérons vous voir nombreux à notre veillée du 22 décembre ouverte à tous de 19h30 à 21h.

C'est Noël à Jaloine !

Le centre de loisirs sera ouvert du 19 au 23 décembre.

Renseignements et inscriptions dès aujourd'hui dans la limite des places disponibles. [Fiche d'inscription](/media/fiche-d-inscription-jaloine-noel-2022.pdf).

Nous vous souhaitons de très belles fêtes de fin d'année

L'équipe d'animation

![](/media/prg-jaloine-2022-noel-1.png)

![](/media/prg-jaloine-2022-noel-2.png)