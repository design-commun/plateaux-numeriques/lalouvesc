+++
date = 2021-07-07T22:00:00Z
description = "Précipitez vous, le 5 sept à Lalouvesc affiche presque complet. si vous voulez vendre des objets..."
header = "/media/brocante-1.jpg"
icon = ""
subtitle = ""
title = "Attention ! Plus que quelques places pour la brocante..."
weight = 1

+++
Le Comité des fêtes nous communique :

_Contre toute attente nous affichons déjà presque complet pour la brocante du 5 sept !!! Il ne reste en effet seulement que quelques places._

_Les_ [_prochains bulletins_](/media/brocante-bulletin_inscription_2021.pdf) _que nous recevrons seront inscrits dans l'ordre d'arrivée. A la clôture définitive nous retournerons les inscriptions qui ne pourront pas être prises en compte._

_Cette année encore cet évènement a connu un réel succès._

_A ceux qui ne pourront pas venir nous donnons rendez-vous l'année prochaine._