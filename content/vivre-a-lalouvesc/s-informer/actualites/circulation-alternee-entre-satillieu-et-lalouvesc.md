+++
date = 2021-11-22T23:00:00Z
description = "Travaux de voirie entre le 24/11/2021 et le 30/11/2021"
header = ""
icon = ""
subtitle = ""
title = "Circulation alternée entre Satillieu et Lalouvesc"
weight = 1

+++
La circulation sera alternée en deux points de la RD 578A entre Satillieu et Lalouvesc du 24 au 30 novembre.

Société chargée des travaux : Eiffage.

Nom de la personne chargée de l'intervention : M. David Bodin tél 06 20 44 38 88, mail : david.bodin@eiffage.com