+++
date = 2022-03-07T23:00:00Z
description = "Une équipe du Journal de 13h est passée au village"
header = "/media/tf1-mars-2022-1.jpg"
icon = ""
subtitle = ""
title = "Lalouvesc, bientôt sur TF1"
weight = 1

+++
Une équipe de tournage de TF1 est passée ce mardi 8 mars à Lalouvesc pour réaliser un reportage sur la distance entre les villages et les services de l'Etat. Ils ont interviewé le Maire et rencontré plusieurs commerçants.

Le sujet devrait durer une dizaine de minutes. La date de l'émission n'est pas encore connue. Nous l'annoncerons dès qu'elle sera fixée.

{{<grid>}}{{<bloc>}}

![](/media/tf1-mars-2022-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/tf1-mars-2022-3.jpg)

{{</bloc>}}{{</grid>}}