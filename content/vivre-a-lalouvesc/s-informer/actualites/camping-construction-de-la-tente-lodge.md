+++
date = 2021-05-20T19:00:00Z
description = "Tout est en place, il ne reste plus qu'un peu de ménage..."
header = "/media/camping-construction-lodge-3.jpg"
icon = ""
subtitle = ""
title = "Camping : construction de la tente lodge"
weight = 1

+++
Les derniers éléments du renouveau du camping se mettent en place cette semaine. Ci-dessous un aperçu de l'installation en cours de la tente lodge, cinq places, deux chambres, sur plancher bois, poêle à bois, entièrement équipée, coin cuisine sur la terrasse de 10m2. Le confort canadien en Ardèche.

Très bientôt des nouvelles du cottage.

Pour réserver votre place, [c'est ici](/decouvrir-bouger/hebergement-restauration/camping/).

{{<grid>}}

{{<bloc>}}

![](/media/camping-construction-lodge-20.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-18.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-23.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-22.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-14.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge16.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-5.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/camping-construction-lodge-2.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/camping-construction-lodge-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-9.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-12.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge10.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-8.jpg)

{{</bloc>}}

{{</grid>}}