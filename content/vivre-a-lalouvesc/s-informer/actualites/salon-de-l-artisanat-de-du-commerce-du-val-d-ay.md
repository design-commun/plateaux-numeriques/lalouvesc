+++
date = 2022-09-22T22:00:00Z
description = "Dimanche 25 sept à Ardoix"
header = "/media/atout-val-d-ay-logo-2022.jpg"
icon = ""
subtitle = ""
title = "Salon de l'Artisanat et du Commerce du Val d'Ay"
weight = 1

+++
![](/media/atout-val-d-ay-2022.jpg)

Menu à 16€ préparé par la truffolie de Saint-Alban-d'Ay :

* Terrine de campagne et toast de pain
* Rôti de porc, sauce poivre et pomme de terre fondante
* Brie
* Tarte au citron