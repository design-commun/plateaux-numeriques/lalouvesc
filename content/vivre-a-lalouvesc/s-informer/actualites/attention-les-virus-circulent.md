+++
date = 2022-12-19T23:00:00Z
description = "Covid, grippe, bronchiolite sont bien présentes dans la région... et dans le village"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Attention, les virus circulent !"
weight = 1

+++
L'ARS communique :

Alors que les fêtes de fin d’année approchent dans un contexte de recrudescence des épidémies dues aux virus de l’hiver (Covid-19, grippe, bronchiolite), l’Agence régionale de santé Auvergne-Rhône-Alpes appelle chacun et chacune d’entre nous à maintenir les réflexes que nous avons adoptés pour limiter la transmission et l’impact des infections hivernales.

Pour se protéger et protéger ses proches, les plus fragiles notamment, et pour soulager notre système de santé qui est actuellement très mobilisé, la responsabilité et la solidarité de toutes et tous est plus que jamais nécessaire.

## La circulation active des virus en Auvergne-Rhône-Alpes

**Concernant le Covid-19,** d’après le [dernier bulletin de Santé publique France](https://www.auvergne-rhone-alpes.ars.sante.fr/media/103265/download?inline), **en semaine 49 l’épidémie continue de progresser dans la région**. Pour la semaine glissante du 6 au 13 déc., le taux d’incidence atteint 710 cas pour 100 000 habitants dans la région (taux France : 620).   
**L’impact hospitalier augmente**, le nombre de nouvelles hospitalisations pour Covid-19 continue d’augmenter en semaine 49 (+18%) ; le nombre de décès hospitaliers augmente également.  
Au 16 décembre, 2 830 patients sont hospitalisés pour Covid-19 dans la région dont 159 patients pris en charge en soins critiques.

**Concernant la grippe,** d’après Santé publique France, la région **Auvergne-Rhône-Alpes est pour la deuxième semaine en phase épidémique, avec une augmentation d’activité relevée par tous les indicateurs** : En médecine libérale, d’après les données SOS Médecins, le nombre d’actes poursuit son augmentation en semaine 49, avec 894 actes pour grippe/syndrome grippal enregistrés (_vs_ 645 actes en S48), ce qui représente 9,9% de la part d’activité.   
En médecine hospitalière, selon les données du réseau Oscour®, les recours aux urgences tous âges pour diagnostic de grippe/syndrome grippal sont en forte augmentation. 437 passages aux urgences pour grippe/syndrome grippal ont été enregistrés (+ 121% par rapport à la semaine 48), sur ces 437 passages, 49 ont été suivis d’une hospitalisation.

**S’agissant de la bronchiolite,** le point de Santé publique France du 16 décembre **indique que la dynamique épidémique de la bronchiolite diminue en Auvergne-Rhône-Alpes, en semaine 49 mais reste très élevée.** Dans la région, en semaine 49, 1 211 passages aux urgences pour bronchiolite ont été relevés, soit 29,2% des passages dans cette classe d’âge. Parmi ces passages, 411 ont été suivis d’une hospitalisation, soit un taux d’hospitalisation de 33,9% pour ce diagnostic. Les hospitalisations pour bronchiolite représentent 63,1% de l’ensemble des hospitalisations dans cette classe d’âge.

## Les gestes barrières sont des protections contre de nombreuses contaminations et appliqués sérieusement, ils sauvent des vies !

Oui, les gestes barrières sauvent des vies. Ils protègent de l’ensemble des virus de l’hiver (Covid-19, grippe, bronchiolite, gastro-entérites, etc.) et doivent (re)devenir des réflexes au quotidien :

* **Porter un masque dans les lieux clos, fréquentés** (transports en commun, salles de concerts, en co-voiturage, etc.)


* **Se laver régulièrement les mains** ou utiliser une solution de lavage hydro-alcoolique**,**
* **Aérer fréquemment les pièces et lieux de vie** 10 minutes tous les jours,
* **Éviter de se serrer la main,** éviter les embrassades notamment des personnes fragiles,
* **Tousser ou éternuer dans son coude,**
* **Utiliser un mouchoir à usage unique.**

Pour protéger les enfants en bas âge **de la bronchiolite**, des gestes complémentaires peuvent être appliqués > [Consulter](https://www.auvergne-rhone-alpes.ars.sante.fr/bronchiolite-comment-proteger-son-enfant)

## Soyez doublement protégé : se vacciner contre la grippe et le Covid-19 c’est possible !

Contre le Covid-19 et la grippe, **la vaccination reste un levier de protection majeur** qui permet de réduire considérablement le risque de formes graves et de décès.

**Elle est ouverte à tous les Français qui le souhaitent et, est très fortement recommandée pour** :

* Les personnes âgées de 60 ans et plus
* Les résidents d’EHPAD et USLD, quel que soit leur âge
* Les personnes immunodéprimées, quel que soit leur âge
* Les personnes souffrant de comorbidité(s), quel que soit leur âge
* Les femmes enceintes dès le 1er trimestre de grossesse
* Les personnes vivant dans l’entourage ou en contact régulier avec des personnes vulnérables
* Les professionnels des secteurs sanitaire et médico-social.

La double vaccination grippe-Covid19 est nécessaire pour être protégé contre les deux maladies ; pour rappel, **les deux injections peuvent être réalisées en même temps** chez votre médecin, pharmacien, infirmier.

**A consulter :**

[Vaccination contre la grippe saisonnière | ameli.fr | Assuré](https://www.ameli.fr/assure/sante/assurance-maladie/campagnes-vaccination/vaccination-grippe-saisonniere)

[Vaccination Covid-19 - Ministère de la Santé et de la Prévention (solidarites-sante.gouv.fr)](https://solidarites-sante.gouv.fr/grands-dossiers/vaccin-covid-19/je-suis-un-particulier/)

## Besoin de soins immédiats ? Ne vous déplacez pas directement aux urgences : appelez en priorité votre médecin traitant, ou le 15

Si vous avez un problème de santé qui ne peut pas attendre l’ouverture habituelle des cabinets médicaux, ne vous déplacez pas directement aux urgences : **appelez en priorité votre médecin traitant.**

Si vous ne parvenez pas à le joindre, **contactez le 15 avant de vous déplacer aux urgences**.

Les régulateurs du Centre 15 pourront vous proposer **une orientation adaptée** à votre état de santé :

* Conseils médicaux nécessaires par un médecin régulateur ;
* Orientation vers un médecin de garde ou organisation d’une visite à domicile ;
* En cas d’urgence, un régulateur vous orientera vers un service d’urgence ou organisera l’intervention d’une équipe adaptée (ambulance, SMUR, etc.).