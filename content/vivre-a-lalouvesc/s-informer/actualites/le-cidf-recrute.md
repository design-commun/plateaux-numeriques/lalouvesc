+++
date = 2023-02-16T23:00:00Z
description = "Deux temps partiels sur Aubenas et Tournon"
header = "/media/cidf-2023.png"
icon = ""
subtitle = ""
title = "Le CIDFF recrute"
weight = 1

+++
Le **CIDFF (Centre d'information sur les droits des femmes et des familles)** de l’Ardèche recrute deux Intervenant-e-s en espace de rencontre :

* **1 poste sur AUBENAS** : CDI, Temps partiel de 17.5 heures mensuelles, Diplôme social et débutant accepté, Travail d’appoint, pas de développement du poste prévu, Date d’embauche : 19 Mai 2023
* **1 poste sur TOURNON-SUR-RHÔNE** : CDI, Temps partiel de 17.58 heures mensuelles, Dipôme social et débutant accepté, Travail d’appoint, pas de développement du poste prévu, Date d’embauche : 5 Juin 2023

Possibilité de recevoir les fiches de poste par mail sur demande auprès  du secrétariat en précisant bien la ville de rattachement.

#### Vous voulez postuler ??

Merci d'envoyer une lettre de motivation et un Curriculum Vitae, par mail, au CIDFF de l'Ardèche à cidff07@cidff07.fr.

Restant à votre disposition pour toute information.

Cordialement,    Le Secrétariat

CIDFF - MIFE de l'Ardèche  Quartier Les Oliviers  Pôle de Services  30 avenue de Zelzate  07200 AUBENAS  04.75.93.31.70  cidff07@cidff07.fr

Site internet : https://ardeche.cidff.info/