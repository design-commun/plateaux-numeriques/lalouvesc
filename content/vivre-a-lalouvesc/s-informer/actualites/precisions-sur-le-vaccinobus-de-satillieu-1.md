+++
date = 2021-03-25T23:00:00Z
description = "La vaccination sera proposée du 6 au 9 avril, elle est élargie à toutes personnes de plus de 70 ans"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Précisions sur le vaccinobus de Satillieu"
weight = 1

+++
Rappel : un “vaccinobus” (un bus dans lequel seront pratiquées les vaccinations contre la Covid-19) va s’installer sur Satillieu. Les dates sont maintenant fixées du 6 au 9 avril.

**La tranche d'âge concernée a été élargie à toutes les personnes de plus de 70 ans.** La vaccination sera effectuée par les pompiers.

Toutes les personnes intéressées, même déjà inscrites sur une autre liste d’attente, doivent s’inscrire en cliquant sur [ce formulaire](https://forms.gle/3UdEiiLJeW9Az2ST9) **jusqu’au 31 mars inclus.** Les inscrits seront contactés par téléphone pour la prise de rendez-vous. Pour ceux qui auraient des difficultés pour se déplacer, merci de contacter la Mairie de Lalouvesc.

**N’hésitez pas à aider ceux qui n’arriveraient pas à s’inscrire. Le secrétariat de mairie peut aussi faire les démarches le cas échéant.**