+++
date = 2021-12-21T23:00:00Z
description = "Le coin des enfants, le coin des résidents de la Maison de retraite... les derniers occupants de l'hôtel Beauséjour"
header = "/media/decorations-noel-mairie-2021-10.jpg"
icon = ""
subtitle = ""
title = "Décorations : tout est en place !"
weight = 1

+++
Après le square et la place Marrel, voici, comme promis, la présentation des dernières décorations : celles mises en place par les résidents de la Maison de retraite, le coin des enfants devant la Mairie et, surprise !, le dernier éclairage des fenêtres de l'hôtel Beauséjour.

L'ensemble des décorations pour les fêtes de fin d'année sont maintenant installées et le village sourit, de jour comme de nuit, [tout illuminé](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/quand-lalouvesc-s-illumine/) malgré la conjoncture difficile, pour le plaisir des Louvetous et pour accueillir les visiteurs.

### Le coin des seniors, place du Lac

A côté du square de la place du Lac, les animatrices de la Maison du Balcon des Alpes ont disposé les décorations fabriquées par les résidents.

![](/media/decorations-noel-ephad-2021-10.jpeg)

{{<grid>}}{{<bloc>}}

![](/media/decorations-noel-ephad-2021-1.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/decorations-noel-ephad-2021-4.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/decorations-noel-ephad-2021-3.jpeg)![](/media/decorations-noel-ephad-2021-5.jpeg)

{{</bloc>}}{{<bloc>}}

![](/media/decorations-noel-ephad-2021-7.jpeg)![](/media/decorations-noel-ephad-2021-8.jpeg)

{{</bloc>}}{{</grid>}}

![](/media/decorations-noel-ephad-2021-6.jpeg)

### Le coin des enfants, devant la Mairie

En face de la Mairie, un bonhomme de neige a été installé à côté du calendrier de l'Avent, maintenant presque complet avec les dessins des écoliers de Lalouvesc. Vous pouvez vous y photographier..

{{<grid>}}{{<bloc>}}

![](/media/decorations-noel-mairie-2021-6.jpg){{</bloc>}}{{<bloc>}}

![](/media/decorations-noel-mairie-2021-3.jpg)

{{</bloc>}}{{</grid>}}

![](/media/decorations-noel-mairie-2021-7.jpg)

Et un magnifique petit train a rejoint l'énorme sapin.

{{<grid>}}{{<bloc>}}

![](/media/decorations-noel-mairie-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decorations-noel-mairie-2021-4.jpg)

{{</bloc>}}{{</grid>}}

### L'hôtel Beauséjour s'éclaire pour ses derniers occupants...

Et, tiens, tiens, L'hôtel Beauséjour nous fait un dernier clin d’œil avant démolition.

![](/media/decorations-noel-beausejour-2021-1.jpg)

![](/media/decorations-noel-beausejour-2021-2.jpg)![](/media/decorations-noel-beausejour-2021-3.jpg)![](/media/decorations-noel-beausejour-2021-4.jpg)