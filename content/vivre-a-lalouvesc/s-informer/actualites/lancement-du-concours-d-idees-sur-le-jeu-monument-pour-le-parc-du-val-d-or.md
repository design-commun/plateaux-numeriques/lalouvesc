+++
date = 2021-11-23T23:00:00Z
description = "Ouvert aux étudiants en école d'architecture, de design et de la filière bois"
header = "/media/journee-citoyenne.jpg"
icon = ""
subtitle = ""
title = "Lancement du concours d'idées sur le jeu-monument pour le parc du Val d'Or"
weight = 1

+++
Le Comité technique réunissant des élus et des architectes amis du village a beaucoup travaillé pour préparer un concours d'idées ouvert aux étudiants pour le jeu-monument prévu sur le parc du Val d'Or suite à la démolition de l'hôtel Beauséjour. Ca y est, tout est prêt.

[Son règlement](https://www.lalouvesc.fr/projets-avenir/opportunites/concours-d-idees-sur-un-jeu-monument-sur-le-parc-du-val-d-or/) va être prochainement envoyé aux écoles d'architecture, de design et de la filière bois. N'hésitez pas à le faire suivre autour de vous et par vos réseaux. Plus nous recevrons de candidatures, plus nous pourrons brasser d'idées et trouver la meilleure.

Les inscriptions sont prises jusqu'au 10 janvier 2022.