+++
date = 2022-01-02T23:00:00Z
description = "Faites suivre aux étudiants en architecture, design et construction-bois"
header = "/media/journee-citoyenne.jpg"
icon = ""
subtitle = "Faites suivre aux étudiants en architecture, design et construction-bois"
title = "Attention plus qu'une semaine pour s'inscrire au concours d'idées sur le jeu-monument !"
weight = 1

+++
Pour l’[inscription au concours](https://www.lalouvesc.fr/projets-avenir/opportunites/concours-d-idees-sur-un-jeu-monument-sur-le-parc-du-val-d-or/) **avant le 11 janvier**.

Présentation du projet [courte](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/un-jeu-monument-sur-le-parc-du-val-d-or/) et [longue](https://docs.google.com/document/d/10wbSzgavO6ykDykgHuMkqLvhkm5nBu2t93pf5AJ1k4U/edit?usp=sharing).

## **Faites-le savoir à tous les intéressés potentiels !**