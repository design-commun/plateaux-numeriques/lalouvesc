+++
date = 2021-12-02T23:00:00Z
description = "Le foyer de ski de fond vous propose des raquettes pour de belles balades. Se renseigner au Vival."
header = "/media/chasse-neige-28-nov-2021.jpg"
icon = ""
subtitle = ""
title = "La neige est là, partez en raquettes ce week end !"
weight = 1

+++
Vous voulez profiter de la nature enneigée à Lalouvesc ? Le foyer de ski de fond loue des raquettes à un prix défiant toutes concurrences : 5 € la journée.

Elles sont à récupérer à l'épicerie le Vival. Attention à bien respecter les horaires, le magasin est fermé de 13h à 15h et le soir à partir de 19 h.

Espérons que la neige tienne... Lalouvesc, que du plaisir !

![](/media/les-alpes-o-de-framond.jpg)