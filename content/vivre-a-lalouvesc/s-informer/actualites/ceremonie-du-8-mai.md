+++
date = 2021-05-10T22:00:00Z
description = "Commémoration de la fin de la seconde guerre mondiale.   "
header = "/media/8-mai-2021.jpg"
icon = ""
subtitle = ""
title = "Cérémonie du 8 mai"
weight = 1

+++
La commémoration du 8 mai 1945, [fin de la seconde guerre mondiale](https://fr.wikipedia.org/wiki/8_mai_1945), a réuni autour du Monument aux morts trois anciens combattants, le Maire et le 1er adjoint ainsi que l'adjudant-chef de gendarmerie et le père Olivier de Framond, dans le respect des conditions sanitaires actuelles.

Une gerbe a été déposée. Le Maire a lu le manifeste et le père Olivier a récité une prière, suivie d'un moment de recueillement.