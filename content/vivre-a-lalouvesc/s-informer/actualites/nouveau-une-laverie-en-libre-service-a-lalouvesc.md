+++
date = 2022-06-23T22:00:00Z
description = "Située sur le Camping, ouverte à tous"
header = "/media/laverie-2022-3.jpg"
icon = ""
subtitle = ""
title = "Nouveau : une laverie en libre-service à Lalouvesc"
weight = 1

+++
La Mairie a installé une machine à laver et un sèche-linge professionnels au Camping du Pré du Moulin. Ceux-ci servent à laver et sécher sur place la literie des hébergements communaux mis en location, mais ils sont aussi accessibles à toutes et tous.

**Ces machines sont en libre-service pour tous, Louvetous et visiteurs.**

Horaire d'ouverture de la salle : 9h-19h

Tarifs : 5€ par lavage, 5€ par séchage. Les jetons sont à acheter au bureau du Camping et à l'agence postale. La lessive est fournie.

Capacité : 9kg de linge

{{<grid>}}{{<bloc>}}

![](/media/laverie-2022-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/laverie-2022-12.jpg)

{{</bloc>}}{{</grid>}}