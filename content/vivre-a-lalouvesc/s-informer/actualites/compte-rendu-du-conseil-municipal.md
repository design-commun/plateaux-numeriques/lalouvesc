+++
date = 2021-04-14T22:00:00Z
description = "Séance du 7 avril 2021"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Compte rendu du Conseil municipal"
weight = 1

+++
Le compte-rendu du Conseil municipal du 7 avril est [accessible ici](/media/2021-3-conseil-municipal-lalouvesc.pdf).

Ordre du jour :

1. COMMISSION FINANCES
   1. Demande de subvention déneigement
   2. Taux des taxes locales directes 2021
   3. Tarif de l’eau et l’assainissement
   4. Modifications des régies municipales
   5. Ouverture d’un compte DFT
   6. Ouverture d’un service DPfip
   7. Tarifs nouveaux hébergements du camping
   8. Tarif du minigolf
2. COMMISSION GESTION
   1. Permis de démolition
   2. Assurance risque statutaire
3. COMITÉ VIE LOCALE
   1. Convention OGEC