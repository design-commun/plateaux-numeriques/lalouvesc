+++
date = 2022-05-01T22:00:00Z
description = "Dépôt de plaintes en ligne"
header = "/media/thesee-e-escroquerie-2022.jpg"
icon = ""
subtitle = ""
title = "Une plateforme contre les e-escroqueries"
weight = 1

+++
La gendarmerie communique :

_Depuis le 15 mars dernier, une plateforme de recueil des signalements ou plaintes concernant des escroqueries par internet (e-escroquerie) a été lancée._

_Désormais, grâce à la plateforme Thésée, vous pouvez déposer plainte en ligne rapidement et facilement. Il suffit de vous rendre sur le site_ [_service-public.fr_](http://service-public.fr) _via le lien suivant et de suivre la démarche :_ [_https://www.service-public.fr/particuliers/vosdroits/N31138_](https://www.service-public.fr/particuliers/vosdroits/N31138 "https://www.service-public.fr/particuliers/vosdroits/N31138")

### _Qu'est-ce qu'une e-escroquerie ?_

*  Piratage de compte mail ou de réseau social avec demande d’argent :  
  Votre adresse mail ou votre profil sur les réseaux ont été piratés et de l’argent a été demandé à l’un de vos contacts, en votre nom.
* Escroquerie à la petite annonce, faux acheteur ou faux vendeur :  
  Vous avez été escroqué(e) par un faux acheteur suite à la vente d’un produit en ligne sur un site de petites annonces.
* Fausse location :  
  Vous avez été escroqué(e) à l’occasion d’une démarche pour louer un bien immobilier en ligne.
* Ransomware :  
  Les fichiers de votre ordinateur, tablette ou téléphone mobile ont été cryptés et une rançon vous est demandée.
* Chantage en ligne :  
  Vous faites l’objet de menaces en ligne, de diffusion d’images portant atteinte à votre honneur.
* Escroquerie aux sentiments :  
  Lors d’une relation en ligne, vous avez été incité(e) par des moyens frauduleux à verser de l’argent.
* Faux site de vente :  
  Vous avez été escroqué(e) lors d’un achat sur un site de vente en ligne frauduleux.