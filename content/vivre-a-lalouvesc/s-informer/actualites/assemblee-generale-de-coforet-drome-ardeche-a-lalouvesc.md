+++
date = 2022-05-08T22:00:00Z
description = "La Mairie a accueilli l'assemblée le jeudi 5 mai"
header = "/media/coforet-2022-1.jpg"
icon = ""
subtitle = ""
title = "Assemblée générale de Coforet Drôme-Ardèche à Lalouvesc"
weight = 1

+++
[Coforet](https://www.coforet.com/) est une coopérative forestière, implantée dans les régions Auvergne-Rhône-Alpes et Bourgogne-Franche-Comté, qui accompagne les propriétaires dans la gestion de leur(s) parcelle(s) forestière(s) (cf. [livret](https://www.coforet.com/wp-content/uploads/2022/01/LivretPourquoiIntervenirEnForet_RVB.pdf)).

Elle a tenu son assemblée générale pour la Drôme et l'Ardèche dans les locaux de la Mairie jeudi 5 mai.

Nous avons été heureux de l'accueillir, la forêt est dans l'ADN du village. Les responsables nous ont remercié et ont ajouté : _C’était tout simplement parfait..._ 

![](/media/coforet-2022-4.jpg "De gauche à droite : Jacques Burriez, maire de Lalouvesc, le président de COFORET, Henri BATTIE, le président de la section Drôme-Ardèche, André Bergeron et Lionel PIET, le directeur de COFORET (à droite)")

{{<grid>}}{{<bloc>}}

![](/media/coforet-2022-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/coforet-2022-2.jpg)

{{</bloc>}}{{</grid>}}