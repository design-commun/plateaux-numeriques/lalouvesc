+++
date = 2021-04-01T22:00:00Z
description = "Avril 2021  "
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = ""
title = "Le bulletin d'information n°9 est paru"
weight = 1

+++
Les incertitudes sanitaires sont toujours devant nous. Pourtant notre village est confiant en son avenir. Le budget 2021 ouvre enfin la porte aux investissements et, surtout, les Louvetonnes et les Louvetous ont montré par leur engagement citoyen sur le camping au mois de mars qu’ils n’étaient pas prêts à laisser se dégrader leur bien commun. Parallèlement, la saison estivale se prépare et nous disposons d’un outil de communication efficace. Voilà quelques sujets développés dans ce bulletin.  
Vous y trouverez aussi, comme toujours plein d’autres informations et quelques sourires.

[Lire la suite...](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/9-la-relance/)