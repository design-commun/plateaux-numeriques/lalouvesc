+++
date = 2022-07-20T22:00:00Z
description = "Ordre du jour et lien vers le PV"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Le PV du Conseil municipal du 11 juillet est en ligne"
weight = 1

+++
Conformément à l’[évolution de la réglementation au 1er juillet 2022 concernant la rédaction et la publicité des procès-verbaux et des délibérations](https://www.collectivites-locales.gouv.fr/institutions/le-conseil-municipal#__RefHeading__604_1423777299), ce document publié sur internet comprend les faits et décisions de la séance du conseil municipal et l’ensemble des délibérations (accessibles en annexe), il fait office de procès-verbal. La liste des délibérations est publiée le même jour en parallèle sur internet. Il n’y a plus d’affichage extérieur, mais ces documents sont consultables sur papier en mairie.

[**PV du Conseil municipal du 11 juillet 2022**](/media/2022-3-conseil-municipal-lalouvesc.pdf)

## Ordre du jour

1. COMMISSION FINANCES
   1. Première décision modificative
   2. Passage à la nouvelle nomenclature M 57 au 1er janvier 2023
2. COMMISSION GESTION
   1. Tarif pour le lave-linge et le sèche-linge en libre-service au camping
   2. Elargissement de la zone d’assainissement collectif
3. COMITÉ VIE LOCALE
   1. Convention ouverture au public de sentier
   2. Convention autorisation pose de figurines
4. COMITÉ DÉVELOPPEMENT
   1. Composition et réunions pour l’année 2022 du Comité de développement
   2. Résultat du concours d’idées sur le Jeu-Monument
   3. Rencontre avec les sœurs du Cénacle et premier comité de pilotage de l’étude du SCOT sur le Cénacle et Sainte Monique
   4. Acceptation du devis du maître d’œuvre Archipolis pour l’espace «Beau séjour»
   5. Convention EPORA
5. DIVERS