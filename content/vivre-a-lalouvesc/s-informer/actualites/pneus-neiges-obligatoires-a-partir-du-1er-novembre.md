+++
date = 2021-09-30T22:00:00Z
description = "Carte du territoire concerné et précisions"
header = "/media/carte-2021-pneus-neiges.jpg"
icon = ""
subtitle = ""
title = "Pneus neiges obligatoires à partir du 1er novembre"
weight = 1

+++
L'arrêté de la préfecture  a été publié. [Communiqué](http://www.ardeche.gouv.fr/IMG/pdf/cp_obligation_pneus_hiver.pdf) ; carte et extraits :

![](/media/carte-2021-pneus-neiges.jpg)

> Ainsi, les véhicules légers, utilitaires et les camping-cars devront soit détenir des chaînes métalliques à neige ou textiles permettant d’équiper au moins deux roues motrices, soit être équipés de quatre pneus hiver.  
> Les autocars, autobus et poids lourds sans remorque ni semi-remorque sont également soumis à l’obligation avec le choix entre les chaînes ou les pneus hiver. Les poids lourds avec remorque ou semi-remorque doivent, quant à eux, détenir des chaînes à neige permettant d’équiper au moins deux roues motrices, même s’ils sont équipés de pneus hiver.  
> Cette obligation ne s’applique pas aux véhicules équipés de pneus à clous. (...)
>
> **Caractéristiques des "pneus hiver"**  
> Les pneumatiques “hiver” sont identifiés par la présence conjointe du marquage “symbole alpin” et de l’un des marquages “M+S”, “M.S” ou “M&S”  
> Jusqu’au 1er novembre 2024, les pneus uniquement marqués "M+S" seront tolérés. Les pneus 4 saisons (4S, All Weather, All Season) n’ont pas de définition réglementaire : pour être considérés comme pneus hiver, il faut qu’ils soient estampillés « 3PMSF » , ou au minimum « M+S » pour la période transitoire des 3 premières années d’application de la nouvelle réglementation.  
> A partir du 1er novembre 2024, seuls les pneumatiques 3PMSF seront admis en équivalence aux chaînes. L’achat et l’utilisation d’autres « pneus neige » resteront possibles, mais les usagers devront dans ce cas, détenir en plus, des chaînes pour circuler du 1er novembre au 31 mars dans les zones concernées par la mesure.