+++
date = 2022-11-02T23:00:00Z
description = "Porte ouverte, manoeuvre et démonstration des pompiers"
header = "/media/pompiers-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Le Chemin Grosjean interdit à la circulation samedi 5 novembre et dimanche 13 novembre au matin"
weight = 1

+++
La circulation automobile sera interdite sur le chemin Grosjean le samedi 5 novembre de 8h à 14h en raison des portes ouvertes à la caserne des pompiers.

La circulation, le stationnement et l'accès piéton seront interdits sur le chemin Grosjean le dimanche 13 novembre de 7h à 13h en raison des manœuvres prévues des pompiers.

Pour en savoir plus sur [les pompiers volontaires de Lalouvesc]().

![](/media/exercice-pompiers-ecole-2022-1.jpg)