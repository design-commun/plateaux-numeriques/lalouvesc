+++
date = 2021-12-13T23:00:00Z
description = "Le Comité réunit élus et non-élus et réfléchit aux orientations à prendre pour le développement du village"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Le compte-rendu de la seconde réunion du Comité de développement est en ligne"
weight = 1

+++
Le Comité est composé d’élus et de non-élus. Il a pour objectif d’éclairer la réflexion du Conseil par un recueil plus large des points de vue et des expertises.

Le Comité est consultatif, il formule des avis, mais les décisions restent réglementairement de la responsabilité du Conseil municipal.

Il se réunit deux fois par an. La dernière réunion s'est tenue le 26 novembre dernier. Son [compte-rendu](/media/reunion-du-comite-de-developpement-novembre-2021.pdf) est publié. Il contient de nombreuses suggestions pour le développement du village.

Pour rappel les comptes-rendus du Comité sont accessibles sur [cette page]().