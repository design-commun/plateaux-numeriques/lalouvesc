+++
date = 2021-09-02T22:00:00Z
description = "Le risque d'allergie liée à l'ambroisie est de niveau très élevé en Ardèche. Il est encore temps de la détruire avant qu'elle ne libère sa graine."
header = "/media/ambroisie-wkp.jpg"
icon = ""
subtitle = ""
title = "Attention à l'ambroisie !"
weight = 1

+++
Le risque d'allergie liée à l'ambroisie est de niveau très élevé en Ardèche. Il est encore temps de la détruire avant qu'elle ne libère sa graine.

Pour la reconnaître : [https://www.stopambroisie.com/comment-reconnaitre-ambroisie/](https://www.stopambroisie.com/comment-reconnaitre-ambroisie/ "https://www.stopambroisie.com/comment-reconnaitre-ambroisie/")

### Que faire ?

Sur ma propriété : [**je l'arrache !**](http://www.ambroisie.info/pages/elimine3.htm)

Hors de ma propriété, sur un terrain public, s'il y a seulement quelques plants : [**je l'arrache !**](http://www.ambroisie.info/pages/elimine3.htm)

Hors de ma propriété, s'il y a en a beaucoup : **je signale** la zone infestée par un des moyens suivants :

* Onglet [signalement](https://signalement-ambroisie.atlasante.fr/dashboard) de ce site
* Application mobile Signalement Ambroisie
* Mél : contact@signalement-ambroisie.fr
* Téléphone : 0 972 376 888

![](/media/ambroisie-sept-2021.jpg)