+++
date = 2022-01-03T23:00:00Z
description = "Rue des Cévennes interdite aux voitures pour 10 jours"
header = "/media/beausejour-photo-o-de-framond.jpg"
icon = ""
subtitle = ""
title = "Beauséjour, c'est parti ! Circulation réduite"
weight = 1

+++
Voici donc enfin le dernier épisode d'un mauvais feuilleton long de plusieurs années : la démolition de l'hôtel Beauséjour démarre cette semaine. Les travaux sont effectués par l'entreprise Mounard TP.

Par arrêté du Maire, pendant la durée des travaux (du 4 au 15 janvier inclus), la rue des Cévennes sera interdite aux véhicules légers (voitures). Seuls les poids lourds seront autorisés de 7h30 à 18h et un système de circulation alternée sera mis en place.

La déviation pour les voitures passera par le chemin Grosjean, dans les deux sens.