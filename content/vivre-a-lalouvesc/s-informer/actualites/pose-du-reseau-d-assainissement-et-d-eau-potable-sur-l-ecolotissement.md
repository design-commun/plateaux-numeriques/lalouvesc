+++
date = 2022-04-16T22:00:00Z
description = "Avancement du chantier"
header = "/media/ecolotissement-chantier-reseaux-3.jpg"
icon = ""
subtitle = ""
title = "Pose du réseau d'assainissement et d'eau potable sur l'écolotissement"
weight = 1

+++
Le chantier de viabilisation avance bien. La semaine dernière le réseau d'eau et d'assainissement a été posé. La première phase du chantier, la viabilisation du lotissement, sera achevée fin mai, ouvrant la voie à la construction des habitations. 

D'après nos informations, les premières constructions seront en fustes, rejoignant le souhait de la municipalité de privilégier le bois et la filière locale.

{{<grid>}}{{<bloc>}}

![](/media/ecolotissement-chantier-reseaux-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecolotissement-chantier-reseaux-1.jpg)

{{</bloc>}}{{</grid>}}