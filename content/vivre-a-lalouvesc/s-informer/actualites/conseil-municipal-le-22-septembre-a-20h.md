+++
date = 2022-09-18T22:00:00Z
description = "Ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 22 septembre à 20h"
weight = 1

+++
Le prochain Conseil municipal se tiendra jeudi 22 septembre  à 20h en Mairie

### Ordre du jour

1\. COMMISSION FINANCES  
a. Décision budgétaire modificative n°2  
b. Désignation des représentants de la Commune a la CLECT (commission locale d’évaluation des charges transférées) de la CCVA

2\. COMMISSION GESTION  
a. Gestion des ordures ménagères  
b. Recrutement d'un agent municipal  
c. Transaction du terrain de la copropriété Grosjean  
d. Rapport annuel sur le prix et la qualité du service SPANC pour l’année 2021

3\. COMITE VIE LOCALE  
a. Avancement du local pour les adolescents  
b. Suites de l'étude d'usages de l'information

4\. COMITE DEVELOPPEMENT  
a. Avancement de l'étude sur le Cénacle (SCOT)  
b. Approbation des plans de l’aménagement de I'espace Beauséjour

5\. DIVERS