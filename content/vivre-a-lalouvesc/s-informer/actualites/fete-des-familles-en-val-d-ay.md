+++
date = 2021-09-02T22:00:00Z
description = "Samedi 18 septembre, 14h-17h30, au Parc Jaloine à St Romain d'Ay"
header = "/media/invitation-ram-2021-2.jpg"
icon = ""
subtitle = ""
title = "Fête des familles en Val d'Ay"
weight = 1

+++
Les AFR et l'ACEPP, associations gestionnaires de services pour les jeunes enfants, enfants, ados et familles sur le Val d'Ay, s'associent pour proposer à leurs publics un temps festif en famille. Dans le parc de l'espace Jaloine, le 18 septembre, de 14h à 17h30, nous proposerons des jeux en bois, jeux de kermesse, jeux de société, espace de motricité, stand de crêpes... Avec la participation de Garance et La Berlue.

Entrée gratuite, soumise aux conditions sanitaires en vigueur.

![](/media/invitation-ram-2021.jpg)