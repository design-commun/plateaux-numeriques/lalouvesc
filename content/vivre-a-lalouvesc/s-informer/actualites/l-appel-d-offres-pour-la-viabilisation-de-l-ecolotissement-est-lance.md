+++
date = 2021-10-07T22:00:00Z
description = "Bientôt quatre compromis de ventes signés, les travaux peuvent démarrer"
header = "/media/ecolotissemet-plan.jpg"
icon = ""
subtitle = ""
title = "L'appel d'offres pour la viabilisation de l'écolotissement est lancé"
weight = 1

+++
Les promesses de vente des lots sur l'[écolotissement du Bois de Versailles](https://www.lalouvesc.fr/projets-avenir/changement-climatique/ecolotissement-du-bois-de-versailles/) s'accumulent. Trois compromis sont signés, bientôt quatre et plusieurs autres en discussion.  
La municipalité a donc décidé de lancer les travaux de viabilisation. Première étape : la publication de l'[appel d'offres](https://www.achatpublic.com/sdm/ent/gen/ent_detail.do?selected=0&PCSLID=CSL_2021_B-wZyQIcpi).

Si tout va bien les premières constructions pourront sortir de terre cet été.

Attention, si vous êtes intéressés par un terrain pour construire à Lalouvesc, ne tardez pas. Ce sont les derniers lots constructibles.