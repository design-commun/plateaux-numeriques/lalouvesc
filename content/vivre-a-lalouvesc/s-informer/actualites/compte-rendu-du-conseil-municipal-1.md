+++
date = 2021-06-08T22:00:00Z
description = "Séance du 7 juin 2021"
header = "/media/Mairie-Lalouvesc-4x1-1280w-dithered.jpg"
icon = ""
subtitle = ""
title = "Compte rendu du Conseil municipal"
weight = 1

+++
On peut lire [le compte rendu ici](/media/2021-4-conseil-municipal-lalouvesc.pdf).

### Ordre du jour

1. VIE MUNICIPALE

   a. Rencontre avec le Directeur diocésain de l’enseignement catholique

   b. Subventions reçues
2. COMMISSION FINANCES

   a. Fusion des budgets

   b. Décision modificative n°1

   c. Fonds de solidarité crise COVID

   d. Subvention exceptionnelle OGEC
3. COMMISSION GESTION

   a. Avenant au contrat de travail de Sylvie Deygas

   b. Création de deux postes de travail pour le camping

   c. Constat de désaffection et déclassement d’un terrain en vue de sa vente

   d. Suivi de l’appel d’offres pour la démolition de l’hôtel Beauséjour

   e. Délégation de la maitrise d’ouvrage des travaux de voirie communale à la CCVA
4. COMITE VIE LOCALE

   a. Plan de circulation et de stationnement

   b. Fleurissement
5. COMITE DEVELOPPEMENT

   c. Composition du Comité
6. DIVERS

   a. Point voirie communale

   b. aménagements réalisés au camping