+++
date = 2022-06-14T22:00:00Z
description = "L'accueil est prêt et, pour la première fois, des vélos électriques"
header = "/media/ardechoise-2022-1er-jour-1.jpg"
icon = ""
subtitle = ""
title = "L'Ardéchoise, premier jour"
weight = 1

+++
Pour l'Ardéchoise verte, la première journée, tout est en place à Lalouvesc. Le ravitaillement est prêt, les bénévoles en poste, et une animation est prévue... avec des cloches.

Nous avons eu la surprise de la visite de Christian Féroussier, deuxième vice-président du département, sur son vélo électrique. Le vélo électrique est une première pour l'Ardéchoise.

En effet pour sa 29ème édition, il est possible de faire l'un des parcours de l’Ardèche Verte en vélo à assistance électrique pour la première fois, le mercredi 15 juin 2022.

{{<grid>}}{{<bloc>}}

![](/media/ardechoise-2022-1er-jour-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ardechoise-2022-1er-jour-2.jpg)

{{</bloc>}}{{</grid>}}

![](/media/ardechoise-2022-1er-jour-4.jpg)