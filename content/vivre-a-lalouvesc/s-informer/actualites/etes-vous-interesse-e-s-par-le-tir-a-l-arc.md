+++
date = 2021-04-16T22:00:00Z
description = "Votre avis sur l'ouverture d'une nouvelle section d'Ardèche Sport Nature   "
header = "/media/tir-a-l-arc-2.jpg"
icon = ""
subtitle = ""
title = "Etes-vous intéressé(e)s par le tir à l'arc ?"
weight = 1

+++
Une nouvelle section d'[Ardèche Sport Nature](https://www.sport-nature-ardeche.fr/) pourrait s'ouvrir dans le Nord-Ardèche pour le tir à l'arc s'il y a suffisamment d'intéressés.

Si vous êtes tenté(e)s par ce sport de concentration et de précision, répondez-vite à ce [court questionnaire](https://docs.google.com/forms/d/e/1FAIpQLScXEbgPKBCah0bbC_sbrCXL8qfXliuqYRrvw6jzNwumIFpyJw/viewform?vc=0&c=0&w=1&flr=0&gxids=7628).

Vous serez ensuite contactés par les responsables de cette initiative.