+++
date = 2023-01-18T23:00:00Z
description = "Le 25 janvier à 20h, ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Premier Conseil municipal de l'année"
weight = 1

+++
Le premier Conseil municipal de l'année 2023 se tiendra le 25 janvier à partir de 20h à la Mairie, salle du Conseil.

### Ordre du jour

1\. COMMISSION FINANCES

a. Demande de subventions pour l’aménagement de l’espace Beau-Séjour

b. Demande de subventions pour la sécurisation du réseau d’eau

c. Tarifs de l'eau et de l’assainissement 2023 (abonnement, m3, taxes de raccordement)

d. Subvention 2023 pour l’OGEC (école St Joseph)

e. Subventions pour les associations

f. Reliures et archives

g. Contrat assistance et maitre d’œuvre pour la réfection des routes SDEA

2\. COMMISSION GESTION

a. Avancement de l'adressage

b. Actions pour la qualité de l’eau

c. Validation localisation et mise en place du city-Stade

3\. COMITÉ VIE LOCALE

a. Conventions pour l’utilisation des locaux communaux par les associations

b. Avancement du recensement

c. Logo et sacs

4\. COMITÉ DÉVELOPPEMENT

a. Avancement de l’étude sur Ste Monique et le Cénacle (SCOT)

b. Composition du comité de développement 2023

c. Grand atelier des maires ruraux pour la transition écologique

5\. DIVERS

a. Élection partielle

b. Panneau Pocket

c. Déneigement

d. Atlas de la biodiversité communale (ABC) 2023