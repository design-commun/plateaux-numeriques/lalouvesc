+++
date = 2023-01-02T23:00:00Z
description = "Pour les jeunes enfants et leurs parents"
header = "/media/parentibulle.jpg"
icon = ""
subtitle = ""
title = "Parentibulle, rencontres du 1er trimestre"
weight = 1

+++
Rencontres Espace Jaloine à St Romain d'Ay à partir de 9h30 les 20 janvier, 3 et 17 février, 3, 17 et 31 mars.

Cet espace d'accueil est destiné aux parents de jeunes enfants. Au programme, des jeux, jouets, espaces de motricité, échanges entre parents, livres...

Chaque parent peut oser pousser la porte, c'est un lieu ouvert à tous, libre, gratuit.

Renseignements au 06.48.01.90.84 et sur [Facebook](https://www.facebook.com/Parentibulle07/).

![](/media/le-parentibulle-dates-1et-trimestre-2023.png)