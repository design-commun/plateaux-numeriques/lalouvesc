+++
date = 2021-06-19T22:00:00Z
description = "Une participation faible mais supérieure à la moyenne nationale, un choix net…"
header = "/media/regionales-1er-tour-2021.png"
icon = ""
subtitle = ""
title = "Résultat du premier tour des élections régionales et départementales"
weight = 1

+++
## Régionales premier tour

### Lalouvesc

* Inscrits : 321
* Votants : 141, soit 44 % de votants
* Blancs : 1


* WAUQUIEZ (LUD) 95 voix
* KOTARAC (RN) 16 voix
* BONNELL (LUC) 9 voix
* GREBERT (LECO) 8 voix
* VALLAUD-BELKACEM (LUG) 6 voix
* CUKIERMAN (LUG) 6 voix
* GOMEZ (LEXG) 0 voix
* GILL (DIV) 0 voix
* OMEIR (LDIV) 0 voix

![](/media/regionales-1er-tour-2021.png)

### Ardèche

* WAUQUIEZ (LUD) 39 346 voix, 43.69 % 
* VALLAUD-BELKACEM (LUG) 12 685 voix, 14.09 %
* KOTARAC (RN) 11 710 voix, 13.00 %
* GREBERT (LECO) 11 229 voix, 12.47 % 
* CUKIERMAN (LUG) 6 706 voix, 7.45 % 
* BONNELL (LUC) 6 141 voix, 6.82 % 
* GOMEZ (LEXG) 1 720 voix, 1.91 %
* GILL (DIV) 512 voix, 0.57 % 
* OMEIR (LDIV) 2 voix, 0.00 %

Abstentions 62.84 %

### Région

Ballottage :

* WAUQUIEZ (LUD) 43.79 % 
* GREBERT (LECO) 14.45 % 
* KOTARAC (RN) 12.33 %
* VALLAUD-BELKACEM (LUG) 11.4 %

Éliminés :

* BONNELL (LUC) 9.87 % 
* CUKIERMAN (LUG) 5.56 % 
* GOMEZ (LEXG) 1 720 voix, 1.57 %
* GILL (DIV) - de 1 % 
* OMEIR (LDIV) - de 1 %

## Départementales premier tour

### Lalouvesc

* Inscrits : 321
* Votants : 140, soit 44% de votants
* Legros & Magnouloux (RN) : 18
* Bourjat & Vallon (DVD) : 90
* Vigouroux & Vigouroux (PC) : 10
* Mourier & Vercasson (DVG)	: 21

![](/media/departementales-1er-tour-2021.png)

### Canton du Haut-Vivarais

Inscrits : 16 907, Votants : 7 428

Ballottage :

*  Laetitia Bourjat & Jean-Paul Vallon (BC-DVD, Divers droite) : 3 598 voix, 50.24 % 
*  Aurélien Mourier & Marie Vercasson (BC-DVG, Divers gauche) : 2 101 voix, 29.34 % 

Eliminés :

* Maryse Legros & Bernard Magnouloux (BC-RN, Rassemblement National) : 774 voix, 10.81 %
* Catherine & Laurent Vigouroux (BC-COM, Parti Communiste) : 689 voix, 9.62 % 

[Tous les résultats de l'Ardèche](https://france3-regions.francetvinfo.fr/auvergne-rhone-alpes/ardeche/direct-ardeche-les-resultats-du-1er-tour-des-elections-departementales-2021-2137162.html)