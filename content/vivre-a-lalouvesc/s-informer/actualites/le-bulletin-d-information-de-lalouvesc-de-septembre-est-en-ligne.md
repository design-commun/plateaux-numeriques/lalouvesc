+++
date = 2022-08-30T22:00:00Z
description = "Présentation et sommaire"
header = "/media/entete-automne.jpg"
icon = ""
subtitle = ""
title = "Le Bulletin d'information de Lalouvesc de septembre est en ligne"
weight = 1

+++
## Bulletin n°26 – Septembre 2022

### 26. Convivialités

Les convivialités nourrissent la vie d’un village en bonne santé. Comment peut-on les traduire sur un site web ou dans des services numériques ? Voilà quelques réponses que nous chercherons en septembre. Dans ce Bulletin, nous faisons aussi un premier retour sur la saison qui s’achève et vous y trouverez encore, comme toujours, bien d’autres informations.

[**Lire de Bulletin complet**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/)

### Accéder via le sommaire

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#mot-du-maire)
* [Actualités de la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#actualités-de-la-mairie)
  * [Coupure d’eau](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#coupure-deau)
  * [Nouvelles plaques de rue, nouveaux numéros](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#nouvelles-plaques-de-rue-nouveaux-numéros)
  * [Recensement début 2023](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#recensement-début-2023)
  * [L’étude sur le Cénacle se poursuit](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#létude-sur-le-cénacle-se-poursuit)
  * [Travaux pour la mise en place de la fibre](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#travaux-pour-la-mise-en-place-de-la-fibre)
* [Zoom](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#zoom)
  * [Les usages de l’information locale](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#les-usages-de-linformation-locale)
  * [Des Rencontres pour un numérique convivial en milieu rural](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#des-rencontres-pour-un-numérique-convivial-en-milieu-rural)
* [Culture - loisir](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#culture---loisir)
  * [Affluence au cinéma](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#affluence-au-cinéma)
  * [Démontage de l’exposition du Carrefour des Arts](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#démontage-de-lexposition-du-carrefour-des-arts)
  * [Parentibulle : rencontres parents, jeunes enfants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#parentibulle--rencontres-parents-jeunes-enfants)
  * [Bientôt la fête du livre](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#bientôt-la-fête-du-livre)
  * [Voitures de collection](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#voitures-de-collection)
* [Sanctuaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#sanctuaire)
  * [Un 15 août sous le soleil](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#un-15-août-sous-le-soleil)
  * [Fête de Sainte Thérèse Couderc](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#fête-de-sainte-thérèse-couderc)
* [Fermetures temporaires de commerces](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#fermetures-temporaires-de-commerces)
* [Santé - sécurité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#santé---sécurité)
  * [Un nouveau kiné à Lalouvesc](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#un-nouveau-kiné-à-lalouvesc)
  * [Destruction de nids de frelons](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#destruction-de-nids-de-frelons)
  * [Suite du calendrier Santé-Environnement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#suite-du-calendrier-santé-environnement)
* [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#dans-lactualité-le-mois-dernier)
  * [Quatre mariages cet été](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#quatre-mariages-cet-été)
* [Puzzle : les nouvelles rues louvetonnes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/26-convivialite/#puzzle--les-nouvelles-rues-louvetonnes)