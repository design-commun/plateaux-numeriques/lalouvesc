+++
date = 2021-06-05T22:00:00Z
description = "Le Comité des fêtes a installé ses décorations 2021 sur le square de la place du Lac"
header = "/media/decoration-2021-1.jpg"
icon = ""
subtitle = ""
title = "Le village se décore"
weight = 1

+++
Si vous n'avez pas réussi le puzzle du [dernier Bulletin](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/11-embellir/) ou si vous n'avez pas l'occasion de passer à la place du Lac, voici un aperçu des nouvelles décorations du village imaginées par le Comité des fêtes pour 2021.

{{<grid>}} {{<bloc>}}

![](/media/decoration-2021-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-2021-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-2021-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/decoration-2021-2.jpg)

{{</bloc>}}{{</grid>}}

Après le balayage complet du village la semaine dernière et en attendant la mise en place des fleurs la semaine prochaine, voici donc un sourire du Comité des fêtes, une nouvelle étape de l'embellissement pour Lalouv'estivales !