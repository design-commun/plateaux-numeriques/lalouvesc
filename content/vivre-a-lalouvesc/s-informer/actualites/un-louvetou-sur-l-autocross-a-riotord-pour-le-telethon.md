+++
date = 2021-12-06T23:00:00Z
description = "N°43, pas de pare-brise, pas de rétroviseur, aucune vitre, arceaux obligatoires, casques obligatoires..."
header = "/media/autocross-telethon-2021-1.jpg"
icon = ""
subtitle = ""
title = "Un Louvetou sur l’Autocross à Riotord pour le Téléthon"
weight = 1

+++
Romain Fourezon originaire de Lalouvesc et Tony Bert originaire de Pailharés se sont lancés cette année sur la piste de l’Autocross pour une bonne cause « LE TÉLÉTHON »

La Clio ayant le numéro 43 a été minutieusement préparée afin de respecter les mesures de sécurité : pas de pare-brise, pas de rétroviseur, aucune vitre, arceaux obligatoires, casques obligatoires…

Dans la journée il a fallu s’inventer des talents de mécaniciens et ne pas avoir peur de sombrer sous la boue.

La journée s’est enfin terminée avec le sourire en ayant partagé un moment de convivialité sous une météo capricieuse pour le « TÉLÉTHON ».

![](/media/autocross-telethon-2021-2.jpg)