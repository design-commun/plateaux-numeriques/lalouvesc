+++
date = 2022-04-06T22:00:00Z
description = "Ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 11 avril à 20h"
weight = 1

+++
Un conseil municipal est convoqué à la mairie pour le lundi 11 avril à 20h, consacré essentiellement au budget 2022.

Ordre du jour :

1. COMMISSION FINANCES
   1. Document de valorisation financière et fiscale 2021
   2. Approbation du compte de gestion 2021
   3. Approbation du compte administratif 2021
   4. Affectation des résultats 2021
   5. Approbation du budget primitif 2022
   6. Information sur le budget CCVA
2. COMMISSION GESTION
   1. Recrutement d’employés municipaux
   2. Annulation de la délibération de l’adressage votée en 2020
3. COMITÉ VIE LOCALE
   1. Date des journées citoyennes
4. COMITÉ DÉVELOPPEMENT
   1. Réunion avec le SCOT
5. DIVERS