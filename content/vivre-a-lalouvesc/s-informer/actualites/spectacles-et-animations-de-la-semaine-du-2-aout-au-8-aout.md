+++
date = 2021-07-29T22:00:00Z
description = "Tous les jours d'été, il se passe quelque chose à Lalouvesc"
header = "/media/copie-de-20210703_112053.jpg"
icon = ""
subtitle = ""
title = "Spectacles et animations de la semaine du 2 au 8 août "
weight = 1

+++
En plus des [nombreuses activités](/decouvrir-bouger/activites/) possibles, voici les spectacles et manifestations proposés à Lalouvesc du 2 au 8 août.

## Tous les jours

* 14h30 à 18h30 **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal
* 10h à 12h et de 15h à 18h **Exposition _La matière habitée_**  sculptures à la chapelle Saint Ignace
* Du 24 juillet au 6 août l’AFTL propose des stages de **tennis**

## 2 août

* 9h **Gym**,  au camping
* 10h30 **Visite du jardin en permaculture** au Mont Besset
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 19h théâtre, **Confinement, quoi d’autres ?**, Camping
* 21h cinéma, **Un tour chez ma fille**, [bande annonce](https://youtu.be/nOT_rgmdAGg)

## 3 août

* 9h **Gym**, au camping
* 14h-17h **Animation enfants**, au camping (sur inscription)
* 18h : visites estivales **Paroles d'habitants**, départ à l'Office du tourisme
* 19h : théâtre, **Confinement, quoi d’autres ?**, Camping

## 4 août

* 15h-17h empruntez des livres à la **bibliothèque**
* 17h **Apéro découverte** devant l'Office du tourisme
* 17h conférence, **Sainte Hildegarde de Bingen, une bénédictine allemande inspirée**, _Dominique ESTRAGNAT_ à la Maison St Régis
* 21h cinéma, **Le sens de la fête**, [bande annonce](https://youtu.be/-fycN9kNaXk)

## 5 août

* 10h **Balade à la découverte des plantes comestibles et leurs usages culinaires**, rdv devant la Maison St Régis (sur inscription Office du tourisme)
* 13h30 **Animation enfants**, au camping (sur inscription)
* 21h cinéma, **Les Croods 2 : une nouvelle ère**, [bande annonce](https://youtu.be/h43qitmhLxQ)

## 6 août

* 17h conférence, **La médecine de Sainte Hildegarde de Bingen** _Dominique ESTRAGNAT_ à la Maison St Régis
* 19h théâtre, **Confinement, quoi d’autres ?**, Camping

## 7 août

* 9h30-11h30 empruntez des livres à la **bibliothèque**
* 20h concert, **Eugène Electre** à la Grange de Polly, hommage à Geny Detto
* 21h cinéma, **Le sens de la fête**, [bande annonce](https://youtu.be/-fycN9kNaXk)

## 8 août

* Toute la journée, fête du Sanctuaire, **kermesse**
* 21h cinéma, **Les Croods 2 : une nouvelle ère**, [bande annonce](https://youtu.be/h43qitmhLxQ)