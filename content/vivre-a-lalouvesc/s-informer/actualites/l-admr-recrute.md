+++
date = 2021-12-23T23:00:00Z
description = "On recherche des aides à domicile et des auxiliaires de vie"
header = "/media/admr-aide_seniors2.jpg"
icon = ""
subtitle = ""
title = "L'ADMR recrute sur Lalouvesc"
weight = 1

+++
L'ADMR nous a envoyé ce courrier:

"Comme vous le savez, le secteur de l’aide à domicile est en pénurie de personnel. C’est donc vos administrés « séniors » qui sont personnellement touchés et qui subissent les répercussions indirects.

Afin de garantir leur autonomie et leur bien-être, nous sommes à la recherche de personnel : principalement des aides à domicile et des auxiliaires de vie.

Toutes les candidatures sont les bienvenues **\[avec ou sans expérience : nous assurons la formation du personnel\]."**

![](/media/recrutement-admr-2021.jpg)