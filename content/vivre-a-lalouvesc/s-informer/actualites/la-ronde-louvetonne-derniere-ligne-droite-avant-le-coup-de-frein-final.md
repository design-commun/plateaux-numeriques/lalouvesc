+++
date = 2022-03-26T23:00:00Z
description = "Demandez le programme !"
header = "/media/ronde-louvetonne-1.jpg"
icon = ""
subtitle = "Demandez le programme !"
title = "La Ronde Louvetonne, dernière ligne droite avant le coup de frein final !"
weight = 1

+++
Les inscriptions tombent tous les jours dans la B.P.. Dès à présent **125 voitures** sont engagées.

Voici des précisions sur le déroulement de cette journée.

⇒ Les voitures seront accueillies sur le parc d’exposition à partir de **08h30**.

⇒ La voiture de tête donnera le top départ de la balade à **10h00**.

C’est une balade et non pas une course de vitesse. Les participants prendront le temps d’apprécier le parcours et les points de vue que le tracé leur offrira généreusement. En cours de route une halte apéritive sera proposée.

⇒ Le retour de ce joyeux cortège se fera aux alentours de **12h00**.

Il sera alors temps de se restaurer : plateaux repas pour les participants qui auront commandé au préalable. Sans oublier notre restauration rapide habituelle (frites, saucisses, crêpes) pour tous les affamés.

Frédérika assurera l’animation musicale toute la journée. Des jeux musicaux seront proposés. Les meilleurs talents seront récompensés et deux places pour le concert d’Amaury Vassili seront à gagner pour les plus experts !

Dans l’après-midi le tirage de la tombola fera trois heureux gagnants tant les paniers gourmands/découvertes sont dotés de magnifiques dons.

⇒ De 14h00 à 18h00 une démonstration de mini-montgolfières en vol captif mettra encore un peu plus de couleur et d’ambiance cette journée.

Enfin notre buvette désaltérera tout le monde et favorisera de bons moments de convivialité.

**Nous aurons besoin de bénévoles**. N’hésitez pas à proposer vos services, avant, pendant, après. Même quelques heures de votre présence seront les bienvenues.

Le Comité des Fêtes est toujours joignable soit :

* par téléphone au 07.66.67.94.59,
* par mail [comitedesfetes.lalouvesc@gmail.com](mailto:comitedesfetes.lalouvesc@gmail.com)
* par courrier adressé à la BP 16 – 07520 LALOUVESC.

Agnès G.