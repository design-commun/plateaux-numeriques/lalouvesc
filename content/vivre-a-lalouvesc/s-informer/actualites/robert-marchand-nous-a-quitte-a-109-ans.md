+++
date = 2021-05-22T12:01:00Z
description = "Robert Marchand, champion cycliste centenaire, icône de l'Ardéchoise. "
header = "/media/robert-marchand-3.png"
icon = ""
subtitle = ""
title = "Robert Marchand nous a quitté à 109 ans"
weight = 1

+++
Robert Marchand s’est éteint dans la nuit de vendredi à samedi, à Mitry-Mory, en Seine-et-Marne. A Mitry, c'était un homme très engagé, militant communiste de toujours. Chez nous il était connu et très apprécié pour sa participation et son soutien à l'Ardéchoise, ce qui lui a valu l'honneur de voir son prénom accolé au nom du col du Marchand près de Lalouvesc.

Sapeur-pompier, planteur de canne à sucre, marchand de vin ou maraîcher… Robert Marchand a travaillé jusqu’à un âge avancé, après avoir renoncé, dans sa jeunesse, à une carrière de cycliste professionnel. Sa vie fut marquée par les deux conflits mondiaux, la guerre froide et des années à bourlinguer au Venezuela et au Canada. Il était la mémoire du XXe siècle, ayant connu 17 présidents de la République, et l’intégralité des Coupes du monde de football.

Robert Marchand était devenu une icône, alors qu’il effectuait encore des exploits sur son vélo à 100 ans bien tassés. Il a enchaîné les records : record de l’heure des plus de 100 ans, puis des plus de 105 ans, titre de champion du monde de cyclisme sur route des plus de 105 ans, sous le maillot jaune et violet de L’Ardéchoise,

Le jour de son dernier anniversaire, le champion avait adressé [une lettre ouverte au président de la République](/media/lettre-president-republique-1.pdf), un plaidoyer pour le sport et une déclaration d'amour à l'Ardèchoise.

![](/media/robert-marchand.png)

A voir le [reportage réalisé par Fr3](https://youtu.be/q-mISNfLgSE) au moment de sa prise de retraite sportive... à 106 ans.