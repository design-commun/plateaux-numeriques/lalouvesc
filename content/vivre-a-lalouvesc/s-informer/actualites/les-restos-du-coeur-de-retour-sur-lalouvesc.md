+++
date = 2023-01-12T23:00:00Z
description = "Mardi 17 janvier de 14h à 15h30 au CAC"
header = "/media/logo-restos-du-coeur.jpg"
icon = ""
subtitle = ""
title = "Les restos du coeur de retour sur Lalouvesc"
weight = 1

+++
Il y aura une distribution des restos du cœur le mardi 17 janvier de 14h à 15h30 au CAC.

**Faites-le savoir.**