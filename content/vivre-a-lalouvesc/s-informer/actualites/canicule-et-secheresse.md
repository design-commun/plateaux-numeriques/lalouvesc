+++
date = 2022-07-12T22:00:00Z
description = "Restons raisonnables dans notre consommation d'eau"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "Canicule et sécheresse"
weight = 1

+++
A mille mètres, Lalouvesc ne souffre pas vraiment de la canicule, mais, même si les sources sont bien alimentées, il faut rester prudent face à la sécheresse.

L'ARS nous alerte : _après les évènements pluvieux de fin juin, les conditions météorologiques semblent à nouveau se diriger vers une période de temps sec laissant craindre des risques de pénuries d’eau sur certains réseaux de distribution d’eau potable sensible._

Rappel : notre territoire est en alerte sécheresse renforcée depuis un mois. En plus des [restrictions déjà publiées](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/actualites/secheresse-alerte-renforcee...canicule-vigilance/), tout le monde doit avoir une attitude responsable.

![](/media/22096-ecogestes-secheresse_rs-carre_1-2022.jpg)