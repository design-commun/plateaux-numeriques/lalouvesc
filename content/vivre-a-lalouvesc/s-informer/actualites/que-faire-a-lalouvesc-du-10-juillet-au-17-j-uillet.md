+++
date = 2022-07-07T22:00:00Z
description = "Il se passe toujours quelque chose aux Lalouv'estivales"
header = "/media/carrefour-des-arts-2022-inauguration-4.jpg"
icon = ""
subtitle = "Quelques suggestions"
title = "Que faire à Lalouvesc du 10 juillet au 17 juillet ?"
weight = 1

+++
A part le farniente, les [**innombrables balades**](https://www.lalouvesc.fr/decouvrir-bouger/activites/randonnees-sympas-autour-de-lalouvesc/), le [**vélotourisme**](https://www.lalouvesc.fr/decouvrir-bouger/activites/velotourisme/), le[ **cheval**](https://www.lalouvesc.fr/decouvrir-bouger/activites/centre-equestre/)... voici quelques suggestions :

### Dimanche 10 juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 20h30 aller au **Concert Let’s Goldman**, Parc des Pèlerins
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Mystère_** ( [bande annonce](https://youtu.be/nmfMMrijOzU) ) à l’Abri du Pèlerin

### Lundi 11 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_La nuit du 12_** ( [bande annonce](https://youtu.be/nC6AbWkkULc) ) · AVANT-PREMIÈRE à l’Abri du Pèlerin

### Mardi 12 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 20h30 **Théâtre** **_Politiquement correct_** par la Compagnie théâtrale Arcorcour à l’Abri du Pèlerin

### Mercredi 13 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 15h - 17h emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* 17h partager un **Apéro découverte** à l’Office du tourisme
* à partir de 19h : **Bal et feu d’artifice** au parc du Val d'Or

### Jeudi 14 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Vendredi 15 Juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal

### Samedi 16 juillet

* 9h30 - 11h30  emprunter des livres à la [**bibliothèque**](https://www.lalouvesc.fr/vivre-a-lalouvesc/acteurs-services/ecole/#la-bibliochouette)
* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_En corps_** ( [bande annonce](https://youtu.be/WMqIkiI6fAA) ) à l’Abri du Pèlerin

### Dimanche 17 juillet

* déambuler dans **l'exposition de photographies** de Serge Rousse au Centre du village, [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.fr)
* à partir de 10h : **Fête du livre**, rencontrer des auteurs à la bibliothèque
* 11h - 12h15 et 15h30 - 18h visiter **l'exposition de peintures** de Marie-José Planson à la chapelle Saint Ignace
* 14h30 à 18h30 visiter  **l'exposition du** [**Carrefour des Arts**](http://www.carrefourdesarts-lalouvesc.com/) (cinq artistes plasticiens) au Centre d’Animation Communal
* 21h aller au [**Cinéma**](https://www.lalouvesc.fr/decouvrir-bouger/lalouv-estivals/autres-rejouissances/#cin%C3%A9ma-juillet---ao%C3%BBt), **_Le Temps des Secrets_** ( [bande annonce](https://youtu.be/RFpGLeipEBA) ) à l’Abri du Pèlerin