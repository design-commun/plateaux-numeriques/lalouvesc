+++
date = 2023-02-15T23:00:00Z
description = "Les sacs siglés sont arrivés"
header = "/media/sac-lalouvesc-fev-2023.jpg"
icon = ""
subtitle = ""
title = "Le chic louvetou"
weight = 1

+++
### Ça y est le village a son sac ~~Louis Vuitton~~ Louveton personnalisé !

![](/media/sac-lalouvesc-fev-2023.jpg)

Vendu à prix coûtant à la mairie 6,50 € !

### Tout le monde le monde voudra le sien... du plus bel effet cet été dans les rues du village.