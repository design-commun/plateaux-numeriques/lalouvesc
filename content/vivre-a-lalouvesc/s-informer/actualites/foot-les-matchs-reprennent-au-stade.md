+++
date = 2022-01-22T23:00:00Z
description = "Le prochain dimanche 30 janvier 14h30"
header = "/media/foot-match-18-09-2021-2.jpg"
icon = ""
subtitle = ""
title = "Foot : les matchs reprennent au stade"
weight = 1

+++
![/media/foot-calendrier-2022-1.jpg](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/foot-calendrier-2022-1.jpg)

* [Le calendrier officiel est ici](https://drome-ardeche.fff.fr/recherche-clubs/?scl=15675&tab=resultats&subtab=calendar&competition=381728&stage=1&group=5&label%20group=POULE%20E).
* [Tout savoir sur l'équipe](https://www.lalouvesc.fr/decouvrir-bouger/activites/autres-activites/#football).