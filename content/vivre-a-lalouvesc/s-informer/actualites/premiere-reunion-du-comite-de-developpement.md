+++
date = 2021-06-22T22:00:00Z
description = "Le Comité a précisé les dossiers prioritaires et souhaite associer les Louvetous à la réflexion"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = ""
title = "Première réunion du Comité de développement"
weight = 1

+++
La première réunion plénière du Comité de développement s'est tenue le 18 juin. Elle fut à la fois constructive et sympathique. Le compte-rendu est [disponible ici](/media/reunion-du-comite-de-developpement-juin-2021.pdf), ci-dessous son sommaire :

* Présents à la réunion 
* **Engagements et réalisations 2020-2021**
  * “Lalouvesc un village qui a du caractère”
  * Une planification exigeante 
  * Perspectives 
  * Ce qui a déjà été réalisé ou est en cours de réalisation dans la première année 


* **Rappel du rôle du Comité** 
  * Calendrier 
* **Tour de table sur les pôles à structurer** 
  * Beauséjour - Parc du Val d’or / Thématique : tourisme éco-responsable, filière bois
  * Quartier Basilique / Thématique : histoire du village, pèlerinage
  * CAC / Thématique : animation, culture
  * Hébergements/ Thématique : élargissement et coordination de l’offre
  * Autres thèmes abordés
    * Participation numérique 
    * Espace de coworking 
    * Football 
    * Pêche 
    * Foyer de ski de fond 
    * Deux espaces à animer 
* **Modalités de travail**

Dans les modalités de travail, le Comité a souhaité pouvoir élargir le recueil des suggestions à tous les Louvetous qui le souhaitent. Si vous êtes dans ce cas, merci de l'indiquer sur [ce formulaire](https://docs.google.com/forms/d/e/1FAIpQLSditZ6DVizWDO4FVqG5P0VDO43Fii4Tpk4v9bs5fn_6HbSlcA/viewform?usp=pp_url). En fonction du nombre de réponses et de leur contenu, nous ajusterons l'animation de la suite des travaux.