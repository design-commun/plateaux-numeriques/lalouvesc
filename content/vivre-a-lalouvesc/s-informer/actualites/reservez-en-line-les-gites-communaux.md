+++
date = 2021-05-09T22:00:00Z
description = "Une plateforme de réservations est maintenant disponible"
header = "/media/acacias-2.jpg"
icon = ""
subtitle = ""
title = "Réservez en ligne les gîtes communaux"
weight = 1

+++
Attention, les réservations des gîtes communaux pour cette saisons vont bon train. Les créneaux disponibles se réduisent !

Vous pouvez maintenant faire vos réservations directement en ligne. [Tout est présenté sur cette page](/decouvrir-bouger/hebergement-restauration/gites-municipaux/).

Le paiement en ligne est pour bientôt.