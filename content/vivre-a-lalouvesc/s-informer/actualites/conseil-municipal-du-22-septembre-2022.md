+++
date = 2022-09-26T22:00:00Z
description = "Le procès-verbal est en ligne"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal du 22 septembre 2022"
weight = 1

+++
Le procès-verbal du Conseil municipal du 22 septembre 2022 est en ligne :

* [Procès-verbal](/media/2022-4-conseil-municipal-lalouvesc.pdf)
* [Délibérations](/media/deliberations-du-cm-du-22-septembre-2022.pdf)

### Ordre du jour

1. COMMISSION FINANCES
   1. Décision budgétaire modificative n°2
   2. Désignation des représentants de la Commune à la CLECT (commission locale d’évaluation des charges transférées) de la CCVA
   3. Achat d’une saleuse
   4. Démarrage d’une première tranche de travaux eau-assainissement
2. COMMISSION GESTION
   1. Gestion des ordures ménagères
   2. Recrutement d’un agent municipal
   3. Transaction du terrain de la copropriété « Grosjean »
   4. Rapport annuel sur le prix et la qualité du service SPANC pour l’année 2021
3. COMITÉ VIE LOCALE
   1. Avancement du local pour les adolescents
   2. Suites de l’étude d’usages de l’information
4. COMITÉ DÉVELOPPEMENT
   1. Avancement de l’étude sur le Cénacle (SCOT)
   2. Approbation des plans de l’aménagement de l’espace Beauséjour
5. DIVERS