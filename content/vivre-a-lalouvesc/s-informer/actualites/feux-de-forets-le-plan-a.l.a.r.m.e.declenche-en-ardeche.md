+++
date = 2022-08-10T22:00:00Z
description = "Soyez TRES prudents !"
header = "/media/feux-interdits-2022.jpg"
icon = ""
subtitle = ""
title = "Feux de forêts : le plan A.L.A.R.M.E. déclenché en Ardèche"
weight = 1

+++
Au vu des renseignements fournis par le bulletin météorologique, de l'état de sécheresse de la végétation et des nombreux départs de feux de la journée d'hier et celle d'aujourd'hui, sur proposition du Directeur du SDIS, **le Préfet a déclenché le plan A.L.A.R.M.E., sur l'ensemble du département de l'Ardèche.**  
   
Le plan A.L.A.R.M.E (Alerte Liée Au Risque Météorologique Exceptionnel) définit un ensemble de mesures visant à renforcer les moyens de détection et de lutte contre les feux de forêts.  
 Le plan prévoit une partie ou la totalité des actions suivantes :  
 - la mobilisation complète et le renforcement du dispositif de commandement avec une éventuelle activation du COD,  
 - le renforcement des détachements d’intervention préventifs,  
 - le recours éventuel à des renforts de moyens civils ou militaires.  
   
La mise en œuvre du plan A.L.A.R.M.E. s’appuie sur les services habituellement concernés par les mesures préventives feux de forêts dont l’intervention est coordonnée par le centre opérationnel départemental d’incendie et de secours (CODIS), organe opérationnel du préfet, mais également par la mobilisation des services de Police et de Gendarmerie pour mettre en place des actions spécifiques de surveillance, de contrôles routiers et de répression des infractions en milieu forestier.

**Rappel : tout feu est interdit en extérieur, soyez particulièrement attentifs aux mégots de cigarettes mal éteints et signalez immédiatement toute fumée suspecte.**