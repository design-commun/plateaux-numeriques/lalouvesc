+++
date = 2021-05-05T22:00:00Z
description = "Attention, à partir du 17 mai les horaires changent..."
header = "/media/Mairie-Lalouvesc-4x1-1280w-dithered.jpg"
icon = ""
subtitle = ""
title = "La Mairie et l'Agence postale passent à l'horaire d'été"
weight = 1

+++
## Horaires d’ouverture de la Mairie

**Valables à partir du 17 mai**

* Lundi, Mardi, Jeudi : 10h - 12h15
* Mercredi, Vendredi : 14h - 16h30

## Horaires d’ouverture de la Poste

**Valables à partir du 17 mai**

* Lundi, Mercredi, Jeudi, Vendredi : 10h - 12h15
* Mardi : 14h - 16h