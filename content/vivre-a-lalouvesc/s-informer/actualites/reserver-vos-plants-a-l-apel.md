+++
date = 2023-04-10T22:00:00Z
description = "Fleurs, légumes, plantes aromatiques..."
header = "/media/apel-operation-plants.jpg"
icon = ""
subtitle = ""
title = "Réservez vos plants à l'APEL"
weight = 1

+++
Le printemps est là. L'association des parents d'élèves de l'école St Joseph organise comme l'année dernière une vente de plants : **fleurs, légumes, plantes aromatiques à réserver avant le 1er mai**. 

Tout est expliqué sur le [bon de commande](/media/bon-de-commande-plants-2023.pdf).

![](/media/vente-plantes-apel-avril-2023.jpg)