+++
date = 2023-01-18T23:00:00Z
description = "Vérifiez vos boîtes aux lettres ces prochains jours"
header = "/media/recensement-2023-affiche.png"
icon = ""
subtitle = ""
title = "Premier jour du recensement"
weight = 1

+++
{{<grid>}}{{<bloc>}}

![](/media/nicole-porte-2022.JPG)

{{</bloc>}}{{<bloc>}}

A partir d'aujourd'hui Nicole Porte, notre agent recenseur pour Lalouvesc, dépose un courrier dans la boîte aux lettres des habitants du village indiquant toutes les modalités pour vous recenser. Faites-le le plus tôt possible, tout est expliqué sur le courrier. Mais vous pouvez aussi interroger Nicole, si vous avez des hésitations.

Il est important de déclarer **toutes les personnes vivant sous votre toit** pour que la population louvetonne soit comptabilisée. C'est à partir de ce chiffre que seront calculées les dotations de l’État à la Commune pour une période de cinq années !

{{</bloc>}}{{</grid>}}