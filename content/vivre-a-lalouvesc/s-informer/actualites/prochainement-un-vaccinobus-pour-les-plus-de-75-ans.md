+++
date = 2021-03-17T23:00:00Z
description = "Inscrivez-vous avant le 26 mars"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Prochainement un \"vaccinobus\" pour les plus de 75 ans"
weight = 1

+++
Un "vaccinobus" (un bus dans lequel seront pratiquées les vaccinations contre la Covid-19) va s'installer sur Satillieu mi-avril (dates à préciser).

**Cette vaccination concernera les personnes de plus de 75 ans.**

Toutes les personnes intéressées, même déjà inscrites sur une autre liste d'attente, doivent s'inscrire en cliquant sur [ce formulaire](https://forms.gle/3UdEiiLJeW9Az2ST9) **jusqu'au 25 mars inclus.**  
  
**N'hésitez pas à aider ceux qui n'arriveraient pas à s'inscrire. Le secrétariat de mairie peut aussi faire les démarches le cas échéant.**