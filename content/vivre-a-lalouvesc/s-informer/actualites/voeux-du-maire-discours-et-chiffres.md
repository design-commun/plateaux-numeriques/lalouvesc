+++
date = 2023-01-15T23:00:00Z
description = "Des retrouvailles chaleureuses et instructives. Résumé pour les absents"
header = "/media/voeux-maire-2023-5.jpg"
icon = ""
subtitle = ""
title = "Voeux du Maire : discours et chiffres"
weight = 1

+++
La cérémonie des vœux du Maire a réuni une centaine de Loutetonnes et de Louvetous qui ont pu profiter d'un petit concert de la Lyre et écouter le discours du Maire, celui de la Conseillère départementale Laetitia Bourjat et de la Conseillère régionale Virginie Ferrand avant de partager un verre et un morceau de galette dans une ambiance sympathique.

Ci-dessous quelques photos et, pour ceux qui n'ont pu venir, des extraits du discours du Maire et quelques chiffres.

{{<grid>}}{{<bloc>}}

![](/media/voeux-maire-2023-2.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/voeux-maire-2023-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/voeux-maire-2023-4.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/voeux-maire-2023-6.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/voeux-maire-2023-3.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/voeux-maire-2023-7.jpg)

{{</bloc>}}{{</grid>}}

### Discours du maire

Extraits :

> Merci d’avoir répondu à notre invitation pour passer ce petit moment ensemble pour fêter la nouvelle année, merci d’être venus nombreux.
>
> Bonsoir et merci aussi de leur présence aux élus locaux :
>
> * Madame Virginie Ferrand, maire de Vocance et conseillère régionale auprès du président Laurent Wauquiez
> * Madame Laetitia Bourjat, maire de Vaudevant et conseillère départementale auprès de notre président Olivier Amrane
> * André Ferrand, maire de St Alban d’Ay
> * René Sabatier, maire de st Clair
> * Stephane Roche, maire de Lafarre
> * Jacques Dubay, maire de St Peray
>
> Madame Marie Vercasson, maire de Satillieu, présidente de la Communauté de communes du Val d’Ay, retenue dans sa Commune regrette de ne pas pouvoir venir ce soir.
> Bonsoir à tous les présidents des associations du village.
>
> Bonsoir à toutes les personnes présentes.
>
> A tous, nous souhaitons une très bonne année, beaucoup de joie, de bonheur, d’enthousiasme, de sérénité ET ET surtout une très bonne santé !
>
> Je voudrais, ce soir, retracer les actions menées depuis le début de notre mandature et vous parler des axes forts pour l’avenir.
>
> Au départ du mandat, nous nous sommes attaqué à une remise en état globale du village, avec une vigilance accrue sur l’entretien et la propreté et nous avons effectué une restructuration partielle des sens de circulation et du stationnement. Cela a été notre tout premier travail.
>
> Ensuite, place à l’action ! Nous avons engagé trois dossiers importants : la réfection du pôle de la Mairie - le mur de soutènement cimetière - la démolition de l’hôtel Beauséjour .
>
> Pour ces dossiers nous avons déposé des demandes de subventions que nous avons obtenues. L’ensemble de nos demandes ont été financées à 80% par l’État et la Région.
>
> Vous avez les détails chiffrés sur les panneaux d’information.
>
> Nous avons engagé également des travaux de réfection sur le terrain de camping avec un investissement relativement conséquent, achat d'une tente Lodge et d'un chalet PMR, remise en état des mobil-homes, avec le choix de nouveaux emplacements. Après avoir vérifié notre propriété sur les cabanes perchées, nous avons effectué une remise en état de celles-ci.
>
> Nous avons engagé une réflexion sur la pertinence de continuer le projet de l’écolotissement. À la sortie du premier Covid, il y a eu une réelle demande immobilière dans le milieu rural. Nous avons profité de cette opportunité pour finaliser les travaux de viabilisation afin de pouvoir mettre les lots à la vente le plus rapidement possible. Et ça a marché, nous avons vendu pratiquement tout. Il reste un lot disponible suite à un désistement.
>
> Nous avons pris la décision de changer le chasse neige par un tracteur agricole avec porte-outils. Le montant d’achat d’un tel engin étant important, nous nous sommes tournés vers le Conseil départemental. Nous avons pu bénéficier d’une dotation, couvrant pratiquement le montant de l’achat de l’engin affecté au déneigement.
>
> Une décision de la Communauté de communes nous a conduit à changer nos conteneurs à ordure ménagères. Nous étions un peu inquiet, mais finalement tout s'est bien passé et je vous remercie pour votre civisme.
>
> Nous avons été interpellés par la Direction départementale des routes au sujet de la réfection de la partie haute du RD 578 A (entre Milagro et la pharmacie), qui est en théorie prévue avant l’ardéchoise. Milagro/centre bourg. Une autre prévision de travaux à l’horizon 2024/25 concerne la réfection du RD 532 (traversée du village).
>
> Nous avons pu bénéficier d’une étude gratuite grâce à M. André Ferrand, référent du Scot - Rives du Rhône auprès de la communauté de communes du Val D’Ay. Celle-ci est toujours en cours et se réalise en concertation avec les élus.
>
> Nous avons acheté un City Parck aux enchères à démonter et à remonter sur site pour 8.400€. Une partie du grillage d’entourage sera posé au terrain de foot.
>
> Le recensement se prépare et démarrera le 19 janvier. L'adressage est en cours avec la pose des numéros et des noms de rue.
>
> Pour les actions diverses menées tout au long de l’année, je remercie toutes les associations, et aussi tous les bénévoles qui ont participé aux journées citoyennes. J'associe à ces remerciements le conseil municipal, les employés de mairie, les pères Jésuite de la Maison st Regis, Madame Séverine Moulin, directrice de l’office de tourisme, les membres des services de Gendarmerie, les pompiers. Je n'oublie pas les services de l’État, du Département, de la Région, qui sont à nos côtés pour nous aider à avancer ensemble de toute nos forces pour Lalouvesc !

### Chiffres des chantiers

#### Chantiers autofinancés

{{<grid>}}{{<bloc>}}

![](/media/ecolotissement-nov-2022-1.jpg)

![](/media/lotissement-voeux-2023.png)

{{</bloc>}}{{<bloc>}}

![](/media/camping-construction-lodge-14.jpg)

![](/media/bilan-camping-voeux-2023.png)

{{</bloc>}}{{</grid>}}

#### Chantiers subventionnés

{{<grid>}}{{<bloc>}}

![](/media/rampe-d-acces-mairie-2021.jpg)

![](/media/mairie-voeux-2023.png)

{{</bloc>}}{{<bloc>}}

![](/media/cimetiere-octobre-2022-1.jpg)

![](/media/cimetere-voeux-2023.png)

{{</bloc>}}{{</grid>}}

![](/media/demolition-beausejour-2022-54.jpg)

![](/media/beausejour-voeux-2023.png)

![](/media/synthese-subv-voeux-2023.png)