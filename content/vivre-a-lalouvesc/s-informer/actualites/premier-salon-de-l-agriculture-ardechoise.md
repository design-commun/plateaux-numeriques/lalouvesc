+++
date = 2021-09-16T22:00:00Z
description = "Dimanche 26 septembre 2021, organisé par la FDSEA de l’Ardèche, sur le domaine Olivier de Serres au Pradel (Mirabel - 07170)."
header = "/media/salon-de-l-agriculture-ardechoise-2021.jpg"
icon = ""
subtitle = ""
title = "Premier Salon de l'agriculture ardéchoise"
weight = 1

+++
**Première édition du salon de l’agriculture ardéchoise !**

_Une journée à la rencontre du patrimoine agricole ardéchois et de ceux et celles qui le font grandir !_

**Le dimanche 26 septembre 2021 se tiendra le 1er Salon de l'Agriculture Ardéchoise, organisé par la FDSEA de l’Ardèche, sur le célèbre et magnifique domaine Olivier de Serres au Pradel (Mirabel - 07170).**

Premier du genre, ce Salon se veut la vitrine de l’agriculture ardéchoise dans toute sa diversité.

Vous pourrez donc venir découvrir ou redécouvrir les différentes races de vaches (plus de 30 vaches présentes à viande ou à lait), des chevaux de trait (avec des balades en calèche et en poney, des démonstrations de traction animale), des moutons (démonstration de chiens de troupeau, de tonte ou encore départ de la transhumance en fin de journée), des chèvres avec l’élevage expérimental du Pradel. Pour les filières végétales, la viticulture sera à l’honneur (dégustations des vins de toute l’Ardèche, du Nord au Sud, ballades dans les vignes avec des viticulteurs), les fruits, les châtaignes, les olives ou encore les truffes seront présentées, avec de nombreuses animations et dégustations.

Cette journée à destination du grand public a pour but de sensibiliser les visiteurs sur le monde agricole en leur parlant des métiers de l’agriculture mais aussi de leur faire découvrir ou redécouvrir les produits agricoles ardéchois et les savoir-faire.

**Un marché des producteurs,** vous permettra de ramener chez vous un petit bout de notre Ardèche, afin de vous remémorer cette journée riche en découvertes. **Vous pourrez également vous restaurer** sur place avec le délicieux Biassou ou steak d'Aubrac !

Les Organisations Professionnelles Agricoles, créées il y a de nombreuses années pour le bon fonctionnement des exploitations agricoles, seront également présentes sur le salon pour expliquer leurs rôles auprès des agriculteurs.

**Nous vous donnons rendez-vous le dimanche 26 septembre de 9h à 18h, sur le domaine Olivier de Serres au Pradel (Mirabel) pour cette journée de partage et de découverte.**

**N’hésitez pas à acheter vos entrées en amont** (4€ en prévente, 5€ sur place pour les adultes) **ou réserver votre repas.  
**Comme pour tous les évènements festifs et à cause du contexte sanitaire, le contrôle du pass sanitaire sera fait à l’entrée du site.

Pour retrouver toutes les informations sur le salon, n’hésitez pas à consulter le [site internet de l’évènement](https://www.salondelagricultureardechoise.com/) et à suivre la [page Facebook ](https://www.facebook.com/salonagricole07/?ref=pages_you_manage)!

FDSEA de l’Ardèche