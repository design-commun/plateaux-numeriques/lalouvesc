+++
date = 2021-04-28T22:00:00Z
description = "Les pompiers assurent les vaccinations les week-ends à la clinique des Cévennes à Annonay. "
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Vaccinations sans rendez-vous les dimanches 2 et 9 mai"
weight = 1

+++
En plus des créneaux déjà ouverts en semaine, les pompiers assureront les week-ends des vaccinations à la clinique des Cévennes à Annonay. Les dimanches 2 et 9 mai, les personnes éligibles pourront se faire vacciner de 9h à 12h30 sur rendez-vous (via la plateforme [doctolib.fr](https://www.doctolib.fr/vaccination-covid-19/annonay)) et de 13h30 à 16h30 sans rendez-vous.

### Les personnes éligibles

Le vaccin Pfizer-BioNtech sera administré aux personnes éligibles : les personnes de 18 à 59 ans avec comorbidité, les personnes vivant dans le même foyer qu'une personne souffrant d'une pathologie à très haut risque de forme grave de Covid-19 (sous réserve expresse de présentation d'une prescription médicale) et les personnes de plus de 60 ans.

Par ailleurs, les centres de vaccination restent ouverts les jours fériés du mois de mai.