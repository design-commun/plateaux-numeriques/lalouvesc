+++
date = 2021-07-31T22:00:00Z
description = "13 - Août 2021 - Sérieux et sourires"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = ""
title = "Le Bulletin n°13 est paru"
weight = 1

+++
Pour avancer, il faut du sérieux. Il faut s'organiser dans le bon ordre et un peu de discipline pour, par exemple, circuler correctement dans le village, pour limiter la pandémie, pour chercher et trouver des financements. Mais il ne faut pas trop de sérieux, ne pas se prendre trop la tête, il faut aussi savoir sourire, s'émerveiller et s'émouvoir.

On trouvera ces deux volets dans ce bulletin avec un zoom sur les mesures sanitaires et un autre sur les poètes qui enchantent le Carrefour des Arts et bien d'autres informations qui mêlent chaque fois esprit de sérieux et rires louvetous.

[Lire la suite](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/13-serieux-et-sourires/)