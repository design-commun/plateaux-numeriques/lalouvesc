+++
date = 2023-03-20T23:00:00Z
description = "Pour parents et jeunes enfants"
header = "/media/ccva.jpg"
icon = ""
subtitle = ""
title = "Parentibulle : dates du 2e trimestre 2023"
weight = 1

+++
Le Parentibulle vous propose un espace aménagé et dédié aux jeunes enfants et à leurs parents, pour jouer, découvrir des nouveaux jeux, rencontrer d'autres enfants et d'autres parents, pour échanger sur la parentalité, discuter, lire, boire un café, prendre une pause...

Rencontres Espace Jaloine à St Romain d’Ay à partir de 9h30 les 14 et 21 avril, 5 et 26 mai, 2, 16 et 30 juin, 21 juillet.

![](/media/parentibule-2eme-trimestre-2023.jpg)