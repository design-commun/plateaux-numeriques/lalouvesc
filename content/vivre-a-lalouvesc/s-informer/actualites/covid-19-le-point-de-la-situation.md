+++
date = 2021-03-04T23:00:00Z
description = "Synthèse de la situation et des mesures pour combattre la pandémie en Ardèche et en France"
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Covid 19 : le point de la situation"
weight = 1

+++
Rappel, pour se faire vacciner en Ardèche : 

* Un numéro est disponible le 04 75 66 77 99 du lundi au jeudi de 8h30 à 12h30 et de 13h30 à 17h et le vendredi de 8h30 à 12h30 et de 13h30 à 16h30.
* La liste des centres de vaccination est [accessible ici](https://www.sante.fr/cf/centres-vaccination-covid.html).

Un couvre-feu est en place à partir de 18h, le formulaire d'attestation de déplacement dérogatoire est [accessible ici](https://www.service-public.fr/particuliers/vosdroits/R57403).

L'association des Maires de France a fait une synthèse de la situation en France que nous partageons avec vous.

### Indicateurs de suivi de la campagne vaccinale

Quelques chiffres au 1er mars 2021 :

* Au total depuis le début de la campagne de vaccination : un peu plus de 5 017 000 injections ont été réalisées (en cumulant première et deuxième injections) dont près d’ 1 690 000 personnes ont reçu les deux injections de vaccin.
* 5,9% des personnes de plus de 18 ans ont reçu au moins une injection de vaccin et 3,2% ont reçu les deux injections.
* 81% des résidents d’EHPAD ou d’USLD ont reçu au moins une injection de vaccin.
* 28% des personnes âgées de plus de 75 ans ont reçu au moins une injection de vaccin.
* 33% des professionnels et intervenants de santé ont reçu au moins une injection de vaccin. 48% des professionnels et intervenants de santé de plus de 50 ans ont reçu au moins une injection de vaccin.

### Rappel des publics cibles prioritaires

* -Conformément au récent avis de la Haute Autorité de Santé, le Gouvernement a actualisé ses recommandations concernant le vaccin Astra Zeneca.
* Celui-ci est désormais accessible aux personnes de plus de 65 ans.
* Le vaccin Astra Zeneca est donc aujourd’hui accessible à l’ensemble des personnes éligibles à la vaccination. Consulter la liste des personnes éligibles à la vaccination sur : \[[https://www.sante.fr/cf/centres-vaccination-covid.html](https://www.sante.fr/cf/centres-vaccination-covid.html "https://www.sante.fr/cf/centres-vaccination-covid.html")\]([https://www.sante.fr/cf/centres-vaccination-covid.html](https://www.sante.fr/cf/centres-vaccination-covid.html "https://www.sante.fr/cf/centres-vaccination-covid.html") "[https://www.sante.fr/cf/centres-vaccination-covid.html](https://www.sante.fr/cf/centres-vaccination-covid.html "https://www.sante.fr/cf/centres-vaccination-covid.html")")
* Le vaccin Astra Zeneca est toujours accessible auprès des médecins de ville, qui peuvent désormais disposer de trois flacons par semaine.
* Conformément au récent avis de la HAS les pharmaciens, les infirmiers et les sages femmes auront prochainement la capacité de vacciner : un décret devrait être publié demain.
* Les vaccins Moderna et Pfizer restent accessibles prioritairement aux personnes de plus de 75 ans ainsi qu’aux personnes à très haut risque de forme grave de COVID.
* Des supports de communication actualisés seront prochainement établis par le ministère de la Santé.

### Mise en place des tests salivaires

* -Le ministère de l’Education Nationale déploie, depuis les retours des congés d’hier, une campagne de dépistage dans les écoles, collèges et lycées. Conformément à l’avis de la Haute Autorité de Santé, la priorité est donnée aux enfants d’âge maternel et élémentaire pour l’organisation des tests salivaires. Les tests antigéniques concernent les collèges et les lycées.
* Les tests salivaires seront effectués dans les zones à forte circulation du virus, dans les écoles où des cas avérés ont été signalés. En outre, chaque académie définira un échantillon d’écoles, de collèges et de lycées au sein desquels seront effectués tous les 15 jours des tests pour suivre l’évolution de l’épidémie.
* Objectif à terme : 300.000 tests par semaine. Le déploiement sera progressif car tous les laboratoires ne sont pas équipés pour l’instant pour analyser ces tests.
* Les DASEN sont les contacts des maires pour toute information sur l’organisation des tests salivaires et pour faire remonter des points de vigilance ou des demandes particulières. L’AMF a souligné l’importance que les maires soient automatiquement informés en amont des campagnes de dépistage organisées dans les écoles de leur commune.
* Les tests sont réalisés par des personnels volontaires de l’Education nationale.
* Les tests sont gratuits. Ils peuvent être proposés également au personnel de l’Education nationale et au personnel des collectivités travaillant dans les établissements scolaires.

### Bilan de l’utilisation des doses et des approvisionnements à venir

* 6 600 000 doses de vaccin ont été reçues depuis le début de la campagne de vaccination.
* 4 500 000 doses ont été injectées soit près de 70% des doses reçues.
* Pour le vaccin Astra Zeneca 187 000 injections ont été réalisées pour 600 000 doses livrées Le ministère de la Santé réfléchit aux moyens à déployer pour augmenter la consommation des doses disponibles. Pour rappel, les médecins peuvent actuellement s’approvisionner auprès de leur pharmacie de référence.

  ### Anticipation de la montée en charge de la campagne vaccinale
* Les préfets vont se rapprocher rapidement des élus locaux afin de réfléchir aux moyens à mettre en œuvre pour anticiper la montée en charge de la campagne vaccinale.
* Le ministère de la santé étudie différentes pistes de travail visant à accroitre la capacité actuelle de vaccination : augmenter les capacités des centres actuels, créer de nouveaux centres, dont de très gros centres (environ 1 000 à 1 500 injections par jour).

### Point sur les centres de vaccination et les plateformes de prises de rdv

* 1 270 centres de vaccination sont en activité au 4 mars 2021 avec environ 84 rendez-vous quotidiens en moyenne.
* Le ministère de la Santé observe que les centres de vaccination continuent d’ouvrir leurs rendez-vous au dernier moment pour deux raisons principales : ils souhaitent sécuriser les doses de vaccin reçues mais également sécuriser les professionnels de santé présents sur le centre pour procéder à la vaccination.
* L’objectif est d’ouvrir 2 400 000 rendez-vous pour des premières injections pour le mois de mars. Seuls 50% de ces rendez-vous ont pour l’heure été ouverts.