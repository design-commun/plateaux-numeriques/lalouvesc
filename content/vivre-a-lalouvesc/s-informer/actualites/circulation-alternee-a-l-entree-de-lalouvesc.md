+++
date = 2022-01-31T23:00:00Z
description = "Route d'Annonay du 2 au 8 février"
header = "/media/circulation-alternee.jpg"
icon = ""
subtitle = ""
title = "Circulation alternée à l'entrée de Lalouvesc"
weight = 1

+++
Pour permettre le remplacement d'un poteau d'incendie, la circulation automobile sera alternée à l'entrée de Lalouvesc sur la route d'Annonay du 2 au 8 février.  
Les travaux sont effectués par l'entreprise Faurie TP.