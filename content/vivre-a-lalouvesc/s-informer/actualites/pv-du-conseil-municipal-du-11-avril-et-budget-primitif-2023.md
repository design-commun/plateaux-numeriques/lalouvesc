+++
date = 2023-04-19T22:00:00Z
description = "Un conseil consacré aux finances"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "PV du Conseil municipal du 11 avril et Budget primitif 2023"
weight = 1

+++
Le Conseil municipal du 11 avril a été consacré uniquement aux questions financières : approbation des comptes de 2022, reports sur 2023, budget primitif 2023...

Vous trouverez ci-dessous les comptes-rendus :

* [PV Conseil municipal 2](https://www.lalouvesc.fr/media/2023-04-conseil-municipal-lalouvesc.pdf),
* [Budget primitif 2023](https://www.lalouvesc.fr/media/bp-2023-lalouvesc.pdf),
* [délibérations](https://www.lalouvesc.fr/media/deliberations-cm-11-04-23-lalouvesc.pdf),
* [liste des délibérations](https://www.lalouvesc.fr/media/liste-des-deliberations-11-04-2023.pdf)

Nous reviendrons avec plus d'explications sur l'évolution des finances de la Commune dans le Bulletin de mai.