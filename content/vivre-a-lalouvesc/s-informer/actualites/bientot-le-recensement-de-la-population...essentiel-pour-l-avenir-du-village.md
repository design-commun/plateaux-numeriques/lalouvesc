+++
date = 2023-01-12T23:00:00Z
description = "Il démarre jeudi 19 janvier... seuls les résidents sont concernés, mais tous les résidents."
header = "/media/recensement-2023-affiche.png"
icon = ""
subtitle = ""
title = "Bientôt le recensement de la population... essentiel pour l'avenir du village"
weight = 1

+++
**Le 19 janvier ou les jours suivants toutes les Louvetonnes et tous les Louvetous, résidant de façon permanente au village, recevront un courrier important dans leur boîte aux lettres.**

Il s'agit de toutes les instructions pour se faire recenser, c'est-à-dire répondre à un questionnaire sur leur situation actuelle (pour plus de détails, voir l'image ci-dessous). Seuls les résidents permanents sur le village sont concernés.

Toutes les réponses à vos interrogations sur le recensement sont sur le [site de l'Insee](https://www.le-recensement-et-moi.fr/reponses/), vous pouvez aussi interroger directement notre agent recenseur, Nicole Porte.

**Vos réponses au questionnaire du recensement sont essentielles pour l'avenir du village car elles conditionnent les dotations de l’État pour les cinq années à venir.** Il ne faut donc pas [procrastiner](https://fr.wikipedia.org/wiki/Procrastination), oublier de répondre ou rechigner. Aucune des données individuelles collectées par l'Insee n'est divulguée à une autre institution ou administration. Elles ne servent que globalement, sous forme de statistiques.

### Comment répondre

Vous aurez deux possibilités pour répondre au questionnaire :

* La plus simple : directement par internet en vous connectant sur le site dédié et en entrant le code qui sera inscrit sur le courrier qui vous sera distribué à partir du 19 janvier (chaque logement aura un code personnalisé). C'est plus simple, car vous serez guidé par l'ouverture des fenêtres au fur et à mesure de vos réponses. Et la confidentialité est assurée par le système. Si vous lisez ces lignes, vous avez accès à une connexion, il est donc préférable pour vous de privilégier cette formule ;
* Les personnes qui n'ont pas de connexion ou ne sont pas à l'aise pour répondre en ligne, seront contactées directement par Nicole Porte qui leur fournira un questionnaire papier et pourra aussi, le cas échéant, les aider à le remplir.

Le recensement est une opération lourde pour la Commune, plus vite vous remplirez les questionnaires, plus vous éviterez les relances et plus simple sera sa gestion.

![](/media/insee-recensement-2023-questions.jpg)