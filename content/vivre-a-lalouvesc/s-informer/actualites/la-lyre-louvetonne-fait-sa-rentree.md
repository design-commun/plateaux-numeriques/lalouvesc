+++
date = 2021-10-15T22:00:00Z
description = "C'est vendredi 22 octobre 2021 à 20h30 à la salle de musique. Porte ouverte..."
header = "/media/lyre-louvetonne-17-juillet-2020.jpg"
icon = ""
subtitle = ""
title = "La Lyre Louvetonne fait sa rentrée"
weight = 1

+++
La Lyre Louvetonne fait sa rentrée vendredi 22 octobre 2021 à 20h30 à la salle de musique (CAC).

N’hésitez pas à pousser la porte de la salle et à venir découvrir ce qui s’y passe !

… Et comme dit le proverbe : « Plus on est de fous, plus on rit. »

… Alors n’hésitez pas ! La Lyre Louvetonne a besoin de musiciens…