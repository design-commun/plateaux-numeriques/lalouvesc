+++
date = 2022-07-05T22:00:00Z
description = "Ordre du jour"
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 11 juillet à 20h"
weight = 1

+++
Un Conseil municipal  est convoqué en Mairie pour le 11 juillet à 20h.

## Ordre du jour

1\. COMMISSION FINANCES

a. Première décision modificative

b. Passage à la nouvelle nomenclature M 57 développée au 1 er janvier 2023

2\. COMMISSION GESTION

a. Tarif pour le lave-linge et le sèche-linge en libre-service au camping

b. Élargissement de la zone d'assainissement collectif

3\. COMITÉ VIE LOCALE

a. Convention ouverture au public de sentier

b. Convention autorisation pose de figurines

4\. COMITÉ DÉVELOPPEMENT

a. Composition et réunions pour l'année 2022 du Comité de développement

b. Résultat du concours d'idées sur le Jeu-Monument

c. Rencontre avec les sœurs du Cénacle et premier comité de pilotage de l'étude du SCOT sur le Cénacle et Sainte Monique

d. Acceptation du devis du maitre d’œuvre Archipolis Pour l'espace « Beauséjour »

e. Convention EPORA

5\. DIVERS