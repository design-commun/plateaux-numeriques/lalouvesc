+++
date = 2022-12-30T23:00:00Z
description = "n°30 - Janvier 2023 - Maintenons le cap"
header = "/media/entete-hiver-2022.png"
icon = ""
subtitle = "n°30 - Janvier 2023 - Maintenons le cap"
title = "Bonne et heureuse année, le premier Bulletin est paru !"
weight = 1

+++
![](/media/voeux-2023-lalouvesc.png)Maintenir le cap, c’est faire aussi bien, sinon mieux, en 2023 qu’en 2022, année de crises en Europe et dans le monde mais pourtant année faste pour le village. Quelques étapes de 2022 sont rappelées dans ce Bulletin par des photos. Vous y trouverez aussi les étonnantes réalisations de l’école, le programme 2023 des principales associations, la date de la fête du centenaire de l’Abri du pèlerin, des invitations à la balade, une carte des loyers, bien sûr des vœux pour la nouvelle année… et encore, comme toujours, bien d’autres informations.

### [Lire le Bulletin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/) (cliquer)

### Sommaire

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#mot-du-maire)
* [Actualités de la Mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#actualités-de-la-mairie)
  * [Les vœux du Maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#les-vœux-du-maire)
  * [Réunion publique sur l’avenir de Ste Monique et du Cénacle](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#réunion-publique-sur-lavenir-de-ste-monique-et-du-cénacle)
  * [Babets ou repas festif](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#babets-ou-repas-festif)
  * [La CCVA sur Panneau Pocket](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#la-ccva-sur-panneau-pocket)
  * [Très bientôt le recensement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#très-bientôt-le-recensement)
* [ZOOM : D’une année à l’autre](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#zoom--dune-année-à-lautre)
  * [Un mois, une image](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#un-mois-une-image)
  * [Vœux](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#vœux)
* [Sanctuaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#sanctuaire)
  * [Le centenaire de l’Abri du pèlerin](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#le-centenaire-de-labri-du-pèlerin)
* [École Saint Joseph](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#école-saint-joseph)
  * [Des enfants entreprenants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#des-enfants-entreprenants)
  * [Tombola, calendrier et fromage à l’APEL](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#tombola-calendrier-et-fromage-à-lapel)
* [Culture - Loisirs](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#culture---loisirs)
  * [On prépare l’année 2023…](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#on-prépare-lannée-2023)
  * [Balades louvetonnes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#balades-louvetonnes)
* [Logement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#logement)
  * [Carte des loyers](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#carte-des-loyers)
  * [Coupures de courant : réfrigérateur, congélateur… précautions alimentaires](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#coupures-de-courant--réfrigérateur-congélateur-précautions-alimentaires)
  * [Monoxyde de carbone : comment prévenir les intoxications](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#monoxyde-de-carbone--comment-prévenir-les-intoxications)
* [Rivières](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#rivières)
  * [Dates d’ouverture de la pêche](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#dates-douverture-de-la-pêche)
  * [L’agence de l’eau nous parle de la biodiversité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#lagence-de-leau-nous-parle-de-la-biodiversité)
* [Naissances, mariages, décès en 2022](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#naissances-mariages-décès-en-2022)
* [Dans l’actualité le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#dans-lactualité-le-mois-dernier)
* [Un nouveau logo à découvrir (puzzle)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2023/30-maintenons-le-cap/#un-nouveau-logo-à-découvrir-puzzle)