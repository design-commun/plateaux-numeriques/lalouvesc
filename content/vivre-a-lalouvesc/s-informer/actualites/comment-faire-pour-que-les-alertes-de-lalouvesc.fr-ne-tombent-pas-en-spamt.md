+++
date = 2023-01-15T23:00:00Z
description = "Pour les abonnés email en @orange.fr"
header = ""
icon = ""
subtitle = ""
title = "Comment faire pour que les alertes de lalouvesc.fr ne tombent pas en spam"
weight = 1

+++
Plusieurs abonnés de la liste d'alertes du lundi sur les actualités du site, se sont plaints de ne plus les recevoir. Nous n'avons, bien entendu, désabonné personne. Mais il semble que Orange ait décidé de classer notre liste dans les spams.

Si vous êtes dans ce cas, vous retrouverez tous les messages dans votre boîte de spams. Il faut les replacer dans votre boîte de réception. Progressivement les choses devraient rentrer dans l'ordre.

Comment réduire cette difficulté définitivement et rapidement ? Il faut remettre l'adresse de la liste : mairielalouvesc@gmail.com dans la liste verte de votre boîte. Tout est expliqué sur [cette page de l'assistance Orange](https://assistance.orange.fr/ordinateurs-peripheriques/installer-et-utiliser/l-utilisation-du-mail-et-du-cloud/mail-orange/mail-orange/les-options-de-securite/messagerie-mail-orange-parametrer-les-options-de-securite_26256-26904).

Faites-suivre ce conseil à tous les abonnés orange.fr de votre connaissance intéressés par notre service. Il y a 76 orange.fr à être abonnés à la liste de lalouvesc.fr.