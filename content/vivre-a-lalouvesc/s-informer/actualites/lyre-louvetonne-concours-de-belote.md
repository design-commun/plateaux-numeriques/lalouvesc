+++
date = 2023-03-02T23:00:00Z
description = "Le dimanche 12 mars au CAC"
header = "/media/voeux-maire-2023-2.jpg"
icon = ""
subtitle = "Le dimanche 12 mars au CAC"
title = "Lyre louvetonne : concours de belote"
weight = 1

+++
Inscription 15 € la doublette. Toutes les doublettes primées.

![](/media/belote-lyre-mars-2023.jpg)