+++
date = 2022-07-18T22:00:00Z
description = "Un site pour informer sur la qualité de l'eau des plages de la rivière Ardèche"
header = "/media/plages-ardeche-2022.jpg"
icon = ""
subtitle = ""
title = "\"Ardèche Inf'eau Plage\" est en ligne !"
weight = 1

+++
Ardèche, Beaume, Chassezac, Lignon, Borne... 26 plages "publiques" en rivière sur lesquelles la qualité des eaux de baignade est contrôlée tout au long de la saison estivale.  
  
En ce début d'été 2022, l'EPTB(*) Ardèche lance "[**Ardèche Inf'eau Plage**](https://qualite.ardeche-eau.fr)", le site Internet officiel d'information sur la qualité de l'eau et les conditions de baignade à l'échelle du grand bassin versant de la rivière Ardèche.  
  
(*) Contact Etablissement Public Territorial du Bassin Versant de l'Ardèche   
tél. 04 75 37 82 20  
Chargée de mission qualité des eaux de baignade - [qualite@ardeche-eau.fr](mailto:qualite@ardeche-eau.fr "mail")  
Chargée de communication - [communication@ardeche-eau.fr](mailto:communication@ardeche-eau.fr "mail")