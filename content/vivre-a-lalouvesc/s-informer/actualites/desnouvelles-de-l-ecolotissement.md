+++
date = 2021-11-09T23:00:00Z
description = "Signature du 4e compromis, lancement prochain des travaux... et vandalisme"
header = "/media/ecolotissemet-plan.jpg"
icon = ""
subtitle = ""
title = "Des nouvelles de l'écolotissement"
weight = 1

+++
Le quatrième compromis de vente sera signé ce vendredi chez le notaire. L'appel d'offres pour l'aménagement est clos. Le maître d’œuvre sera bientôt choisi pour un démarrage des travaux début 2022. L'aménagement devrait être achevé vers Pâques.

Avec la démolition de l'hôtel Beauséjour, nous aurons donc d'importants chantiers au premier trimestre de l'année prochaine. Le renouveau du village se poursuit.

Dommage qu'il faille déplorer encore quelques actes stupides de vandalisme. La nuit du Trail, trois panneaux présentant l'écolotissement ont été volontairement renversés. La brigade de gendarmerie est venue constater les dégâts.

{{<grid>}} {{<bloc>}}

![](/media/ecolotissement-2021-panneau-casse-1.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ecolotissement-2021-panneau-casse-2.jpg)

{{</bloc>}}{{</grid>}}

Pas de confusion donc, l'absence de ces panneaux ne signifie pas que l'offre est close : sur les 9 lots, 5 sont encore disponibles. Les lots situés aux quatre coins ont été réservés, plusieurs autres acquéreurs potentiels ont montré un intérêt. Si vous souhaitez construire à Lalouvesc, ne tardez pas trop : ce sont les derniers terrains constructibles.