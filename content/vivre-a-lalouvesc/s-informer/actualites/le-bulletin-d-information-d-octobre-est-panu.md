+++
date = 2022-09-30T22:00:00Z
description = "Résumé, lien et sommaire"
header = "/media/entete-automne.jpg"
icon = ""
subtitle = "Bulletin n°27 Une bonne rentrée"
title = "Le Bulletin d'information d'octobre est paru"
weight = 1

+++
C'est l'automne, la saison estivale est derrière nous. La vie du village se déplace. Dans ce Bulletin la place d'honneur est laissée à l'école, le cœur battant du village hors-saison. On y parle aussi de l'eau, de l'avancement des chantiers ou dossiers, de l'organisation souterraine la brocante et vous y trouverez encore, comme toujours, bien d'autres informations.

### [**Lire le Bulletin**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/) **(cliquer)**

#### Entrées par le sommaire

* [Actualités de la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#actualités-de-la-mairie)
  * [L’eau trouble (suite)](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#leau-trouble-suite)
  * [Curage de l’étang du camping](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#curage-de-létang-du-camping)
  * [Ordures ménagères](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#ordures-ménagères)
  * [Espace Beauséjour](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#espace-beauséjour)
  * [Cénacle et Sainte Monique](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#cénacle-et-sainte-monique)
  * [Pourquoi la taxe foncière augmente ?](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#pourquoi-la-taxe-foncière-augmente-)
* [Zoom sur l’école St Joseph](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#zoom-sur-lécole-st-joseph)
  * [Vincent, comment fonctionne l’école Saint-Joseph ?](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#vincent-comment-fonctionne-lécole-saint-joseph-)
  * [Sasha, comment avez-vous connu Lalouvesc et notre école ?](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#sasha-comment-avez-vous-connu-lalouvesc-et-notre-école-)
* [Loisir - Culture](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#loisir---culture)
  * [Brocante, une chaîne de solidarité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#brocante-une-chaîne-de-solidarité)
  * [Trésors de la nature au centre de Lalouvesc](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#trésors-de-la-nature-au-centre-de-lalouvesc)
  * [C](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#c)
  * [Programme du Centre de loisirs - Vacances d’automne](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#programme-du-centre-de-loisirs---vacances-dautomne)
* [Santé - Sécurité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#santé---sécurité)
  * [Opération Brioche ADAPEI 10 octobre](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#opération-brioche-adapei-10-octobre)
  * [Aidants](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#aidants)
  * [Suite du calendrier Santé-Environnement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#suite-du-calendrier-santé-environnement)
* [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#dans-lactualité-le-mois-dernier)
  \*
* [Recette de bouillabaisse louvetonne](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/27-une-bonne-rentree/#recette-de-bouillabaisse-louvetonne)