+++
date = 2021-05-31T22:00:00Z
description = "Bulletin du mois de juin"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = ""
title = "Le Bulletin d'information n°11 est paru"
weight = 1

+++
L'été approche. Balayage, fleurissement, nouveau plan de stationnement, nouveaux hébergements de qualité sur le camping, préparation des décorations, présentation des événements... le village se fait beau pour accueillir les estivants.

Dans ce bulletin vous trouverez aussi un hommage de ses amis à Geny Detto qui nous a quittés. Et comme toujours, plein d'autres informations.

[Lire le Bulletin](/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2021/11-embellir/)