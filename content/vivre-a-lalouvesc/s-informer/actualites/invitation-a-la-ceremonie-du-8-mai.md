+++
date = 2022-05-03T22:00:00Z
description = "Défilé et cérémonie au Monument aux morts"
header = "/media/8-mai-2021.jpg"
icon = ""
subtitle = ""
title = "Invitation à la cérémonie du 8 mai"
weight = 1

+++
Vous êtes invités à participer aux cérémonies commémoratives de l'armistice du 8 mai 1945, dimanche 8 mai 2022 :

* Défilé, départ de la basilique à 11h15
* Cérémonie du souvenir à 11h30 au monument au mort.