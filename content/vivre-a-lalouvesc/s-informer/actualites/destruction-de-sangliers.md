+++
date = 2023-04-03T22:00:00Z
description = "Du 3 avril au 3 mai sur Lafarre et Lalouvesc"
header = "/media/sanglier-wkp.jpg"
icon = ""
subtitle = ""
title = "Destruction de sangliers"
weight = 1

+++
Compte-tenu des nuisances et des risques répétés sur les terres agricoles, les jardins et les équipements, les lieutenants de louveterie M CHABRIOL Jean-Louis et M. BRUNEL Mickael sont chargés par arrêté préfectoral de détruire les sangliers compromettant la sécurité et les cultures, soit sous forme de battue, soit individuellement, soit par tir à l'affût, soit par tir de nuit à l'affût sur les territoires communaux de LALOUVESC et LAFARRE. 

Les opérations auront lieu dans la période du 3 avril au 3 mai.

Voir l'[arrêté préfectoral](/media/ap-destruction-sangliers_lalouvesc-et-lafarre.pdf).

### Qu'est-ce qu'un lieutenant de louveterie ?

{{<grid>}}{{<bloc>}}

![](/media/insigne_lieutenant_de_louveterie.svg)

{{</bloc>}}{{<bloc>}}

En France, un **lieutenant de louveterie** ou **louvetier** est une personne privée exerçant à titre bénévole une fonction civique d'auxiliaire de l’État auprès des services publics de la commune dans laquelle ils sont domiciliés en matière de faune sauvage, y compris sur le plan sanitaire. L'appellation remonte à Louis XI.

{{</bloc>}}{{</grid>}}

La louveterie est aujourd’hui chargée de veiller à la régulation de certaines espèces dites _« nuisibles »_ et au maintien de l’équilibre de la faune sauvage, les lieutenants de louveterie étant reconvertis en « conseillers cynégétiques » et « auxiliaires de l'agriculture ». Tout est expliqué sur [Wikipédia](https://fr.wikipedia.org/wiki/Lieutenant_de_louveterie).