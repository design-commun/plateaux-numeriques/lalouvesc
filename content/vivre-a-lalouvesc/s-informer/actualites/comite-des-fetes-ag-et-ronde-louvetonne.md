+++
date = 2023-01-19T23:00:00Z
description = "Affiches"
header = "/media/logo-cdf-2023.png"
icon = ""
subtitle = ""
title = "Comité des fêtes : AG et Ronde louvetonne"
weight = 1

+++
Plus de détails dans le Bulletin de février...

{{<grid>}}{{<bloc>}}

![](/media/ag-cdf-2023.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/ronde-louvetonne_2023.jpg)

{{</bloc>}}{{</grid>}}