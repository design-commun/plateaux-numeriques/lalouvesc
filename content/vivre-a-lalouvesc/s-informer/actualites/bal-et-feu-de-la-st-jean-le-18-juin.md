+++
date = 2022-06-09T22:00:00Z
description = "... à partir de 20h30 sur le terrain de boules, parc du Val d'Or organisés par la Lyre Louvetonne."
header = "/media/feu-st-jean-2021-1.jpg"
icon = ""
subtitle = "... à partir de 20h30 sur le terrain de boules, parc du Val d'Or organisés par la Lyre Louvetonne."
title = "Bal et feu de la St Jean, le 18 juin"
weight = 1

+++
![](/media/bal-st-jean-2022.jpg)