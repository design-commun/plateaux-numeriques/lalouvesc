+++
date = 2022-09-01T22:00:00Z
description = "10 sept 18h30 Bar du Lac"
header = "/media/theatre-cie-turpaux-2022-2.jpg"
icon = ""
subtitle = ""
title = "Théâtre d'improvisation en itinérance"
weight = 1

+++
_L'été dernier nous étions venues en période touristique par chez vous._

 _En 2022 nous avons été en Nord Isère sur juillet._

 _Mais pour cette rentrée nous revenons sur notre parcours Ardéchois pour une Marche d'Automne 2022, traversant 8 communes._

 _Ainsi nous serons à St Péray Mardi prochain à 18h30, puis nous rejoindrons Saint Sylvestre à pied le lendemain et ainsi de suite jusqu'à votre commune de Lalouvesc le 10 septembre._

 _Nous jouerons sur la terrasse du Bar du Lac de votre commune._

Anne-Sophie Ortiz-Balin

![](/media/theatre-cie-turpaux-2022.jpg)