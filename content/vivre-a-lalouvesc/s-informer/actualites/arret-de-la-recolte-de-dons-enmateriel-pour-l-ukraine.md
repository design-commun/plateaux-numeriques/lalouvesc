+++
date = 2022-03-16T23:00:00Z
description = "Privilégiez les dons en argent"
header = "/media/ukraine-ardeche-2022.jpg"
icon = ""
subtitle = ""
title = "Arrêt de la récolte de dons en matériel pour l'Ukraine"
weight = 1

+++
La logistique posant des problèmes pour la distribution sur place, l'AMF a décidé de suspendre la récolte des dons en matériel. Ils ne seront donc plus récupérés à la Mairie.

Mais, évidemment, les dons en argent sont encouragés. Pour les dons en argent, il faut s’adresser à la Croix-Rouge : [https://donner.croix-rouge.fr/urgence-ukraine/](https://donner.croix-rouge.fr/urgence-ukraine/ "https://donner.croix-rouge.fr/urgence-ukraine/") ou par chèque à l’attention de : « Croix-Rouge française – Urgence Ukraine » à l’adresse suivante : Croix-Rouge française, CS 20011 - 59895 Lille cedex 9

Pour toute information concernant les dons : 09 70 82 05 05