+++
date = 2022-06-12T12:00:00Z
description = "Près de 61% de votants..."
header = "/media/elections.jpg"
icon = ""
subtitle = ""
title = "Résultats du premier tour des élections législatives à Lalouvesc et partout"
weight = 1

+++
Le premier tour des législatives est passé, ci-dessous les résultats et **rendez-vous pour le second tour dimanche 19 juin**, ouverture du bureau de vote à la Mairie de 8h à 18h.

Deux candidats restent à départager : Olivier Dussopt (Ensemble !) et Christophe Goulouzelle (NUPES).

## Résultats du premier tour des législatives de 2022

### Lalouvesc

| Liste des candidats | Nuances | Voix | % Inscrits | % Exprimés |
| --- | --- | --- | --- | --- |
| Mme Claire GUILMIN | LP | 4 | 1,17 | 1,95 |
| Mme Michèle GAILLARD | LO | 0 | 0 | 0 |
| M. Cyrille GRANGIER | RN | 24 | 7,02 | 11,71 |
| M. Sébastien GLADIEUX | MR | 6 | 1,75 | 2,93 |
| M. Marc-Antoine QUENETTE | LR | 53 | 15,50 | 25,85 |
| M. Martin CHAIZE | MAU | 1 | 0,29 | 4,88 |
| M. Christophe GOULOUZELLE | NUP | 31 | 9,06 | 15,12 |
| M. Philippe BORY | Rec | 15 | 4,38 | 7,32 |
| M. Olivier DUSSOPT | Ens | 71 | 20,76 | 34,63 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 342 |  |  |
| Abstentions | 134 | 39,18 |  |
| Votants | 208 | 60,82 |  |
| Blancs | 3 | 0,88 | 1,44 |
| Nuls | 0 | 0,00 | 0,00 |
| Exprimés | 205 | 59,94 | 98,56 |

### 2e circonscription d'Ardèche

Taux d'abstention : 47,19%

| Liste des candidats | Nuances | Voix | % Inscrits | % Exprimés |
| --- | --- | --- | --- | --- |
| Mme Claire GUILMIN | LP | 777 | 1,55 |  |
| Mme Michèle GAILLARD | LO | 640 | 1,28 |  |
| M. Cyrille GRANGIER | RN | 9.512 | 19,03 |  |
| M. Sébastien GLADIEUX | MR | 641 | 1,28 |  |
| M. Marc-Antoine QUENETTE | LR | 8.941 | 17,89 |  |
| M. Martin CHAIZE | MAU | 902 | 1,80 |  |
| M. Christophe GOULOUZELLE 2e tour | NUP | 11.785 | 23,58 |  |
| M. Philippe BORY | Rec | 1.767 | 3,54 |  |
| M. Olivier DUSSOPT 2e tour | Ens | 15.014 | 30,04 |  |

## Rappel des résultats du premier tour des législatives de 2017

### Lalouvesc

| Liste des candidats | Nuances | Voix | % Inscrits | % Exprimés |
| --- | --- | --- | --- | --- |
| Mme Laurette GOUYET-POMMARET | REM | 57 | 16,96 | 29,08 |
| M. Marc-Antoine QUENETTE | LR | 37 | 11,01 | 18,88 |
| M. Olivier DUSSOPT | SOC | 35 | 10,42 | 17,86 |
| Mme Odile LASFARGUES-BOUYON | FN | 30 | 8,93 | 15,31 |
| M. Jean-Paul VALLON | DVD | 17 | 5,06 | 8,67 |
| Mme Samia HASNAOUI | FI | 9 | 2,68 | 4,59 |
| Mme Isabelle FRANÇOIS | DLF | 4 | 1,19 | 2,04 |
| Mme Nadia SENNI | ECO | 4 | 1,19 | 2,04 |
| Mme Isabelle STRUBI | DIV | 2 | 0,60 | 1,02 |
| M. Daniel BABAROSSA | COM | 1 | 0,30 | 0,51 |
| M. Christophe MARCHISIO | EXG | 0 | 0,00 | 0,00 |
| M. Jean-Paul Louis VALLON | EXD | 0 | 0,00 | 0,00 |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 336 |  |  |
| Abstentions | 138 | 41,07 |  |
| Votants | 198 | 58,93 |  |
| Blancs | 0 | 0,00 | 0,00 |
| Nuls | 2 | 0,60 | 1,01 |
| Exprimés | 196 | 58,33 | 98,99 |

### 2e circonscription d'Ardèche

| Liste des candidats | Nuances | Voix | % Inscrits | % Exprimés | Elu(e) |
| --- | --- | --- | --- | --- | --- |
| Mme Laurette GOUYET-POMMARET | REM | 12 718 | 13,67 | 25,96 | Ballotage** |
| M. Olivier DUSSOPT | SOC | 11 531 | 12,39 | 23,54 | Ballotage** |
| M. Marc-Antoine QUENETTE | LR | 6 585 | 7,08 | 13,44 | Non |
| Mme Odile LASFARGUES-BOUYON | FN | 6 026 | 6,48 | 12,30 | Non |
| Mme Samia HASNAOUI | FI | 4 897 | 5,26 | 10,00 | Non |
| M. Jean-Paul VALLON | DVD | 3 293 | 3,54 | 6,72 | Non |
| Mme Nadia SENNI | ECO | 1 460 | 1,57 | 2,98 | Non |
| Mme Isabelle FRANÇOIS | DLF | 1 116 | 1,20 | 2,28 | Non |
| M. Daniel BABAROSSA | COM | 859 | 0,92 | 1,75 | Non |
| M. Christophe MARCHISIO | EXG | 263 | 0,28 | 0,54 | Non |
| Mme Isabelle STRUBI | DIV | 239 | 0,26 | 0,49 | Non |
| M. Jean-Paul Louis VALLON | EXD | 0 | 0,00 | 0,00 | Non |

|  | Nombre | % Inscrits | % Votants |
| --- | --- | --- | --- |
| Inscrits | 93 064 |  |  |
| Abstentions | 43 197 | 46,42 |  |
| Votants | 49 867 | 53,58 |  |
| Blancs | 627 | 0,67 | 1,26 |
| Nuls | 253 | 0,27 | 0,51 |
| Exprimés | 48 987 | 52,64 | 98,24 |

## Tous les résultats des législatives

Retrouvez tous les résultats de toutes les élections législatives de tous les départements sur le [site du Ministère de l'intérieur](https://www.interieur.gouv.fr/Elections/Les-resultats/Legislatives).