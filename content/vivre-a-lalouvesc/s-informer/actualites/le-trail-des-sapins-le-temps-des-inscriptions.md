+++
date = 2022-09-21T22:00:00Z
description = "Une compétition et une ambiance"
header = "/media/trail-deco-2021-3.jpg"
icon = ""
subtitle = ""
title = "Le Trail des Sapins : le temps des inscriptions "
weight = 1

+++
Le Trail des Sapins est prévu comme toujours ou presque (déjà six ans) pour Halloween. Mille coureurs sont attendus pour ce trail en semi-nocturne à l'ambiance très spéciale organisé par le Comité des fêtes avec déguisements et repas de spécialités ardéchoises.

Cette année un partenariat a été signé avec @SeinformerCancer. Le Comité des fêtes s'est engagé à reverser 1€ par inscription et 1€ par tee-shirt vendu.

48 km, 22 km, 12 km et 7 km, comme chaque année quatre parcours inédits sont proposés. Les deux plus courts sont ouverts à la randonnée, les deux plus longs permettent d’engranger des points pour l’UTMB.

Déjà près des 350 inscrits. Toutes les[ infos sont ici](https://www.traildessapins-lalouvesc.fr/infos-pratiques/).

![](/media/affiche-trail-des-sapins-2022.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/HH68ibYv6po" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>