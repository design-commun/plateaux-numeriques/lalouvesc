+++
date = 2022-03-06T23:00:00Z
description = "Recensement des initiatives"
header = "/media/ukraine-ardeche-2022.jpg"
icon = ""
subtitle = ""
title = "Solidarité avec l'Ukraine en Ardèche"
weight = 1

+++
Suite à l'aggravation de la situation en Ukraine, la préfecture de l'Ardèche coordonne les initiatives de solidarité pour le département.

Un recensement des possibilités d'hébergements institutionnels est en cours. Pour les particuliers désireux de s'impliquer, on trouvera ci-dessous la liste des possibilités.

**• Recensement des offres d’hébergement des personnes physiques (initiative citoyenne, particuliers)**

Les personnes physiques qui souhaitent accompagner des ressortissants ukrainiens sont invitées à se signaler sur le site [https://parrainage.refugies.info/](https://parrainage.refugies.info/ "https://parrainage.refugies.info/")

Cette plateforme a vocation à recenser des initiatives d’aide de différentes natures (insertion professionnelle, éducation, rencontres/loisirs), et en particulier les initiatives d’hébergement solidaire. Les particuliers volontaires seront mis en relation à cette fin avec des associations.

Les habitants qui souhaiteraient apporter une aide ont la possibilité de s’inscrire sur la plateforme [https://www.jeveuxaider.gouv.fr/](https://www.jeveuxaider.gouv.fr/ "https://www.jeveuxaider.gouv.fr/")

**• Collecte de dons**

La Croix-Rouge française lance un appel à dons, en soutien de la Croix-Rouge ukrainienne, du Comité International de la Croix-Rouge et de toutes les sociétés Croix-Rouge qui interviennent dans les pays limitrophes, pour venir en aide à toutes les populations touchées par le conflit.

Faites un don sur : [https://donner.croix-rouge.fr/urgence-ukraine/](https://donner.croix-rouge.fr/urgence-ukraine/ "https://donner.croix-rouge.fr/urgence-ukraine/") ou par chèque à l’attention de : « Croix-Rouge française – Urgence Ukraine » à l’adresse suivante : Croix-Rouge française, CS 20011 - 59895 Lille cedex 9

Pour toute information concernant les dons : 09 70 82 05 05

La Protection Civile appelle à la solidarité nationale pour soutenir la population. Une mission d’action humanitaire immédiate ainsi qu’un appel aux dons sont en cours. Les dons peuvent être effectués par sms en envoyant le mot « DON » au 92 3 92 ou via le site internet : don.protection-civile.org

Pour tout renseignement : mission.ukraine@protection-civile.org et 01 41 21 21 21

Avant de faire un don, consultez [La liste de dons matériels pour la mission Ukraine ](http://www.ardeche.gouv.fr/IMG/pdf/liste_de_dons_materiels_mission_ukraine_maj_03_03_2022.pdf "La liste de dons matériels pour la mission Ukraine  - pdf - 125.6 ko (ouverture nouvelle fenêtre)")(format pdf - 125.6 ko - 04/03/2022)

Par ailleurs, deux partenariats nationaux ont été mis en place entre, d’une part, l’Association des Maires de France (AMF) et la Protection Civile, et d’autre part, les Départements de France et la Croix-Rouge pour centraliser les dons de toute nature.

**La mairie de Lalouvesc va organiser la collecte locale de dons. Plus d'informations dans une prochaine actualité.**

***