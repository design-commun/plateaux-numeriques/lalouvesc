+++
date = 2022-08-04T22:00:00Z
description = "Comment un incident de parcours permet de régler un problème récurrent"
header = "/media/source-du-perrier2.jpg"
icon = ""
subtitle = "Comment un incident de parcours permet de régler un problème récurrent"
title = "Coupure d'eau et eau trouble à Chante Ossel et Grand Lieu"
weight = 1

+++
Le récit de la partie souterraine des récentes mésaventures de plusieurs Louvetous concernant leur alimentation en eau mérite d'être conté. Comme on le verra, elles n'ont rien à voir avec la sécheresse, mais beaucoup avec les défauts d'entretien du réseau.

### Une eau trouble

Nous avons été alertés par plusieurs habitants des secteurs Chante Ossel et Grand Lieu d'un problème d'eau trouble au robinet. Le problème ne nous était pas inconnu, il arrivait régulièrement en période estivale. Les résidences secondaires étant fermées l'hiver, l'eau dans les canalisations stagnait et les particules s'y déposaient et réapparaissaient à la belle saison au moment de la réouverture des compteurs.

### Une coupure anormale

Nous avons tenté de purger le circuit grâce à l'ouverture d'une borne incendie sur la partie basse de la zone impactée. Mais cela a eu pour conséquence de désamorcer le circuit d'alimentation d'eau du quartier. Ce désamorçage était anormal car les circuits haut et bas d'alimentation sont interconnectés.

Nous avons d'abord soupçonné une fuite d'eau invisible en surface. Mais cette hypothèse a été rapidement écartée. Les réservoirs étaient au plus haut. Les alimentations de la partie basse du secteur fonctionnaient, seule une partie des maisons situées sur le lotissement Chante Ossel et à gauche de la route de Saint-Bonnet restaient privées d'eau.

### Une vanne ensablée

L'interconnexion des réseaux était donc coupée entre les deux secteurs. À cet endroit une grosse vanne sectionnaire nous a paru suspecte. Elle était ouverte et pourtant ne semblait pas laisser passer l'eau. Le diagnostic de l'entreprise FAURIE, spécialisée dans la remise en état de nos circuits d´eau, alors contactée, a confirmé nos soupçons : la vanne était bouchée.

Cette vanne possède un gros filtre tamis qui doit être nettoyé périodiquement et, depuis sa pause en 2013, personne ne semble s'en être préoccupé. La vanne était ensablée. Le filtre a donc été nettoyé, la vanne réglée et remise en service.

### Morale de l'histoire

La réparation a permis ainsi, non seulement de rétablir l'alimentation en eau, mais aussi, espérons-le, de régler le problème récurrent d'eau trouble du secteur.

La détection de la panne a été rapide et efficace grâce au plan récemment effectué (schéma directeur du réseau) qui nous permet de raisonner en visualisant les circuits et de connaître leur implantation.

Cette mésaventure souligne une nouvelle fois l'importance de l'entretien régulier des infrastructures du village.

### Qualité de l'eau

Suite à cet incident, plusieurs habitants se sont inquiétés de la qualité de l'eau du village. Rappelons que celle-ci est contrôlée très régulièrement par l'ARS. Le dernier contrôle a eu lieu le 27 juillet. Il est affiché en mairie.

La qualité de l’eau est publiée aussi en temps réel par l’ARS, commune par commune. On peut y avoir accès en ligne par [ce site](https://orobnat.sante.gouv.fr/orobnat/afficherPage.do?methode=menu&usd=AEP&idRegion=84) (entrer le nom du département et la commune).

Voici le dernier diagnostic de qualité : "eau d'alimentation conforme aux limites de qualité et non conforme aux références de qualité", autrement dit l'eau est bonne, mais trop douce (manque de calcaire dû à la géologie du sol).