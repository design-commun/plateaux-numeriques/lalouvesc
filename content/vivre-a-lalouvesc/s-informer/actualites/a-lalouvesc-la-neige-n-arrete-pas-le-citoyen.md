+++
date = 2021-04-17T22:00:00Z
description = "Le tennis est nettoyé, les refuges sont réparés, le grillage du mini-golf est posé... "
header = "/media/journee-citoyenne-jour-3-6.jpg"
icon = ""
subtitle = ""
title = "A Lalouvesc, la neige n'arrête pas le citoyen"
weight = 1

+++
On dit que la pluie n'arrête pas le pèlerin. Mais à Lalouvesc la neige n'arrête pas le citoyen. Pour la troisième et dernière journée citoyenne, samedi 17 avril au matin, la neige recouvrait le camping.

{{<grid>}}

{{<bloc>}}

![](/media/journee-citoyenne-jour-3-9.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-jour-3-10.jpg)

{{</bloc>}}

{{</grid>}}

![](/media/journee-citoyenne-jour-3-11.jpg)

Pourtant à midi l'ensemble de la terre qui restait sur le revêtement était grattée. A 16h, elle était entièrement évacuée. Ce n'était pas rien : il a fallu pas moins de quinze voyages du camion !

![](/media/journee-citoyenne-jour-3-3.jpg)

Une douzaine de Louvetonnes, Louvetous et apparentés, petits et grands, étaient sur le tennis, d'autres continuaient la réparation des refuges, d'autres encore terminaient le grillage du mini-golf.

{{<grid>}}

{{<bloc>}}

![](/media/journee-citoyenne-jour-3-4.jpg)

{{</bloc>}}

{{<bloc>}}

![](/media/journee-citoyenne-jour-3-7.jpg)

{{</bloc>}}

{{</grid>}}

Les enfants présents, après avoir participé au travail, ont testé les 18 trous du mini-golf et nous ont confirmé que l'ensemble du parcours prenait moins de deux heures.

Plusieurs citoyens ont indiqué vouloir poursuivre pour terminer la peinture du mini-golf avant l'ouverture du camping.

Encore une fois, un grand merci à toutes et tous !

### Plus jamais ça !

![/media/journee-citoyenne-jour-3-1.jpg](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/journee-citoyenne-jour-3-1.jpg)La dernière pelletée...

![/media/journee-citoyenne-jour-3-2.jpg](https://app.forestry.io/sites/cwzrmliwzyy5-a/body-media//media/journee-citoyenne-jour-3-2.jpg)

Les deux photos satellites ci-dessous du tennis du Camping du Moulin, prises à deux années d'intervalles, témoignent de la vitesse à laquelle peut se produire une dégradation lorsque l'on ne prend pas les mesures préventives indispensables.

{{<grid>}}

{{<bloc>}}

![](/media/tennis-satellite-2018-google-map.jpg)2018 (Google Map)

{{</bloc>}}

{{<bloc>}}

![](/media/tennis-satellite-2020-geoportail.jpg)2020 (Géoportail)

{{</bloc>}}

{{</grid>}}

Maintenant que le tennis est de nouveau propre grâce à l'effort collectif des citoyens, il faut impérativement nous donner les moyens d'éviter une répétition des dommages.