+++
date = 2022-01-04T23:00:00Z
description = "Les inscrits recevront des Babets"
header = "/media/admr-aide_seniors2.jpg"
icon = ""
subtitle = ""
title = "Annulation du repas des ainés"
weight = 1

+++
En raison de la situation sanitaire, nous sommes contraints d'annuler le repas prévu pour les ainés. Les personnes qui s'étaient inscrites recevront prochainement à la place des Babets.

![](/media/babet-2021-2.jpg)