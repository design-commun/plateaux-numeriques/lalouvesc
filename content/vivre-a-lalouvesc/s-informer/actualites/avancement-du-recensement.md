+++
date = 2023-01-25T23:00:00Z
description = "N'oubliez pas de remplir le formulaire en ligne..."
header = "/media/recensement-2023-affiche.png"
icon = ""
subtitle = ""
title = "Avancement du recensement"
weight = 1

+++
Ce jeudi 26 novembre, vous êtes déjà 172 Louvetonnes et Louvetous à vous êtes recensés par internet pour 85 résidences principales.

Si vous vous ne l'avez pas encore fait, ne tardez plus à remplir le formulaire en ligne à partir des instructions que vous avez reçues dans votre boîte aux lettres. C'est le plus simple et ne prend qu'une dizaine de minutes. Sinon, vous serez relancés par Nicole Porte.

Les formulaires papiers seront distribués par Nicole Porte à partir de mardi prochain pour ceux qui ne sont pas connectés.

**Tous les habitants du village doivent être recensés.**