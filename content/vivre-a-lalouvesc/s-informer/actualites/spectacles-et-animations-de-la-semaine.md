+++
date = 2021-07-10T22:00:00Z
description = "En juillet, il se passe tous les jours quelque chose à Lalouvesc"
header = "/media/copie-de-20210703_112053.jpg"
icon = ""
subtitle = "Que faire cette semaine ?"
title = "Spectacles et animations de la semaine du 12 au 18 juillet"
weight = 1

+++
En plus des [nombreuses activités](https://www.lalouvesc.fr/decouvrir-bouger/activites/) possibles, voici les spectacles et manifestations proposés à Lalouvesc du 12 au 18 juillet :

* Tous les jours **Exposition de peintures et sculptures contemporaines** du [Carrefour des Arts](http://www.carrefourdesarts-lalouvesc.com/) au Centre d'Animation Communal tous les jours de 14h30 à 18h30
* Tous les jours **Exposition _La matière habitée_**  sculptures à la chapelle Saint Ignace de 10h à 12h et de 15h à 18h
* 12 juillet 10h30 **Visite du jardin en permaculture** au Mont Besset
* 12 juillet 21h cinéma, **Poly**, [bande annonce](https://youtu.be/_RWTmSpJoLg), à l'Abri du pèlerin
* 13 juillet 21h **Bal et feu d'artifice** au Parc du Val d'Or, buvette ouverte à partir de 19h
* 14 juillet 15h-17h empruntez des livres à la **bibliothèque**
* 15 et 18 juillet 21h cinéma, **The Father** (vf), [bande annonce](https://youtu.be/HVCs_ahGpls) à l'Abri du pèlerin
* 17 juillet : cinéma 21h **Antoinette dans les Cévennes**, [bande annonce](https://youtu.be/qsbBWaCKlW4) à l'Abri du pèlerin
* 18 juillet 9h30-19h **Fête du livre** à la bibliothèque, nombreux auteurs présents