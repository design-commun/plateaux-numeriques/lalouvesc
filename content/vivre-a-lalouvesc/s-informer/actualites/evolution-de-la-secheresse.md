+++
date = 2022-08-23T22:00:00Z
description = "Passage du niveau crise à alerte renforcée"
header = "/media/coupure-d-eau-1.jpg"
icon = ""
subtitle = ""
title = "Evolution de la sécheresse"
weight = 1

+++
La préfecture signale :

Les conditions météorologiques de la mi-août ont permis d’améliorer temporairement les débits dans les cours d’eau en Ardèche, qui sont tous remontés au-dessus du niveau de crise durant plus de 5 jours. Une levée de ce niveau de crise est donc possible.

Les évolutions constatées nécessitent la prise d’un nouvel arrêté préfectoral n° 07-2022-08-23-00001 du 23 août 2022 qui maintient des restrictions d’usage de l’eau assez fortes :  
 - au niveau **ALERTE RENFORCEE pour l’ensemble du département**,  
 - **à l’exception du secteur hydrographique Ardèche**, ainsi que les rivières **Ardèche** (en aval de la confluence avec la Fontaulière) et **Fontaulière** (en aval du barrage de Pont de Veyrières) et la rivière **Eyrieux** en aval du barrage des Collanges (cette dernière pour les usages professionnels agricoles uniquement) soutenus, qui sont placés en **situation d’ALERTE**.  
Le répit donné par les pluies entre le 14 et le 17 août n’enlève rien à l’intensité et la persistance de la sécheresse. Tous les usagers doivent continuer à maintenir une grande rigueur, d’un niveau inédit, et réduire durablement et autant que possible leurs consommations.

[Lire l'arrêté]().

Par ailleurs, un lien vers un [outil informatique](https://www.ardeche.gouv.fr/IMG/ods/2022_outil_ide_ddt07_ap12_localisation_prescriptions_secheresse.ods "outil informatique - ods - 180.1 ko (ouverture nouvelle fenêtre)") (format ods - 180.1 ko - 23/08/2022) permet à l’usager de prendre rapidement connaissance des dispositions de l’arrêté spécifiquement applicables à sa situation, à l’échelle de sa commune. Pour Lalouvesc, il est indiqué les dispositions suivantes :

### « ALERTE RENFORCEE »

* L’arrosage des pelouses, ronds points, espaces verts publics et privés, jardins d’agrément est interdit, sauf pour les arbres et arbustes plantés en pleine terre depuis moins de deux ans, pour lesquels l’arrosage est autorisé trois jours par semaine (lundi, mercredi et vendredi) entre 20 h et 9 h.
* L’arrosage des jardins potagers, hors prélèvement dans des canaux oubéalières, est autorisé trois jours par semaine (mercredi, vendredi et dimanche) et trois heures par jour (entre 19 h et 22 h).
* L’arrosage des espaces sportifs est autorisé deux jours par semaine (lundi et jeudi) et trois heures par jour (entre 19 h et 22 h).
* Le lavage des voitures est interdit hors des stations professionnelles recyclant l’eau et sauf pour les véhicules ayant une obligation réglementaire(véhicules sanitaires, alimentaires ou techniques) et pour les organismes liés à la sécurité.
* Le premier remplissage des piscines est interdit (sauf piscines de volume inférieur à 1 m³).
* Le remplissage complémentaire des piscines n’est autorisé qu’entre 22 h et 6 h
* Le lavage à l'eau des voiries est interdit, sauf impératifs sanitaires et à l’exception des lavages effectués par des balayeuses laveuses automatiques.
* Les fontaines publiques en circuits ouverts doivent rester arrêtées.
* Les tests de capacité des hydrants et points d’eau incendie (PEI) sont interdits
* L’alimentation en eau des plans d’eau, des canaux d’agrément et des béalières et le prélèvement depuis ces ouvrages sont interdits. Une attention particulière sera portée à ces opérations afin de ne pas porter préjudice à la faune piscicole lors de la fermeture de ces canaux.