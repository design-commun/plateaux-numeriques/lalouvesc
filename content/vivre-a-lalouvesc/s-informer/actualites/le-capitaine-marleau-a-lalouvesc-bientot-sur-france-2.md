+++
date = 2021-03-16T23:00:00Z
description = "Vous vous souvenez peut-être des énormes camions garés sur le parking de Lalouvesc cet été. C'était le tournage d'un épisode de la série du célèbre Capitaine Marleau. Eh bien ça y est la série redémarre. L'épisode Ardéchois, intitulé \"Au nom du fils\" sera diffusé le 2 avril sur France 2."
header = "/media/marleau-1.jpg"
icon = ""
subtitle = ""
title = "Le capitaine Marleau à Lalouvesc, bientôt sur France 2"
weight = 1

+++
Vous vous souvenez peut-être des énormes camions garés sur le parking de Lalouvesc cet été. C'était le tournage d'un épisode de la série du célèbre Capitaine Marleau. Eh bien ça y est la série redémarre, mais sur France 2 et non plus sur France 3. L'épisode Ardéchois, intitulé "Au nom du fils" sera diffusé le 2 avril.

La régisseuse du tournage nous avait indiqué ([Bulletin d'août 2020](https://www.lalouvesc.fr/media/bulletin-d-information-n-2-aout-2020-lalouvesc.pdf)) la raison du choix de Lalouvesc :

_Dans notre cas précis le héros étant un prêtre, nous avions plusieurs églises en vue et, de loin, la basilique de Lalouvesc correspondait à ce que nous cherchions.  
Nous avons pris rendez-vous avec le père Iratzoquy avec lequel il s’est créé un lien d’affinité rapide, car il fut auparavant le professeur de notre décorateur._

Le tournage a eu lieu aussi à Lamastre, à Boucieu le Roi et à St Victor.

![](/media/marleau-2.jpg)

### Résumé de l'histoire

Le conducteur d’un train touristique (le train de l'Ardèche) est retrouvé mort, il s’agit de Robert Lanski. Il y a 5 ans déjà il était responsable d’un accident ferroviaire qui avait coûté la mort d’un adolescent. Une vengeance a-t-elle été organisée ?

Capitaine Marleau soupçonne un prêtre. Mais, rassurez-vous, le père Iratzoquy n'est pas concerné !

Le père Damien, par contre, ne dit pas tout. Il avait de bonnes raisons d’en vouloir à Lanski. L’a-t-il tué ou couvre-t-il le vrai assassin ?

### Casting

Corinne Masiero est toujours le capitaine Marleau. A ses côtés de nombreux acteurs connus : Olivier Gourmet (le père Damien), Anouk Grinberg, Grégoire Colin, Jeanne Cherhal, Arthur Mazet, Fatou N’Diaye et l’humoriste Yves Lecoq dans le rôle d'un évêque.

Réalisation : Josée Dayan.

Pour les très impatients, il est possible de visionner dès à présent l'épisode... [mais seulement depuis la Suisse](https://www.rts.ch/play/tv/capitaine-marleau/video/au-nom-du-fils?urn=urn:rts:video:11826121).