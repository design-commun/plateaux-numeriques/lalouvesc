+++
date = 2022-10-13T22:00:00Z
description = "A Lalouvesc et ailleurs..."
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Attention le COVID revient..."
weight = 1

+++
Nous n'en avons malheureusement pas fini avec le virus. Les contaminations remontent un peu partout en France, en Ardèche aussi comme le montre la courbe des hospitalisations pour cause de Covid 19. Il y en avait 40 au 13 octobre 2022.

![](/media/covid-stats-oct-2022.jpg)

Le village n'est pas épargné. Soyez prudents et vigilants, en particulier si vous côtoyez des personnes fragiles. Les gestes barrières sont toujours importants :

* Maintenez une distance de sécurité avec tout le monde (1 mètre au moins), y compris les personnes qui ne semblent pas malades.
* Portez un masque dans les espaces publics, notamment en intérieur ou lorsque la distanciation physique n'est pas possible.
* Préférez les zones ouvertes et bien ventilées aux espaces fermés. Ouvrez une fenêtre si vous êtes en intérieur.
* Lavez-vous fréquemment les mains. Utilisez du savon et de l'eau ou une solution hydroalcoolique.
* Faites-vous vacciner dès que vous en avez la possibilité. Suivez les recommandations locales concernant la vaccination.
* En cas de toux ou d'éternuement, couvrez-vous le nez et la bouche avec le pli du coude ou avec un mouchoir.
* Restez chez vous si vous ne vous sentez pas bien.