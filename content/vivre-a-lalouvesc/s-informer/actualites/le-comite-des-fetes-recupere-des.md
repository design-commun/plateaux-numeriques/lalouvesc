+++
date = 2023-03-02T23:00:00Z
description = "réveils, cafetières, horloges, bougeoirs, tasses, chapeaux, théières hors d'usage"
header = "/media/ardechoise-2.jpg"
icon = ""
subtitle = "réveils, cafetières, horloges, bougeoirs, tasses, chapeaux, théières hors d'usage"
title = "Le Comité des fêtes récupère des..."
weight = 1

+++
En prévision des décorations pour l'Ardéchoise sur le thème de _Alice au pays des merveilles_.

**![](/media/recup_ardechoise-cdf-mars-2023.jpg)**