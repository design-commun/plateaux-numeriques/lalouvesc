+++
date = 2022-05-31T22:00:00Z
description = "Juin 2022 - Salut les artistes !"
header = "/media/entete-bulletin-ete-2.jpg"
icon = ""
subtitle = "Juin 2022 - Salut les artistes !"
title = "Le Bulletin mensuel d'information de Lalouvesc n°23 est paru"
weight = 1

+++
_Le village prend ses atours d'été grâce aux artistes : les artistes louvetous qui inventent et trouvent des outils, des formes et des matériaux originaux pour décorer le village, aussi, bien sûr, les artistes invités pour des événements d'envergure, expositions et musique au Carrefour des Arts, concerts du Comité des fêtes qui nous enchantent tout au long de la saison et encore des artistes, nouveaux venus ou nouveaux talents sur le village, méconnus des Louvetous à découvrir en conclusion de ce Bulletin. Un numéro de début de saison particulièrement illustré et fourni donc, avec aussi, comme toujours, bien d'autres informations encore._

### [**Lire le Bulletin**](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/)

## Entrée directe par le sommaire

* [Mot du maire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#mot-du-maire)
* [Actualités de la mairie](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#actualités-de-la-mairie)
  * [Fin de la première phase du chantier Beauséjour et suite](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#fin-de-la-première-phase-du-chantier-beauséjour-et-suite)
  * [Étude sur le Cénacle et la Maison Ste Monique](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#étude-sur-le-cénacle-et-la-maison-ste-monique)
  * [Aire du Grand Lieu](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#aire-du-grand-lieu)
  * [Rue de la Fontaine, une rue partagée](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#rue-de-la-fontaine-une-rue-partagée)
  * [Un village bien fleuri !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#un-village-bien-fleuri-)
  * [Nouvelle organisation pour les poubelles](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#nouvelle-organisation-pour-les-poubelles)
* [Zoom : l’Ardéchoise et le Carrefour des Arts](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#zoom--lardéchoise-et-le-carrefour-des-arts)
  * [L’Ardéchoise, bicycle et recycle](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#lardéchoise-bicycle-et-recycle)
  * [Carrefour des Arts : demandez le programme !](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#carrefour-des-arts--demandez-le-programme-)
* [Suivi : Culture - Loisirs](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#suivi--culture---loisirs)
  * [Les concerts du Comité des fêtes](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#les-concerts-du-comité-des-fêtes)
* [Suivi : Sanctuaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#suivi--sanctuaire)
  * [Colloque de l’association des Villes Sanctuaires en France](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#colloque-de-lassociation-des-villes-sanctuaires-en-france)
  * [Exposition et concert cet été](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#exposition-et-concert-cet-été)
  * [Rencontre avec les responsables du sanctuaire](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#rencontre-avec-les-responsables-du-sanctuaire)
* [Suivi : Santé - Sécurité](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#suivi--santé---sécurité)
  * [Emploi ADMR](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#emploi-admr)
  * [Suite du calendrier Santé-Environnement](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#suite-du-calendrier-santé-environnement)
  * [Conseils pendant les fortes chaleurs](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#conseils-pendant-les-fortes-chaleurs)
  * [Conseils pour la rénovation des bâtiments](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#conseils-pour-la-rénovation-des-bâtiments)
* [Dans l’actualité, le mois dernier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#dans-lactualité-le-mois-dernier)
* [Découvrez deux auteurs louvetous](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#découvrez-deux-auteurs-louvetous)
  * [Thierry Rouby](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#thierry-rouby)
  * [Félix Brassier](https://www.lalouvesc.fr/vivre-a-lalouvesc/s-informer/bulletins-information/bulletins-2022/23-salut-les-artistes/#félix-brassier)