+++
date = 2021-01-05T23:00:00Z
description = "Voici les informations qui viennent de nous arriver de la préfecture."
header = "/media/vaccin-covid-19.jpg"
icon = ""
subtitle = ""
title = "Vaccination COVID 19 pour les plus de 75 ans"
weight = 1

+++
Voici les informations qui viennent de nous arriver de la préfecture.

La phase de vaccinations pour les personnes âgées de plus de 75 ans vivant à domicile ou en résidence autonomie/senior et pour les personnes handicapées vulnérables prises en charge dans des foyers médicalisés débutera le 18 janvier en Ardèche.

Les centres de vaccination de l’Ardèche sont précisés sur le tableau.

![](/media/centres-de-vaccination-ardeche.jpg)

Les centres les plus proches de Lalouvesc sont donc : celui d’Annonay (prise de rendez-vous sur Doctotlib ou au tél 07 88 58 21 86) et celui de Lamastre (prise de rendez-vous sur (Maiia ou 04 75 06 30 08). Si vous êtes concernés, nous vous invitons à faire les démarches… en vous armant de patience, les sites web et les lignes téléphoniques risquent d’être surchargés. Si vous connaissez des personnes concernées et qui ont des difficultés pour les prises de rendez-vous, n’hésitez pas à les aider. La solidarité est essentielle en ces périodes incertaines.

L’équipe municipale réfléchit aux solutions à mettre en place pour les personnes les plus en difficultés. Nous ferons le point dans le courant de la semaine prochaine.