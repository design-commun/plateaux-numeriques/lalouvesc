+++
date = 2021-03-21T23:00:00Z
description = "Le conseil est exclusivement réservé aux discussions budgétaires. En raison de la situation sanitaire, il se tiendra à huis clos."
header = "/media/mairie-lalouvesc-2.jpg"
icon = ""
subtitle = ""
title = "Conseil municipal le 24 mars à 18h"
weight = 1

+++
Le conseil du 24 mars à18h est exclusivement réservé aux discussions budgétaires. En raison de la situation sanitaire, il se tiendra à huis clos en Mairie.

### Ordre du jour

1. COMMISSION FINANCES

a- Validation du compte de gestion et du compte administratif

b- Affectation des résultats.

c- Vote budget primitif.

d- Emprunt Caisse d ‘épargne

e- Prêt in fine Banque postale

f- Dissolution budget eau et assainissement, transfert au budget principal

g- Vote aide aux associations

h- Vote tarification droit de place de marché et terrasses

i- Modification tarif camping

2. COMMISSION GESTION

a- Rémunération stagiaire

b- Vente terrain