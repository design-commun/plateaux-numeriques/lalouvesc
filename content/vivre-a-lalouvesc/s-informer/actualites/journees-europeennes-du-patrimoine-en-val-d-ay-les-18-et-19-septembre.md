+++
date = 2021-09-08T22:00:00Z
description = "Demandez le programme !"
header = "/media/journees-patrimoine-lalouvesc-2021-2.jpg"
icon = ""
subtitle = ""
title = "Journées européennes du patrimoine en Val d'Ay : les 18 et 19 septembre"
weight = 1

+++
## Lalouvesc

**Journées européennes du patrimoine : Visite Paroles d’habitants**  
Lalouvesc Office de tourisme  
04 75 67 84 20 contact@valday–[ardeche.com](http://ardeche.com) [http://www.valday](http://www.valday "http://www.valday")–[ardeche.com](http://ardeche.com)  
Lalouvesc est un village sanctuaire, où pèlerins et artistes ont laissé leurs empreintes à travers les siècles : des œuvres à découvrir, une poésie, une chanson, un témoignage…  
Samedi 18 septembre à 10h. Gratuit.

## Satillieu

**Journées du patrimoine : visite, expositions, animations**  
Satillieu  
Château et Point information touristique  
04 75 67 84 20 contact@valday–[ardeche.com](http://ardeche.com) [http://www.valday](http://www.valday "http://www.valday")–[ardeche.com](http://ardeche.com)  
Venez visiter le château, ses pièces maîtresses, toiles et tapisseries classées, l’exposition d’artistes locaux, de photos anciennes, d’archives sur les usines TSR. Animations sur le thème du trésor, parcours énigmes, jeu de piste, déambulation fanfare les Bleuets… Gratuit.

![](/media/journees-patrimoine-satillieu-2021.jpg)

## Saint Romain d’Ay

**Journées européennes du patrimoine : visite Moulin des Claux**  
Moulin des claux Saint–Romain–d’Ay  
Visite guidée du Moulin toutes les heures sur réservation.  
06 07 83 67 83 [grimonpont.fv@gmail.com](mailto:grimonpont.fv@gmail.com)  
Samedi 18 septembre de 14h à 18h. Dimanche 19/09 de 10h à 17h. Gratuit.

**Journées européennes du patrimoine : visite sanctuaire et château**  
ND d’Ay Saint–Romain–d’Ay  
Les Amis de ND d’Ay organisent l’ouverture au public du château . Visites guidées prévues le samedi et le dimanche à 14h30 gratuites.  
Chapelle ouverte.  
De 10h a 19h les samedi et dimanche 18 et 19 septembre. Gratuit.