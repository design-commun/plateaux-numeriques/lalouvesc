+++
date = 2022-03-20T23:00:00Z
description = "Vous êtes tous concernés, tous invités, Faites-le savoir !"
header = "/media/journee-citoyenne-jour-3-2.jpg"
icon = ""
subtitle = ""
title = "Réunion Journées citoyennes 2022, mercredi 30 mars à 20h"
weight = 1

+++
Le printemps approche. Il est temps d'organiser les journées citoyennes 2022 pour préparer le village à l'ouverture de la saison estivale.

En 2020, nous avons inauguré la tradition par la réfection du jeu de boules sur le parc du Val d'Or. L'année dernière, les citoyens ont remis en état le camping. Les[ résultats furent spectaculaires](https://www.lalouvesc.fr/projets-avenir/gerer-le-bien-commun/des-journees-citoyennes-pour-le-camping/) et très bénéfiques pour la Commune. Le Comité de développement a suggéré de consacrer les journées citoyennes de cette année à la remise en état des différents parcours (Lapins, Champignons, Botanique...). Si nous sommes suffisamment nombreux d'autres tâches utiles pour rendre le village accueillant pourront être ajoutées. Chacun participera à son rythme et selon ses moyens.

**Pour organiser ces journées, vous êtes toutes et tous conviés à une première réunion mercredi 30 mars à 20h au CAC.**

![](/media/journee-citoyenne-lalouvesc-11-juillet-2020.jpg "2020")

![](/media/journee-citoyenne-1-2021-26.jpg "2021")