+++
date = 2021-09-07T22:00:00Z
description = "Quelques chiffres..."
header = "/media/vaccination-contre-la-covid-19-en-france-le-monde-8-sept-2021vaccination-contre-la-covid-19-en-france-le-monde-8-sept-2021-2.png"
icon = ""
subtitle = ""
title = "La vaccination en France, en Ardèche et à Lalouvesc"
weight = 1

+++
## La vaccination contre la COVID 19 en France...

![](/media/vaccination-contre-la-covid-19-en-france-le-monde-8-sept-2021vaccination-contre-la-covid-19-en-france-le-monde-8-sept-2021-2.png)

Source : _Le Monde_

## ... en Ardèche...

66% des personnes sont complètement vaccinées et 71,5% ont reçu au moins une dose en Ardèche au 8 sept, un peu moins bien que la France entière (68% et 73,1%).

Cela peut se lire aussi sur un graphique pour les personnes complètement vaccinées :

![](/media/vaccination-contre-la-covid-19-ardeche-sante-publique-france-8-sept-2021.png)

Source : _Santé publique France_

## ... et à Lalouvesc

Nous ne disposons pas de chiffres récents et précis sur le niveau de vaccination pour la Commune, mais le ministère des Solidarités et de la Santé, a fait parvenir à l'AMF des données relatives au nombre de personnes non-vaccinées de plus de 80 ans par Communes au 31 mars dernier.

Lalouvesc ne figurait pas dans les meilleurs élèves, loin de là ! Avec un taux de 74,1% de personnes de 80 ans et plus vaccinées, nous figurions à la 23.305ème place sur 25.563 communes recensées.

**Faut-il rappeler que le village a subi lourdement les effets des deuxième et troisième vagues de la pandémie ?** Plus de 80% des personnes hospitalisées aujourd'hui pour la Covid 19 sont des non-vaccinées. Il est essentiel pour la sécurité de tous dans le village que chacun prenne ses responsabilités et, s'il est éligible et ne l'a encore fait, prenne un rendez-vous pour un vaccin.

Il reste désormais en France moins de 10 millions de personnes de 12 ans et plus non vaccinées (dont 500 000 personnes de plus de 80 ans n’ayant reçu aucune injection).