+++
date = ""
description = "Une appli pour se tenir au courant"
header = ""
icon = "📲"
subtitle = "Une appli pour se tenir au courant"
title = "Panneau Pocket"
weight = 1
[menu.direct_access]
weight = 3

+++
Les dernières informations services du village sur une appli. Téléchargez _Panneau Pocket_ sur votre téléphone ou consultez-là ci-dessous.

<iframe src="https://app.panneaupocket.com/embeded/77003417" height="518" width="330" frameborder="0"></iframe>