+++
date = ""
description = ""
header = "/media/pelerinage-des-hommes-vers-1910.jpg"
icon = "👪"
subtitle = "Le défi d'un village rural"
title = "Démographie"
weight = 4

+++
### Population

![https://lh3.googleusercontent.com/SVBpp2ORZ4fm8VS4nnoUzgAlMYREV8YjI0M6b074aRpVOhOAzEJP9xYFiRi5fKDsQ6uKVZ](https://lh3.googleusercontent.com/SVBpp2ORZ4fm8VS4nnoUzgAlMYREV8YjI0M6b074aRpVOhOAzEJP9xYFiRi5fKDsQ6uKVZ_93wraeBtBxtEbQjKYOhy-S1DRADGojrmuiXk2Y6_RkfPTbvOGUKOzuALpMlXhQ-Z- "Source Wikipédia (Zimpala) cc")La courbe ci-dessus montre de façon spectaculaire l’attirance du village, découlant principalement du pèlerinage, puis son déclin progressif. Le graphique ci-dessous montre que l’évolution actuelle de la population dépend essentiellement de son vieillissement.

Le renouveau actuel dépend donc principalement d’une seule variable : l’attirance du village pour des jeunes ménages. C’est le défi qu’il faut relever.

Néanmoins, les chiffres de la population doivent être nuancés par l’activité principale traditionnelle du village : le tourisme. Ainsi en 2017, le parc des résidences secondaires représentait 43,7% de l’ensemble des résidences du village et, si l’on ajoute les capacités d’hébergement du camping, des hôtels et des bâtiments religieux, la population estivale du village peut facilement tripler.

![](/media/evolution-population-inse.jpg)  
Pour en savoir plus : [Dossier Insee sur Lalouvesc](https://www.insee.fr/fr/statistiques/2011101?geo=COM-07128#graphique-POP_G2), [Comparateur de territoire (Insee)](https://www.insee.fr/fr/statistiques/1405599?geo=COM-07128), [Chapitre 5 du PLU](https://drive.google.com/file/d/1P1jeY42vxcUFR4o1CsAm9K7LlMucHdqt/view?usp=sharing). [Lalouvesc sur Wikipédia](https://fr.wikipedia.org/wiki/Lalouvesc).