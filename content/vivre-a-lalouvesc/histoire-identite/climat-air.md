+++
date = ""
description = ""
draft = true
header = ""
icon = ""
subtitle = ""
title = "Climat, air"
weight = 1

+++
<script id="widget-recosante" src="https://recosante.beta.gouv.fr/iframe.js" data-search="?theme=default"></script>

### Lalouvesc un air pur... sauf pour l'ozone

Le docteur Eugène Vincent écrivait en 1911 dans son opuscule intitulé [_La Louvesc, station de cure d'air et d'altitude _](http://www.pierre-armand-roger.fr/lalouvesc/vincent_ll/pages/picture_000.html):

> A La Louvesc la population n’est pas nombreuse, c’est un village, il n’y a pas d’usines. Les immenses forêts de sapins contribuent à la purification de l’air par l’absorption de l’acide carbonique et par les flots d’émanations balsamiques, qu’elles projettent dans l’atmosphère, surtout en été. L’absence de poussières et de fumées, l’absence de germes morbides, l’absence de microbes, sont encore assurées par les longs hivers, par l’abondance des neiges, qui entraineraient si il y en avait, les impuretés de l’air, et les fixeraient au sol, où s’opère un travail très actif d’assimilation et d’épuration chimique.

Le docteur était-il trop enthousiaste et optimiste ? En plus de cent ans la qualité de l'air a évolué et, surtout, les mesures sur les conséquences de sa qualité sur la santé sont plus précises.

La [première étude régionale effectuée en France](https://www.santepubliquefrance.fr/determinants-de-sante/pollution-et-sante/air/documents/enquetes-etudes/evaluation-quantitative-d-impact-sur-la-sante-eqis-de-la-pollution-de-l-air-ambiant-en-region-auvergne-rhone-alpes-2016-2018), à partir de modélisation et de données d’exposition aux polluants de l’air sur 2016-2018 en Auvergne-Rhone-Alpes est, en effet, éclairante. Elle a été réalisée par Santé Publique France. Elle ne concerne pas les gaz à effets de serre (pollution anthropique), mais la pollution de l'air ayant un effet direct sur la santé humaine et plus particulièrement les pollutions au dioxyde d'azote (NO2), aux particules fines (PM2,5) et à l'ozone.

L'étude montre qu'aujourd'hui à Lalouvesc, l'air est pur, plus pur qu'en de nombreux endroits de la région... avec pourtant quelques nuances à surveiller.  Vous trouverez ci-dessous des cartes que nous avons centrées sur l'Ardèche. L'étoile rouge marque la situation de Lalouvesc. Pour les cartes complètes de la région, on peut se référer à l'étude en ligne.

{{<grid>}}{{<bloc>}}

![](/media/pollution-2021-azote.jpg)![](/media/pollution-2021-legende-azote.jpg)

{{</bloc>}}{{<bloc>}}

![](/media/pollution-2021-particules.jpg)![](/media/pollution-2021-legende-particules.jpg)

{{</bloc>}}{{</grid>}}

La pollution au dioxyde d'azote (NO2) est principalement due au trafic routier tandis que la pollution aux particules fines provient majoritairement du chauffage individuel au bois. L'étude montre que 7 % de la mortalité de la région est attribuable à l'exposition à long terme aux particules fines soit environ 4 300 décès par an. Plus de 4 % des cancers du poumon, 6 % des accidents vasculaires cérébraux et 5 % des recours aux urgences pour asthme chez les enfants sont attribuables à l'exposition chronique aux particules fines.

Mais, comme on peut le voir sur les cartes, Lalouvesc se trouve dans une zone peu touchée par ces deux pollutions qui concernent principalement les grandes agglomérations, les fonds de vallée et les alentours de la vallée du Rhône.

#### Une pollution à l'ozone à surveiller

En ce qui concerne l’ozone présent dans les couches basses de l'atmosphère (à ne pas confondre avec la "couche d'ozone" qui nous protège des UV), il s’agit d’un polluant secondaire, découlant des autres polluants. L’ensoleillement joue un rôle déterminant dans sa formation. C’est le seul polluant dont les concentrations mesurées ne diminuent pas ces dernières années, vraisemblablement à cause du réchauffement climatique.

Comme on peut le voir sur la  carte ci-dessous, la situation de Lalouvesc est beaucoup moins favorable pour cette pollution, comme d'ailleurs celle de toutes les zones d'altitude.

![](/media/pollution-2921-ozone.jpg)![](/media/pollution-2021-legende-ozone.jpg)

L'ozone serait responsable chaque année, chez les personnes âgées de 65 ans et plus, de 0,6 % des hospitalisations pour causes respiratoires et de l'ordre de 1 % des hospitalisations pour causes cardio-vasculaires dans la région. Il faut se rappeler également que la pollution de l’air impacte l’homme, mais aussi le vivant et la biodiversité.

Nous subissons pour une bonne part le contrecoup de la pollution des agglomérations et de la vallée du Rhône qui se transforme en ozone sous l'effet du soleil. Mais cela ne nous empêche pas de réduire nos propres sources de pollution. Il faudra y réfléchir.