+++
date = ""
description = "... de la préhistoire à nos jours"
header = "/media/histoire-lalouvesc-curley.gif"
icon = "🔖"
subtitle = "... de la préhistoire à nos jours"
title = "Histoire du village"
weight = 2

+++
Vous trouvez ci-dessous un résumé de l'histoire du village. Vous pouvez aussi le consulter :

* sous forme de [ebook](https://www.flipsnack.com/Lalouvesc/histoire-de-lalouvesc-de-la-pr-histoire-nos-jours.html)
* sous forme de[ frise chronologique](https://cdn.knightlab.com/libs/timeline3/latest/embed/index.html?source=1mDDQVtwzQWTFBde7m10PI92NDikzuKh-Q1pZtSstsl8&font=Default&lang=fr&initial_zoom=2&height=650)

## Préambule

Quand on recherche des informations historiques sur le village, il faut se rappeler que son nom a pris différentes formes au cours des siècles : Alaudisco (1179), Alauzeto (1256), Lalalvesc (1275), Alauvesco (1336), Alavesc (1464), Olovescum (XVI siècle), La Louvee (1790), La Louvesc (XIX siècle), Lalouvesc (fin du XX siècle).

La source majeure d’information sur l’histoire du village est l’ouvrage du [Père de Curley : _Le tombeau de St. Régis à Lalouvesc_](https://gallica.bnf.fr/ark:/12148/bpt6k5478293q/f23.image.r=plas%20regis), publié en 1886 par la librairie générale catholique et classique à Lyon et accessible sur Gallica.fr. Cette histoire a été aussi enrichie par le _Bulletin de l'Alauda, revue sur l'histoire de Lalouvesc_ (un numéro par an). On trouvera enfin de nombreuses informations historiques sur [le site de Pierre Roger](http://www.pierre-armand-roger.fr/lalouvesc/index.htm), auteur principal des textes de ce document.

Toutes les références sont rappelées plus bas.

## Néolithique - Premières traces d'une présence humaine

![](/media/lac-grand-lieu-vers-1910.png)_Lac du Grand Lieu vers 1910_

Des haches de pierre polie ont été trouvées près du lac (aujourd’hui comblé pour aménager la place du même nom), dit lac du Grand Lieu. Cela indique une présence humaine sur le site de Lalouvesc au Néolithique (vers 8000 av. J.-C.).

## -750 - Oppidum fortifié

![](/media/oppidum-du-chirat-blanc.jpg)

_(A.Boudon-Lashermes, Archives départementales de l’Ardèche)_

_Plan de l’oppidum du Chirat Blanc au XIXe siècle_

Au Chirat Blanc, une dizaine de km au nord de Lalouvesc, on trouve une enceinte ovale d’environ 250 mètres de long, construite en partie par l’homme. À l’intérieur de l’enceinte, on reconnaît des fonds de cabanes carrés ou ronds de 3 à 4 mètres de diamètre.

Cet oppidum fortifié a été occupé entre le Premier Âge du Fer (725-480 avant J.C.) et le Second (130-30 avant notre ère). C’était un centre d’habitation et d’échanges commerciaux sécurisé grâce à la topographie : un sommet rocheux de plusieurs hectares bordé à au sud et à l’ouest par des falaises rocheuses (au-dessus du village de Saint Symphorien et du haut de la vallée du Nant).

Informations sur le site des Amis de Veyrines.

[https://lesamisdeveyrines.org/2018/06/30/le-site-prehistorique](https://lesamisdeveyrines.org/2018/06/30/le-site-prehistorique "https://lesamisdeveyrines.org/2018/06/30/le-site-prehistorique")

## -50 - Legio Alaudae

![](/media/monnaie-legion-alaudae.png)

_(Wikimedia)_

_Monnaie de l’empereur Claude montrant les insignes des légions V Alaudae (noter l’alouette) et VIII Augusta_

Comme l’indique le Père de Curley, _L’histoire primitive de Lalouvesc est obscure comme les bois qui entourent le village … Pendant longtemps elle ne se distingue pas de l’histoire générale des Helviens_. Les Helviens (en latin _Helvii_) sont un peuple gaulois situé dans le sud du Vivarais qui apparait dans l’Histoire lors de la conquête romaine de la future Gaule Narbonnaise.

50 av. J.C., après la conquête de la Gaule, Jules César créa la _Quinta alaudae_, aussi connue comme _Legio Gallica_, une légion composée uniquement de Gaulois qui furent faits citoyens romains. Alauda ou Alouda est le nom latin de l’alouette et dérive du celtique (_al_ : qui s'élève ; _chweld_ : chant => _alwede_: qui s'élève en chantant). Le surnom de la légion, Alaudae, ferait référence à la coutume de certains Gaulois de porter des ailes d’alouettes sur leur casque et souligne l’origine des soldats.

![](/media/embleme-legion-alaudae.png)

Son emblème était l’éléphant, symbole inhabituel pour une légion levée par Jules César qui lui fut conféré en 46 av. J.-C. pour souligner la bravoure dont elle avait fait preuve à la bataille de Thapsus (aujourd’hui Ras Dimass, en Tunisie) où elle eut à combattre contre les éléphants des ennemis de César.

Un _alauda_ était un soldat de ce régiment d’élite. Une hypothèse est que l’un d’eux, devenu vétéran, reçut une concession de terre qui porta son nom : _Alaudacum_ ou _Alaudiscum_ (villa d’Alauda) devenue La Louvesc. Une autre hypothèse est que la tour de l’ancien poste romain de la légion _Quinta alaudae_ qui gardait le passage de La Louvesc soit à l’origine du nom du village.

Mais Alaudiscum, d’après du Cange (auteur d’un dictionnaire de latin médiéval), pourrait aussi dériver de _alaudis_, qui désigne un fond de « terre libre de toute imposition » ou _franc alleu_.

Les Romains, traçant la route de Lyon à Marseille, ouvrirent à Vienne un embranchement vers Le Puy qui allait d’Annonay au Tracol du Puy. Dans un article de M-C Chavarot (1986), on trouve également une référence à un chemin romain allant de Serrières au Velay en passant par Satillieu et Lalouvesc. On a retrouvé dans le quartier de La Vialette des pavés témoignant de l’existence d’une ancienne voie romaine. Le nom de La Vialette viendrait d’ailleurs de _Via Lata_, nom de la voie romaine qui allait à St. Agrève.

## 1200-1312 - La Maison de La Louvesc

![](/media/maison-claire.png)

Durant le Moyen-Âge, Lalouvesc dépendait de la seigneurie puis de la baronnie de Mahun qui englobait les villages de St. Symphorien, Veyrines, St. Pierre des Machabées (devenu St. Pierre sur Doux), La Louvesc, Seray, Satillieu, Préaux, Vaudevant, Saint Jeure d’Ayet ainsi que la partie de la vallée de la Vocance située entre St Bonnet-le-Froid et Villevocance.

1179 : Le premier document authentique où il est question de La Louvesc date du 1er avril 1179. Il s’agit d’une bulle pontificale énumérant les biens de l’abbaye de Saint-Chaffre, située au Monastier-sur-Gazeille en Haute Loire. Cette puissante abbaye a possédé au Moyen Âge jusqu’à une soixantaine de dépendances en Vivarais, parmi lesquelles figure l’église d’Alaudiscum entre celle de Vérines (Veyrines) et de Saint-Pierre-des Macchabées (Saint-Pierre-sur-Doux). La Louvesc est donc la paroisse d’Alaudiscum. A noter qu’à cette époque, Lalouvesc est une paroisse et non une simple chapelle. L’église se trouvait au niveau du chœur actuel de la crypte de la Basilique.

Le Père de Curley, dans son ouvrage, indique qu’il a retrouvé huit membres d’une famille de gentilshommes portant le nom de « de La Louvesc » entre 1200 et 1400. Ils sont qualifiés de seigneurs dès l’an 1294. Leur maison était une maison forte avec tour et chapelle. La tour a été démolie et la chapelle a disparu. Cette maison était appelée la Maison Claire (peut-être primitivement la maison des clercs). Il semble que ce puisse être la Maison-Claire actuelle, (Caillet & Mathieu, bul. de l’Alauda n°3).

Les trois premiers de la lignée s’appelaient tous Siméon. Siméon I n’avait pas de profession connue, Siméon II était juge des terres de Guignes Pagan et Siméon III était curé de Vocance. Viendra ensuite Jean qui était juge des terres et intendant de la Maison de Guillaume de Roussillon, seigneur d’Annonay. Avec son fils Pierre, il accompagna en 1355 le Cardinal Bertrand au sacre de l’empereur germanique Charles IV.

Pierre de La Louvesc sera armé Chevalier du Saint-Empire. Le texte de son serment a été conservé : _Je, noble Pierre de La Louvesc, chevalier bachelier de la Couronne de France et chevalier du Saint Empire, promet par la foy de mon corps, comme loyal chevalier, de défendre et de protéger la Sainte Église romaine et le Saint Empire contre tous leurs ennemis, voulant être tenu pour faux, mauvais et déloyal chevalier, parjure et foy menteur, si je fais le contraire. Ainsi Dieu m’aide en ces saints Évangiles, que je touche_.

La Maison de La Louvesc s’éteignit vers 1400 alors que disparaissait Guigues V de Pagan, le dernier des Pagan. Lalouvesc eut ensuite pour seigneurs les « de Beauchastel », puis les « de Tournon » vers 1600.

1312 : Une maison à Lalouvesc appelée « l’hôpital » et le mas de la Vialette sont donnés par Hugo de Paganis aux Chevaliers de Saint-Jean de Jérusalem de la commanderie de Sainte-Épine-lès-Tournon, située sur la commune de St. Jean de Muzols. L’ordre de Saint-Jean de Jérusalem, appelé aussi ordre des Hospitaliers, est un ordre religieux catholique, hospitalier et militaire qui a existé de l’époque des croisades jusqu’au début du XIX siècle.

## 1500-1640 - Le Col de La Louvesc, un pays dépeuplé

![](/media/possession-des-nobles-vivarais-14e.png)

_(Marie-Claire Chavarot)_

_Possessions des nobles au XIVe siècle_

Au XIV siècle, il est probable que Lalouvesc n’est qu’un ensemble de quelques maisons. Sur la carte présentée dans _Mentalités nobiliaires en Vivarais au XIVe siècle : la succession de Briand de Retourtour_ par Marie-Claire Chavarot on s’aperçoit que La Louvesc ne figure pas en tant que village mais uniquement en tant que « Col de Lalouvesc ».

1603 : Sous le gouvernement des de Tournon, les jésuites du Puy prirent possession des anciens prieurés à la place des Bénédictins. Dès 1603, les Pères jésuites étaient seigneurs de Veyrines et patrons des églises de Saint-Pierre, de Lalouvesc et de Veyrines (Poidebard R. 1928. L’église de Veyrines : XIIe siècle. Aubenas : C. Habauzit. 64 p).

1621 : Guerres de religion. L’église de Lalouvesc est pillée par les troupes protestantes (Mathieu, Alauda n°2).

1640 : Un pays dépeuplé. Laissons la parole au Père de Curley. _En l’année 1640, Lalouvesc dépend de Tournon. La maison forte des de La Louvesc appartient à noble Gilles de la Franchière, seigneur de Girodon. On compte comme principaux propriétaires, noble Guillaume de Julliens, seigneur de Rocheume et Jean Buisson de la Grange Neuve, dont la famille subsistait déjà à Lalouvesc depuis plusieurs siècles. La commanderie de Saint Epine-les-Tournon possédait encore la maison appelée la maison de l’hôpital et des propriétés. Cinquante-quatre autres familles se partageaient le territoire. Une trentaine au moins n’habitaient pas le pays. Le plus grand nombre étaient dispersés dans les hameaux dépendants du village. Quant au village proprement dit, les témoins qui déposèrent dans les enquêtes ecclésiastiques sont unanimes à lui donner trois ou quatre maisons outre l’église. C’était une véritable décadence. Dans les temps anciens La Louvesc avait été paroisse mais dès l’an 1602 était devenue simple annexe de Verrines qui à son tour, comme n’ayant plus aucune importance, avait été annexé au prieuré de Macheville (Lamastre). Le pays se dépeuplait._

## 31 décembre 1640-5 avril 1737 - Mort et canonisation du Révérend Père Régis

![](/media/regis2.gif)

La Louvesc ne s’est véritablement développé qu’à partir du 31 décembre 1640 quand François Régis, apôtre du Vivarais et du Velay (1597–1640), vint mourir dans le hameau qui ne comptait alors qu’une chapelle et « quatre feux ». Il était envoyé par les Pères jésuites pour évangéliser les populations vivaroises. Il partit du Puy à pied, le 22 décembre et alla coucher à Raucoules, village située à l’est du Velay, dans le département de la Haute-Loire. Le 23 décembre, il repartit et s’égara à la nuit tombante aux environs de La Louvesc. Il atteignit Vérines à bout de force et passa la nuit dans une masure. Il arriva le 24 décembre à La Louvesc, fiévreux, et mourut le 31 décembre. Le lendemain de sa mort, il fut proclamé saint dans tous les pays qu’il avait évangélisés. _Des miracles, des guérisons, s’opérèrent alors à La Louvesc et dans tout le Vivarais_.

Jean-François Régis était né Fontcouverte en Languedoc en 1597 (d’où le jumelage de Lalouvesc avec ce village). Après des études au Collège de Béziers, il était entré dans la Compagnie de Jésus à l’âge de 19 ans. Son nom de famille est devenu grâce à lui un prénom.

1715 : Louis XIV intervient à Rome pour la béatification du Père François Régis. Un décret de béatification est rendu par le pape Clément IX en 1716 suite à cette intervention.

1721 : la ville d’Annonay fut miraculeusement préservée de la peste, les rois de France (Louis XV) et d’Espagne rendirent un hommage public à François Régis et sollicitèrent la canonisation.

1737 : Louis XV obtient la canonisation de Saint Régis qui fut promulguée le 5 avril 1737 en même temps que Saint Vincent de Paul. Deux grandes fêtes eurent lieu en juin 1738, durant toute une semaine avec plus de 8.000 pèlerins.

## 1653-1804 - Lalouvesc devient un village

![](/media/lalouvesc-gravure-ancienne.png)

_Village de Lalouvesc (gravure ancienne)_

_Lalouvesc devient un lieu de pèlerinage important. Dès 1653, douze ecclésiastiques ne suffisent pas pour entendre les confessions lors des fêtes principales. Cet afflux des foules est, pour La Broüe_ \[premier biographe de St Régis\]_, d’autant plus merveilleux que le lieu où il repose n’est qu’un petit village de trois ou quatre maisons, en pays perdu, qu’on ne peut aborder que par des précipices et des montagnes affreuses et où, par conséquent, on ne peut être attiré que par le seul désir de visiter et d’honorer ses Reliques._ (Dompnier, 2015)

Devenu lieu de pèlerinage, La Louvesc se transforme alors en véritable village. La construction de bâtiments cultuels, l’installation sur la commune de congrégations religieuses, la construction d’auberges, de relais pour l’accueil des pèlerins s’accompagne de la création d’activités économiques, d’équipements et de services.

1744 : Début de la construction de la seconde église de La Louvesc qui est agrandissement de la première, sur le même emplacement. Elle sera bénite en 1754. Elle sera démolie après la construction du chœur de l’actuelle basilique.

### Les hameaux entourent le village

![](/media/la-louvesq-cassini.jpg)

Sur la [carte de Cassini ]()datant de 1750, on observe que la plupart des hameaux existants aujourd’hui, étaient déjà̀ présents : Bobigneux (M. Mathieu, Alauda n°4), le Besset, la Vialette, les Sagnes, les Grands, Fournel, Maison Claire (Clairet), Malatrey, la Valette (B. Soumille, Alauda n°7), Grange Neuve.

1768 : Achat d’un terrain par le curé Bilhot pour y construire un hôpital. La construction durera cinq ans et s’achèvera en 1784 (J. Mathieu, Alauda n°8). Le bâtiment, maintenant devenu privé, est la seule construction avec un toit de lauze dans le village.

1794 : Le pillage du trésor de l’église décide les frères Buisson à cacher les restes de Saint Régis à Grange-Neuve. Ce n’est qu’en 1802 que l’église sera remise en ordre et accueillera de nouveau les reliques de St. Régis. Elle sera restaurée en 1828.

1804 : Le 3 germinal de l’an 12 (24 mars 1804) le Préfet d’Ardèche autorise un marché par semaine et trois foires à Lalouvesc. Autorisation retoquée par le Ministre de l’Intérieur en 1807 qui n’autorise plus que le marché de jeudi ! Heureusement en 1819 une ordonnance du roi Louis XVII accorde deux nouvelles foires au village. Le marché du jeudi et les foires perdurent encore (J-C. Régal, Alauda n°6).

### Un village avec deux centres

![](/media/lalouvesc-sur-le-cadastre-napoleon.png)

_(Archives de l’Ardèche)_

_Lalouvesc sur le cadastre de Napoléon_

Sur le cadastre napoléonien de 1836, on observe que « le centre » était bipolaire : Lalouvesc et Grand- Lieu. Grand-Lieu constitue le village d’origine avec la maison Delubac. Maison Claire était également présente. On note la présence du ruisseau du Val d’Or et son étang, et du Lac de Grand Lieu (place du lac).

## 1821-1898 - Constructions religieuses

![](/media/appel-aux-dons-basilique-lalouvesc.png)

_Appel aux dons pour la construction de la Basilique_

1826-27 : Constatant que la promiscuité́ dans les auberges ne favorise guère le pèlerinage surtout pour les femmes, le Père Terme et Thérèse Couderc, fondent la congrégation des sœurs de Notre-Dame-du-Cénacle et entreprennent la construction du Cénacle lieu dédié́ à l’accueil, l’animation des retraites et à la formation chrétienne (J. Tixador, Alauda n°4).

1849 : Les jésuites achètent la maison où était décédé St. Régis et érigent une chapelle qui sera achevée en 1852. C’est l’actuelle Chapelle St. Régis dans la rue du même nom.

1858 : Installation d’une statue de St. Régis dans le bâtiment qui abrite la source (P. Roger, Alauda n°6). Cette source a été déclarée propriété communale.

1865: Pose de la première pierre de la troisième église de Lalouvesc, qui est construite à la place de la chapelle primitive. Cette construction entrainera un apport de population extérieure très important (la population communale était de 1241 habitants en 1866). L’église sera consacrée en 1877, et honorée du titre de Basilique en 1888. Elle est l’œuvre de l’architecte Lyonnais Bossan. Elle est bâtie tout en granit. Les flèches de clochers ne seront construites qu’en 1900.

1875 : Grâce à la générosité de Joséphine de Noblet d’Anglure un bourdon utilisant « 6000 hg d’airain » est coulé dans un moule précédemment utilisé pour fabriquer le bourdon de Genève. En l’honneur de la donatrice, il est baptisé « Joséphine ». Il fallut six paires de bœufs pour le monter jusqu’à Lalouvesc.

1877 : Consécration de la troisième église, ce qui ne marque pas la fin des travaux. La chapelle Sainte Agathe est construite en 1878, la chaire en 1879, la chapelle Saint-Joseph en 1880, celle de la vierge du Sacré-Cœur en 1881. Le perron, le vestibule d’entrée, et la tribune intérieure sont achevés en 1884.

En 1890 on pose les vitraux qui racontent la vie de St. Régis. Ils sont l’œuvre du maitre-verrier Lucien Begule qui a exécuté également les verrières du chœur de la basilique de Fourvières.

En 1900 on construit les deux flèches des clochers. La décoration intérieure de la coupole et du chœur sera achevée en 1934.

1896-1898 : Construction de la chapelle St. Ignace.

## 1832–10 mai 1970 - Thérèse Couderc, de la fondation du Cénacle à la canonisation

![](/media/ste-therese-743091.jpg)

_(Wikimédia)_

_Thérèse Couderc, fondatrice de la congrégation Notre-Dame du Cénacle_

Marie-Victoire Couderc est née à Sablières en Ardèche, le 1er février 1805. En janvier 1826, elle quitte sa famille et rejoint la communauté d’Aps où elle prend le nom de Thérèse.

En 1827, le Père Terme appelle Thérèse et deux autres sœurs à Lalouvesc pour prendre en charge la congrégation du Cénacle. Vers 1832, Thérèse devient supérieure générale de toutes les communautés fondées par le père Terme. En 1838, épuisée Thérèse démissionne et s’enfonce dans le silence et l’humilité.

En 1844, une communauté s’installe à Fourvière : Thérèse est nommée assistante. La congrégation prend le nom de Notre Dame du Cénacle. En 1856, elle devient supérieure de la communauté de Tournon. En 1860, elle est nommée assistante de la supérieure de Montpellier En 1867, elle revient à Lyon qu’elle ne quittera plus. Thérèse meurt le 26 septembre 1885 au Cénacle de Fourvière. Quelques jours plus tard, son corps est transporté à Lalouvesc.

1951-52 : Béatification de Mère Thérèse Couderc par le Pape Pie XII.

Le 10 mai 1970, Paul VI l’inscrit au nombre des Saintes. Lalouvesc, lieu de pèlerinage à Saint Jean-François Régis est devenu aussi lieu de pèlerinage à Ste Thérèse Couderc.

2018 : Translation du corps de Ste. Thérèse Couderc du Cénacle à la Basilique.

## 1900-1960 - Pèlerinage des hommes

![](/media/pelerinage-des-hommes-vers-1910.jpg)

_Pèlerinage des hommes vers 1910_

1900 : Premier grand pèlerinage des hommes. Lalouvesc devient un lieu de pèlerinage très fréquenté.

Vers 1932 le lac du Grand Lieu est comblé pour pouvoir accueillir les autocars des pèlerins sur la « Place du Lac » qui devient un parking et accueillera aussi le marché..

1950 : Mgr Roncalli, futur pape Jean XXIII, nonce apostolique, et le Cardinal Gerlier, Primat des Gaules, président les fêtes du 50e anniversaire du pèlerinage des hommes.

Une retombée économique inattendue des pèlerinages est le développement de la fabrication des chapelets dont Lalouvesc était devenu un producteur majeur. Au milieu du XIXe siècle, on en produisait plus de 9.000 par mois ! (C. Caillet, Alauda n°2).

## 1900–1950 - Lalouvesc, station estivale

![](/media/institut-d-aerotherapie-lalouvesc.png)

_Entrée de l'institut d'aérothérapie_

Au début du XX siècle le tourisme commença à se développer, Lalouvesc devenant, peu à peu, une station de villégiature. D'après un témoin de l'époque : _Vers 1900, on ne comptait guère comme estivants que cinq à six familles ... Dès 1904, on note que la colonie est de plus en plus nombreuse et plusieurs commencent à s'inquiéter !_ Et, le 28 août 1904, _La Croix de l'Ardèche_ écrit que : _Saint Régis n'est plus l'unique attraction !_

Au même moment, le docteur Vincent de Lyon avait fait des bâtiments du Cénacle, inoccupés suite à la séparation de l’Eglise et de l’Etat, une maison de repos, baptisée par lui " Institut d' Aérothérapie", avant qu'il ne publie, en 1911, son célèbre opuscule, intitulé : _Lalouvesc, station de cure d'air et d'altitude, étude médicale et souvenirs religieux_. L'établissement accueille, par exemple en 1905, 375 consultants, dont certains pour des actes de petite chirurgie.

A l’époque La Louvesc comptait environ une douzaine d'hôtels et d'auberges, certains établis depuis fort longtemps. Les établissements anciens sont modernisés, des villas se construisent, les premiers cours de tennis s'installent. Le chemin de Saint-Félicien devient le _Boulevard des Élégants_. Les estivants fréquentent le Bois de Versailles, le Val d'Or, le Pont des Soupirs ... Quant aux Sept Fayards, c'était un des grands lieux de promenades familiales.

1905 : Les villageois créent le bureau de renseignement de Lalouvesc. Le Syndicat d’initiative » de Lalouvesc sera déclaré en 1928 (L. Lhopital, 2017).

1910 : Premier concours de ski en Ardèche. Lalouvesc est une des étapes. Cet évènement sera renouvelé annuellement jusqu’à la première guerre mondiale. (J. & M. Mathieu, Alauda n°7).

### Un village prospère

Vers 1910, la commune comptait environ 1.100 habitants, soit plus du double de l'époque actuelle. Il faut dire que les hameaux et les écarts de Lalouvesc étaient beaucoup plus peuplés que maintenant, du fait de nombreuses familles de cinq, six, enfants, et plus.

Les activités des habitants étaient beaucoup plus tournées vers l'agriculture et l'élevage qu'à l'époque actuelle. Mais, hormis quelques grosses fermes forestières, il s'agissait généralement de petites exploitations, qui permettaient aux familles de vivre en autarcie, et de vendre le surplus de leurs productions aux marchés alentours et à ceux du jeudi, à Lalouvesc (P. Roger, Alauda n°7).

Il y avait 33 petits commerces dont de nombreux commerces de bouche (J-C Régal, n°7). Mais il y avait encore plus de commerces de souvenirs dont un grand nombre dans la rue de la Fontaine (P. Roger, n°5).

## 1921–1960 - Colonies de vacances

![](/media/colonie-d-oran-lalouvesc.png)

_Colonie de vacances d'Oran en représentation théâtrale devant la maison Ste Monique_

1921 : Construction de l’abri du Pèlerin. La même année, le Père Féraud crée « Les colonies de vacances à Lalouvesc » pour les enfants des Oranais.

1923 : les Colonies d’Oran construisent la Maison Ste Monique (pour les filles) et la Maison St. Augustin (pour les garçons). Ces bâtiments seront utilisés comme colonies de vacances jusque dans les années 60 (J-C Régal, Alauda n°4).

## 1960–2000 - Difficultés d’un village rural

![](/media/le-crouzet-lalouvesc.png)

_Travail des champs sur la ferme du Crouzet_

A partir du milieu du XX siècle, la population communale connait une baisse. Les recensements officiels indiquent un peu plus de 400 habitants vers 1800, jusqu'à 1.242 habitants en 1866, et un peu plus de 1.000 habitants jusqu’en 1900 grâce à la construction de la basilique et des édifices religieux.

La première guerre mondiale, puis l’exode rural ramènent le niveau de population permanente autour de 700 habitants dans la première moitié́ du XX siècle et autour de 500 habitants vers les années 1970.

Elle s'est maintenue autour de 500 habitants dans les années 1970 à 2000, puis a diminué pour atteindre aujourd'hui environ 400 habitants. En été, par contre, le nombre de résidents augmente jusqu'à tripler.

Le village subit des évènements et des tendances négatives extérieures. 46 noms figurent sur le monument au mort du village pour la première guerre mondiale (P. Besset, Mémoire). Les colonies d'Oran ferment suite à l'indépendance de l'Algérie. La montée de l'agriculture productiviste pèse sur les petites exploitations de montagne. La dernière ferme en exploitation dans les années 2020, Le Crouzet, est passé d'un cheptel de 22 vaches dans les années 50 à 80 aujourd'hui, ce qui nécessite une surface de pâturage bien plus large. La baisse de la pratique religieuse, jointe au vieillissement des congrégations religieuses, conduit à la désaffection des bâtiments religieux dont plusieurs sont vendus ou en vente. Et, plus largement, les politiques nationales privilégient le développement urbain, oubliant les campagnes notamment pour la couverture des services publics.

Les conséquences de l'ensemble de ces phénomènes sont lisibles dans le village : plusieurs hôtels et commerces sont obligés de fermer.

## 2000–2020 - Les temps changent

![](/media/plan-lalouvesc-mairie.jpg)

_(Jean-Pascal Hamard)_

_Plan de Lalouvesc_

Depuis les années 2000, le Sanctuaire, la Mairie de Lalouvesc et de nombreux bénévoles regroupés dans des associations allient leur force pour rendre au village son attractivité.

Lalouvesc est devenu membre de l'association des villes sanctuaires en 2019.

De nombreuses manifestations et évènements de qualité (concerts, expositions, cinéma, etc.) animent le village en été. Leur programmation est consultable sur l'affiche Lalouv'estival.

Le village s’appuie sur sa situation géographique, sur la recherche de sens et l’engagement citoyen des populations, sur la préservation de l’environnement et sur l’ensemble des métiers de la filière bois pour son développement vers un tourisme adapté aux temps nouveaux.

## Sources de l’histoire de Lalouvesc

Les textes de cette histoire ont été pour la plupart rédigés par Pierre Roger, leur édition a été assurée par Jean-Michel Salaün. Remerciements à Paul Besset pour sa relecture attentive.

**Bibliographie**

* Besset P. (2018) Lalouvesc 1914-1918 - Morts pour la France. Mémoire. 52 p.
* Caillet C.(2013) La fabrication des chapelets à Lalouvesc. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°2 p. 10-12.
* Caillet C. et Mathieu M., (2014-2015) Maison Claire, une maison forte et son seigneur au XVIIe siècle. L’inventaire après décès de Gilles de la Franchiere. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°3 p. 3-8.
* Chavarot M-C. (1986) Mentalités nobiliaires en Vivarais au XIVe siècle : la succession de Briand de Retourtour . Bibliothèque de l'École des chartes, 144-1 p. 87-143.
* de Curley F. (1886) Le tombeau de St. Régis à Lalouvesc » librairie générale catholique et classique Vitte et Perussel, 3 et 5 place Bellecour à Lyon. 342 p.
* Dompnier, B. (2015). _Les Jésuites et la dévotion populaire : Autour des origines du culte de saint Jean-François Régis (1641-1676)_ In : _Missions, vocations, dévotions : Pour une anthropologie historique du catholicisme moderne_ \[en ligne\]. LARHRA, p. 271-284.
* Iratzoquy P., Ayel J., Chaleat A., Piat I. (2020) La basilique de Lalouvesc. Supplément à la revue Saint Régis et sa mission. 86 p.
* Le Blévec D. (1977) Aux origines des hospitaliers de Saint-Jean de Jérusalem : Gérard dit « Tenque » et l'établissement de l'Ordre dans le Midi. Annales du Midi , Tome 89, N°132, p. 137-151.
* Lhopital L. (2017) L’office du tourisme de Lalouvesc et du Val d’ay, de sa création à aujourd’hui. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°5 p. 28-31.
* Mathieu J. (2013) Un épisode des guerres de religion à Lalouvesc. Le pillage de l’église en1621. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°2 p. 13-14.
* Mathieu J. (2020) Les premières années de l’hôpital de Lalouvesc. D’une maison de charité à l’établissement de santé 1ere partie. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°8 p. 3-10.
* Mathieu M. (2016). Bobigneux, histoire d’un hameau de Lalouvesc (XV1e -XXIe siècle) Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°4 p. 3-6.
* Mathieu J. & Mathieu M. (2019). Les sports d’hiver à Lalouvesc. Du syndicat d’initiative au Foyer de ski de fond. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°7 p. 25-30.
* Mathieu M. & Mathieu J. (2012). Terriers et compoix de Lalouvesc (XIVe – XVIIe) sources disponibles pour l’histoire locale. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°1 p 7-11.
* Poidebard R. (1928). L’église de Veyrines : XIIe siècle. Aubenas : C. Habauzit. 64 p.
* Régal J-C. (2016). Les colonies d’Oran à Lalouvesc. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°4 p. 16-21.
* Régal J-C. (2016). Foires et marchés à Lalouvesc. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°6 p. 8-10.
* Régal J-C (2019). De tuades en boucheries 1900-1930. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°7 p. 9-12.
* Roger P. (2014-2015). Les cartes postales anciennes de Lalouvesc : les hôtels. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°3 p. 20-27.
* Roger P. (2017). Les cartes postales et les photos anciennes racontent l’histoire de la rue de la Fontaine. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°5 p. 21-27.
* Roger P. (2019). Les cartes postales anciennes de Lalouvesc : la fontaine St. Régis. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°6 p. 21-25.
* Roger P. (2019). Les gens de Lalouvesc au début du XXe siècle à travers les cartes postales et les photos anciennes. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°7 p. 15-24.
* Roger P. (2020). Les cartes postales anciennes de Lalouvesc : histoire de la basilique. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°8 p. 17-28.
* Soumille B. (2019) La Valette, chronique familiale d’un hameau, XV – XIX siècles. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°7 p. 3-8.
* Tixador J. (2016) Le Cénacle à Lalouvesc, des origines à 1951. Du côté de Lalouvesc. Bulletin de l’ALAUDA, association culturelle de Lalouvesc. Mairie de Lalouvesc. N°4 p. 7-15.

**Les sites**

* [https://lesamisdeveyrines.org](https://lesamisdeveyrines.org "https://lesamisdeveyrines.org")
* [http://www.pierre-armand-roger.fr/lalouvesc/index.htm](http://www.pierre-armand-roger.fr/lalouvesc/index.htm)
* [https://fr.wikipedia.org/wiki/Commanderie_de_Sainte-](https://fr.wikipedia.org/wiki/Commanderie_de_Sainte- "https://fr.wikipedia.org/wiki/Commanderie_de_Sainte-")[Épine](https://fr.wikipedia.org/wiki/Commanderie_de_Sainte-Épine)
* Cartuaire de l’abbaye de Saint-Chaffre du Monastier : [https://gallica.bnf.fr/ark:/12148/bpt6k736641.texteIm](https://gallica.bnf.fr/ark:/12148/bpt6k736641.texteIm "https://gallica.bnf.fr/ark:/12148/bpt6k736641.texteIm")

## Pour en savoir plus

Voir aussi [l'histoire du village sur le site de Pierre Roger](http://www.pierre-armand-roger.fr/lalouvesc/index.htm), plus de 500 photos anciennes sur les [Archives départementales de l'Ardèche](https://archives.ardeche.fr/archive/resultats/iconographie/lineaire/FRAD007_ICONOGRAPHIE_COLLECTION_DURRENMATT/n:105?rech_lieu=%22Lalouvesc%22&type=iconographie), une intéressante [collection de cartes postales anciennes sur un site de généalogiste](https://www.geneanet.org/cartes-postales/cartographie?idCard=20432&lat=45.1200&lon=4.5323).