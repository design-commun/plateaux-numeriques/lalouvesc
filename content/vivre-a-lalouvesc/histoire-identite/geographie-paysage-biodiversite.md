+++
date = ""
description = "Une situation exceptionnelle"
header = "/media/leve-de-soleil-bellevue.jpg"
icon = "🌄"
subtitle = "Une situation exceptionnelle"
title = "Paysage, biodiversité"
weight = 3

+++
## Paysage

Sur un col, Lalouvesc s'ouvre d'un côté sur la vallée du Rhône, le Mont Blanc et toute la chaîne des Alpes et, de l'autre, sur le Mont Mézenc et l'ensemble des Cévennes. De magnifiques panoramas changeant au fil des saisons et de la météo enchantent le quotidien des Louvetous et émerveillent les visiteurs.

{{<grid>}}{{<bloc>}}

![](/media/cevennes-2021-o-de-framond.jpg)Le village et les Cévennes

![](/media/les-cevennes-ch-loucel.jpg)

![](/media/les-cevennes-o-de-framond.JPG)Les Cévennes vues du village

{{</bloc>}}{{<bloc>}}

![](/media/paysage-lalouvesc.png)Le village et les Alpes

![](/media/paysage-lalouvesc-3.JPG)

![](/media/les-alpes-o-de-framond.jpg "Photo O. de Framond")

![](/media/les-alpes-photo-m-bober.jpg)Les Alpes vues du village

{{</bloc>}}{{</grid>}}

## Biodiversité

La Commune de Lalouvesc par sa situation est un lieu propice à la diversité animale et végétale. On y croise de nombreuses espèces aussi bien dans les sous-bois que dans les prairies.

Les milieux boisés recouvrent une grande portion du territoire et constituent en partie la trame verte de la Commune. Ils sont constitués :

* de bois mixtes Feuillus/Résineux sous forme de hêtraie-sapinière (Fagus sylvatica accompagné de Abies alba), avec sous bois de Sorbier, Cornouiller... ainsi que des myrtilles et des champignons.
* de bois de conifères en grande partie issus de plantations : Douglas, Epicéas, Mélèzes... Ces milieux ne sont pas très diversifiés du fait des sous-­bois sombres et ferment le paysage.
* d’une hêtraie au « Peigne du Chat » particulièrement remarquable,
* de boisements lâches situés sur le flanc Sud du Suc de Mirabel et la frange Sud du bois de Bellevue.

On y trouve aussi des pelouses sèches. Les pelouses sèches sont des terrains dominés par des formations végétales rases composées de grandes herbacées, de mousses ou de lichens. Il n’y a pas une présence significative d’arbres ou d’arbustes. Les sols sont qualifiés de perméables, cela veut dire qu’ils ne retiennent pas l’eau comme un sol calcaire. Les sols qui ont des pelouses sèches ont d’autres caractéristiques, comme le fait de présenter une forte pente ou encore d’être qualifié de sol pauvre en éléments nutritifs.

L’ensemble de ces critères permettent à une faune et une flore très caractéristiques de se développer. On y retrouve de nombreuses plantes patrimoniales (Aster amelle, Immortelles, …), les plus connues étant les **orchidées** avec 40 espèces inféodées aux pelouses sèches.

Mais aussi…  
**Des papillons** dont certaines espèces sont typiques des pelouses sèches comme l’Azuré du serpolet, ou l’Hermite, …  
**Des oiseaux** habitant ce type de milieux secs : les pies-grièches, le guêpier d’Europe, …  
**Des reptiles,** qui sont les hôtes privilégiés des pelouses, on y trouve couleuvres, vipères et lézards.  
**…et bien d’autres !** Sauterelles, criquets, Mantes religieuses, araignées, …

![](/media/pelouses-seches-lalouvesc.jpg)

Sept pelouses ont été identifiées sur la Commune de Lalouvesc. Lalouvesc accueille notamment des pelouses particulières, liées à l’altitude et au niveau trophique bas des sols (sol peu riche, oligotrophe) : les pelouses acidiclines oligotrophes (en orange sur la carte), ces deux pelouses sont particulièrement riches et à enjeux. D’autres pelouses plus typiques de l’ouest du Massif Central sont également présentes sur la commune, les pelouses à Fétuque d’Auvergne (en jaune sur la carte). Deux espèces particulièrement intéressantes y ont été observées, la Pie-grièche écorcheur, à proximité du bourg, et l’Orchis grenouille, sur les pelouses situées vers le Besset.  
Le discret Orchis grenouille ou Satyrion vert se rencontrent aussi sur les pelouses oligothophes de montagnes.

#### Photos prises par le père Olivier de Framond sur la Commune

{{<grid>}}{{<bloc>}}

![](/media/o-de-framond-30_mar-2.jpg "Mars")

![](/media/o-de-framond-32_avr-1.jpg "Avril")

![](/media/o-de-framond-39_avr-8.jpg "Avril")

![](/media/o-de-framond-54_mai-14.jpg "Mai")

![](/media/o-de-framond-58_mai-18.jpg "Mai")

![](/media/o-de-framond-61_juin-2.jpg "Juin")

![](/media/o-de-framond-64_juil-3.jpg "Juillet")

![](/media/o-de-framond-67_juil-6.jpg "Juillet")

![](/media/o-de-framond-70_aou-3.JPG "Août")

![](/media/o-de-framond-72_aou-5.jpg "Août")

![](/media/o-de-framond-76_aou-9.jpg "Août")

![](/media/o-de-framond-78_aou-11.jpg "Août")

![](/media/o-de-framond-1_sep-1.jpg "Septembre")

{{</bloc>}}{{<bloc>}}

![](/media/o-de-framond-31_mar-3.jpg "Mars")

![](/media/o-de-framond-33_avr-2.jpg "Avril")

![](/media/o-de-framond-40_avr-9.jpg "Avril")

![](/media/o-de-framond-57_mai-17.jpg "Mai")

![](/media/o-de-framond-60_juin-1.jpg "Juin")

![](/media/o-de-framond-63_juil-2.jpg "Juillet")

![](/media/o-de-framond-65_juil-4.jpg "Juillet")

![](/media/o-de-framond-68_aou-1.jpg "Août")

![](/media/o-de-framond-71_aou-4.jpg "Août")

![](/media/o-de-framond-73_aou-6.jpg "Août")

![](/media/o-de-framond-77_aou-10.jpg "Août")

![](/media/o-de-framond-79_aou-12.jpg "Août")

![](/media/o-de-framond-3_sep-3.jpg "Septembre")

![](/media/o-de-framond-4_sep-4.jpg "Septembre")

{{</bloc>}}{{</grid>}}

## Pour en savoir plus

Un chapitre est consacré au paysage dans le [diagnostic réalisé pour le PLU](/media/plu-piece-1-rdp-tome1-lalouvesc-appobation-juillet-2018.pdf).