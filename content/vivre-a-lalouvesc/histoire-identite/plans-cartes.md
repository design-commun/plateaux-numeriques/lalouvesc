+++
date = ""
description = ""
header = "/media/plan-lalouvesc-geoportail.jpg"
icon = "📍"
subtitle = "S'orienter dans le village"
title = "Plans, cartes"
weight = 1

+++
## Situation

![](/media/carte-situation-lalouvesc.jpg)

Lalouvesc est située en Ardèche du nord, l'Ardèche verte, au sud d'Annonay à une heure et demi de Lyon ou une heure de Saint-Étienne, de Valence et du Puy-en-Velay en voiture.

## Plans du village

**Attention, un nouvel adressage ayant été réalisé en 2022, les cartes ne sont pas toujours actualisées.**

Consulter le [plan en haute définition](/media/plan-lalouvesc-mairie-hd.jpg).![](/media/plan-lalouvesc-mairie.jpg)

* [Lalouvesc sur Géoportail](https://www.geoportail.gouv.fr/carte)
* [Lalouvesc sur Googlemap](https://www.google.com/maps/place/07520+Lalouvesc/@45.1168991,4.4865486,13z/data=!3m1!4b1!4m5!3m4!1s0x47f5714817207ac3:0x4093cafcbeb8e10!8m2!3d45.118592!4d4.535528)
* [Lalouvesc sur Openstreetmap](https://www.openstreetmap.org/search?query=lalouvesc#map=13/45.1169/4.5216)
* [Lalouvesc sur Adresse.data.gouv.fr](https://adresse.data.gouv.fr/base-adresse-nationale/07128#11.9/45.117/4.5215/0/2)

## Cartes diverses

### État des routes

* [Restrictions de gabarit](https://geoids.geoardeche.fr/gabarits/index.html) (GéoArdèche)
* [Travaux en cours](https://geoids.geoardeche.fr/infostrafic07/index.html) (GéoArdèche)
* [Comptage routier sur l'Ardèche](https://geoids.geoardeche.fr/comptages_routier/index.html) (GéoArdèche)

### Cartes des logements

* [Carte des ventes immobilières](https://app.dvf.etalab.gouv.fr/) (indiquez “Ardèche” puis “Lalouvesc” sur le formulaire)
* [Carte des loyers](https://www.ecologie.gouv.fr/carte-des-loyers) (commune par commune)

### Cartes anciennes

* [Carte de Cassini](https://www.geoportail.gouv.fr/donnees/carte-de-cassini), (indiquez "Lalouvesc" dans le formulaire)
* [Cadastre de Napoléon](https://archives.ardeche.fr/archive/resultats/planscadastraux/n:102?RECH_Commune=Lalouvesc&RECH_type_de_plan=Napol%C3%A9onien&type=planscadastraux) (1832), Ensemble des feuillets disponibles.
* [Tableau d’assemblage du cadastre de Napoléon](https://archives.ardeche.fr/ark:/39673/vtabe0cac18d6668963/dao/0/layout:table/idsearch:RECH_abb2e8549b0a14aaf7184914ccf1d20d#id:1174898366?gallery=true&brightness=100.00&contrast=100.00&center=3862.979,-2565.795&zoom=15&rotation=0.000&print=true)
* [Photographies aériennes 1950-1965](https://www.geoportail.gouv.fr/donnees/photographies-aeriennes-1950-1965) (géoportail)