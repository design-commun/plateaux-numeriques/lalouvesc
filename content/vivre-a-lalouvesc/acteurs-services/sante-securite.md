+++
date = ""
description = "Médecin, infirmiers, pharmacie, pompiers, gendarmerie, Ehpad,Admr"
header = "/media/pharmacie-lalouvesc.jpg"
icon = "⚕️"
subtitle = "Médecin, infirmiers, pharmacie, pompiers, gendarmerie, Ephad, ADMR"
title = "Santé - sécurité - seniors"
weight = 4

+++
## Urgences

* Santé : 15 (SAMU) 18 (pompiers) ou 112
* Sécurité : 17 (police, gendarmerie)

## Santé

### Médecin généraliste

Docteur Didier Chieze

Le balcon des Alpes  
Rue de la Fontaine  
07520 Lalouvesc
Tél 04 75 67 82 55  
Port. 06 08 45 63 64

### Cabinet de soins infirmiers

Chemin de l'école  
07520 Lalouvesc  
infirmierslalouvesc@gmail.com

* Cyrille Reboullet (06 07 21 64 49)
* Perrine Fraysse-Bourguignon (07 66 89 72 38)

### Pharmacie

Cécile Goetz Couette
2 rue de la fontaine  
07520 Lalouvesc  
04 75 67 84 84

* Lundi, mardi, jeudi, vendredi : 9h-12h/15h-18h30.
* Mercredi : 9h-12h.
* Samedi : 9h-12h/15h-17h.

## Sécurité

### Pompiers

![](/media/le-nouveau-camion-10-10-2020.jpg)

La Commune accueille un centre de secours de pompiers.(derrière la Mairie) rue des Cévennes, 07520 LALOUVESC.

Pour appeler les pompiers, faire le 18.

Pour toutes informations sur les pompiers volontaires de Lalouvesc, voir sur [cette page](/projets-avenir/gerer-le-bien-commun/des-pompiers-au-service-de-tous/).

### Gendarmerie

Brigade de gendarmerie de Satillieu  
935 Rue Jean Moulin  
07290 Satillieu  
04 75 34 96 31

Brigade de gendarmerie de St Félicien  
Route de Colombier-le-Vieux  
07410 Saint-Félicien  
04 75 06 00 44

* Lundi : de 08h00 à 12h00, de 14h00 à 18h00
* Mardi : de 08h00 à 12h00, de 14h00 à 18h00
* Mercredi : de 08h00 à 12h00, de 14h00 à 18h00
* Jeudi : de 08h00 à 12h00, de 14h00 à 18h00
* Vendredi : de 08h00 à 12h00, de 14h00 à 18h00
* Samedi : de 08h00 à 12h00, de 14h00 à 18h00
* Dimanche : de 09h00 à 12h00, de 15h00 à 18h00
* Jours fériés : de 09h00 à 12h00, de 15h00 à 18h00

## Seniors

### EHPAD (établissement d'hébergement pour personnes âgées dépendantes)

Lalouvesc accueille un EHPAD su un terrain dominant les Alpes.

Balcon des Alpes  
Rue de la Fontaine  
07520 Lalouvesc
Tel. : 04 75 67 81 22

Toutes [les informations sur l'EHPAD](https://www.pour-les-personnes-agees.gouv.fr/annuaire-ehpad-et-maisons-de-retraite/EHPAD/ARDECHE-07/lalouvesc-07520/ehpad-le-balcon-des-alpes/070780531) (hébergement, aides, accompagnement, tarifs)

### ADMR (aide à domicile en milieu rural)

[Site web](https://www.admr-ardeche.fr/)
112 Avenue Ferdinand Janvier  
07100 Annonay  
[nord.ardeche.rhone@fede07.admr.org](mailto:nord.ardeche.rhone@fede07.admr.org)  
04 75 33 49 28

**Lundi au jeudi** de 8h30 à 12h et de 13h30 à 17h.  
**Vendredi** de 8h30 à 12h et de 13h30 à 16h.