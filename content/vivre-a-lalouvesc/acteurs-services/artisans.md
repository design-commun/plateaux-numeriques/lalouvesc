+++
date = ""
description = ""
header = ""
icon = "✏️"
subtitle = ""
title = "Artisans"
weight = 1

+++
{{<grid>}} {{<bloc>}}

### Couette Fuste

Construction d'immobilier en rondin

Les Grands,
07520 - Lalouvesc

Mail : couette.fuste@gmail.com
Tél : 06 78 65 12 05
Site : [Url](https://www.couette-fuste.com/)

{{</bloc>}} {{<bloc>}}

![](/media/couette_fuste_2022.jpg)

{{</bloc>}} {{</grid>}}

---

{{<grid>}} {{<bloc>}}

### L'étoffe

Votre Artisan Tapissier vous propose ses services pour apporter de l'étoffe à vos paysages intérieur.

14 rue des alpes
07520 - Lalouvesc

Tél : 07 81 99 18 13
Mail : etoffe@orange.fr
{{</bloc>}} {{<bloc>}}

![](/media/site_artisan_2022.jpg)
{{</bloc>}} {{</grid>}}

---

{{<grid>}} {{<bloc>}}

### Menuiserie Besset fils

Escaliers, charpente, couverture, maisons ossature bois et isolants durables.

Route de Rochepaule
07520 - Lalouvesc

Tél : 04 75 67 81 98
Tél : 06 88 49 94 64
Tél : 06 76 86 39 85
Fax : 04 75 67 83 63
Mail : bessetfilsmenuiserie@orange.fr
Site : [Url](https://menuiseriebesset.fr/)
{{</bloc>}} {{<bloc>}}

![](/media/menuserie_besset_2022-min.jpg)

{{</bloc>}} {{</grid>}}

---

{{<grid>}} {{<bloc>}}

### Ondu'ligne Coiffure

7 rue des Alpes
07520 - Lalouvesc

Tél : 04 75 67 84 48
{{</bloc>}} {{<bloc>}}

![](/media/coiffeur_2022.jpg)

{{</bloc>}} {{</grid>}}

---

{{<grid>}} {{<bloc>}}

### Scierie Frédéric Poinard

Route de St Bonnet
07520 Lalouvesc

Tél : 04 75 67 80 53
{{</bloc>}} {{<bloc>}}

![](/media/scierie_2022.jpg)

{{</bloc>}} {{</grid>}}