+++
date = ""
description = ""
header = "/media/trail-des-sapins-accueil.jpg"
icon = "👫"
subtitle = ""
title = "Annuaire des associations"
weight = 1

+++
## Toutes les associations de Lalouvesc

Le bénévolat, l'animation de la collectivité, l'entretien du bien commun sont depuis toujours dans l'ADN des Louvetous. La meilleure façon de s'intégrer au village est d'adhérer à une ou plusieurs associations. Il y en a pour tous les goûts et pour tous les moments de la vie.

|               NOM               |         PRESIDENT(E)         |                 ADRESSE                |    TELEPHONE   |                MAIL                |                                               SITE WEB                                               |
|:-------------------------------:|:----------------------------:|:--------------------------------------:|:--------------:|:----------------------------------:|:----------------------------------------------------------------------------------------------------:|
| ACCA (Chasse)                   |         SONIER Jérôme        |           MAIRIE de LALOUVESC          | 06 99 07 14 67 |       jeromesonier@hotmail.fr      |                                                                                                      |
| ALAUDA   (Histoire)             |       REGAL Jean-Claude      |           MAIRIE de LALOUVESC          | 04 75 32 51 01 |      regal.jeanclaude@neuf.fr      |                                                                                                      |
| AMICALE DES   SAPEURS POMPIERS  |       SERAYET Corentin       |       Chemin Grosjean  LALOUVESC       | 06 40 71 93 12 |     cserayet@outlook.fr     |                                                                                                      |
| APPEL (Parents   d'élèves)      |       COULAUD Amandine       |        Ecole St Joseph LALOUVESC       | 06 87 90 59 78 |      amandine.coulaud@yahoo.fr     |                                                                                                      |
| BIBLIO'CHOUETTE                 |       DEVESA Catherine       |           MAIRIE de LALOUVESC          | 06 27 00 59 87 |   bm-lalou@inforoutes-ardeche.fr   |                                                                                                      |
| CARREFOUR DES ARTS            |         BESSET Julien        |           MAIRIE de LALOUVESC          | 06 32 23 84 20 |   contact@carrefourdesarts-lalouvesc.fr   | http://www.carrefourdesarts-lalouvesc.fr      https://www.facebook.com/carrefourdesartsdelalouvesc/ |
| CLUB DES 2 CLOCHERS (Seniors) |        IVANEZ Georges        |           MAIRIE de LALOUVESC          | 06 62 00 45 69 |      georges.ivanez@orange.fr      |                                                                                                      |
| COMITE DES   FETES              | DESGRAND-FOUREZON   Nathalie |       MAIRIE de LALOUVESC   BP16       | 06 72 76 65 96 | comitedesfetes.lalouvesc@gmail.com |                           https://www.facebook.com/comitedesfeteslalouvesc/                          |
| EQUIFONTCOUVERTE   (Cheval)     |        DUPRE Françoise       |        Domaine de   Fontcouverte       | 04 75 34 99 40 |     francoise.dupre3@orange.fr     |                                                                                                      |
| FNACA (Anciens combattants)   |         POINARD Jean         |           MAIRIE de LALOUVESC          | 04 75 67 80 65 |                                    |                                                                                                      |
| FOOT USPL                       |        FOUREZON Romain       |           MAIRIE de LALOUVESC          | 06 78 56 11 40 |     romainfourezon07@gmail.com     |                                                                                                      |
| FOYER DU SKI DE FOND          |       BESSET Guillaume       |           MAIRIE de LALOUVESC          | 06 76 86 39 85 |         gus.besset@yahoo.fr        |                                                                                                      |
| JARDINS DU HAUT-VIVARAIS         |       POURTAU CAZALET Xavier      |      Mont Besset               | 06 86 04 55 71 |      jardinsduvivarais@gmail.com      |                                                                                                      |
| LES AMIS DU   CENACLE           |                              |   Maison Thérèse   Couderc LALOUVESC   |                |                                    |                                                                                                      |
| LES MONTS DU   BALCON (Ephad)   |        BOIDIN Isabelle       |     Maison de retraite   LALOUVESC     | 06 48 32 84 86 |    animationlalouvesc@orange.fr    |                                                                                                      |
| LES SALES   GOSSES              |        TRACOL Colette        |     22 rue des Cévennes   LALOUVESC    | 06 26 56 62 63 |                                    |                                                                                                      |
| LYRE   LOUVETONNE (Musique)     |        ROCHE Stéphane        |           MAIRIE de LALOUVESC          | 06 76 82 94 47 |     stephane.roche123@orange.fr    |                                                                                                      |
| OGEC (Gestion   de l'école)     |         OLLIER Claude        |       Ecole St Joseph   LALOUVESC      |                |       claude.ollier@yahoo.fr       |                                                                                                      |
| PROMENADES   MUSICALES          |         CHIEZE Didier        | Villa les Vernes   Rochelipe LALOUVESC | 06 08 45 63 64 |     didier.chieze819@orange.fr     |                             http://www.promenadesmusicales-lalouvesc.com/                            |
| THEATRE DE LA   VEILLEE         |        BRUNEL Frédéric       |       4 rue des Alpes   LALOUVESC      | 04 75 32 25 28 |    theatredelaveillee@orange.fr    |                                                                                                      |
| TENNIS   FAMILIAL               |       MIRIBEL Elisabeth      |                                        | 06 63 63 86 82 |      miribel.elisabeth@sfr.fr      |                                                                                                      |