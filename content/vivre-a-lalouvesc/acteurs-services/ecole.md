+++
date = ""
description = "Services publics"
header = ""
icon = "🆗"
subtitle = ""
title = "Ecole - Poste - Bibliothèque"
weight = 3

+++
## L'école primaire Saint Joseph

L'école St Joseph est une école privée sous contrat à classe unique allant de la Très petite section au CM2 caractérisée par une pédagogie différenciée (d'inspiration Montessori et Freinet).

Amandine Verdun - chef d'établissement et enseignante et Véronique Petit - ASEM, accueillent les enfants dans une atmosphère bienveillante, cadrée et organisée à travers l'utilisation de nombreux ateliers autonomes et de feuilles de route adaptées à chaque niveau.

![](/media/ecole-3.jpg)

> Le nom de l’école « St Joseph » provient de notre tutelle, explique la directrice. Ainsi dans cette école, nous cherchons à vivre au mieux l’esprit des sœurs St Joseph et leurs valeurs : bienveillance, accompagnement, éducation au choix, humilité, rigueur, disponibilité.
>
> Le projet d'établissement (en cours d’élaboration) véhicule notamment le développement de l'autonomie dans un climat de classe serein tourné vers la bienveillance et le goût d'apprendre. Le projet développe aussi un volet autour de l'éco-responsabilité que nous avons chacun d'entre nous envers notre planète.
>
> L'école porte une attention particulière à chaque élève : un accompagnement individuel et adapté est proposé à tous afin que chaque enfant puisse avancer à son rythme.

L'école propose une garderie le matin (de 8h à 8h20) et le soir (de 16h40 à 17h30) ainsi qu'un service de cantine (dans les locaux de l'EHPAD de Lalouvesc).

[Plus d'informations sur l'école primaire de Lalouvesc]()

L'école est soutenue par deux associations.

L'établissement est géré par les bénévoles de l'**OGEC** (_Organisme de Gestion de l'Enseignement Catholique)_. L'association assure la gestion :

* économique et financière : comptabilité, restaurations, ...
* sociale : employeur du personnel non-enseignant, ainsi que du Chef d’établissement
* immobilière : entretien des bâtiments scolaires.

![](/media/ecole-5.jpg)

Les membres de l'**APEL** sont des parents d'enfants scolarisés qui mettent à disposition de l'école leurs compétences, talents, enthousiasme et disponibilité, pour :

* assurer l'information des parents,
* favoriser l'entraide mutuelle des familles,
* organiser des évènements dont les bénéfices serviront à financer des activités pour les élèves.

L'APEL finance la plupart des activités proposés aux élèves : natation, équitation, tennis, arbre de Noël, sorties scolaires...

## L'Agence postale communale

Lalouvesc dispose d'une Agence postale communale. Elle est mitoyenne de la Mairie.  
Vous pouvez y faire les opérations suivantes :

* achats : timbres, enveloppes prêt-à-poster, emballages des colis,
* réexpédition ou garde des colis,
* déposer lettres et colis,
* retirer lettres recommandées, colis,
* effectuer des opérations financières de dépannage : retrait d’espèces, transmission de versements d’espèces.

### Horaires

Voir sur la [page dédiée](/mairie-demarches/demarches/adresse-horaires-plan/).

## La Biblio’chouette

Biblio’chouette, la bibliothèque municipale de Lalouvesc a été créée en décembre 2007. La nouvelle année commence bien pour la bibliothèque. Les bibliothécaires bénévoles vous accueillent et vous conseillent dans leur nouveau local refait à neuf, clair, chaleureux, directement accessible depuis la rue principale, voisin de l’Agence postale à côté de la Mairie.

![](/media/bibliochouette-2021.jpg)  
Vous y trouverez de nombreux ouvrages de fiction : romans, science fiction, policiers et bandes dessinées. Des ouvrages documentaires sur la région, les arts, la psychologie, le sport, la cuisine, l’histoire, la géographie et des ouvrages écrits en “GROS CARACTERES”.  
**Vous pouvez emprunter trois livres pour trois semaines.**  
L’inscription est individuelle et valable un an :

* Adultes : 5 € pour l’année.
* Hors commune : 7 €.
* Familles de “passage” : 7 € avec la carte d’adhérent valable toute l’année.
* Gratuité pour les moins de 18 ans.
* La consultation sur place est gratuite.

### Horaire

Mercredi de 15h à 17h, samedi de 9h30 à 11h30.

Biblio’chouette propose chaque année vers mi-juillet sa fête du livre avec de nombreux auteurs régionaux, l'entrée est gratuite pour tout le monde.

**Pour joindre la bibliothèque** :

* 04 75 67 83 67
* bm-lalou@inforoutes-ardeche.fr