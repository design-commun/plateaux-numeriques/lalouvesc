---
title: Mentions légales
date: 2020-09-06T18:45:44.284+00:00
subtitle: ''
icon: ''

---
## Informations relatives à l’éditeur

### Village de Lalouvesc

www.lalouvesc.fr est le site officiel de Lalouvesc, dont la mairie est située :

Rue des Cévennes
07520 Lalouvesc

Tél. : +33 (0)4 75 67 83 67
Fax : +33 (0)4 75 67 84 83

#### Directeur de Publication

Jacques Burriez, maire de Lalouvesc

#### Responsable technique

Timothée Goguely (Plateaux numériques)

#### Hébergeur

Tous les contenus du site sont actuellement hébergés sur le dépôt GitLab suivant : https://gitlab.com/design-commun/plateaux-numeriques/lalouvesc

Pour toute suggestion, information, réaction concernant ce site, n’hésitez pas à nous contacter.

## Conception et développement

### Plateaux numériques

Email : contact@plateauxnumeriques.fr  
Site web : [plateaux-numeriques.fr](https://plateaux-numeriques.fr/ "https://plateaux-numeriques.fr/")

## Droits d’auteurs

### Contenus éditoriaux

Toute reproduction, intégrale ou partielle, des textes publiés sur le site lalouvesc.fr n’est pas soumise à une autorisation préalable. Cette utilisation doit néanmoins mentionner explicitement et obligatoirement la référence au village de Lalouvesc, ainsi que l’url du site source de l’article. Les utilisateurs des contenus éditoriaux du site lalouvesc.fr informeront la mairie de l’usage qui en a été fait.

### Photos et images

Les photos, images fixes et animées, ne peuvent être reproduites sans autorisation préalable sollicitée auprès de la mairie. La demande précisera le support de reproduction et son mode de diffusion. Les photos et les images utilisées après autorisation mentionneront le copyright du village de Lalouvesc ou celui indiqué au moment de l’autorisation par la mairie.

La reproduction de contenus éditoriaux, de photos et d’images dans un but commercial ou publicitaire n’est pas autorisée.

### Liens hypertextes

Les liens pointant vers la page d’accueil ou l’une des pages du site lalouvesc.fr peuvent être établis sans autorisation préalable. Dans ce cas la mention explicite du lien “Site de Lalouvesc” sera faite pour informer l’internaute de la teneur du lien. Cette autorisation tacite n’est pas valable pour les sites ayant un but commercial ou publicitaire voulant ainsi se prévaloir de la référence de Lalouvesc. Elle ne s’applique pas non plus aux sites internet diffusant des informations à caractère illicite, violent, polémique, pornographique, xénophobe ou pouvant porter atteinte à la sensibilité du plus grand nombre. Liens vers les documents

Il est recommandé de ne pas pointer directement vers les documents téléchargeables du site (plaquettes, formulaires...) mais de pointer vers les pages les contenant. Le village de Lalouvesc ne saurait être tenu responsable de l’utilisation abusive qui serait faite des contenus éditoriaux, photos, images, liens et documents hors des sites dont il est l’éditeur.

### Statistiques

Le site du village de Lalouvesc utilise Simple Analytics pour collecter des statistiques anonymes sur son utilisation par les internautes (pages consultées, nombre de visiteurs, navigateurs utilisés...). Ces données nous permettent d’améliorer votre accès aux informations. Aucun cookie n’est déposé sur votre ordinateur.

## Traitement des données personnelles

### Droit d’accès, de modification et de suppression aux informations vous concernant

Vous disposez d’un droit d’accès aux données personnelles collectées, à leur rectification, à leur suppression lorsque celles-ci ne sont plus nécessaires au regard des finalités pour lesquelles elles ont été collectées. Vous pouvez vous opposer au traitement de vos données pour motif légitime. Pour exercer vos droits vous pouvez, écrire à Mairie, Rue des Cévennes, 07520 Lalouvesc. Vous pouvez aussi vous adresser à l’équipe municipale de Lalouvesc par courrier (adresse de la Mairie) ou par mail à l’adresse : mairie@lalouvesc.fr