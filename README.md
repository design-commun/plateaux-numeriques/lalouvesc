# Lalouvesc

[![Netlify Status](https://api.netlify.com/api/v1/badges/2914a2e7-840a-4786-94c3-2479a5ebb47c/deploy-status)](https://app.netlify.com/sites/lalouvesc/deploys)

Site web de la mairie de Lalouvesc en Ardèche réalisé avec [Plateaux numériques](https://gitlab.com/plateaux-numeriques).

## Installation

[![Import this project into Forestry](https://assets.forestry.io/import-to-forestryK.svg)](https://app.forestry.io/quick-start?repo=design-commun/plateaux-numeriques/lalouvesc&provider=gitlab&engine=hugo&version=0.79.0) [![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/design-commun/plateaux-numeriques/lalouvesc)



